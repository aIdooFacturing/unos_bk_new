package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;

public interface CommonService {

	String getWorkerList(ChartVo chartVo) throws Exception;

	String getAllWorkerList(ChartVo chartVo) throws Exception;

	String getPrdNoList(ChartVo chartVo) throws Exception;

	String dvcList() throws Exception;

	String getPrdNoListByRtng(ChartVo chartVo) throws Exception;

	String getDeviceCnt(ChartVo chartVo) throws Exception;
	
	String getCommonTyList(ChartVo chartVo) throws Exception;


}
