package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;

public interface OrderService {

	String getPrdData(ChartVo chartVo) throws Exception;

	String getPrdNoList(ChartVo chartVo) throws Exception;

	String getWorkerPerfomance(ChartVo chartVo) throws Exception;

	String getLatestDate() throws Exception;

	String getTargetData(ChartVo chartVo) throws Exception;

	String getTargetData1(ChartVo chartVo) throws Exception;

	String getTargetData2(ChartVo chartVo) throws Exception;

	String addTargetCnt(String val,String addval,String capaval) throws Exception;

	String waitTimeList(ChartVo chartVo) throws Exception;

	String getDvcIdByPrdNo(ChartVo chartVo) throws Exception;

	String dvcWorkerlist(int empCd) throws Exception;
	
	String saveWorkerList(String val) throws Exception;
	
	String delWorkerList(String idx) throws Exception;
	
	String dvcToWorkerList(ChartVo chartVo) throws Exception;

	String test(String val) throws Exception;
	
	String workerDvclist(int dvcId) throws Exception;

	String checkIdPw(ChartVo chartVo) throws Exception;

	String checkPartCnt(ChartVo chartVo) throws Exception;

	String getPartCnt(ChartVo chartVo) throws Exception;

	String insertTgndExtra(String val) throws Exception;

	String deleteJobSplit(ChartVo chartVo) throws Exception;

	String getGoEndHistoryList(ChartVo chartVo) throws Exception;
	
	String getAddTargetDetail(ChartVo chartVo) throws Exception;
	
	String saveAddTargetDetail(String val) throws Exception;


}
