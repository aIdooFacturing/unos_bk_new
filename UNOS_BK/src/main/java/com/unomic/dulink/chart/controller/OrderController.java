package com.unomic.dulink.chart.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.*;

@RequestMapping(value = "/order")
@Controller
public class OrderController {

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "addPrdCmplHistory")
	public String addPrdCmplHistory() {
		return "order/addPrdCmplHistory";
	};
	
	@RequestMapping(value = "getAttendanceList")
	public String getAttendanceList() {
		return "order/getAttendanceList";
	};

	@RequestMapping(value = "addTarget")
	public String addTarget() {
		return "order/addTarget";
	};

	@RequestMapping(value = "addTargetPlan")
	public String addTargetPlan() {
		return "order/addTargetPlan";
	};
	
	@RequestMapping(value = "workingReportTotalMenu")
	public String workingReportTotalMenu() {
		return "order/workingReportTotalMenu";
	};
	
	@RequestMapping(value = "getPartCyl")
	public String getPartCyl() {
		return "order/getPartCyl";
	};
	
	@RequestMapping(value = "getGoWorkList")
	public String getGoWorkList() {
		return "order/getGoWorkList";
	};

	@RequestMapping(value = "addNoOperationHistory")
	public String addNoOperationHistory(ChartVo chartVo) {
		return "order/addNoOperationHistory";
	};

	@RequestMapping(value = "addTargetDetail")
	public String addTargetDetail(ChartVo chartVo) {
		return "order/addTargetDetail";
	};

	@RequestMapping(value = "getPrdData")
	@ResponseBody
	public String getPrdData(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = orderService.getPrdData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getWorkerWatingTimeView")
	public String getWorkerWatingTimeView() {
		return "order/getWorkerWatingTimeView";
	}

	@RequestMapping(value = "getWorkerPerfomance")
	@ResponseBody
	public String getWorkerPerfomance(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.getWorkerPerfomance(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getLatestDate")
	@ResponseBody
	public String getLatestDate() {
		String str = "";
		try {
			str = orderService.getLatestDate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getWorkerPerformance")
	public String getWorkerPerformance() {

		return "order/getWorkerPerformance";
	}

	@RequestMapping(value = "getTargetData")
	@ResponseBody
	public String getTargetData(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.getTargetData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	// �ְ� �̸�����
	@RequestMapping(value = "getTargetData1")
	@ResponseBody
	public String getTargetData1(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.getTargetData1(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	// �߰� �̸�����
	@RequestMapping(value = "getTargetData2")
	@ResponseBody
	public String getTargetData2(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.getTargetData2(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};

	@RequestMapping(value = "addTargetCnt")
	@ResponseBody
	public String addTargetCnt(String val,String addval,String capaval) {
		String str = "";
		try {
			str = orderService.addTargetCnt(val,addval,capaval);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "insertTgndExtra")
	@ResponseBody
	public String insertTgndExtra(String val) {
		String str = "";
		try {
			str = orderService.insertTgndExtra(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};


	@RequestMapping(value = "waitTimeList")
	@ResponseBody
	public String waitTimeList(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.waitTimeList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getDvcIdByPrdNo")
	@ResponseBody
	public String getDvcIdByPrdNo(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.getDvcIdByPrdNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value= "registerWorker")
	public String registerWorker() {
		return "order/registerWorker";
	}
	
	@RequestMapping(value="dvcWorkerlist")
	@ResponseBody
	public String dvcWorkerlist(int empCd) {
		String str="";
		try {
			str = orderService.dvcWorkerlist(empCd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("strstrstr::"+str);
		return str;
	}

	@RequestMapping(value="saveWorkerList")
	@ResponseBody
	public String saveWorkerList(String val) {
		String str="";
		try {
			str = orderService.saveWorkerList(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="delWorkerList")
	@ResponseBody
	public String delWorkerList(String idx) {
		String str="";
		try {
			str = orderService.delWorkerList(idx);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="dvcToWorkerList")
	@ResponseBody
	public String dvcToWorkerList(ChartVo chartVo) {
		String str = "";
		try {
			str = orderService.dvcToWorkerList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value="test")
	@ResponseBody
	public String test(String val) {
		String str = "";
		try {
			str = orderService.test(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="workerDvclist")
	@ResponseBody
	public String workerDvclist(int dvcId) {
		String str="";
		try {
			str = orderService.workerDvclist(dvcId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="checkIdPw")
	@ResponseBody
	public String checkIdPw(ChartVo chartVo) {
		String str="";
		try {
			str = orderService.checkIdPw(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="checkPartCnt")
	@ResponseBody
	public String checkPartCnt(ChartVo chartVo) {
		String str="";
		try {
			str = orderService.checkPartCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getPartCnt")
	@ResponseBody
	public String getPartCnt(ChartVo chartVo) {
		String str="";
		try {
			str = orderService.getPartCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="deleteJobSplit")
	@ResponseBody
	public String deleteJobSplit(ChartVo chartVo) {
		String str="";
		try {
			str = orderService.deleteJobSplit(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value="getGoEndHistoryList")
	@ResponseBody
	public String getGoEndHistoryList(ChartVo chartVo) {
		String str="";
		try {
			str = orderService.getGoEndHistoryList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getAddTargetDetail")
	@ResponseBody
	public String getAddTargetDetail(ChartVo chartVo) {
		String str="";
		try {
			str = orderService.getAddTargetDetail(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="saveAddTargetDetail")
	@ResponseBody
	public String saveAddTargetDetail(String val) {
		String str="";
		try {
			str = orderService.saveAddTargetDetail(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

}
