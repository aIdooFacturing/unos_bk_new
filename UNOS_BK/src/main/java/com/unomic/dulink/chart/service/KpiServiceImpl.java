package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.chart.domain.AverageVo;
import com.unomic.dulink.chart.domain.ChartVo;


@Service
@Repository
public class KpiServiceImpl implements KpiService {
	
	private final static String namespace = "com.unomic.dulink.kpi.";

	private static final Logger logger = LoggerFactory.getLogger(KpiServiceImpl.class);

	@Inject
	SqlSession sqlSessionTemplate;

	@Override
	public String getWorkerCnt(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = new ArrayList<ChartVo>();

		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getWorkerCnt", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("goalRatio", dataList.get(i).getGoalRatio());
			map.put("rank", dataList.get(i).getRank());
			map.put("countDvc", dataList.get(i).getCountDvc());

			list.add(map);
		};

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getPrdNoCnt(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = new ArrayList<ChartVo>();

		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getPrdNoCnt", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("okRatio", dataList.get(i).getOkRatio());
			map.put("goalRatio", dataList.get(i).getGoalRatio());
			map.put("rank", dataList.get(i).getRank());
			map.put("average", dataList.get(i).getAverage());
			map.put("countDvc", dataList.get(i).getCountDvc());

			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getDeviceCnt(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = new ArrayList<ChartVo>();

		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getDeviceCnt", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));		
//			map.put("name", dataList.get(i).getName());
			map.put("target", dataList.get(i).getTarget());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("goalRatio", dataList.get(i).getGoalRatio());
			map.put("rank", dataList.get(i).getRank());
			
			map.put("chk", dataList.get(i).getChkTy());
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Transactional
	@Override
	public String getDetailProductStatus(ChartVo chartVo,HttpServletResponse response) throws Exception {
		
		System.out.println("상세보기 스타트");
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		System.out.println("상세보기 스타트1");
 
		try {
			System.out.println("상세보기 스타트2");
			dataList = sqlSessionTemplate.selectList(namespace + "getDetailProductStatus", chartVo);
			System.out.println("상세보기 스타트3");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	/*	List<ChartVo> rankList = sqlSessionTemplate.selectList(namespace + "getDetailRank", chartVo);
		List<ChartVo> dvcList = sqlSessionTemplate.selectList(namespace + "getWorkerDvcId", chartVo);
		List<ChartVo> prdNoList = sqlSessionTemplate.selectList(namespace + "getWorkerPrdNoCnt", chartVo);
		
		int rawSize = dataList.size();
		int rankSize = rankList.size();
		int dvcSize = dvcList.size();
		int prdNoSize = prdNoList.size();
		
		int lastRank=1;
		for(int i=0;i<rawSize;i++) {
			for(int j=0;j<rankSize;j++) {
				if(dataList.get(i).getEmpCd().equals(rankList.get(j).getEmpCd())) {
					if(lastRank<Integer.parseInt(rankList.get(j).getRank())) {
						lastRank = Integer.parseInt(rankList.get(j).getRank());
					}
					dataList.get(i).setRank(rankList.get(j).getRank());
					dataList.get(i).setAverage(rankList.get(j).getAverage());
				}
			}
		}
		int lastRankWorkerCnt = 0;
		int empCd=0;
		
		System.out.println("dataList ::"+dataList);
		System.out.println("rawSize ::"+rawSize);
		System.out.println("dvcList ::"+dvcList);
		System.out.println("dvcSize ::"+dvcSize);
		System.out.println("rankList ::"+rankList);
		System.out.println("rankSize ::"+rankSize);
		System.out.println("prdNoList ::"+prdNoList);
		System.out.println("prdNoSize ::"+prdNoSize);
		
		for(int i=0;i<rawSize;i++) {
			if(Integer.parseInt(dataList.get(i).getRank())==lastRank) {
				if(empCd==0) {
					empCd=Integer.parseInt(dataList.get(i).getEmpCd());
					lastRankWorkerCnt++;
				}else{
					if(empCd==Integer.parseInt(dataList.get(i).getEmpCd())) {
						
					}else{
						empCd=0;
					}
				}
			}
		}
		
		int dvcCnt=0;
		empCd=0;
		for(int i=0;i<rawSize;i++) {
			for(int j=0;j<dvcSize;j++) {
				if(dataList.get(i).getEmpCd().equals(dvcList.get(j).getEmpCd())) {
					if(Integer.parseInt(dataList.get(i).getRank())==lastRank) {
						if(empCd==0) {
							empCd=Integer.parseInt(dataList.get(i).getEmpCd());
							if(dvcCnt!=0) {
								
							}else {
								dvcCnt=dvcList.get(j).getDvcCnt();
							}
						}else{
							if(empCd==Integer.parseInt(dataList.get(i).getEmpCd())) {
								
							}else{
								dvcCnt=dvcList.get(j).getDvcCnt()+dvcCnt;
								empCd=0;
							}
						}
					}else {
						dataList.get(i).setDvcCnt(dvcList.get(j).getDvcCnt());
					}
				}
			}
		}
		
		for(int i=0;i<rawSize;i++) {
			for(int j=0;j<prdNoSize;j++) {
				if(dataList.get(i).getEmpCd().equals(prdNoList.get(j).getEmpCd())) {
					dataList.get(i).setPrdNoCnt(prdNoList.get(j).getPrdNoCnt());
				}
			}
		}
		
		for(int i=0;i<rawSize;i++) {
			if(Integer.parseInt(dataList.get(i).getRank())==lastRank) {
				dataList.get(i).setCntWorker(lastRankWorkerCnt-1);
				dataList.get(i).setDvcCnt(dvcCnt);
			}
		}*/

		List list = new ArrayList();
		
		int rank=0;
		int idx=0;
		int cntWorker=0;
		for (int i = 0; i < dataList.size(); i++) {
			System.out.println("장비 :" + dataList.get(i).getName() +" 목표수량 : " + dataList.get(i).getTarget()  +" 목표수량1 : " + dataList.get(i).getTgCyl() + " 실적 : " + dataList.get(i).getCnt() + " 평균 : " + dataList.get(i).getAverage());

			Map map = new HashMap(); 
			 
			map.put("prdNo", dataList.get(i).getPrdNo()); 
			map.put("oprNm", dataList.get(i).getOprNm()); 
			map.put("name", dataList.get(i).getName());
			map.put("worker", URLEncoder.encode(dataList.get(i).getWorker(), "utf-8"));
			map.put("target", dataList.get(i).getTarget());
//			map.put("cnt", dataList.get(i).getCnt());
			map.put("cnt", dataList.get(i).getTgCyl());
			map.put("goalRatio", dataList.get(i).getGoalRatio());
			map.put("faultCnt", dataList.get(i).getFaultCnt());
			map.put("okRatio", dataList.get(i).getOkRatio());
			map.put("average", dataList.get(i).getAverage()); 
			if(i==0) { 
				rank=rank+1;
				map.put("rank", rank);
				
				idx=idx+1;
				map.put("idx", idx);
			}else {
				if(dataList.get(i).getAverage()==dataList.get(i-1).getAverage()) {
					map.put("rank", rank);
				}else {
					rank=rank+1;
					map.put("rank", rank);
				}
				if(dataList.get(i).getWorker().equals(dataList.get(i-1).getWorker())) {
					map.put("idx", idx);
				}else {
					/*System.out.println("idx 비교");
					System.out.println("전 작업자 : " + dataList.get(i-1).getWorker());
					System.out.println("현작업자 : " + dataList.get(i).getWorker());*/
					idx=idx+1;
					map.put("idx", idx);
				}
				/*if(dataList.get(i).getWorker()!=dataList.get(i-1).getWorker()) {
					
				}*/
			}
			if(i==0) {
				cntWorker = cntWorker+1;
				map.put("cntWorker", cntWorker);
				map.put("lastRank", cntWorker);

			}else {
				if(dataList.get(i).getAverage()==dataList.get(i-1).getAverage()) {
					if(dataList.get(i).getWorker().equals(dataList.get(i-1).getWorker())){
						map.put("cntWorker", cntWorker);
					}else {
//						System.out.println("비교 ::"+ dataList.get(i).getWorker() + " , " + dataList.get(i-1).getWorker());
						cntWorker = cntWorker +1;
						map.put("cntWorker", cntWorker);
//						System.out.println("숫자 :"+cntWorker);
					}
					map.put("lastRank", cntWorker);
				}else {
					map.put("cntWorker", cntWorker);
					map.put("lastRank", cntWorker);
					cntWorker=1;
				}
			}
//			map.put("cntWorker", dataList.get(i).getCntWorker());
			map.put("dvcCnt", dataList.get(i).getDvcCnt());
			map.put("prdNoCnt", dataList.get(i).getPrdNoCnt());
			list.add(map);
		}
		;

		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getoperationStatus(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub

		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getoperationStatus", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("rank", dataList.get(i).getRank());
			map.put("name", dataList.get(i).getName());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("optime", dataList.get(i).getOpTime());
			map.put("cuttingRatio", dataList.get(i).getCuttingRatio());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("alarmTime", dataList.get(i).getAlarmTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("work_date", dataList.get(i).getWorkDate());
			map.put("targetRuntime", dataList.get(i).getTargetRuntime());
			list.add(map);
			
			System.out.println("dvcId :: " + dataList.get(i).getDvcId() + " , CUtime :: " + dataList.get(i).getCuttingTime() + " , tgTime :: " + dataList.get(i).getTargetRuntime());
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}

	@Override
	public String getStandard(ChartVo chartVo) throws Exception {

		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getStandard", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("workerStandard", dataList.get(i).getWorkerStandard());
			map.put("deviceStandard", dataList.get(i).getDeviceStandard());
			map.put("workDate", dataList.get(i).getWorkDate());
			map.put("workIdx", dataList.get(i).getWorkIdx());
			
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}

	@Override
	public String updateStandard(ChartVo chartVo) throws Exception {
		String str="";
		try {
			sqlSessionTemplate.update(namespace+"updateStandard", chartVo);
			str="success";
		}catch(Exception e) {
			str="fail";
		}
		
		
		return str;
	}

	@Override
	public String getoperationStatusNd(ChartVo chartVo) throws Exception {

		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getoperationStatusNd", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("rank", dataList.get(i).getRank());
			map.put("name", dataList.get(i).getName());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("optime", dataList.get(i).getOpTime());
			map.put("cuttingRatio", dataList.get(i).getCuttingRatio());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("alarmTime", dataList.get(i).getAlarmTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("work_date", dataList.get(i).getWorkDate());
			map.put("targetRuntime", dataList.get(i).getTargetRuntime());
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}

	@Override
	public String getoperationStatusNight(ChartVo chartVo) throws Exception {
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -1);
		
		String sDate = dayTime.format(cal.getTime());
		
		List<ChartVo> dataListDay = new ArrayList<ChartVo>();
		List<ChartVo> dataListLatest = new ArrayList<ChartVo>();
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		
		try {
			dataListDay = sqlSessionTemplate.selectList(namespace + "selectOperationStatusDay", sDate);
			dataListLatest = sqlSessionTemplate.selectList(namespace + "selectOperationStatus", sDate);
			
			int sizeLatest = dataListLatest.size();
			int sizeDay = dataListDay.size();
			
			for(int i=0;i<sizeLatest;i++) {
				
				for(int j=0;j<sizeDay;j++) {
					ChartVo chartVo1 = new ChartVo();
					if(dataListLatest.get(i).getDvcId().equals(dataListDay.get(j).getDvcId())) {
						chartVo1.setWorkIdx(1); 
						chartVo1.setWorkDate(sDate);
						chartVo1.setDvcId(dataListLatest.get(i).getDvcId());
						chartVo1.setTgRunTime(String.valueOf(Integer.parseInt(dataListLatest.get(i).getTgRunTime())/2));
						chartVo1.setInCycleTime(dataListLatest.get(i).getInCycleTime() - dataListDay.get(j).getInCycleTime());
						chartVo1.setWaitTime(dataListLatest.get(i).getWaitTime() - dataListDay.get(j).getWaitTime());
						chartVo1.setAlarmTime(dataListLatest.get(i).getAlarmTime() - dataListDay.get(j).getAlarmTime());
						chartVo1.setNoConnectionTime(dataListLatest.get(i).getNoConnectionTime() - dataListDay.get(j).getNoConnectionTime());
						chartVo1.setOpTime(String.valueOf(Integer.parseInt(dataListLatest.get(i).getOpTime()) - Integer.parseInt(dataListDay.get(j).getOpTime())));
						chartVo1.setAvgCyleTime(dataListLatest.get(i).getAvgCyleTime() - dataListDay.get(j).getAvgCyleTime());
						chartVo1.setCuttingTime(dataListLatest.get(i).getCuttingTime() - dataListDay.get(j).getCuttingTime());
						dataList.add(chartVo1);
					}else {
						
					}
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("rank", dataList.get(i).getRank());
			map.put("name", dataList.get(i).getName());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("optime", dataList.get(i).getOpTime());
			map.put("cuttingRatio", dataList.get(i).getCuttingRatio());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("alarmTime", dataList.get(i).getAlarmTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("work_date", dataList.get(i).getWorkDate());
			map.put("targetRuntime", dataList.get(i).getTargetRuntime());
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}

	@Override
	public String getoperationStatusToNight(ChartVo chartVo) throws Exception {
		
		List<ChartVo> dataListDay = new ArrayList<ChartVo>();
		List<ChartVo> dataListLatest = new ArrayList<ChartVo>();
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		
		String sDate = chartVo.getSDate();
		
		try {
			dataListDay = sqlSessionTemplate.selectList(namespace + "selectOperationStatusDay", sDate);
			dataListLatest = sqlSessionTemplate.selectList(namespace + "getoperationStatus", sDate);
			
			int sizeLatest = dataListLatest.size();
			int sizeDay = dataListDay.size();
			
			for(int i=0;i<sizeLatest;i++) {
				for(int j=0;j<sizeDay;j++) {
					ChartVo chartVo1 = new ChartVo();
					if(dataListLatest.get(i).getDvcId().equals(dataListDay.get(j).getDvcId())) {
						chartVo1.setRank(String.valueOf(i+1));
						chartVo1.setWorkIdx(1); 
						chartVo1.setWorkDate(sDate);
						chartVo1.setName(dataListLatest.get(i).getName());
						chartVo1.setDvcId(dataListLatest.get(i).getDvcId());
						chartVo1.setTargetRuntime(dataListLatest.get(i).getTargetRuntime());
						chartVo1.setInCycleTime(dataListLatest.get(i).getInCycleTime() - dataListDay.get(j).getInCycleTime());
						chartVo1.setWaitTime(dataListLatest.get(i).getWaitTime() - dataListDay.get(j).getWaitTime());
						chartVo1.setAlarmTime(dataListLatest.get(i).getAlarmTime() - dataListDay.get(j).getAlarmTime());
						chartVo1.setNoConnectionTime(dataListLatest.get(i).getNoConnectionTime() - dataListDay.get(j).getNoConnectionTime());
						chartVo1.setOpTime(String.valueOf(Integer.parseInt(dataListLatest.get(i).getOpTime()) - Integer.parseInt(dataListDay.get(j).getOpTime())));
						chartVo1.setAvgCyleTime(dataListLatest.get(i).getAvgCyleTime() - dataListDay.get(j).getAvgCyleTime());
						chartVo1.setCuttingTime(dataListLatest.get(i).getCuttingTime() - dataListDay.get(j).getCuttingTime());
						dataList.add(chartVo1);
					}else {
						
					}
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("rank", dataList.get(i).getRank());
			map.put("name", dataList.get(i).getName());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("optime", dataList.get(i).getOpTime());
			map.put("cuttingRatio", dataList.get(i).getCuttingRatio());
			map.put("cuttingTime", dataList.get(i).getCuttingTime());
			map.put("inCycleTime", dataList.get(i).getInCycleTime());
			map.put("waitTime", dataList.get(i).getWaitTime());
			map.put("alarmTime", dataList.get(i).getAlarmTime());
			map.put("noConnectionTime", dataList.get(i).getNoConnectionTime());
			map.put("work_date", dataList.get(i).getWorkDate());
			map.put("targetRuntime", dataList.get(i).getTargetRuntime());
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}

	@Override
	public String getopearationStatus(ChartVo chartVo) throws Exception{
		
		// TODO Auto-generated method stub
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getopearationStatus", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("workerStandard", dataList.get(i).getWorkerStandard());
			map.put("deviceStandard", dataList.get(i).getDeviceStandard());
			map.put("workDate", dataList.get(i).getWorkDate());
			map.put("workIdx", dataList.get(i).getWorkIdx());
			
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		
		return str;
	}

	@Override
	public String updateOpStandard(ChartVo chartVo) throws Exception {
		String str="";
		try {
			sqlSessionTemplate.update(namespace+"updateOpStandard", chartVo);
			str="success";
		}catch(Exception e) {
			str="fail";
		}
		
		
		return str;
	}
	
	@Override
	public String getTotalRatio(ChartVo chartVo) throws Exception {
		
		//금일 일평균
		List<ChartVo> getTodayRatio = new ArrayList<ChartVo>();
		//어제 일평균
		List<ChartVo> getYesterdayRatio = new ArrayList<ChartVo>();
		//일 평균
		List<ChartVo> getDayRatio = new ArrayList<ChartVo>();
		//금월 평균
		List<ChartVo> getNowMonthRatio = new ArrayList<ChartVo>();
		//전월 평균
		List<ChartVo> getBeforeMonthRatio = new ArrayList<ChartVo>();
		//10 개월평균
		List<ChartVo> getMonthRatio = new ArrayList<ChartVo>();
		
		
		try {
			
			logger.info(chartVo.getSDate());
			logger.info(chartVo.getDate());
			logger.info(chartVo.getMonthDate());
			
			
			getTodayRatio = sqlSessionTemplate.selectList(namespace + "getTodayRatio", chartVo);//오늘날짜 달성율

			//어제저번달 일 평균 달성율 가동율
			getYesterdayRatio = sqlSessionTemplate.selectList(namespace + "getYesterdayRatio", chartVo);//어제 날짜 달성율
			
			//일 평균
			getDayRatio = sqlSessionTemplate.selectList(namespace + "getDayRatio", chartVo);//한달 달성율
			
			//금월 평균
			getNowMonthRatio = sqlSessionTemplate.selectList(namespace + "getNowMonthRatio", chartVo);//한달 달성율

			//전월 평균
			getBeforeMonthRatio = sqlSessionTemplate.selectList(namespace + "getBeforeMonthRatio", chartVo);//한달 달성율
						
			//10 개월평균
			getMonthRatio = sqlSessionTemplate.selectList(namespace + "getMonthRatio", chartVo);//한달 달성율
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		List list1 = new ArrayList();
		List list2 = new ArrayList();
		
		//금일
		for (int i = 0; i < getTodayRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map1.put("rank", 1);
			map1.put("tgDate", getTodayRatio.get(i).getTgDate());
			map1.put("goalRatio", getTodayRatio.get(i).getGoalRatio());
			map1.put("operatingRate", getTodayRatio.get(i).getOperatingRate());
			map1.put("faultCnt", getTodayRatio.get(i).getFaultCnt());//불량수량
			map1.put("faultCntRatio", getTodayRatio.get(i).getFaultCntRatio());//불량율
			map1.put("cnt",getTodayRatio.get(i).getCnt());
			map1.put("opTime", getTodayRatio.get(i).getOpTime());
			map1.put("dataTime", getTodayRatio.get(i).getTime());
			map1.put("progress", getTodayRatio.get(i).getTargetRatio());
			map1.put("timeCheck", getTodayRatio.get(i).getTimeCHECK());
			
			map1.put("targetCnt",getTodayRatio.get(i).getTargetCnt());
			map1.put("targetTime", getTodayRatio.get(i).getTargetTime());
			
			map1.put("cnt1", getTodayRatio.get(i).getCnt1());
			map1.put("cnt2", getTodayRatio.get(i).getCnt2());

			map2.put("rank", 1);
			map2.put("group", "day");
			map2.put("tgDate", getTodayRatio.get(i).getTgDate());
			map2.put("cnt", getTodayRatio.get(i).getCnt());
			map2.put("opTime", getTodayRatio.get(i).getOpTime());
			map2.put("faultCnt", getTodayRatio.get(i).getFaultCnt());
			
			list1.add(map1);
			list2.add(map2);
		
		};
		
		//어제
		for (int i = 0; i < getYesterdayRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map1.put("rank", 2);
			map1.put("tgDate", getYesterdayRatio.get(i).getTgDate());
			map1.put("goalRatio", getYesterdayRatio.get(i).getGoalRatio());
			map1.put("operatingRate", getYesterdayRatio.get(i).getOperatingRate());
			map1.put("faultCnt", getYesterdayRatio.get(i).getFaultCnt());

			map2.put("rank", 2);
			map2.put("group", "day");
			map2.put("tgDate", getYesterdayRatio.get(i).getTgDate());
			map2.put("cnt", getYesterdayRatio.get(i).getCnt());
			map2.put("opTime", getYesterdayRatio.get(i).getOpTime());
			map2.put("faultCnt", getYesterdayRatio.get(i).getFaultCnt());
			
			list1.add(map1);
			list2.add(map2);
		};
		
		//일평균
		for (int i = 0; i < getDayRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map2.put("rank", 3);
			map2.put("group", "day");
			map2.put("tgDate", getDayRatio.get(i).getTgDate());
			map2.put("cnt", getDayRatio.get(i).getCnt());
			map2.put("opTime", getDayRatio.get(i).getOpTime());
			map2.put("faultCnt", getDayRatio.get(i).getFaultCnt());
			
			list2.add(map2);
			
		};
		
		//금월 평균
		for (int i = 0; i < getNowMonthRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
//			System.out.println("for 문 ( "+i+" ): " +  getNowMonthRatio.get(i).getTgDate());
			
			map2.put("rank", 4);
			map2.put("group", "mon");
			map2.put("tgDate", getNowMonthRatio.get(i).getTgDate());
			map2.put("cnt", getNowMonthRatio.get(i).getCnt());
			map2.put("opTime", getNowMonthRatio.get(i).getOpTime());
			map2.put("faultCnt", getNowMonthRatio.get(i).getFaultCnt());
			
			list2.add(map2);
		};
				
		//전월 평균
		for (int i = 0; i < getBeforeMonthRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map2.put("rank", 5);
			map2.put("group", "mon");
			map2.put("tgDate", getBeforeMonthRatio.get(i).getTgDate());
			map2.put("cnt", getBeforeMonthRatio.get(i).getCnt());
			map2.put("opTime", getBeforeMonthRatio.get(i).getOpTime());
			map2.put("faultCnt", getBeforeMonthRatio.get(i).getFaultCnt());
			
			list2.add(map2);
		};
		
		//10개월 평균
		for (int i = 0; i < getMonthRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map2.put("rank", 6);
			map2.put("group", "mon");
			map2.put("tgDate", getMonthRatio.get(i).getTgDate());
			map2.put("cnt", getMonthRatio.get(i).getCnt());
			map2.put("opTime", getMonthRatio.get(i).getOpTime());
			map2.put("faultCnt", getMonthRatio.get(i).getFaultCnt());
			
			list2.add(map2);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList1", list1);
		listMap.put("dataList2", list2);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}

	@Override
	public String getprodataTime(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str = (String) sqlSessionTemplate.selectOne(namespace + "getprodataTime", chartVo);
		System.out.println("데이터수신시간 :" +str);
		return str;
	}

	@Override
	public String getOpdataTime(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str = (String) sqlSessionTemplate.selectOne(namespace + "getOpdataTime", chartVo);
		System.out.println("데이터수신시간 :" +str);
		return str;
	}
	
	@Override
	public String viewByWorker(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub

		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "viewByWorker", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {

			Map map = new HashMap();
			
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", dataList.get(i).getName());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("tgCyl", dataList.get(i).getTgCyl());
			map.put("cntCyl", dataList.get(i).getCntCyl());
			
			list.add(map);
		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
		
		
	}
	
	@Override
	public String viewByDvc(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "viewByDvc", chartVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		List list = new ArrayList();
		
		for (int i = 0; i < dataList.size(); i++) {
			
			 Map map = new HashMap();
	            map.put("dvcId", ((ChartVo)dataList.get(i)).getDvcId());
	            map.put("prdNo", ((ChartVo)dataList.get(i)).getPrdNo());
	            map.put("oprNo", ((ChartVo)dataList.get(i)).getOprNo());
	            map.put("workIdx", Integer.valueOf(((ChartVo)dataList.get(i)).getWorkIdx()));
	            map.put("tgCyl", ((ChartVo)dataList.get(i)).getTgCyl());
	            map.put("cnt", ((ChartVo)dataList.get(i)).getCnt());
	            map.put("goalRatio", ((ChartVo)dataList.get(i)).getGoalRatio());
	            map.put("dvcName", URLEncoder.encode(((ChartVo)dataList.get(i)).getDvcName(), "utf-8"));
	            map.put("prdNo", ((ChartVo)dataList.get(i)).getPrdNo());
	            map.put("name", URLEncoder.encode(((ChartVo)dataList.get(i)).getName(), "utf-8"));
	            list.add(map);

		}
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
		
		
	}

	@Override
	public String getAnalysisKpi(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str = "";
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getAnalysisKpi", chartVo);
			
			List list = new ArrayList();
			
//			System.out.println("== 생산 실적 분석 ==");
//			System.out.println("받아온 값 ==> sDate: " + chartVo.getSDate() + " , eDate: " + chartVo.getEDate());
//			System.out.println("== 결과 ==");
//			System.out.println(dataList);
//			logger.info("test");
			for (int i = 0; i < dataList.size(); i++) {
				
				Map map = new HashMap();
				map.put("prdNo", ((ChartVo)dataList.get(i)).getPrdNo());
	            map.put("item", ((ChartVo)dataList.get(i)).getItem());
	            
	            map.put("workIdx", Integer.valueOf(((ChartVo)dataList.get(i)).getWorkIdx()));

	            map.put("tgCylR", ((ChartVo)dataList.get(i)).getTgCylR());
	            map.put("cntR", ((ChartVo)dataList.get(i)).getCntR());
	            map.put("ratioR", ((ChartVo)dataList.get(i)).getRatioR());

	            map.put("tgCylM", ((ChartVo)dataList.get(i)).getTgCylM());
	            map.put("cntM", ((ChartVo)dataList.get(i)).getCntM());
	            map.put("ratioM", ((ChartVo)dataList.get(i)).getRatioM());
	            
	            map.put("tgCylC", ((ChartVo)dataList.get(i)).getTgCylC());
	            map.put("cntC", ((ChartVo)dataList.get(i)).getCntC());
	            map.put("ratioC", ((ChartVo)dataList.get(i)).getRatioC());
	            
	            map.put("tgCylS", ((ChartVo)dataList.get(i)).getTgCylS());
	            map.put("cntS", ((ChartVo)dataList.get(i)).getCntS());
	            map.put("ratioS", ((ChartVo)dataList.get(i)).getRatioS());

//	            map.put("time", ((ChartVo)dataList.get(i)).getRatioS());
//	            map.put("reason", ((ChartVo)dataList.get(i)).getRatioS());
//	            map.put("ty", ((ChartVo)dataList.get(i)).getRatioS());
//	            map.put("dvcId", ((ChartVo)dataList.get(i)).getRatioS());
//	            map.put("dvcId", ((ChartVo)dataList.get(i)).getRatioS());
	            
	            
//	            map.put("dvcId", ((ChartVo)dataList.get(i)).getDvcId());
//	            map.put("oprNo", ((ChartVo)dataList.get(i)).getOprNo());
//	            map.put("dvcName", URLEncoder.encode(((ChartVo)dataList.get(i)).getDvcName(), "utf-8"));
//	            map.put("prdNo", ((ChartVo)dataList.get(i)).getPrdNo());
//	            map.put("name", URLEncoder.encode(((ChartVo)dataList.get(i)).getName(), "utf-8"));
	            list.add(map);

			}
			
			Map listMap = new HashMap();
			listMap.put("dataList", list);
			
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
		}
		
		return str;
	}

	@Override
	public String getFpList(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str = "";
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "getFpList", chartVo);
			
			List list = new ArrayList();
			
			for (int i = 0; i < dataList.size(); i++) {
				
				Map map = new HashMap();
				map.put("ty", dataList.get(i).getTy());
	            map.put("name", dataList.get(i).getName());
	            map.put("time", dataList.get(i).getTime());
	            map.put("startTime", dataList.get(i).getStartDateTime());
	            map.put("endTime", dataList.get(i).getEndDateTime());
//	            map.put("nm", dataList.get(i).getNm());
	            map.put("nm", URLEncoder.encode(dataList.get(i).getNm(),"utf-8"));
	            map.put("cause", URLEncoder.encode(dataList.get(i).getCause(),"utf-8"));
	            map.put("result", URLEncoder.encode(dataList.get(i).getResult(),"utf-8"));
	
//	            map.put("dvcName", URLEncoder.encode(((ChartVo)dataList.get(i)).getDvcName(), "utf-8"));
	            list.add(map);
	
			}
			
			Map listMap = new HashMap();
			listMap.put("dataList", list);
			
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
	
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
		}
		return str;
	}
	
	@Override
	public String FpQuartCnt(ChartVo chartVo) throws Exception {
		// TODO Auto-generated method stub
		String str = "";
		List<ChartVo> dataList = new ArrayList<ChartVo>();
		try {
			dataList = sqlSessionTemplate.selectList(namespace + "FpQuartCnt", chartVo);
			
			List list = new ArrayList();
			
			for (int i = 0; i < dataList.size(); i++) {
				
				Map map = new HashMap();
				map.put("category", ((ChartVo)dataList.get(i)).getCategory());
				map.put("value", ((ChartVo)dataList.get(i)).getValue());
				map.put("year", ((ChartVo)dataList.get(i)).getDate());
				
				list.add(map);
			}
			
			Map listMap = new HashMap();
			listMap.put("dataList", list);
			
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
			
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
		}
		return str;
	}
	
	
	//백업파일 PRODUCTIONSTATUSKPI_BACKUP
	
	/*@Override
	public String getTotalRatio(ChartVo chartVo) throws Exception {
		
		//금일 일평균
		List<ChartVo> getTodayRatio = new ArrayList<ChartVo>();
		//어제 일평균
		List<ChartVo> getYesterdayRatio = new ArrayList<ChartVo>();
		//일 평균
		List<ChartVo> getDayRatio = new ArrayList<ChartVo>();
		//금월 평균
		List<ChartVo> getNowMonthRatio = new ArrayList<ChartVo>();
		//전월 평균
		List<ChartVo> getBeforeMonthRatio = new ArrayList<ChartVo>();
		//10 개월평균
		List<ChartVo> getMonthRatio = new ArrayList<ChartVo>();
		
		
		try {
			if(chartVo.getCheckedStandard().equals("true")) {
				//금일 일 평균
				getTodayRatio = sqlSessionTemplate.selectList(namespace + "getTodayRatio", chartVo);//오늘날짜 달성율
				System.out.println("오늘");
			}else {
				//금일 일 평균
				getTodayRatio = sqlSessionTemplate.selectList(namespace + "getTodayRatioNd", chartVo);//오늘날짜 달성율
				System.out.println("오늘 아님");
			}
			
			//어제저번달 일 평균 달성율 가동율
			getYesterdayRatio = sqlSessionTemplate.selectList(namespace + "getYesterdayRatio", chartVo);//오늘날짜 달성율
			
			//일 평균
			getDayRatio = sqlSessionTemplate.selectList(namespace + "getDayRatio", chartVo);//한달 달성율
			
			//금월 평균
			getNowMonthRatio = sqlSessionTemplate.selectList(namespace + "getNowMonthRatio", chartVo);//한달 달성율

			//전월 평균
			getBeforeMonthRatio = sqlSessionTemplate.selectList(namespace + "getBeforeMonthRatio", chartVo);//한달 달성율
						
			//10 개월평균
			getMonthRatio = sqlSessionTemplate.selectList(namespace + "getMonthRatio", chartVo);//한달 달성율
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		List list1 = new ArrayList();
		List list2 = new ArrayList();
		
		//금일
		for (int i = 0; i < getTodayRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map1.put("rank", 1);
			map1.put("tgDate", getTodayRatio.get(i).getTgDate());
			map1.put("goalRatio", getTodayRatio.get(i).getGoalRatio());
			map1.put("operatingRate", getTodayRatio.get(i).getOperatingRate());
			map1.put("faultCnt", getTodayRatio.get(i).getFaultCnt());
			
			map2.put("rank", 1);
			map2.put("group", "day");
			map2.put("tgDate", getTodayRatio.get(i).getTgDate());
			map2.put("cnt", getTodayRatio.get(i).getCnt());
			map2.put("opTime", getTodayRatio.get(i).getOpTime());
			map2.put("faultCnt", getTodayRatio.get(i).getPpm());
			
			list1.add(map1);
			list2.add(map2);
		
		};
		
		//어제
		for (int i = 0; i < getYesterdayRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map1.put("rank", 2);
			map1.put("tgDate", getYesterdayRatio.get(i).getTgDate());
			map1.put("goalRatio", getYesterdayRatio.get(i).getGoalRatio());
			map1.put("operatingRate", getYesterdayRatio.get(i).getOperatingRate());
			map1.put("faultCnt", getYesterdayRatio.get(i).getFaultCnt());

			map2.put("rank", 2);
			map2.put("group", "day");
			map2.put("tgDate", getYesterdayRatio.get(i).getTgDate());
			map2.put("cnt", getYesterdayRatio.get(i).getCnt());
			map2.put("opTime", getYesterdayRatio.get(i).getOpTime());
			map2.put("faultCnt", getYesterdayRatio.get(i).getPpm());
			
			list1.add(map1);
			list2.add(map2);
		};
		
		//일평균
		for (int i = 0; i < getDayRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map2.put("rank", 3);
			map2.put("group", "day");
			map2.put("tgDate", getDayRatio.get(i).getTgDate());
			map2.put("cnt", getDayRatio.get(i).getCnt());
			map2.put("opTime", getDayRatio.get(i).getOpTime());
			map2.put("faultCnt", getDayRatio.get(i).getPpm());
			
			list2.add(map2);
			
		};
		
		//금월 평균
		for (int i = 0; i < getNowMonthRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			System.out.println("for 문 ( "+i+" ): " +  getNowMonthRatio.get(i).getTgDate());
			
			map2.put("rank", 4);
			map2.put("group", "mon");
			map2.put("tgDate", getNowMonthRatio.get(i).getTgDate());
			map2.put("cnt", getNowMonthRatio.get(i).getCnt());
			map2.put("opTime", getNowMonthRatio.get(i).getOpTime());
			map2.put("faultCnt", getNowMonthRatio.get(i).getPpm());
			
			list2.add(map2);
		};
				
		//전월 평균
		for (int i = 0; i < getBeforeMonthRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map2.put("rank", 5);
			map2.put("group", "mon");
			map2.put("tgDate", getBeforeMonthRatio.get(i).getTgDate());
			map2.put("cnt", getBeforeMonthRatio.get(i).getCnt());
			map2.put("opTime", getBeforeMonthRatio.get(i).getOpTime());
			map2.put("faultCnt", getBeforeMonthRatio.get(i).getPpm());
			
			list2.add(map2);
		};
		
		//10개월 평균
		for (int i = 0; i < getMonthRatio.size(); i++) {
			Map map1 = new HashMap();
			Map map2 = new HashMap();
			
			map2.put("rank", 6);
			map2.put("group", "mon");
			map2.put("tgDate", getMonthRatio.get(i).getTgDate());
			map2.put("cnt", getMonthRatio.get(i).getCnt());
			map2.put("opTime", getMonthRatio.get(i).getOpTime());
			map2.put("faultCnt", getMonthRatio.get(i).getPpm());
			
			list2.add(map2);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList1", list1);
		listMap.put("dataList2", list2);

		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);

		return str;
	}*/

}

