package com.unomic.dulink.chart.service;

import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.PopVo;
@Service
@Repository
public class PopServiceImpl implements PopService {
	private final static String namespace = "com.unomic.dulink.pop.";

	private static final Logger logger = LoggerFactory.getLogger(PopServiceImpl.class);

	@Inject
	SqlSession sqlSessionTemplate;

	@Override
	public String test(PopVo popVo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getGoWorkList(PopVo popVo) throws Exception {
		

		// 출근목록 리스트 가져오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getGoWorkList", popVo);
		
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("startTime", dataList.get(i).getStartTime());
			map.put("endTime", dataList.get(i).getEndTime());
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));

			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String GoWorkSave(PopVo popVo) throws Exception {// 출근보고하기
		

		// 사원번호 있는지 체크하기
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkNumber", popVo);

		//조회되는 사원번호 없을시 리턴
		if(chkNm==0) {
			return "no";
		}
		
		//이미 출근했는지 확인하기
		int DupleChk = (int) sqlSessionTemplate.selectOne(namespace + "DupleGoWork",popVo);
		
		if(DupleChk!=0) {
			return "duple";
		}
			
		String str="";
		try {
			sqlSessionTemplate.insert(namespace + "GoWorkSave", popVo);
			str="success";
		} catch (Exception e) {
			str="fail";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
//		sqlSessionTemplate.insert(namespace + "das", popVo);
//		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "chkNumber", popVo);
		
	/*	List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("startTime", dataList.get(i).getStartTime());
			map.put("endTime", dataList.get(i).getEndTime());
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);*/
		return str;
	}
	
	@Override
	public String getOffWorkList(PopVo popVo) throws Exception {
		
		// 출근목록 리스트 가져오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getOffWorkList", popVo);
		
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("startTime", dataList.get(i).getStartTime());
			map.put("endTime", dataList.get(i).getEndTime());
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String OffWorkSave(PopVo popVo) throws Exception {// 퇴근보고하기
		
		
		// 사원번호 있는지 체크하기
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkNumber", popVo);
		
		//조회되는 사원번호 없을시 리턴
		if(chkNm==0) {
			return "no";
		}
		
		//출근처리는 했는지 확인하기
		int DupleChk = (int) sqlSessionTemplate.selectOne(namespace + "DupleOffWork",popVo);
		
		if(DupleChk==0) {
			return "duple";
		}
		
		String str="";
		try {
			sqlSessionTemplate.update(namespace + "OffWorkSave", popVo);
			str="success";
		} catch (Exception e) {
			str="fail";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
	}
	
	@Override
	public String getStartJobList(PopVo popVo) throws Exception {
		
		
		// 사원번호 있는지 체크하기
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkNumber", popVo);
		
		//조회되는 사원번호 없을시 리턴
		if(chkNm==0) {
			return "no";
		}
		
		// 작업목록 리스트 가져오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getStartJobList", popVo);
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("startWorkTime", dataList.get(i).getStartWorkTime());
			map.put("endWorkTime", dataList.get(i).getEndWorkTime());
			map.put("workIdx", dataList.get(i).getWorkIdx());
			map.put("ty", URLEncoder.encode(dataList.get(i).getTy(), "utf-8"));
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String StartJobSave(PopVo popVo) throws Exception {// 퇴근보고하기
		
		
		/*// 사원번호 있는지 체크하기
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkNumber", popVo);
		
		//조회되는 사원번호 없을시 리턴
		if(chkNm==0) {
			return "no";
		}
		
		//출근처리는 했는지 확인하기
		int DupleChk = (int) sqlSessionTemplate.selectOne(namespace + "DupleOffWork",popVo);
		
		if(DupleChk==0) {
			return "duple";
		}*/
		
		String str="";
		try {
//			sqlSessionTemplate.update(namespace + "popHistoryUpdate", popVo);
			//트렌스 이력남기기
			sqlSessionTemplate.insert(namespace + "StartJobSave", popVo);
			//재고수량 변경
			sqlSessionTemplate.insert(namespace + "updatePopProccess", popVo);
			str="success";
		} catch (Exception e) {
			str="fail";
			logger.error(e.getMessage());
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return str;
	}
	
	@Override
	public String getHistoryList(PopVo popVo) throws Exception {
		
		
		//TB_STOCK_HISTORY 바코드 있는지 체크하기
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkHistory", popVo);
		
		//조회되는 바코드 없을시 리턴
		if(chkNm==0) {
			return "no";
		}
		
		//먼저 TB_STOCK_POP_HISTORY 에 공정이동 내역이 있는지 먼저 조회
		int chkHis = (int) sqlSessionTemplate.selectOne(namespace + "chkPopHistory", popVo);
		
		//작업이 진행중인지 확인하기
		int working = (int) sqlSessionTemplate.selectOne(namespace + "chkWorking", popVo);
		if(working==0) {
			return "working";
		}
		
		List<PopVo> dataList;
		// 작업목록 리스트 가져오기
		if(chkHis==0) {
			// 0일경우 HISTORY 에서 데이터 참조
			dataList = sqlSessionTemplate.selectList(namespace + "getHistoryList", popVo);
		}else {
			// 0이 아닐경우 이미 한번 공정이동이 이루어져 POP_HISTORY TABLE 참조
			dataList = sqlSessionTemplate.selectList(namespace + "getPopHistoryList", popVo);
		}
		
		logger.info("====list chk====");
		logger.info(""+dataList);
		
		/*logger.info("====들고 오는값 확인하기====");
		logger.info(popVo.getName());
		logger.info(popVo.getNM());
		logger.info(popVo.getDvcId());
		logger.info(popVo.getTy());*/
		
		
		
		/*
		//소재 ,품번 선택 리스트 가져오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "prdNoList", popVo);
		
		
		logger.info(dataList);
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("prdNo", dataList.get(i).getBeforePrdNo());
			map.put("proj", dataList.get(i).getProj());
			map.put("beforeProj", dataList.get(i).getBeforeProj());
			map.put("beforePrdNo", dataList.get(i).getBeforePrdNo());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("stock", dataList.get(i).getCnt());
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);*/
		
		
		
		
		
		
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("idx", dataList.get(i).getIdx());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("stock", dataList.get(i).getStock());
			map.put("date", dataList.get(i).getDate());
			map.put("barcode", dataList.get(i).getBarcode());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
//			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String getEndJobList(PopVo popVo) throws Exception {
		
		
		// 사원번호 있는지 체크하기
//		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkHistory", popVo);
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkNumber", popVo);
		//조회되는 사원번호 없을시 리턴
		if(chkNm==0) {
			return "no";
		}
		
		// 작업목록 리스트 가져오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getEndJobList", popVo);
		
		// 작업시작된 리스트 없을시 리턴
		if(dataList.size()==0) {
			return "duple";
		}
		
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("idx", dataList.get(i).getIdx());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("cnt", dataList.get(i).getCnt());
			map.put("stock", dataList.get(i).getStock());
			map.put("date", dataList.get(i).getDate());
			map.put("barcode", dataList.get(i).getBarcode());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	
	@Override
	public String popLoginChk(HttpSession session,PopVo popVo) throws Exception {
		
		
		// 사원번호 있는지 체크하기
		int chkNm = (int) sqlSessionTemplate.selectOne(namespace + "chkNumber", popVo);
//		logger.info("여기는왔음??111");
		//조회되는 사원번호 없을시 리턴
		if(chkNm==0) {
//			logger.info("여기는왔음??");
			return "no";
		}
//		logger.info("여기는왔음??2222");
		// 있으면 사원번호와 이름 가지고 오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getPersonInfo", popVo);
//		logger.info(""+dataList);
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
//			logger.info("작업자 :" + URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
//			logger.info("작업자 :" + dataList.get(i).getEmpCd());
			
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("lv", dataList.get(i).getLv());
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);

			session.setAttribute("empCd",  dataList.get(i).getEmpCd());
			session.setAttribute("nm",  dataList.get(i).getNM());
			session.setAttribute("lv",  dataList.get(i).getLv());
//			session.setAttribute("nm",  URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));

		}
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	@Override
	public String getEndJobInfo(PopVo popVo) throws Exception {
		
		
		logger.info("==============hahaa============");
		logger.info(popVo.getNM());
		logger.info(popVo.getName());
		logger.info(popVo.getEmpCd());
		logger.info(popVo.getDvcId());
		logger.info(""+popVo.getIdx());
		
		// 있으면 사원번호와 이름 가지고 오기
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getEndJobInfo", popVo);
		
		logger.info(""+dataList);
		
		
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("name", dataList.get(i).getName());
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("prdNo", dataList.get(i).getPrdNo());
			map.put("item", dataList.get(i).getItem());
			map.put("oprNo", dataList.get(i).getOprNo());
			map.put("barcode", dataList.get(i).getBarcode());
			map.put("stock", dataList.get(i).getStock());
			map.put("cnt", dataList.get(i).getCnt());
//			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
			
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	@Override
	public String finshEndJob(PopVo popVo) throws Exception {
		
		
		List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getPersonInfo", popVo);
		
		List list = new ArrayList<PopVo>();
		for (int i = 0; i < dataList.size(); i++) {
			Map map = new HashMap();
			
			map.put("empCd", dataList.get(i).getEmpCd());
			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
			
			list.add(map);
			
		}
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String strTest(PopVo popVo) throws Exception {
		String str="";
		try {
			str = "success";
		} catch (Exception e) {
			str = "fail";
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return str;
	}

	@Override
	public String popGoEndHistory(PopVo popVo) throws Exception {
		String str="";
		try {
			List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "popGoEndHistory", popVo);
			
			List list = new ArrayList<PopVo>();
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				
				
//				map.put("empCd", dataList.get(i).getEmpCd());
				map.put("date", dataList.get(i).getDate());
				map.put("startTime", dataList.get(i).getStartTime());
				map.put("endTime", dataList.get(i).getEndTime());
				map.put("earlyTime", dataList.get(i).getEarlyTime());
				map.put("outTime", dataList.get(i).getOutTime());
				
				list.add(map);
				System.out.println(list);
			}
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String jsonTest(PopVo popVo) throws Exception {
		String str="";
		try {
			List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "jsonTest", popVo);
			
			List list = new ArrayList<PopVo>();
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				
//				map.put("empCd", dataList.get(i).getEmpCd());
				map.put("dvcId", dataList.get(i).getDvcId());
				map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
				
				list.add(map);
				
			}
			logger.info(popVo.getEmpCd());
			System.out.println(dataList);
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String popLineChk(PopVo popVo) throws Exception {
		String str="";
		
		try {
//			System.out.println("가져온 IP :" + popVo.getIp());
			popVo = (PopVo) sqlSessionTemplate.selectOne(namespace + "popLineChk", popVo);
//			System.out.println("성공했다");
			
			// 한글 인코딩처리.. null 이면 에러 발생할까봐 예외처리
			if(popVo.getChkCnt() >= 1) {
				popVo.setLine(URLEncoder.encode(popVo.getLine(), "utf-8"));
			}
//			logger.info(popVo.getIp());

			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(popVo);
			
		} catch (Exception e) {
			System.out.println("실패했다");
			e.printStackTrace();
			str = "fail";
			// TODO: handle exception
		}
//		System.out.println("str ::" + str);
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String getPopDevice(PopVo popVo) throws Exception {
		String str="";
		try {
			
			//popId == ALL 일떄 (일생산계획에서 불러올때) 데이터 그대로 사용하기
			//pop 에서 불러올때
			if(popVo.getPopId().equals("ALL")) {
				popVo.setDate(popVo.getDate());
			}else {
				String date = (String) sqlSessionTemplate.selectOne(namespace + "getCurrentDate", popVo);
				popVo.setDate(date);
			}
			

			List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getPopDevice", popVo);
			
			System.out.println(popVo.getDate());
			System.out.println(popVo.getPopId());
			System.out.println(dataList);
			
			List list = new ArrayList<PopVo>();
			
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				
				map.put("dvcId", dataList.get(i).getDvcId());
//				map.put("workIdx", dataList.get(i).getWorkIdx());
				map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
				map.put("date", dataList.get(i).getDate());
				map.put("tgCyl", dataList.get(i).getTgCyl());
				map.put("cntPerCyl", dataList.get(i).getCntPerCyl());
				
				map.put("empCdD", dataList.get(i).getEmpCdD());
				map.put("startTimeD", dataList.get(i).getStartTimeD());
				map.put("endTimeD", dataList.get(i).getEndTimeD());

				map.put("empCdN", dataList.get(i).getEmpCdN());
				map.put("startTimeN", dataList.get(i).getStartTimeN());
				map.put("endTimeN", dataList.get(i).getEndTimeN());

				map.put("group", dataList.get(i).getGroup());
				map.put("copy", dataList.get(i).getCopyChk());
				
				list.add(map);
				
			}
			
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String getPopChangeWorker(String val) throws Exception {
		String str="";
		try {
			
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
			JSONArray jsonArray = (JSONArray) jsonObj.get("val");
			List<PopVo> list = new ArrayList<PopVo>();
			List<PopVo> listCopy = new ArrayList<PopVo>();
			for (int i = 0; i < jsonArray.size(); i++) {
				
				JSONObject tempObj = (JSONObject) jsonArray.get(i);

				PopVo popVo = new PopVo();
				popVo.setDvcId(tempObj.get("dvcId").toString());
				popVo.setDate(tempObj.get("date").toString());
				popVo.setTgCyl(tempObj.get("tgCyl").toString());
				popVo.setCntPerCyl(Integer.parseInt(tempObj.get("cntPerCyl").toString()));

				popVo.setEmpCd(tempObj.get("empCd").toString());
				popVo.setStartTime(tempObj.get("startTime").toString());
				popVo.setEndTime(tempObj.get("endTime").toString());
				popVo.setWorkIdx(tempObj.get("workIdx").toString());
				popVo.setGroup(tempObj.get("group").toString());
				
//				arr.dvcId = data.dvcId;
//				arr.name = data.name;
//				arr.date = data.date;
//				arr.empCd = data.empCdD;
//				arr.startTime = data.startTimeD;
//				arr.endTime = data.endTimeD;
//				arr.workIdx = 2;
//				arr.copy = data.copy;
//				
				
				//기존 데이터인지 추가 데이터인지 확인하기
				//추가된 데이터일경우 => tb_dvc_tg_nd_add
				if(tempObj.get("copy").toString().equals("true") || tempObj.get("copy").toString().equals(true)) {
					listCopy.add(popVo);
				}else {// 기존 데이터일 경우 => tb_dvc_tg_nd
					list.add(popVo);
				}

			}
//			
//			System.out.println("추가 데이터 목록");
//			System.out.println(listCopy);
//
//			System.out.println("저장 데이터 목록");
//			System.out.println(list);

			// empty exception
			if(list.size()>0) {
				// tb_dvc_tg_nd ==> 기존 데이터
				sqlSessionTemplate.update(namespace + "currentSaveWorker",list);
				
				// listCopy 에 안넣은 이유는 추가할 데이터를 모두
				// 삭제할경우에는 listCopy.size() == 0 이기 때문에
				// 그럴때는 어차피 tb_dvc_tg_nd_add 에 데이터가 없을 경우니
				// 여기서 삭제를 시도한다.
				// tb_dvc_tg_nd_add ==> 삭제 하기
				sqlSessionTemplate.delete(namespace + "copyDelWorker",list);
				
				/*//작업자 변경시
				//기존에 작업자별 수량 쌓는 테이블 재계산하기
				// 기존 데이터 삭제후 새로운 데이터 집어넣는 방식
				//where 작업자 , date 조건으로 삭제후 재생성
				sqlSessionTemplate.delete(namespace + "deleteWorkerCnt" , list);
				sqlSessionTemplate.insert(namespace + "changeWorkerCnt" , list);*/
				
				//tg_dvc_tg_nd 에 수량 다시 넣기
				//관련된 dvc아이디만 업데이트 시키기
				//기존 작업자 수량 변경
				sqlSessionTemplate.update(namespace + "changeEndCnt" , list);
				
			}
			
			// empty exception
			if(listCopy.size()>0) {
				// tb_dvc_tg_nd_add ==> 추가 데이터
				sqlSessionTemplate.insert(namespace + "copySaveWorker",listCopy);
				//tg_dvc_tg_nd 에 수량 다시 넣기
				//관련된 dvc아이디만 업데이트 시키기
				//추가 작업자 수량변경
				sqlSessionTemplate.update(namespace + "changeEndCntAdd" , listCopy);
				
			}
			
			
			

			str = "success";
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String getWorkerList(HttpSession session,PopVo popVo) throws Exception {
		String str="";
		try {
			List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getWorkerList", popVo);
			List<PopVo> FPDataList = sqlSessionTemplate.selectList(namespace + "getDeviceStatus", popVo);
			
			int FpCnt = (int) sqlSessionTemplate.selectOne(namespace + "getFpCnt", popVo);
			
			List list = new ArrayList<PopVo>();
			List FPlist = new ArrayList<PopVo>();
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				
				map.put("dvcId", dataList.get(i).getDvcId());
				map.put("empCd", dataList.get(i).getEmpCd());
				map.put("color", dataList.get(i).getColor());
				map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
				map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
				map.put("cnt", FpCnt);

				list.add(map);
				
			}

			session.setAttribute("fpCnt", FpCnt);
			for (int i = 0; i < FPDataList.size(); i++) {
				Map map = new HashMap();
				if(i==0) {
					session.setAttribute("cnt", FPDataList.get(i).getCnt());
				}
				map.put("cnt", FPDataList.get(i).getCnt());
				map.put("dvcId", FPDataList.get(i).getDvcId());
				map.put("ty", FPDataList.get(i).getTy());
//				map.put("name", URLEncoder.encode(FPDataList.get(i).getName(), "utf-8"));
//				map.put("nm", URLEncoder.encode(FPDataList.get(i).getNM(), "utf-8"));
				
				
				FPlist.add(map);
				
			}
//			logger.info(popVo.getEmpCd());
//			System.out.println(dataList);
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			dataMap.put("fplist", FPlist);
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String getFpList(PopVo popVo) throws Exception {
		String str="";
		try {
			List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "getFpList", popVo);
			
			List<PopVo> fpActionList = sqlSessionTemplate.selectList(namespace + "fpActionList", popVo);
			
			List list = new ArrayList<PopVo>();
			List actionList = new ArrayList<PopVo>();
			
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				
				map.put("dvcId", dataList.get(i).getDvcId());
				map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
				map.put("ty", dataList.get(i).getTy());
				map.put("startDateTime", dataList.get(i).getStartDateTime());
				map.put("endDateTime", dataList.get(i).getEndDateTime());
				map.put("date", dataList.get(i).getDate());
				map.put("cause", dataList.get(i).getCause());
				map.put("result", dataList.get(i).getResult());

				list.add(map);
				
			}
			
			for (int i = 0; i < fpActionList.size(); i ++) {
				Map map = new HashMap();

				map.put("id",fpActionList.get(i).getIdx());
				map.put("ty",fpActionList.get(i).getTy());
				map.put("content",URLEncoder.encode(fpActionList.get(i).getContent(),"utf-8"));
				actionList.add(map);
				
			}
			
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			dataMap.put("fpActionList", actionList);
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return str;
	}

	@Override
	public String fpSaveList(String val) throws Exception {
		// TODO Auto-generated method stub
		String str ="";
		try {
			System.out.println(val);
			
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject)parser.parse(val);
			JSONArray array = (JSONArray)obj.get("val");
			
			List<PopVo> list = new ArrayList<PopVo>();
			
			for(int i=0; i<array.size(); i++) {
				PopVo popVo= new PopVo();
				JSONObject temp = (JSONObject) array.get(i);
				
				popVo.setDvcId(temp.get("dvcId").toString());
				popVo.setStartDateTime(temp.get("startDateTime").toString());
				popVo.setEndDateTime(temp.get("endDateTime").toString());
				popVo.setDate(temp.get("date").toString());
				popVo.setCause(temp.get("cause").toString());
				popVo.setResult(temp.get("result").toString());
				popVo.setEmpCd(temp.get("empCd").toString());
				
				list.add(popVo);
			}
			
			sqlSessionTemplate.update(namespace + "fpSaveList",list);
			System.out.println(val);
			

			str = "success";
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	
	@Override
	public String PoPprdNoList(PopVo popVo) throws Exception {
		String str = "";
		try {
			List<PopVo> dataList = sqlSessionTemplate.selectList(namespace + "PoPprdNoList", popVo);
			List<PopVo> popList = sqlSessionTemplate.selectList(namespace + "popIdList", popVo);
	
			List list = new ArrayList<ChartVo>();
			List listpop = new ArrayList<ChartVo>();
			//전체 라우팅
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				map.put("prdNo", URLEncoder.encode(dataList.get(i).getPrdNo(), "utf-8"));
//				map.put("matNo", URLEncoder.encode(dataList.get(i).getRWMATNO(), "utf-8"));
				list.add(map);
			};
			//pop장비 라우팅
			for (int i = 0; i <popList.size(); i++) {
				Map map = new HashMap();
				map.put("prdNo", URLEncoder.encode(popList.get(i).getPrdNo(), "utf-8"));
//				map.put("matNo", URLEncoder.encode(dataList.get(i).getRWMATNO(), "utf-8"));
				listpop.add(map);
			}
			
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			dataMap.put("popList", listpop);
			ObjectMapper om = new ObjectMapper();

			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			str = "fail";
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	
}
