package com.unomic.dulink.chart.domain;


import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class BarChartVo{

	List<DataVo> data;
	String color;

	
}


