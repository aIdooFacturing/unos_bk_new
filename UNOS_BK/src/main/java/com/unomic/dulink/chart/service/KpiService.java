package com.unomic.dulink.chart.service;

import javax.servlet.http.HttpServletResponse;

import com.unomic.dulink.chart.domain.ChartVo;

public interface KpiService {

	String getWorkerCnt(ChartVo chartVo) throws Exception;

	String getPrdNoCnt(ChartVo chartVo) throws Exception;

	String getDeviceCnt(ChartVo chartVo) throws Exception;

	String getDetailProductStatus(ChartVo chartVo,HttpServletResponse response) throws Exception;
	
	String getoperationStatus(ChartVo chartVo) throws Exception;

	String getStandard(ChartVo chartVo) throws Exception;

	String updateStandard(ChartVo chartVo) throws Exception;

	String getoperationStatusNd(ChartVo chartVo) throws Exception;

	String getoperationStatusNight(ChartVo chartVo) throws Exception;

	String getoperationStatusToNight(ChartVo chartVo) throws Exception;

	String getopearationStatus(ChartVo chartVo) throws Exception;

	String updateOpStandard(ChartVo chartVo) throws Exception;
	
	String getTotalRatio(ChartVo chartVo) throws Exception;

	String getprodataTime(ChartVo chartVo) throws Exception;
	
	String getOpdataTime(ChartVo chartVo) throws Exception;

	String viewByWorker(ChartVo chartVo) throws Exception;

	String viewByDvc(ChartVo chartVo) throws Exception;

	String getAnalysisKpi(ChartVo chartVo) throws Exception;

	String getFpList(ChartVo chartVo) throws Exception;

	String FpQuartCnt(ChartVo chartVo) throws Exception;
}
