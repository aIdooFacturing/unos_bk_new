package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InventoryVo {

	String hi;
	String prdNo;
	String item;
	String id;
	String minOpr;
	String maxOpr;
	String cause;
	String bigo;
	
	int biniohdCnt;
	int bohdCnt;
	
	int iniohdCnt;
	int rcvCnt;
	int notiCnt;
	int issCnt;
	int ohdCnt;
	
	int iniohdCntL;
	int rcvCntL;
	int notiCntL;
	int issCntL;
	int ohdCntL;
	
	int iniohdCntR;
	int rcvCntR;
	int notiCntR;
	int issCntR;
	int ohdCntR;
	
	int iniohdCntM;
	int rcvCntM;
	int notiCntM;
	int issCntM;
	int ohdCntM;
	
	int iniohdCntC;
	int rcvCntC;
	int notiCntC;
	int issCntC;
	int ohdCntC;
	
	int iniohdCntF;
	int rcvCntF;
	int notiCntF;
	int issCntF;
	int ohdCntF;

	int iniohdCntP;
	int rcvCntP;
	int notiCntP;
	int issCntP;
	int ohdCntP;
	
	int copy;
	int copyL;
	int copyR;
	int copyM;
	int copyC;
	int copyP;
	
	int sumIn;
	int sumInL;
	int sumInR;
	int sumInM;
	int sumInC;
	int sumInF;
	int sumInP;
	
	int sumOut;
	int sumOutL;
	int sumOutR;
	int sumOutM;
	int sumOutC;
	int sumOutF;
	int sumOutP;
	
	String idL;
	String idR;
	String idM;
	String idC;
	String idP;
	
	String sDate;
	String eDate;
	String date;
	String proj;
	
	String chk;
	
	String oprNm;
	String dvcId;
	String dvcName;
	
	String afterLot;
	String productName;
	String datetime;
	String lotNo;
	
	String beforeProj;
	String newLotNo;
	
	int cnt;
	String afterProj;
	
	int stock;
	
	String wLotNo;
	String rLotNo;
	String mLotNo;
	String cLotNo;
	String pLotNo;
	
	String wLotCnt;
	String rLotCnt;
	String mLotCnt;
	String cLotCnt;
	String pLotCnt;
	
	String wLotStock;
	String rLotStock;
	String mLotStock;
	String cLotStock;
	String pLotStock;
	
	String wRegDate;
	String rRegDate;
	String mRegDate;
	String cRegDate;
	String pRegDate;
	
	String groupLotNo;
	
	String vndNo;
	int sendCnt;
	String plan;
	String barcode;
	int idx;
	int prce;
	
	String deliveryNo;
	String remarks;
	int fault;
	int ex;
	
	String shipLot;
	String vndNm;
	
	String RWMATNO;
	String tgCyl;
	String capa;
	String ITEMNO;
	String wcCd;
	String oprNo;
	String name;
	String nm;
	String empCd;
	String empCdD;
	String empCdN;
	int cntD;
	int cntN;
	int workIdx;
}
