package com.unomic.dulink.chart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;
import com.unomic.dulink.chart.service.CommonService;

@RequestMapping(value = "/common")
@Controller
public class CommonController {
	
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value = "getWorkerList")
	@ResponseBody
	public String getWorkerList(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.getWorkerList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "getAllWorkerList")
	@ResponseBody
	public String getAllWorkerList(ChartVo chartVo) {
		String result = "";
		try {
			result = commonService.getAllWorkerList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value = "getPrdNo")
	@ResponseBody
	public String getPrdNo(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.getPrdNoList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value = "getPrdNoList")
	@ResponseBody
	public String getPrdNoList(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.getPrdNoList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getPrdNoListByRtng")
	@ResponseBody
	public String getPrdNoListByRtng(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.getPrdNoListByRtng(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getDeviceCnt")
	@ResponseBody
	public String getDeviceCnt(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.getDeviceCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="dvcList")
	@ResponseBody
	public String dvcList(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.dvcList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getCommonTyList")
	@ResponseBody
	public String getCommonTyList(ChartVo chartVo) {
		String str = "";
		try {
			str = commonService.getCommonTyList(chartVo);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return str;
	}
}
