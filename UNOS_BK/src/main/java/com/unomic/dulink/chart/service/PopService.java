package com.unomic.dulink.chart.service;

import javax.servlet.http.HttpSession;

import com.unomic.dulink.chart.domain.PopVo;

public interface PopService {
	public String test(PopVo popVo) throws Exception;

	public String getGoWorkList(PopVo popVo) throws Exception;

	public String GoWorkSave(PopVo popVo) throws Exception;

	public String getOffWorkList(PopVo popVo) throws Exception;
	
	public String OffWorkSave(PopVo popVo) throws Exception;

	public String getStartJobList(PopVo popVo) throws Exception;
	
	public String StartJobSave(PopVo popVo) throws Exception;

	public String getHistoryList(PopVo popVo) throws Exception;

	public String getEndJobList(PopVo popVo) throws Exception;

	public String popLoginChk(HttpSession session,PopVo popVo) throws Exception;
	
	public String getEndJobInfo(PopVo popVo) throws Exception;
	
	public String finshEndJob(PopVo popVo) throws Exception;

	public String strTest(PopVo popVo) throws Exception;

	public String popGoEndHistory(PopVo popVo) throws Exception;

	public String jsonTest(PopVo popVo) throws Exception;

	public String popLineChk(PopVo popVo) throws Exception;

	public String getPopDevice(PopVo popVo) throws Exception;

	public String getPopChangeWorker(String val) throws Exception;
	
	public String getWorkerList(HttpSession session,PopVo popVo) throws Exception;

	public String getFpList(PopVo popVo) throws Exception;

	public String fpSaveList(String val) throws Exception;
	
	public String PoPprdNoList(PopVo popVo) throws Exception;

}
