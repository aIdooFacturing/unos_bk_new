package com.unomic.dulink.popApi.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.chart.domain.PopVo;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.popApi.domain.BigoVo;
import com.unomic.dulink.popApi.domain.CommuteVo;
import com.unomic.dulink.popApi.domain.JobVo;
import com.unomic.dulink.popApi.domain.PopApiVo;
import com.unomic.dulink.popApi.domain.PrdVo;
@Service
@Repository
public class PopApiServiceImpl implements PopApiService {
	private final static String namespace = "com.unomic.dulink.popApi.";

	private static final Logger logger = LoggerFactory.getLogger(PopApiServiceImpl.class);

	@Inject
	SqlSession sql;

	@Override
	@Transactional
	public String postCommute(CommuteVo cmtVo) throws Exception {
		String str= CommonCode.MSG_CD_FAILED;
		int cnt = 0;
		try {
//			//test
//			cmtVo.setIs_outing(0);
//			cmtVo.setTy_start("I");
//			cnt = (int) sql.selectOne(namespace + "cntCommute", cmtVo);
//		if(cnt>0) {
//			return str = CommonCode.MSG_CD_DUPLE;
//		}
			
			sql.insert(namespace + "addCommute", cmtVo);
			str = CommonCode.MSG_CD_SUCCESS;
		} catch (Exception e) {
			logger.info("Exception!");
			e.printStackTrace();
			// TODO: handle exception
			str= CommonCode.MSG_CD_FAILED; 
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	public String getBigo(BigoVo bigoVo) throws Exception {
		String str= CommonCode.MSG_CD_FAILED;
		//try {
		List<BigoVo> list	=sql.selectList(namespace + "addBigo", bigoVo);
		
//		for(int i : list) {
//			
//		}
//			str = CommonCode.MSG_CD_SUCCESS;
//		} catch (Exception e) {
//			logger.info("Exception!");
//			e.printStackTrace();
//			// TODO: handle exception
			str= CommonCode.MSG_CD_FAILED; 
//		}
		

//		List<PopVo> dataList = sql.selectList(namespace + "getGoWorkList", popVo);
//		
//		List list = new ArrayList<PopVo>();
//		for (int i = 0; i < dataList.size(); i++) {
//			Map map = new HashMap();
//			map.put("startTime", dataList.get(i).getStartTime());
//			map.put("endTime", dataList.get(i).getEndTime());
//			map.put("nm", URLEncoder.encode(dataList.get(i).getNM(), "utf-8"));
//
//			list.add(map);
//		}
//		Map dataMap = new HashMap();
//		dataMap.put("dataList", list);
//		ObjectMapper om = new ObjectMapper();
//		String str = "";
//		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
//		return str;

		
		return str;
	}
	
	@Override
	@Transactional
	public String postBigo(BigoVo bigoVo) throws Exception {
		String str= CommonCode.MSG_CD_FAILED;
		try {
			sql.insert(namespace + "postBigo", bigoVo);
			str = CommonCode.MSG_CD_SUCCESS;
		} catch (Exception e) {
			logger.info("Exception!");
			e.printStackTrace();
			// TODO: handle exception
			str= CommonCode.MSG_CD_FAILED; 
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	@Transactional
	public String putBigo(BigoVo bigoVo) throws Exception {
		String str= CommonCode.MSG_CD_FAILED;
		try {
			sql.update(namespace + "putBigo", bigoVo);
			str = CommonCode.MSG_CD_SUCCESS;
		} catch (Exception e) {
			logger.info("Exception!");
			e.printStackTrace();
			// TODO: handle exception
			str= CommonCode.MSG_CD_FAILED; 
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	
	/*
	 * 외출 체크.
	 * */
	@Override
	public String chkIsOut(CommuteVo cmtVo) throws Exception {
		String str= CommonCode.MSG_CD_FAILED;
		int cnt_O = 0;
		int cnt_I = 0;
		try {
			cmtVo.setIs_outing(1);
			cmtVo.setTy_start("O");
			cnt_O = (int) sql.selectOne(namespace + "cntCommute", cmtVo);
			
			cmtVo.setTy_start("I");
			cnt_I = (int) sql.selectOne(namespace + "cntCommute", cmtVo);

			logger.info("cnt_O:"+cnt_O);
			logger.info("cnt_I:"+cnt_I);
			if(cnt_O>cnt_I) {
				str = CommonCode.MSG_CD_YES;
			}else{
				str = CommonCode.MSG_CD_NO;
			}

		} catch (Exception e) {
			str = CommonCode.MSG_CD_FAILED;
		}
		// TODO Auto-generated method stub
		return str;
	}
	
	@Override
	@Transactional
	public String addJobHist(String val) {
		String str= CommonCode.MSG_CD_FAILED;
		//test
		try {
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
			JSONArray jsonArray = (JSONArray) jsonObj.get("val");

			Integer jobNd = 0;
			jobNd = (int)sql.selectOne(namespace + "chkJobNd");
			if(jsonArray.size()==0) {
				str = CommonCode.MSG_CD_SUCCESS;
				return str;
			}
			
			List<JobVo> list = new ArrayList<JobVo>();
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject tempObj = (JSONObject) jsonArray.get(i);

				JobVo jobVo = new JobVo();
				jobVo.setWorker(tempObj.get("worker").toString());
				jobVo.setDvc_id(tempObj.get("dvc_id").toString());
				jobVo.setTy_se(tempObj.get("ty_se").toString());
				jobVo.setPop_id(tempObj.get("pop_id").toString());
				jobVo.setDvc_cnt(tempObj.get("dvc_cnt").toString());
				jobVo.setRst_cnt(tempObj.get("rst_cnt").toString());
				jobVo.setPrd_no(tempObj.get("prd_no").toString());
				jobVo.setTy_nd(jobNd.toString());
				jobVo.setTy(jobNd.toString());
				jobVo.setTtl_cnt(Integer.parseInt(tempObj.get("rst_cnt").toString()));
				list.add(jobVo);
			}
			sql.insert(namespace + "addJobHist", list);
			
			/*Author Cannon
			 * 
			 * ty_se ==2 means when end job.
			 * 
			 * */
			if(list.get(0).getTy_se().equals("2")) {
				sql.insert(namespace + "addHstPrd", list);
			}

			str = CommonCode.MSG_CD_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			str = CommonCode.MSG_CD_FAILED;
		}

		return str;
	}
	
	@Override
	public List<PrdVo> getListPrdTg(PrdVo prdVo) throws Exception {
		//String str="";
		List<PrdVo> listPrdVo=null;
		try {
			logger.info("tgDate:"+prdVo.getTgDate());
			listPrdVo = sql.selectList(namespace + "getListPrdTg", prdVo);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return listPrdVo;
	}
	

	@Override
	public String jsonTest(PopApiVo popVo) throws Exception {
		String str="";
		try {
			List<PopVo> dataList = sql.selectList(namespace + "jsonTest", popVo);
			
			List list = new ArrayList<PopApiVo>();
			for (int i = 0; i < dataList.size(); i++) {
				Map map = new HashMap();
				map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
				list.add(map);
			}
			logger.info(popVo.getEmpCd());
			Map dataMap = new HashMap();
			dataMap.put("dataList", list);
			ObjectMapper om = new ObjectMapper();
			str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		} catch (Exception e) {
			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return str;
	}
}
