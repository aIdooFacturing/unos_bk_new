package com.unomic.dulink.popApi.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PrdVo {
	String dvcId;
	String isAdd;
	String name;
	String tgDate;
	String prdNo;
	String cntPerCyl;
	String tgCyl;
	String tgRunTime;
	String workIdx;
	String empCd;
	String startCnt;
	String endCnt;
	String startWorkTime;
	String endWorkTime;
	String inputDateTime;
	String group;
}
