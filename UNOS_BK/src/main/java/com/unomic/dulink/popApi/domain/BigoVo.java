package com.unomic.dulink.popApi.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BigoVo {
	
	Integer id;
	Integer cd;
	String bigo;
	Integer ty;
	Integer worker;
	String regdt;
	Integer is_del;
	
	
}
