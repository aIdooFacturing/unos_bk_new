package com.unomic.dulink.popApi.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.popApi.domain.BigoVo;
import com.unomic.dulink.popApi.domain.CommuteVo;
import com.unomic.dulink.popApi.domain.PopApiVo;
import com.unomic.dulink.popApi.domain.PrdVo;
import com.unomic.dulink.popApi.service.PopApiService;


@RequestMapping(value = "/popApi")
@Controller
public class PopApiController {
	private static final Logger logger = LoggerFactory.getLogger(PopApiController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private PopApiService popApiService;

	
	
	@ResponseBody
	@RequestMapping(value = "jsonTest")
	public String jsonTest(PopApiVo popVo) {
		String str="";
		try {
			str = popApiService.jsonTest(popVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	/*
	 * Author Cannon
	 * param
	 * worker : worker ID
	 * ty_start : I:출근, O:퇴근
	 * */
	@ResponseBody
	@RequestMapping(value = "postCommute" ,produces="text/plain;charset=UTF-8")
	public String postCommute(CommuteVo cmtVo) {
		
		logger.info("cmtVo.worker:"+cmtVo.getWorker());
		logger.info("cmtVo.ty_start:"+cmtVo.getTy_start());
		
		String rtnStr="";
		
		try {
			
			rtnStr = popApiService.postCommute(cmtVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return rtnStr;
	}
	
	/*
	 * Author Cannon
	 * */
	@ResponseBody
	@RequestMapping(value = "getBigo" ,produces="text/plain;charset=UTF-8")
	public String getBigo(BigoVo bigoVo) {
		logger.info("cmtVo.worker:"+bigoVo.getId());
		logger.info("cmtVo.worker:"+bigoVo.getCd());
		logger.info("cmtVo.worker:"+bigoVo.getTy());
		logger.info("cmtVo.worker:"+bigoVo.getBigo());
		logger.info("cmtVo.worker:"+bigoVo.getWorker());
		
		String rtnStr="";
		try {
			rtnStr = popApiService.getBigo(bigoVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return rtnStr;
	}
	
	@ResponseBody
	@RequestMapping(value = "postBigo" ,produces="text/plain;charset=UTF-8")
	public String postBigo(BigoVo bigoVo) {
		logger.info("cmtVo.worker:"+bigoVo.getId());
		logger.info("cmtVo.worker:"+bigoVo.getCd());
		logger.info("cmtVo.worker:"+bigoVo.getTy());
		logger.info("cmtVo.worker:"+bigoVo.getBigo());
		logger.info("cmtVo.worker:"+bigoVo.getWorker());
		
		String rtnStr="";
		try {
			rtnStr = popApiService.postBigo(bigoVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return rtnStr;
	}
	@ResponseBody
	@RequestMapping(value = "putBigo" ,produces="text/plain;charset=UTF-8")
	public String putBigo(BigoVo bigoVo) {
		logger.info("cmtVo.worker:"+bigoVo.getId());
		logger.info("cmtVo.worker:"+bigoVo.getCd());
		logger.info("cmtVo.worker:"+bigoVo.getTy());
		logger.info("cmtVo.worker:"+bigoVo.getBigo());
		logger.info("cmtVo.worker:"+bigoVo.getWorker());
		
		String rtnStr="";
		try {
			rtnStr = popApiService.putBigo(bigoVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return rtnStr;
	}
	
	@ResponseBody
	@RequestMapping(value = "chkOuting" ,produces="text/plain;charset=UTF-8")
	public String chkIsOut(CommuteVo cmtVo) {
		String rtnStr=CommonCode.MSG_CD_FAILED;
		if(cmtVo.getWorker()==null) {
			return rtnStr;
		}
			
		logger.info("cmtVo:"+cmtVo);
//		logger.info("cmtVo.worker:"+bigoVo.getId());
//		logger.info("cmtVo.worker:"+bigoVo.getCd());
//		logger.info("cmtVo.worker:"+bigoVo.getTy());
//		logger.info("cmtVo.worker:"+bigoVo.getBigo());
//		logger.info("cmtVo.worker:"+bigoVo.getWorker());
		
		try {
			rtnStr = popApiService.chkIsOut(cmtVo);
			logger.info("rtnStr:"+rtnStr);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return rtnStr;
	}
	
	@ResponseBody
	@RequestMapping(value = "postJobHist" ,produces="text/plain;charset=UTF-8")
	public String postJobHist(String val) {
		logger.info("@@@@@@@@@@@로그 테스트셩~1!!");
		String str=CommonCode.MSG_CD_FAILED;
		try {
			str = popApiService.addJobHist(val);
		} catch (Exception e) {
			str=CommonCode.MSG_CD_FAILED;
			e.printStackTrace();
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value = "getListPrdTg" ,produces="text/plain;charset=UTF-8")
	public List<PrdVo> getListPrdTg(PrdVo prdVo) {
		List<PrdVo> listPrdVo=null;
		try {
			listPrdVo = popApiService.getListPrdTg(prdVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listPrdVo;
	}
	
}
