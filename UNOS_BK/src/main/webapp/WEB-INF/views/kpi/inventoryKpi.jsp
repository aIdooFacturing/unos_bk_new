<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

.k-icon.k-i-collapse{
	display:none;
} 

#grid tbody tr.k-grouping-row td{
	font-size:0px;
	padding :3px;
/* 	background: dodgerblue; */
}

#grid thead tr th{
	vertical-align: middle;
	text-align: center;
}
#grid tbody {
	font-weight: bold;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>

	//grid 변수
	var kendotable;
	
	const loadPage = () =>{
		createMenuTree("kpi", "inventoryKpi")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		
		//grid 그리기
		gridTable();
		
		// getWorkerList => getTable()
		getWorkerList();

		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
// 		$(".date").val(moment().format("YYYY-MM-DD"))
		
		$("#sDate").val(moment().subtract("day",5).format("YYYY-MM-DD"));
		$("#eDate").val(moment().subtract("day",1).format("YYYY-MM-DD"));
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
// 			,"background" : "red"
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
// 			,"background" : "blue"
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
	}
	function sumView(num){
		if(num!=undefined){
			return numberWithCommas(num.sum)
		}else {
			return 0
		}
	}
	var sumRcvCnt = 0;
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height : getElSize(1650)
			,editable : true
			,dataBound : function(e){
				//kendotable 
				if(kendotable!=undefined){
					kendotable.autoFitColumn(0)
				}
			}
			,columns : [{
				field : "item"
				,title : "차종"
				,groupHeaderTemplate: ' '

			},{
				field : "tgCyl"
				,title : "CAPA"
				, attributes: {
					class: "editable-cell" ,
					style: "text-align: center; background-color:yellow; color:black !important;"
				}
				,template : "#=numberWithCommas(tgCyl)#"
			},{
				field : "rcvCnt"
				,title : "소재"
				,template : "#=numberWithCommas(rcvCnt)#"
				,footerTemplate : "#=numberWithCommas(sumRcvCnt)#"
			},{
				field : "rcvCntC"
				,title : "완가공품"
				,template : "#=numberWithCommas(rcvCntC)#"
				,footerTemplate : "#=sumView(data.rcvCntC)#"
			},{
				field : "rcvCntF"
				,title : "도금진행"
				,template : "#=numberWithCommas(rcvCntF)#"
				,footerTemplate : "#=sumView(data.rcvCntF)#"
			},{
				title : "완제품"
				,columns : [{
					field : "issCntF"
					,title : "도금입고"
					,template : "#=numberWithCommas(issCntF)#"
					,footerTemplate : "#=sumView(data.issCntF)#"
				},{
					field : "issCntP"
					,title : "납품실적"
					,template : "#=numberWithCommas(issCntP)#"
					,footerTemplate : "#=sumView(data.issCntP)#"
				},{
					field : "ohdCntP"
					,title : "현 재고"
					,template : "#=numberWithCommas(ohdCntP)#"
				}]
			
			},{
				field : "capa"
				,title : "재고일수"
			}]
		}).data("kendoGrid")
	}
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getTable();
				
			}
		});
	}
	
	function saveRow(){
		var savelist = kendotable.dataSource.data();

// 		tgCyl
// 		item
		var obj = new Object();
		obj.val = savelist;
		
		var url = "/aIdoo/chart/savePrdCapa.do";
		var param = "val=" + JSON.stringify(obj);
// 		console.log(param)

		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				getTable();
// 				console.log(data)
				$.hideLoading();
			}
		})
		
		
	}
	function getTable(){
		
		
		var url = "${ctxPath}/chart/getTermStockStatus.do";
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val();

// 		var param = "eDate=" + $("#eDate").val() +
// 					"&sDate=" + $("#sDate").val();

		$.showLoading()
		$.ajax({
			url: url,
			data: param,
			dataType: "json",
			type: "post",
			success: function (data) {
				$.hideLoading();
				var json = data.dataList;
				console.log(json);
				
				//중복 체크하기 소재총합계수량 구하기 위해서
				var chkDuple = [];
				sumRcvCnt = 0;
				$(json).each(function(idx,data){
					if(chkDuple.indexOf(data.prdNo)==-1){
						chkDuple.push(data.prdNo)
						// 총합계 누적 소재
						sumRcvCnt+=data.rcvCnt;
					}
				})
				console.log(sumRcvCnt)
				$("#sumRcvCnt").html(sumRcvCnt);
// 				console.log(sumRcvCnt)
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					group: [/* {field:"ex",dir:"asc"}, */{ field: "prdNo" ,dir: "asc"}],
					aggregate: [
				        { field: "rcvCnt", aggregate: "sum" },
				        { field: "rcvCntC", aggregate: "sum" },
				        { field: "rcvCntF", aggregate: "sum" },
				        { field: "issCntF", aggregate: "sum" },
				        { field: "issCntP", aggregate: "sum" },
				    ],
					sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    },{
                    	field: "ex" , dir:"asc" 
                    }],
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								item: { editable: false},
								rcvCnt: { editable: false, type: "number" },
								rcvCntC: { editable: false, type: "number" },
								rcvCntF: { editable: false, type: "number" },
// 								capa: { editable: false, type: "number" },
								issCntF: { editable: false, type: "number" },
								issCntP: { editable: false, type: "number" },
								ohdCntP: { editable: false, type: "number" },
							}
						}
					}
				});
				
	
				kendotable.setDataSource(kendodata);
				
			}
		})
			
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				작업자 : <select id="worker"></select>
				날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
				　　<button onclick="saveRow()">저장</button>
				
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
				
			</div>
		</div>
	</div>	
</body>
</html>	