<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 2180 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

.date{
	text-align: center;
}

.k-grid-content-locked tbody tr{
	background: aliceblue;
}

.k-grid-content-locked tbody tr.k-alt{
	background: #F8F8FF;
}

.k-header.k-grid-toolbar {
/*	background: linear-gradient( to top, black, gray); */
	background: #222327;
	color: white;
	text-align: center;
}
.k-grid-custom {
	background: rgb(155, 155, 155);
    color: black;
    font-weight: bold;
}

.k-grid-excel{
	float: right;
	background: whitesmoke;
}

.k-grouping-row {
	height: 0 !important;
	font-size: 0.3 !important;
	line-height: 0;
	background: lavender;
}

.k-grouping-row td{
	height: 0 !important;
	font-size: 0.3 !important;
	line-height: 0;
	background: lavender;
}

.table-main{
	background: #222327;
}
.table-space{
	background: #808080;
}

.cntCss{
/* 	font-family: fantasy; */
	font-weight: bold;
}
.color1{
	color: green !important;
/* 	font-weight: bold; */
	font-family: fantasy;
}

.color2{
	color: rgb(0 183 0) !important;
/* 	font-weight: bold; */
	font-family: fantasy;
}

.color3{
	color: #EDA900 !important; /* yellow */
/* 	font-weight: bold; */
	font-family: fantasy;
}

.color4{
	color: #FF5A5A !important; /* red */
/* 	font-weight: bold; */
	font-family: fantasy;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("kpi", "analysisKpi")
	}
	
	$(function(){
		gridTable();
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => getTable()
		getWorkerList();

		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(50)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1750)
			,"background" : "blue"
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(53)
			,"font-weight" : "bold"
		})
	}
	
	
	function date() {
		return '날짜  :<input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">'
// 		<button onclick="getTable()" <i class="fa fa-search" aria-hidden="true"></i> 조회 </button>
	}
	
	//그리드 그리기
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			toolbar: [
				{ name: "날짜", template: '#=date()#' }
				,{ name: "custom", text: "조회" , iconClass: 'k-icon k-i-search' }
				,{name:"excel"}
			]
			,excel : {
// 				fileName : $("#sDate ").val() + " ~ " + $("#eDate ").val() + "_생산실적.xlsx"
				fileName : "생산실적.xlsx"
			}
			,height : getElSize(1750)
			,dataBound : function(e){
				$(".k-icon.k-i-collapse").remove();
				
				$(".k-grid thead tr th").css({
					"text-align" : "center",
				    "vertical-align" : "middle"
				})
			}
			,columns : [{
				field : "prdNo"
				,title : "차종"
				,groupHeaderTemplate: '　'
				,width : getElSize(455)
				,locked: true
				,lockable: false
			},{
				field : "item"
				,title : "품번"
				,width : getElSize(455)
				,locked: true
				,lockable: false
			},{
				width : getElSize(30)
				,attributes: {
      	    		class: "table-space"
   				}
			},{
				title : "비가동 분석 (분)"
				,columns:[{
					title : "소재"
					,width : getElSize(218)
				},{
					title : "공구교환"
					,width : getElSize(218)
				},{
					title : "설비이상"
					,width : getElSize(218)
				},{
					title : "기타"
					,width : getElSize(218)
				}]
			},{
				title : "비고"
				,width : getElSize(730)
			},{
				title : "R삭"
				,columns:[{
					field : "tgCylR"
					,title : "목표"
					,width : getElSize(218)
				},{
					field : "cntR"
					,title : "실적"
					,attributes: {
	      	    		class: "cntCss"
	   				}
					,width : getElSize(218)
				},{
					field : "ratioR"
					,title : "달성률"
					,width : getElSize(218)
	      	    	,attributes: {
	      	    		class: "#=className(ratioR)#"
	   				}
				}]
			},{
				width : getElSize(30)
				,attributes: {
      	    		class: "table-space"
   				}
			},{
				title:"MCT"
				,columns:[{
					field : "tgCylM"
					,title : "목표"
					,width : getElSize(218)
				},{
					field : "cntM"
					,title : "실적"
					,attributes: {
	      	    		class: "cntCss"
	   				}
					,width : getElSize(218)
				},{
					field : "ratioM"
					,title : "달성률"
					,width : getElSize(218)
	      	    	,attributes: {
	      	    		class: "#=className(ratioM)#"
	   				}
				}]
			},{
				width : getElSize(30)
				,attributes: {
      	    		class: "table-space"
   				}
			},{
				title:"CNC삭"
				,columns:[{
					field : "tgCylC"
					,title : "목표"
					,width : getElSize(218)
				},{
					field : "cntC"
					,title : "실적"
					,attributes: {
	      	    		class: "cntCss"
	   				}
					,width : getElSize(218)
				},{
					field : "ratioC"
					,title : "달성률"
					,width : getElSize(218)
	      	    	,attributes: {
	      	    		class: "#=className(ratioC)#"
	   				}
				}]
			},{
				width : getElSize(30)
				,attributes: {
      	    		class: "table-space"
   				}
			},{
				title:"CNC 누계"
				,columns:[{
					field : "tgCylS"
					,title : "목표"
					,width : getElSize(218)
				},{
					field : "cntS"
					,title : "실적"
					,attributes: {
	      	    		class: "cntCss"
	   				}
					,width : getElSize(218)
				},{
					field : "ratioS"
					,title : "달성률"
					,width : getElSize(218)
	      	    	,attributes: {
	      	    		class: "#=className(ratioS)#"
	   				}
				}]
			}]
		}).data("kendoGrid");
		
		$(".k-grid-custom").click(function(e){
		    // handler body
			getTable();
		});
	}
	
	function className (val){
		if(val > 100){
			return "color1"
		}else if(val >=80){
			return "color2"
		}else if(val >=60){
			return "color3"
		}else{
			return "color4"
		}
		console.log(val)
	}
	//작업자 리스트
	function getWorkerList(){
		
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getTable();
				
			}
		});
	}
	
	function getTable(){
		
		var url = "${ctxPath}/kpi/getAnalysisKpi.do";
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val();
		
		console.log(param);
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading();
				var json = data.dataList;
				console.log(json);
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					group: [
						{field:"prdNo",dir:"asc"}
// 						,{ field: "item" ,dir: "asc"}
					],
// 					sort: [{
//                     	field: "prdNo" , dir:"asc" 
//                     },{
//                     	field: "item" , dir:"asc"
//                     },{
//                     	field: "ex" , dir:"asc" 
//                     }],
				});
				
				kendotable.setDataSource(kendodata)
			}
		})
		
		
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
<!-- 				작업자 : <select id="worker"></select> -->
<!-- 				　날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly"> -->
<!-- 				<button onclick="getTable()"> -->
<!-- 					<i class="fa fa-search" aria-hidden="true"></i> 조회 -->
<!-- 				</button> -->
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
			</div>
		</div>
	</div>	
</body>
</html>	