<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
#grid thead th{
	background: linear-gradient( to bottom, gray, black ) !important;
	color : white;
	text-align: center;	
}
#grid .k-grid-content{
	background: black !important;
}
#grid2 .k-grid-content{
	background: gray !important;
}
#grid2 thead th{
	background: linear-gradient( to bottom, gray, black ) !important;
	color : white !important;
	/* color : linear-gradient( to bottom, gray, black ) !important; */
	text-align: center;	
}
#grid2.k-grid-pager, #grid2.k-grid-header {
    border: 0 !important;
    background:gray;
}
/* #grid2 tbody td{
	background : black;
	border: 0 !important;
	font-weight: bold;
	
} */
#grid .k-grid-pager, #grid .k-grid-header {
    border: 0 !important;
    background:black;
}
#grid tbody td{
	background : black;
	border: 0 !important;
	font-weight: bold;
/* 	font-family: fantasy; */
	
}

.grouping {
	background : linear-gradient( to bottom, gray, black ) !important;
	text-align: left !important;
}
.minus{
	font-weight:bold;
/* 	font-family: fantasy; */
	color : #1312FF;
}

.plus{
	font-weight:bold;
	color : #DB0000;
}
.arrow{
	font-family: fantasy;
}
#peChart text{
	fill : white
}

#avChart text{
	fill : white
}

</style> 
<script type="text/javascript">
	
	const loadPage = () =>{
		createMenuTree("kpi", "productionStatusKpi_backUp")
	}

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	$(function(){
		$("#sDate").datepicker({ maxDate: 0});

		setEl();
		time();
		setDate();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		var show=1;
		$('.tableDate').mouseover(function() {
			show=3;
		});
		$('.tableDate').mouseout(function() {
			show=1;
		});

		window.setInterval(function(){
			
			if(show==1){
				$("#dayDate").css("display","none")
				$("#monthDate").css("display","")
				show=2
			}else if(show==2){
				$("#dayDate").css("display","")
				$("#monthDate").css("display","none")
				show=1
			}
		},5000);
		
		chkBanner();
	});
	
	var todayChk="";
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var time =addZero(String(date.getHours()))+":"+addZero(String(date.getMinutes()));
		var Yday=new Date(date.setDate(date.getDate()-1));
		
		if(time<="08:30"){	//8시 30분이전일때 전날의 데이터 보여주기
			month = addZero(String(Yday.getMonth()+1));
			day = addZero(String(Yday.getDate()));
			todayChk="true";
		}
		$("#sDate").val(year + "-" + month + "-" + day);
	};
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function setEl(){
		
		$("select, button, input").css({
			"font-size" : getElSize(38),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("input[type=button]").css({
			"background" : "#9B9B9B",
			"padding" : getElSize(15),
			"border-radius": getElSize(8)
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		$(".text2").css({
			"font-size" : getElSize(45),
			"padding-top" : getElSize(280),
			"padding-left" : getElSize(360)
		});
		
		$(".text1").css({
			"font-size" : getElSize(52),
//			"background" : "linear-gradient( to bottom, #00A04B, #005026 )",
			"padding" : getElSize(8),
			
		});
		
		$(".minus").css({
			"font-size" : getElSize(45),
		})
		 	
		$(".plus").css({
			"font-size" : getElSize(45),
		})
		
		$("select").css("font-size",getElSize(47));
		$("select").css("height",getElSize(88))
		$("input").css("height",getElSize(88))
		$("input").css("font-size",getElSize(47));
		
		$("#sDate").css({
			"width" : getElSize(400),
			"background" : "black",
			"color" : "white",
			"border" : "none"
		})
		$("select").css("width",getElSize(400))
		$(".tableTitle").css("font-size",getElSize(50));
		
		$("select").css({
		    "border": "1px black #999",
		    "font-family": "inherit", 
		    "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%",
		    "background-color" : "black",
		    "z-index" : "999",
		    "border-radius": "0px",
		    "-webkit-appearance": "none",
		    "-moz-appearance": "none",
		    "appearance":"none",
			"background-size" : getElSize(60),
			"color" : "white",
			"border" : "none"
		})
		
		$("select option").css({
	        "background-color" : "rgba(0,0,0,5)",
	        "color" : "white",
		    "border" : "1",
	        "transition-delay" : 0.05,
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(52) + "px",
	        "transition" : "1s"
	    })
	    
	    $("#chartDiv").css("height",getElSize(1000))
	    $(".contWidth").css("width",getElSize(3840))
	    
		$(".ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all").css("font-size",getElSize(42))

	    $("#div1").css({
			"height" : getElSize(150)
		})
		
		$("#div2").css({
			"height" : getElSize(1000)
		})

		$("#div3").css({
			"height" : getElSize(650)
		})

	};
	
	function comma(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function IdxName(name){
		if(name==1){
			return "야간"
		}else if(name==2){
			return "주간"
		}
	}
	
	function c(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	

	var kendotable;
	
	$(document).ready(function(){
		getWorkerList();
	})
    var date;

	var ChartShowCnt;
	var ChartShowTime;
	function getTotalRatio(){
		$.showLoading()
		var url = "${ctxPath}/kpi/getTotalRatio.do";
		
		var checkedStandard="false";
//	   var date = new Date('2018-02-01');
		var performance;
		var Availability;
		var Quality;
		var OEE;
		
		
		//하루전 데이터 구하기
		var dayValue=1;	//기본 하루전날의 데이터구하기
	    date = new Date($("#sDate").val())
		var dayLabel = date.getDay();
		// 오늘이 월요일일시 3일전 금요일 날짜 구하기
		if(dayLabel==1){
			dayValue=3;
		}
		
	    var workIdx = Number($("#workIdx").val());
	    date.setDate(date.getDate() - dayValue);	//전날의 데이터 구하기
	    var arr=date.toLocaleString().split(".")
	    if(arr[1].trim().length==1){
	    	arr[1]="0"+arr[1].trim()
	    }
	    if(arr[2].trim().length==1){
	    	arr[2]="0"+arr[2].trim()
	    }
	    date=arr[0]+"-"+arr[1].trim()+"-"+arr[2].trim();
		monthDate=$("#sDate").val();
	    monthDate=monthDate.substring(0,7)
		
		if(today==$("#sDate").val()){
			checkedStandard="true";
		}
	    
		var param = "shopId=" + shopId +
					"&worker=" + $("#worker").val() +
					"&sDate=" + $("#sDate").val() +
					"&date=" + date +
					"&monthDate=" + monthDate +
					"&dvcId=" + $("#dvclist").val() +
					"&checkedStandard=" + checkedStandard +
					"&workIdx=" + workIdx;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json1 = data.dataList1;	//위 테이블
				json2 = data.dataList2;	//아래 테이블
				var arr={};
				
				console.log("dd")
				console.log(json1)
				console.log(json2)
				console.log("dd")
				
				//게이지 부분
				performance=json1[0].goalRatio
				Availability=json1[0].operatingRate
				Quality=json1[0].faultCntRatio;
				OEE=(performance*Availability*Quality/10000).toFixed(0);
				
				
				var year=$("#sDate").val().substr(0,4);
				var mon=$("#sDate").val().substr(5,2);
				var day=$("#sDate").val().substr(8,2);

				//금월예상실적수량 구하기
				if(year==(new Date()).getFullYear() && mon==(new Date()).getMonth()+1){
					
					var lastDay = ( new Date( year, mon, 0) ).getDate();	//마지막날짜
					
					var holCnt=0;
					var PassCnt=0;
					
					for(i=1; i<=(new Date()).getDate(); i++){
						var hoilyDay = new Date(year+"-"+mon+"-"+i)
						if(Number(hoilyDay.getDay())==0 /* || Number(hoilyDay.getDay())==6 */){
							PassCnt++;
						}
					}
					
					for(i=1; i<=lastDay; i++){
						var hoilyDay = new Date(year+"-"+mon+"-"+i)
						if(Number(hoilyDay.getDay())==0 /* || Number(hoilyDay.getDay())==6 */){
							holCnt++;
						}
					}
					
					json2[3].cnt=json2[3].cnt/((new Date()).getDate())*(lastDay-holCnt+PassCnt)
					json2[3].opTime=json2[3].opTime/((new Date()).getDate())*(lastDay-holCnt+PassCnt)

				}
				
				if(moment().format("YYYY-MM-DD")!=$("#sDate").val()){
					todayChk="false";
				}
				
				if(moment().format("YYYY-MM-DD")==$("#sDate").val() || todayChk=="true"){
					//금일예상 퍼센트 계산하기
					var timeCheck=json1[0].timeCheck;
					if(workIdx==0){
						totalCnt=1440
					}else{
						totalCnt=720
					}
					
					json2[0].cnt=Number(json1[0].cnt/timeCheck*totalCnt).toFixed(0)
					json2[0].opTime=Number(json1[0].opTime/timeCheck*totalCnt).toFixed(0)
				}

				//상,하강 퍼센트 구하기
				for(i=0;i<3;i++){
					json2[i].expCnt=Number((json2[i].cnt-json2[2].cnt)/json2[2].cnt*100).toFixed(0)
					json2[i].expTime=Number((Number(json2[i].opTime)-Number(json2[2].opTime))/Number(json2[2].opTime)*100).toFixed(0)
					json2[i].expFault=Number((json2[i].faultCnt-json2[2].faultCnt)/json2[2].faultCnt*100).toFixed(0)
					//0번 자리까지 표시
					json2[i].cnt=Number(json2[i].cnt).toFixed(0)
					json2[i].opTime=Number(json2[i].opTime).toFixed(0)
				}
				for(i=3;i<json2.length;i++){
					json2[i].expCnt=Number((json2[i].cnt-json2[5].cnt)/json2[5].cnt*100).toFixed(0)
					json2[i].expTime=Number((Number(json2[i].opTime)-Number(json2[5].opTime))/Number(json2[5].opTime)*100).toFixed(0)
					json2[i].expFault=Number((json2[i].faultCnt-json2[5].faultCnt)/json2[5].faultCnt*100).toFixed(0)
				}
				if(json2[0].cnt=="NaN"){
					console.log("들어옴")
					json2[0].cnt=0
				}
				if(json2[0].opTime=="NaN"){
					console.log("들어옴11")
					json2[0].opTime=0
				}
				
				//진도 뿌리기
				var dataTime=json1[0].tgDate;
				var progress=0;
				if(moment().format("YYYY-MM-DD")==$("#sDate").val() || todayChk=="true"){
					progress=json1[0].progress;
					dataTime+=" "+json1[0].dataTime.substr(11,8);
/* 					console.log(dataTime)
					console.log(json1[0].dataTime.substr(11,8))
					console.log(json1[0].dataTime) */
				}else if(moment().format("YYYY-MM-DD")!=$("#sDate").val()){
					progress=100
				}
				if(json1[0].progress==null){
					progress=0;
				}
				console.log(dataTime)
//				$("#progress").html(dataTime.substr(0,10)+"<br>"+dataTime.substr(11,8)+"<br>(진도 :" + progress + " %)")
				
				//달성율 테이블 뿌리기
				$("#perforM").html(Number(performance).toFixed(0)+" %")
				$("#AvailaB").html(Number(Availability).toFixed(0)+" %")
				$("#Quality").html(Number(Quality).toFixed(2)+" %")
				
				//실적 테이블 뿌리기
				$("#perCnt").html(comma(Number(json1[0].cnt).toFixed(0))+" EA")
				$("#AvailTime").html(comma(Number(json1[0].opTime).toFixed(0))+" Hr")
				$("#QuaCnt").html(comma(Number(json1[0].faultCnt).toFixed(0))+" EA")
				
				//현재달성율,실적 뿌리기
				$("#CurtP").html(Number(performance).toFixed(0)+" %");
				$("#CurtCnt").html(comma(Number(json1[0].cnt).toFixed(0))+" EA");
				
				//현재 가동율,시간 뿌리기
				$("#CurtA").html(Number(Availability).toFixed(0)+" %");
				$("#CurtHr").html(comma(Number(json1[0].opTime).toFixed(0))+" Hr");
				
				console.log("----json1----")
				console.log(json1)
				console.log("----json2----")
				console.log(json2)
				
				//달성율
				var wcName=["${current}${Performance}","금일예상","전일${Performance}"]
				var nowDate=[json1[0].cnt,json2[0].cnt,json2[1].cnt]
				var	stdDate=[json1[0].cnt1,json2[2].cnt,json2[2].cnt]
				
				//가동율
				var Name=["${current}${ophour}","금일예상${ophour}","전일${ophour}"]
				var avNowDate=[Number(json1[0].opTime).toFixed(0),json2[0].opTime,json2[1].opTime]
				var	avStdDate=[json1[0].cnt2,json2[2].opTime,json2[2].opTime]
				
				console.log(wcName)
				console.log(nowDate)
				console.log(stdDate)
				
				//그래프 하나 닫성율
				wcName=["${current}${Performance}", "${target}", "${days}${avrg}"];
				nowDate=[Number(json1[0].cnt),null,null];
				stdDate=[Number(json2[0].cnt)-Number(json1[0].cnt),null,null];
				var target=[null,Number(json1[0].cnt1),null];
				var avg=[null,null,Number(json2[2].cnt)];
				var targetCnt = [null,Number(json1[0].targetCnt)-Number(json1[0].cnt1),null];
				ChartShowCnt=Number(json1[0].targetCnt)-Number(json1[0].cnt1)
				//그래프 하나 가동율
				Name=["${current}${ophour}", "${target}${ophour}","${avrg}${ophour}"];
				avNowDate=[Number(Number(json1[0].opTime).toFixed(0)),null,null];
				avStdDate=[Number(Number(json2[0].opTime)-Number(json1[0].opTime).toFixed(0)),null,null];
				var avtarget = [null,Number(json1[0].cnt2),null];
				avAvg = [null,null,Number(json2[2].opTime)];
				var targetTime = [null,Number((Number(json1[0].targetTime)-Number(json1[0].cnt2)).toFixed(0)),null];
				ChartShowTime=Number((Number(json1[0].targetTime)-Number(json1[0].cnt2)).toFixed(0))
 				/* performanceGauge(performance);
				AvailabilityGauge(Availability);
				createGauge2(Quality); */
				
				//달성율 차트
				peChart(wcName,nowDate,stdDate,target,avg,targetCnt);
//				peChart(wcName,nowDate,stdDate);
				//가동율 차트
				avChart(Name,avNowDate,avStdDate,avtarget,avAvg,targetTime);
				
				
				
				
				//일자료
				$("#dayDate").empty();
				$("#monthDate").empty();
				
				var dayTable="<table border='8' class='tableCss' style='height:100%; width:100%;'><thead><tr><th> ${days}/${months} </th> <th> ${division} </th> <th> ${Production_Quantity2} </th> <th> ${ophour} </th> <th> ${faulty_cnt} </th></tr></thead><tr style='height :33%'><td rowspan='3' class='Tfirst' style='width:7%'>${days}${material}</td>";
				for(var i=0, length=3; i<length; i++){
					if(i==0){
						json2[i].name="${Today} ${prediction}"
					}else if(i==1){
						json2[i].name="${The_day_before} ${Performance}"
					}else if(i==2){
						json2[i].name="${Last_30_days}"
					}
					var expCnt= Number(json2[i].expCnt);
					var expTime= Number(json2[i].expTime);
					var expFault= Number(json2[i].expFault);

					dayTable += "<td style='width:13%' class='Tname'>" + json2[i].name +"</td>" ;
					dayTable +=	"<td style='width:26%' class='TNum'>" + comma(json2[i].cnt) +" EA " ;
					// 상,하강 if문  -->생산수량
					if(i==2){
						dayTable += "</td>";
					}else if(expCnt>=0){
						dayTable += "  <label class='plus'>  ( "+ expCnt + "%";
							if(expCnt>=15){
								dayTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expCnt<0){
						dayTable += "  <label class='minus'>  ( "+ expCnt + "%";
							if(expCnt>=-15){
								dayTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}
					dayTable +=	"<td style='width:26%' class='TNum'>" + comma(json2[i].opTime) + " Hr ";
					
					// 상,하강 if문  -->${ophour}
					if(i==2){
						dayTable += "</td>";
					}else if(expTime>=0){
						dayTable += "  <label class='plus'>  ( "+ expTime + "%";
							if(expTime>=15){
								dayTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expTime<0){
						dayTable += "  <label class='minus'>  ( "+ expTime + "%";
							if(expTime>=-15){
								dayTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								dayTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}else{
						dayTable += "</td>"
					}
					// 상,하강 if문  -->불량 수량
					dayTable +=	"<td style='width:26%' class='TNum'>" + comma(Number(json2[i].faultCnt).toFixed(0)) + " EA" ;
					expFault=expFault*-1;
					if(i==2){
						dayTable += " </td></tr><tr style='height :33%'>" ;
					}else if(expFault>=0){
						dayTable += "  <label class='plus'>  ( "+ expFault + "%";
							if(expFault>=15){
								dayTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								dayTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}		

					}else if(expFault<0){
						dayTable += "  <label class='minus'>  ( "+ expFault + "%";
							if(expFault>=-15){
								dayTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								dayTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}	
					}else{
						dayTable += "</td></tr><tr style='height :33%'>" ;
					}
					
				}
				
				dayTable +="</tr></table>"
				$("#dayDate").append(dayTable)
				
				//월자료
				var monthTable="<table border='8' class='tableCss' style='height:100%; width:100%'; ><thead><tr><th> ${days}/${months} </th> <th> ${division} </th> <th> ${Production_Quantity2} </th> <th> ${ophour} </th> <th> ${faulty_cnt} </th></tr></thead><tr style='height :33%'><td rowspan='3' class='Tfirst' style='width:7%'>${months}${material}</td>";
				for(var i=3, length=json2.length; i<length; i++){
					if(i==3){
						json2[i].name="${This_month} ${prediction}"
					}else if(i==4){
						json2[i].name="${Previous_month} ${Performance}"
					}else if(i==5){
						json2[i].name="${Last_3_months}"
					}
					var expCnt= Number(json2[i].expCnt);
					var expTime= Number(json2[i].expTime);
					var expFault= Number(json2[i].expFault);
					
					monthTable += "<td style='width:13%' class='Tname'>" + json2[i].name + "</td>" ;
					monthTable += "<td style='width:26%' class='TNum'>" + comma(Number(json2[i].cnt).toFixed(0)) + " EA ";
					// 상,하강 if문 
					if(i==5){
						monthTable += "</td>";
					}else if(expCnt>=0){
						monthTable += "  <label class='plus'>  ( "+ expCnt + "%";
							if(expCnt>=15){
								monthTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expCnt<0){
						monthTable += "  <label class='minus'>  ( "+ expCnt + "%";
							if(expCnt>=-15){
								monthTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}else{
						monthTable += "</td>"
					}
					monthTable +=	"<td style='width:26%' class='TNum'>" + comma(Number(json2[i].opTime).toFixed(0)) + " Hr ";
					// 상,하강 if문  -->${ophour}
					if(i==5){
						monthTable += "</td>";
					}else if(expTime>=0){
						monthTable += "  <label class='plus'>  ( "+ expTime + "%";
							if(expTime>=15){
								monthTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td>" ;
							}		

					}else if(expTime<0){
						monthTable += "  <label class='minus'>  ( "+ expTime + "%";
							if(expTime>=-15){
								monthTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td>" ;
							}else{
								monthTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td>" ;
							}	
					}else{
						monthTable += "</td>"
					}
					// 상,하강 if문  -->불량 수량
					monthTable +=	"<td style='width:26%' class='TNum'>" + comma(Number(json2[i].faultCnt).toFixed(0)) + " EA";
					expFault=expFault*-1;
					if(i==5){
						monthTable += " </td></tr><tr style='height :33%'>" ;
					}else if(expFault>=0){
						monthTable += "  <label class='plus'>  ( "+ expFault + "%";
							if(expFault>=15){
								monthTable += "<label class='arrow'> ↑ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								monthTable += "<label class='arrow'> ▲ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}		

					}else if(expFault<0){
						monthTable += "  <label class='minus'>  ( "+ expFault + "%";
							if(expFault>=-15){
								monthTable += "<label class='arrow'> ▼ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}else{
								monthTable += "<label class='arrow'> ↓ </label> ) " + "</label> </td></tr><tr style='height :33%'>" ;
							}	
					}else{
						monthTable += "</td></tr><tr style='height :33%'>" ;
					}
				}
				
				monthTable +="</tr></table>"
				$("#monthDate").append(monthTable)
				
//				$(".tableCss").css("text-align","center");
				
				$(".Tfirst").css("padding-left",getElSize(35));
				$(".Tname").css("padding-left",getElSize(87));
				$(".TNum").css("padding-left",getElSize(262));
				$(".tablecss").css({
					"font-size":getElSize(36) + "px"
				});
				
				$(".tablecss tbody tr td").css({
					"height":getElSize(96) + "px"
				});
				$("#gridTable").css({
					"margin-top" : getElSize(1050)
				})
				
				$.hideLoading()
				/* 				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#dvclist").html(option);
 */				
//				getFaultyList();
			},error:function(request,status,error){
	       		alert("등록된 작업이 없습니다.")
	       		$.hideLoading();
			}

		});
	}
	var today
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvclist").html(list);
				getTotalRatio();
			}
		});	
	};
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getDvcList();
				
			}
		});
	}
	
	function doBlink(){
		console.log("깜박이자")
	}
	//chart toolip show
	function showCnt(a,b){
		if(Number(ChartShowCnt)!=Number(a)){
			return 0;
		}else{
			return b;
		}
	}
	function showTime(a,b){
		if(Number(ChartShowTime)!=Number(a)){
			return 0;
		}else{
			return b;
		}
	}
	function peChart(wcName,nowDate,stdDate,target,avg,targetCnt) {
		
		console.log(avg)
		
		$("#peChart").kendoChart({
			chartArea: {
				background:"#121212",
			},
			title: false,
            legend: {	//범례표?
             	labels:{
            		font:getElSize(36) + "px sans-serif",
            		color:"white"
            	},
            	stroke: {
            		width:100
            	},
            	position: "bottom",
            	orientation: "horizontal",
                offsetX: getElSize(120),
//                offsetY: getElSize(800)
           
            },
            render: function(e) {	//범례 두께 조절
                var el = e.sender.element;
                el.find("text")
                    .parent()
                    .prev("path")
                    .attr("stroke-width", getElSize(20));
            },
			seriesDefaults: {	//데이터 기본값 ()
				gap: getElSize(3),
				type: "column" ,
				stack:true,
				spacing: getElSize(1),
 				labels:{
 					font:getElSize(36) + "px sans-serif",	//no working
 					margin:0,
 					padding:0
				}/* , 
				visual: function (e) {
	                return createColumn(e.rect, e.options.color);
	            } */
			},	
			categoryAxis: {	//x축값
                categories: wcName,
                line: {
                    visible: true
                },majorGridLines: {	//차트안 실선
                    visible: false
                },
                labels:{
                	font:getElSize(36) + "px sans-serif",	
                	color:"white"
				} 
            },
            tooltip: {	//커서 올리면 값나옴
                visible: true,
                format: "{0}%",
                font:getElSize(36) + "px sans-serif",	
                template: "#= series.name #: #= value #"
            },
            valueAxis: {	//간격 y축
                labels: {
                    format: "{0}",
                    font:getElSize(36) + "px sans-serif",
                    color:"white"
                },majorGridLines: {	//차트안 실선
                    visible: true,
                    color : "#353535"
                }

//                majorUnit: 100	//간격
            },
  
            
            series: [
/* {	//데이터
                labels: {
                    visible: true,
                    color: "black",	
                    background:"gray",
                    font:getElSize(60) + "px sans-serif",	
              	    position: "center",
                  },
                  color : "gray",
                  name: "noConnBar",
                  data: noConnBar		
                }, */{	//데이터
				overlay: { gradient: "none" },
                labels: {
                    visible: true,
                    visual: function(e) {
                    	var text=e.text
                    	
                    	/* setInterval(function(){
                			if (e.text != "0") {
                				console.log("0이다")
                				e.text=0;
	                			return e.createVisual();
                			}else{
                				console.log("깜밖")
                				e.text=text
                				return e.createVisual(0);
                			}
                		},0,350); */
                    	
                		if (e.text != "0") {
                         	if(e.text=="null"){
                         	}else{
                                 return e.createVisual(); 
                         	}
                         }
                    },
                    color: "white",	
                    background:"#4374D9",
                    font:getElSize(36) + "px sans-serif",	
              	    position: "center",
                    margin: {
                  		//right : 45
                 	}
//              	    margin:10
                  },
                  color : "#4374D9",

                  name: "${current}${Performance}",
                  data: nowDate		
                },{	//데이터
    				overlay: { gradient: "none" },
                    labels: {
                        visible: true,
                        background:"#CC527A",
                        font:getElSize(36) + "px sans-serif",	
                        template : "#= stackValue #",
                        //0값은 표시 X
                        visual: function(e) {
                            if (e.text != "0") {
                            	if(e.text=="null"){
                                	
                            	}else{
                                    return e.createVisual(); 
                            	}
                            }
                        },
                	    color: "white",
                	    margin: {
                	    	left : getElSize(20)
//                        	bottom : 100,
                        //	left : 45
                        }
                    },
	                    color : "#CC527A",
	                    name: " ${prediction}${Performance}",
	                    data: stdDate
                 } ,{	//데이터
					overlay: { gradient: "none" },
                    labels: {
                         visible: true,
                         background:"#00B300",
                         font:getElSize(36) + "px sans-serif",	
                         position: "center",
                         color: "white",
                         margin: {
//                         	bottom : 100,
                        // 	right : 45
                         }
                     },
 						color : "#00B300",
 						name: "${target}",
 						data: target		
 				},{	//데이터
 					overlay: { gradient: "none" },
                    labels: {
                        visible: true,
                        background:"#005900",
                        font:getElSize(36) + "px sans-serif",	
//                        position: "center",
						template : "#= showCnt(value,stackValue) #",
                        //0값은 표시 X
                        visual: function(e) {
                        	if (e.text != "0") {
                            	if(e.text=="null"){
                            	}else{
                                    return e.createVisual(); 
                            	}
                            }
                        },
                        color: "white",
                        margin: {
//                        	bottom : 100,
                       // 	right : 45
                        }
                    },
						color : "#005900",
						name: "${Final_goal}",
						data: targetCnt		
				},{	//데이터
					overlay: { gradient: "none" },
                    labels: {
                        visible: true,
                        background:"#E8175D",
                        font:getElSize(36) + "px sans-serif",	
//                        position: "center",
                        color: "white",
                        margin: {
//                        	bottom : 100,
                       // 	right : 45
                        }
                    },
						color : "#E8175D",
						name: "${avrg}",
						data: avg		
				}]
		})
	}
	
	
	function avChart(wcName,nowDate,stdDate,avtarget,avAvg,targetTime) {
		$("#avChart").kendoChart({
			chartArea: {
//				height: getElSize(1000),
				background:"#121212",
			},
			title: false,
            legend: {	//범례표?
             	labels:{
            		font:getElSize(36) + "px sans-serif",
            		color:"white"
            	},
            	stroke: {
            		width: getElSize(580)
            	},
            	position: "bottom",
            	orientation: "horizontal",
                offsetX: getElSize(120),
           
            },
            render: function(e) {	//범례 두께 조절
                var el = e.sender.element;
                el.find("text")
                    .parent()
                    .prev("path")
                    .attr("stroke-width", getElSize(20));
            },
			seriesDefaults: {	//데이터 기본값 ()
				gap: getElSize(3),
				type: "column" ,
				stack:true,
				spacing: getElSize(1),
 				labels:{
 					font:getElSize(36) + "px sans-serif",	//no working
 					margin:0,
 					padding:0
				}/* , 
				visual: function (e) {
	                return createColumn(e.rect, e.options.color);
	            } */
			},	
			categoryAxis: {	//x축값
                categories: wcName,
                line: {
                    visible: true
                },majorGridLines: {	//차트안 실선
                    visible: false
                },
                labels:{
                	font:getElSize(36) + "px sans-serif",	
                	color:"white"
				} 
            },
            tooltip: {	//커서 올리면 값나옴
                visible: true,
                format: "{0}%",
                font:getElSize(36) + "px sans-serif",	
				template: "#= series.name #: #= value #"
            },
            valueAxis: {	//간격 y축
                labels: {
                	font:getElSize(36) + "px sans-serif",	
                    format: "{0}",
                    color:"white"
                },majorGridLines: {	//차트안 실선
                    visible: true,
                    color : "#353535"
                }

//                majorUnit: 100	//간격
            },
  
            
            series: [/* {	//데이터
                labels: {
                    visible: true,
                    color: "black",	
                    background:"gray",
                    font:getElSize(60) + "px sans-serif",	
              	    position: "center",
                  },
                  color : "gray",
                  name: "noConnBar",
                  data: noConnBar		
                }, */{	//데이터
   				overlay: { gradient: "none" },
                labels: {
                    visible: true,
                    color: "white",	
                    background:"#4374D9",
                    font:getElSize(36) + "px sans-serif",	
              	    position: "center",
                    margin: {
                  	//	right : 45
                 	}
//              	    margin:10
                  },
                  color : "#4374D9",

                  name: "${current} ${ophour}",
                  data: nowDate		
                },{	//데이터
    				overlay: { gradient: "none" },
                    labels: {
                        visible: true,
                        background:"#CC527A",
                        font:getElSize(36) + "px sans-serif",	
                        template : "#=stackValue #",
                        //0값은 표시 X
                        visual: function(e) {
                            if (e.text != "0") {   
                              return e.createVisual(); 
                            }
                        },
                	    color: "white",
                	    margin: {
//                        	bottom : 100,
                        //	left : 45
                        }
                    },
	                    color : "#CC527A",
	                    name: " ${prediction} ${ophour}",
	                    data: stdDate
                 } ,{	//데이터
     				overlay: { gradient: "none" },
                     labels: {
                         visible: true,
                         background:"#00B300",
                         font:getElSize(36) + "px sans-serif",	
                         position: "center",
                         color: "white",
                         margin: {
//                         	bottom : 100,
                         //	right : 45
                         }
                     },
 						color : "#00B300",
 						name: "${target}",
 						data: avtarget		
 				},{	//데이터
 					overlay: { gradient: "none" },
                    labels: {
                        visible: true,
                        background:"#005900",
                        font:getElSize(36) + "px sans-serif",	
                        template : "#= showTime(value,stackValue) #",
                        //0값은 표시 X
                        visual: function(e) {
                        	if (e.text != "0") {
                            	if(e.text=="null"){
                            	}else{
                                    return e.createVisual(); 
                            	}
                            }
                        },
//                        position: "center",
                        color: "white",
                        margin: {
//                        	bottom : 100,
                       // 	right : 45
                        }
                    },
						color : "#005900",
						name: "${Final_goal}",
						data: targetTime		
				},{	//데이터
					overlay: { gradient: "none" },
                    labels: {
                        visible: true,
                        background:"#E8175D",
                        font:getElSize(36) + "px sans-serif",	
//                        position: "center",
                        color: "white",
                        margin: {
//                        	bottom : 100,
                        //	right : 45
                        }
                    },
						color : "#E8175D",
						name: "${avrg}",
						data: avAvg		
				}]
		})

		
		
		/* 	$("#avChart").kendoChart({
			chartArea: {
				background:"#121212",
			},
			title: false,
            legend: {	//범례표?
             	labels:{
            		font:getElSize(48) + "px sans-serif",
            	},
            	stroke: {
            		width:100
            	},
            	position: "bottom",
            	orientation: "horizontal",
                offsetX: getElSize(450),
//                offsetY: getElSize(800)
           
            },
            render: function(e) {	//범례 두께 조절
                var el = e.sender.element;
                el.find("text")
                    .parent()
                    .prev("path")
                    .attr("stroke-width", getElSize(20));
            },
			seriesDefaults: {	//데이터 기본값 ()
				gap: getElSize(5),
				type: "column" ,
				spacing: getElSize(2),
 				labels:{
 					font:getElSize(50) + "px sans-serif",	//no working
 					margin:0,
 					padding:0
				}//, 
				//visual: function (e) {
	            //    return createColumn(e.rect, e.options.color);
	            //}
			},	
			categoryAxis: {	//x축값
                categories: wcName,
                line: {
                    visible: true
                },majorGridLines: {	//차트안 실선
                    visible: false
                },
                labels:{
                	font:getElSize(48) + "px sans-serif",	//no working
				} 
            },
            tooltip: {	//커서 올리면 값나옴
                visible: true,
                format: "{0} Hr",
                template: "#= series.name #: #= value # Hr"
            },
            valueAxis: {	//간격 y축
                labels: {
                    format: "{0} Hr"
                },majorGridLines: {	//차트안 실선
                    visible: true,
                    color : "#353535"
                }
//                majorUnit: 100	//간격
            },
            series: [{	//데이터
                labels: {
                  visible: true,
                  background:"#7F3F97",
//                  rotation: 270,
                },
                color : "#7F3F97",
                name: "${ophour}",
                data: nowDate		
              },{
                labels: {
					visible: true,
					background:"#0080FF",
//					position: "bottom",
//					rotation: 270
           	    },
					color : "#0080FF",
					name: "계획${ophour}",
					data: stdDate
              }]
		}) */
	}
	var drawing = kendo.drawing;
	var geometry = kendo.geometry;
	function createColumn(rect, color) {
        var origin = rect.origin;
        var center = rect.center();
        var bottomRight = rect.bottomRight();
        var radiusX = rect.width() / 2;
        var radiusY = radiusX / 3;
        var gradient = new drawing.LinearGradient({
            stops: [{
                offset: 0,
                color: color
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 1,
                color: color
            }]
        });

        var path = new drawing.Path({
                fill: gradient,
                stroke: {
                    color: "none"
                }
            }).moveTo(origin.x, origin.y)
            .lineTo(origin.x, bottomRight.y)
            .arc(180, 0, radiusX, radiusY, true)
            .lineTo(bottomRight.x, origin.y)
            .arc(0, 180, radiusX, radiusY);

        var topArcGeometry = new geometry.Arc([center.x, origin.y], {
            startAngle: 0,
            endAngle: 360,
            radiusX: radiusX,
            radiusY: radiusY                
        });

        var topArc = new drawing.Arc(topArcGeometry, {
            fill: {
                color: color
            },
            stroke: {
                color: "#BDBDBD"
            }
        });
        var group = new drawing.Group();
        group.append(path, topArc);
        return group;
    }
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				<label class="text1">${Base_date} </label> <input type="text" id="sDate" size="9%" readonly> <label class="text1"> ${day}/${night} </label> <select id="workIdx"><option value='0'>전체</option><option value=2>주간</option><option value=1>야간</option></select>
				<label class="text1"> ${worker} </label> <select id="worker"></select><label class="text1"> ${device} </label><select id="dvclist"><option></option></select>
				<input type="button" value="${check}" onclick="getTotalRatio()">
			</div>
		</div>
		<div id="div2">
			<table border="1" style="float: left; width: 50%; height: 100%; text-align: center;">
		 		<tr style="height: 10%">
		 			<th class="tableTitle" rowspan="2" style="width: 65%">Performance<br>( ${Achievement_rate } )</th>
		 			<td class="tableTitle" style="width: 18%;"> ${current}${Achievement_rate } </td>
		 			<td class="tableTitle" id='CurtP' style="width: 17%;"> 0% </td>
		 		</tr>
		 		<tr style="height: 10%">
		 			<td class="tableTitle" style="width: 18%;"> ${current}${Performance } </td>
		 			<td class="tableTitle" id='CurtCnt' style="width: 17%;"> 0 EA </td>
		 		</tr>
		 		<tr style="height: 80%">
		 			<td colspan="3"><div style="margin: 0; height: 100% ;width: 100%" id="peChart"></div></td>
		 		</tr>
		 	</table>
		 	<table border="1" style=" float: left; width: 50%; height: 100%; text-align: center;" >
		 		<tr style="height: 10%">
		 			<th class="tableTitle" rowspan="2" style="width: 65%">Availability<br>( ${op_ratio } )</th>
		 			<td class="tableTitle" style="width: 18%;"> ${current}${op_ratio } </td>
		 			<td class="tableTitle" id='CurtA' style="width: 17%;"> 0% </td>
		 		</tr>
		 		<tr style="height: 10%">
		 			<td class="tableTitle" style="width: 18%;"> ${ophour } </td>
		 			<td class="tableTitle" id='CurtHr' style="width: 17%;"> 0 Hr </td>
		 		</tr>
		 		<tr style="height: 80%">
		 			<td colspan="3"><div style="margin: 0; height: 100% ;width: 100%" id="avChart"></div></td>
		 		</tr>
		 	</table>
		</div>
		<div id="div3">
			<div id="dayDate" class="tableDate" style="height: 100%; width: 100%; background: linear-gradient( to bottom, #181818 , #323232)">	일자료
			</div>
			<div id="monthDate" class="tableDate" style="height: 100%; width: 100%; background: linear-gradient( to bottom, #323232, #181818 ); display: none;">	월자료
			</div>
		</div>
					
<!-- 					<div style="background: red; position: absolute; width: 100%; height: 30% ;float: left;">
						asd
					</div>
 -->					
<!--  					 <div class="contWidth" id="chartDiv" style="position: absolute; float: left; background: #242424;" > -->
 					 	
<!--  					 </div> -->
<!-- 					<div class="contWidth" id="gridTable" style="float: left ; height: 28%; position: absolute;"> -->
						
						
<!-- 					</div> -->
	 </div>
</body>
</html>	