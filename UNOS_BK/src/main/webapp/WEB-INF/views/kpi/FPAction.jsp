<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("kpi", "FPAction")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => gridTable() => getTable()
		getDvcList();
		//장비리스트
//		getWorkerList();
		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
		$("#sDate").val(moment().subtract("month",1).format("YYYY-MM-DD"));
		$("#eDate").val(moment().subtract("day",0).format("YYYY-MM-DD"));
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
	}
	
	//장비 리스트
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" ;		
		dvcArray = [];
		$.showLoading();
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#group").html(list);
				
				gridTable();
// 				getAlarmData();
			}
		});	
	};
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				
				
			}
		});
	}
	
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height : getElSize(1650)
			,filterable: true
			,toolbar :[{name:"excel"}]
			,excel:{
				fileName:"FP조치관리" + moment().format("YYYY-MM-DD") + ".xlsx"
			}
			,columns : [{
				title : "FP 종류"
				,field : "ty"
				,filterable: {
	                multi: true,
	                search: true
	            }
				,width : getElSize(270)
			},{
				title : "장비명"
				,field : "name"
				,filterable: {
	                multi: true,
	                search: true
	            }
				,width : getElSize(630)
			},{
				title : "발생시간"
				,field : "startTime"
				,filterable : false
				,width : getElSize(630)
			},{
				title : "조치시간"
				,field : "endTime"
				,filterable : false
				,width : getElSize(630)
			},{
				title : "작업자"
				,field : "nm"
			},{
				title : "작업시간"
				,filterable : false
				,field : "time"
			},{
				title : "FP 원인"
				,field : "cause"
			},{
				title : "조치내용"
				,filterable : false
				,field : "result"
			}]
		}).data("kendoGrid")
		getTable();
	}
	function getTable(){
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&dvcId=" + $("#group").val();
		var url = ctxPath + "/kpi/getFpList.do"
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			success : function(data){
				
				var json = data.dataList;
				
				$(json).each(function(idx,data){
					data.nm = decode(data.nm);
					data.cause = decode(data.cause);
					data.result = decode(data.result);
					data.time = data.time + " 분"
				})
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					schema: {
						model: {
							id: "id",
							fields: {
								rcvCntR: { editable: true, type: "number" },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);
				console.log(data.dataList)
				$.hideLoading();
			}
		})
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				　장비 : <select id="group"></select>
				날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
			
			</div>
		</div>
	</div>	
</body>
</html>	