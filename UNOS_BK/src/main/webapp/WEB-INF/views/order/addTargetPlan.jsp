<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<object id="factory" style="display:none" viewastext classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" codebase="http://www.test.com/ActiveX/ScriptX.cab#Version=6,1,431,2">
</object>
<title>Dash Board</title>
<script type="text/javascript">

const loadPage = () =>{
    /* createMenuTree("maintenance", "Alarm_Manager") */  
    createMenuTree("om", "addTarget")
}


	var autoComplete;
	var buffer="";
		
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<%-- <script src="${ctxPath }/resources/js/jquery.min.js"></script> --%>
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	/* font-family:'Helvetica'; */	/* jane -> 주*/
}
a.k-grid-filter.k-state-active{
	background-color: rgba( 255, 255, 255, 0 );
	color:yellow;
}
span.k-widget.k-dropdown.k-header{
	display:none;
}
@page a4sheet{
 	size:21.0cm 29.7cm 
}
.a4{
	page : a4sheet; page-break-after: always
}
#printD{
	page : a4sheet; page-break-after: always
}
@page a4sheet { size: 21.0cm 29.7cm } 
.a4 { page: a4sheet; page-break-after: always } 
@media print { 
  @page { 
    size:21cm 29.7cm; /*A4*/ 
    margin:0; 
  } 
  html, body { border:0; margin:0; padding:0; } 
} 

.routing{
	color : red !important;
	padding : 0px;
	font-weight: bold;
}
.k-icon.k-i-collapse{
    display:none;
} 

/* .k-icon {
	font-size: 20px; /* Sets icon size to 32px */
/* } */		/* k icon 크기  jane*/



/* .k-dropdown {
    height:1400 !important;
} */

.k-list-container{
    height: 350px !important;
}
.k-list-scroller {
     height: 350px !important;
}

/* .k-filter-menu {
     font-size: 45px !important;
} */

 
/* #wrapper tbody tr td{
	padding :5px;
} */
.k-group-cell{
	background :gray;
}
button#search{
	background : linear-gradient(lightgray, gray);
}
button#search:hover{
	background : gray;
}
.k-header{
	background-color : rgb(53, 53, 66);
}
.k-grid-header{
	background : rgb(53, 53, 66) !important;
}
.actionBtn{
	background : linear-gradient( to bottom, darkgray, gray );
	color : white;
}
.actionBtn:hover{
	background : gray;
}
.copyDateColor td{
	background : red
}

select{
    width:200px;
    padding: .8em .5em;
    border: 1px solid #999;
    font-family: inherit;
    background: url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%;
    z-index : 999;
    border-radius: 0px;
    -webkit-appearance: none;
   -moz-appearance: none;
    appearance:none;
}    
select::-ms-expand{
    display : none;
}
</style> 
<script type="text/javascript">

	var selectlist =[];

	var kendotable;
	var kendoRow;
	
	var currentStart;
	var currentEnd;
	
	var currentStartN;
	var currentEndN;
	
	var compareStartTimeN;
	var compareEndTimeN;
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorverTarget = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		date.setDate(date.getDate()-2)
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	//var handle = 0;
	
	$(function(){
		//주변 어둡게 하기위해
		createCorver();
		
		getWorkerList();
		
		setDate();

		/* createNav("order_nav", 0); */
		
		/* setEl(); */
		setEl_dark();
		/* setEl_dark_test(); */
		/* time(); */
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);	// jane  10 -> 10000
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	/* function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
	function setEl_dark() {
		
		/* var elements = document.getElementsByClassName('k-filter-menu');

		for (var i = 0; i < elements.length; i++) {
		  var element = elements[i];
		  element.style.fontSize = "100px";
		} */

		/* $("#middle_bar").css({
			"left" : getElSize(280) + "px"
		});
 */		
		$("#table").css({
			/* "margin-top" : getElSize(250) + "px",	// jane 추가 */
			"position" : "absolute",
			"width" : $("#container").width(),
			"font-size" : getElSize(32) + "px"	// 저장 print 버튼 들.. 크기 바
			/* "top" : getElSize(100) + marginHeight */
		});
		
		$(".k-button").css({
			"border-radius" : "100px"
		});
		
		
		$(".wrapper").css({
			/* "width" : $(".menu_right").width(), */
			"width" : "100%",
			"margin-top" : getElSize(120),
			"position" : "absolute"	,
		});
		
		$("#info").css({
			"font-size" : getElSize(50) + "px"
		});	// ... 라우팅 미등록 폰....
		
		$(".btnGroup").css({
			"font-size" : getElSize(45) + "px"		// 일자  string 크기 바
		});	// 
		
		$("#date").css({
			"font-size" : getElSize(55) + "px",
			"color" : "white",
			"border-color" : "black",
			"background-color" : "black"
		});	// 
		
		// 버튼 크기 ?? ====================
		$("select, button, input").css({
			"font-size" : getElSize(40)
			/* "margin-left" : getElSize(20),
			"margin-right" : getElSize(20) */
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200)
		});
		
		
		// 타이틀 "작업자 클릭"
		/* $(".k-list-container").css({
			"height" : getElSize(1000) + "px"
		});
		
		$(".k-list-scroller").css({
			"height" : getElSize(1000) + "px"
		}); */
		
		// 첫번째 타이틀
		$(".k-animation-container").css({
			"width" : getElSize(700) + "px"
		});
		
		$(".k-filter-menu").css({
			"font-size" : getElSize(45) + "px"
		});
		
		
		$("div").css({            
	         "border-radius" : getElSize(8) + "px"                
	 	});
		
		$("button").css({            
	         "border-radius" : getElSize(8) + "px"                
	 	});
	}
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"margin-top" : getElSize(250) + "px",	// jane 추가
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$(".wrapper").css({
			/* "width" : $(".menu_right").width(), */
			"width" : "100%",
			"margin-top" : getElSize(120),
			"position" : "absolute"	,
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#info").css({
			"font-size" : getElSize(55)
		})
		
		$("div.k-filter-help-text").html("찾을 작업자를 선택하세요.");
		
		$(".sDate, .hasDatepicker").css({
			"width" : getElSize(330),
			"font-size" : getElSize(60)
		})
		
		$(".btnGroup").css({
			"font-size" : getElSize(60)
		})
		
		$("#search").css({
			"font-size" : getElSize(45),
			"border-radius" : getElSize(12),
			"border" : "0px"
		})
		
	};
	
	/* function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#date").val(year + "-" + month + "-" + day);
		gridTable();
	};
	
	function getToday2(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		return year + "-" + month + "-" + day	
	};
	
	$(function() {
    	$("#date").datepicker({
    		onSelect:function(){
    		}
    	})
	    	
	});
	
	function upGridCss(){
		$(".k-grouping-row td").css({
			"line-height" : 0
			,"font-size" : getElSize(13)
		});
		
		//line 색상
		$(".lineTy").css({
		    "border-right": "2px solid gray"
		})
		
		//button size 조절
		$(".actionBtn").css({
			"border" : "1px solid black",
			"font-size" : getElSize(40),
			"border-radius" : getElSize(15)+"px",
			"padding" : getElSize(15),
			"padding-left" : getElSize(40),
			"padding-right" : getElSize(40),
			"cursor" : "pointer",
			"background" : "aliceblue",
			"color" : "black"
		});
		
	}
	function gridSort(){
		kendoRow.dataSource.data().sort(function(a, b) { // 오름차순
		    //return a.startTimeD < b.startTimeD ? -1 : a.startTimeD > b.startTimeD ? 1 : 0;
		    if(a.startTimeD < b.startTimeD)
		    	return -1;
		    if(a.startTimeD > b.startTimeD)
		    	return 1;
		    if(a.compareStartTimeN < b.compareStartTimeN)
		    	return -1;
		    if(a.compareStartTimeN < a.compareStartTimeN)
		    	return 1;
		});
		
		var list = kendoRow.dataSource.data()
		
	}
	
	//template name
	function worKerName(id){
		var chk=0;
		for(var i = 0; i < selectlist.length; i++){
			if(selectlist[i].value==id){
				chk++;
				return selectlist[i].textname;
			}
		}
		if(chk==0){
			return "미 배 치"
		}
	}
	
	//작업자 리스트 불러오기
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + 1;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var table = "<tbody>";
							
				//workerList 에도 추가
				var select ;
				$(json).each(function(idx, data){
					select += "<option id=" + data.id + ">" + decode(data.name) + "</option>";
					
					var arr=new Object();
					arr.text=decode(data.name);
					arr.value=decode(data.id);
					arr.textname=decode(data.name);
					selectlist.push(arr)
				});
				
				$("#workerList").append(select);
					console.log(selectlist)
			}
		});
	}
	
	// comboBox
	function comboBox(container, options, a){
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: selectlist,
			autoWidth: true,
			dataTextField: "text",
			dataValueField: "value",
            placeholder: "작업자 선택",
            height : getElSize(1000),
            change:function(e){
//             	upGridCss();
            }
        }).data("kendoComboBox");
	}

	// 팝업창 오픈
	function openPopup(e){
		showCorverTarget()
/* 		var selected_item = $("#" + selected_menu_id);
		
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    }) */
		var tr = $(e).closest("tr");
		var uid = tr[0].dataset.uid;
		
		var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
		rowItem = $("#grid").data("kendoGrid").dataItem(row)
		
		$("#cTitle span").html(rowItem.name)
		
		console.log("확인----")
		console.log(rowItem)
		
		//같은 장비 담을 리스트 변수
		var rowlist = []
		//같은 장비 데이터 찾아서 팝업창에 모두 띄어주기
		var list = kendotable.dataSource.data();
		$(list).each(function(idx,data){
			if(rowItem.dvcId==data.dvcId){
				var arr = {};
				arr.dvcId = data.dvcId
				arr.name = data.name
				arr.date = data.date
				arr.tgCnt = data.tgCnt
				arr.cntPerCyl = data.cntPerCyl
				arr.empCdD = data.empCdD
				arr.startTimeD = data.startTimeD
				arr.endTimeD = data.endTimeD
				arr.empCdN = data.empCdN
				arr.startTimeN = data.startTimeN
				arr.endTimeN = data.endTimeN
				arr.compareStartTimeN = data.compareStartTimeN
				arr.compareEndTimeN = data.compareEndTimeN
				arr.prdNo = data.name
				arr.copy = data.copy
				arr.group = data.group
				
				rowlist.push(arr)
			}
		})
		console.log(rowlist)
		var dataRowSource = new kendo.data.DataSource({
			/* data: [{
					"dvcId" : rowItem.dvcId
					,"name" : rowItem.name
					,"date" : rowItem.date
					,"empCdD" : rowItem.empCdD
					,"startTimeD" : rowItem.startTimeD
					,"endTimeD" : rowItem.endTimeD
					,"empCdN" : rowItem.empCdN
					,"startTimeN" : rowItem.startTimeN
					,"endTimeN" : rowItem.endTimeN
					,"compareStartTimeN" : rowItem.compareStartTimeN
					,"compareEndTimeN" : rowItem.compareEndTimeN
					,"prdNo" : rowItem.name
					,"copy" : rowItem.copy
					,"group" : rowItem.group
				}],  */
			data: rowlist,
			autoSync: true,
			schema: {
			    model: {
					id: "a",	 
					fields: {
					attributes:{style:"text-align:right;"},
						l: { editable: true,validation: { required: false },nullable:true },
					}
			    }
			},
			sort: [{
            	field: "startTimeD" , dir:"asc"
            },{
            	field:"compareStartTimeN",dir:"asc"
            }]
		});
		
		kendoRow.setDataSource(dataRowSource)
		
	  	$("#popup").css({
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": "aliceblue",
// 	        "top": getElSize(700),
// 	        "left": getElSize(1000),
	        "height": getElSize(1700),
	        "width": getElSize(3000),
	        "z-index" : 10,
	        "top":"50%",
		    "left":"50%",
		    "transform": "translate(-50%, -50%)",
	        "transition" : "1s",
	    })
	    
// 	  	$("#popup").css({
		    
// 	    })
	}
	
	//취소버튼
	function cancelRow(){
		$("#popup").css({
	        "opacity" : 0,
	        "display" : "none",
	        "z-index" : 1,
	    })
	    
	    hideCorver();
		getTable();
	}
	
	//주간 작업시작 변경시
	function TimeDSelect(container, options){
		
 		currentStart=options.model.startTimeD;
		currentEnd=options.model.endTimeD; 
	//	startTime="08:30"
	//	endTime="20:30"
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"08:30",
	    	    max:"20:30",
	    	    change:function(e){
	    	    	//시간 타입을 format 변경하여 저장하기.
	    	    	if(options.field=="startTimeD"){
		    	    	var hour = options.model.startTimeD.getHours();
	           			var minute = options.model.startTimeD.getMinutes();
	    	    	}else if(options.field=="endTimeD"){
	    	    		var hour = options.model.endTimeD.getHours();
	           			var minute = options.model.endTimeD.getMinutes();
	    	    	}
           			
	    	    	options.model.startTimeD = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
	    	    	
	    	    	var list = kendoRow.dataSource.data();
	    	    	
	    			var row = kendoRow.tbody.find("tr[data-uid='" + options.model.uid + "']");
	    			var idx = kendoRow.dataSource.indexOf(kendoRow.dataItem(row));
	    			
	    			var maxLength = kendoRow.dataSource.data().length;
	    			
	    			//작업 종료시간은 변경 못하게 막기
					if(options.field=="endTimeD" && maxLength!=1){
						alert("종료시간을 변경할수 없습니다. ( 작업시작 시간을 변경해주세요 )")
						
	    				options.model.startTimeD = currentStart
	    				options.model.endTimeD = currentEnd
	    				
	    				//grid 재정렬
		    	    	gridSort();
						
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
					}

	    			// 데이터가 한개일 경우에는 수정 불가능하게 하기
	    			if(maxLength==1){
	    				alert("시작,종료 시간을 변경할수 없습니다. 작업자를 먼저 추가해주세요.");
	    				
	    				options.model.startTimeD = currentStart
	    				options.model.endTimeD = currentEnd
	    				
	    				//grid 재정렬
		    	    	gridSort();
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
	    			}else{	// 종료시간을 시작시간 일치시키기
	    				//젤 위쪽의 데이터먼저 수정시키기 위해서
	    			
	    				console.log(list);
	    				console.log(idx);
	    				if(list[idx-1].endTimeD == currentStart && list[idx-1].startTimeD == currentStart){
	    					alert("이전 작업을 먼저 변경해주세요");
	    					options.model.startTimeD = currentStart
		    				options.model.endTimeD = currentEnd
		    				
		    				//grid 재정렬
			    	    	gridSort();
			    	    	// grid CSS
			    	    	upGridCss();
			    	    	kendoRow.dataSource.fetch()

		    				return;
	    				}else{	// 최종 변경시키기
	    					
	    					console.log("===최종 선택===")
	    					console.log(options.model)
	    					
	    					// 시작시간 변경시 그전작업자의 시작시간보다 빠르지 못하게 막기
	    					if(options.model.startTimeD <= list[idx-1].startTimeD){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						options.model.startTimeD = currentStart
			    				options.model.endTimeD = currentEnd
			    				
			    				//grid 재정렬
				    	    	gridSort();
				    	    	// grid CSS
				    	    	upGridCss();
				    	    	kendoRow.dataSource.fetch()

			    				return;
	    					}
		    				list[idx-1].endTimeD = options.model.startTimeD
	    				}
	    			}
	    	    	
	    	    	kendoRow.dataSource.fetch()
	    	    	
	    	    	console.log(options)
	    	    	console.log(container)
	    	    	
	    	    	//grid 재정렬
	    	    	gridSort();
	    	    	
	    	    	// grid CSS
	    	    	upGridCss(); 
	    	    }
	   	});
	}
	
	// 시간 + 30 하여 비교하기 위해서 함수 생성
	function plusTh(time){
		//hour = "3" + options.model.startTimeN.substring(1,5);
		hour = time.substring(0,1);
		hour = Number(hour) + 3;
		time = hour + time.substring(1,5);
		
		return time;
	}
	
	//야간 작업시작 변경시
	function TimeNSelect(container, options){
		currentStartN=options.model.startTimeN;
		currentEndN=options.model.endTimeN;
		
		compareStartTimeN = options.model.compareStartTimeN;
		compareEndTimeN = options.model.compareEndTimeN;
		
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"20:30",
	    	    max:"08:30",
	    	    change:function(e){
	    	    	//시간 타입을 format 변경하여 저장하기.
	    	    	if(options.field=="startTimeN"){
		    	    	var hour = options.model.startTimeN.getHours();
	           			var minute = options.model.startTimeN.getMinutes();
	    	    	}else if(options.field=="endTimeN"){
	    	    		var hour = options.model.endTimeN.getHours();
	           			var minute = options.model.endTimeN.getMinutes();
	    	    	}
	    	    	
					options.model.startTimeN = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
	    	    	
	    	    	var list = kendoRow.dataSource.data();
	    	    	
	    			var row = kendoRow.tbody.find("tr[data-uid='" + options.model.uid + "']");
	    			var idx = kendoRow.dataSource.indexOf(kendoRow.dataItem(row));
	    			var maxLength = kendoRow.dataSource.data().length;
	    			
	    			//작업 종료시간은 변경 못하게 막기
					if(options.field=="endTimeN" && maxLength!=1){
						alert("종료시간을 변경할수 없습니다. ( 작업시작 시간을 변경해주세요 )")

	    				options.model.startTimeN = currentStartN
	    				options.model.endTimeN = currentEndN
	    				
	    				//grid 재정렬
		    	    	//gridSort();
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
					}
					
					// 데이터가 한개일 경우에는 수정 불가능하게 하기
	    			if(maxLength==1){
	    				alert("시작,종료 시간을 변경할수 없습니다. 작업자를 먼저 추가해주세요." + currentStartN);
	    				
	    				options.model.startTimeN = currentStartN
	    				options.model.endTimeN = currentEndN
	    				
	    				//grid 재정렬
		    	    	//gridSort();
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
	    			}else{	// 종료시간을 시작시간 일치시키기
	    				console.log(list[idx-1].endTimeN,currentStartN)
	    				console.log(list[idx-1].startTimeN,currentStartN)
	    				//젤 위쪽의 데이터먼저 수정시키기 위해서
	    				if(list[idx-1].endTimeN == currentStartN && list[idx-1].startTimeN == currentStartN){
	    					alert("이전 작업을 먼저 변경해주세요");
	    					options.model.startTimeN = currentStartN
		    				options.model.endTimeN = currentEndN
		    				
			    	    	upGridCss();
			    	    	kendoRow.dataSource.fetch()

		    				return;
	    				}else{	// 최종 변경시키기
	    					//시간 계산을 위해 시간 +30 변환	 00:00 ~ 08:30 일 경우
	    					
							if(options.model.startTimeN >= "00:00" && options.model.startTimeN <="08:30"){
	    						//최종 결졍 ( 비교 시작시간 변경)
	    						options.model.compareStartTimeN  = plusTh(options.model.startTimeN);
	    					}else{
	    						options.model.compareStartTimeN = options.model.startTimeN;
	    					}
	    					
	    					// 그전작업자의 시작시간보다 빠르거다 같을경우 제외처리
	    					console.log("==== 바교 ====")
	    					console.log("현재값 : " + options.model.compareStartTimeN + "<= , 이전값 : " + list[idx-1].compareStartTimeN);
	    					if(options.model.compareStartTimeN <= list[idx-1].compareStartTimeN){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						
	    						options.model.startTimeN = currentStartN
			    				options.model.endTimeN = currentEndN
			    				options.model.compareEndTimeN = compareEndTimeN;
			    				options.model.compareStartTimeN = compareStartTimeN;
			    				//grid 재정렬
				    	    	//gridSort();
				    	    	// grid CSS
				    	    	upGridCss();
				    	    	kendoRow.dataSource.fetch()

			    				return;
	    					}
	    					
	    					
	    					//최종 결정 ( 실제 종료시간 변경 )
	    					list[idx-1].endTimeN = options.model.startTimeN	
	    					//최종 결정 ( 비교 종료시간 변경)
	    					list[idx-1].compareEndTimeN = options.model.compareStartTimeN;
	    					
	    				}
	    			}
					
	    			kendoRow.dataSource.fetch()
	    	    	
	    	    	console.log(options)
	    	    	console.log(container)
	    	    	
	    	    	//grid 재정렬
	    	    	gridSort();
	    	    	
	    	    	// grid CSS
	    	    	upGridCss();
	    	    	
	    	    }
	   	});
	
	}
	
	//삭제 버튼 클릭시
	function deleteRow(e){
		
		if(confirm("삭제하시겠습니까?")==false){
			return false;
		}
		
		$('.actionBtn').attr('disabled', true);
		
		setTimeout(function() {
			var row = $(e).closest("tr");
			// 선택한 행의 인덱스값
			var rowIdx = $("tr", kendoRow.tbody).index(row);
			// 선택한 행의 배열 값
			var dataItem = $(e).closest("tr")[0].dataset.uid;
				dataItem = kendoRow.dataItem(row);
			// 체크??
			var check = 0;
			// 팝업 데이터 길이
			var length = kendoRow.dataSource.data().length	
			// 팝업 데이터 리스트
			var table= kendoRow.dataSource.data();

			//한개 일때 삭제버튼을 클릭시 막기 // 기존 데이터는 삭제 불가능
			console.log(length)
			console.log(dataItem.copy)
			if(length==1 || dataItem.copy==false || dataItem.copy=="false"){
				alert("처음 데이터는 지울수 없습니다. ")
				$('.actionBtn').attr('disabled', false);
				$.hideLoading()
				return;
			}
			
			// 삭제 시키기
			// 먼저 그리드 먼저 삭제해보기
			// 이전 데이터의 종료시간에 삭제할 데이터 종료시간 넣기
			if(table[rowIdx-1]!=undefined){
				table[rowIdx-1].endTimeD = dataItem.startTimeD;
				table[rowIdx-1].compareEndTimeD = dataItem.compareStartTimeD;
				table[rowIdx-1].endTimeN = dataItem.startTimeN;
				table[rowIdx-1].compareEndTimeN = dataItem.compareStartTimeN;
			}
			// 다음 데이터의 시작시간에 삭제할 데이터 종료시간 넣기
			if(table[rowIdx+1]!=undefined){
				table[rowIdx+1].startTimeD = dataItem.startTimeD;
				table[rowIdx+1].compareStartTimeD = dataItem.compareStartTimeD;
				table[rowIdx+1].startTimeN = dataItem.startTimeN;
				table[rowIdx+1].compareStartTimeN = dataItem.compareStartTimeN;
			}
			// 뒤에 데이터가 없을경우에는 종료시간에 마지막 시간 집어넣어주기 위함
			if(table[rowIdx+1]==undefined){
				table[rowIdx-1].endTimeD = dataItem.endTimeD;
				table[rowIdx-1].compareEndTimeD = dataItem.compareEndTimeD;
				table[rowIdx-1].endTimeN = dataItem.endTimeN;
				table[rowIdx-1].compareEndTimeN = dataItem.compareEndTimeN;
			}
			
			kendoRow.dataSource.remove(dataItem);

			$.hideLoading()
			$('.actionBtn').attr('disabled', false);
		}, 10);
		$.showLoading()
	}
	
	
	//분할 버튼 클릭시
	function insertRow(e){
		$('.actionBtn').attr('disabled', true);
		$.showLoading()

		gridSort();
		
		setTimeout(function() {
			
			kendoRow;
			
			var row = $(e).closest("tr");
			var rowIdx = $("tr", grid.tbody).index(row);
			
			console.log("---insert---")
			var dataItem = $(e).closest("tr")[0].dataset.uid;
			var initData = kendoRow.dataItem(row);
			var length = kendoRow._data.length;
			var loopChk=0; // 1이면 빠져나감
			var maxGroup;	// 분할 수량 구하기
			
			var list=[];
			for(i=0; i<length; i++){
				if(kendoRow._data[i].name==initData.name ){
					if((kendoRow._data[i].startTimeD==kendoRow._data[i].endTimeD ) && kendoRow._data[i].startTimeN==kendoRow._data[i].endTimeN){
						loopChk=1;
						alert("작업시간을 선택해 주세요");
						$.hideLoading()
						$('.actionBtn').attr('disabled', false);
						return;
					}
					if(kendoRow._data[i].prdNo!=initData.prdNo){
						list.push(kendoRow._data[i].prdNo)
					}
				}
			}
			
			//작업시간 변경없이 한번더 추가할경우
			if(loopChk==1){
				console.log("return;");
				$.hideLoading()
				$('.actionBtn').attr('disabled', false);
				return false;
			}
			
			kendoRow.dataSource.insert({
				 routing: initData.routing,
				 dvcId: initData.dvcId,
				 name: initData.name,
				 date: initData.date,
				 empCdD: "",
				 empCdN: "",
				 tgCnt: initData.tgCnt,
				 cntPerCyl: initData.cntPerCyl,
	            /*  capa: initData.capa,
	             empD: initData.empD,
	             empN: initData.empN,
	             prdNo: initData.prdNo,
	             uph: initData.uph, */
	             startTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
	             endTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
	             startTimeN:  moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
	             endTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
	             compareStartTimeN: "38:30",
	             compareEndTimeN: "38:30",
	             copy:"true",
	             group: kendoRow.dataSource.data().length-1
			});

			gridSort();
			
			$('.actionBtn').attr('disabled', false);
	
			$.hideLoading()
		}, 10);
		
	}
	
	//저장한값 저장하기 위한 함수
	function saveRow(){
		
		var kendolist = kendoRow.dataSource.data();
		var savelist = [];
		$(kendolist).each(function(idx,data){
			// 주간 데이터 넣기
			var arr={};
			arr.dvcId = data.dvcId;
			arr.name = data.name;
// 			arr.date = data.date;
			arr.date = $("#date").val();
			arr.empCd = data.empCdD;
			arr.tgCnt = data.tgCnt;
			arr.tgCyl = data.tgCnt;
			arr.cntPerCyl = data.cntPerCyl;
			arr.startTime = data.startTimeD;
			arr.endTime = data.endTimeD;
			arr.workIdx = 2;
			arr.group = data.group;
			arr.copy = data.copy;
			
			savelist.push(arr);
			
			var arr={};
			arr.dvcId = data.dvcId;
			arr.name = data.name;
// 			arr.date = data.date;
			arr.date = $("#date").val();
			arr.empCd = data.empCdN;
			arr.tgCnt = data.tgCnt;
			arr.tgCyl = data.tgCnt;
			arr.cntPerCyl = data.cntPerCyl;
			arr.startTime = data.startTimeN;
			arr.endTime = data.endTimeN;
			arr.workIdx = 1;
			arr.group = data.group;
			arr.copy = data.copy;
			
			savelist.push(arr);
			
			
			/* {
// 				"dvcId":"32"
// 				,"name":"OS/R M#1"
// 				,"date":"2019-04-26"
// 				,"empCdD":"157"
// 				,"startTimeD":"08:00"
// 				,"endTimeD":"20:00"
				,"empCdN":"239"
				,"startTimeN":"20:00"
				,"endTimeN":"08:00"
				,"compareStartTimeN":"20:00"
				,"compareEndTimeN":"08:00"
				,"prdNo":"OS/R M#1"
				,"copy":"false"
			} */
			
		})
		
		var obj = new Object();
		obj.val = savelist;
		
		var url = "${ctxPath}/pop/getPopChangeWorker.do";
		var param = "val=" + JSON.stringify(obj);

		$.showLoading()
		console.log(param)
 		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				if(data=="success"){
					$("#popup").css({
				        "transition" : "0.5s",
				        "opacity" : 0,
				        "display" : "none",
				        "z-index" : 1,
				    })
				    hideCorver();
					getTable();
					alert("저장 완료되었습니다");
				}else{
					alert("저장 실패하였습니다. ");
				}
				$.hideLoading()
			}
 		})
		
		console.log("saveRow")
	}
	
	function gridTable(){
		//메인 그리드
		kendotable = $("#grid").kendoGrid({
			editable:false
			,height : getElSize(1500)
			,dataBound :function(e){
				// 데이터가 있을때 바인딩 ()
				if(kendotable!=undefined){
					var list = kendotable.dataSource.data();
					
					as = list[0]
					$(list).each(function(idx,data){
						if(data.copy==true || data.copy=="true"){
							var row = kendotable.tbody.find("tr[data-uid='" + data.uid + "']");
							row[0].setAttribute("class","copyData")
						}
					})
				}
				
				upGridCss();
				
				//group css 버튼 삭제
				$(".k-icon.k-i-collapse").remove();
				/* 
				$("#grid thead tr th").css({
					"font-size" : $("#logo").width() * 0.16 + "px",
					"text-align" : "center",
				    "vertical-align" : "middle"
				});
				
				$("#grid tbody tr td").css({
					"font-size" : $("#logo").width() * 0.2 + "px",
					"text-align" : "center",
					"height" : $("#logo").height() * 2 + "px"
					
				});
				
				$(".actionBtn").css({
					"border" : "1px solid black",
					"font-size" : $("#logo").width() * 0.2 + "px",
					"border-radius" : getElSize(15)+"px",
					"padding" : $("#logo").width() * 0.05,
					"padding-left" : $("#logo").width() * 0.15,
					"padding-right" : $("#logo").width() * 0.15,
					"cursor" : "pointer",
					"background" : "aliceblue"
				}); */
			},
	      	columns:[
				{
					field:"name",title:"장비",width: originWidth * 0.1
					,groupHeaderTemplate: '　'
					,attributes: {
						"class" : "lineTy"
        			}
				},{
					title:"싸이클당<br>생산량"
					,field:"cntPerCyl"
					,width: originWidth * 0.040
				},{
					title:"목표수량"
					,field:"tgCnt"
					,width: originWidth * 0.040
				},{
					title:"UPH"
					,field:"uph"
					,width: originWidth * 0.040
				},{
					title:"생산<br>Capa<br>(EA)"
					,field:"capa"
					,width: originWidth * 0.040
					,attributes: {
						"class" : "lineTy"
        			}
				},{
					title : "주간"
					,columns:[{
						
							field:"empCdD",title:"작업자",width:	originWidth * 0.058
							,editor : comboBox
							,template:"#=worKerName(empCdD)#"
						},{
							field:"startTimeD",title:"시작",width: originWidth * 0.040
							,format:"{0:HH:mm}"
						},{
							field:"endTimeD",title:"종료",width: originWidth * 0.045
							,format:"{0:HH:mm}"
							,attributes: {
								"class" : "lineTy"
		        			}
						}
					]
				},{
					title : "야간"
					,columns:[{
						field:"empCdN",title:"작업자",width:	originWidth * 0.058
						,editor : comboBox
						,template:"#=worKerName(empCdN)#"
					},{
						field:"startTimeN",title:"시작",width: originWidth * 0.040
					},{
						field:"endTimeN",title:"종료",width: originWidth * 0.045
						,attributes: {
							"class" : "lineTy"
	        			}
					}]
				},{
					template : "<button class='actionBtn' onclick='openPopup(this)'> 변경 </button>"
					,width: originWidth * 0.060
				}
			]
		}).data("kendoGrid")

		//팝업창 그리드
		kendoRow = $("#upGrid").kendoGrid({
			editable:true,
			dataBound :function(e){
				// 데이터가 있을때 바인딩 ()
				if(kendoRow!=undefined){
					var list = kendoRow.dataSource.data();
					
					as = list[0]
					$(list).each(function(idx,data){
						if(data.copy==true || data.copy=="true"){
							var row = kendoRow.tbody.find("tr[data-uid='" + data.uid + "']");
							row[0].setAttribute("class","copyData")
						}
					})
				}
				
				// 팝업창 그리드 CSS
				upGridCss();
			},
			cellClose:function(e){
				
				console.log(e)
			},
	      	columns:[
				{
					title : "주간"
					,columns:[{
						
							field:"empCdD",title:"작업자",width:	originWidth * 0.055
							,editor : comboBox
							,template:"#=worKerName(empCdD)#"
						},{
							template: "<span class='k-icon k-i-close-outline k-i-x-outline k-i-error' style='color:red;'></span>"
							,width:	originWidth * 0.020
						},{
							field:"startTimeD",title:"시작",width: originWidth * 0.04
							,format:"{0:HH:mm}"
							,editor : TimeDSelect
						},{
							field:"endTimeD",title:"종료",width: originWidth * 0.04
							,format:"{0:HH:mm}"
							,editor : TimeDSelect
						}
					]
				},{
					title : "야간"
					,columns:[{
						field:"empCdN",title:"작업자",width:	originWidth * 0.055
						,editor : comboBox
						,template:"#=worKerName(empCdN)#"
					},{
						template: "<span class='k-icon k-i-close-outline k-i-x-outline k-i-error' style='color:red;'></span>"
						,width:	originWidth * 0.020
					},{
						field:"startTimeN",title:"시작",width: originWidth * 0.04
						,format:"{0:HH:mm}"
						,editor : TimeNSelect
					},{
						field:"endTimeN",title:"종료",width: originWidth * 0.04
						,format:"{0:HH:mm}"
						,editor : TimeNSelect
					}]
				},{
					template : "<button class='actionBtn' onclick='insertRow(this)'> 분할 </button> <button class='actionBtn delBtn' onclick='deleteRow(this)'> 삭제 </button>"
					,width: originWidth * 0.085
				}
			]
		}).data("kendoGrid")
		
	    $(kendoRow.tbody).on("click", "td", function (e) {
	    	
	    	console.log($(this))

	    	var row = $(this).closest("tr");
            var curRowIdx = $("tr", kendoRow.tbody).index(row);
            var colIdx = $("td", row).index(this);
            var item = kendoRow.dataItem(row);
            
    		//첫번째 행 1번선택했을경우 주간작업자 리셋
    		if($(this)[0].cellIndex==1){
    			item.empCdD="";
    			kendoRow.dataSource.fetch()
				return;
    		}
    		//첫번째 행 5번선택했을경우 야간작업자 리셋
    		if($(this)[0].cellIndex==5){
    			item.empCdN="";
    			kendoRow.dataSource.fetch()
				return;
    		}
    		
	    });
	    
		
		getTable();
	}
	
	function getTable(){
		
		var url = "${ctxPath}/pop/getPopDevice.do";

		var param = "shopId=" + shopId + 
					"&date=" + $("#date").val() +
					"&popId=" + "ALL";
					
		$.showLoading(); 
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList
				console.log(json);
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					console.log(data.startTimeN)
					if(data.startTimeN >="00:00" && data.startTimeN <="08:00"){
						data.compareStartTimeN = plusTh(data.startTimeN);
						data.compareEndTimeN = plusTh(data.endTimeN);
					}else{
						data.compareStartTimeN = data.startTimeN;
						data.compareEndTimeN = data.endTimeN;
					}
					
					
				})
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
	                autoSync: true,
	                schema: {
	                    model: {
	                      id: "a",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                         l: { editable: true,validation: { required: false },nullable:true },
	                      }
	                    }
	                },
	                group: {
                        field: "name",
                        dir: "asc",
                    },
					sort: [{
						field: "name" , dir:"asc" 
					},{
						field: "startTimeD" , dir:"asc"
					},{
						field:"compareStartTimeN",dir:"asc"
					}]
	             });
				
				kendotable.setDataSource(dataSource)
				
				$.hideLoading(); 
			}
		})
		
		
	}
	
	</script>
</head>
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
	
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td> --%>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table style="width: 100%; float: right;">
						<Tr>
							<Td style="color:white; ">
								<div id="info"><center style="background:gray; border-radius:5px;width: 75%;margin-left: 25%;"><spring:message code="routing_manual"></spring:message></center></div>
							</Td>
							<Td class="btnGroup"style="color: white; text-align: right;">
								<spring:message code="date_"></spring:message> :
								<input type="text" id="date" class="sDate"> 
								<button id="search" onclick="getTable()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<input type="hidden" value="<spring:message code="save"></spring:message>" id='save'>
								<input type="hidden" value="<spring:message code="day_to_Night"></spring:message>" id='copy_data'>
							</Td>
						</Tr>
					</table>
			
					<div id="grid" class="wrapper" style="background: rgb(53, 53, 66);">
					</div>
				</td>
				
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
		</table>
	 </div>
	
	<div id="popup" style="display: none; opacity: 0;">
		<div id="cTitle" style="height: 15%; width: 100%; font-size: 350%; font-weight: bold; text-align: center; color: black;">
			<span>
			</span>
			<div style="float: right;">
				<button id="save" class="btn" onclick="saveRow()">저장</button>
				<button id="cancle" class="btn" onclick="cancelRow()">취소</button>
			</div>
		</div>
		<table style="height: 85%; width: 99%;">
			<div id="upGrid" style="width: 99.7%; height: 89.7%%;"></div>
		</table>
	</div>


	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	