<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<%
	response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
	response.setHeader("pragma", "no-cache"); // HTTP 1.0
	response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">

<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body {
	background-color: black;
}

.k-grouping-row {
	height: 0 !important;
	font-size: 0.3 !important;
	line-height: 0;
	background: lavender;
}

.k-grouping-row td {
	height: 0 !important;
	font-size: 0.3 !important;
	line-height: 0;
	background: lavender;
}
</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("om", "addTargetDetail")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// gridTable() => getTable()
		gridTable();
// 		getTable();

		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
			,"background" : "blue"
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
		
		$(".saveBtn").css({"margin-right": getElSize(70)})
	}
	
	//kendo grid 그리기
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height : getElSize(1650)
			,editable : true
			,dataBound : function(e){
				
				$(".k-icon.k-i-collapse").remove();
				
				$(".k-grid thead tr th").css({
					"text-align" : "center",
				    "vertical-align" : "middle"
				})
				
				$(".editable-cell:even").css({
					"background" : "#FAF4C0"
				})
				
				$(".editable-cell:odd").css({
					"background" : "#FAF4DE"
				})
				
			}
			,columns : [{
				field : "num"
				,title : "비교"
				,width : getElSize(200)
				,groupHeaderTemplate: '　'
				,hidden : true
			},{
				field : "dvcId"
// 				,groupHeaderTemplate: '　'
				,title : "번호"
				,width : getElSize(200)
			},{
				field : "name"
				,title : "장비명"
				,width : getElSize(450)
			},{
				field : "prdNo"
				,title : "품번"
				,width : getElSize(450)
			},{
				field : "tgCylS"
				,title : "장비목표"
				,width : getElSize(300)
			},{
				field : "tgCyl"
				,title : "목표수량"
				,width : getElSize(300)
				,attributes : {
					class : "editable-cell"
				}
			}]
			
		}).data("kendoGrid")
		
		getTable();
	}
	
	function saveRow(){
		var list = kendotable.dataSource.data();
		var savelist = [];
		
		$(list).each(function(idx,data){
			if(data.btgCyl!=data.tgCyl){
				savelist.push(data)
			}
		});
		
		if(savelist.length==0){
			alert("수정된 값이 없습니다.");
			return;
		}
		
		console.log("=====저장할 리스트=====");
		console.log(savelist);
		
		var obj = new Object();
		obj.val = savelist;
		
		
		var url = ctxPath + "/order/saveAddTargetDetail.do";
		var param = "val=" + JSON.stringify(obj) ;
		
		$.showLoading();
		
		console.log(obj)
		$.ajax({
			url : url,
			data : param,
			type : "post",
// 			dataType : "json",
			success : function(data){
				$.hideLoading();
				if(data=="success"){
					alert("저장완료 되었습니다.");
					getTable();
				}else{
					alert("저장에 실패하였습니다_01");
				}
				console.log(data)
// 				json = data.dataList;
// 				console.log(json);
			}
		})
		
		
	}
	
	//작업자 리스트
	function getTable(){
		var url = ctxPath + "/order/getAddTargetDetail.do";
// 		var param = "shopId=" + shopId;
		var param = "sDate=" + $("#sDate").val() ;

		$.showLoading();
		console.log(param);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				console.log(json)
				$.hideLoading();
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.btgCyl = data.tgCyl;
				})
				var kendodata = new kendo.data.DataSource({
					data : json
					,group : [{
						field : "num", dir:"asc"
					}]
// 					group: [{field:"ex",dir:"asc"},{ field: "prdNo" ,dir: "asc"}],
					,sort: [{
	                	field: "name" , dir:"asc" 
	                },{
	                	field: "prdNo" , dir:"asc"
	                }]
					,schema:{
						model: {
							id: "id"
							,fields:{
								dvcId : {editable:false}
								,name : {editable:false}
								,prdNo : {editable:false}
								,tgCylS : {editable:false}
								,tgCyl : {type:"number"}
							}
						}
					}
				})
// 				gridMovelist.setDataSource(movelistdata);
				kendotable.setDataSource(kendodata);
			},error : function(e){
				alert("error")
			}
		});
	}
	
</script>
<body>
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				날짜 : <input type="text" id="sDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
				<button onclick="saveRow()" class="saveBtn" style="float: right;">
					<span class="k-icon k-i-save"></span> 저장
				</button>
			</div>
		</div>

		<div id="div2">
			<div id="grid"></div>
		</div>
	</div>
</body>
</html>
