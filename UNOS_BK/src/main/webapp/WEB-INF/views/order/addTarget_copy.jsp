<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<object id="factory" style="display:none" viewastext classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814" codebase="http://www.test.com/ActiveX/ScriptX.cab#Version=6,1,431,2">
</object>
<title>Dash Board</title>
<script type="text/javascript">

const loadPage = () =>{
    /* createMenuTree("maintenance", "Alarm_Manager") */  
    createMenuTree("om", "addTarget")
}


	var autoComplete;
	var buffer="";
		
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<%-- <script src="${ctxPath }/resources/js/jquery.min.js"></script> --%>
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	/* font-family:'Helvetica'; */	/* jane -> 주*/
}
a.k-grid-filter.k-state-active{
	background-color: rgba( 255, 255, 255, 0 );
	color:yellow;
}
span.k-widget.k-dropdown.k-header{
	display:none;
}
@page a4sheet{
 	size:21.0cm 29.7cm 
}
.a4{
	page : a4sheet; page-break-after: always
}
#printD{
	page : a4sheet; page-break-after: always
}
@page a4sheet { size: 21.0cm 29.7cm } 
.a4 { page: a4sheet; page-break-after: always } 
@media print { 
  @page { 
    size:21cm 29.7cm; /*A4*/ 
    margin:0; 
  } 
  html, body { border:0; margin:0; padding:0; } 
} 

.routing{
	color : red !important;
	padding : 0px;
	font-weight: bold;
}
.k-icon.k-i-collapse{
    display:none;
} 

/* .k-icon {
	font-size: 20px; /* Sets icon size to 32px */
/* } */		/* k icon 크기  jane*/



/* .k-dropdown {
    height:1400 !important;
} */

.k-list-container{
    height: 350px !important;
}
.k-list-scroller {
     height: 350px !important;
}

/* .k-filter-menu {
     font-size: 45px !important;
} */

 
/* #wrapper tbody tr td{
	padding :5px;
} */
.k-group-cell{
	background :gray;
}
button#search{
	background : linear-gradient(lightgray, gray);
}
button#search:hover{
	background : gray;
}
.k-header{
	background-color : rgb(53, 53, 66);
}
.k-grid-header{
	background : rgb(53, 53, 66) !important;
}
.actionBtn{
	background : linear-gradient( to bottom, darkgray, gray );
	color : white;
}
.actionBtn:hover{
	background : gray;
}
.copyDateColor td{
	background : red
}

select{
    width:200px;
    padding: .8em .5em;
    border: 1px solid #999;
    font-family: inherit;
    background: url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%;
    z-index : 999;
    border-radius: 0px;
    -webkit-appearance: none;
   -moz-appearance: none;
    appearance:none;
}    
select::-ms-expand{
    display : none;
}
</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		date.setDate(date.getDate()-2)
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	//var handle = 0;
	
	$(function(){
		getWorkerList();
		
		setDate();

		/* createNav("order_nav", 0); */
		
		/* setEl(); */
		setEl_dark();
		/* setEl_dark_test(); */
		/* time(); */
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);	// jane  10 -> 10000
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	/* function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
	function setEl_dark() {
		
		/* var elements = document.getElementsByClassName('k-filter-menu');

		for (var i = 0; i < elements.length; i++) {
		  var element = elements[i];
		  element.style.fontSize = "100px";
		} */

		/* $("#middle_bar").css({
			"left" : getElSize(280) + "px"
		});
 */		
		$("#table").css({
			/* "margin-top" : getElSize(250) + "px",	// jane 추가 */
			"position" : "absolute",
			"width" : $("#container").width(),
			"font-size" : getElSize(32) + "px"	// 저장 print 버튼 들.. 크기 바
			/* "top" : getElSize(100) + marginHeight */
		});
		
		$(".k-button").css({
			"border-radius" : "100px"
		});
		
		
		$(".wrapper").css({
			/* "width" : $(".menu_right").width(), */
			"width" : "100%",
			"margin-top" : getElSize(120),
			"position" : "absolute"	,
			"height" : getElSize(100) + "px"	// jane
		});
		
		$("#info").css({
			"font-size" : getElSize(50) + "px"
		});	// ... 라우팅 미등록 폰....
		
		$(".btnGroup").css({
			"font-size" : getElSize(45) + "px"		// 일자  string 크기 바
		});	// 
		
		$("#date").css({
			"font-size" : getElSize(55) + "px",
			"color" : "white",
			"border-color" : "black",
			"background-color" : "black"
		});	// 
		
		// 버튼 크기 ?? ====================
		$("select, button, input").css({
			"font-size" : getElSize(40)
			/* "margin-left" : getElSize(20),
			"margin-right" : getElSize(20) */
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200)
		});
		
		
		// 타이틀 "작업자 클릭"
		/* $(".k-list-container").css({
			"height" : getElSize(1000) + "px"
		});
		
		$(".k-list-scroller").css({
			"height" : getElSize(1000) + "px"
		}); */
		
		// 첫번째 타이틀
		$(".k-animation-container").css({
			"width" : getElSize(700) + "px"
		});
		
		$(".k-filter-menu").css({
			"font-size" : getElSize(45) + "px"
		});
		
		
		$("div").css({            
	         "border-radius" : getElSize(8) + "px"                
	 	});
		
		$("button").css({            
	         "border-radius" : getElSize(8) + "px"                
	 	});
	}
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"margin-top" : getElSize(250) + "px",	// jane 추가
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$(".wrapper").css({
			/* "width" : $(".menu_right").width(), */
			"width" : "100%",
			"margin-top" : getElSize(120),
			"position" : "absolute"	,
			"height" : getElSize(100) + "px"	// jane
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#info").css({
			"font-size" : getElSize(55)
		})
		
		$("div.k-filter-help-text").html("찾을 작업자를 선택하세요.");
		
		$(".sDate, .hasDatepicker").css({
			"width" : getElSize(330),
			"font-size" : getElSize(60)
		})
		
		$(".btnGroup").css({
			"font-size" : getElSize(60)
		})
		
		$("#search").css({
			"font-size" : getElSize(45),
			"border-radius" : getElSize(12),
			"border" : "0px"
		})
		
	};
	
	/* function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#date").val(year + "-" + month + "-" + day);
		getTargetData();
	};
	
	var className = "";
	var classFlag = true;
	var selectlist=[];
	var printlist=[]
	
	var gridlist;
	console.log("jane kendo start")
	$(document).ready(function(e){
		gridlist = $("#wrapper").kendoGrid({
			dataSource:{
				sort: [{
                	field: "prdNo" , dir:"asc" 
                },{
                	field: "startTimeD" , dir:"asc"
                },{
                	field:"startTimeN",dir:"asc"
                }]
			},
			sort: [{
            	field: "prdNo" , dir:"asc" 
            },{
            	field: "startTimeD" , dir:"asc"
            },{
            	field:"startTimeN",dir:"asc"
            }],
			toolbar: [{name:$("#save").val()}, {name:$("#copy_data").val(),template:'#=copy()#'},{name: "prtD",template:'#=prtD()#'},{name: "prtN",template:'#=prtN()#'}],
			height:getElSize(1650),
			editable:true,
			filterable: {
                extra: false
            },
            /* edit: function (e) {
            	var countchk=0;
            	var gridlist=grid.dataSource.data();
            	var input = e.container.find(".k-input");
				var value = input.val();
						
				$(grid.tbody).off("change").on("change", "td", function (e) {
					var col = $(this).closest("td");
					var row = $(this).closest("tr");
					dataItem = grid.dataItem(row);
					var clone = $.extend({}, dataItem);
					var rowIdx = $("tr", grid.tbody).index(row);
					var colIdx = $("td", row).index(this);
					var colName = $('#wrapper').find('th').eq(colIdx).text()
					
 					console.log(row)
					console.log("행 :"+rowIdx)
					console.log("열 :"+colIdx)
					console.log("이름 :"+colName)
					console.log("값 :"+clone.name) 
					console.log("thisthisthis")
//					selectedTime = this.value();
//					console.log(selectedTime)
					console.log(value)
					console.log(dataItem.startTimeD);
					console.log(dataItem.tgRunTimeD);
					for(i=0;i<gridlist.length;i++){
						if(gridlist[i].name==clone.name){
							gridlist[i].set("tgRunTimeD",dataItem.tgRunTimeD)//가동시간
	            			gridlist[i].set("tgRunTimeN",dataItem.tgRunTimeN)//가동시간야간

	            			if(clone.prdNo!=gridlist[i].prdNo){
		            			gridlist[i].set("empD",dataItem.empD)//주간작업자
		            			gridlist[i].set("empN",dataItem.empN)//야간 작업자
	            			}
	            			gridlist[i].set("cntPerCyl",dataItem.cntPerCyl)//사이클당생산량
	            			
	            			countchk++;
						}
					}
					
					if(countchk>=2){//느려서 같은 데이터가 있어 패치되어야할곳만 적용
						console.log("---fetch---")
//						 grid.refresh();
	            	} 
//					if(clone.prdNo == kendolist[idx].prdNo )
					
				});
            }, */
            cellClose:function(e){
            	var gridlist=grid.dataSource.data();
				var countchk=0;
				var startchk=0;
				var startchkN=0;
				var cuTime=new Date();
				console.log(e.container[0].cellIndex)
				
				/* alert(e.model.startTimeD);
				alert(cuTime);
				if($("#date").val()==getToday().substr(0,10) && (e.model.startTimeD<=cuTime)==true){
					e.model.startTimeD=currentStart
					e.model.endTimeD=currentEnd
					alert("현재이전의 시간을 선택할수 없습니다.")
    				return;
				} */
				
				//입력 표준시간을 그냥 기본형식으로 변경
            	if(e.model.startTimeD.length==undefined){
                	var hour = e.model.startTimeD.getHours();
           			var minute = e.model.startTimeD.getMinutes();
           			e.model.startTimeD = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
            	}
				
            	if(e.model.startTimeN.length==undefined){
                	var hour = e.model.startTimeN.getHours();
           			var minute = e.model.startTimeN.getMinutes();
           			e.model.startTimeN = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
            	}
				
            	//인풋 들어간곳이 시작,종료 컬럼일때 경고창 발생	cellIndex=11 주간시작시간 12=주간 종료시간
            	if(e.container[0].cellIndex==6 && e.model.endTimeD!=currentEnd){
					e.model.endTimeD=currentEnd
					alert("종료시간을 조정할수없습니다.(시작시간을 조정해 주세요)")
					return;
				}
				//cellIndex=20 야간 종료시간            	
            	if(e.container[0].cellIndex==14 && e.model.endTimeN!=currentEndN){
					e.model.endTimeN=currentEndN
					alert("종료시간을 조정할수없습니다.(시작시간을 조정해 주세요)")
					return;
				}
            	
            	/* 498~632 주간 작업자 관련로직 */
				if(e.container[0].cellIndex==5 || e.container[0].cellIndex==6){
					
	            	var expArr=-1;	//자기 배열은 제외 하기위해 카운트로 사용
	            	
	            	var endchk=0;
	            	var columns = grid.options.columns;
	            	var colName = $('#wrapper').find('th').eq(e.container[0].cellIndex).text()
	            	var explist=[]
					
	            	//시작,종료시간이 변동이없으면 바로 빠져나가기(느려짐방지)
					if(e.model.startTimeD==currentStart && e.model.endTimeD==currentEnd){
						return false;
					}
	            	
	            	if(e.model.startTimeD>=e.model.endTimeD){
	            		e.model.startTimeD=currentStart
	            		alert("종료시간를 넘어설수 없습니다.")
	            		return;
	            	}
					if(currentStart=="08:30" && currentEnd=="20:30"){
						e.model.startTimeD=currentStart
						e.model.endTimeD=currentEnd
        				alert("기본작업 시간은 수정할수 없습니다")
        				return;
        			}
				
					for(j=0; j<gridlist.length; j++){
						
						
						
						
						
						if(e.model.name==gridlist[j].name && currentStart == gridlist[j].endTimeD && currentStart == gridlist[j].endTimeD && gridlist[j].startTimeD != e.model.startTimeD && gridlist[j].startTimeD!="20:30"){
							if(e.model.startTimeD<=gridlist[j].startTimeD){
								e.model.startTimeD=currentStart
								alert("기존 작업시간보다 빠를수 없습니다.")
								return;
							}
						}
						//시작시작과 같은장비의 종료시간이 같을경우
						if(e.model.name==gridlist[j].name && currentStart == gridlist[j].endTimeD && e.model.startTimeD >= gridlist[j].startTimeD){
							explist.push(j)
							//e.model.startTimeD=currentStart
							//alert("같으면안됌ㅋ")
						}
						
					}
					
	            	
	            	for(i=0;i<gridlist.length;i++){
	            		
	            		if((currentStart=="08:30" && currentEnd!="20:30") && (currentStart!=e.model.startTimeD)){
							e.model.startTimeD=currentStart
							alert("시작시간을 조정할수 없습니다.")
							return;
						}
						
						if(currentStart==gridlist[i].endTiemdD/* (currentStart!="08:30" && currentEnd=="20:30") */ /* && (currentEnd!=e.model.endTimeD) */){
							/* console.log("---종료---");
							console.log("기존 시작,종료시간 :"+currentStart + " , " + currentEnd)
							console.log("변경 시작,종료시간 :"+e.model.startTimeD + " , " + e.model.endTimeD) */
							e.model.endTimeD=currentEnd
							alert("종료시간을 조정할수 없습니다.")
							return;
						}

	            		//장비가 같을때 변경
	            		if(e.model.name==gridlist[i].name){
	            			
	            			gridlist[i].set("tgRunTimeD",e.model.tgRunTimeD)//가동시간
	            			gridlist[i].set("tgRunTimeN",e.model.tgRunTimeN)//가동시간야간
	            			gridlist[i].set("cntPerCyl",e.model.cntPerCyl)//사이클당생산량
	            			
	            			if(e.model.prdNo!=gridlist[i].prdNo){
	            				
	            				if(currentStart==gridlist[i].endTimeD && gridlist[i].startTimeD!="20:30" ){
	            					gridlist[i].set("endTimeD",e.model.startTimeD)
	            				}
	            				
	            				//차종과 작업시간이 같을때
	            				console.log(startchk)
	            				if(currentStart==gridlist[i].startTimeD){
	            					if(startchk==0){
	            						gridlist[i].set("empD",e.model.empD)//주간작업자
			                			gridlist[i].set("empN",e.model.empN)//야간 작업자
											
		                				gridlist[i].set("startTimeD",e.model.startTimeD)
		                				startchk++;
			                				/* console.log("--같음--")
			                				console.log(gridlist[i].startTimeD + " , " + gridlist[i].endTimeD)

				            				console.log("비교 대상 :" + e.model.startTimeD + " , " + gridlist[i].startTimeD)
				            				console.log(e.model.startTimeD==gridlist[i].startTimeD) */
	            					}
	            				}
	                			
	            			}
	            			
	            			countchk++;
	            			
	            			
	            			//차종과 작업시간도 같을때(시작시간 변경할때 종료시간 같은것도 같이 변경하는 로직)
	            			if(e.model.prdNo==gridlist[i].prdNo && currentStart == gridlist[i].endTimeD){
	            				if(currentStart==gridlist[i].endTimeD && gridlist[i].startTimeD!="20:30" ){
	            					console.log("확실")
		            				if( e.model.startTimeD!=gridlist[i].startTimeD){
		            					gridlist[i].set("endTimeD",e.model.startTimeD)
		            				}
	            				}
	            			}
	            		}
	            		
	            		
	            	}
					//시작시작과 같은장비의 종료시간이 같을경우
					if(explist.length==1){
						explist=explist[0]
					}else{
						if(gridlist[explist[0]].startTimeD<=gridlist[explist[1]].startTimeD){
							explist=explist[0]
						}else{
							explist=explist[1]
						}
					}
					if(e.model.startTimeD<=gridlist[explist].startTimeD){
						e.model.startTimeD=currentStart
						alert("기존의 종료시간과 시작시간이 같을수 없습니다.")
						return;
					}
				}
				
				/* 632~752 야간 작업자 관련로직 */
//				console.log(e.container[0].cellIndex)

				if((e.container[0].cellIndex==13 || e.container[0].cellIndex==14)){
					
//	            	var expArr=-1;	//자기 배열은 제외 하기위해 카운트로 사용
	            	
	            	var endchkN=0;
//	            	var columnsN = grid.options.columns;
//	            	var colName = $('#wrapper').find('th').eq(e.container[0].cellIndex).text()
	            	var explistN=[]
	            	var timedefault=0; //1이면 트루
	            	//시작,종료시간이 변동이없으면 바로 빠져나가기(느려짐)
					if(e.model.startTimeN==currentStartN && e.model.endTimeN==currentEndN){
						return false;
					}
	            	
	            	
           			
					
					
					var timeArr;
	            	//console.log("e.model.startTimeN 변경 전 :" + e.model.startTimeN)
	            	if(e.model.startTimeN<"20:30"){
	            		timeArr=e.model.startTimeN.split(":");
	            		timeArr[0]=Number(timeArr[0])+30
	            		e.model.startTimeN=timeArr[0]+":"+timeArr[1]
	            	}
	            	console.log("변경 후 :" + e.model.startTimeN)
	            	
	            	
	            	//console.log("e.model.endTimeN 변경 전 :" + e.model.endTimeN)
	            	if(e.model.endTimeN<"20:30"){
	            		timeArr=e.model.endTimeN.split(":");
	            		timeArr[0]=Number(timeArr[0])+30
	            		e.model.endTimeN=timeArr[0]+":"+timeArr[1]
	            	}
	            	//console.log("변경 후 :" + e.model.endTimeN)
	            	
	            	//onsole.log("currentStartN 변경 전 :" + currentStartN)
	            	if(currentStartN<"20:30"){
	            		timeArr=currentStartN.split(":");
	            		timeArr[0]=Number(timeArr[0])+30
	            		currentStartN=timeArr[0]+":"+timeArr[1]
	            	}
	            	//console.log("변경 후 :" + currentStartN)
	            	
	            	//console.log("currentEndN 변경 전 :" + currentEndN)
	            	if(currentEndN<"20:30"){
	            		timeArr=currentEndN.split(":");
	            		timeArr[0]=Number(timeArr[0])+30
	            		currentEndN=timeArr[0]+":"+timeArr[1]
	            	}
	            	//console.log("변경 후 :" + currentEndN)
	            	
	            	if(e.model.startTimeN>=e.model.endTimeN){
	            		console.log(e.model.startTimeN + " , " + e.model.endTimeN)
						console.log(e.model.startTimeN<=e.model.endTimeN)
						/////////////////////////////////////////////////////////////////////
		            	
						if(e.model.startTimeN>="30:30"){
		            		timeArr=e.model.startTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(e.model.endTimeN>="30:30"){
		            		timeArr=e.model.endTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(currentStartN >="30:30"){
		            		timeArr=currentStartN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(currentEndN >="30:30"){
		            		timeArr=currentEndN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
		            	}
						///////////////////////////////////////////////////////////////////////
	            		e.model.startTimeN=currentStartN
	            		alert("종료시간를 넘어설수 없습니다.")
	            		return;
	            	}
	            	
					if(currentStartN=="20:30" && currentEndN=="38:30"){
    					//////////////////////////////////////////////////////////////////////
						if(currentStartN >="30:30"){
		            		timeArr=currentStartN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(currentEndN >="30:30"){
		            		timeArr=currentEndN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					//////////////////////////////////////////////////////////////////////
						e.model.startTimeN=currentStartN
						e.model.endTimeN=currentEndN
        				alert("기본작업 시간은 수정할수 없습니다")
        				return;
        			}
					
					for(j=0; j<gridlist.length; j++){
						
						if(gridlist[j].startTimeN<"20:30"){
		            		timeArr=gridlist[j].startTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])+30
		            		gridlist[j].startTimeN=timeArr[0]+":"+timeArr[1]
		            	}
		            	if(gridlist[j].endTimeN<"20:30"){
		            		timeArr=gridlist[j].endTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])+30
		            		gridlist[j].endTimeN=timeArr[0]+":"+timeArr[1]
		            	}
		            	
		            	
						if(e.model.name==gridlist[j].name && currentStartN == gridlist[j].endTimeN && currentStartN == gridlist[j].endTimeN && gridlist[j].startTimeN != e.model.startTimeN && gridlist[j].startTimeN!="38:30"){
							
							console.log(j)
							console.log(gridlist[j])
			            	if(j!=0){
			            	
				            	if(gridlist[j-1].startTimeN=="38:30"){
				            		if(gridlist[j-1].startTimeN>="30:30"){	// j-1 함
					           			timeArr=gridlist[j-1].startTimeN.split(":");
					            		timeArr[0]=Number(timeArr[0])-30
					            		gridlist[j-1].startTimeN="0"+timeArr[0]+":"+timeArr[1]
					            	}
					            	if(gridlist[j-1].endTimeN>="30:30"){
					            		timeArr=gridlist[j-1].endTimeN.split(":");
					            		timeArr[0]=Number(timeArr[0])-30
					            		gridlist[j-1].endTimeN="0"+timeArr[0]+":"+timeArr[1]
					            	}
					            	if(gridlist[j].startTimeN>="30:30"){	// j-1 함
					            		timeArr=gridlist[j].startTimeN.split(":");
					            		timeArr[0]=Number(timeArr[0])-30
					            		gridlist[j].startTimeN="0"+timeArr[0]+":"+timeArr[1]
					            	}
					            	if(gridlist[j].endTimeN>="30:30"){
					            		timeArr=gridlist[j].endTimeN.split(":");
					            		timeArr[0]=Number(timeArr[0])-30
					            		gridlist[j].endTimeN="0"+timeArr[0]+":"+timeArr[1]
					            	}
									if(e.model.startTimeN>="30:30"){
	        		            		timeArr=e.model.startTimeN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					if(e.model.endTimeN>="30:30"){
	        		            		timeArr=e.model.endTimeN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					if(currentStartN >="30:30"){
	        		            		timeArr=currentStartN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					if(currentEndN >="30:30"){
	        		            		timeArr=currentEndN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					
	            					///////////////////////////////////////////////////
									e.model.startTimeN=currentStartN
				            		alert("이전 작업 먼저 시간을 선택하세요")
				            		return;
				            	}
			            	
			            	}
			            	if(e.model.startTimeN<"20:30"){
			            		timeArr=e.model.startTimeN.split(":");
			            		timeArr[0]=Number(timeArr[0])+30
			            		e.model.startTimeN=timeArr[0]+":"+timeArr[1]
			            	}
							
							if(e.model.startTimeN<=gridlist[j].startTimeN){
								///////////////////////////////////////////////////
								
								if(gridlist[j].startTimeN>="30:30"){//
				            		timeArr=gridlist[j].startTimeN.split(":");
				            		timeArr[0]=Number(timeArr[0])-30
				            		gridlist[j].startTimeN="0"+timeArr[0]+":"+timeArr[1]
				            	}
				            	if(gridlist[j].endTimeN>="30:30"){
				            		timeArr=gridlist[j].endTimeN.split(":");
				            		timeArr[0]=Number(timeArr[0])-30
				            		gridlist[j].endTimeN="0"+timeArr[0]+":"+timeArr[1]
				            	}
				            	
								if(e.model.startTimeN>="30:30"){
        		            		timeArr=e.model.startTimeN.split(":");
        		            		timeArr[0]=Number(timeArr[0])-30
        		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
        		            	}
            					if(e.model.endTimeN>="30:30"){
        		            		timeArr=e.model.endTimeN.split(":");
        		            		timeArr[0]=Number(timeArr[0])-30
        		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
        		            	}
            					if(currentStartN >="30:30"){
        		            		timeArr=currentStartN.split(":");
        		            		timeArr[0]=Number(timeArr[0])-30
        		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
        		            	}
            					if(currentEndN >="30:30"){
        		            		timeArr=currentEndN.split(":");
        		            		timeArr[0]=Number(timeArr[0])-30
        		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
        		            	}
            					
            					///////////////////////////////////////////////////
								e.model.startTimeN=currentStartN
								alert("기존 작업시간보다 빠를수 없습니다.11")
								return;
            					
							}
						}
						//시작시작과 같은장비의 종료시간이 같을경우
						if(e.model.name==gridlist[j].name && currentStartN == gridlist[j].endTimeN && e.model.startTimeN >= gridlist[j].startTimeN){
							explistN.push(j)
							//e.model.startTimeN=currentStartN
							//alert("같으면안됌ㅋ")
						}
						if(explistN.length==0){
							if(gridlist[j].startTimeN>="30:30"){//
			            		timeArr=gridlist[j].startTimeN.split(":");
			            		timeArr[0]=Number(timeArr[0])-30
			            		gridlist[j].startTimeN="0"+timeArr[0]+":"+timeArr[1]
			            	}
			            	if(gridlist[j].endTimeN>="30:30"){
			            		timeArr=gridlist[j].endTimeN.split(":");
			            		timeArr[0]=Number(timeArr[0])-30
			            		gridlist[j].endTimeN="0"+timeArr[0]+":"+timeArr[1]
			            	}
							if(currentStartN=="20:30"){
								if(e.model.startTimeN>="30:30"){
	    		            		timeArr=e.model.startTimeN.split(":");
	    		            		timeArr[0]=Number(timeArr[0])-30
	    		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
	    		            	}
	        					if(e.model.endTimeN>="30:30"){
	    		            		timeArr=e.model.endTimeN.split(":");
	    		            		timeArr[0]=Number(timeArr[0])-30
	    		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
	    		            	}
	        					if(currentStartN >="30:30"){
	    		            		timeArr=currentStartN.split(":");
	    		            		timeArr[0]=Number(timeArr[0])-30
	    		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
	    		            	}
	        					if(currentEndN >="30:30"){
	    		            		timeArr=currentEndN.split(":");
	    		            		timeArr[0]=Number(timeArr[0])-30
	    		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
	    		            	}
	        					
								alert("최초 시작시간을 조정할수 없습니다.")
								e.model.startTimeN=currentStartN
								return false;
							}
						}
						
					}
					
					console.log(explistN)
					//시작시작과 같은장비의 종료시간이 같을경우
					if(explistN.length==1){
						explistN=explistN[0]
					}else{
						if(gridlist[explistN[0]].startTimeN<=gridlist[explistN[1]].startTimeN){
							explistN=explistN[0]
						}else{
							explistN=explistN[1]
						}
					}
					
					if(e.model.startTimeN<=gridlist[explistN].startTimeN){
						console.log(e.model.startTimeN + " , " + gridlist[explistN].startTimeN)
						
						//////////////////////////////////////////////////////////////////////
						
						if(e.model.startTimeN>="30:30"){
		            		timeArr=e.model.startTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(e.model.endTimeN>="30:30"){
		            		timeArr=e.model.endTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(currentStartN >="30:30"){
		            		timeArr=currentStartN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					if(currentEndN >="30:30"){
		            		timeArr=currentEndN.split(":");
		            		timeArr[0]=Number(timeArr[0])-30
		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
		            	}
    					
    					/////////////////////////////////////////////////////////////////////////
						
						e.model.startTimeN=currentStartN
						timedefault=1;
//						return;
					}
	            	
					if(timedefault==1){
						for(j=0;j<gridlist.length;j++){
							if(gridlist[j].startTimeN>="30:30"){//
			            		timeArr=gridlist[j].startTimeN.split(":");
			            		timeArr[0]=Number(timeArr[0])-30
			            		gridlist[j].startTimeN="0"+timeArr[0]+":"+timeArr[1]
			            	}
			            	if(gridlist[j].endTimeN>="30:30"){
			            		timeArr=gridlist[j].endTimeN.split(":");
			            		timeArr[0]=Number(timeArr[0])-30
			            		gridlist[j].endTimeN="0"+timeArr[0]+":"+timeArr[1]
			            	}
						}
						alert("기존의 종료시간과 시작시간이 같을수 없습니다.1c")
						return;
					}
	            	for(i=0;i<gridlist.length;i++){
	            		if(gridlist[i].startTimeN<"20:30"){
		            		timeArr=gridlist[i].startTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])+30
		            		gridlist[i].startTimeN=timeArr[0]+":"+timeArr[1]
		            	}
		            	if(gridlist[i].endTimeN<"20:30"){
		            		timeArr=gridlist[i].endTimeN.split(":");
		            		timeArr[0]=Number(timeArr[0])+30
		            		gridlist[i].endTimeN=timeArr[0]+":"+timeArr[1]
		            	}
	            		if((currentStartN=="20:30" && currentEndN!="38:30") && (currentStartN!=e.model.startTimeN)){
	            			
	            			///////////////////////////////////////////////////
	            			
	            			if(e.model.startTimeN>="30:30"){
    		            		timeArr=e.model.startTimeN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					if(e.model.endTimeN>="30:30"){
    		            		timeArr=e.model.endTimeN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					if(currentStartN >="30:30"){
    		            		timeArr=currentStartN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					if(currentEndN >="30:30"){
    		            		timeArr=currentEndN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
    		            	}	            			
	            			
	            			////////////////////////////////////////////////////
							e.model.startTimeN=currentStartN
							alert("시작시간을 조정할수 없습니다.")
							return;
						}
						
						if(currentStartN==gridlist[i].endTiemdN){
							
							/////////////////////////////////////////////////////
							if(e.model.startTimeN>="30:30"){
    		            		timeArr=e.model.startTimeN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					if(e.model.endTimeN>="30:30"){
    		            		timeArr=e.model.endTimeN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					if(currentStartN >="30:30"){
    		            		timeArr=currentStartN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					if(currentEndN >="30:30"){
    		            		timeArr=currentEndN.split(":");
    		            		timeArr[0]=Number(timeArr[0])-30
    		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
    		            	}
        					/////////////////////////////////////////////////////
        					
							//console.log("---종료---");
							//console.log("기존 시작,종료시간 :"+currentStartN + " , " + currentEndN)
							//console.log("변경 시작,종료시간 :"+e.model.startTimeD + " , " + e.model.endTimeD)
							e.model.endTimeN=currentEndN
							alert("종료시간을 조정할수 없습니다.")
							return;
						}

	            		//장비가 같을때 변경
	            		if(e.model.name==gridlist[i].name){
	            			
	            			if(e.model.prdNo!=gridlist[i].prdNo){
	            				
	            				if(currentStartN==gridlist[i].endTimeN && gridlist[i].startTimeN!="38:30" ){
	            					
	            					/* //////////////////////////////////////////////////////
	            					if(e.model.startTimeN>="30:30"){
	        		            		timeArr=e.model.startTimeN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					if(e.model.endTimeN>="30:30"){
	        		            		timeArr=e.model.endTimeN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					if(currentStartN >="30:30"){
	        		            		timeArr=currentStartN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					if(currentEndN >="30:30"){
	        		            		timeArr=currentEndN.split(":");
	        		            		timeArr[0]=Number(timeArr[0])-30
	        		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
	        		            	}
	            					////////////////////////////////////////////////////// */
	            					
	            					console.log(1111)
	            					gridlist[i].set("endTimeN",e.model.startTimeN)
	            				}
	            				
	            				//차종과 작업시간이 같을때
	            				console.log(startchk)
	            				if(currentStartN==gridlist[i].startTimeN){
	            					if(startchk==0){
	            						
	            						/* ////////////////////////////////////////////////////
	            						if(e.model.startTimeN>="30:30"){
	            		            		timeArr=e.model.startTimeN.split(":");
	            		            		timeArr[0]=Number(timeArr[0])-30
	            		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
	            		            	}
	                					if(e.model.endTimeN>="30:30"){
	            		            		timeArr=e.model.endTimeN.split(":");
	            		            		timeArr[0]=Number(timeArr[0])-30
	            		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
	            		            	}
	                					if(currentStartN >="30:30"){
	            		            		timeArr=currentStartN.split(":");
	            		            		timeArr[0]=Number(timeArr[0])-30
	            		            		currentStartN="0"+timeArr[0]+":"+timeArr[1]
	            		            	}
	                					if(currentEndN >="30:30"){
	            		            		timeArr=currentEndN.split(":");
	            		            		timeArr[0]=Number(timeArr[0])-30
	            		            		currentEndN="0"+timeArr[0]+":"+timeArr[1]
	            		            	}
		            					///////////////////////////////////////////////////// */
		            					
	            						if(e.model.startTimeN>="30:30"){
		        		            		timeArr=e.model.startTimeN.split(":");
		        		            		timeArr[0]=Number(timeArr[0])-30
		        		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
		        		            	}
		            					console.log(2222)
		                				gridlist[i].set("startTimeN",e.model.startTimeN)
		                				startchk++;
			                				//console.log("--같음--")
			                				//console.log(gridlist[i].startTimeD + " , " + gridlist[i].endTimeD)

				            				//console.log("비교 대상 :" + e.model.startTimeD + " , " + gridlist[i].startTimeD)
				            				//console.log(e.model.startTimeD==gridlist[i].startTimeD)
	            					}
	            				}
	                			
	            			}
	            			
	            			countchk++;
	            			
	            			
	            			//차종과 작업시간도 같을때(시작시간 변경할때 종료시간 같은것도 같이 변경하는 로직)
	            			if(e.model.prdNo==gridlist[i].prdNo && currentStartN == gridlist[i].endTimeN){
	            				console.log(e.model.prdNo + " , " + currentStartN + " , " + gridlist[i].endTimeN )
	            				console.log(e.model.startTimeN + " , " + gridlist[i].startTimeN) 
	            				console.log(4444444444)
	            				if(currentStartN==gridlist[i].endTimeN && gridlist[i].startTimeN!="38:30" && e.model.startTimeN!=gridlist[i].startTimeN){
		            					console.log(33333)
		            					////////////////////////////////////////////////
		            					if(e.model.startTimeN>="30:30"){
		        		            		timeArr=e.model.startTimeN.split(":");
		        		            		timeArr[0]=Number(timeArr[0])-30
		        		            		e.model.startTimeN="0"+timeArr[0]+":"+timeArr[1]
		        		            	}
		            					if(e.model.endTimeN>="30:30"){
		        		            		timeArr=e.model.endTimeN.split(":");
		        		            		timeArr[0]=Number(timeArr[0])-30
		        		            		e.model.endTimeN="0"+timeArr[0]+":"+timeArr[1]
		        		            	}
		            					////////////////////////////////////////////////
		            					gridlist[i].set("endTimeN",e.model.startTimeN)
	            				}
	            			}
	            		}
	            		
	            		
	            	}
	            	
				}
				
				/* 목표수량,사이클생산량,가동시간,작업자 변경 로직 */
				for(i=0; i<gridlist.length; i++){
					
					////////////////////////////////////
					if(gridlist[i].startTimeN>="30:30"){
	            		timeArr=gridlist[i].startTimeN.split(":");
	            		timeArr[0]=Number(timeArr[0])-30
	            		gridlist[i].startTimeN="0"+timeArr[0]+":"+timeArr[1]
	            	}
					if(gridlist[i].endTimeN>="30:30"){
	            		timeArr=gridlist[i].endTimeN.split(":");
	            		timeArr[0]=Number(timeArr[0])-30
	            		gridlist[i].endTimeN="0"+timeArr[0]+":"+timeArr[1]
	            	}
					////////////////////////////////////
					
            		//장비가 같을때 변경
            		if(e.model.name==gridlist[i].name){
            			gridlist[i].set("tgRunTimeD",e.model.tgRunTimeD)//가동시간
            			gridlist[i].set("tgRunTimeN",e.model.tgRunTimeN)//가동시간야간
            			gridlist[i].set("tgCntD",e.model.tgCntD)//가동시간야간
            			gridlist[i].set("tgCntN",e.model.tgCntN)//가동시간야간
            			gridlist[i].set("cntPerCyl",e.model.cntPerCyl)//사이클당생산량
            			
            			if(e.model.prdNo!=gridlist[i].prdNo){
            				console.log("?")
            				//차종과 작업시간이 같을때
            				if(currentStart==gridlist[i].startTimeD){
            					if(startchk==0){
            						gridlist[i].set("empD",e.model.empD)//주간작업자
		                			gridlist[i].set("empN",e.model.empN)//야간 작업자
	                				startchk++;
            					}
            				}else if(e.model.startTimeD==gridlist[i].startTimeD){
            					gridlist[i].set("empD",e.model.empD)//주간작업자
	                			gridlist[i].set("empN",e.model.empN)//야간 작업자
	                			gridlist[i].set("tgCntD",e.model.tgCntD)
            				}

            			}
            			countchk++;
            		}
            	}
            	/* if(countchk>=2){//느려서 같은 데이터가 있어 패치되어야할곳만 적용
            		grid.dataSource.sort([
    					{ field: "prdNo", dir: "asc" },{field:"startTimeD",dir:"asc"},{field:"endTimeD",dir:"asc"},{field:"startTimeN",dir:"asc"}
    				])
    				
//            		grid.refresh();
            	} */
            	
            	
            }, 
            groupable: false,
            dataBound: onDataBound,
			columns:[{
					field:"dvcId",title:"　" ,width : getElSize(120)
					,locked: true
					,lockable: false
					, attributes: {
        				style: "text-align: center; font-size:" + getElSize(40)
        			}, headerAttributes: {
        				style: "text-align: center; font-size:" + getElSize(40)
        			}
			
				},{
					field:"name",title:"${device}",groupHeaderTemplate: '　',width : getElSize(500)
 					,locked: true
					,lockable: false
					, attributes: {
        				style: "text-align: center; font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center; font-size:" + getElSize(45)
        			},filterable:{
        				 multi: true,
        				 click: function(e) {
        					console.log("--wkdql click--") 
        				 },
        	             search: true
        			}
				}/* ,
				{
					field:"prdNo",title:"${prdct_machine_line}", width:getElSize(680)
 					,locked: true
					,lockable: false
					, attributes: {
        				style: "text-align: center; font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center; font-size:" + getElSize(45)
        			},filterable: false
				} */,{
					field:"cntPerCyl",title:"${prdct_per_cycle}",width : getElSize(300)
 					,locked: true
					,lockable: false
					, attributes: {
        				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center;  font-size:" + getElSize(30)
        			},filterable: false
				},{
					field :"rowClass" , width : getElSize(1)	
					,locked: true
					,lockable: false
				},{
					field:"copyDateColor",width : getElSize(1)
					,locked: true
					,lockable: false
				} ,{
				title:"${day}",attributes: {
        				style: "text-align: center;  font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center;  font-size:" + getElSize(40)
        			},
				columns:[{
					field:"tgCntD",title:"${target_prdct_cnt}",width : getElSize(270), attributes: {
	        				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
            			},headerAttributes: {
            				style: "text-align: center;  font-size:" + getElSize(40)
            			},filterable: false
				},{
					field:"uph",title:"UPH" , width:getElSize(150) ,attributes: {
        				style: "text-align: center; ; font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center; ; font-size:" + getElSize(40)
        			},filterable: false, width:getElSize(150)
				},{
					field:"capa",title:"${working_capa}",  width:getElSize(150) ,attributes: {
						style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center; ; font-size:" + getElSize(40)
        			},filterable: false, width:getElSize(150)
				},{
					field:"tgRunTimeD",title:"${ophour}"+"(h)",  width:getElSize(280) ,attributes: {
            				
            			},headerAttributes: {
            				style: "text-align: center;  font-size:" + getElSize(40)
            			},filterable: false
				},{
					field:"empD",title:"${worker}", editor: workauto,template:"#=worKerName(empD)#",  width:getElSize(250) ,attributes: {
        				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center;  font-size:" + getElSize(40)
        			},filterable: {
                        ui: nameFilter,
                        cell: {
                            showOperators: false,
                            operator: "contains"
                        }
                    }
				},{
					
					
					field: "startTimeD",
					title: "${start}${time_}",
					width : getElSize(220),
					format:"{0:HH:mm}",
					editor : testTimeD,
					filterable: false,
					attributes: {
        				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center;  font-size:" + getElSize(40)
        			}

					
					
				},{
					
					
					field: "endTimeD",	
					title: "${end}${time_}",
					width : getElSize(220),
					editor : testTimeD,
					format:"{0:HH:mm}",
					filterable: false,
					attributes: {
        				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
        			},headerAttributes: {
        				style: "text-align: center;  font-size:" + getElSize(40)
        			}
					
					
					
				},{
					
					
					
					
					title:"${Split_work}",
					width : getElSize(550),
					template : "<button class='actionBtn' onclick='insertRow(this)'>${divide}</button> <button class='actionBtn' onclick='deleteRow(this)'>${del}</button>",
					attributes: {
        				style: "text-align: center;  font-size:" + getElSize(38) 
        			},headerAttributes: {
        				style: "text-align: center;  font-size:" + getElSize(40)
        			}
					
					
					
					
				}]},
				{
					title:"${night}",attributes: {
            				style: "text-align: center; ; font-size:" + getElSize(38)
            			},headerAttributes: {
            				style: "text-align: center; ; font-size:" + getElSize(40)
            			},
					columns:[{
						field:"tgCntN",title:"${target_prdct_cnt}",  width:getElSize(270) ,attributes: {
            				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
            			},headerAttributes: {
            				style: "text-align: center; ; font-size:" + getElSize(40)
            			},filterable: false
						},{
							field:"uph",title:"UPH",  width:getElSize(150) ,attributes: {
	            				style: "text-align: center; ; font-size:" + getElSize(38)
	            			},headerAttributes: {
	            				style: "text-align: center; ; font-size:" + getElSize(40)
	            			},filterable: false, width:getElSize(150)
						},{
							field:"capa",title:"${working_capa}",  width:getElSize(150) ,attributes: {
	            				style: "text-align: center; ; font-size:" + getElSize(38)
	            			},headerAttributes: {
	            				style: "text-align: center; ; font-size:" + getElSize(40)
	            			},filterable: false, width:getElSize(150)
						},{
							field:"tgRunTimeN",title:"${ophour}"+"(h)",  width:getElSize(280) ,attributes: {
	            				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
	            			},headerAttributes: {
	            				style: "text-align: center; ; font-size:" + getElSize(40)
	            			},filterable: false
						},{
						field:"empN",title:"${worker}",editor: workauto ,template:"#=worKerName(empN)#" ,  width:getElSize(250) ,attributes: {
            				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
            			},headerAttributes: {	
            				style: "text-align: center; ; font-size:" + getElSize(40)
            			},filterable: {
                            ui: nameFilter
                        }
					},{
						
						
						field: "startTimeN",
						title: "${start}${time_}",
						width : getElSize(220),
						format:"{0:HH:mm}",
						editor : testTimeN,
						filterable: false,
						attributes: {
            				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
	        			},headerAttributes: {
	        				style: "text-align: center;  font-size:" + getElSize(40)
	        			}

						
						
					},{
						
						
						field: "endTimeN",
						title: "${end}${time_}",
						width : getElSize(220),
						editor : testTimeN,
						format:"{0:HH:mm}",
						filterable: false,
						attributes: {
            				style: "text-align: center; color:black; background:rgb(210,210,210); font-size:" + getElSize(38)
	        			},headerAttributes: {
	        				style: "text-align: center;  font-size:" + getElSize(40)
	        			}
						
						
						
					},{
						
						
						
						
						title:"${Split_work}",
						width : getElSize(550),
						template : "<button class='actionBtn' onclick='insertRow(this)'>${divide}</button> <button class='actionBtn' onclick='deleteRow(this)'>${del}</button>",
						attributes: {
	        				style: "text-align: center;  font-size:" + getElSize(38) 
	        			},headerAttributes: {
	        				style: "text-align: center;  font-size:" + getElSize(40)
	        			}
						
						
						
						
					}]
				},{
					field:"copyDateColor",width : getElSize(1)
				},{
					field :"rowClass" , width : getElSize(1)		
				}],
            filterMenuInit: function(e) {
            	
                
                if(e.field == "dvcId"){
                	e.container.css({"height": getElSize(300) + "px"});
                    e.container.css({"width": getElSize(600) + "px"});
                }
                
                if(e.field == "empD"){
                	e.container.css({"height": getElSize(300) + "px"});
                    e.container.css({"width": getElSize(600) + "px"});
                }
                
                if(e.field == "empN"){
                	e.container.css({"height": getElSize(300) + "px"});
                    e.container.css({"width": getElSize(600) + "px"});
                }
                
                /* if(e.field == "name"){
                	e.container.css({"height": getElSize(700) + "px"});
                } */
                
             // 타이틀 "작업자 클릭"
        		/* $(".k-list-container").css({
        			"height" : getElSize(1000) + "px"
        		});
        		
        		$(".k-list-scroller").css({
        			"height" : getElSize(1000) + "px"
        		}); */
        		
        		// 첫번째 타이틀
        		$(".k-animation-container").css({
        			"width" : getElSize(700) + "px"
        		});
        		
        		$(".k-filter-menu").css({
        			"font-size" : getElSize(45) + "px"
        		});
                
                console.log("TTTTTT")
            }
			}).data("kendoGrid");
			var grid = $("#wrapper").data("kendoGrid");
		
			/*  */
			
			console.log("jane line");
		});
	
	function deleteRow(e){
		
		if(confirm("삭제하시겠습니까?")==false){
			return false;
		}
		
		$('.actionBtn').attr('disabled', true);
		
		setTimeout(function() {
			var row = $(e).closest("tr");
			var rowIdx = $("tr", grid.tbody).index(row);
			var dataItem = $(e).closest("tr")[0].dataset.uid;
				dataItem = gridlist.dataItem(row);
			var check = 0;
			var length = grid.dataSource.data().length	
			var table= grid.dataSource.data();
			var removelist=[];
			var a="";
			console.log(dataItem.endTimeD);
			console.log()
			
			for(var i=0;i<length;i++){			 
				if(grid.dataSource.data()[i].dvcId==dataItem.dvcId && grid.dataSource.data()[i].prdNo == dataItem.prdNo){
					 check++;
				}
				
				if(table[i].endTimeD==dataItem.startTimeD && table[i].name==dataItem.name && table[i].endTimeN==dataItem.startTimeN ){
//					console.log(table[i])
					table[i].set("endTimeD",dataItem.endTimeD)
					table[i].set("endTimeN",dataItem.endTimeN)
				}
				
				if(table[i].name==dataItem.name && table[i].prdNo!=dataItem.prdNo && table[i].startTimeD==dataItem.startTimeD && table[i].startTimeN==dataItem.startTimeN){
					removelist.push(i)
				}
			}
			
			if(check<=1 || dataItem.startTimeD=="08:30"){
				alert(" 1개 일때는 지울 수 없습니다. ")
				$('.actionBtn').attr('disabled', false);
				$.hideLoading()
				return;
			}else{
				console.log("--delete--")
				console.log(removelist)
				console.log(dataItem.dvcId)
				console.log(dataItem.group)
				
				var url = "${ctxPath}/order/deleteJobSplit.do";
				var param = "dvcId=" + dataItem.dvcId + 
							"&tgDate=" + $("#date").val() +
							"&group=" + dataItem.group;
				
				$.ajax({
					url : url,
					data : param,
//					dataType : "json",
					type : "post",
					success : function(data){
						console.log("suc")
					}
				});
						
				
				
				
				
				if(removelist.length!=0){
					grid.dataSource.remove(table[removelist[0]])
				}
				grid.dataSource.remove(dataItem)
			}
			$.hideLoading()
			$('.actionBtn').attr('disabled', false);
		}, 10);
		$.showLoading()
	}

	
	function insertRow(e){
		$('.actionBtn').attr('disabled', true);
		setTimeout(function() {
	
			console.log("---insert---")
			var row = $(e).closest("tr");
			var rowIdx = $("tr", grid.tbody).index(row);
			var dataItem = $(e).closest("tr")[0].dataset.uid;
			var initData = gridlist.dataItem(row);
			var length = gridlist._data.length;
			var loopChk=0; // 1이면 빠져나감
			var list=[];
			for(i=0; i<length; i++){
				if(gridlist._data[i].name==initData.name ){
					if((gridlist._data[i].startTimeD==gridlist._data[i].endTimeD /* && gridlist._data[i].empD=="null" */) && (gridlist._data[i].prdNo==initData.prdNo) && gridlist._data[i].startTimeN==gridlist._data[i].endTimeN){
						loopChk=1;
						alert("작업시간을 선택해 주세요");
						$.hideLoading()
						$('.actionBtn').attr('disabled', false);
						return;
					}
					if(gridlist._data[i].prdNo!=initData.prdNo){
						list.push(gridlist._data[i].prdNo)
					}
				}
			}
			
			console.log(list)
			if(loopChk==1){
				console.log("return;");
				$.hideLoading()
				$('.actionBtn').attr('disabled', false);
				return false;
			}
			
			if(list.length>=1){
				grid.dataSource.insert({
					 routing: initData.routing,
					 dvcId: initData.dvcId,
					 name: initData.name,
		             tgCntD: initData.tgCntD,
		             tgRunTimeD: initData.tgRunTimeD,
		             cntPerCyl: initData.cntPerCyl,
		             tgCntN: initData.tgCntN,
		             tgRunTimeN: initData.tgRunTimeN,
		             capa: initData.capa,
		             empD: initData.empD,
		             empN: initData.empN,
		             prdNo: list[0],
		             uph: initData.uph,
		             startTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
		             endTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
		             startTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
		             endTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
		             copyDateColor:"copyDateColor"
				});
			}
			grid.dataSource.insert({
				 routing: initData.routing,
				 dvcId: initData.dvcId,
				 name: initData.name,
	             tgCntD: initData.tgCntD,
	             tgRunTimeD: initData.tgRunTimeD,
	             cntPerCyl: initData.cntPerCyl,
	             tgCntN: initData.tgCntN,
	             tgRunTimeN: initData.tgRunTimeN,
	             capa: initData.capa,
	             empD: initData.empD,
	             empN: initData.empN,
	             prdNo: initData.prdNo,
	             uph: initData.uph,
	             startTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
	             endTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
	             startTimeN:  moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
	             endTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
	             copyDateColor:"copyDateColor"
			});
			
			/* grid.dataSource.sort([
				{ field: "prdNo", dir: "asc" },{field:"startTimeD",dir:"asc"},{field:"startTimeN",dir:"asc"}
			]) */
			$('.actionBtn').attr('disabled', false);
	
			$.hideLoading()
		}, 10);
		
		$.showLoading()
		
	}

	var showDate;
	
	function getTargetData(){
		var list=[];
		var multiPrdNoList = [] ;
		
		$.showLoading()
		
		var url = "${ctxPath}/order/getTargetData.do";
		
		var param = "shopId=" + shopId + 
					"&date=" + $("#date").val();
		showDate=$("#date").val();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				$(".k-grid-저장").unbind("click")
				$(".k-header.k-grid-toolbar a:eq(1)").unbind("click")
				var  json = data.dataList;
				
				console.log(json)
				
				var class_name = "";
				
				/* var startTimeD = moment().set({'hour' : 8, 'minute' : 30}).format("HH:mm")
				var endTimeD = moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm")
				var startTimeN = moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm")
				var endTimeN = moment().set({'hour' : 08, 'minute' : 30}).add(1,'day').format("HH:mm") */

				$(json).each(function(idx, data){
					var arr=new Object();
					
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					class_name = " ";

					var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
					arr.dvcId=data.dvcId;//hidden
					arr.routing=decode(data.routing) ;
					arr.name=name;
					arr.tgCntD=data.tgCntD;
					arr.tgRunTimeD=data.tgRunTimeD/60/60;
					arr.cntPerCyl=data.cntPerCyl;
					arr.tgCntN=data.tgCntN;
					arr.tgRunTimeN=data.tgRunTimeN/60/60;
					arr.capa=data.capa;
					arr.empD=decode(data.empD);
					arr.empN=decode(data.empN);
					arr.z1=$('.worklist').eq(idx).val();
					arr.uph=data.uph;
					arr.copy="";
					arr.prdNo=decode(data.prdNo);
					arr.multiPrdNo = data.multiPrdNo
					arr.group = data.group
					if(idx%2==0){
						arr.rowClass = "evenRow";//even짝수
					}else{
						arr.rowClass = "oddRow";//홀수  
					}
					//추가작업은 빨강표시
					if(data.multiPrdNo==9999){
						arr.copyDateColor="copyDateColor";
					}
						
					if(data.startTimeD==null){
						arr.startTimeD = moment().set({'hour' : 8, 'minute' : 30}).format("HH:mm")
					}else{
						arr.startTimeD = data.startTimeD
					}
					if(data.endTimeD==null){
						arr.endTimeD = moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm")
					}else{
						arr.endTimeD = data.endTimeD
					}
					if(data.startTimeN==null){
						arr.startTimeN = moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm")
					}else{
						arr.startTimeN = data.startTimeN
					}
					if(data.endTimeN==null){
						arr.endTimeN = moment().set({'hour' : 08, 'minute' : 30}).add(1,'day').format("HH:mm")
					}else{
						arr.endTimeN = data.endTimeN
					}

					/* arr.startTimeD = data.startTimeD
					arr.endTimeD = data.endTimeD
					arr.startTimeN = data.startTimeN
					arr.endTimeN = data.endTimeN */

					if(data.multiPrdNo > 1){
						multiPrdNoList.push(data)
					} 
					
					list.push(arr);
				});
				
				var dataSource = new kendo.data.DataSource({
	                data: list,
	                group: {
                        field: "name",
                        dir: "asc",
                    },
                    sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "startTimeD" , dir:"asc"
                    },{
                    	field:"startTimeN",dir:"asc"
                    }],
                    groupable: false,
	                schema: {
	                    model: {
	                      id: "dvcId",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                    	 dvcId: { editable: false },
	                    	 routing:{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         name: { editable: false, nullable: true , attributes:{style:"text-align:right;"},sortable:false},
	                         tgCntD: { validation: { required: true } },
	                         tgRunTimeD: { validation: { required: true } },
	                         cntPerCyl: { validation: { required: false },nullable:true },
	                         tgCntN: { validation: { required: true } },
	                         tgRunTimeN: { validation: { required: true } },
	                         capa: { editable: true , type:"number" },
	                         empD: { validation: { required: false },nullable:true },
	                         empN: { validation: { required: false },nullable:true },
	                         prdNo: { editable: false ,dir: "asc"},
	                         uph: { editable: false },
	                         startTimeD : {editable:true},
	                         copyDateColor:{editable:false }
	                      }
	                    }
	                }
	             });
 				
				table += "</tbody>";
				grid = $("#wrapper").data("kendoGrid");	
				
				grid.setDataSource(dataSource);
				var ssss=grid.dataSource.data();
				
				$(".k-link").css({
					"color" : "white"
				})
					
				printlistD();
				printlistN();

				$(".k-grid-저장").click(function(){
					saveRow()
					$(".k-alt").css({
						"background-color" : "#BDBDBD"
					})	
				});
		
				$('.k-alt').css({
					"background-color" : "#BDBDBD"
				})							

				$(".save_btn").click(function(){
					var img = document.createElement("img");
					img.setAttribute("id", "loading_img");
					img.setAttribute("src", "${ctxPath}/images/load.gif");
					img.style.cssText = "width : " + getElSize(500) + "; " + 
										"position : absolute;" + 
										"z-index : 99999999;" + 
										"border-radius : 50%;"
										
					$("body").prepend(img);
					$("#loading_img").css({
						"top" : (window.innerHeight/2) - ($("#loading_img").height()/2),
						"left" : (window.innerWidth/2) - ($("#loading_img").width()/2)
					});
					
				});
				
				$(".targetCnt").css({
					"font-size" : getElSize(40),
					"width" : getElSize(250),
					"outline" : "none",
					"border" : "none"
				});
				
				$(".span").css({
					"font-size" : getElSize(40),
					"color" : "red",
					"margin-left" : getElSize(10),
					"font-weight" : "bolder"
				});
				
				$(".save_btn").css({
					"background-color" : "white",
					"color" : "black",
					"border-radius" : getElSize(10),
					"font-weight" : "bolder",
					"padding" : getElSize(10)
				});
				
				
				$(".tdisable").each(function(idx, data){
					this.disabled = true;
					this.value = "";
				});
				
				$(".tdisable").css({
					"background-color" : "rgba(	4,	238,	91,0.5)"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222",
					"font-size" : getElSize(40)
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232",
					"font-size" : getElSize(40)
				});
				$.hideLoading()
			}//success
		});
		
	};
	
	 timeArrayE = [];
	 timeArrayS = [];

	 var currentStart;
	 var currentEnd;
	 
	 function testTimeN(container, options){
			
			currentStartN=options.model.startTimeN;
			currentEndN=options.model.endTimeN;
			console.log("inputTimepi")
			$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
		    .appendTo(container)
		    .kendoTimePicker({
			    	dateInput: true,
		    	    format: "HH:mm",
		    	    min:"20:30",
		    	    max:"08:30"
		   	});

		}
	 
	function testTimeD(container, options){
		
		currentStart=options.model.startTimeD;
		currentEnd=options.model.endTimeD;
		console.log("inputTimepi")
	//	startTime="08:30"
	//	endTime="20:30"
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"08:30",
	    	    max:"20:30"
	   	});
		
	
	}	

	
function setTimeD(container, options, a){
		
		var startTime = "08:30";
		var endTime = "20:31";
		
		var dataItem = $(container).closest("tr")[0].dataset.uid;
		var grid = $("#wrapper").data("kendoGrid");
		var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem + "']");
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 var dataItem = grid.dataSource.at(idx);
		 var length = grid.dataSource.data().length
		 var check = 0;
		 timeArrayE = [];
		 timeArrayS = [];
		 
		 
		 
		 
		 for(var i=0;i<length;i++){
			 
			 if(grid.dataSource.data()[i].dvcId==dataItem.dvcId && grid.dataSource.data()[i].prdNo == dataItem.prdNo){
				 
				 check++;
				 timeArrayS.push(grid.dataSource.data()[i].startTimeD)
				 timeArrayE.push(grid.dataSource.data()[i].endTimeD)
			 }
		 }
		 if(check<=1){
			 
		 }else{
			 
			 
			 var length = timeArrayE.length
			 for(var i=0;i<length;i++){
				 if(typeof timeArrayE[i] == 'string'){
					 
					 var array =  timeArrayE[i].split(':');
					 
					 timeArrayE[i] = moment().set({'hour' : array[0], 'minute' : array[1]})
				 }
 				if(typeof timeArrayS[i] == 'string'){
					 
					 var array =  timeArrayS[i].split(':');
					 
					 timeArrayS[i] = moment().set({'hour' : array[0], 'minute' : array[1]})
				 }
			 }
			 
			 
			   timeArrayE.sort(function (dvcId, name) {
				 	 if (dvcId.isAfter(name)) {
					    return -1;
					  }
					  if (name.isAfter(dvcId)) {
					    return 1;
					  }
					  return 0;
				});
			   
			   timeArrayS.sort(function (dvcId, name) {
				 	 if (dvcId.isAfter(name)) {
					    return -1;
					  }
					  if (name.isAfter(dvcId)) {
					    return 1;
					  }
					  return 0;
				});
			 
			 console.log(timeArrayE)
			 console.log(timeArrayS)
			 
			 if(options.field=='endTimeD'){
				 
			 }else{
				 
			 }
			 startTime = timeArrayE[0].format("HH:mm")
		 }
			 
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
        .appendTo(container)
        .kendoTimePicker({
        	min: startTime,
        	max: endTime,
        	format: "HH:mm",
        	change: function() {
        		selectedTime = this.value();
        		
        		if(check<=1){
        			alert("작업이 1개 일시에는 작업을 분할 할 수 없습니다.")
        			options.model.startTimeD = startTime;
        			options.model.endTimeD = endTime;
        		}else{
            		
        			selectedTime = this.value();
        			var hour = selectedTime.getHours();
        			var minute = selectedTime.getMinutes();
        			
        			selectedTime = moment().set({'hour' : hour, 'minute' : minute})
    				
    				console.log(selectedTime)
        			
    				if(options.field=='endTimeD'){
    					if(typeof options.model.startTimeD!='undefined' && selectedTime.isBefore(moment(options.model.startTimeD))){
    						alert("종료시간은 시작시간보다 후여야 합니다.")
    						options.model.endTimeD=options.model.startTimeD;
    					}else{
    						options.model.endTimeD=selectedTime.format("HH:mm")
    					}
    				}else{
    					if(typeof options.model.endTimeD!='undefined' && selectedTime.isAfter(moment(options.model.endTimeD))){
    						alert("시작시간은 종료시간보다 전이여야 합니다.")
    						options.model.startTimeD=options.model.endTimeD;
    					}else{
    						options.model.startTimeD=selectedTime.format("HH:mm")
    					}
    				}
        		}
        	},close: function(e) {
        		$(".actionBtn").css({
    				"border" : "1px solid black",
    				"border-radius" : getElSize(15)+"px",
    				"font-size" : getElSize(50),
    				"padding" : getElSize(10)
    			})
            }
        });
		
		
	}
	
	function setTimeN(container, options, a){
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
        .appendTo(container)
        .kendoTimePicker({
        	min: ("20:30"),
        	max: ("08:30"),
        	 change: function() {
        		$(".actionBtn").css({
       				"border" : "1px solid black",
       				"border-radius" : getElSize(15)+"px",
       				"font-size" : getElSize(50),
       				"padding" : getElSize(10)
       			})
       	        var value = this.value();
       	        var nowTime = moment(value)
       	        var standardTime = moment().set({'hour':20, 'minute':29});
       	        
       	     	if(nowTime.isBefore(standardTime)){
       	     		console.log("오늘보다 예전입니다.")
	     			nowTime.add(1, 'days')
	     		}
       	        
       	     	if(options.field=='endTimeN'){
       	     		if(typeof options.model.startTimeN!='undefined' && nowTime.isBefore(moment(options.model.startTimeN))){
	       	     		console.log(options.model.startTimeN)
	       	     		alert("종료시간이 시작시간보다 후 여야 합니다.")
	   	     			options.model.endTimeN=options.model.startTimeN;
       	     		}else{
       	     			
       	     		}
				}else{
					if(typeof options.model.endTimeN!='undefined' && nowTime.isAfter(moment(options.model.endTimeN))){
						alert("시작시간이 종료시간보다 전이여야 합니다.")
	   	     			options.model.startTimeN=options.model.endTimeN;
       	     		}else{
	       	     		
       	     		}
				}
       	    },close: function(e) {
        		$(".actionBtn").css({
    				"border" : "1px solid black",
    				"border-radius" : getElSize(15)+"px",
    				"font-size" : getElSize(50),
    				"padding" : getElSize(10)
    			})
            }
        });
	}

	
	
	
	
	
	
	
	
	
	
	function workauto(container, options, a){
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: selectlist,
			autoWidth: true,
			dataTextField: "text",
			dataValueField: "value",
            placeholder: "작업자 선택",
            select:function(e){
            }
        }).data("kendoComboBox");
	}
	
		function nameFilter(element) {
		
        element.kendoComboBox({
            dataSource: selectlist,
            dataTextField: "text",
			dataValueField: "value",
			placeholder: "작업자선택"
        });
    }
	
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var table = "<tbody>";
							
				$(json).each(function(idx, data){
					
					
					var arr=new Object();
					arr.text=decode(data.name);
					arr.value=decode(data.id);
					arr.textname=decode(data.name);
					selectlist.push(arr)
				});
				
				
				console.log(selectlist)
			}
		});
	}
	
	function printlistD(){
		var table="";
		var list=[];
		var selectlist=[];
		var url = "${ctxPath}/order/getTargetData1.do";
		
		var param = "shopId=" + shopId + 
					"&date=" + $("#date").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var  json = data.dataList;
				var class_name = "";
				var page=1;	//현재페이지 나타내기
				var divideNum=31;	//page 나눌수량
				var totalpage=Math.ceil(json.length/divideNum); //인쇄 총페이지
				table+="<div class='a4'><table align='center' border='4' width='90%'>"
				+ "<tr style='background-color:#D5D5D5;'><th colspan='3' rowspan='3' width='60%'><h1 style=' margin-bottom:5px; padding-bottom:3px;'>일 생산 계획</h1><br>${day} ("+$("#date").val()+")</th><th colspan='3'>결제	</th></tr>"
				+ "<tr align='center' style='background-color: #EAEAEA;'><td width='10%'>담당</td><td width='10%'>검토</td><td width='10%'>승인</td></tr>"
				+"<tr style='height:4%;'> <td> </td> <td> </td><td></td>"
				+ "<tr style='background-color: #EAEAEA;'><th width='30%'>${device}</th><th width='15%'>${prdct_cnt}</th><th width='15%'>${ophour}(h)</th><th >uph</th><th>${working_capa}</th><th>${worker}</th></tr>"
				$(json).each(function(idx, data){
					var arr=new Object();
					var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
					table+="<tr><td align='center' >"
					table+=name+"</td><td align='center'>";
					table+=data.tgCntD+"</td><td align='center'>";
					table+=data.tgRunTimeD/60/60+"</td><td align='center'>";
					table+=data.uph+"</td><td align='center'>";
					table+=data.capa+"</td><td align='center'>";
					table+=decode(data.empD)+"</td>";
					table+="</tr>"
					if(idx%divideNum==0 && idx!=0){
						page++;
						table+="</table><div style='page-break-before:always'></div>"
						table+="<br><br><br><table align='center' border='4' width='90%'><tr style='height=30%; background-color:#D5D5D5; page-break-before:always' ><th colspan='5' rowspan='2'><h1 style=' margin-bottom:0px; padding-bottom:0px;'>일 생산 계획</h1><br>${day} ("+$("#date").val()+")</th><td align='center' style='background-color: #D5D5D5;'>페이지</td></tr>"
						+ "<tr align='center' style='background-color: #EAEAEA;'><td>"+page+"/"+totalpage+"</td></tr>"
						+ "<tr style='background-color: #EAEAEA;'><th width='30%'>${device}</th><th width='15%'>${prdct_cnt}</th><th style="+'"'+"width:15%"+'"'+">${ophour}(h)</th><th width='10%'>uph</th><th width='10%'>${working_capa}</th><th width='10%'>${worker}</th></tr>"
					}
				});
				table+="</table></div>"
				$("#printD").empty();
				$("#printD").append(table);
			}
		});
	}
	
	var a=1;
	var onDataBound = function(e) {
		
	    $('td').each(function(){
	    	if($(this).text()=='라우팅 없음'){
    			$(this).addClass('routing')
//    			console.log($(this))
    		}
	    	if($(this).text()=='미배치'){
    			$(this).addClass('routing')
//    			console.log($(this))
    		}
	    	

    	});
	    $(".k-grid td").css("font-size",getElSize(32))
	    $(".k-group-cell").css("background","gray");
	    $(".actionBtn").css({
			"border" : "1px solid black",
			"border-radius" : getElSize(8)+"px",
			"font-size" : getElSize(32),
			"padding" : getElSize(10)
		})
		
		console.log("Click")
		var grid = $("#wrapper").data("kendoGrid");	
		var kendotable=grid.dataSource.data();
		var length=kendotable.length;
		
		var i=0;
		i++;
		console.log(i)
		console.log("----databound----")
			/* grid.dataSource.sort([
				{ field: "prdNo", dir: "asc" },{field:"startTimeD",dir:"asc"}
			]) */
	     $('#wrapper tbody tr').each(function(){
	    	 if($(this).text().indexOf("copyDateColor")==-1){
	 	    	
	    	 }else{
	    		 $(this).addClass('copyDateColor')
	    	 }
	    	 
	    	 if($(this).text().indexOf('oddRow')!=-1){
    			$(this).addClass('oddRow')
//	    			console.log($(this))
	    	}else if($(this).text().indexOf('evenRow')!=-1){
	    		$(this).addClass('evenRow')
	    	}else{
	    		
	    	}
    	});
//		$("#wrapper tbody tr td").css("line-height",getElSize(7.5)); 딱적당
		/* $("#wrapper tbody tr td").css("line-height",getElSize(7.8)); */
		$("#wrapper tbody tr td").css("line-height","3.5");	// jane (setElsize 로 조정이 해상도에 따라 line-height 차이 많이 남)
		/* $("#wrapper tbody tr td").css("font-size",getElSize(55) + "px"); */	// jane
        $(".k-grouping-row td").css("line-height",0);
        $("#wrapper tbody tr td").css("padding",getElSize(0));
        $(".k-grouping-row").css("height",getElSize(16));
        $(".evenRow").css("background","#F6F6F6")
        $(".oddRow").css("background","#EAEAEA")
		$(".k-grid td").css({
            "white-space": "nowrap",
            "text-overflow": "ellipsis"
        })
        
        $(".k-header").css({            
	         "font-family" : "NotoSansCJKkrBold",
	         "font-size" : getElSize(32) + "px",
	         "background-image" : "none",
		     "background-color" : "#353542"     
	 	});	// jane_font
	 	
        /* var color="blue"
        var length=$('.k-grid-content').length
        for(i=0; i<length; i++){
        	
        } */
//        $("#wrapper tbody tr td").css("background","blue");
//        $("#wrapper tbody tr td").css("background","blue");
		return;
        	
	};
	//야간정렬
	function printlistN(){
		var table="";
		var list=[];
		var url = "${ctxPath}/order/getTargetData2.do";
		
		var param = "shopId=" + shopId + 
					"&date=" + $("#date").val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var  json = data.dataList;
				var class_name = "";
				var page=1;	//현재페이지 나타내기
				var divideNum=31;	//page 나눌수량
				var totalpage=Math.ceil(json.length/divideNum); //인쇄 총페이지
				
				table+="<div class='a4'><table align='center' border='4' width='90%'>"
				+ "<tr style='background-color:#D5D5D5;'><th colspan='3' rowspan='3' width='60%'><h1 style=' margin-bottom:5px; padding-bottom:3px;'>일 생산 계획</h1><br>${night} ("+$("#date").val()+")</th><th colspan='3'>결제	</th></tr>"
				+ "<tr align='center' style='background-color: #EAEAEA;'><td width='10%'>담당</td><td width='10%'>검토</td><td width='10%'>승인</td></tr>"
				+"<tr style='height:4%;'> <td> </td> <td> </td><td></td>"
				+ "<tr style='background-color: #EAEAEA;'><th width='30%'>${device}</th><th width='15%'>${prdct_cnt}</th><th width='15%'>${ophour}(h)</th><th >uph</th><th>${working_capa}</th><th>${worker}</th></tr>"
				$(json).each(function(idx, data){
					var arr=new Object();
					var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
					table+="<tr><td align='center' >"
					table+=name+"</td><td align='center'>";
					table+=data.tgCntD+"</td><td align='center'>";
					table+=data.tgRunTimeD/60/60+"</td><td align='center'>";
					table+=data.uph+"</td><td align='center'>";
					table+=data.capa+"</td><td align='center'>";
					table+=decode(data.empN)+"</td>";
					table+="</tr>"
					if(idx%divideNum==0 && idx!=0){
						page++;
						table+="</table><div style='page-break-before:always'></div>"
						table+="<br><br><br><table align='center' border='4' width='90%'><tr style='height=30%; background-color:#D5D5D5; page-break-before:always' ><th colspan='5' rowspan='2'><h1 style=' margin-bottom:0px; padding-bottom:0px;'>일 생산 계획</h1><br>${night} ("+$("#date").val()+")</th><td align='center' style='background-color: #D5D5D5;'>페이지</td></tr>"
						+ "<tr align='center' style='background-color: #EAEAEA;'><td>"+page+"/"+totalpage+"</td></tr>"
						+ "<tr style='background-color: #EAEAEA;'><th width='30%'>${device}</th><th width='15%'>${prdct_cnt}</th><th style="+'"'+"width:15%"+'"'+">${ophour}(h)</th><th width='10%'>uph</th><th width='10%'>${working_capa}</th><th width='10%'>${worker}</th></tr>"
					}
				});
				table+="</table></div>"
				
				$("#printN").empty();
				$("#printN").append(table);
			}
		});
	}
	
	function copy(e){
		return '<a class="k-button" href="#" id="toolbar-print_user" onclick="copyWork()">${day} <-> ${night} ${worker}</a>';
	}

	function prtD(e){
		return '<a class="k-button" href="#" id="toolbar-print_user" onclick="d_print()">${day} Print</a>';
	}
	function prtN(e){
		return '<a class="k-button" href="#" id="toolbar-print_user" onclick="n_print()">${night} Print</a>';
	}
	var win=null;
	
	function worKerName(id){
		var chk=0;
		for(var i = 0; i < selectlist.length; i++){
			if(selectlist[i].value==id){
				chk++;
				return selectlist[i].textname;
			}
		}
		if(chk==0){
			return "미 배 치"
		}
	}
	
	var printChkWin=false;
	function d_print(){
		printChkWin=true;
		win = window.open("","","height=" + screen.height + ",width=" + screen.width + "fullscreen=yes");
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('#adb { background-color:red	}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('printD').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        
        if(printChkWin==true){
			$(window).click(function() {
				//Hide the menus if visible
				alert('프린터 창이 열려 있습니다')
			});
		}
        win.print();
       
        printChkWin=false;
		$(window).off("click");
        
		win.close();
	}
	function n_print(){
		printChkWin=true;
		win = window.open("","","height=" + screen.height + ",width=" + screen.width + "fullscreen=yes");
		
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('printN').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        
        if(printChkWin==true){
			$(window).click(function() {
				//Hide the menus if visible
				alert('프린터 창이 열려 있습니다')
			});
		}

       win.print();
       
       printChkWin=false;
	   $(window).off("click");
		
        win.close();
	}

	function copyRow(){
		$(".contentTr").each(function(idx, data){
			$(data).children("td:nth(4)").children("input").val($(data).children("td:nth(1)").children("input").val());
			$(data).children("td:nth(5)").children("input").val($(data).children("td:nth(2)").children("input").val())
			$(data).children("td:nth(6)").children("input").val($(data).children("td:nth(3)").children("input").val())
		});
	};
	
	var valueArray = [];
	var addWorker = [];
	function saveRow(){
		console.log(getToday2())
		console.log($("#date").val());
		
		if(showDate==$("#date").val()){
			if(!confirm("저장하시겠습니까?")){
				return;
			}
		}else{
			if(!confirm("일자가 "+$("#date").val()+" 로 변경되었습니다"+"\n저장하시겠습니까?")){
				return;
			}
		}
		$.showLoading();

		valueArray = [];
		addWorker = [];
		capalist = [];
		grid = $("#wrapper").data("kendoGrid");	        
		
		var ssss=grid.dataSource.data();
		var savedata=[]
		var extraSaveData=[]
		
		var MaxGp=0;
		for(i=0; i<ssss.length; i++){
			if(isNaN(ssss[i].group)!=true){
				MaxGp=ssss[i].group
			}
			savedata.push(ssss[i]);
			
		}
		
		/* for(i=0; i<savedata.length; i++){
			var chk=0;
			for(j=0; j<savedata.length; j++){
				if(savedata[j].name==savedata[i].name){
					chk++;
				}
			}
			if(chk>1){
				savedata.splice(i,1);
			}
		} */
		
		console.log(savedata)
		
		var groupCnt=MaxGp;
		$(savedata).each(function(idx, data){
			//주간
			var dataItem = grid.dataItem(data)
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntD;
			obj.tgRunTime = data.tgRunTimeD*3600;
			obj.tgDate = $("#date").val();
			obj.type = 2;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeD;
			obj.endTime = data.endTimeD;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}
			
			var changeemp=data.empD.split(',');
			if(changeemp.length==2){	
				obj.empCd=changeemp[1];
			}else{
			obj.empCd=data.empD;
			}
			
			if(obj.type==2 && obj.startTime =="08:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==2 && obj.startTime !="08:30"){ //추가작업자
				addWorker.push(obj)			
			}
			
			//야간
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntN;
			obj.tgRunTime = data.tgRunTimeN*3600;
			obj.tgDate = $("#date").val();
			obj.type = 1;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeN;
			obj.endTime = data.endTimeN;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}

			var changeemp=data.empN.split(',');
			if(changeemp.length==2){
				obj.empCd=changeemp[1];	
			}else{
			obj.empCd=data.empN;
			}
			if(obj.type==1 && obj.startTime =="20:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==1 && obj.startTime !="20:30"){ //추가작업자
				addWorker.push(obj)			
			}

			if(data.multiPrdNo>1){
				extraSaveData.push(obj)
			}
			groupCnt++;
			
			//capa
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 2;
			capalist.push(obj);

			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 1;

			capalist.push(obj);
			
		});
		
		var obj = new Object();
		obj.val = valueArray;
		
		var addobj = new Object();
		addobj.val = addWorker;
		
		var capaobj = new Object();
		capaobj.val = capalist;
		
		console.log(valueArray)
		console.log(addWorker)
		console.log(extraSaveData)
		console.log("--==--")
		console.log(capaobj)
		var url = "${ctxPath}/order/addTargetCnt.do";
		var param = "val=" + JSON.stringify(obj) +
					"&addval=" + JSON.stringify(addobj) +
					"&capaval=" + JSON.stringify(capaobj);
		console.log(param)
 		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				if(data=="success"){
					alert("${save_ok}");
					getTargetData(); 
				}else{
					alert("저장에 실패 하였습니다.")
					getTargetData(); 
				}
			}
		});
		
	};
	
	
	function copyWork(){
		
		if(getToday2()!= $("#date").val()){
			if(!confirm("저장하시겠습니까?")){
				return;
			}
		}
		
		$.showLoading();

		valueArray = [];
		addWorker = [];
		grid = $("#wrapper").data("kendoGrid");	        
		
		var ssss=grid.dataSource.data();
		var savedata=[]
		var extraSaveData=[]
		var capalist=[];

		var MaxGp=0;
		for(i=0; i<ssss.length; i++){
			if(isNaN(ssss[i].group)!=true){
				MaxGp=ssss[i].group
			}
			savedata.push(ssss[i]);
			
		}
		
		/* for(i=0; i<savedata.length; i++){
			var chk=0;
			for(j=0; j<savedata.length; j++){
				if(savedata[j].name==savedata[i].name){
					chk++;
				}
			}
			if(chk>1){
				savedata.splice(i,1);
			}
		} */
		
		console.log(savedata)
		
		var groupCnt=MaxGp;
		$(savedata).each(function(idx, data){
			//주간
			var dataItem = grid.dataItem(data)
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntD;
			obj.tgRunTime = data.tgRunTimeD*3600;
			obj.tgDate = $("#date").val();
			obj.type = 2;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeD;
			obj.endTime = data.endTimeD;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}
			
			var changeemp=data.empD.split(',');
			if(changeemp.length==2){	
				obj.empCd=changeemp[1];
			}else{
			obj.empCd=data.empN;
			}
			
			if(obj.type==2 && obj.startTime =="08:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==2 && obj.startTime !="08:30"){ //추가작업자
				addWorker.push(obj)			
			}
			
			//야간
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntN;
			obj.tgRunTime = data.tgRunTimeN*3600;
			obj.tgDate = $("#date").val();
			obj.type = 1;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeN;
			obj.endTime = data.endTimeN;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}

			var changeemp=data.empN.split(',');
			if(changeemp.length==2){
				obj.empCd=changeemp[1];	
			}else{
			obj.empCd=data.empD;
			}
			if(obj.type==1 && obj.startTime =="20:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==1 && obj.startTime !="20:30"){ //추가작업자
				addWorker.push(obj)			
			}

			if(data.multiPrdNo>1){
				extraSaveData.push(obj)
			}
			groupCnt++;
			
			//capa
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 2;
			capalist.push(obj);

			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 1;

			capalist.push(obj);
			
		});
		
		
		var obj = new Object();
		obj.val = valueArray;
		
		var addobj = new Object();
		addobj.val = addWorker;
		
		var capaobj = new Object();
		capaobj.val = capalist;
		
		console.log(valueArray)
		console.log(addWorker)
		console.log(extraSaveData)
		

		
		
		var url = "${ctxPath}/order/addTargetCnt.do";
		var param = "val=" + JSON.stringify(obj) +
					"&addval=" + JSON.stringify(addobj) +
					"&capaval=" + JSON.stringify(capaobj);

		console.log(param)
 		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				if(data=="success"){
					alert("${save_ok}");
					getTargetData(); 
				}else{
					alert("저장에 실패 하였습니다.")
					getTargetData(); 
				}
			}
		});
		
	};
	
	/* function copyWork(){
		if(getToday2()!= $("#date").val()){
			if(!confirm("저장하시겠습니까?")){
				return;
			}
		}
		valueArray = [];
		grid = $("#wrapper").data("kendoGrid");
		
		var ssss=grid.dataSource.data();
		//중복제거 주소값 바꾸기위해
		var savedata=[]
		
		for(i=0; i<ssss.length; i++){
			savedata.push(ssss[i]);
		}
		
		for(i=0; i<savedata.length; i++){
			var chk=0;
			for(j=0; j<savedata.length; j++){
				if(savedata[j].name==savedata[i].name){
					chk++;
				}
			}
			if(chk>1){
				savedata.splice(i,1);
			}
		}
		
		$(savedata).each(function(idx, data){
			var dataItem = grid.dataItem(data)
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntD;
			obj.tgRunTime = data.tgRunTimeD*3600;
			obj.tgDate = $("#date").val();
			obj.type = 2;
			obj.cntPerCyl = data.cntPerCyl;
			
			obj.startTime = data.startTimeD;
			obj.endTime = data.endTimeD;
			
			var changeemp=data.empN.split(',');
			if(changeemp.length==2){	
				obj.empCd=changeemp[1];
			}else{
			obj.empCd=data.empN;
			}
			
			valueArray.push(obj)
			
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntN;
			obj.tgRunTime = data.tgRunTimeN*3600;
			obj.tgDate = $("#date").val();
			obj.type = 1;
			obj.cntPerCyl = data.cntPerCyl
			
			obj.startTime = data.startTimeN;
			obj.endTime = data.endTimeN;

			var changeemp=data.empD.split(',');
			if(changeemp.length==2){
				obj.empCd=changeemp[1];	
			}else{
			obj.empCd=data.empD;
			}
			valueArray.push(obj)
			console.log(obj.startTime)
		});
		
		
		console.log("--end--")
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/order/addTargetCnt.do";
		var param = "val=" + JSON.stringify(obj) +
					"&addval=" + JSON.stringify(addobj);
		if(confirm("자동 저장됩니다. 작업자 변경 하시겠습니까?")==true){
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function (data){
					if(data=="success"){
						alert("${save_ok}");
					}getTargetData();
				}
			});
		}else{
			return;
		}
	}; */
	
	function getToday2(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		return year + "-" + month + "-" + day	
	};
	
	$(function() {
    	$("#date").datepicker({
    		onSelect:function(){
//    			$.showLoading()
    			console.log($(".sDate").val());
//    			getTargetData()
    		}
    	})
	    	
	});
	
	
	</script>
</head>
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
	
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td> --%>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table style="width: 100%; float: right;">
						<Tr>
							<Td style="color:white; ">
								<div id="info"><center style="background:gray; border-radius:5px;width: 75%;margin-left: 25%;"><spring:message code="routing_manual"></spring:message></center></div>
							</Td>
							<Td class="btnGroup"style="color: white; text-align: right;">
								<spring:message code="date_"></spring:message> :
								<input type="text" id="date" class="sDate"> 
								<button id="search" onclick="getTargetData()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<input type="hidden" value="<spring:message code="save"></spring:message>" id='save'>
								<input type="hidden" value="<spring:message code="day_to_Night"></spring:message>" id='copy_data'>
							</Td>
						</Tr>
					</table>
			
					<div id="wrapper" class="wrapper" style="background: rgb(53, 53, 66);">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" id="target" class="targetTable" border="1">
						</table>
					</div>
				</td>
				
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
		</table>
	 </div>
	<div id="printD" class="a4" style="display:none;">
	</div>
	<div id="printN"  style="display:none;">
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	