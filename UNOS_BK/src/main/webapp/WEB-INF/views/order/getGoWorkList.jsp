<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>

	//전역변수 선언하는곳
	var kendotable;
	
	const loadPage = () =>{
		createMenuTree("om", "getGoWorkList")
	}
	
	$(function(){

		setEl();
		//create grid
		gridTable();
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => getTable()
		getWorkerList();

	})
	
	//datepicker event
	function dateEvt(){
		
		$("#sDate").val(moment().subtract(5, 'day').format("YYYY-MM-DD"))
		$("#eDate").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	//기초 CSS
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
// 			,"background" : "red"
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
// 			,"background" : "blue"
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
	}
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
				getTable();
				
			}
		});
	}
	
	//그리그 그리기 테이블 그리기
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height : $("#div2").height()
			,columns:[{
				field : "date"
				,title : "날짜"
			},{
				field : "name"
				,title : "작업자"
			},{
				field : "startTime"
				,title : "출근 시간"
			},{
				field : "endTime"
				,title : "퇴근 시간"
			}/* ,{
				field : ""
				,title : "외출 기록"
			} */,{
				field : "bigo"
				,title : "사유"
			}]
		}).data("kendoGrid");
	}
	
	//테이블 데이터 가져오기 //출근 리스트
	function getTable(){
		
		var url = "${ctxPath}/order/getGoEndHistoryList.do";
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&empCd=" + $("#worker").val();
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.bigo = decode(data.bigo);
				})

				console.log("==success==")
				console.log(json)
				
				kendodata = new kendo.data.DataSource({
					data: json,
					group: [{
								field:"date"
								,dir:"desc"
							}],
				})
				
// 				sort: [{
//                 	field: "prdNo" , dir:"asc" 
//                 },{
//                 	field: "item" , dir:"asc"
//                 },{
//                 	field: "ex" , dir:"asc" 
//                 }],

				kendotable.setDataSource(kendodata);
			}
		})
		
		console.log(param)
		$.hideLoading();
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				작업자 : <select id="worker"></select>
				날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
			</div>
		</div>
		
		<div id="div2">
			<div id="grid"></div>
		</div>
	</div>	
</body>
</html>	