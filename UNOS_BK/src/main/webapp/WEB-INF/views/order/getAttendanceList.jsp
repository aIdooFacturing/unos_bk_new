<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">

const loadPage = () =>{
    /* createMenuTree("maintenance", "Alarm_Manager") */  
    createMenuTree("om", "getAttendanceList")
};


	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	/* font-family:'Helvetica'; */
}
#searchBtn{
	font-weight : bolder;
    background: linear-gradient(lightgray, gray);
   	border : 0px solid;
   	border-radius : 5px
}
#searchBtn:hover{
	background: gray;
}
.k-window-titlebar{
	display: none;
}
button#approve:hover{
	background: #239027;
}
#logInBtn{
	background: linear-gradient(#7777f5, #2f2fff);
	border-radius: 5px;
	color: white;
	font-weight: bolder;
	border: 0px solid;
}
#logInBtn:hover{
	background: #2f2fff;
}
</style> 
<script type="text/javascript">
	$(function(){
		
		/* createNav("order_nav", 2); */
		
		/* setEl(); */
		setEl_dark();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		if(sessionStorage.getItem("name")!=null){
			$("#logInBtn").text('로그아웃')
			$("#logInBtn").css({
			    "background": "darkred"
			})
			$("#logInBtn").hover(function(){
				$(this).css("background-color", "red");
			    }, function(){
			    $(this).css("background-color", "darkred");
			});
			$("#logInBtn").attr("onclick","logout()");
			$("#info").css({
				"display":"inline"
			})
			$("#info").text(sessionStorage.getItem("name")+'님 반갑습니다.')
		}
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		
		$("#selected").css({
			"color" : "white",
		});
		
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#searchBtn").css({
			"height" : getElSize(120),
			"margin-top" : getElSize(35),
			"width" : getElSize(300),
			"margin-right" : getElSize(40),
			"font-size" : getElSize(60)
		})
		
		$("#buttonGroup").css({
			"height" : getElSize(180)
		})
		
		$("#dateGroup").css({
			"font-size" : getElSize(60),
			"margin-top" : getElSize(40),
		    "margin-left" : getElSize(40)
		})
		
		$("i").css({
			"margin-right" : getElSize(30)
		})
		
		$("#grid").css({
			"width" : getElSize(3300)
		})
		
		$("#logInBox").css({
		    "top": getElSize(138),
		    "right": getElSize(60)
		})
		
		$("#logInBtn").css({
		    "height": getElSize(115),	
	    	"width": getElSize(275),
	    	"font-size": getElSize(45)
		})
		
		$("#info").css({
			"position": "absolute",
	    	"top":getElSize(165),
	    	"right": getElSize(450),
	    	"display":"none"
		})
		
		$("#sDate, #eDate").css({
			"font-size": getElSize(52)
		})
		
		
	};
	
	function setEl_dark(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
	
		/* $("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			/* "top" : getElSize(100) + marginHeight, */
			"top" : getElSize(400) + marginHeight	// jane
		});
		
				
		$("#home").css({
			"cursor" : "pointer"
		})
		
		
		$("#selected").css({
			"color" : "white",
		});
		
		
		$("#searchBtn").css({
			"height" : getElSize(120),
			"margin-top" : getElSize(35),
			"width" : getElSize(300),
			"margin-right" : getElSize(40),
			"font-size" : getElSize(60)
		})	// jane : "검색 버튼"
		
		$("#buttonGroup").css({
			"height" : getElSize(180),
			"width" : $("#container").width(), // jane
			"font-size" : getElSize(32) + "px"	// jane
		})	// jane : 일자, 검색 버튼 ... div
		
		$("#dateGroup").css({
			/* "font-size" : getElSize(60), */
			"font-size" : getElSize(45),
			"margin-top" : getElSize(40),
		    "margin-left" : getElSize(40)
		})	// jane : 일자 ...
		
		
		$("#grid").css({
			/* "width" : getElSize(3300) */
			"width" : $("#container").width()
		});	// jane : grid 테이블
		
		$("#logInBox").css({
		   /*  "top": getElSize(138),
		    "right": getElSize(160), */
		    
		    "top" : getElSize(35),	// jane
		    "right" : getElSize(450)	// jane
		})	// jane : 로그인 버튼 div
		
		$("#logInBtn").css({
		    "height": getElSize(115),	
	    	"width": getElSize(275),
	    	"font-size": getElSize(45)
		})	// jane : 로그인 버튼 button
		
		
		$("#info").css({
			"position": "absolute",
	    	/* "top":getElSize(165),
	    	"right": getElSize(450), */
	    	
	    	"top" : getElSize(35),	// jane
			"right" : getElSize(950),	// jane
	    	"display":"none"
		})
		
		$("#sDate, #eDate").css({
			"font-size": getElSize(52)
		});
		
		/* $(".k-grid-header").css({
	    	"padding-right" : "0px"
		})	// jane */
		
		$(".k-header").css({            
	         "font-family" : "NotoSansCJKkrBold",
	         "font-size" : getElSize(32) + "px",
	         "background-image" : "none",
		     "background-color" : "#353542"     
	 	});
		
		table_color_set_dark()	// jane
	};
	
	function table_color_set_dark() {
		
		$('.k-grid-content table tbody tr').css("background-color","rgb(200,200,201)");
		$('.k-grid-content table tbody tr td').css("color","black");
		$('.k-alt').css("background-color","rgb(218,218,219)");

		$('.k-grid-content table tbody tr td').css("border","none");
		$('.k-grid-header').css("border","none");
		$('.k-grid-header th').css("border","none");
		$('#grid').css("border","none");


		$('.k-grid-header th').css("border-left","1px solid black");
		$('.k-grid-header th').css("border-right","1px solid black");
		$('.k-grid-header th').css("border-bottom","1px solid black");

		$('.k-grid-content table tbody tr td').css("border-left","1px solid black");
		$('.k-grid-content table tbody tr td').css("border-right","1px solid black");
		$('.k-grid-content table tbody tr td').css("border-bottom","1px solid black");
		
		$("#contentTable").css({
			"padding-left" : "1%"
		});
	}
	
	/* function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	
	var loginDialog;
	var popup;
	$(function(){
		
		
		$("#sDate").datepicker({
			changeMonth: true,
	        changeYear: true,
	        maxDate: '0'
		})
		
		$("#eDate").datepicker({
			changeMonth: true,
	        changeYear: true,
			maxDate: '0'
		})
		
		popup = $("#popup").kendoPopup({
			anchor: $("#dialog"),
			position: "center",
			animation: {
				close: {
			    	effects: "zoom:out",
			    	duration: 300
	 			}
			},
			open: function(e){
				$("#popup").css({
					"font-size" : getElSize(50),
					"width" : getElSize(1000),
					"color" : "red"
				})
			}
		}).data("kendoPopup");
		
		loginDialog = $("#dialog").kendoDialog({
			visible: false,
			open:function(e){
				$("#combobox").val("")
				$("#pwd").val("")
				
				$(".loginText").css({
					"width" : getElSize(600),
					"font-size" : getElSize(50),
					"font-weight" : "bolder"
				})
				
				$(".input").css({
					"font-size": getElSize(50),
					"width" : getElSize(300)
				})
				
				$(".ID").css({
					"margin-bottom" : getElSize(50)
				})
				
				$(".Login").css({
					"margin-right" : getElSize(300),
					"font-size" : getElSize(50),
					"width" : getElSize(300),
					"height" : getElSize(100)
				})
				
				$(".Close").css({
					"font-size" : getElSize(50),
					"width" : getElSize(300),
					"height" : getElSize(100)
				})
				
				$("input[name=id]").keydown(function (key) {
			        if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
			        	loginCheck();
			        }
			    })
			    
				$("input[name=pwd]").keydown(function (key) {
			        if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
			        	loginCheck();
			        }
			    })
				
				
			}
		    }).data("kendoDialog");
		
		
		$("#sDate, #eDate").val(moment().format("YYYY-MM-DD"))
		
		grid = $("#grid").kendoGrid({
			height: getElSize(1590),
			scrollable:true,
			columns:[
				{
					field: 'worker',
					title: '작업자',
					template: '#=worker#',
					width : getElSize(300),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'approve',
					title: '승인여부',
					width : getElSize(600),
					template : "#=approveChk(approve)#",
//					template: '#=(approve!=2)?"<button id="+"approve"+" onclick=approveAttendence(this)>승인</button>   <button id="+"takeAway"+" onclick=takeAwayApprove(this)>미승인</button>":"<button id="+"takeAway2"+" onclick=takeAwayApprove(this)>승인해제</button>"#',
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'attendanceTy',
					title: '근태유형',
					template: '#=attendanceTy#',
					width : getElSize(400),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'date',
					title: '등록일자',
					template: '#=date#',
					width : getElSize(400),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'attendanceDiv',
					title: '근태분류',
					width : getElSize(400),
					template: '#=attendanceDiv#',
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'attendance',
					title: '근태',
					width : getElSize(400),
					template: '#=attendance#',
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'sDate',
					title: '시작',
					template: '#=sDate#',
					width : getElSize(300),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'eDate',
					title: '종료',
					template: '#=eDate#',
					width : getElSize(300),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'reason',
					title: '사유',
					template: '#=reason#',
					width : getElSize(500),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				},
				{
					field: 'msg',
					title: '미승인 이유',
					template: '#=msg#',
					width : getElSize(600),
					attributes: {
						style : '',
					},
					headerAttributes: {
						style : 'font-size : ' + getElSize(50)
					}
				}
			]
		}).data("kendoGrid");
		
		getAttendanceList();
		
		setEl_dark();	// jane
	})
	
	function approveChk(chk){
		if(chk==1){
			return "<button id="+"approve"+" onclick=approveAttendence(this)>승인</button>   <button id="+"takeAway"+" onclick=takeAwayApprove(this)>미승인</button>"
		}else if(chk==2){
			return "<button id="+"takeAway2"+" onclick=cancelApprove(this)>승인해제</button>"
		}else if(chk==3){
			return "<button id="+"takeAway2"+" onclick=cancelApprove(this)>미승인해제</button>"
		}
	}
	
	function getAttendanceList(){
		
		var url = "${ctxPath}/chart/getAttendanceHist.do";
		var param = "sDate=" + $("#sDate").val() +"&eDate=" + $("#eDate").val() + "&approve=1";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				
				var json = data.dataList;
				
				attendanceList_DateSource = new kendo.data.DataSource({});
				
				$(json).each(function(idx, data){
					var object = {
							"id" : data.id,
							"worker" : decode(data.name),
							"attendanceTy" : decode(data.attendanceTy),
							"date" : data.date,
							"attendanceDiv" : decode(data.attendanceDiv),
							"attendance" : decode(data.attendance),
							"sDate" : data.sDate,
							"eDate" : data.eDate,
							"approve" : data.approve,
							"reason" : decode(data.reason),
							"msg" : decode(data.msg)
					}
					attendanceList_DateSource.add(object)
				});
				
				grid.setDataSource(attendanceList_DateSource);
				
				$("button#approve").css({
					"width" : getElSize(200),
					"height" : getElSize(105),
					"border" : "0px solid",
					"font-weight" : "bolder",
					"font-size" : getElSize(40),
			    	"background": "linear-gradient(#59ff43, #239027)",
			    	"margin-right" : getElSize(30),
				   	"border-radius" : "5px"
				})
				
				$("button#takeAway").css({
					"width" : getElSize(200),
					"height" : getElSize(105),
					"border" : "0px solid",
					"font-weight" : "bolder",
					"font-size" : getElSize(40),
					"background": "linear-gradient(#f5522d, rgb(236, 0, 0))",
				   	"border-radius" : "5px"
				})
				
				$("button#takeAway2").css({
					"width" : getElSize(400),
					"height" : getElSize(105),
					"border" : "0px solid",
					"font-weight" : "bolder",
					"font-size" : getElSize(40),
					"background": "linear-gradient(#f5522d, rgb(236, 0, 0))",
				   	"border-radius" : "5px"
				})
				
				setEl_dark();	// jane ajax
			}
		})
	}
	
	function approveAttendence(e){
		
		if(sessionStorage.getItem("name")==null || sessionStorage.getItem("name")=='undefined'){
			loginDialog.open();
			return ;
		}
		if(sessionStorage.getItem("lv")<=1){
			kendo.alert("권한이 없는 작업자 입니다.")
			return ;
		}
		
		$.showLoading()
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var row = $("#grid").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + dataItem + "']");
		 var idx = grid.dataSource.indexOf(grid.dataItem(row));
		 var dataItem = grid.dataSource.at(idx);
		 
		 
		var url = "${ctxPath}/chart/approveAttendence.do";
		var param = "id=" + dataItem.id
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			success : function(data){
				$.hideLoading()
				getAttendanceList();
			},error : function(Error){
				$.hideLoading()
				getAttendanceList();
				console.log(Error)
			}
		})
	}
	
	function takeAwayApprove(e){
		
		if(sessionStorage.getItem("name")==null || sessionStorage.getItem("name")=='undefined'){
			loginDialog.open();
			return ;
		}
		
		if(sessionStorage.getItem("lv")<=1){
			kendo.alert("권한이 없는 작업자 입니다.")
			return ;
		}
		
		$("span.k-window-title.k-dialog-title").text("")
		kendo.prompt("미승인 사유를 적어 주세요", "").then(function (data) {
			$("span.k-window-title.k-dialog-title").text("")
			$.showLoading()
			var dataItem = $(e).closest("tr")[0].dataset.uid;
			var row = $("#grid").data("kendoGrid")
	        .tbody
	        .find("tr[data-uid='" + dataItem + "']");
			var idx = grid.dataSource.indexOf(grid.dataItem(row));
			var dataItem = grid.dataSource.at(idx);
			 
			var url = "${ctxPath}/chart/takeAwayApprove.do";
			var param = "id=" + dataItem.id + "&msg=" +data
				
			$.ajax({
				url : url,
				data : param,
				async : false,
				type : "post",
				success : function(data){
					$.hideLoading()
					getAttendanceList();
				},error : function(Error){
					$.hideLoading()
					getAttendanceList();
					console.log(Error)
				}
			})
        }, function () {
        })
		
		
	}
	
	function cancelApprove(e){
		$.showLoading()
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var row = $("#grid").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + dataItem + "']");
		var idx = grid.dataSource.indexOf(grid.dataItem(row));
		var dataItem = grid.dataSource.at(idx);
		 
		var url = "${ctxPath}/chart/cancelApprove.do";
		var param = "id=" + dataItem.id;
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			success : function(data){
				getAttendanceList();
				$.hideLoading()
			},error : function(Error){
				getAttendanceList();
				$.hideLoading()
				console.log(Error)
			}
		})
	}
	
	function loginCheck(){
		$.showLoading()
		
		var url = "${ctxPath}/order/checkIdPw.do";
		var param = "empCd=" + $("#combobox").val() + "&pwd=" + $("#pwd").val()
			
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				if(json.length>0){
					sessionStorage.setItem("name", decode(json[0].name));
					sessionStorage.setItem("lv", json[0].lv);
					loginDialog.close()
					$("#logInBtn").text('로그아웃')
					$("#logInBtn").css({
					    "background": "darkred"
					})
					$("#info").css({
						"display":"inline"
					})
					
					$("#info").text(sessionStorage.getItem("name")+'님 반갑습니다.')
					
					$("#logInBtn").hover(function(){
						$(this).css("background-color", "red");
					    }, function(){
					    $(this).css("background-color", "darkred");
					});
					$("#logInBtn").attr("onclick","logout()");
				}else{
					popup.open()
					setTimeout(function(){
						popup.close(); 
					}, 3000);
				}
				$.hideLoading()
			},error : function(Error){
				$.hideLoading()
			}
		})
	}
	
	function logout(){
		
		kendo.alert("로그아웃 되었습니다.")
		
		sessionStorage.clear()
		$("#logInBtn").text('로그인')
		$("#logInBtn").css({
		    "background": "linear-gradient(#7777f5, #2f2fff)"
		})
		$("#logInBtn").attr("onclick","loginDialog.open()");
		$("#info").css({
			"display":"none"
		})
	}
	
	
	</script>
</head>
<body>
	
	<!-- <div id="time"></div> -->
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<%-- <Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td>
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr> --%>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td> --%>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="info">비로그인</div>
					<div id="logInBox" style="position: absolute;"><button id="logInBtn" onclick='loginDialog.open()'>로그인</button></div>
					<div id="buttonGroup">
						<div id="dateGroup" style="display: -webkit-inline-box;">
							<spring:message code="date_"></spring:message> : 
							<input type="date" id="sDate" readonly> ~ <input type="date" id="eDate" readonly>
						</div>
						<button id="searchBtn" onclick="getAttendanceList()" style="float: right;"><i class="fa fa-search" aria-hidden="true"></i>검색</button>
					</div>
					<div id="grid"></div>
				</td>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
			<Tr>
				<%-- <Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td> --%>
			</Tr>
		</table>
	 </div>
	<div id="dialog">
	<div class="ID"><p class="loginText">사원(사원번호): </p><input name="id" class="input" id="combobox" /></div>
	<div class="ID"><p class="loginText">비밀번호: </p><input name="pwd" class="input" type="password" id="pwd" /></div>
	<div id="popup">아이디 또는 비밀번호가 맞지 않습니다.</div>
	<div><center><button class="Login" id="Login" onclick="loginCheck()"><i class="fa fa-sign-in" aria-hidden="true"></i>로그인</button><button class="Close" onclick="loginDialog.close()" id="Close">닫기</button></center></div>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>

<style>
	.loginText{
		display: -webkit-inline-box;
	}
	.Login{
	    border-radius: 5px;
	    background: linear-gradient(#7777f5, #2f2fff);
	    border: 0px solid;
	    color: white;
	}
	.Login:hover{
	background: #2f2fff;
	}
	.Close{
	    border-radius: 5px;
	    color: black;
	}
</style>

</html>	