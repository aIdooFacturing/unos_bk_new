<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

#wrapper tbody tr td{
	background: white;
}

.edit-color{
	background: rgb(210,210,210) !important;
}

.k-grouping-row td{
	background: black !important;
}


.copyDateColor td{
	background : red !important;
}



</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>

	var selectlist = [];
	var kendotable ;
	
	var currentStart;
	var currentEnd;

	const loadPage = () =>{
		createMenuTree("om", "addTarget")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList
		getWorkerList();

		//gridTable => getTable()
		gridTable();
		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
// 		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
		
		$(".infoAlarm").css({
			"font-size" : getElSize(50) + "px"
			,"padding" : getElSize(18) + "px"
		});
	}

	
	
	//작업자 리스트
	function getWorkerList(){
		
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var table = "<tbody>";
							
				$(json).each(function(idx, data){
					
					
					var arr=new Object();
					arr.text=decode(data.name);
					arr.value=decode(data.id);
					arr.textname=decode(data.name);
					selectlist.push(arr)
				});
			}
		});
		
	}
	
	//filter
	function nameFilter(element) {
        element.kendoComboBox({
            dataSource: selectlist,
            dataTextField: "text",
			dataValueField: "value",
			placeholder: "작업자선택"
        });
    }
    
	//kendo grid edit
	function workauto(container, options, a){
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: selectlist,
			autoWidth: true,
			dataTextField: "text",
			dataValueField: "value",
            placeholder: "작업자 선택",
            select:function(e){
            }
        }).data("kendoComboBox");
	}
	
	//kendo grid template
	function worKerName(id){
		var chk=0;
		for(var i = 0; i < selectlist.length; i++){
			if(selectlist[i].value==id){
				chk++;
				return selectlist[i].textname;
			}
		}
		if(chk==0){
			return "미 배 치"
		}
	}

	function getToday2(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		return year + "-" + month + "-" + day	
	};
	
function copyWork(){
		
		if(getToday2()!= $("#sDate").val()){
			if(!confirm("저장하시겠습니까?")){
				return;
			}
		}
		
		$.showLoading();

		valueArray = [];
		addWorker = [];
		grid = $("#wrapper").data("kendoGrid");	        
		
		var ssss=grid.dataSource.data();
		var savedata=[]
		var extraSaveData=[]
		var capalist=[];

		var MaxGp=0;
		for(i=0; i<ssss.length; i++){
			if(isNaN(ssss[i].group)!=true){
				MaxGp=ssss[i].group
			}
			savedata.push(ssss[i]);
			
		}
		
		console.log(savedata)
		
		var groupCnt=MaxGp;
		$(savedata).each(function(idx, data){
			//주간
			var dataItem = grid.dataItem(data)
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntD;
			obj.tgRunTime = data.tgRunTimeD*3600;
			obj.tgDate = $("#sDate").val();
			obj.type = 2;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeD;
			obj.endTime = data.endTimeD;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}
			
			var changeemp=data.empD.split(',');
			if(changeemp.length==2){	
				obj.empCd=changeemp[1];
			}else{
			obj.empCd=data.empN;
			}
			
			if(obj.type==2 && obj.startTime =="08:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==2 && obj.startTime !="08:30"){ //추가작업자
				addWorker.push(obj)			
			}
			
			//야간
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntN;
			obj.tgRunTime = data.tgRunTimeN*3600;
			obj.tgDate = $("#sDate").val();
			obj.type = 1;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeN;
			obj.endTime = data.endTimeN;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}

			var changeemp=data.empN.split(',');
			if(changeemp.length==2){
				obj.empCd=changeemp[1];	
			}else{
			obj.empCd=data.empD;
			}
			if(obj.type==1 && obj.startTime =="20:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==1 && obj.startTime !="20:30"){ //추가작업자
				addWorker.push(obj)			
			}

			if(data.multiPrdNo>1){
				extraSaveData.push(obj)
			}
			groupCnt++;
			
			//capa
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 2;
			capalist.push(obj);

			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 1;

			capalist.push(obj);
			
		});
		
		
		var obj = new Object();
		obj.val = valueArray;
		
		var addobj = new Object();
		addobj.val = addWorker;
		
		var capaobj = new Object();
		capaobj.val = capalist;
		
		console.log(valueArray)
		console.log(addWorker)
		console.log(extraSaveData)
		

		
		
		var url = "${ctxPath}/order/addTargetCnt.do";
		var param = "val=" + JSON.stringify(obj) +
					"&addval=" + JSON.stringify(addobj) +
					"&capaval=" + JSON.stringify(capaobj);

		console.log(param)
 		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				if(data=="success"){
					alert("${save_ok}");
					getTable(); 
				}else{
					alert("저장에 실패 하였습니다.")
					getTable(); 
				}
			}
		});
		
	};
	
	
	function copy(e){
		return '<a class="k-button" href="#" id="toolbar-print_user" onclick="copyWork()">${day} <-> ${night} ${worker}</a>';
	}

	function prtD(e){
		return '<a class="k-button" href="#" id="toolbar-print_user" onclick="d_print()">${day} Print</a>';
	}
	function prtN(e){
		return '<a class="k-button" href="#" id="toolbar-print_user" onclick="n_print()">${night} Print</a>';
	}
	
	function onDataBound(){
		//추가된 항목 찾아서 라우팅 미배치 표시
		 $('td').each(function(){
	    	if($(this).text()=='라우팅 없음'){
    			$(this).addClass('routing')
    		}
	    	if($(this).text()=='미배치'){
    			$(this).addClass('routing')
    		}
    	});
		
	$('#wrapper tbody tr').each(function(){
		if($(this).text().indexOf("copyDateColor")==-1){
			
		}else{
			$(this).addClass('copyDateColor')
		}
		 
		if($(this).text().indexOf('oddRow')!=-1){
			$(this).addClass('oddRow')
		}else if($(this).text().indexOf('evenRow')!=-1){
			$(this).addClass('evenRow')
		}else{
			
		}
	});
		
		gridCss()
	}
	
	function gridCss(){

		$("#wrapper thead tr th").css({
			"font-size" : getElSize(38),
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$("#wrapper tbody tr td").css({
			"font-size" : getElSize(38)
		})
		
		$(".k-i-collapse").remove();
		$(".k-grouping-row td").css({
			"font-size":getElSize(5)
		});
		$(".k-group-cell").css({"font-size":getElSize(5)});

		$(".lineView").css({
		    "border-right": "2px solid gray"
		})
		
		//button size 조절
		$(".actionBtn").css({
			"border" : "1px solid black",
			"font-size" : getElSize(40),
			"border-radius" : getElSize(15)+"px",
			"padding" : getElSize(10),
			"padding-left" : getElSize(20),
			"padding-right" : getElSize(20),
			"margin-right" : getElSize(10),
			"cursor" : "pointer",
			"background" : "aliceblue",
			"color" : "black"
		});
		
	}
	
	function gridSort(){
		
		kendotable.dataSource.data().sort(function(a, b) { // 오름차순
		    //return a.startTimeD < b.startTimeD ? -1 : a.startTimeD > b.startTimeD ? 1 : 0;
		    if(a.name < b.name)
		    	return -1;
		    if(a.name > b.name)
		    	return 1;
		    if(a.prdNo < b.prdNo)
		    	return -1;
		    if(a.prdNo > b.prdNo)
		    	return 1;
		    if(a.startTimeD < b.startTimeD)
		    	return -1;
		    if(a.startTimeD > b.startTimeD)
		    	return 1;
		    if(a.compareStartTimeN < b.compareStartTimeN)
		    	return -1;
		    if(a.compareStartTimeN < a.compareStartTimeN)
		    	return 1;
		});

// 		$(kendotable.dataSource.data()).each(function(idx,data){
// 			console.log(data.compareStartTimeN)
// 		})
		
		var list = kendotable.dataSource.data()
		
	}
	
	function testTimeD(container, options){

 		currentStart=options.model.startTimeD;
		currentEnd=options.model.endTimeD; 
	//	startTime="08:30"
	//	endTime="20:30"
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"08:30",
	    	    max:"20:30",
	    	    change:function(e){
	    	    	//시간 타입을 format 변경하여 저장하기.
	    	    	if(options.field=="startTimeD"){
		    	    	var hour = options.model.startTimeD.getHours();
	           			var minute = options.model.startTimeD.getMinutes();
	    	    	}else if(options.field=="endTimeD"){
	    	    		var hour = options.model.endTimeD.getHours();
	           			var minute = options.model.endTimeD.getMinutes();
	    	    	}
           			
	    	    	options.model.startTimeD = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
	    	    	
	    	    	var list = kendotable.dataSource.data();
	    	    	
	    			var row = kendotable.tbody.find("tr[data-uid='" + options.model.uid + "']");
	    			var idx = kendotable.dataSource.indexOf(kendotable.dataItem(row));
	    			
	    			var maxLength = kendotable.dataSource.data().length;
	    			
	    			//작업 종료시간은 변경 못하게 막기
					if(options.field=="endTimeD" && maxLength!=1){
						alert("종료시간을 변경할수 없습니다. ( 작업시작 시간을 변경해주세요 )")
						
	    				options.model.startTimeD = currentStart
	    				options.model.endTimeD = currentEnd
	    				
	    				//grid 재정렬
		    	    	gridSort();
						
		    	    	// grid CSS
		    	    	gridCss();
		    	    	//kendotable.dataSource.fetch()

	    				return;
					}

	    			// 데이터가 한개일 경우에는 수정 불가능하게 하기
	    			if(maxLength==1){
	    				alert("시작,종료 시간을 변경할수 없습니다. 작업자를 먼저 추가해주세요.");
	    				
	    				options.model.startTimeD = currentStart
	    				options.model.endTimeD = currentEnd
	    				
	    				//grid 재정렬
		    	    	gridSort();
		    	    	// grid CSS
		    	    	gridCss();
		    	    	//kendotable.dataSource.fetch()

	    				return;
	    			}else{	// 종료시간을 시작시간 일치시키기
	    				//젤 위쪽의 데이터먼저 수정시키기 위해서
	    			
	    				console.log(list);
	    				console.log(idx);
	    				if(list[idx-1].endTimeD == currentStart && list[idx-1].startTimeD == currentStart){
	    					alert("이전 작업을 먼저 변경해주세요");
	    					options.model.startTimeD = currentStart
		    				options.model.endTimeD = currentEnd
		    				
		    				//grid 재정렬
			    	    	gridSort();
			    	    	// grid CSS
			    	    	gridCss();
			    	    	//kendotable.dataSource.fetch()

		    				return;
	    				}else{	// 최종 변경시키기
	    					
	    					console.log("===최종 선택===")
	    					console.log(options.model)
	    					
	    					// 시작시간 변경시 그전작업자의 시작시간보다 빠르지 못하게 막기
	    					if(options.model.startTimeD <= list[idx-1].startTimeD){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						options.model.startTimeD = currentStart
			    				options.model.endTimeD = currentEnd
			    				
			    				//grid 재정렬
				    	    	gridSort();
				    	    	// grid CSS
				    	    	gridCss();
				    	    	//kendotable.dataSource.fetch()

			    				return;
	    					}
		    				list[idx-1].endTimeD = options.model.startTimeD
	    				}
	    			}
	    	    	
	    	    	//kendotable.dataSource.fetch()
	    	    	
	    	    	console.log(options)
	    	    	console.log(container)
	    	    	
	    	    	//grid 재정렬
	    	    	gridSort();
	    	    	
	    	    	// grid CSS
	    	    	gridCss(); 
	    	    }
	   	});
	}	
	 
	function testTimeN(container, options){
		currentStartN=options.model.startTimeN;
		currentEndN=options.model.endTimeN;
		
		compareStartTimeN = options.model.compareStartTimeN;
		compareEndTimeN = options.model.compareEndTimeN;
		
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"20:30",
	    	    max:"08:30",
	    	    change:function(e){
	    	    	//시간 타입을 format 변경하여 저장하기.
	    	    	if(options.field=="startTimeN"){
		    	    	var hour = options.model.startTimeN.getHours();
	           			var minute = options.model.startTimeN.getMinutes();
	    	    	}else if(options.field=="endTimeN"){
	    	    		var hour = options.model.endTimeN.getHours();
	           			var minute = options.model.endTimeN.getMinutes();
	    	    	}
	    	    	
					options.model.startTimeN = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
	    	    	
	    	    	var list = kendotable.dataSource.data();
	    	    	
	    			var row = kendotable.tbody.find("tr[data-uid='" + options.model.uid + "']");
	    			var idx = kendotable.dataSource.indexOf(kendotable.dataItem(row));
	    			var maxLength = kendotable.dataSource.data().length;
	    			
	    			//작업 종료시간은 변경 못하게 막기
					if(options.field=="endTimeN" && maxLength!=1){
						alert("종료시간을 변경할수 없습니다. ( 작업시작 시간을 변경해주세요 )")

	    				options.model.startTimeN = currentStartN
	    				options.model.endTimeN = currentEndN
	    				
	    				//grid 재정렬
		    	    	//gridSort();
		    	    	// grid CSS
		    	    	gridCss();
		    	    	//kendotable.dataSource.fetch()

	    				return;
					}
					
					// 데이터가 한개일 경우에는 수정 불가능하게 하기
	    			if(maxLength==1){
	    				alert("시작,종료 시간을 변경할수 없습니다. 작업자를 먼저 추가해주세요." + currentStartN);
	    				
	    				options.model.startTimeN = currentStartN
	    				options.model.endTimeN = currentEndN
	    				
	    				//grid 재정렬
		    	    	//gridSort();
		    	    	// grid CSS
		    	    	gridCss();
		    	    	//kendotable.dataSource.fetch()

	    				return;
	    			}else{	// 종료시간을 시작시간 일치시키기
	    				console.log(list[idx-1].endTimeN,currentStartN)
	    				console.log(list[idx-1].startTimeN,currentStartN)
	    				//젤 위쪽의 데이터먼저 수정시키기 위해서
	    				if(list[idx-1].endTimeN == currentStartN && list[idx-1].startTimeN == currentStartN){
	    					alert("이전 작업을 먼저 변경해주세요");
	    					options.model.startTimeN = currentStartN
		    				options.model.endTimeN = currentEndN
		    				
			    	    	gridCss();
			    	    	//kendotable.dataSource.fetch()

		    				return;
	    				}else{	// 최종 변경시키기
	    					//시간 계산을 위해 시간 +30 변환	 00:00 ~ 08:30 일 경우
	    					
							if(options.model.startTimeN >= "00:00" && options.model.startTimeN <="08:30"){
	    						//최종 결졍 ( 비교 시작시간 변경)
	    						options.model.compareStartTimeN  = plusTh(options.model.startTimeN);
	    					}else{
	    						options.model.compareStartTimeN = options.model.startTimeN;
	    					}
	    					
	    					// 그전작업자의 시작시간보다 빠르거다 같을경우 제외처리
	    					console.log("==== 바교 ====")
	    					console.log("현재값 : " + options.model.compareStartTimeN + "<= , 이전값 : " + list[idx-1].compareStartTimeN);
	    					if(options.model.compareStartTimeN <= list[idx-1].compareStartTimeN){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						
	    						options.model.startTimeN = currentStartN
			    				options.model.endTimeN = currentEndN
			    				options.model.compareEndTimeN = compareEndTimeN;
			    				options.model.compareStartTimeN = compareStartTimeN;
			    				//grid 재정렬
				    	    	//gridSort();
				    	    	// grid CSS
				    	    	gridCss();
				    	    	//kendotable.dataSource.fetch()

			    				return;
	    					}
	    					
	    					
	    					//최종 결정 ( 실제 종료시간 변경 )
	    					list[idx-1].endTimeN = options.model.startTimeN	
	    					//최종 결정 ( 비교 종료시간 변경)
	    					list[idx-1].compareEndTimeN = options.model.compareStartTimeN;
	    					
	    				}
	    			}
					
	    			//kendotable.dataSource.fetch()
	    	    	
	    	    	console.log(options)
	    	    	console.log(container)
	    	    	
	    	    	//grid 재정렬
	    	    	gridSort();
	    	    	
	    	    	// grid CSS
	    	    	gridCss();
	    	    	
	    	    }
	   	});
	
	}
	
	function insertRow(e){
		
		gridSort();
		
		$('.actionBtn').attr('disabled', true);
		setTimeout(function() {
	
			console.log("---insert---")
			var row = $(e).closest("tr");
			var rowIdx = $("tr", kendotable.tbody).index(row);
			var dataItem = $(e).closest("tr")[0].dataset.uid;
			var initData = kendotable.dataItem(row);
			var length = kendotable._data.length;
			var loopChk=0; // 1이면 빠져나감
			var list=[];
			for(i=0; i<length; i++){
				if(kendotable._data[i].name==initData.name ){
					if((kendotable._data[i].startTimeD==kendotable._data[i].endTimeD /* && kendotable._data[i].empD=="null" */) && (kendotable._data[i].prdNo==initData.prdNo) && kendotable._data[i].startTimeN==kendotable._data[i].endTimeN){
						loopChk=1;
						alert("작업시간을 선택해 주세요");
						$.hideLoading()
						$('.actionBtn').attr('disabled', false);
						return;
					}
					if(kendotable._data[i].prdNo!=initData.prdNo){
						list.push(kendotable._data[i].prdNo)
					}
				}
			}
			
			console.log(list)
			if(loopChk==1){
				console.log("return;");
				$.hideLoading()
				$('.actionBtn').attr('disabled', false);
				return false;
			}
			
			if(list.length>=1){
				kendotable.dataSource.insert({
					 routing: initData.routing,
					 dvcId: initData.dvcId,
					 name: initData.name,
		             tgCntD: initData.tgCntD,
		             tgRunTimeD: initData.tgRunTimeD,
		             cntPerCyl: initData.cntPerCyl,
		             tgCntN: initData.tgCntN,
		             tgRunTimeN: initData.tgRunTimeN,
		             capa: initData.capa,
		             empD: initData.empD,
		             empN: initData.empN,
		             prdNo: list[0],
		             uph: initData.uph,
		             startTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
		             endTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
		             startTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
		             endTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
		             compareStartTimeN: "38:30",
		             compareEndTimeN: "38:30",
		             copyDateColor:"copyDateColor"
				});
			}
			kendotable.dataSource.insert({
				 routing: initData.routing,
				 dvcId: initData.dvcId,
				 name: initData.name,
	             tgCntD: initData.tgCntD,
	             tgRunTimeD: initData.tgRunTimeD,
	             cntPerCyl: initData.cntPerCyl,
	             tgCntN: initData.tgCntN,
	             tgRunTimeN: initData.tgRunTimeN,
	             capa: initData.capa,
	             empD: initData.empD,
	             empN: initData.empN,
	             prdNo: initData.prdNo,
	             uph: initData.uph,
	             startTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
	             endTimeD: moment().set({'hour' : 20, 'minute' : 30}).format("HH:mm"),
	             startTimeN:  moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
	             endTimeN: moment().set({'hour' : 08, 'minute' : 30}).format("HH:mm"),
	             compareStartTimeN: "38:30",
	             compareEndTimeN: "38:30",
	             copyDateColor:"copyDateColor"
			});
			
			/* grid.dataSource.sort([
				{ field: "prdNo", dir: "asc" },{field:"startTimeD",dir:"asc"},{field:"startTimeN",dir:"asc"}
			]) */
			$('.actionBtn').attr('disabled', false);
	
			gridSort()
			
			$.hideLoading()
			
		}, 10);
		
		$.showLoading()
	}
	
	function deleteRow(e){
		
		if(confirm("삭제하시겠습니까?")==false){
			return false;
		}
		
		$('.actionBtn').attr('disabled', true);
		
		setTimeout(function() {
			var row = $(e).closest("tr");
			var rowIdx = $("tr", kendotable.tbody).index(row);
			var dataItem = $(e).closest("tr")[0].dataset.uid;
				dataItem = kendotable.dataItem(row);
			var check = 0;
			var length = kendotable.dataSource.data().length	
			var table= kendotable.dataSource.data();
			var removelist=[];
			var a="";
			console.log(dataItem.endTimeD);
			console.log()
			
			for(var i=0;i<length;i++){			 
				if(kendotable.dataSource.data()[i].dvcId==dataItem.dvcId && kendotable.dataSource.data()[i].prdNo == dataItem.prdNo){
					 check++;
				}
				
				if(table[i].endTimeD==dataItem.startTimeD && table[i].name==dataItem.name && table[i].endTimeN==dataItem.startTimeN ){
//					console.log(table[i])
					table[i].set("endTimeD",dataItem.endTimeD)
					table[i].set("endTimeN",dataItem.endTimeN)
				}
				
				if(table[i].name==dataItem.name && table[i].prdNo!=dataItem.prdNo && table[i].startTimeD==dataItem.startTimeD && table[i].startTimeN==dataItem.startTimeN){
					removelist.push(i)
				}
			}
			
			if(check<=1 || dataItem.startTimeD=="08:30"){
				alert(" 1개 일때는 지울 수 없습니다. ")
				$('.actionBtn').attr('disabled', false);
				$.hideLoading()
				return;
			}else{
				console.log("--delete--")
				console.log(removelist)
				console.log(dataItem.dvcId)
				console.log(dataItem.group)
				
				var url = "${ctxPath}/order/deleteJobSplit.do";
				var param = "dvcId=" + dataItem.dvcId + 
							"&tgDate=" + $("#sDate").val() +
							"&group=" + dataItem.group;
				
				$.ajax({
					url : url,
					data : param,
//					dataType : "json",
					type : "post",
					success : function(data){
// 						saveRow();
// 						console.log("suc")
					}
				});
						
				
				
				
				
				if(removelist.length!=0){
					kendotable.dataSource.remove(table[removelist[0]])
				}
				kendotable.dataSource.remove(dataItem)
			}
			$.hideLoading()
			$('.actionBtn').attr('disabled', false);
		}, 10);
		$.showLoading()
	}
	
	function gridTable(){
		kendotable = $("#wrapper").kendoGrid({
			dataSource:{
				sort: [{
                	field: "prdNo" , dir:"asc" 
                },{
                	field: "startTimeD" , dir:"asc"
                },{
                	field:"startTimeN",dir:"asc"
                }]
			},
			sort: [{
            	field: "prdNo" , dir:"asc" 
            },{
            	field: "startTimeD" , dir:"asc"
            },{
            	field:"startTimeN",dir:"asc"
            }],
			toolbar: [{name:$("#save").val()}, {name:$("#copy_data").val(),template:'#=copy()#'}/* ,{name: "prtD",template:'#=prtD()#'},{name: "prtN",template:'#=prtN()#'} */],
			height:getElSize(1650),
			editable:true,
			filterable: {
                extra: false
            },
            groupable: false,
            dataBound: onDataBound,
			columns:[{
					field:"dvcId"
					,title:"　"
					,width : getElSize(120)
// 					,locked: true
// 					,lockable: false
				},{
					field:"name"
					,title:"${device}"
					,groupHeaderTemplate: '　'
					,width : getElSize(500)
//  					,locked: true
// 					,lockable: false
					,filterable:{
        				 multi: true,
        	             search: true
        			}
				},{
					field:"cntPerCyl"
					,title:"싸이클<br>생산량"
					,width : getElSize(190)
//  					,locked: true
// 					,lockable: false
					,filterable: false
					,attributes: {
        				"class" : "edit-color"
        			}
				},{
					field:"uph"
					,title:"UPH"
					,width:getElSize(165)
//  					,locked: true
// 					,lockable: false
					,filterable: false
				},{
					field:"capa"
					,title:"${working_capa}"
					,width:getElSize(170)
//  					,locked: true
// 					,lockable: false
					,filterable: false
					,attributes: {
						"class" : "edit-color lineView"
					}
				},{
					field :"rowClass"
					,width : getElSize(1)	
					,hidden : true
				},{
					field:"copyDateColor"
					,width : getElSize(1)
					,hidden : true
				},{
					title:"${day}"
					,columns:[{
						field:"tgCntD"
						,title:"목표<br>수량"
						,width : getElSize(190)
						,filterable: false
						,attributes: {
							"class" : "edit-color"
            			}
					},{
						field:"tgRunTimeD"
						,title:"${ophour} <br> (h)"
						,width:getElSize(220)
						,filterable: false
						,attributes: {
							"class" : "edit-color"
            			}
					},{
						field:"empD"
						,title:"${worker}"
						,editor: workauto
						,template:"#=worKerName(empD)#"
						,width:getElSize(250)
						,attributes: {
							"class" : "edit-color"
	        			},filterable: {
	                        ui: nameFilter,
	                        cell: {
	                            showOperators: false,
	                            operator: "contains"
	                        }
	                    }
					},{
						field: "startTimeD",
						title: "${start}${time_}",
						width : getElSize(220),
						format:"{0:HH:mm}",
						editor : testTimeD,
						filterable: false,
						attributes: {
							"class" : "edit-color"
	        			}
					},{
						field: "endTimeD",	
						title: "${end}${time_}",
						width : getElSize(220),
						editor : testTimeD,
						format:"{0:HH:mm}",
						filterable: false,
						attributes: {
							"class" : "edit-color lineView"
	        			}
					}]},
				{
					title:"${night}"
					,columns:[{
						field:"tgCntN"
						,title:"목표<br>수량"
						,width:getElSize(190)
						,filterable: false
						,attributes: {
							"class" : "edit-color"
            			}
					},{
						field:"tgRunTimeN"
						,title:"${ophour}<br>(h)"
						,width:getElSize(220)
						,filterable: false
						,attributes: {
							"class" : "edit-color"
           				}
					},{
						field:"empN"
						,title:"${worker}"
						,editor: workauto
						,template:"#=worKerName(empN)#"
						,width:getElSize(250)
						,filterable: {
                            ui: nameFilter
                        }
						,attributes: {
							"class" : "edit-color"
            			}
					},{
						field: "startTimeN",
						title: "${start}${time_}",
						width : getElSize(220),
						format:"{0:HH:mm}",
						editor : testTimeN,
						filterable: false,
						attributes: {
							"class" : "edit-color"
	        			}
					},{
						field: "endTimeN",
						title: "${end}${time_}",
						width : getElSize(220),
						editor : testTimeN,
						format:"{0:HH:mm}",
						filterable: false,
						attributes: {
							"class" : "edit-color lineView"
	        			}
					},{
						title:"${Split_work}",
						width : getElSize(350),
						template : "<button class='actionBtn' onclick='insertRow(this)'>${divide}</button> <button class='actionBtn' onclick='deleteRow(this)'>${del}</button>",
					}]
			}
		]}).data("kendoGrid");
		
		getTable()
	}
	
	function getTable(){
		
		var list=[];
		var multiPrdNoList = [] ;
		
		$.showLoading()
		
		var url = "${ctxPath}/order/getTargetData.do";
		
		var param = "shopId=" + shopId + 
					"&date=" + $("#sDate").val();
		showDate=$("#sDate").val();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data.dataList)
				
				var json = data.dataList
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.tgRunTimeD=data.tgRunTimeD/60/60;
					data.tgRunTimeN=data.tgRunTimeN/60/60;
					
					//추가작업은 빨강표시
					if(data.multiPrdNo==9999){
						data.copyDateColor="copyDateColor";
					}
					
					//야간 시간변경을 위한 로직
					if(data.startTimeN >="00:00" && data.startTimeN <="08:00"){
						data.compareStartTimeN = plusTh(data.startTimeN);
						data.compareEndTimeN = plusTh(data.endTimeN);
					}else{
						data.compareStartTimeN = data.startTimeN;
						data.compareEndTimeN = data.endTimeN;
					}
				})
				
				var dataSource = new kendo.data.DataSource({
					data : json
					,group: {
					    field: "name",
					    dir: "asc",
					}
					,sort: [{
						field: "prdNo" , dir:"asc" 
					},{
						field: "startTimeD" , dir:"asc"
					},{
						field:"startTimeN",dir:"asc"
					}]
					,schema: {
	                    model: {
	                      id: "dvcId",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                    	 dvcId: { editable: false },
	                    	 routing:{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         name: { editable: false, nullable: true , attributes:{style:"text-align:right;"},sortable:false},
	                         tgCntD: { validation: { required: true } },
	                         tgRunTimeD: { validation: { required: true } },
	                         cntPerCyl: { validation: { required: false },nullable:true },
	                         tgCntN: { validation: { required: true } },
	                         tgRunTimeN: { validation: { required: true } },
	                         capa: { editable: true , type:"number" },
	                         empD: { validation: { required: false },nullable:true },
	                         empN: { validation: { required: false },nullable:true },
	                         prdNo: { editable: false ,dir: "asc"},
	                         uph: { editable: false },
	                         startTimeD : {editable:true},
	                         copyDateColor:{editable:false }
	                      }
	                    }
	                }
				})
				
				$.hideLoading();

				kendotable.setDataSource(dataSource);
				
				$(".k-grid-저장").unbind("click")
				$(".k-grid-저장").click(function(){

					if(showDate==$("#sDate").val()){
						if(!confirm("저장하시겠습니까?")){
							return;
						}else{
							saveRow()
						}
					}else{
						if(!confirm("일자가 "+$("#sDate").val()+" 로 변경되었습니다"+"\n저장하시겠습니까?")){
							return;
						}else{
							saveRow()
						}
					}
					$(".k-alt").css({
						"background-color" : "#BDBDBD"
					})	
				});
		
			}
		})
	}
	
	// 시간 + 30 하여 비교하기 위해서 함수 생성
	function plusTh(time){
		console.log(time)
		//hour = "3" + options.model.startTimeN.substring(1,5);
		hour = time.substring(0,1);
		hour = Number(hour) + 3;
		time = hour + time.substring(1,5);
		
		return time;
	}
	
	function saveRow(){
		console.log($("#sDate").val());
		
		$.showLoading();

		valueArray = [];
		addWorker = [];
		capalist = [];
		grid = $("#wrapper").data("kendoGrid");	        
		
		var ssss=grid.dataSource.data();
		var savedata=[]
		var extraSaveData=[]
		
		var MaxGp=0;
		for(i=0; i<ssss.length; i++){
			if(isNaN(ssss[i].group)!=true){
				MaxGp=ssss[i].group
			}
			savedata.push(ssss[i]);
			
		}
		
		/* for(i=0; i<savedata.length; i++){
			var chk=0;
			for(j=0; j<savedata.length; j++){
				if(savedata[j].name==savedata[i].name){
					chk++;
				}
			}
			if(chk>1){
				savedata.splice(i,1);
			}
		} */
		
		console.log(savedata)
		
		var groupCnt=MaxGp;
		$(savedata).each(function(idx, data){
			//주간
			var dataItem = grid.dataItem(data)
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntD;
			obj.tgRunTime = data.tgRunTimeD*3600;
			obj.tgDate = $("#sDate").val();
			obj.type = 2;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeD;
			obj.endTime = data.endTimeD;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}
			
			var changeemp=data.empD.split(',');
			if(changeemp.length==2){	
				obj.empCd=changeemp[1];
			}else{
			obj.empCd=data.empD;
			}
			
			if(obj.type==2 && obj.startTime =="08:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==2 && obj.startTime !="08:30"){ //추가작업자
				addWorker.push(obj)			
			}
			
			//야간
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.tgCyl = data.tgCntN;
			obj.tgRunTime = data.tgRunTimeN*3600;
			obj.tgDate = $("#sDate").val();
			obj.type = 1;
			obj.cntPerCyl = data.cntPerCyl;
			obj.startTime = data.startTimeN;
			obj.endTime = data.endTimeN;
			// 새로 작업자 추가하여 group 이 없을 경우
			if(data.group==undefined){
				obj.count = groupCnt;
			}else{
				obj.count = data.group;
			}

			var changeemp=data.empN.split(',');
			if(changeemp.length==2){
				obj.empCd=changeemp[1];	
			}else{
			obj.empCd=data.empN;
			}
			if(obj.type==1 && obj.startTime =="20:30"){ //기존작업자
				valueArray.push(obj)
			}else if(obj.type==1 && obj.startTime !="20:30"){ //추가작업자
				addWorker.push(obj)			
			}

			if(data.multiPrdNo>1){
				extraSaveData.push(obj)
			}
			groupCnt++;
			
			//capa
			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 2;
			capalist.push(obj);

			var obj = new Object();
			obj.dvcId = data.dvcId;
			obj.name = data.name;
			obj.capa = data.capa;
			obj.type = 1;

			capalist.push(obj);
			
		});
		
		var obj = new Object();
		obj.val = valueArray;
		
		var addobj = new Object();
		addobj.val = addWorker;
		
		var capaobj = new Object();
		capaobj.val = capalist;
		
		console.log(valueArray)
		console.log(addWorker)
		console.log(extraSaveData)
		console.log("--==--")
		console.log(capaobj)
		var url = "${ctxPath}/order/addTargetCnt.do";
		var param = "val=" + JSON.stringify(obj) +
					"&addval=" + JSON.stringify(addobj) +
					"&capaval=" + JSON.stringify(capaobj);
		console.log(param)
 		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				if(data=="success"){
					alert("${save_ok}");
					getTable(); 
				}else{
					alert("저장에 실패 하였습니다.")
					getTable(); 
				}
			}
		});
	}
</script>
<body>
	<input type="hidden" value="<spring:message code="save"></spring:message>" id='save'>
	<input type="hidden" value="<spring:message code="day_to_Night"></spring:message>" id='copy_data'>

	<div id="container">
		<div id="div1" style="display: table; width: 100%;">
			<div style="width: 50%; display: table-cell; vertical-align: middle; text-align: center;">
				<span class="infoAlarm" style="background:gray; border-radius:5px;">
					<spring:message code="routing_manual"></spring:message>
				</span>
			</div>
			<div style="width: 25%; display: table-cell; vertical-align: middle; text-align: center;">
				<span class="infoAlarm" style="background:gray; border-radius:5px;">
					※삭제 후 저장버튼 클릭 부탁드립니다.※
				</span>
			</div>
			<div style="width: 25%; display: table-cell; vertical-align: middle; text-align: right;">
				<spring:message code="date_"></spring:message> : <input type="text" id="sDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
			</div>
		</div>
		
		<div id="div2">
			<div id="wrapper">
			</div>
		</div>
	</div>	
</body>
</html>	