<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/x-kendo-template" id="windowTemplate">
	<center>
    	<button class="k-button" id="yesButton">${check}</button>
    	<button class="k-button" id="noButton">${cancel}</button>
	</center>
</script>
<script type="text/javascript">
	var chk_del = "${chk_del}";
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>

<%-- <script type="text/javascript" src="${ctxPath }/js/jquery.ajax-cross-origin.min.js"></script>
 --%>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
select{
	padding: .1em .5em;
    width: 12.4em;
}

#table22{
	border-bottom : 1px solid #1E1E23  !important;
	border-right : 1px solid #1E1E23 !important;
	border-left : 1px solid #1E1E23  !important;
	border-top : 1px solid #1E1E23 !important;
}
.k-grid-header-wrap{
	border-right : 1px solid #1E1E23 !important;
}

.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
}
.k-grid-header thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid-content tbody > tr
{
 background : #DCDCDC ;
 text-align: center;
}
.k-grid-content tbody > .k-alt
{
 background : #F0F0F0 ;
 text-align: center;
}

.k-grid-content tbody > tr:hover, .k-grid-content tbody > .k-alt:hover 
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}

button:hover{
	cursor: pointer;
	background: #6699F0 !important;
	border-color : #6699F0 !important;
}

select option{
	background: black;
	color: white;
}

</style> 
<script type="text/javascript">

const loadPage = () =>{
	createMenuTree("maintenance","workerMaanger")
}

	//작업자 변경시 추가 버튼 클릭했는지
	//변경 버튼을 클릭했는지 확인하기 위한 변수
	//groupwareAPI와 연동하는데
	//추가 API 와 변경 API가 다르므로
	//각 작업마다 맞는 API로 보내주기 위해서
	//add => 추가 버튼
	//update => 변경 버튼
	//으로 작업할 예정
	//2019-10-02;
	
	var clickBtn;
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};

	function closeForm(){
		$("#insertForm").css({
			"z-index" : -999,
			"display" : "none"
		});
		closeCorver()
	}
	
	function addWorker(){
		
		console.log(clickBtn);
		console.log(originGroupId);
		
		var emailChk = /^[A-Za-z0-9+]*$/;
		
		//필수항목 체크하기
		if($("#name").val()==""){
			alert("이름을 입력해주세요");
			$("#name").focus();
			return;
		}else if($("#pwd").val()==""){
			alert("비밀번호를 입력해주세요");
			$("#pwd").focus();
			return;
		}else if($("#part").val()==null){
			alert("부서를 선택해주세요");
			$("#part").focus();
			return;
		}else if($("#position").val()==undefined){
			alert("직위를 입력해주세요");
			$("#position").focus();
			return;
		}
		/* else if($("#email").val()==""){
			alert("email 입력해 주세요");
			$("#email").focus();
			return;
		}else if(!emailChk.test($("#email").val())){
			alert("영어 & 숫자 조합만 가능합니다.");
			$("#email").focus();
			return;
		} */
		
		//그룹웨어 user,admin 구분 저장
		var ty = "";
		if ($("#LV").val()==1){
			ty = "user"
		}else{
			ty = "admin"
		}
		
		var orgId;
		$(groupListApi).each(function(idx,data){
			if(data.name==originGroupId){
				orgId = data.id;
			}
		})
		if(orgId==undefined){
			orgId = $("#part option:selected").attr("name");
		}
		var url = "${ctxPath}/chart/addWorker.do";
		var param = "id=" + $("#workerId").html() + 
					"&barcode=" + $("#barcodeId").val() + 
					"&name=" + $("#name").val() + 
					"&pwd=" + $("#pwd").val() + 
					"&partTy=" + $("#part option:selected").attr("name") + 
					"&part=" + $("#part").val() + 
					"&position=" + $("#position").val() + 
					"&email=" + $("#email").val()+
					"&LV=" + $("#LV").val() +
					"&ty=" + ty +
					"&originGroupId=" + orgId +
					"&shopId=" + shopId +
					"&token=" + token;
		
		console.log(param);

		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success"){
					
					//GROUPWARE API 각각 맞는 상황으로 이동하기
					/* if(clickBtn=="update"){
						//변경클릭시 GROUPWARE API 보내기
						APIUPDATE();
						
						
					}else if(clickBtn=="add"){
						//추가 클릭시 GROUPWARE API 보내기
						APIADD();
						
					} */
					
					closeForm();
					getWorkerList();
				}
				$.hideLoading();
			}
		});
	};
	
	$(function(){
		
		//그룹웨어 API 참조하기
		//부서 SELECT 가져오기
		bkpiApiGroups();
		
		windowTemplate = kendo.template($("#windowTemplate").html());
		delMsg = $("#window").kendoWindow({
		    title: chk_del,
		    visible: false, //the window will not appear before its .open method is called
		    width: getElSize(1000) + "px",
		    height: getElSize(250) + "px",
		}).data("kendoWindow");
		
		getWorkerList();
		bindEvt2();
		$(".excel").click(csvSend);
		setEl();
		setDate();
		
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#startDate_form").val() + "," + $("#startTime_form").val());
		var endTime = new Date($("#endDate_form").val() + "," + $("#endTime_form").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return timeDiff;
	}
	
	
	function bindEvt2(){

	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	var csvOutput;
	function csvSend(){
		var sDate, eDate;
		
		sDate = $("#sDate").val();
		eDate = $("#eDate").val();
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};

	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(40),
			"width" : $("#container").width() - getElSize(80),
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(25),
			"padding" : getElSize(15),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : -1,
			"background-color" : "rgba(0,0,0,0.7)",
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8),
			"color" : "black",
		})

		$("#goMakeNo").css({
			"height" : getElSize(100),
			"width" : getElSize(350),
			"border-color" : "#9B9B9B",
			"background" : "antiquewhite",
			"border-radius" : getElSize(8),
			"color" : "black",
		})
		
		$("select").css({
			"font-size" : getElSize(48),
			"height" : getElSize(80),
			"border": "1px #F0F0F0 #999",
           "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "#F0F0F0",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "black",
            "border" : "none",
			"padding-top": getElSize(10),
			"padding-bottom": getElSize(10),
			"width" : getElSize(450)
		});
		
		
		$("input").css({
			 "border": "1px #F0F0F0 #999",
	            "width" : getElSize(450),
	            "height" : getElSize(80),
	            "font-family": "inherit",
	            "background-color" : "F0F0F0",
	            "z-index" : "999",
	            "border-radius": "0px",
	            "appearance":"none",
	            "background-size" : getElSize(60),
	            "color" : "black",
	            "border" : "none",
	            "font-size" : getElSize(48),
				"border-color" : "#222327"
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		$(".alarmTable td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(35),
			"border": getElSize(1) + "px solid black"
		});
		
		$("#wrapper").css({
			"height" :getElSize(1550),
			"width" : $(".right").width(),
			"overflow" : "auto"
		});
		
		$("#insertForm").css({
			"width" : getElSize(1500),
			"position" : "absolute",
			"z-index" : -999,
			"display" : "none",
		    "border": getElSize(5) + "px solid #00C6FF"
		});
		
		
		$("#insert table td, #update table td").css({
			"color" : "black",
			"font-size" : getElSize(36),
			"padding" : getElSize(30),
			"text-align" : "Center",
			"background-color" : "#DCDCDC"
		});
		
		$("#insertForm").css({
			"left" : (originWidth/2) - ($("#insertForm").width()/2),
			"top" : (originHeight/2) - ($("#insertForm").height()/2)
		});
		
		$(".table_title").css({
			"background-color" : "#353542",
			"color" : "white"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};

	
	var className = "";
	var classFlag = true;

	function getWorkerList(){
		classFlag = true;
		var url = "${ctxPath}/common/getAllWorkerList.do";
		var list=[];

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				$(json).each(function(idx, data){
					if(data.seq!="."){
						var arr=new Object();
						
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						arr.a=data.id;
						arr.b=decodeURIComponent(data.name).replace(/\+/gi, " ");
						arr.c=decodeURIComponent(data.part).replace(/\+/gi, " ");
						arr.d=data.email;
						arr.LV=data.LV;
						arr.barcode = data.barcode;
						arr.img = data.img;
						arr.imgChk = true;
						arr.originGroupId=decodeURIComponent(data.part).replace(/\+/gi, " ");
						arr.token = data.token;
						list.push(arr);

					}
				});

				$("#table22").kendoGrid({
					height:getElSize(1580),
					dataSource:list,
					sortable : true,
					dataBound:function(e){
						//성적서 이미지 클릭시 이벤트 발생시키기
				    	$("span[name=imgPlus]").click(function(){
				    		var dataItem= $(this).closest("tr")[0].dataset.uid;
				    		var grid = $("#table22").data("kendoGrid")
				    		var row = $("#table22").data("kendoGrid")
				    		      .tbody
				    		      .find("tr[data-uid='" + dataItem + "']");
				    		var initData = grid.dataItem(row)
				    		
				    		console.log(initData);
				    		
					    	$("#dtImg").off();
				    		$("#dtImg").click();
							$("#dtImg").change(function() {
								dtImgAdd(initData);
							});
//				    		
				    		
						});
				    	//성적서 이미지 클릭시 이벤트 발생시키기
				    	$("span[name=imgView]").click(function(){
				    		var dataItem= $(this).closest("tr")[0].dataset.uid;
				    		var grid = $("#table22").data("kendoGrid")
				    		var row = $("#table22").data("kendoGrid")
				    		      .tbody
				    		      .find("tr[data-uid='" + dataItem + "']");
				    		var initData = grid.dataItem(row)
				    		console.log(initData.img)
				    		if(initData.img==undefined || initData.img=="" || initData.img=="undefined"){
				    			alert("등록된 이미지가 없습니다.");
				    			return;
				    		}else{
								window.open("http://bkjm.iptime.org/idcard/"+initData.img,"성적서","width=" + getElSize(2500) + ",height= " + getElSize(1200) + ",top= " + getElSize(500) + ",left="+ getElSize(800))
				    		}
							
						});
					},
					columns:[{
						field:"barcode",title:"barcode",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"a",title:"${worker_num}",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"b",title:"${name}",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"c",title:"${depart}",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"d",title:"Email",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},{
						field:"LV",
						title:"권한",
						template: "#=getLv(LV)#",
						attributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			},headerAttributes: {
	            				style: "text-align: center; font-size:" + getElSize(36)
	            			}
					},
					{
						title: "바코드",
						template: "#=imgDraw(imgChk)#",
						attributes: {
            				style: "text-align: center; font-size:" + getElSize(36)
            			},headerAttributes: {
            				style: "text-align: center; font-size:" + getElSize(36)
            			}
					},
					{
        				command : [{
        					name : "퇴사",
        					click : function(e){
        						del_btn_evt(e,this,"delWorker()")
        						//delRouting(e,this)
        					}
        				}],
        				attributes: {
            				style: "text-align: center; font-size: " + getElSize(36) + "px;"
          				}
        			}
					/* ,
					{
						command : [{
							name : "퇴사"
							,click : function(e){
								var tr = $(e.target).closest("tr"); // get the current table row (tr)
					            var data = this.dataItem(tr);
								console.log(data)
								del_workerId=data.a
								var url = "${ctxPath}/chart/delWorker.do";
								var param = "workerId=" + del_workerId;
								
								
								$.ajax({
									url : url,
									data : param,
									type : "post",
									dataType : "text",
									success :function(data){
										if(data=="success"){
											getWorkerList();
										}
									}
								});
							}
						}]
					} */]
				});
				
			    var xgrid = $("#table22").data("kendoGrid");

			    $(xgrid.tbody).on("click", "td", function (e) {
			    		if(typeof($(this).children()[0])!="undefined"){
			    			return
			    		};
			    		
			            var row = $(this).closest("tr");
			            var curRowIdx = $("tr", xgrid.tbody).index(row);
			            var colIdx = $("td", row).index(this);
			            var item = xgrid.dataItem(row);
			            
			            // GROUPWARE API 를 위한 변수
			            clickBtn = "update";
			            showEditForm(item.a);
			    });
			
				$(".k-button").css({
					"height" : getElSize(60),
					"width" : getElSize(200),
					"border-color" : "#9B9B9B",
					"background" : "#9B9B9B",
					"border-radius" : getElSize(8),
					"color" : "black",
				});
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(1) + "px solid black"
				});
				

			}
		});
	};
	
	function getLv(LV){
		if(LV==1){
			return '작업자';
		}else if(LV==2){
			return '관리자';
		}else if(LV==3){
			return 'ADMIN';
		}
	}
	
	//사진 추가하기
	// 교정이력 성적서 추가
	function dtImgAdd(item){
// 		console.log(item)
		
		//이미지 파일 이름
		var imgName=$("#dtImg").val();
		//이미지 확장자 확인
		var ext=imgName.substring(imgName.lastIndexOf(".")+1,imgName.length);
		console.log(ext)
		
		if(imgName==""){
			alert("업로드할 이미지를 선택하여 주세요.")
			return;
		}else if(ext!="png" && ext!="jpg" && ext!="bmp" && ext!="PNG" && ext!="JPG" && ext!="JPEG"){
			$("#dtImg").val("");
			alert("png,jpg,bmp 파일의 형식만 업로드만 가능합니다.");
			return;
		}
		
		if(confirm(item.b + " 님의 이미지를 업로드 하시겠습니까? \n업로드 하는데 시간이 소요될 수 있습니다.")==false){
			$("#dtImg").val("")
			return;
		}

		//$.showLoading();

		var frm = document.getElementById('dtImgForm');
	    frm.method = 'POST';
	    frm.enctype = 'multipart/form-data';
	  
	    var fileData = new FormData(frm);

	    fileData.append("empCd",item.a);
	    fileData.append("code",$("#code").val());
	    fileData.append("idx",item.idx);
// 	    fileData.append("imgTy","detail");

		var url = "${ctxPath}/chart/idcardimg.do";
//		var param = "code=" + $("#code").val() +
//					"fileData=" + new FormData(frm);

		$.showLoading();
		
		$.ajax({
			url : url,
			data : fileData,
			processData : false,
			contentType: false,
			type : "post",
			success : function(data){
				if(data=="success"){
					
					getWorkerList();
					$("#dtImg").val("")
					$.hideLoading();
					alert("저장되었습니다.");
					
					setTimeout(function() {
						
					}, 1000)
				}else{
					alert("저장에 실패하였습니다");
					//$.hideLoading();
				}
			}
		})
	}
	
	//교정이력 성적서 template
	function imgDraw(chk){
		if(chk){
			return "<span name='imgPlus' class='k-icon k-i-plus-circle'></span>　<span name='imgView' class='k-icon k-i-zoom-actual-size'></span>" 
		}else {
			return "미저장"
		}
	}

	
	var windowTemplate, delMsg;
	function del_btn_evt(e, el, cd){
		e.preventDefault();
		var tr = $(e.target).closest("tr");
		var data = el.dataItem(tr); //get the row data so it can be referred later
		
		
		delMsg.content(windowTemplate(data)); //send the row data object to the template and render it
		delMsg.center().open();
		
		
		var grid = $("#table22").data("kendoGrid");
	    var dataItem = grid.dataItem(tr);
		var id = dataItem.id 
// 		console.log(dataItem);
		del_email = dataItem.d;
		del_workerId = dataItem.a;
		del_token = dataItem.token;
		del_group = dataItem.c;
	    $("#yesButton").click(function(){
	    	eval(cd)
	        grid.dataSource.remove(data)
	        delMsg.close();
	    })
	    $("#noButton").click(function(){
	    	delMsg.close();
	    }) 
	}

	var del_workerId;
	function delWorker(){
		var url = "${ctxPath}/chart/delWorker.do";
		
		var del_groupId;
		$(groupListApi).each(function(idx,data){
			if(data.name==del_group){
				del_groupId = data.id;
			}
		})
		
		if(del_groupId==undefined){
			del_groupId = $("#part option:selected").attr("name");
		}
		
		var param = "workerId=" + del_workerId +
					"&email=" + del_email +
					"&shopId=" + shopId +
					"&group=" + del_groupId +
					"&token=" + del_token;
		
		console.log(param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success :function(data){
				console.log(data)
				if(data=="success"){
					getWorkerList();
					alert("삭제되었습니다.");
				}else{
					getWorkerList();
					alert("저장에 실패하였습니다");
				}
			}
		});
	};
	
	function showEditForm(id){
		var url = "${ctxPath}/chart/getWorkerInfoo.do";
		var param = "id=" + id;

		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = $("#table22").data("kendoGrid").dataSource.data();
				
				$(json).each(function(idx,item){
					if(data.id==item.a){
						originGroupId = item.originGroupId;
					}	
				})
				token = data.token;

				console.log(data)
				$("#workerId").html(data.id);
				$("#barcodeId").val(data.barcode);
				$("#name").val(data.name);
				$("#pwd").val(data.pwd);
				$("#part").val(data.part);
				$("#email").val(data.email);
				$("#position").val(data.position);

				$('#email').prop('readonly', true)
				
				$("#insertForm").css({
					"z-index" : 999,
					"display" : "inline"
				});
				
				groupwarePart = data.part
				groupwareId = "";
				//그룹웨어 API 보내기 위한 변수 담기
				$(groupListApi).each(function(idx,item){
					if(data.part==item.name){
						groupwarePart = item.name;
						groupwareId = item.id;
					}
				})
				
				showCorver();
				$.hideLoading()
			}
		});
	};

	function showInsertForm(){
		//GROUPWARE API 를 위한 변수
		clickBtn = "add";
		originGroupId = "";
		token = "";
		$("input").val("")
		getNextWorkerId();
	};

	function getNextWorkerId(){
		var url = "${ctxPath}/chart/getNextWorkerId.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				$("#workerId").html(data);
				$("#email").val(data);
				$('#email').prop('readonly', false)
				
				$("#insertForm").css({
					"z-index" : 999,
					"display" : "inline"
				});
				
				showCorver();
			}
		});
	};
	function showCorver(){
		$("#corver").css("z-index", 999);	
	};

	function hideCorver(){
		$("#corver").css("z-index", -999);
	};

	
	function closeInsertForm(){
		hideCorver();
		$("#insertForm, #updateForm").css("z-index",-9999);
		$("#updateForm #rcvCnt").css("color", " white");
		return false;
	};
	
	//api 참조 부서리스트 가져오기
	function bkpiApiGroups(){
		var param = {
			"shopId" : "1",
			"id" : "0"
		}
		
/* 		$.ajax({
		    crossOrigin : true,
// 	    	contentType : "application/json; charset=utf-8",
		    type : "post",
		    url : "http://hsm.iptime.org:8090/Master/getNcData?ncId=1",
// 		    data : JSON.stringify(param),
		    dataType : "json",
		    success : function(data) {
		        console.log(data);
		    },error:function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		}); */
		
		$.ajax({
// 			crossOrigin : true,
//  		    contentType : "application/json",
		    type : "post",
		    url : "https://bkpi.co.kr/elfinos_API/getGroups",
// 		    url : "http://192.168.0.2/elfinos_API/getGroups",
// 		    url : "http://192.168.0.2/elfinos_API/getAllMachineStatus",
 		    data : JSON.stringify(param),
		    dataType : "json",
		    success : function(data) {
		        console.log(data);
		        var list = [];
		        groupListApi = data;
		        var options;
		        
		        $(data).each(function(idx,data){
		        	options += "<option name=" + data.id + ">" + data.name + "</option>"
		        });
		        
		        console.log(options)
		        
		        $("#part").empty();
		        $("#part").append(options);
		        
/* 		        varjson = data.
		        partSelect */
		    },error:function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
		
// 		console.log("asd")
	}
	
	//api 참조 사원정보 변경하기
	function APIUPDATE(){
		
		var param="shopId=" + shopId.toString()
					+"&workerId=" + $("#workerId").html()
					+"&firstName=" + $("#name").val().substring(1,$("#name").val().length)
					+"&lastName=" + $("#name").val().substring(0,1)
					+"&groupId=" + groupwareId				//groupwarePart
					+"&position=" + ""				//공백으로 들고가도됨	-1
					+"&tell=" + "01045934671"				//				-1
					+"&originGroupId=" + groupwareId	//기존 그룹아이디groupId 랑 동일
					+"&destGroupId=" + $("#part option:selected").attr("name")		//바꾸는 ㄷ그륩아이디 
					+"&date_of_employment=" + "2019-10-01"//입사일짜		-1
		
					console.log(param)
		$.ajax({
		    type : "get",
		    url : "https://www.bkpi.co.kr/workermanager/updateUser",
 		    data : param,
//  		    data : JSON.stringify(param),
		    dataType : "text",
		    success : function(data) {
		        console.log(data)
		    },error:function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
	}
	
	//api 참조 사원정보 추가하기ㅏ
	function APIADD(){
		console.log("API 추가 시작하기")
	}
	
	function testt(){
		var param="shopId=" + shopId.toString()
				+"&id=" + "bkjm_master"
				+"&ty=" + "writer"
				+"&status=" + "all"
				
				$.ajax({
				type : "get",
				url : "https://bkpi.co.kr/EA/getMyESList",
				 data : param,
				//  data : JSON.stringify(param),
				dataType : "text",
				success : function(data) {
				    console.log(data)
				},error:function(request,status,error){
					alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				}
				});
	}
	
	</script>
</head>
<body>
	<div id="window"></div>
	<div id="corver"></div>
	
	<form id='dtImgForm'><input type='file' id='dtImg' name='file' style='display:none'></form>
	
	<div id="insertForm">
		<form action="" id="insert">
			<Table style="width: 100%">
				<tr >
					<td class='table_title'>
						<spring:message code="worker_num"></spring:message>
					</td>
					<td id="workerId">
						
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						barcode
					</td>
					<td>
						<input type="text" id="barcodeId">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						<spring:message code="name"></spring:message>
					</td>
					<td>
						<input type="text" id="name">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						<spring:message code="pwd"></spring:message>
					</td>
					<td>
						<input type="password" id="pwd">
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						<spring:message code="depart"></spring:message>
					</td>
					<td>
						<select id="part">
									
						</select>
<!-- 						<input type="text" id="part"> -->
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						Email
					</td>
					<td>
						<input type="text" id="email"> @ bkpi.co.kr
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						권한
					</td>
					<td>
						<select id="LV">
							<option value='1'>작업자</option>
							<option value='2'>관리자</option>
							<option value='3'>ADMIN</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class='table_title'>
						직위
					</td>
					<td>
						<input type="text" id="position">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;"><button onclick="addWorker(); return false;"><spring:message code="confirm"></spring:message></button> <button onclick="closeForm(); return false;"><spring:message code="cancel"></spring:message></button> </td>
				</tr>
			</Table>
		</form>
	</div>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table style="width: 100%">
						<Tr>
							<Td align="right">
								
								<button onclick="testt()">API 업데이트하기</button>
								<button id="goMakeNo" onclick="location.href='https://bkpi.co.kr/cardprint'">사원증 만들기</button>
								<button onclick="showInsertForm()"><spring:message code="add"></spring:message> </button>

							</Td>
						</Tr>
					</table>
					<div id="table22"></div>
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	