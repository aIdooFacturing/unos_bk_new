<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}
span.k-link{
	color:black !important;
}

.k-link{
	color:white !important;
}

#grid{
	background: black;
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

#gridIn .k-grid-header{
	border-color : #222327;
}
#gridOut .k-grid-header{
	border-color : #222327;
}
#gridOuter .k-grid-header{
	border-color : #222327;
}
#gridShip .k-grid-header{
	border-color : #222327;
}
#gridMove .k-grid-header{
	border-color : #222327;
}


.k-header .k-grid-toolbar {
	background : #222327;
	border-color : #222327;
}


.k-grid-header-wrap k-auto-scrollable table thead tr th{
	border-color : #222327;
}

.k-grid-content k-auto-scrollable{
	border-color : #222327;
}

.k-grid-footer {
	border-color : #222327;
}

.k-footer-template td{
	background : #353542;
	border-color : #222327;
	color : white;
}



#wrapper .k-grid-header-wrap {
	border-color : #222327;
}

.k-grid-footer-wrap tbody tr td{
	color : white;
}

#downLoad:hover{
	background : #766b7c !important;
}

[aria-owns]{
	background : rgb(230, 229, 230);
	border-color:#7c7e84;
}
[aria-label]{
	background : rgb(230, 229, 230);
	border-color:#7c7e84;
}

.k-state-active.k-item.k-tab-on-top.k-state-default{
    background: #858585 !important;
}

.k-dropdown-wrap k-state-default{
	border-color: rgb(230, 229, 230);
}



</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("im", "StockHistory")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

//	var handle = 0;
	
	$(function(){
		$("#sDate").val(moment().format("YYYY-MM-DD"));
		$("#eDate").val(moment().format("YYYY-MM-DD"));
		
//		createNav("inven_nav", 7);
		
//		setEl();
//		time();

		setEl2();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		$( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	$("#eDate").val(e);
		    }
	    })
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
 */	
 
 
	/* function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#iframe").css({
			"width" : getElSize(3500),
			"height" : getElSize(1800),
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#tabstrip").css("height",getElSize(1656))
		$("#tabstrip").css("height",getElSize(1656))
		
		$("#downLoad").css({
			"background" : "rgb(250,235,255)",
			"padding" : getElSize(10),
			"color" : "black",
			"font-size" : getElSize(45),
			"margin-left" : getElSize(1000),
			"cursor":"pointer"
		})
		
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
	/* 	
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		 */
		 
		$("#iframe").css({
			"width" : getElSize(3500),
			"height" : getElSize(1800),
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
	
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
			
		$("#content_table").css({
			"margin-top" : getElSize(300)
		})
			
			
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight,
			"margin-top" : getElSize(250)
		});
		
		$("#wrapper").css({
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50)
		});
		 
/* 		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		 */
	
		 
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
/* 		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		}); */
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(22),
			"margin-right" : getElSize(22)
		});
		
		$("select").css({
			"width" : getElSize(380),
			"margin-right" : getElSize(50)
		});
		
		$("#sDate").css({
			"width" : getElSize(400),
			"text-align" : "center",
			"margin-right" : getElSize(50)
		});
		
		$("#eDate").css({
			"width" : getElSize(400),
			"text-align" : "center",
			"margin-right" : getElSize(50)
		});
		
		$("input").css({
			"background-color" : "black",
			"border-color" : "black",
			"height" : getElSize(80)		
		})

		$("#td_first").css({
			"padding-left" : getElSize(30),
			"padding-bottom" : getElSize(30),
			"padding-top" : getElSize(50)
		});
		

		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(40),
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$(".k-grid-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(40),
		    "background-image" : "none",
		    "background-color" : "#353542",
		    "color" : "white"
		}); 
		
		$("button").css({
			"padding" : getElSize(10),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(30),
			"font-size" : getElSize(44),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#tabstrip").css("height",getElSize(1580))
		$("#tabstrip").css("background","#181818")
		$("#tabstrip").css("border-color","#181818")
		$("#tabstrip").css("padding-top",getElSize(30))
//		$("#tabstrip").css("height",getElSize(1620))

		$("#gridIn").css("background", "#26282c")
		$("#gridIn").css("border-color","#26282c")
		
		$("#gridIn .k-grid-footer-wrap tbody tr td").css("background", "#26282c")
		
		$("#gridOut").css("background", "#26282c")
		$("#gridOut").css("border-color","#26282c")
		
		$("#gridOuter").css("background", "#26282c")
		$("#gridOuter").css("border-color","#26282c")
		
		$("#gridShip").css("background", "#26282c")
		$("#gridShip").css("border-color","#26282c")
		
		$("#gridMove").css("background", "#26282c")
		$("#gridMove").css("border-color","#26282c")
		
		$("#tabstrip li").css({
			"background" : "#e2e1e2",
			"border-color" : "#26282c"
		})
		
		$(".k-grid-footer tr td").css("background", "#909090")
		$(".k-grid-footer").css("border-color", "#26282c")
		
//		$("#k-grid-content k-auto-scrollable").css("heght", getElSize(1000))
		
		
		
		$("#downLoad").css({
			"background" : "rgb(133, 133, 133)",
			"padding" : getElSize(10),
			"color" : "black",
			"position" : "absolute",
			"margin-left" : getElSize(1070),
			"font-size" : getElSize(50),
// 			"top" : getElSize(100),
			"cursor" : "pointer",
			"text-align" : "right",
			"vertical-align" : "center"
		})
		
		
		console.log("enenene 700")
		
		if(getUrlVars()["lang"]=="en") {
			// lan == "en"
			$("#downLoad").css({
				"margin-left" : getElSize(830)
			})
		}
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
			
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
				"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
	};
	
	
	
	
/* 	function time(){
		$("#time").html(getToday());
//		 handle = requestAnimationFrame(time)
	}; */
	
	function getUrlVars() {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
	    function(m,key,value) {
	      vars[key] = value;
	    });
	    return vars;
	  }
	
	function numberWithCommas(x) {
		if(x==undefined){
			return 0
		}
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function totalFooter(sum){
		if(sum!=undefined){
			var prceSum=numberWithCommas(sum.sum)
			return "총금액 : " + prceSum 
		}else{
			return "총금액 : 0"
		}
	}
	
	function cntFooter(sum){
		if(sum!=undefined){
			var prceSum=numberWithCommas(sum.sum)
			return "총수량 : " + prceSum 
		}else{
			return "총수량 : 0"
		}
	}
	
	//납품서 재발행
	function InStockPrint(){
		chk = 0;
		
		//납품서 번호 확인하기
		for(i=0,len=inList.length;i<len;i++){
			console.log(inList[i])
			if(inList[i].id==$("#comboboxIn").val()){
				chk++;
			}
		}
		if(chk==0){
			alert("조회된 납품서 번호가 없습니다. \n날짜 및 납품서 번호를 확인해주세요")
			return;
		}
		
		//프린터를 위한 데이터 담기
		savelist=[]
		for(i=0,len=gridIn.dataSource.data().length; i<len; i++){
			if($("#comboboxIn").val()==gridIn.dataSource.data()[i].deliveryNo){
				var arr={};
				arr.deliveryNo = gridIn.dataSource.data()[i].deliveryNo;
				//납품회사
				arr.deliverer = encodeURIComponent(gridIn.dataSource.data()[i].vndNm);
				//바코드
				arr.barcode = gridIn.dataSource.data()[i].barcode;
				//로트번호
				arr.lotNo = gridIn.dataSource.data()[i].lotNo;
				//납품 날짜
				arr.deliverDate = gridIn.dataSource.data()[i].date.substr(0,10);
//				arr.prdNo = gridIn.dataSource.data()[i].RWMATNO
				//차종
				arr.prdNo = gridIn.dataSource.data()[i].prdNo
				//품번
				arr.ITEMNO = ""
				//스팩
				arr.spec = "SPEC"
				//단위
				arr.unit = "EA"
				//수량
				arr.cnt = gridIn.dataSource.data()[i].cnt;
				//단가
				arr.prc = gridIn.dataSource.data()[i].prce;
				//금액
				arr.totalPrc = Number(gridIn.dataSource.data()[i].cnt) * Number(gridIn.dataSource.data()[i].prce)
				
//				arr.item = gridIn.dataSource.data()[i].prdNo
//				arr.ITEMNO = gridIn.dataSource.data()[i].ITEMNO
//				arr.afterProj = gridIn.dataSource.data()[i].afterProj
//				arr.sendCnt = gridIn.dataSource.data()[i].cnt
//				arr.remarks = encodeURI(decode(gridIn.dataSource.data()[i].remarks))
				savelist.push(arr);
			}
		}
		
//		console.log(savelist)

		
		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + $("#comboboxIn").val(),
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
					
					$("#iframe").attr("src", "${ctxPath}/chart/printInvoice.do?json=" + encodeURIComponent(JSON.stringify(savelist))).css({
						"display" : "inline",
						"background-color" : "white"
					});
					
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
					
					$("body").append(close_btn);						
											
					$("#close_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
						"z-index" : 9999
					}).click(function(){
						$(this).remove();
						$("#print_btn").remove();
						$("#iframe").css("display" , "none")
					})
							
					
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
					
					$("body").append(print_btn);						
											
					$("#print_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
						"z-index" : 9999
					}).click(function(){
						var frm = document.getElementById("iframe").contentWindow;
						frm.focus();// focus on contentWindow is needed on some ie versions
						frm.print();	
					})
					
//					getDeliveryHistory(); 
				}
			}
		});
	}
	
	// 반출서 재발행 
	function exportPrint(){
		chk = 0;
		
		//반출서 번호 확인하기
		for(i=0,len=exportList.length;i<len;i++){
			console.log(exportList[i])
			if(exportList[i].id==$("#combobox").val() && exportList[i].plan=="반출"){
				chk++;
			}
		}
		if(chk==0){
			alert("조회된 반출번호가 없습니다. \n날짜 및 반출번호를 확인해주세요")
			return;
		}
		
		//프린터를 위한 데이터 담기
		savelist=[]
		for(i=0,len=gridOuter.dataSource.data().length; i<len; i++){
			if($("#combobox").val()==gridOuter.dataSource.data()[i].deliveryNo && gridOuter.dataSource.data()[i].plan=="반출"){
				var arr={};
				arr.deliveryNo = gridOuter.dataSource.data()[i].deliveryNo;
				arr.vndNm = encodeURIComponent(gridOuter.dataSource.data()[i].vndNm);
				arr.barcode = gridOuter.dataSource.data()[i].barcode;
				arr.lotNo = gridOuter.dataSource.data()[i].barcode;
				arr.date = gridOuter.dataSource.data()[i].date.substr(0,10);
				arr.prdNo = gridOuter.dataSource.data()[i].RWMATNO
				arr.item = gridOuter.dataSource.data()[i].prdNo
				arr.ITEMNO = gridOuter.dataSource.data()[i].ITEMNO
				arr.afterProj = gridOuter.dataSource.data()[i].afterProj
				arr.sendCnt = gridOuter.dataSource.data()[i].cnt
				arr.remarks = encodeURI(decode(gridOuter.dataSource.data()[i].remarks))
				savelist.push(arr);
			}
		}
		
		console.log(savelist)
		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + $("#combobox").val(),
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
					console.log(savelist)
					console.log(JSON.stringify(savelist))
					$("#iframe").attr("src", "${ctxPath}/chart/printExport.do?json=" + encodeURIComponent(JSON.stringify(savelist))).css({
						"display" : "inline",
						"background-color" : "white"
					});
					
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
					
					$("body").append(close_btn);						
											
					$("#close_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
						"z-index" : 9999
					}).click(function(){
						$(this).remove();
						$("#print_btn").remove();
						$("#iframe").css("display" , "none")
					})
							
					
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
					
					$("body").append(print_btn);						
											
					$("#print_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
						"z-index" : 9999
					}).click(function(){
						var frm = document.getElementById("iframe").contentWindow;
						frm.focus();// focus on contentWindow is needed on some ie versions
						frm.print();	
					})
				}else{
					alert("실패")
				}
			},error : function(){
				alert("에러")
			}
		});
	}
	
	function deliveryNoSearch(event){
		if(event.keyCode == 13){
			getTable()
		}
	}	

	var downLoadGrid;
	function onSelect(e){
		var text=$(e.item).find("> .k-link").text();
		text = text.trim();
		
		if(text=="입고 현황 조회"){
			downLoadGrid = $("#gridIn").data("kendoGrid");			
 			gridCss();
			console.log("입고")
			return;			
		}else if(text=="불출 현황 조회"){
			downLoadGrid = $("#gridOut").data("kendoGrid");
			gridCss();
			console.log("불출")
			return;			
		}else if(text=="반　출입 현황 조회"){
			downLoadGrid = $("#gridOuter").data("kendoGrid");
			gridCss();
			console.log("반출")
			return;			
		}else if(text=="출하 현황 조회"){
			downLoadGrid = $("#gridShip").data("kendoGrid");
			gridCss();
			console.log("출하")
			return;			
		}else if(text=="재고 이동 현황 조회"){
			downLoadGrid = $("#gridMove").data("kendoGrid");
			gridCss();
			console.log("이동현황")
			return;			
		}
	}
	
	function gridCss(){
		$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1000))
	}
	var gridInlist
	var gridOutlist
	var gridOuterlist
	var gridShiplist
	var gridMovelist
	var inSum=0;
	
	function gridCommonCss(e){
		$(e.sender.element).find('[data-role="autocomplete"]').each(function(){
			$(this).data("kendoAutoComplete").setOptions({autoWidth:true})
		})
	}
	$(document).ready(function(){
		
		$("#downLoad").click(function(e) {
			if(downLoadGrid==undefined || downLoadGrid=="undefined"){
				downLoadGrid = $("#gridIn").data("kendoGrid");
			}
			
			downLoadGrid.saveAsExcel();
			
			
//		    var grid = $("#gridIn").data("kendoGrid");
//		    grid.saveAsExcel();
		});
		
		 $("#tabstrip").kendoTabStrip({
			 select:onSelect
             ,animation:  {
                 open: {
                     effects: "fadeIn"
                 }
             }
		 	
         });
		 //입고 조회
		 gridInlist = $("#gridIn").kendoGrid({
			 toolbar : [{
				 template: "<input id='comboboxIn'>"
			 },{
				 template: "<a class='k-button' id='button1' style='background:rgb(133,133,133)' onclick='InStockPrint()'>납품서 재발행</a>"
		     }],
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"입고현황_" + moment().format("YYYY-MM-DD") + ".xlsx"
	         },
			 dataBound : function(e) {
				 gridCommonCss(e);
// 			 	$("#gridIn").css("height", getElSize(1400))
// 				$("#gridOut").css("height", getElSize(1400))
// 				$("#gridOuter").css("height", getElSize(1400))
// 				$("#gridShip").css("height", getElSize(1400))
// 				$("#gridMove").css("height", getElSize(1400))
// 				$(".k-grid-content .k-auto-scrollable").css("height",getElSize(500))
			 },
			 sortable: true,
			 columns:[{
				 title:"${com_name}"
				 ,field:"vndNm"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
     				style: "text-align: center; font-size:" + getElSize(35)
     			},headerAttributes: {
     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
 				}
			 },{
				 title:"${prd_no}"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,attributes: {
     				style: "text-align: center; font-size:" + getElSize(35)
     			},headerAttributes: {
     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40) 
 				}
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"${Movetype}"
				 ,field:"plan"
				 ,width:getElSize(375)
				 ,filterable: {
	                    cell: {
	                    	suggestionOperator: "contains"
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"${Delivery_number}"
				 ,field:"deliveryNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"Lot No."
				 ,field:"lotNo"
				 ,width:getElSize(400)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"${Quantity}"
				 ,field:"cnt"
				 ,width:getElSize(205)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"${unit_price}"
				 ,field:"prce"
				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(205)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"${reg_time}"
				 ,field:"date"
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
//				 ,footerTemplate: "#=totalFooter(data.sumIn)#"
				 ,width:getElSize(455)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 }]
		 }).data("kendoGrid")
		 
		 $("#button1").css({
			 "border-color" : "#222327"
		 })

		 //불출 조회
		 gridOutlist = $("#gridOut").kendoGrid({
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"불출현황_" + moment().format("YYYY-MM-DD") + ".xlsx"
	         },
			 sortable: true,
			 dataBound : function (e){
				 
				 gridCommonCss(e);

//				$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1000))
			 },
//			 height:getElSize(1330),
			 columns:[{
				 title:"품번"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 },attributes: {
	      				style: "text-align: center; font-size:" + getElSize(35)
	      			},headerAttributes: {
	      				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	  				}
		         }
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"로트번호"
				 ,field:"lotNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 },attributes: {
	      				style: "text-align: center; font-size:" + getElSize(35)
	      			},headerAttributes: {
	      				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	  				}
		         }
			 },{
				 title:"바코드"
				 ,field:"barcode"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"가격"
				 ,field:"prce"
				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(500)
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 }]
		 }).data("kendoGrid")

		 //반출입 조회
		 gridOuterlist = $("#gridOuter").kendoGrid({
			 toolbar : [{
				 template: "<input id='combobox'>"
			 },{
				 template: "<a class='k-button' id='button2' style='background:rgb(133,133,133)' onclick='exportPrint()'>반출서 재발행</a>"
		     }],
		     excel:{
		            fileName:"반출입현황" + moment().format("YYYY-MM-DD") + ".xlsx"
	         },
			 filterable: {
			      mode: "row"
			 },
			 sortable: true,
			 dataBound : function (e){

				 gridCommonCss(e);

// 					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(900))
			 },
//			 height:getElSize(700),
			 columns:[{
				 title:"회사명"
				 ,field:"vndNm"
				 ,width:getElSize(450)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"품명"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"품번"
				 ,field:"ITEMNO"
				 ,width:getElSize(350)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"반출 번호"
				 ,field:"deliveryNo"
				 ,width:getElSize(450)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				title:"lotNo"
				,field:"barcode"
				,width:getElSize(320)
				,filterable: {
					cell: {
	                	suggestionOperator: "contains"
	                }
		        },attributes: {
     				style: "text-align: center; font-size:" + getElSize(35)
     			},headerAttributes: {
					style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
 				}
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(320)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"공정"
				 ,field:"proj"
				 ,width:getElSize(390)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(230)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(490)
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 }]
		 }).data("kendoGrid")
		 
		 $("#button2").css({
			 "border-color" : "#222327"
		 })
		 
		  //출하 조회
		 gridShiplist = $("#gridShip").kendoGrid({
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"출하현황" + moment().format("YYYY-MM-DD") + ".xlsx"
	         },
			 sortable: true,
			 dataBound : function (e){
				 
				 gridCommonCss(e);
				 
//					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1000))
			 },
//			 height:getElSize(1330),
			 columns:[{
				 title:"품명"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"품번"
				 ,field:"ITEMNO"
				 ,width:getElSize(350)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"출하번호"
				 ,field:"shipLot"
				 ,width:getElSize(300)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(200)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"가격"
				 ,field:"prce"
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
//				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(470)
// 				 ,footerTemplate: "#=totalFooter(data.prce)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 }]
		 }).data("kendoGrid")
		 
		  //재고 이동 조회
		 gridMovelist = $("#gridMove").kendoGrid({
			 filterable: {
			      mode: "row"
			 },
			 excel:{
		            fileName:"재고이동현황" + moment().format("YYYY-MM-DD") + ".xlsx"
	         },
			 dataBound : function (e){
				 
				 gridCommonCss(e);

//					$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1000))
			 },
//			 height:getElSize(1330),
			 sortable: true,
			 columns:[{
				 title:"품번"
				 ,field:"prdNo"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"유형"
				 ,field:"plan"
				 ,width:getElSize(500)
				  ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"로트(출하)번호"
				 ,field:"shipLot"
				 ,width:getElSize(500)
				 ,filterable: {
					 cell: {
	                        suggestionOperator: "contains"
	                 }
		         },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"수량"
				 ,field:"cnt"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"가격"
				 ,field:"prce"
				 ,template:"#=numberWithCommas(prce)#"
				 ,width:getElSize(250)
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 },{
				 title:"날짜"
				 ,field:"date"
				 ,width:getElSize(500)
				 ,footerTemplate: "#=cntFooter(data.cnt)#"
				 ,filterable: {
	                    cell: {
	                        enabled: false
	                    }
	             },attributes: {
	     				style: "text-align: center; font-size:" + getElSize(35)
	     			},headerAttributes: {
	     				style: "text-align: center; color:white; font-family : NotoSansCJKkrBold; background-color:#353542; font-size:" + getElSize(40)
	 				}
			 }]
		 }).data("kendoGrid")
		 
		 $("#combobox").kendoComboBox({
			dataSource: exportList,
			dataTextField: "id",
			dataValueField: "id"
		 });

		 $("#comboboxIn").kendoComboBox({
			dataSource: exportList,
			dataTextField: "id",
			dataValueField: "id"
		 });
		 
		 getTable();
		 gridCss();
		
	})
	var exportList=[];
	var inList=[];
	function getTable(){
		var url = "${ctxPath}/chart/getInhistory.do";
		var param  = "sDate=" + $("#sDate").val() + 
						"&eDate=" + $("#eDate").val() +
						"&deliveryNo=" + $("#deliveryNo").val();
	
	//		$("#checkall")[0].checked=false;
		var json;
		console.log(param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
				json = data.dataList;//입고리스트
				jsonOut = data.dataListOut;//불출리스트
				jsonOuter = data.dataListOuter;//불출리스트
				jsonShip = data.dataListShip;//출하리스트
				jsonMove = data.dataListMove;//출하리스트
				
				exportList=[]
				inList=[]
				//입고
				inSum = 0;
				$(json).each(function(idx,data){
					data.sumIn = data.cnt * data.prce;
					data.vndNm=decode(data.vndNm)
					data.plan=decode(data.plan)
					
					chk = 0;
					for(i=0,len=inList.length;i<len;i++){
						if(inList[i].id==data.deliveryNo && data.deliveryNo!=""){
							chk++
						}
					}
					if(chk==0 && data.deliveryNo!=""){
						var arr={id : data.deliveryNo, plan : data.plan}
						inList.push(arr)
					}
				})				

				console.log(inSum)
				$("#inSum").html(inSum)
				
				inlistdata = new kendo.data.DataSource({
					data: json,
					aggregate: [
				          { field: "prce", aggregate: "sum" }
				          ,{ field: "sumIn", aggregate: "sum" }
				          ,{ field: "cnt", aggregate: "sum" }
			        ],
				});
				
				//불출
				$(jsonOut).each(function(idx,data){
					data.plan=decode(data.plan)
				})
				outlistdata = new kendo.data.DataSource({
					data: jsonOut,
					aggregate: [
				          { field: "prce", aggregate: "sum" },
				          { field: "cnt", aggregate: "sum" }
			        ],
				}); 

				//반출입
				$(jsonOuter).each(function(idx,data){
					data.vndNm=decode(data.vndNm)
					data.plan=decode(data.plan)
					
					chk = 0;
					for(i=0,len=exportList.length;i<len;i++){
						if(exportList[i].id==data.deliveryNo && data.deliveryNo!="" && data.plan=="반출" && exportList[i].plan=="반출"){
							chk++
						}
					}
					if(chk==0 && data.deliveryNo!="" && data.plan=="반출"){
						var arr={id : data.deliveryNo, plan : data.plan}
						exportList.push(arr)
					}
					if(data.plan=="반출"){
						if(data.proj=="0030"){
							data.proj="CNC삭(외주)"
						}else if(data.proj=="0040"){
							data.proj="도금(외주)"
						}
					}else if(data.plan=="반입"){
						if(data.proj=="0030"){
							data.proj="CNC삭"
						}else if(data.proj=="0040"){
							data.proj="도금"
						}else if(data.proj=="0090"){
							data.proj="도금"
						}
					}
				})
				outerlistdata = new kendo.data.DataSource({
					data: jsonOuter,
					aggregate: [
				          { field: "prce", aggregate: "sum" },
				          { field: "cnt", aggregate: "sum" }
			        ],
				}); 
				
				//출하
				$(jsonShip).each(function(idx,data){
					data.plan=decode(data.plan)
				})
				shiplistdata = new kendo.data.DataSource({
					data: jsonShip,
					aggregate: [
				          { field: "prce", aggregate: "sum" },
				          { field: "cnt", aggregate: "sum" }
			        ],
				}); 

				//재고이동
				$(jsonMove).each(function(idx,data){
					data.plan=decode(data.plan)
				})
				movelistdata = new kendo.data.DataSource({
					data: jsonMove,
				}); 
				
				gridInlist.setDataSource(inlistdata);
				gridOutlist.setDataSource(outlistdata);
				gridOuterlist.setDataSource(outerlistdata);
				gridShiplist.setDataSource(shiplistdata);
				gridMovelist.setDataSource(movelistdata);
				
				gridIn = $("#gridIn").data("kendoGrid");
				gridOut = $("#gridOut").data("kendoGrid");
				gridOuter = $("#gridOuter").data("kendoGrid");
				gridShip = $("#gridShip").data("kendoGrid");
				gridMove = $("#gridMove").data("kendoGrid");
				
/* 				$("#combobox").kendoComboBox({
					dataSource: exportList,
					dataTextField: "id",
					dataValueField: "id"
				}); */
				
				var comboList = new kendo.data.DataSource({
				  data: exportList
				});
				var comboInList = new kendo.data.DataSource({
				  data: inList
				});
				var combobox = $("#combobox").data("kendoComboBox");
				combobox.setDataSource(comboList);

				var comboboxIn = $("#comboboxIn").data("kendoComboBox");
				comboboxIn.setDataSource(comboInList);
				
				$("#gridIn thead tr th").css("font-size",getElSize(39))
				$("#gridIn tbody tr td").css("font-size",getElSize(39))

				$("#gridOut thead tr th").css("font-size",getElSize(39))
				$("#gridOut tbody tr td").css("font-size",getElSize(39))
				
				$("#gridOuter thead tr th").css("font-size",getElSize(39))
				$("#gridOuter tbody tr td").css("font-size",getElSize(39))
				
				$("#gridShip thead tr th").css("font-size",getElSize(39))
				$("#gridShip tbody tr td").css("font-size",getElSize(39))

				$("#gridMove thead tr th").css("font-size",getElSize(39))
				$("#gridMove tbody tr td").css("font-size",getElSize(39))
				
//				$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1000))
				
				$.hideLoading()
				
				gridCss();
			}
		})
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<iframe id="iframe"></iframe>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
						
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c; ">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td id="td_first" style="text-align: left; vertical-align: middle;">
									날짜 : <input type='text' id='sDate' class="date" readonly="readonly"> ~ <input type="text" id='eDate' class="date" readonly="readonly"> 납품번호 : <input type="text" id="deliveryNo" onkeyup="deliveryNoSearch(event)"> <button onclick="getTable()"><spring:message code="check1"></spring:message></button>
								</td>
							</tr>
							
							<tr>
								<td>	
									<div id="tabstrip">
										<ul>
			                                <li class="k-state-active"> 
			                                    <spring:message code="income_history"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="release_history"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="Display_status"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="ship_history"></spring:message>
			                                </li>
			                                <li>
			                                    <spring:message code="inventorymovementstatus"></spring:message>
			                                </li>
			                                <span id="downLoad">Excel DownLoad</span>
			                            </ul>
			                            <div id="gridIn">
			                            </div>
			                            <div id="gridOut">
			                            </div>
			                            <div id="gridOuter">
			                            </div>
			                            <div id="gridShip">
			                            </div>
			                            <div id="gridMove">
			                            </div>
									</div>
								</Td>
							</Tr>
							
						</table>
					</div>
				</td>
			</Tr>
			
		</table>
	 </div>
	 
	<div id="intro_back"></div>
	<span id="intro"></span>
	
</body>
</html>	