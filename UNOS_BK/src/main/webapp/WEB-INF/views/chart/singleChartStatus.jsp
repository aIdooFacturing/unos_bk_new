<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<%-- <link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css"> --%>
<%-- <link rel="stylesheet" href="${ctxPath }/css/default.css"> --%>
<title>장비별 일 생산 / 가동 현황</title>

<!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>

<script type="text/javascript" src="${ctxPath }/js/multicolor_series.min.js"></script>
<!-- <script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>
         
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script> -->


<!-- <script src="https://www.digitaltwincloud.com:7443/lib/fcm/firebase.js"></script>
<script src="https://www.digitaltwincloud.com:7443/lib/fcm/init.js"></script>
<script src="https://www.digitaltwincloud.com:7443/lib/fcm/event.js"></script> -->

        


<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  		
}
</style> 

<script type="text/javascript">
	var fromDashboard= ""
	var dvcIdx = 0;
	var first = true;
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='-1'>Auto</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.dvcId)
					list += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#dvcId").html(list);
				
				$("#dvcId").css({
					"font-size" : getElSize(70),
					//"margin-bottom" : getElSize(10),
					"width" : getElSize(500),
					"-webkit-appearance": "none",
				    "-moz-appearance": "none",
			    	"appearance": "none",
			    	//"margin-top" : getElSize(100)
				});
				
				if(first){
					//localstorage에 저장 된 dvcid get
					for(var i = 0; i < dvcArray.length; i++){
						if(selectedDvcId == dvcArray[i]){
							dvcIdx = i;
						}
					}
					
					first = false;
				}
				
				dvcId = dvcArray[dvcIdx];
				getDvcData()
				$("#dvcId").val(dvcId);
				
				dvcIdx++;
				if(dvcIdx>=dvcArray.length) dvcIdx=0;
				
				//getDvcId();
				
			}
		});
	};
	
	
	
	function getDvcData(){
		//getMachineName();
		/* getDetailData();
		getAlarmList();
		getRapairDataList(); */
		//getStartTime();
		
		//setTimeout(getDvcData, 5000);
	};
	
	
	var selectedDvcId;
	function drawGauge(){
		$("#gauge").css({
			"width" : getElSize(900),
			"height" : getElSize(380)
		})
		
		var gaugeOptions = {

			    chart: {
			        type: 'solidgauge',
			        backgroundColor : "rgba(0,0,0,0)",
			        marginBottom : 0,
			        marginTop : -getElSize(460)
			        
			    },

			    
			    title: null,

			    pane: {
			        center: ['50%', '100%'],
			        size: '90%',
			        startAngle: -90,
			        endAngle: 90,
			        background: {
			            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
			            innerRadius: '60%',
			            outerRadius: '100%',
			            shape: 'arc'
			        }
			    },

			    tooltip: {
			        enabled: false
			    },

			    // the value axis
			    yAxis: {
			        stops: [
			            /* [0.1, '#55BF3B'], // green
			            [0.5, '#DDDF0D'], // yellow
			            [0.9, '#DF5353'] // red */
			            [1, '#55BF3B'], // green
			        ],
			        lineWidth: 0,
			        minorTickInterval: null,
			        tickAmount: 2,
			        title: {
			            y: -70
			        },
			        labels: {
			            y: 16
			        }
			    },

			    plotOptions: {
			        solidgauge: {
			            dataLabels: {
			                y: 5,
			                borderWidth: 0,
			                useHTML: true
			            }
			        }
			    }
			};

			// The speed gauge
			$('#gauge').highcharts(Highcharts.merge(gaugeOptions, {
			//var chartSpeed = Highcharts.chart('gauge', Highcharts.merge(gaugeOptions, {
			    yAxis: {
			    	min: 0,
					max: 100,
			        title: {
			            text: ''
			        }
			    },

			    credits: {
			        enabled: false
			    },

			    series: [{
			        name: 'Speed',
			        data: [0],
			        dataLabels: {
			            format: '<div style="text-align:center"><font style="font-size:25px; color:' +
			                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'blue') + '">{y}%</font>'
			        },
			        tooltip: {
			            valueSuffix: ' km/h'
			        }
			    }]

			}));
	};
	
	function setStdTime(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() + 1));
		var day = addZero(String(date.getDate()));
		var hour = 17;
		var minute = 30;

		if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
			day = addZero(String(new Date().getDate()+1));
		};
	}
	
	function changeDateVal(){
		window.sessionStorage.setItem("date", $("#today").val())		
		var now = new Date(); 
		//var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var date = new Date();
		
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		// Set specificDate to a specified date at midnight.
		
		var selectedDate = $("#today").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		var time = year + "-" + month + "-" + day;
		
		var today = getToday().substr(0,10);
		if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#today").val(today);
			window.sessionStorage.setItem("date", today)
			$('#up').prop("disabled",false);
			$('#down').prop("disabled", false);
			return;
		};
		
		
		if(today==$("#today").val()){
			var date = new Date();
			var year = date.getFullYear();
			var month = addZero(String(date.getMonth() + 1));
			var day = addZero(String(date.getDate()));
			var hour = date.getHours();
			var minute = addZero(String(date.getMinutes())).substr(0,1);

			if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
				day = addZero(String(new Date().getDate()+1));
			};
			selectedDate = year + "-" + month + "-" + day;			
		}
		
		
		
		/* drawBarChart("timeChart", selectedDate);
		getDetailData(); */
		getCurrentDvcStatus(dvcId);
	};
	
	function upDate(){
		$('#up').prop("disabled", true);
		$('#down').prop("disabled", true);
		
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		
		changeDateVal();
	};
	
	function downDate(){
		$('#up').prop("disabled", true);
		$('#down').prop("disabled", true);
		var $date = $("#today").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#today").val(today);
		
		changeDateVal();
		
	};
	
	
	
	
	var sessionDate;
	/* var pieChart;
	function drawPieChart(){
		  // Build the chart
	    $('#pieChart').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            width : getElSize(350),
	            height : getElSize(350),
	            plotBorderWidth: null,
	            backgroundColor : "rgba(255,255,255,0)",
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: null
	        },
	        tooltip: {
				enabled :false,
	        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        legend : {
	        	enabled : false
	        },
	        exporting : false,
	        credits : false,
	        plotOptions: {
	            pie: {
	            	size: getElSize(350),
	            	//borderWidth: 0,
	                //allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [{
	                name: 'In-Cycle',
	                y: 0,
	                color : 'green'
	            }, {
	                name: 'Cut',
	                y: 0,
	                color : 'brown'
	            }, {
	                name: 'Wait',
	                y: 0,
	                color : 'yellow'
	            }, {
	                name: 'Alarm',
	                y: 0,
	                color : 'red'
	            }, {
	                name: 'Off',
	                y: 0,
	                color : 'gray'
	            }]
	        }]
	    });
		  
	    pieChart = $('#pieChart').highcharts()
	}; */
	
	
	
	$(function(){
		
		
		
		//createMenuTree("maintenance", "Program_Manager")
	
		//drawPieChart();		
		//getAppList();
		
		/* getChartStatus();
		$("#today").change(changeDateVal);
		$("#up").click(upDate);
		$("#down").click(downDate);
		
		//drawGauge();
		selectedDvcId = window.localStorage.getItem("dvcId");
		sessionDate = window.sessionStorage.getItem("date");
		if(sessionDate==null){
			var date = new Date();
			var year = date.getFullYear();
			var month = date.getMonth();
			var day = date.getDate();
			
			var hour = date.getHours();
			
			sessionDate = getToday().substr(0,10)
		};
		
		$("#today").val(sessionDate)
		//if(selectedDvcId==null) selectedDvcId =96; 
		
		
		getDvcList();
		
		
		$("#dvcId").change(function(){
			dvcId = $("#dvcId").val();
			window.localStorage.setItem("dvcId", dvcId)
			if(this.value==-1){
				rotate_flag = true;	
			}else{
				rotate_flag = false;
				getDvcData();
				getCurrentDvcStatus(dvcId);
			}
		});
		
		
	 */
		
		
		/* window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10); */
		
		
		//chkBanner();
	});
	
	var startTimeLabel = new Array();
	var startHour, startMinute;
	
	var now = new Date();
	todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
	
	
	
	var yAxis;
	function getChartStatus(){
		var url = ctxPath + "/chart/getChartStatus.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				yAxis=data;
			}, error : function(e1,e2,e3){
			}
		});	
	};
	
	
	
	
	/* function drawBarChart(id, today) {
		startTimeLabel=[];
		var m0 = "",
			m02 = "",
			m04 = "",
			m06 = "",
			m08 = "",
			
			m1 = "";
			m12 = "",
			m14 = "",
			m16 = "",
			m18 = "",
			
			m2 = "";
			m22 = "",
			m24 = "",
			m26 = "",
			m28 = "",
			
			m3 = "";
			m32 = "",
			m34 = "",
			m36 = "",
			m38 = "",
			
			m4 = "";
			m42 = "",
			m44 = "",
			m46 = "",
			m48 = "",
			
			m5 = "";
			m52 = "",
			m54 = "",
			m56 = "",
			m58 = "";
		
		var n = Number(startHour);
		if(Number(startMinute/10)!=0){
			n+=1;
		}
		for(var i = 0, j = n ; i < 24; i++, j++){
			eval("m" + Number(startMinute/10) + "=" + j);
			
			startTimeLabel.push(m0);
			startTimeLabel.push(m02);
			startTimeLabel.push(m04);
			startTimeLabel.push(m06);
			startTimeLabel.push(m08);
			
			startTimeLabel.push(m1);
			startTimeLabel.push(m12);
			startTimeLabel.push(m14);
			startTimeLabel.push(m16);
			startTimeLabel.push(m18);
			
			startTimeLabel.push(m2);
			startTimeLabel.push(m22);
			startTimeLabel.push(m24);
			startTimeLabel.push(m26);
			startTimeLabel.push(m28);
			
			startTimeLabel.push(m3);
			startTimeLabel.push(m32);
			startTimeLabel.push(m34);
			startTimeLabel.push(m36);
			startTimeLabel.push(m38);
			
			startTimeLabel.push(m4);
			startTimeLabel.push(m42);
			startTimeLabel.push(m44);
			startTimeLabel.push(m46);
			startTimeLabel.push(m48);
			
			startTimeLabel.push(m5);
			startTimeLabel.push(m52);
			startTimeLabel.push(m54);
			startTimeLabel.push(m56);
			startTimeLabel.push(m58);
			
			if(j==24){ j = 0}
		};
		
		var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
		    chart: {
		        type: 'bar',
		        renderTo : $("#timeChart"),
		        backgroundColor : 'rgba(0,0,0,0)',
		        height : getElSize(200),
		        marginRight : getElSize(50),
		    	marginLeft : getElSize(50),
		    	spacingTop : -getElSize(100)
		    },
		    credits : false,
			title : false,
		    tooltip: {
		        enabled: false
		    },
		    xAxis: {
		    	labels: {
		    		style : {
						color : "rgba(0,0,0,0)",
						fontSize : getElSize(30),
						fontWeight : "bold"
					}
	            }
		    },
		    yAxis: {
		        startOnTick: true,
		        endOnTick: true,
		        gridLineWidth: 3,
		        title :{
		        	enabled:false
		        },
		        tickInterval: 3600,
		        minorGridLineColor: '#4f4f4f',
		        minorTickInterval:600,
		        max:86400,
		    	//categories : startTimeLabel,
				labels : {
					step: 1,
					formatter : function() {
						var val = this.value
						if(val>0){
							val=val/3600+Number(startHour)
						}else{
							val=Number(startHour)
						}
						if(val>24){
							val-=24
						}
						return val;
					},
					style : {
						color : "white",
						fontSize : getElSize(30),
						fontWeight : "bold"
					},
				},
		    },
		    legend: {
		        enabled: false
		    },
		    plotOptions: {
		        series: {
		            stacking: 'bar',
		            borderWidth: 0,
		            stickyTracking: true
		        },
		        line : {
					marker : {
						enabled : true
					}
				}
		    },
		    series: []
		};

		$("#" + id).highcharts(options);

		var status = $("#" + id).highcharts();
		//options = status.options;
		//options.series = [];
		options.title = null;
		options.exporting = false;
		
		if(selectOption==1){
			//getTimeData(options, today);
			getStatus(options, today);
		}else{
			getLampData(options, today);
			//getStatus(options, today);
		}
		
		
		///////////////////////// demo data
		
		
//		options.series.push({
//			data : [ {
//				y : Number(20),
//				segmentColor : "red"
//			} ],
//		});
	//	
	//	
//		for(var i = 0; i < 719; i++){
//			var color = "";
//			var n = Math.random() * 10;
//			if(n<=5){
//				color = "green";
//			}else if(n<=8){
//				color = "yellow";
//			}else {
//				color = "red";
//			}
//			options.series[0].data.push({
//				y : Number(20),
//				segmentColor : color
//			});
//		};
	//	
//		status = new Highcharts.Chart(options);
		
		
		
		////////////////////////////////
		
		
	};*/
	
	var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	
	function getStatus(options, today){
		
		var endDateTime=$("#today").val() +' '+ startHour +':' +startMinute;
		
		endDateTime = moment(endDateTime).subtract(2,'minutes').format("YYYY-MM-DD HH:mm:ss")
		
		var startDateTime=moment($("#today").val()).subtract(1, 'd').format("YYYY-MM-DD") +' ' + startHour +':' +startMinute
		
		var url = ctxPath + "/chart/getStatus.do";
		
		var param = "sDate=" + startDateTime + 
					"&eDate=" + endDateTime +
					"&dvcId=" + dvcId;
		
		console.log(url+"?"+param)
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
							
				$("#pmc-btn").css({
					"color" : "white",
					"background" : "black"
				});	
				
				$("#lamp-btn").css({
					"color" : "#999999",
					"background" : "#666666"
				});		
				
				var json = data.statusList;
				
				var color = "";
				var f_Hour;
				var f_Minute;
				var f_Second;
				if(json.length>0){
					f_Hour = json[0].startDateTime.substr(11,2);
					f_Minute = json[0].startDateTime.substr(14,2);
					f_Second = json[0].startDateTime.substr(17,2);
				}else{
					f_Hour = moment().hour();
					f_Minute = moment().minute();
					f_Second = moment().seconds();
				}
				
				if(status=="IN-CYCLE"){
					color = incycleColor;
				}else if(status=="WAIT"){
					color = waitColor;
				}else if(status=="ALARM"){
					color = alarmColor;
				}else if(status=="NO-CONNECTION"){
					color = noconnColor;
				}else if(status=="CUT"){
					color = cuttingColor;
				}else{
					color = "purple"
				}
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				var blank;
				
				var startN = 0;
				
				totalNum_Override=0;
				
				var pushArray=[];
				
				if(Number(f_Hour)==Number(startHour) && Number(f_Minute)==(Number(startMinute))){
					
				}else{
					if(f_Hour>=Number(startHour)){
						startN = (((f_Hour*60*60) + Number(f_Minute*60)+Number(f_Second)) - ((startHour*60*60) + (startMinute*60))); 
					}else{
						startN = ((24*60*60) - ((startHour*60*60) + (startMinute*60)));
						startN += (f_Hour*60*60) + (f_Minute*60);
					};
					
					pushArray.push({
						data : [startN],
						color: "gray"
					})
					
					for(var i=0;i<startN;i++){
						
						spdOverridePoint.push(Number(0));
					} 
				};
				
				
				
				$(json).each(function(idx, data){
					
						totalNum_Override+=data.diff;
						if(data.chart_status=="IN-CYCLE"){
							color = incycleColor;
							if(type=="IO"){
								color = cuttingColor;
							}
						}else if(data.chart_status=="WAIT"){
							color = waitColor;
						}else if(data.chart_status=="ALARM"){
							color = alarmColor;
						}else if(data.chart_status=="NO-CONNECTION"){
							color = noconnColor;
						}else if(data.chart_status=="CUT"){
							color = cuttingColor;
						}else{
							color = "purple"								
						}
						pushArray.push({
							data : [data.diff],
							color: color,
						})
						
						for(var i=0;i<data.diff;i++){
							spdOverridePoint.push(Number(data.spdOverride));
						}
				});
				
				var emptyArray=[];
				for(var i = 0; i < 86400-(totalNum_Override+startN); i++){
					
					/* options.series.push({
						data : [1],	
						color: "rgba(0,0,0,0)",
						pointPlacement: -0.2
					});	 */
					
					//spdLoadPoint.push(Number(-10));
					spdOverridePoint.push(Number(-10));
				};
				var insertArray = [];
				
				for(var i=pushArray.length-1;i>-1;i--){
					insertArray.push({
						data : pushArray[i].data,	
						color: pushArray[i].color,
					});	
				}
				
				/* insertArray.push({
					data : [86399-(totalNum_Override+startN)],	
					color: "rgba(0,0,0,0)",
				}); */
				
				Highcharts.chart('timeChart', {
					    chart: {
					        type: 'bar',
					        backgroundColor : 'rgba(0,0,0,0)',
					        height : getElSize(200),
					        marginRight : getElSize(50),
					    	marginLeft : getElSize(50),
					    	spacingTop : -getElSize(100),
					    	//width : getElSize(2200)
					    },
					    credits : false,
						title : false,
					    tooltip: {
					        enabled: false
					    },
					    boost : {
					    	useGPUTranslations : true
					    },
					    xAxis: {
					    	labels: {
					    		style : {
									color : "rgba(0,0,0,0)",
									fontSize : getElSize(20),
									fontWeight : "bold"
								}
				            },
				            visible : false
					    },
					    yAxis: {
					        startOnTick: false,
					        endOnTick: true,
					        gridLineWidth: 3,
					        title :{
					        	enabled:false
					        },
					        max: 86400,
					        tickInterval: 3600,
					        minorGridLineColor: '#4f4f4f',
					        minorTickInterval:600,
							labels : {
								step: 1,
								formatter : function() {
									var val = this.value
									
									if(val>0){
										val=val/3600+Number(startHour)
									}else{
										val=Number(startHour)
									}
									if(val>=24){
										val-=24
									}
									if(startMinute>0){
										return val+":"+startMinute;
									} 
									return val;
								},
								style : {
									color : "white",
									fontSize : getElSize(25),
									fontWeight : "bold"
								},
							},
					    },
					    legend: {
					        enabled: false
					    },
					    plotOptions: {
					        series: {
					            stacking: 'bar',
					            borderWidth: 0,
					            pointWidth:getElSize(170),
					            stickyTracking: true,
					            turboThreshold: 3000
					        },
					        line : {
								marker : {
									enabled : true
								}
							}
					    },
					    series: insertArray
				});
				
				getSpindleDate2();
				setInterval(function(){
					var cMinute = String(addZero(new Date().getMinutes())).substr(0,1);
					if(targetMinute!=cMinute){
						if($("#today").val()==moment().format("YYYY-MM-DD")){
							targetMinute = cMinute;
							if(selectOption==1){
								getStatus();
							}else{
								getLampData();
							}
							
						}
					};
				},60000)
				
			},error:function(e){
				console.log(e)
			}
		})
		
	}
	
	
	
	function getLampData(options, today){
		
		var green = incycleColor;
		var greenBlink = incycleColor;
		var yellow = "yellow";
		var yellowBlink = "yellow";
		var red = "red";
		var redBlink = "red";
		var gray= "#A0A0A0";
		
		var endDateTime=$("#today").val() +' '+ startHour +':' +startMinute;
		
		endDateTime = moment(endDateTime).subtract(2,'minutes').format("YYYY-MM-DD HH:mm:ss")
		
		var startDateTime=moment($("#today").val()).subtract(1, 'd').format("YYYY-MM-DD") +' ' + startHour +':' +startMinute
		
		var url = ctxPath + "/chart/getLampData.do";
		
		var param = "startDateTime=" + startDateTime + 
					"&endDateTime=" + endDateTime +
					"&dvcId=" + dvcId;
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				
				console.log("Lamp Data : ")
				console.log(data)
			
				
				$("#pmc-btn").css({
					"color" : "#999999",
					"background" : "#666666"
				});	
				
				$("#lamp-btn").css({
					"color" : "white",
					"background" : "black"
				});		
				
			
				var json = data.statusList;
				
				
				var color = "";
				
				var pushArray=[];
				var pushNum=1;
				
				var status = json[0].status;
				if(status=="green"){
					color = green;
				}else if(status=="yellow"){
					color = yellow;
				}else if(status=="red"){
					color = red;
				}else if(status=="gray"){
					color = gray;
				}else if(status=="greenBlink"){
					color = greenBlink;
				}else if(status=="yellowBlink"){
					color = yellowBlink;
				}else if(status=="redBlink"){
					color = redBlink;
				}
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startN = 0;
				
				if(Number(f_Hour)==Number(startHour) && Number(f_Minute)==(Number(startMinute))){
					
					pushArray.push({
						data : [pushNum],
						color: color
					})
					
				}else{
					if(f_Hour>=Number(startHour)){
						startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					pushArray.push({
						data : [pushNum],
						color: gray
					})
					
					for(var i = 0; i < startN-1; i++){
						pushArray.push({
							data : [pushNum],
							color: gray
						})
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				};
				
				var totalNum=0;
	
				$(json).each(function(idx, data){
					spdLoadPoint.push(Number(data.spdLoad));
					spdOverridePoint.push(Number(data.spdOverride));
					
					totalNum++;
					
					if(data.status=="green"){
						color = green;
					}else if(data.status=="yellow"){
						color = yellow;
					}else if(data.status=="red"){
						color = red;
					}else if(data.status=="gray"){
						color = gray;
					}else if(data.status=="greenBlink"){
						color = greenBlink	
					}else if(data.status=="yellowBlink"){
						color = yellowBlink	
					}else if(data.status=="redBlink"){
						color = redBlink	
					}
					
					pushArray.push({
						data : [pushNum],
						color: color
					})
				});
				insertArray = [];
				var insertArray = [];
				for(var i=0;i<719-totalNum;i++){
					insertArray.push({
						data : [1],	
						color: "rgba(0,0,0,0)",
					});	
				}
				
				var totalTime=0;
				for(var i=pushArray.length-1;i>-1;i--){
					insertArray.push({
						data : [1],	
						color: pushArray[i].color,
					});	
					
				}
				
				Highcharts.chart('timeChart', {
				    chart: {
				        type: 'bar',
				        backgroundColor : 'rgba(0,0,0,0)',
				        height : getElSize(200),
				        marginRight : getElSize(50),
				    	marginLeft : getElSize(50),
				    	spacingTop : -getElSize(100)
				    },
				    credits : false,
					title : false,
				    tooltip: {
				        enabled: false
				    },
				    boost : {
				    	useGPUTranslations : true
				    },
				    xAxis: {
				    	visible:false,
				    	labels: {
				    		style : {
								color : "rgba(0,0,0,0)",
								fontSize : getElSize(30),
								fontWeight : "bold"
							}
			            }
				    },
				    yAxis: {
				        startOnTick: true,
				        endOnTick: true,
				        gridLineWidth: 3,
				        title :{
				        	enabled:false
				        },
				        tickInterval: 30,
				        minorGridLineColor: '#4f4f4f',
				        minorTickInterval:5,
				        max:720,
						labels : {
							step: 1,
							formatter : function() {
								var val = this.value
								if(val>0){
									val=val/30+Number(startHour)
								}else{
									val=Number(startHour)
								}
								if(val>=24){
									val-=24
								}
								if(startMinute>0){
									return val+":"+startMinute
								}
								return val;
							},
							style : {
								color : "white",
								fontSize : getElSize(25),
								fontWeight : "bold"
							},
						},
				    },
				    legend: {
				        enabled: false
				    },
				    plotOptions: {
				        series: {
				            stacking: 'bar',
				            borderWidth: 0,
				            pointWidth:getElSize(170),
				            stickyTracking: true,
				            turboThreshold: 3000
				        },
				        line : {
							marker : {
								enabled : true
							}
						}
				    },
				    series: insertArray
			});
			$('#timeChart_td').loading('stop'); 
			},error : function(e1,e2,e3){
			}
		});
	};
	
	
	function getSpindleDate2(){
		spdLoadPoint=[];
		var endDateTime=$("#today").val() +' '+ startHour +':' +startMinute;
		
		endDateTime = moment(endDateTime).subtract(2,'minutes').format("YYYY-MM-DD HH:mm:ss")
		
		var startDateTime=moment($("#today").val()).subtract(1, 'd').format("YYYY-MM-DD") +' ' + startHour +':' +startMinute
		
		var url = ctxPath + "/chart/getSpindleData2.do";
		
		if(moment($("#today").val()).isBefore(moment().format("YYYY-MM-DD"))){
			url = ctxPath +"/getOldSpindleData.do";
		}
		
		var param = "sDate=" + startDateTime + 
					"&eDate=" + endDateTime +
					"&YYMM=" + moment($("#today").val()).format("YYMM") + 
					"&dvcId=" + dvcId;
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				$('#up').prop("disabled", false);
				$('#down').prop("disabled", false);
			
				var json = data.statusList;
				var startN=0;
				var f_Hour;
				var f_Minute;
				var f_Second;
				var totalNum=0;
				
				if(json.length>0){
					f_Hour = json[0].startDateTime.substr(11,2);
					f_Minute = json[0].startDateTime.substr(14,2);
					f_Second = json[0].startDateTime.substr(17,2);
				}else{
					f_Hour = moment().hour();
					f_Minute = moment().minute();
					f_Second = moment().seconds();
				}
				
				if(Number(f_Hour)==Number(startHour) && Number(f_Minute)==(Number(startMinute))){
					
					checkList=true;
					
				}else{
					if(f_Hour>=Number(startHour)){
					
						startN = (((f_Hour*60*60) + Number(f_Minute*60)) - ((startHour*60*60) + (startMinute*60))); 
					}else{
						startN = ((24*60*60) - ((startHour*60*60) + (startMinute*60)));
						startN += (f_Hour*60*60) + (f_Minute*60);
					};
					
					for(var i = 0; i < startN-1; i++){
						spdLoadPoint.push(Number(0));
					};
				};
				
				$(json).each(function(idx, data){
				
					if(totalNum<=totalNum_Override){
						
						totalNum+=Number(data.timeDiff);
						for(var i=0;i<data.timeDiff;i++){
							spdLoadPoint.push(Number(data.spdLoad));
						}
					} 
				});
				
				
				
				$('#timeChart_td').loading('stop');
				
			},error : function(e){
				
			}
		});
		
	}
	
	function drawPrdctChart(ratio){
		$("#target_ratio").html(Math.round(ratio) + "%").css({
			"color" : "#0080FF",
			"font-size" : getElSize(100),
			"right" : getElSize(30),
			"top" : getElSize(850)
		});
		
		$('#prdctChart').highcharts({
		    chart: {
		    	backgroundColor : "rgba(0,0,0,0)",
		    	height : getElSize(230),
		    	marginRight : getElSize(50),
		    	marginLeft : getElSize(50),
		        type: 'bar'
		    },
		    credits : false,
		    exporting : false,
		    title: {
		        text: null
		    },
		    xAxis: {
		    	/* gridLineWidth:0,
		    	lineWidth: 0,*/
		    	tickWidth: 0, 
				labels: {
		       		enabled: false
		    	},    
		    },
		    yAxis: {
			    gridLineWidth: 0,
				minorGridLineWidth: 0,
		        min: 0,
		        max :100,
				labels : {
					style : {
						color : "#7A7B7C",
						fontSize :getElSize(30)
					}
		        },
		        title: {
		            text: null
		        }
		    },
		    tooltip : {
				enabled : false
			},
		    legend: {
		        reversed: true,
		        enabled : false
		    },
		    plotOptions: {
		        series: {
		            stacking: 'normal',
		            groupPadding: 0,
		            pointPadding: 0,
		            borderWidth: 0
		        }
		    },
		    /* series: [{
		    		color : "#BBBDBF",
		        name: 'target',
		        data: [10]
		    },{
		    		color : "#A3D800",
		        name: 'goal',
		        data: [10]
		    }] */
		    series: [{
	    		color : "#A3D800",
	        name: 'goal',
	        data: [Math.round(ratio)]
	    }]
		});			
	};
	
	function getAlarmList(){
		var url = "${ctxPath}/chart/getAlarmList.do";
		
		var sDate = moment($("#today").val())
		var eDate = moment($("#today").val())
		
		sDate = sDate.hour(Number(startHour)).minute(Number(startMinute))
		sDate = sDate.subtract(1,'days')
		sDate = sDate.format("YYYY-MM-DD HH:mm")
		
		eDate = eDate.hour(Number(startHour)).minute(Number(startMinute))
		eDate = eDate.format("YYYY-MM-DD HH:mm")
		
		
		var param = "dvcId=" + dvcId + 
					"&sDate=" + sDate + 
					"&eDate=" + eDate;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var alarm = "";
				
				
				$(json).each(function(idx, data){
					alarm += "[" + data.startDateTime.substr(5,14) + "] - " +data.alarmCode+" "+  decodeURIComponent(data.alarmMsg).replace(/\+/gi, " ")  + "<br><br>";
				});
	
				if(alarm!=""){
					alarm = "<font style='color:red' id='alarm_span'>-Alarm</font><br><div style='height:" + getElSize(350) + "px; overflow:auto;'>" + alarm + "</div>";
				}
				
				$("#alarm_div").html(alarm).not("#alarm_span").css({
					"color" : "white" ,
					"padding" : getElSize(20) + "px" 
				});
				
			 	$("#timeChart_td").loading({
					theme: 'dark'		
				}); 
			}
		});

	};
	
	var type;
	
	
	
	
	
	
	
	var selectOption=1;
	function getTimeChartData(){
		selectOption=1
		
	}
	
	function getLampChartData(){
		selectOption=2
		
	}
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
</script>
<script type="text/javascript" src="${ctxPath }/js/singleChartStatus.js"></script>
</head>
<body>
	<div id="container">
		
	 </div>
</body>
</html>	