<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

.btn:hover {
	background: lightcoral;
}

.btn{
	background: linen;
	cursor : pointer;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("maintenance", "SupplierManage")
	}
	
	//업체 모든 리스트
	var vndAllList = [];
	//품번 모든 리스트
	var prdNoAllList = [];
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// getDeliverList => getTable()
		//작업자 리스트
		getDeliverList();
		//업체,품번리스트
		getListPrdNo();
		

		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
// 			,"background" : "red"
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
// 			,"background" : "blue"
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
	}
	
	function getListPrdNo(){
		
		var param = "shopId=" + shopId;
		var url = ctxPath + "/chart/getListPrdNo.do";
		
		$.ajax({
			url : url
			,data : param
			,type : "post"
			,dataType : "json"
			,success : function(data){
				var json = data.dataList;
				console.log(data);
				$(json).each(function(idx,data){
					var obj = new Object();
					obj.value = data.idx;
					obj.name = data.MATNo;
					
					prdNoAllList.push(obj);
					
				})
			}
		})
		
	}
	
	//작업자 리스트
	function getDeliverList(){
		var url = ctxPath + "/chart/getDeliverer.do";
		var param = "shopId=" + shopId;
		console.log(shopId)
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.chartStatus;
				console.log(json)
// 				var option = "<option value='ALL'>${total}</option>";
				var option = "";
				
				vndAllList = [];
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
					var obj = new Object();
					obj.value = data.id;
					obj.name = decode(data.name);
					vndAllList.push(obj);
				});
				
				$("#vndNo").html(option);
				
				$("#vndNo").change(function(){
					getTable();
				})
				getgrid();
				
			},error : function(e){
				console.log("er")
			}
		});
	}
	
	function prdNoauto(container, options, a){
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: prdNoAllList,
			autoWidth: true,
			dataTextField: "name",
			dataValueField: "value",
            placeholder: "품번 선택",
            change:function(e){
            	console.log(e)
            }
        }).data("kendoComboBox");
	}
	
	function viewVndNo(vndNo){
		for(i=0 , len = vndAllList.length; i<len; i ++){
			if(vndAllList[i].value == vndNo){
				return vndAllList[i].name;
			}
		}
		
	}
	
	function viewPrdNo(prdNo){
		for(i=0 , len = prdNoAllList.length; i<len; i ++){
// 			console.log(prdNoAllList[i].value + " , " + prdNo)
			if(prdNoAllList[i].value==prdNo){
				return prdNoAllList[i].name;
			}
		}
		
		// 미입력
		if(prdNo=="")
			return "===품번 선택==="
	}
	function getgrid(){
		$("#grid").kendoGrid({
			height : getElSize(1650)
			,editable : true
			,dataBound : function(e){
				
// 				$(".btn").hover(function(){
// 					"background" : "lightcoral"
// 				}, function(){
// 					"cursor" : "pointer"
// 					,"background" : "linen"
// 				})
				
				
			
// 				lightcoral
			}
			,columns : [{
				field : "vndNo"
				,template : "#=viewVndNo(vndNo)#"
			},{
				field : "prdNo"
				,editor : prdNoauto
				,template : "#=viewPrdNo(prdNo)#"
			},{
				field : "idx"
				,template : "<button onclick='del(#=idx#)' class='btn'><span class='k-icon k-i-delete k-i-trash'></span>삭제</button>"
				,width : getElSize(300)
			}]
		})
		
		getTable();
	}
	
	function del(evt){
		
		var param = "idx=" + Number(evt);
		var url = ctxPath + "/chart/delVNDPRDMST.do";
		$.showLoading;
		$.ajax({
			url : url
			,data : param
			,type : "post"
			,success : function(e){
				getTable();
				$.hideLoading;
			}
		})
		
	}
	
	function getTable(){
		
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&vndNo=" + $("#vndNo").val()
		
		var url = ctxPath + "/chart/getVndList.do";
		
		console.log(param)
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				console.log(json)
				
				kendodata = new kendo.data.DataSource({
					data : json
					,schema: {
						model: {
							id: "id",
							fields: {
								vndNo : {editable : false}
								,idx : {editable : false}
							}
						}
					}
				})
				
				$("#grid").data("kendoGrid").setDataSource(kendodata)
				$.hideLoading();

			},error : function(e){
				console.log("er")
			}
		});		
		
		
	}
	
	function addRow(){
		
		$("#grid").data("kendoGrid").dataSource.insert(
			0,{
			idx : null
			,vndNo : $("#vndNo").val()
			,prdNo: ""
			
			}
		)
	}
	
	function saveRow(){
		var list = $("#grid").data("kendoGrid").dataSource.data();
		
		for(i=0; i<list.length; i++){
			if(list[i].prdNo==""){
				alert("품번을 선택해 주세요.");
				return
			}
			
		}
		var url = "${ctxPath}/chart/vndListSave.do";
		
		var obj = new Object();
		obj.val = list
		
		var param = "val=" + JSON.stringify(obj);
		
		$.showLoading();
		console.log(param);

		$.ajax({
			url : url
			,data : param
			,type : "post"
			,success : function(data){
				console.log(data)
				getTable();
				$.hideLoading();
			}
		})
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				업체명 : <select id="vndNo"></select>
<!-- 				날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly"> -->
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
				<button onclick="addRow()"><span class="k-icon k-i-plus-outline"></span>추가</button>
				<button onclick="saveRow()"><span class="k-icon k-i-save k-i-floppy"></span>저장</button>
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
			</div>
		</div>
	</div>	
</body>
</html>	