<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

#FpChkCnt text{
	fill : white
}


</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>
	const loadPage = () =>{
		createMenuTree("analysis", "FPAnalysis")
	}
	
	$(function(){
		setEl();
		//datepicker event && date data input 
		dateEvt();
		// getWorkerList => getTable()
		getWorkerList();
		
		gridTable();
		FpChkCnt();
		setEl();
	})
	
	//datepicker event
	function dateEvt(){
		
		$("#sDate").val(moment().format("YYYY-MM-01"));
		$("#eDate").val(moment().subtract("day",0).format("YYYY-MM-DD"));
		
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
		})
		
		$("#section1").css({
			"width" : $("#div2").width() * 0.55 - getElSize(5)
			,"height" : $("#div2").height()
			,"float" : "left"
		})

		$("#section2").css({
			"width" : $("#div2").width() * 0.45 - getElSize(5)
			,"height" : $("#div2").height()
			,"float" : "left"
		})

		$("#article1").css({
			"width" : $("#section1").width()/2 - getElSize(1)
			,"height" : $("#section1").height()/2
			,"float" : "left"
		})

		$("#article2").css({
			"width" : $("#section1").width()/2 - getElSize(1)
			,"height" : $("#section1").height()/2
			,"float" : "left"
		})

		$("#article3").css({
			"width" : $("#section1").width()
			,"height" : $("#section1").height()/2
			,"float" : "left"
		})
		
// 		$("#section1 .drawChart").css({
// 			"height" : $("#article1").height() * 0.85
			
// 		})
		
// 		$("#section1 .chartTitle").css({
// 			"height" : $("#article1").height() * 0.15
// 			,"background" : "black"
// 		})

// 		$("#section2 .drawChart").css({
// 			"height" : $("#section2").height() * 0.85
// 		})
		
// 		$("#section2 .chartTitle").css({
// 			"height" : $("#section2").height() * 0.15
// 			,"background" : "slateblue"
// 		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
		
	}
	
	//작업자 리스트
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>"; 
				});
				
				$("#worker").html(option);
			}
		});
	}
	
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height : $("#article3").height()
			,filterable: true
			,toolbar :[{name:"excel"}]
			,excel:{
				fileName:"FP발생분석" + moment().format("YYYY-MM-DD") + ".xlsx"
			}
			,dataBound : function(e){
				$(".k-grid tr th").css({
					"font-size" : getElSize(42)
				})

				$(".k-grid tr td").css({
					"font-size" : getElSize(38)
				})
			}
			,columns : [{
				field : "ty"
				,title : "FP 종류"
				,filterable: {
	                multi: true,
	                search: true
	            }
				,width : getElSize(240)
			},{
				field : "name"
				,title : "장비"
				,filterable : false
				,width : getElSize(580)
			},{
				field : "startTime"
				,filterable : false
				,title : "발생시간"
				,width : getElSize(520)
			},{
				field : "nm"
				,title : "작업자"
				,filterable: {
	                multi: true,
	                search: true
	            }
			},{
				field : "cause"
				,filterable: {
	                multi: true,
	                search: true
	            }
				,title : "FP 원인"
			}]
		}).data("kendoGrid");
		
		getTable();

	}
	
	var drawing = kendo.drawing;
	var geometry = kendo.geometry;
	function createColumn(rect, color) {
        var origin = rect.origin;
        var center = rect.center();
        var bottomRight = rect.bottomRight();
        var radiusX = rect.width() / 2;
        var radiusY = radiusX / 3;
        var gradient = new drawing.LinearGradient({
            stops: [{
                offset: 0,
                color: color
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 1,
                color: color
            }]
        });

        var path = new drawing.Path({
                fill: gradient,
                stroke: {
                    color: "none"
                }
            }).moveTo(origin.x, origin.y)
            .lineTo(origin.x, bottomRight.y)
            .arc(180, 0, radiusX, radiusY, true)
            .lineTo(bottomRight.x, origin.y)
            .arc(0, 180, radiusX, radiusY);

        var topArcGeometry = new geometry.Arc([center.x, origin.y], {
            startAngle: 0,
            endAngle: 360,
            radiusX: radiusX,
            radiusY: radiusY                
        });

        var topArc = new drawing.Arc(topArcGeometry, {
            fill: {
                color: color
            },
            stroke: {
                color: "#BDBDBD"
            }
        });
        var group = new drawing.Group();
        group.append(path, topArc);
        return group;
    }
	
	function FpChkCnt(){
		
		var param = "sDate=" + $("#sDate").val() +
		"&eDate=" + $("#eDate").val() +
		"&dvcId=" + $("#group").val();
		var url = ctxPath + "/kpi/FpQuartCnt.do"
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				
				var xData=[];
				var yData=[];
				$(json).each(function(idx,data){
					yData.push(data.category + "월");
					xData.push(data.value)
				})
		
				$("#FpChkCnt").kendoChart({
		            title: {
		                text: "단위기간별 FP 발생 수 (수량/월)",
		              	position : "bottom",
		              	color : "white",
		                font:getElSize(80) + "px sans-serif",
		            },
		            legend: {
		                visible: false
		            },
		            seriesDefaults: {
		            	gap: getElSize(1),
						type: "column" ,
						spacing: getElSize(0.5),
		 				labels:{
		 					font:getElSize(36) + "px sans-serif",	//no working
		 					margin:0,
		 					padding:0
						}, 
						visual: function (e) {
			                return createColumn(e.rect, e.options.color);
			            }
// 		                ,type: "bar"
		            },
		            dataSource: {
		                data: json
		            },
		            series: [{
		                name: "Total Visits",
		                data: xData,
		                color : "#EC1C24"
		                
	                	,labels: {
	   	                  visible: true,
	   	                  background:"#EC1C24",
// 	   	                  rotation: 270,
	   	                }
		            }],
		          chartArea: {
		            background:"#121212"
		          },
		          valueAxis: {
//		        	  	majorUnit: 1,
		                line: {
		                    visible: true,
		                    color : "white"
		                },
		                majorGridLines: {
		                    color : "#353535"
		                },
		                labels: {
		                    rotation: "auto",
		                    step : 2,
		                    color:"white",
		                    font:getElSize(50) + "px sans-serif",
		                }
		            },
		            
		            categoryAxis: {
		                categories: yData,
		                line:{
		                	viible:true,
		                	color:"white"
		                },
		                majorGridLines: {
		                    color:"#121212"
		                },
		                color:"white",
		                labels:{
		                    font:getElSize(48) + "px sans-serif",
		                }
		            },
		           
		            tooltip: {
		                visible: true,
		                template: "#= category #: #= value # EA"
		            }
		        });
				
				$.hideLoading();
			}
		})
        
	}
	
	//데이터 가져오고기
	function getTable(){
		
		var param = "sDate=" + $("#sDate").val() +
		"&eDate=" + $("#eDate").val() +
		"&dvcId=" + $("#group").val();
		var url = ctxPath + "/kpi/getFpList.do"
		
		console.log(param);
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			success : function(data){
				abc = data.dataList;
				var json = data.dataList;
				var list = [];
				$(json).each(function(idx,data){
					data.cause = decode(data.cause);
					data.nm = decode(data.nm);
					data.time = data.time + " 분"
					
					var arr = {}
					arr.value = idx;
					arr.category = data.name;
					list.push(arr);
				})
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					schema: {
						model: {
							id: "id",
							fields: {
								rcvCntR: { editable: true, type: "number" },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);
				console.log(data.dataList)
				
				
				//pie chart 1 그리기
				var dupChk=[];
				var count=0;
				var list=[];
				var name;

				//pie 그림 그리기
				//장비별
				for(i=0,length=json.length; i<length; i++){
					json[i].name=decode(json[i].name)
					if(dupChk.indexOf(json[i].name)==-1 ){
						for(j=0,length=json.length; j<length; j++){
							json[j].name=decode(json[j].name)
							
							if(json[i].name==json[j].name){
								count++;
								name=json[i].name;
							}							
							if(j==length-1 && count!=0 ){
								var arr={};
								arr.value=count;
								arr.category=name;
								list.push(arr);
								dupChk.push(name);
								count=0;
								
							}
						}					
					
					}

				}
				byDvcPie(list);
				
				
				var dupChk=[];
				var count=0;
				var list=[];
				var name;
				//pie 그림 그리기
				
				//프로그램별
				for(i=0,length=json.length; i<length; i++){
					if(dupChk.indexOf(json[i].ty)==-1){
						for(j=0,length=json.length; j<length; j++){	
	
							if(json[i].ty==json[j].ty){
								count++;
								name=json[i].ty;
							}							
							if(j==length-1 && count!=0){
								var arr={};
								arr.value=count;
								arr.category=name;
								list.push(arr);
								dupChk.push(name);
								count=0;
								
							}
						}					
					
					}

				}
				byTyCnt(list);

				$.hideLoading();
				
				
			}
		})
	}
	
	function byDvcPie(list) {
		var totalCnt=0;
		/* for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		} */
		list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		var copy=[]
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			copy.push(list[i]);
		}
		list=copy;
        $("#byDvcPie").kendoChart({
        	chartArea: {
				background:"#121212",
			},
            title: {
            	font:getElSize(80) + "px sans-serif",	
            	position: "bottom",
                text: "TOP 5 장비별",
                color : "white"
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category # \n #= value# EA"
                }
            },
            dataSource: {
                data: list	
            },
            seriesColors:["red","#0080FF","#1D8B15","#990085","#4D00ED","#DB005B","#FFBB00"],
            series: [{
                type: "pie",
                startAngle: 100,
                field: "value",
                padding:0,
                labels: {
                    visible: true,
                    position: "center"
                }
            }],
            tooltip: {
                visible: true,
                template: "#= category #: #= value # EA"
//                format: "{0}EA"
            }
        });
    }
	
	function byTyCnt(list) {
		list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		
		var xData=[];
		var yData=[];
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			xData.push(list[i].value)
			yData.push(list[i].category);
			
			
		}
        $("#byTyCnt").kendoChart({
            title: {
                text: "FP별 발생 빈도",
              	position : "bottom",
              	color : "white",
                font:getElSize(80) + "px sans-serif",
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                type: "bar"
            },
            dataSource: {
                data: list
            },
            series: [{
                name: "Total Visits",
                data: xData,
                color : "red"
            }],
          chartArea: {
            background:"#121212"
          },
          valueAxis: {
//        	  	majorUnit: 1,
                line: {
                    visible: true,
                    color : "white"
                },
                majorGridLines: {
                    color : "#353535"
                },
                labels: {
                    rotation: "auto",
                    step : 2,
                    color:"white",
                    font:getElSize(50) + "px sans-serif",
                }
            },
            
            categoryAxis: {
                categories: yData,
                line:{
                	viible:true,
                	color:"white"
                },
                majorGridLines: {
                    color:"#121212"
                },
                color:"white",
                labels:{
                    font:getElSize(48) + "px sans-serif",
                }
            },
           
            tooltip: {
                visible: true,
                template: "#= category #: #= value # EA"
            }
        });
    }
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				작업자 : <select id="worker"></select>
				날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
			</div>
		</div>
		
		<div id="div2">
			<div id="section1">
				<div id="article1">
					<div id="byDvcPie" style="height: 100%;">
					</div>
				</div>
				<div id="article2">
					<div id="byTyCnt" style="height: 100%;">
					
					</div>
					<div class="drawChart">
						그림
					</div>
					<div class="chartTitle">
						제목
					</div>
				</div>
				<div id="article3">
					<div id="grid">
					</div>
				</div>
			</div>
			
			<div id="section2">
				<div id="FpChkCnt" style="height: 100%;">
					
				</div>
<!-- 				<div class="drawChart">
					그림
				</div>
				<div class="chartTitle">
					제목
				</div> -->
			</div>
		</div>
	</div>	
</body>
</html>	