<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<%-- <link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css"> --%>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<title>전체 장비 24hr chart</title>


<!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.min.js"></script>

<!-- <script src="https://code.highcharts.com/modules/xrange.js"></script> -->
<!-- <script src="https://code.highcharts.com/modules/annotations.js"></script> -->
         
<!-- <script src="https://code.highcharts.com/modules/solid-gauge.js"></script> -->



<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	
}
</style> 

<script type="text/javascript">
	
</script>
<script type="text/javascript" src="${ctxPath }/js/dashBoard3.js"></script>
</head>
<body>
	<div id="container">
		
	 </div>
</body>
</html>	