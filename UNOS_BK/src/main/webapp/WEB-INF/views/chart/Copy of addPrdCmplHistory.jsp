<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<spring:message code="start_time" var="start_time"></spring:message>
<spring:message code="end_time" var="end_time"></spring:message>
<spring:message code="fixed_time" var="fixed_time"></spring:message>
<spring:message code="alarm" var="alarm"></spring:message>
<spring:message code="content" var="content"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"

	function getPrdNo(){
	var url = "${ctxPath}/chart/getPrdNo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='ALL'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
			});
			
			$("#group").html(option);
			
			getPrdData();
		}
	});
};
$(function(){
	getPrdNo();
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	/* $("#date").change(getPrdData);
	$("#group").change(getPrdData); */
	getWorkerList();
});
var className = "";
var classFlag = true;

var popup = false;
function getPrdData(){
	classFlag = true;
	var url = "${ctxPath}/chart/getPrdData.do";
	var sDate = $("#date").val();
	var workIdx = $("#workIdx").val();
	var param = "workDate=" + sDate +
				"&prdNo=" + $("#group").val() + 
				"&workIdx=" + workIdx;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<thead>" + 
						"<Tr style='background-color:#222222'>" +
							"<Td rowspan='4'>" +
								"생산차종" +
							"</Td>" +
							"<Td rowspan='4'>" +
								"공정" +
							"</Td>" +
							"<Td rowspan='4'>" +
								"장비" +
							"</Td>" +
							"<Td colspan='4'>" +
								"설비별 UPH 분석<br>(시간당 생산수량)" +
							"</Td>" +
							"<Td rowspan='4'>" +
								"일자" +
							"</Td>" +
							"<Td rowspan='4'>" +
								"구분" +
							"</Td>" +
							"<Td colspan='12'>" +
								"생산계획 대비 실적 분석" +
							"</Td>" +
						"</Tr>" + 
						"<tr style='background-color:#222222'>" + 
							"<Td rowspan='2' colspan='2'>" + 
								"Cycle<br>Time" + 
							"</td>" +
							"<Td rowspan='3' >" + 
								"Ca<br>vi<br>ty" + 
							"</td>" + 
							"<Td rowspan='3' >" + 
								"U<br>P<br>H" + 
							"</td>" +
							"<td rowspan='3'>" + 
								"작업자" + 
							"</td>" +  
							"<td rowspan='3'>" + 
								"근무<br>시간<Br>(분)" + 
							"</td>" +
							"<td rowspan='3'>" + 
								"생산<br>Capa<Br>(EA)" + 
							"</td>" +
							"<td rowspan='3'>" + 
								"생산<br>계획<Br>(EA)" + 
							"</td>" +
							"<td colspan='3'>" + 
								"생산실적" + 
							"</td>" +
							"<td rowspan='3'>" + 
								"불량" + 
							"</td>" +
							"<td rowspan='3'>" + 
								"양<br>품<Br>율<br>(%)" + 
							"</td>" +
							"<td rowspan='3'>" + 
								"달<br>성<Br>율<br>(%)" + 
							"</td>" +
						"</td>" +
						"</Tr>" + 
						"<tr  style='background-color:#222222'>" + 
							"<td rowspan='2'>" + 
								"장비" + 
							"</td>" + 
							"<td rowspan='2'>" + 
								"작업자<br>실적" + 
							"</td>" +
							"<td rowspan='2'>" + 
								"차이" + 
							"</td>" +
						"</tr>" + 
						"<tr  style='background-color:#222222'>" +
							"<Td>" + 
							 	"분" + 
							"</td>" + 
							"<Td>" + 
							 	"초" + 
							"</td>" +
						"</tr>" + 
						"</thead><tbody>";
						
			$(json).each(function(idx, data){
				if(data.seq!="."){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var nd;
					if(data.nd==1){
						nd = "야간"
					}else{
						nd = "주간"
					};
					var oprNo;
					if(data.oprNm=="0010"){
						oprNo = "R삭";
					}else if(data.oprNm=="0020"){
						oprNo = "MCT 1차";
					}else if(data.oprNm=="0030"){
						oprNo = "CNC 2차";
					}
					
					tr += "<tr class='contentTr " + className + "' >" +
								"<td >" + data.prdNo + "</td>" +
								"<td>" + oprNo + "</td>" + 
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
								"<td>" + data.cylTmMi + "</td>" + 
								"<td>" + data.cylTmSec + "</td>" + 
								"<td>" + data.cvt + "</td>" + 
								"<td>" + data.uph + "</td>" + 
								"<td>" + $("#date").val() + "</td>" +
								"<td>" + nd + "</td>" +
								"<td>" + decode(data.worker) + "</td>" +
								"<td>" + data.workTm + "</td>" +
								"<td>" + data.capa + "</td>" +
								"<td>" + data.target + "</td>" +
								"<td>" + data.cnt + "</td>" +
								"<td>" + data.workerCnt + "</td>" +
								"<td>" + (Number(data.workerCnt) - Number(data.cnt)) + " </td>" +
								"<td>" + data.faultCnt + "</td>" +
								"<td>" + data.okRatio + "</td>" +
								"<td>" + data.goalRatio + "</td>" +
						"</tr>";					
				}
			});
			
			tr += "</tbody>";
			
			$(".alarmTable").html(tr).css({
				"font-size": getElSize(40),
			});
			
			$(".alarmTable td").css({
				"padding" : getElSize(20),
				"font-size": getElSize(40),
				"border": getElSize(5) + "px solid black"
			});
			
			$("#wrapper").css({
				"height" :getElSize(1700),
				"width" : "95%",
				"overflow" : "hidden"
			});
			
			$("#wrapper").css({
				"margin-left" : (contentWidth/2) -($("#wrapper").width()/2)
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('.alarmTable'), getElSize(1300));
			$("#wrapper div:last").css("overflow", "auto")
			
			$("button").css("font-size",getElSize(40));
		}
	});
};

var workerList = "<select>";
function getWorkerList(){
	var url = "${ctxPath}/common/getWorkerList.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			workerArray = []
			if(json.length>=50) popup = true; 
			$(json).each(function(idx, data){
				workerList += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				var array = [data.id, decode(data.name)];
				workerArray.push(array)
			});
			
			workerList += "</select>";
			
			if(popup){
				workerList = "<button onclick='popupWorker(this)'>선&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;택</button>";	
			}
		}
	});
};
	
function searchWorker(){
	var url = "${ctxPath}/chart/searchWorker.do";
	var param = "name=" + $("#search_name").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
		
			var div = "";
			$(json).each(function(idx, data){
				var br = "";
				if(idx%5==0 && idx!=0){
					br = "<br>";
				}
				
				var className = "";
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				div +=  br + "<div style='color:white;float :left ' class='" + className + " worker' id='" + data.id + "'>" + decode(data.name) + "</div>";	
			});
				
			$("#workPop_list").html(div);		
			
			$(".worker").click(selectWorker)
			
			$("#workerPop .worker").css({
				"padding" : getElSize(30),
				"width" : getElSize(300),
				"height" : getElSize(120),
				"text-align" : "center",
				"cursor" :"pointer",
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
		}
	});
};

function chkKeyCd(evt){
	if(evt.keyCode==13) searchWorker();
};

function selectWorker(){
	var id = this.id;
	var name = $(this).html();
	
	$("#workerPop").remove();
	closeCorver();
	$(selected_btn).html(name)
	$(selected_btn).parent("td").attr("id", id);
};

var selected_btn;
var workerArray = [];
function popupWorker(el){
	selected_btn = el;
	showCorver();
	
	var div = "<div id='workerPop'><input type='text' id='search_name' onkeyup='chkKeyCd(event)'> <button id='search_btn' onclick='searchWorker();'>검색</button><div id='workPop_list'>"; 
	
	
	for(var i = 0; i < workerArray.length; i++){
		var br = "";
		if(i%5==0 && i!=0){
			br = "<br>";
		}
		
		var className = "";
		if(classFlag){
			className = "row2"
		}else{
			className = "row1"
		};
		classFlag = !classFlag;
		
		div +=  br + "<div style='color:white;float :left ' class='" + className + " worker' id='" + workerArray[i][0] + "'>" + workerArray[i][1] + "</div>";
	}	
	div += "</div></div>";
	
	$("body").prepend(div);
	 
	$(".worker").click(selectWorker)
	
	$("#search_btn, #search_name").css({
		"font-size" : getElSize(50)	
	});
	
	$("#workerPop").css({
		"position" : "absolute",
		"z-index" : 999,
		"vertical-align" : 'middle',
		"fontSize" : getElSize(80)
	});
	
	$("#workPop_list").css({
		"height" : getElSize(1300),
		"overflow" : "auto"
	});
	
	$("#workerPop .worker").css({
		"padding" : getElSize(30),
		"width" : getElSize(300),
		"height" : getElSize(120),
		"text-align" : "center",
		"cursor" :"pointer",
	});
	
	$("#workerPop").css({
		"top" : (window.innerHeight/2) - ($("#workerPop").height()/2),
		"left" : (window.innerWidth/2) - ($("#workerPop").width()/2)
	});
	
	$(".row1").not(".tr_table_fix_header").css({
		"background-color" : "#222222"
	});

	$(".row2").not(".tr_table_fix_header").css({
		"background-color": "#323232"
	});
};

var selectedDvcId;
var selectedNd;
var selectedRow;
var selectedOprNm;
var selectedPrdNo;

function showPopup(dvcId, nd, prdNo, oprNo, name, el, oprCd){
	selectedPrdNo = prdNo;
	showCorver();
	selectedOprNm = oprCd;
	selectedDvcId = dvcId;
	selectedNd = nd;
	selectedRow = $(el).parent("td").parent("tr");
	var url = "${ctxPath}/chart/showPopup.do";
	var param = "dvcId=" + dvcId +
				"&nd=" + nd + 
				"&date=" + $("#date").val() + 
				"&prdNo=" + prdNo;
	
	$(".contentTr2").remove();
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='contentTr2 parent" + data.lotNo + "' >" + 
						"<td style='text-align:center'><span>" + data.lotNo + "</span><input type='hidden' value='" + data.id + "'> </td>" +
						"<td style='text-align:right'>" + data.lotCnt + "</td>" +
						"<td><Center><input type='text' id='baseCnt' size='4'></center></td>" +
						"<td><Center><input type='text' id='inputCnt' size='4'></center></td>" +
						"<td><Center><input type='text' id='prdCnt' size='4'></center></td>" +
						"<td><Center><input type='text' id='outCnt' size='4'></center></td>" +
						"<td><Center>" + getOprNo(prdNo) + "</center></td>" +
						"<td style='text-align:center;'><button onclick='addNewLot(this);' class='" + data.lotNo + "'>분할</button></td>" +
					"</tr>";
			});
			
			if(nd=="1"){
				nd = "야간"
			}else{
				nd = "주간"
			};
			
			$("#prdNo_pop").html(prdNo);
			$("#date_pop").html($("#date").val());
			$("#date_pop").html($("#date").val());
			$("#oprNm_pop").html(oprNo)
			$("#dvcName_pop").html(decodeURIComponent(name).replace(/\+/gi, " "))
			$("#nd_pop").html(nd);
			
			$("#table2").append(tr);
			$("#popup").css("z-index", 999);
			
			$("#popup table td").css({
				"color" :"white",
				"padding" : getElSize(10),
				"border" : getElSize(3) + "px solid white",
				"font-size" :getElSize(55)
			});
			
			$("#popup input, #popup select, #popup button").css({
				"font-size" :getElSize(55)
			});
		}
	});
	
};

var cntArray = [];
function addNewLot(obj){
	var lotName = $(obj).attr("class"); 
	var parent = $(".parent" + lotName)[0];
	var prdNo = selectedPrdNo;
	var date = "<input type='date'  class='date'><input type='time'  class='time'>";
	var dividLot = "<button onclick='delRow(this);' class='" + lotName + "'>삭제</button>";
	var id = $(parent).children("td:nth(0)").children("input").val(); 
		
	var tr = "<tr class='contentTr2'>" +
				"<Td><span>" + lotName + "</span><input type='hidden' value='" + id + "'></td>" +
				"<Td></td>" + 
				"<Td></td>" + 
				"<Td></td>" + 
				"<Td></td>" + 
				"<td ><center><input type='text' size='4'></center></td>" +
				"<td><center>" + getOprNo(prdNo) + "</center></td>" +
				"<td style='text-align:center;'><button onclick='delRow(this);' class='" + lotName + "'>삭제</button></td>" + 
			"</tr>";
			
	$(parent).after(tr);
	
	$("#popup input, #popup select, #popup button").css({
		"font-size" :getElSize(55)
	});
};


function getOprNo(prdNo){
	var url = "${ctxPath}/chart/getOprNo.do";
	var param = "prdNo=" + prdNo;
	
	var rtn = "<select>";
	$.ajax({
		url : url,
		async : false,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			
			
			$(json).each(function(idx, data){
				rtn += "<option value='" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
			});
			rtn += "</select>";
		}
	});
	return rtn;
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$("#date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing-top" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#group, #date, #workIdx").css({
		"font-size" : getElSize(50)	
	});
	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50),
		"margin-bottom" : 0
	});
	
	$(".label").css("margin-top",0);
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#addBtn").css({
		"margin-top" : getElSize(50)	
	}).click(addRow);
	
	chkBanner();
	
	$("#popup").css({
		"position" : "absolute",
		"z-index" :-999,
		"width" : getElSize(2000),
		"color" : "white",
		"background-color" : "black"
	});
	
	$("#popup").css({
		"top" : getElSize(300),
		"left" : (window.innerWidth/2) - ($("#popup").width()/2),
	});
	
	$("#popup table td").css({
		"color" :"white",
		"padding" : getElSize(10),
		"border" : getElSize(3) + "px solid white",
		"font-size" :getElSize(35)
	});
	
	$("button").css({
		"font-size" : getElSize(40)
	});
};

function addRow(){
	if(classFlag){
		className = "row2"
	}else{
		className = "row1"
	};
	classFlag = !classFlag;
	
	var tr = "<tr class='" + className + "'>" +
				"<td>" + "</td>"+
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+  
				"<td>" + "</td>"+
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
				"<td><input type='text'></td>"+ 
				"<td><input type='text'></td>"+
				"<td><input type='text'></td>" +  
			"</tr>";
	
	$("#wrapper table:last tbody").append(tr);
	
	$(".row1").not(".tr_table_fix_header").css({
		"background-color" : "#222222"
	});

	$(".row2").not(".tr_table_fix_header").css({
		"background-color": "#323232"
	});
	
	$("#table td").css({
		"padding" : getElSize(20),
		"font-size": getElSize(40),
		"border": getElSize(5) + "px solid black"
	});
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

function closePop(){
	$("#popup").css("z-index", -999);
	closeCorver();
};

function delRow(el){
	$(el).parent("td").parent("Tr").remove();
}

var valueArray2 = [];
var valid = true;
function saveRow2(){
	valid = true;
	valueArray2 = [];
	
	$(".contentTr2").each(function(idx, data){
		if(valid){
			var obj = new Object();
			
			obj.id = $(data).children("td:nth(0)").children("input").val();
			obj.lotNo = $(data).children("td:nth(0)").children("span").html();
			obj.prdNo = $("#prdNo_pop").html();
			obj.oprNo = $(data).children("td:nth(6)").children("center").children("select").val();
			obj.dvcId = selectedDvcId;
			obj.nd = selectedNd;
			obj.worker = $(selectedRow).children("td:nth(9)").attr("id")
			obj.fromOprNm = selectedOprNm;	
			if(obj.worker==null){
				alert("작업자가 선택되지 않았습니다.");
				valid = false;
				return;
			}
			var baseCnt = $(data).children("td:nth(2)").children("center").children("input").val();
			var inputCnt = $(data).children("td:nth(3)").children("center").children("input").val();
			var prdCnt = $(data).children("td:nth(4)").children("center").children("input").val();
			var outCnt = $(data).children("td:nth(5)").children("center").children("input").val();
			var date = $("#date_pop").html();
			
			console.log(baseCnt)
			if(baseCnt=="" || typeof(baseCnt)=="undefined") baseCnt = 0;
			if(inputCnt=="" || typeof(inputCnt)=="undefined") inputCnt = 0;
			if(prdCnt=="" || typeof(prdCnt)=="undefined") prdCnt = 0;
			if(outCnt=="" || typeof(outCnt)=="undefined") outCnt = 0;
			
			obj.baseCnt = baseCnt
			obj.inputCnt = inputCnt
			obj.prdCnt = prdCnt
			obj.outCnt = outCnt
			obj.date = date
			
			valueArray2.push(obj);			
		}
	});
	
	console.log(valueArray2)
	var obj = new Object();
	obj.val = valueArray2;
	
	if(valid){
		var url = "${ctxPath}/chart/addPrdData.do";
		var param = "val=" + JSON.stringify(obj)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					alert ("저장 되었습니다.");
					closePop();
				}
				
			}
		})		
	}
};

</script>
</head>

<body >
	<div id="popup">
		<table style="width: 100%" id="table2">
			<Tr>
				<Td style="background-color: #323232">생산 차종</Td><Td id="prdNo_pop" colspan="7">
			</Tr>
			<Tr>
				<Td style="background-color: #323232">생산 일자</Td><Td id="date_pop" colspan="3"></Td><Td style="background-color: #323232">공정</Td><Td id="oprNm_pop" colspan="3"></Td>
			</Tr>
			<Tr>
				<Td style="background-color: #323232">장비</Td><Td id="dvcName_pop" colspan="3"></Td><Td style="background-color: #323232">주야</Td><Td id="nd_pop" colspan="3"></Td>
			</Tr>
			<tr>
				<Td style="background-color: #323232; text-align: center;">로트번호</Td>
				<Td style="background-color: #323232; text-align: center;">로트수량</Td>
				<Td style="background-color: #323232; text-align: center;">기초</Td>
				<Td style="background-color: #323232; text-align: center;">입고</Td>
				<Td style="background-color: #323232; text-align: center;">생산</Td>
				<Td style="background-color: #323232; text-align: center;">출고</Td>
				<Td style="background-color: #323232; text-align: center;">출고공정</Td>
				<Td style="background-color: #323232; text-align: center;">로트 분할</Td>
			</tr>
		</table>
		<center>
			<button onclick="saveRow2()">저장</button>
			<button onclick="closePop();">취소</button>
		</center>
	</div>
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title"> </div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							생산 완료 입력 조회
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%">
					<div class="label" style="float: right">
						<table>
							<tr>
								<Td style="color: white">생산차종&nbsp;<select id="group"> </select></Td>
								<td style="color: white">일시&nbsp;<input type="date" id="date"> </td>
								<td style="color: white"><select id="workIdx"><option value="2">주간</option> <option value="1">야간</option> </select></td>
							</tr>
							<Tr>
								<Td  colspan="3" style="text-align: right;"><button onclick="getPrdData();">조회</button></Td>
							</Tr>
						</table>
						
					</div>
				</div>
				<div id="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table">
						
					</table>
				</div>
		</div>
	</div>
</body>
</html>