<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	background-color: black;
  	font-family:'Helvetica';
}


#wrapper{
	border-color : #1E1E23 !important;
}
.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}
.k-grid-header-wrap{
	border-right : 1px solid #1E1E23 !important;
}

.k-grid thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid tbody > tr
{
 background : #DCDCDC ;
 text-align: center;
}
.k-grid tbody > .k-alt
{
 background : #F0F0F0 ;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
  }
  
.k-window-titlebar{
	display: none !important;
}
</style> 
<script type="text/javascript">

const loadPage = () =>{
	createMenuTree("qm","checkPrdct")
}
	

	var workerCd = "${workerCd}";
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function getGroup(){
		var url = "${ctxPath}/common/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#prdNo").html(option).change(getDvcListByPrdNo)
				 
				$("#chkTy").html("<option value='ALL' >${total}</option>" + getCheckType());
				
				getDvcListByPrdNo();
			}
		});
	};
	
	function chkFaulty(el, ty){
		if(ty=="select"){
			var val = $(el).val();
			
			if(val==2){
				$(el).parent("Td").parent("tr").css("background-color" , "red");
			}else{
				if($(el).parent("Td").parent("tr").hasClass("row1")){
					$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
				}else{
					$(el).parent("Td").parent("tr").css("background-color" , "#323232");
				}
			};	
		}else{
			var min = Number($(el).parent("td").parent("tr").children("td:nth(7)").html());
			var max = Number($(el).parent("td").parent("tr").children("td:nth(8)").html());
			var val = Number($(el).val());
			
			if(val>max || val < min){
				$(el).parent("Td").parent("tr").css("background-color" , "red");
			}else{
				if($(el).parent("Td").parent("tr").hasClass("row1")){
					$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
				}else{
					$(el).parent("Td").parent("tr").css("background-color" , "#323232");
				}
			}
		}
	};
	
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var codeName;
					if(decode(data.codeName)=="입고검사"){
						codeName = "${in_check}";
					}else{
						codeName = decode(data.codeName);
					}
					
					option += "<option value='" + data.id + "'>" + codeName + "</option>"; 
				});
				
				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};
	
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		console.log(url+'?'+param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
				});
				
				if(json.length==0){
					option += "<option value='0'>장비 없음</option>";
				}
				console.log($("#dvcId").html(option).val())
				
				if(json.length!=0)
				$("#dvcId").html(option).val(json[0].dvcId);
				//getCheckList();
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	var result = "<select style='font-size:" + getElSize(40) + "' onchange='chkFaulty(this, \"select\")'><option value='0' >${selection}</option><option value='1'>${ok}</option><option value='2'>${faulty}</option></select>";
	function getCheckList(){
		classFlag = true;
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
		var dvc 
		if($("#dvcId option:selected").html()=="장비 없음"){
		    dvc = undefined
		}else{
			dvc = $("#dvcId option:selected").html()
		}
		
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + $("#chkTy").val() +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val() + 
					"&dvcId=" + dvc;
		console.log(url+"?"+param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var tr = "<tbody>";
						
				alarmList = [];
				$(".contentTr").remove();
				
				var dataSource = new kendo.data.DataSource({});
				console.log(json)
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var attrTy;
						if(data.attrTy==1){
							//attrTy = "정성검사";
							attrTy = "${js_check}";
						}else{
							//attrTy = "정량검사";
							attrTy = "${jr_check}";
						};
					
						var date;
						if(data.date==""){
							date = $(".date").val();
						}else{
							date = data.date;
						};
						
						var resultVal, resultVal2, resultVal3, resultVal4;
						if(data.attrTy==1){
							resultVal = resultVal2 = resultVal3 = resultVal4 = result;
						}else{
							resultVal = "<input type='text' value='" + data.result + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal2 = "<input type='text' value='" + data.result2 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal3 = "<input type='text' value='" + data.result3 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal4 = "<input type='text' value='" + data.result4 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						};
						
						//console.log(data.chkTy)
						var chkTy;
						if(decodeURIComponent(data.chkTy).replace(/\+/gi, " ")=="입고검사"){
							chkTy = "${in_check}"	
						}else{
							chkTy = decodeURIComponent(data.chkTy).replace(/\+/gi, " ");
						}
						
						data.name = decode(data.name);
						data.attrNameKo = decode(data.attrNameKo);
						data.spec = decode(data.spec);
						data.measurer = decode(data.measurer);
						data.chkTy = chkTy;
						data.attrTy = attrTy;
						data.resultVal = resultVal;
						data.resultVal2 = resultVal2;
						data.checkSelect = false;
						data.checkSelect = true;
						if($("#checkCycle").val()==1){
							data.checkCycle = "초물"
						}else if($("#checkCycle").val()==2){
							data.checkCycle = "중물"
						}else{
							data.checkCycle = "종물"
						}
						
						if($("#workTime").val()==1){
							data.workTy="야간";
						}else if($("#workTime").val()==2){
							data.workTy="주간";
						}
						
						tr = "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
									"<td id='t" + data.listId + "'>" + data.prdNo + "<input type='hidden' value='" + data.clmNo + "'></td>" +
									"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + chkTy + "</td>" +
									"<td>" + attrTy + "</td>" + 
									"<td>" + decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + data.target + "</td>" + 
									"<td>" + data.low + "</td>" + 
									"<td>" + data.up + "</td>" +
									"<td>" + decode(data.measurer) + "</td>" +
									"<td id='result" + data.id + "'>" + resultVal + "</td>" +
									"<td id='result_2" + data.id + "'>" + resultVal2 + "</td>" +
									/* "<td id='result3" + data.id + "'>" + resultVal3 + "</td>" +
									"<td id='result4" + data.id + "'>" + resultVal4 + "</td>" + */
									"<td id='fResult" + data.id + "'><select style='font-size : " + getElSize(40) + "'><option value='1'>${ok}</option><option value='2'>${faulty}</option></select></td>" +
									"<td><input type='date' value='" + date + "' style='font-size:" + getElSize(40) + "'></td>" +
									"<td id='chkCycle" + data.id + "'>" + $("#checkCycle option:selected").html() + "</td>" +
									"<td id='workTy" + data.id + "'><select style='font-size : " + getElSize(40) + "'><option value='2'>${day}</option><option value='1'>${night}</option></select></td>" +
							"</tr>";		
							
						$(".alarmTable").append(tr).css({
							"font-size": getElSize(40),
						});		
						
						var checkCycle = data.checkCycle;
						if(checkCycle==0) checkCycle = $("#checkCycle").val();
						
						$("#chkCycle" + data.id + " select option[value=" + checkCycle + "]").attr('selected','selected');
						$("#workTy" + data.id + " select option[value=" + $("#workTime").val() + "]").attr('selected','selected');
						$("#fResult" + data.id + " select option[value=" + data.fResult + "]").attr('selected','selected');
						
						if(data.attrTy==1){
							$("#result" + data.id + " select option[value=" + data.result + "]").attr('selected','selected');
							$("#result_2" + data.id + " select option[value=" + data.result2 + "]").attr('selected','selected');
							/* $("#result3" + data.id + " select option[value=" + data.result3 + "]").attr('selected','selected');
							$("#result4" + data.id + " select option[value=" + data.result4 + "]").attr('selected','selected'); */
							
							if(data.result==2 ||data.result2==2 || data.fResult==2){
								alarmList.push("#tr" + data.id);
							}	
						}else if(Number(data.result) > Number(data.up) || Number(data.result) < Number(data.low)){
							alarmList.push("#tr" + data.id);
						}
						
						data.resultVal=2
						data.resultVal2=2
						data.result=2
					}
					
					dataSource.add(data);
				});
				grid.setDataSource(dataSource);
				
				$(".alarmTable").append("</tbody>");
				//$(".alarmTable").css("width", "100%");
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(35),
					"border": getElSize(5) + "px solid black"
				});
				
				$(".contentTr").click(function(){
					selectedRow.push(this)
				}); 
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$(alarmList).each(function(idx,data){
					$(data).css("background-color" , "red");
				});
			}
		});
	};
	
	function getWorkerList(){
		classFlag = true;
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "";
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				
				$("#checker").html(options).val(workerCd)
			}
		});
	};
	
	
	$(function(){
		getWorkerList();
		getGroup();
		bindEvt2();
		$(".excel").click(csvSend);
		setEl();
		setDate();
		
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#startDate_form").val() + "," + $("#startTime_form").val());
		var endTime = new Date($("#endDate_form").val() + "," + $("#endTime_form").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return timeDiff;
	}
	
	
	function bindEvt2(){

	};
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	var csvOutput;
	function csvSend(){
		var sDate, eDate;
		
		sDate = $("#sDate").val();
		eDate = $("#eDate").val();
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(40)	
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(25),
			"padding" : getElSize(15),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$("#content_table").css({
			"width" : $("#container").width() - getElSize(90),
			"height" : getElSize(256),
			"color" : "white"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("select").css({
			"width" : getElSize(550),
			"height" : getElSize(80),
			"border": "1px black #999",
           "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
		})
		
		$("button").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"font-size" : getElSize(48),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8)
		})
		
		$("input").css({
            "border": "1px black #999",
            "width" : getElSize(500),
            "height" : getElSize(80),
            "font-family": "inherit",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none",
			"border-color" : "#222327"
        })
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#table2").css({
			"width" : getElSize(3800)
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		
		$(".alarmTable td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(35),
			"border": getElSize(5) + "px solid black"
		});
		
		$("#wrapper").css({
			"height" :getElSize(1500),
			"width" : $("#container").width() - getElSize(90),
			"overflow" : "auto"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};

	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
	
	var valueArray = [];
	var selectedRow = []
	function saveRow(){
		valueArray = [];
		
		
		if($("#checker").val()==null){
			kendo.alert("검사자를 선택하여 주세요.");
			return;
		}
		
		gridlist=grid.dataSource.data();
		for(var i=0;i<gridlist.length;i++){
			if(gridlist[i].checkSelect){
				console.log("gridlist[i].result : "+gridlist[i].result)
				console.log("gridlist[i].resultVal : "+gridlist[i].resultVal)
				console.log("gridlist[i].resultVal2 : " + gridlist[i].resultVal2)
				if(gridlist[i].result!=1 && gridlist[i].result!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				if(gridlist[i].resultVal!=1 && gridlist[i].resultVal!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				if(gridlist[i].resultVal2!=1 && gridlist[i].resultVal2!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				gridlist[i].checker=$("#checker").val()
				gridlist[i].chkCycle=$("#checkCycle").val()
				gridlist[i].date=$(".date").val()
				gridlist[i].workTy=$("#workTime").val()
				gridlist[i].dvcId=$("#dvcId").val()
				gridlist[i].chkTy=$("#chkTy").val()
				gridlist[i].fResult=gridlist[i].result
				gridlist[i].result=gridlist[i].resultVal
				gridlist[i].result2=gridlist[i].resultVal2
//				gridlist[i].attrNameKo=gridlist[i].attrNameKo.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
				gridlist[i].attrNameKo=encodeURIComponent(gridlist[i].attrNameKo)
				valueArray.push(gridlist[i]);
			}
		}
		if(valueArray.length==0){
			kendo.alert("저장할 항목을 선택하여 주세요.")
			return;
		}
		
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/chart/addCheckStandardList.do";
		var param = "val=" + JSON.stringify(obj);
		
		console.log(url+"?"+param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success") {
					alert ("${save_ok}");
					getCheckList();
				}
			}
		});
	};

	function addFaulty(el){
		var prdNo = $("#prdNo").val();
		var cnt = 0;
		var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
		
		location.href = url;
	};
	
	
	function categoryDropDownEditor(container, options) {
        $('<input name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
            	valuePrimitive: true,
                dataTextField: "text",
                dataValueField: "value",
//                optionLabel: "==선택==",
                dataSource: [
                	{ text: "양호", value: 2 },
                	{ text: "불량", value: 1 }
                ]
            });
    }

	
	function selectOption(num){
		if(num==1){
			return "불량";
		}else if(num==2){
			return "양호";
		}else{
			return "선택하세요"
		}
			
	}
	
	function checkCycle(){
		if($("#checkCycle").val()==1){
			return "초물"
		}else if($("#checkCycle").val()==2){
			return "중물"
		}else{
			return "종물"
		}
	}
	
	function checkRow(e){
		var gridList=grid.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#wrapper").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 var uid = initData.uid
		 var length = gridList.length
		 for(var i=0; i<length; i++){
			 if(uid==gridList[i].uid){
				 if(gridList[i].checkSelect){
					 gridList[i].checkSelect=false;
					 grid.dataSource.fetch();
				 }else{
					 gridList[i].checkSelect=true;
					 grid.dataSource.fetch();
				 }
			 }
		 }
		 console.log(initData)
	}
	
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=grid.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		grid.dataSource.fetch();
	}
	
	var grid;
	$(function(){
		grid = $("#wrapper").kendoGrid({
			scrollable:true,
			editable: true,
			selectable: "row",
			columns: [
			  {
				  field:"checker",
				  editable: true,
				  title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";' disabled='disabled'>",
				  width:getElSize(100),
				  template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";' disabled='disabled'/>", 
				  attributes: {
          				style: "text-align: center; font-size:" + getElSize(35)
       			  },headerAttributes: {
          				style: "text-align: center; background-color:#353542; font-size:" + getElSize(37)
       			  }						
			  },
			  {
			    field: "prdNo",
			    title: "<spring:message  code='prd_no'></spring:message>" ,
			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "name",
			    title: "<spring:message  code='device'></spring:message>" ,
			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "chkTy",
			    title: "<spring:message  code='check_type'></spring:message>" ,
			    width: getElSize(250),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "attrTy",
			    title: "<spring:message  code='character_type'></spring:message>" ,
			    width: getElSize(250),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "attrNameKo",
			    title: "<spring:message  code='character_name'></spring:message>" ,
			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "spec",
			    title: "<spring:message  code='drawing'></spring:message>Spec" ,
			    width: getElSize(350),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "target",
			    title: "<spring:message  code='target_val'></spring:message>" ,
			    width: getElSize(180),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "low",
			    title: "<spring:message  code='min_val'></spring:message>" ,
			    width: getElSize(180),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "up",
			    title: "<spring:message  code='max_val'></spring:message>" ,
			    width: getElSize(180),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "measurer",
			    title: "<spring:message  code='measurer'></spring:message>" ,
			    width: getElSize(350),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "resultVal",
			    template:"#=selectOption(resultVal)#",
			    title: "<spring:message  code='check_result'></spring:message>" ,
			    width: getElSize(250),
			    editable: false,
			    editor: categoryDropDownEditor,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	"class": "table-cell",
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "resultVal2",
			    template:"#=selectOption(resultVal2)#",
			    title: "<spring:message  code='check_result'></spring:message><br>계측기 후" ,
			    width: getElSize(250),
			    editable: false,
			    editor: categoryDropDownEditor,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	"class": "table-cell",
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "result",
			    template:"#=selectOption(result)#",
			    title: "<spring:message  code='result'></spring:message>" ,
			    width: getElSize(250),
			    editable: false,
			    editor: categoryDropDownEditor,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	"class": "table-cell",
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "date",
			    template: "#=moment().format('YYYY-MM-DD')#",
			    editable: true,
			    title: "<spring:message  code='check_date'></spring:message>" ,
			    width: getElSize(250),
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "checkCycle",
			    title: "<spring:message  code='check_cycle'></spring:message>" ,
			    width: getElSize(150),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  },
			  {
			    field: "workTy",
			    title: "<spring:message  code='division'></spring:message>" ,
			    width: getElSize(150),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(36)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(36)
				}
			  }
			],
			
			dataBound: function(e){
				$('.k-grid thead tr th').css("background","#353542");
			   
				
				$(".k-grid-content table tr").css({        
					"background-color" : "#DCDCDC"
			     });

				$('.k-alt').css("background-color","#F0F0F0");
				$('.k-alt td').css("color","black");
				$(".k-grid-content tbody tr:hover").css({
					"background-color" : "#6699F0"
				})
			}
			
		}).data("kendoGrid")
	})
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<tr>
							<Td><spring:message  code="prd_no"></spring:message></Td>
							<Td><select id="prdNo"></select ></Td><Td><spring:message  code="device"></spring:message></td>
							<td ><select id="dvcId"></select></td> <Td><spring:message  code="check_type"></spring:message></Td>
							<Td><select id="chkTy"></select></Td>
							<Td rowspan="2">
							<button id="search" onclick="getCheckList()" style="cursor: pointer; "><i class="fa fa-search" aria-hidden="true"></i>검색</button>							</Td>
							<Td rowspan="2" style="vertical-align: middle;">
								<button onclick="addFaulty()"><spring:message  code="add_faulty"></spring:message></button>
								<button onclick="saveRow();"><spring:message  code="save"></spring:message></button>
							</Td>
						</tr>
						<tr>
							<Td><spring:message  code="check_date"></spring:message></Td>
							<Td><input type="date" class='date'></Td>
							<%-- <Td><spring:message  code="checker"></spring:message></Td><Td><input type="text" id="checker"></Td> --%>
							<Td><spring:message  code="checker"></spring:message></Td><Td><select id="checker"></select></Td>
							<Td ><spring:message  code="check_cycle"></spring:message></Td>
							<Td>
								<select id="checkCycle"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
								<select id="workTime"><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
							</Td>
						</tr>				
					</table> 
					<div id="wrapper">
						
					</div>
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	