<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
<title>Insert title here</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
	var width;
	var height;
	var machine_width;
	var machine_height;
	$(function(){
		width = window.innerWidth;
		height = window.innerHeight;
		
		setElement();
		
		zoom();
		
		$("#v1").click(function(){
			location.href="${ctxPath}/chart/highChart2.do";
		})
		$("#v2").click(function(){
			location.href="${ctxPath}/chart/highChart1.do";
		})
	});

	var index = 1;
	function zoom(){
		setTimeout(function(){
			$(".video").removeClass('largeView');
			$("#v" + index).addClass("largeView");
			
			$(".machine").css("z-index",2);
			$("#m" + index).css("z-index",3);
			
			$("#v" + index).animate({
				"width" : width,
				"height" : height,
			}, 1000, init("v" +index));
			
			if(index==1){
				$("#m1").animate({
					"top" : height - machine_height,
					"right" : 0
				}, 1000);
			}else if(index==2){
				$("#m2").animate({
					"top" : height - machine_height,
					"left" : 0
				}, 1000);
			}else if(index==3){
				$("#m3").animate({
					"top" :  0,
					"right" : 0
				}, 1000);
			}else if(index==4){
				$("#m4").animate({
					"top" :  0,
					"left" : 0
				}, 1000);
			};
			
			index++;
			if(index==5)index=1;
		}, 2000)
	};
	
	function init(el){
		setTimeout(function(){
			$("#" + el).animate({
				"width" : width * 0.5,
				"height" : height * 0.5,
			}, 1000, zoom());
			
			if(el=="v1"){
				$("#m1").animate({
					top : height/2 - machine_height,
					right : width/2
				}, 1000);
			}else if(el=="v2"){
				$("#m2").animate({
					"top" :  height/2 - machine_height,
					"left" : width/2 
				}, 1000);
			}else if(el=="v3"){
				$("#m3").animate({
					"top" :  height/2,
					"right" : width/2
				}, 1000);
			}else if(el=="v4"){
				$("#m4").animate({
					"top" :  height/2,
					"left" : width/2
				}, 1000);
			};
			
		}, 3000);
	};
	
	function setElement(){
		$(".video").css({
			"width" : width * 0.5,
			"height" : height * 0.5
		});
		
		$("#v2").css({
			"right" : 0
		});
		
		$("#v3").css({
			"left" : 0,
			"bottom" : 0
		});
		
		$("#v4").css({
			"right" : 0,
			"bottom" : 0
		});
		
		$(".machine").css({
			width : width * 0.1,
			height : height * 0.15
		});
		
		machine_width = $(".machine").width();
		machine_height = $(".machine").height();
		
		$("#m1").css({
			top : height/2 - machine_height,
			right : width/2
		});
		
		$("#m2").css({
			top : height/2 - machine_height,
			left : width/2
		});
		
		$("#m3").css({
			top : height/2,
			right : width/2
		});
		
		$("#m4").css({
			top : height/2,
			left : width/2
		});
	};
</script>
<style type="text/css">
	*{
		margin: 0px;
		padding: 0px;
	} 
	.video{
		position: absolute;
	}
	.largeView{
		z-index: 3;
	}
	.machine{
		position: absolute;
		z-index: 1;
	}
</style>
</head>
<body>
	<!-- <img id="v1"  src="http://192.168.0.54/mjpg/video.mjpg" style="float: left;"/>
	<img id="v2"  src="http://192.168.0.59/mjpg/video.mjpg" style="float: right;"/>
	<img id="v3"  src="http://192.168.0.54/mjpg/video.mjpg" style="float: left;"/>
	<img id="v4"  src="http://192.168.0.54/mjpg/video.mjpg" style="float: right;"/> -->
	
	<img id="v1"  src="${ctxPath }/images/DashBoard/1.png" class="video"/>
	<img id="v2"  src="${ctxPath }/images/DashBoard/2.png" class="video" />
	<img id="v3"  src="${ctxPath }/images/DashBoard/3.png" class="video"/>
	<img id="v4"  src="${ctxPath }/images/DashBoard/4.png" class="video"/> 
	
	<img id="m1"  src="${ctxPath }/images/machine/1.png" class="machine"/>
	<img id="m2"  src="${ctxPath }/images/machine/2.png" class="machine"/>
	<img id="m3"  src="${ctxPath }/images/machine/3.png" class="machine"/>
	<img id="m4"  src="${ctxPath }/images/machine/1.png" class="machine"/>
</body>
</html>