<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">


<title>iDOO Control</title>
<style type="text/css">
body{
	background-color : black;
	overflow: hidden;
}

</style>


<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
	var login_lv = window.sessionStorage.getItem("lv");

	
	
	
	function goPage(el){
		var url = $(el).attr("url");
		if(url=="url"){
			alert("준비 중입니다.");
			return;
		}
		
		if($(el).attr("disable")=="true"){
			alert("권한이 없습니다.");
			return;
		};
		
		if(url == "banner"){
			getBanner();
			closePanel();
			$("#bannerDiv").css({
				"z-index" : 9999,
				"opacity" : 1
			});
			panel_flag = false;
			return;
		};
		
		location.href = "${ctxPath}/chart/" + url; 
	};
	 
	function showMenu(){
		var id = this.id;
		
		if(id=="monitor"){
			location.href = "${ctxPath}/chart/main.do?lang=" + localStorage.getItem("lang");
		}else if(id=="analysis"){
			location.href = "/Performance_Report_Chart/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="inven"){
			location.href = "${ctxPath}/chart/incomeStock.do?lang=" + localStorage.getItem("lang");
		}else if(id=="maintenance"){
			location.href = "/Alarm_Manager/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="order"){
			location.href = "${ctxPath}/chart/addTarget.do?lang=" + localStorage.getItem("lang");
		}else if(id=="quality"){
			location.href = "${ctxPath}/chart/checkPrdct.do?lang=" + localStorage.getItem("lang");
		}else if(id=="tool"){
			location.href = "${ctxPath}/chart/toolManager.do?lang=" + localStorage.getItem("lang");
		}else if(id=="kpi"){
			location.href = "${ctxPath}/chart/performanceAgainstGoal_chart_kpi.do?lang=" + localStorage.getItem("lang");
		}else if(id=="program_manager"){
			location.href = "/Program_Manager/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="configuration"){
			location.href = "/Device_Status/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="adv_report"){
			window.open(
					"http://52.185.156.250/Reports/browse/",
				  '_blank' // <- This is what makes it open in a new window.
				);
		}else if(id=="ml"){
			window.open(
					"https://studio.azureml.net/",
				  '_blank' // <- This is what makes it open in a new window.
				);
		}else if("remote"){
			 var ws = new WebSocket("ws://localhost:3000", "echo-protocol");
	
			 // 연결이 수립되면 서버에 메시지를 전송한다
			  ws.onopen = function(event) {
			    ws.send("Client message: Hi!");
			  }
	
			  // 서버로 부터 메시지를 수신한다
			  ws.onmessage = function(event) {
			  }
	
			  // error event handler
			  ws.onerror = function(event) {
			  }
			  
			  
		}
		
		
		
		
	};
	
	
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
	};
	
	var dialog;
	
	function signIn(){
		dialog.open();
		
		$("#id-input, #pw-input, #pw-input-repeat").val("")
		$("#id-alert").html("")
		$("#pw-alert").html("")
	}
	
	function checkId(e){
		if($("#id-input").val().length>=5){
			var idRegex = /^[a-z0-9]+$/;
			var validId = $("#id-input").val().match(idRegex);
			
		    if(validId == null){
		    	$("#id-alert").css({
					"color" : "red"
				})
				passId=false;
		    	$("#id-alert").html("아이디에 공백 또는 특수 문자는 포함 될수 없습니다.")
		        return false;
		    }else{
		    	
		    	var url = ctxPath + "/chart/checkId.do";
		    	var param = "shopId=" + shopId +
		    				"&id=" + $("#id-input").val();
		    	
		    	$.ajax({
		    		url : url,
		    		data : param,
		    		type : "post",
		    		dataType : "text",
		    		success : function(data){
		    			var checkedId = data
		    			if(checkedId=="true"){
		    				passId=false;
		    				$("#id-alert").css({
								"color" : "red"
							})
		    				$("#id-alert").html("이미 존재하는 아이디 입니다.");
		    			}
		    			if(checkedId=="false"){
		    				passId=true;
		    				$("#id-alert").html("사용하실수 있는 아이디 입니다.");
		    				$("#id-alert").css({
		    					"color" : "green"
		    				})
		    			}
		    		}
		    	});
		    	
		    }
		}else{
			$("#id-alert").html("아이디는 5자 이상입니다.")
		}
	}
	
	function checkPw(e){
		if($("#pw-input").val().length>=8 && $("#pw-input-repeat").val().length>=8){
			if($("#pw-input").val()==$("#pw-input-repeat").val()){
				passPw=true;
				$("#pw-alert").css({
					"color" : "green"
				})
				$("#pw-alert").html("패스워드가 같습니다.");
			}else{
				passPw=false;
				$("#pw-alert").css({
					"color" : "red"
				})
				$("#pw-alert").html("서로 다른 패스워드입니다.");
			}
		}else{
			passPw=false;
			$("#pw-alert").css({
				"color" : "red"
			})
			$("#pw-alert").html("패스워드는 8자 이상입니다.");
		}
	}
	
	var passId=false;
	var passPw=false;
	
	function doSignIn(){
		if(passId && passPw){
			var url = ctxPath + "/chart/doSignIn.do";
	    	var param = "shopId=" + shopId +
	    				"&id=" + $("#id-input").val() +
	    				"&password=" + $("#pw-input").val();
			
			$.ajax({
	    		url : url,
	    		data : param,
	    		type : "post",
	    		dataType : "text",
	    		success : function(data){
	    			if(data){
	    				alert("회원 가입이 성공적으로 되었습니다.");
	    				dialog.close();
	    			}else{
	    				alert("회원 가입 실패");
	    			}
	    		}
	    	});
			
		}else{
			if(!passId){
				$("#id-input").focus();	
			}else if(!passPw){
				$("#pw-input").focus();	
			}
		}
	}

	

	
</script>

<script src="${ctxPath }/js/main.js"></script>
	

</head>
<body>	
	<div id="container">
	
	</div>	
</body>
</html>	