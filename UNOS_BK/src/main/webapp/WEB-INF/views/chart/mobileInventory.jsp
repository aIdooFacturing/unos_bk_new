<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<%-- <script type="text/javascript" src="${ctxPath }/js/default.js"></script> --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert title here</title>
</head>
<style>
body{
	height: 100%;
	width: 100%;
	position: absolute;
	margin: 0%;
	padding: 0%;
	background: black;
	color: white;
	
}
#header {
	height: 20%;
	width: 100%;
	float: left;
}

#aside {
	height: 20%;
	width: 100%;
	background: black;
	float: left;
}
#animation {
	height: 100%;
	width: 66.7%;
	float: left;
	display: table;
	text-align: center;
	font-size: 300%;
}
#animation span{
	display: table-cell;
	vertical-align: middle;
	width: 100%;
	height: 100%;
	font-size: 100%;
}
#refresh {
	height: 100%;
	width: 33.3%;
	float: left;
	text-align: center;
	background: darkslateblue;
	font-size: 300%;
	overflow: auto;
}
#refresh span{
	width: 100%;
	height: 100%;
	font-size: 100%;
}
#content {
	height: 60%;
	width: 100%;
	background: blue;
	float: left;
	overflow: auto;
}
.menu {
	height: 100%;
	width: 33.3%;
	float: left;
	display: table;
	text-align: center;
	font-size: 300%;
}
.menu span{
	display: table-cell;
	vertical-align: middle;
	width: 100%;
	height: 100%;
	font-size: 100%;
}
#min {
	background: #6E2FC7;
}
#mout {
	background: linear-gradient( #000000,#2A0066);
}
#mre {
	background: linear-gradient( #000000,#2A0066);
}

#InlistTable{
	border: 1
}


</style>
<script>
	//현재 선택된 탭 변수
	var TabMenu="in"	// in => 입고  	out => 반출 	re => 반입
	
	$(function(){
		mInMove();
		
		setTimeout(function() {
			$("#deliveryNo").focus()
		}, 500)
		
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#deliveryNo").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#deliveryNo").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
	})
	
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};

	function enterEvt(event) {
		if(event.keyCode == 13){
			saveRow()
		}
	}
		
	//선택시 색상 변경
	function colorTag(tag){
		var Tcolor = "linear-gradient( #000000,#2A0066)"	//선택안된것 배경색
		var color = "#6E2FC7"	//선택된것 배경색
		
		if(tag=="in"){
			$(".menu").css("background",Tcolor)
			$("#min").css("background",color)
		}else if(tag=="out"){
			$(".menu").css("background",Tcolor)
			$("#mout").css("background",color)
			
		}else if(tag=="re"){
			$(".menu").css("background",Tcolor)
			$("#mre").css("background",color)
			
		}
/* 		$(".menu").css("background",Tcolor)
		$("#content").css("background",color) */
	}
	//입고 클릭시
	function mInMove(){
		$.showLoading();
//		getIncomStock()	// 입고 리스트 들고 오기
		getInlist();	// 입고 히스토리 가지고 오기

		TabMenu = "in"
		colorTag(TabMenu);

		//함수 다 돌고나서 실행시키기 위해
		setTimeout(function() {
			$.hideLoading(); 
			$("#alarm").css("color","white")
//			$("#alarm").html("<marquee scrollamount='14' loop='2'>입고 페이징 로딩 되었습니다.</marquee>")
		}, 500)

	}
	//불출 클릭시
	function mOutMove(){
		$.showLoading(); 
		
		getOutlist();
		
		TabMenu = "out"
		colorTag(TabMenu);

		setTimeout(function() {
			$.hideLoading(); 
			$("#alarm").css("color","white")
//			$("#alarm").html("<marquee scrollamount='14' loop='2'>불출 페이징 로딩 되었습니다.</marquee>")
		}, 500)
	}
	//반입 클릭시	
	function mReInMove(){
		$.showLoading(); 
		
		getRelist();
		
		TabMenu = "re"
		colorTag(TabMenu);

		setTimeout(function() {
			$.hideLoading(); 
			$("#alarm").css("color","white")
//			$("#alarm").html("<marquee scrollamount='14' loop='2'>반입 페이징 로딩 되었습니다.</marquee>")
		}, 500)

//		location.href="${ctxPath}/chart/mobileReInMove.do"
	}
	
	// 바코드 처리 및 입,반출입 시
	function saveRow(){
		if(TabMenu=="in"){
			//먼저 입고 항목 조회
			getIncomStock();
		}else if(TabMenu=="out"){
			//먼저  불출 항목 조회
			getLotInfo()
//			alert("반출 구현중입니다")
		}else if(TabMenu=="re"){
			alert("반입 구현중입니다")
		}
	}
	
	//불출 항목 조회
	function getLotInfo(){
		
		var url = "${ctxPath}/chart/getStockInfoByTableLine.do";
		var param = "prdNo=" + null + 
					"&lotNo=" + $("#deliveryNo").val();

		$.showLoading()
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;

				//반출 항목이 2개 이상 조회되었을 경우 리턴
				if(json.length>1){
					$.hideLoading()
					$("#alarm").css("color","red")
					$("#alarm").html("<marquee scrollamount='14' loop='2' >관리자에게 문의해주세요_[중복]</marquee>")
					$("#deliveryNo").select();
					return;
				}
				// 0개 조회되었을 경우
				if(json.length==0){
					$.hideLoading()
					$("#alarm").css("color","red")
					$("#alarm").html("<marquee scrollamount='14' loop='2' >불출할 항목이 없습니다.</marquee>")
					$("#deliveryNo").select();
					return;
				}
				
				saveOutRow(json);
				console.log(json);
				$.hideLoading()
			} ,error:function(request,status,error){
				$.hideLoading(); 
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        
			}
		})
	}
	
	//불출 처리
	function saveOutRow(savelist){
		
		$(savelist).each(function(idx,data){
			data.cnt = data.lotStock ;
			data.idx = data.id;
			data.dvcName = data.nowDvcName;
			data.afterProj = "0005";
			data.datetime =  moment().format("YYYY-MM-DD HH:mm:ss");
		})
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		console.log(param)
		
		$.showLoading()
		
		var url = "${ctxPath}/chart/moveStockWithTrans.do"
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#alarm").css("color","white")
					$("#alarm").html("<marquee scrollamount='14' loop='2'>" + $("#deliveryNo").val() + " 항목 " + savelist[0].cnt + " 개 불출되었습니다" +"</marquee>")
//					mInMove();
					$("#deliveryNo").select();

				}else{
					$("#alarm").css("color","red")
					$("#alarm").html("<marquee scrollamount='14' loop='2'> 관리자에게 문의해주세요_002 </marquee>")
//					mInMove();
					$("#deliveryNo").select();
				}
					

//				getLotInfo();
			}, error : function(data){
				alert("불출_error_001")
			}
		}); 
		
	}
	//바코드 찍은 항목관련해서 데이터 찾기
	function getIncomStock(){
		$.showLoading(); 
		classFlag = true;
		var tablelist=[];
		var url = "${ctxPath}/chart/getDeliverInfo.do";
		
		
		
		var param = "deliveryNo=" + $("#deliveryNo").val()+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
/* 					"&sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() ;  */
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("완성")
				var json = data.dataList;
				console.log(json)
				$(json).each(function(idx, data){
					data.vndNo=data.id
					data.deliveryNo=data.totaldelivery
					data.lotCnt=data.cnt
					data.smplCnt=0
					data.notiCnt=0
					data.rcvCnt=data.cnt

				});				
				
				// 입고 시키기
				saveInRow(json)
		       					
				
				$.hideLoading(); 
			},error:function(request,status,error){
				$.hideLoading(); 
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
		        
			}

		});
	};
	
	//입고처리 
	function saveInRow(json){
//		alert("구현중입니다")
		
		$.showLoading(); 

		var saveData=[];
		console.log(json)

		//같은 납품서 번호 찾기
		$(json).each(function(idx,data){
			if($("#deliveryNo").val()==data.totaldelivery){
				saveData.push(data)
			}
		})

		//같은 납품서번호 없으면 리턴
		if(saveData.length==0){
			$.hideLoading(); 
			$("#alarm").css("color","red")
			$("#alarm").html("<marquee scrollamount='14' loop='2' >입고할 항목이 없습니다.</marquee>")
			$("#deliveryNo").select();
			return
		}/* else{

			$.hideLoading(); 
			$("#alarm").css("color","white")
			
			console.log($("#deliveryNo").val() + "항목 " + saveData.length + "개 입고되었습니다")
			return
		} */
		
//		console.log(saveData)
		var obj = new Object();
		obj.val = saveData;
		
		var url = "${ctxPath}/chart/addStock.do";
		var param = "val=" + JSON.stringify(obj);
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				$.hideLoading();
				if(data=="success") {
					$("#alarm").css("color","white")
					$("#alarm").html("<marquee scrollamount='14' loop='2'>" + $("#deliveryNo").val() + " 항목 " + saveData.length + " 개 입고되었습니다" +"</marquee>")
					mInMove();
					$("#deliveryNo").select();

				}else{
					$("#alarm").css("color","red")
					$("#alarm").html("<marquee scrollamount='14' loop='2' >관리자 문의해 주세요.</marquee>")

				}
			},error : function(data){
				$.hideLoading(); 
				alert("error_발생_01")
			}
		})
		
	}
	
	function getInlist(){
		var url = "${ctxPath}/chart/getInlist.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var table = "<table id='InlistTable' border=1 style='width:100%; height:100%;font-size:100%; overflow:hidden; text-overflow:ellipsis;white-space: nowrap; text-align: center; '>"
					table += "<tr><th colspan='4'>입고 최근 리스트 <input type='text' id='deliveryNo' onkeyup='enterEvt(event)'><button onclick='saveRow()'>입고</button> </th></tr>"
					table += "<tr><th>품번</th><th>LotNo</th><th>cnt</th><th>등록시간</th></tr>"
					
				$(json).each(function(idx,data){
//					table += "<tr><td>" + decode(data.vndNm) + "</td>";
					table += "<tr><td>" + data.prdNo + "</td>";
//					table += "<td>" + data.deliveryNo + "</td>";
					table += "<td>" + data.lotNo + "</td>";
					table += "<td>" + data.cnt + "</td>";
					table += "<td>" + data.date + "</td></tr>";
				})
				table += "</table>";
				
				$("#content").empty()
				$("#content").append(table)
			}
		})
	}
	
	function getOutlist(){
		var url = "${ctxPath}/chart/getOutlist.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var table = "<table id='InlistTable' border=1 style='width:100%; height:100%;font-size:100%; overflow:hidden; text-overflow:ellipsis;white-space: nowrap; text-align: center; '>"
//					table += "<tr><th colspan='4'>불출 최근 리스트</th></tr>"
					table += "<tr><th colspan='4'>불출 최근 리스트 <input type='text' id='deliveryNo' onkeyup='enterEvt(event)'><button onclick='saveRow()'>반출</button> </th></tr>"

					table += "<tr><th>품번</th><th>LotNo</th><th>cnt</th><th>등록시간</th></tr>"
				$(json).each(function(idx,data){
//					table += "<tr><td>" + decode(data.vndNm) + "</td>";
					table += "<tr><td>" + data.prdNo + "</td>";
//					table += "<td>" + data.deliveryNo + "</td>";
					table += "<td>" + data.lotNo + "</td>";
					table += "<td>" + data.cnt + "</td>";
					table += "<td>" + data.date + "</td></tr>";
				})
				table += "</table>";
				
				$("#content").empty()
				$("#content").append(table)
			}
		})
	}
	
	function getRelist(){
		var url = "${ctxPath}/chart/getRelist.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var table = "<table id='InlistTable' border=1 style='width:100%; height:100%;font-size:100%; overflow:hidden; text-overflow:ellipsis;white-space: nowrap; text-align: center;'>"
//					table += "<tr><th colspan='4'>반입 최근 리스트</th></tr>"
					table += "<tr><th colspan='4'>반입 최근 리스트 <input type='text' id='deliveryNo' onkeyup='enterEvt(event)'><button onclick='saveRow()'>반입</button> </th></tr>"

					table += "<tr><th>품번</th><th>LotNo</th><th>cnt</th><th>등록시간</th></tr>"
				$(json).each(function(idx,data){
//					table += "<tr><td>" + decode(data.vndNm) + "</td>";
					table += "<tr><td>" + data.prdNo + "</td>";
//					table += "<td>" + data.deliveryNo + "</td>";
					table += "<td>" + data.lotNo + "</td>";
					table += "<td>" + data.cnt + "</td>";
					table += "<td>" + data.date + "</td></tr>";
				})
				table += "</table>";
				
				$("#content").empty()
				$("#content").append(table)
			}
		})
	}
</script>
<body>
	<input type="text" id="deliveryNo1" style="display: none;">
	<div id="header">
		<div id='min' class='menu' onclick="mInMove()"><span>입고</span></div>
		<div id='mout' class='menu' onclick="mOutMove()"><span>불출</span></div>
		<div id='mre' class='menu' onclick="mReInMove()"><span>반입</span></div>
	</div>
	<div id="aside">
		<div id='animation'>
			<span id='alarm'>
			</span>
		</div>
		<div id="refresh">
			<span>
				<a href="${ctxPath}/chart/mobileInventory.do">
					<img src="${ctxPath}/images/icons/refresh.png" width="100%" height="100%">
				</a>
			</span>
		</div>
		
	</div>
	<div id="content">
		<table>
			<tr>
				<td height="20%">asd</td>
			</tr>
		</table>
	</div>
</body>
</html>