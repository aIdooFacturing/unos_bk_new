<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}
/* .k-grid-header th{
	background : linear-gradient( to bottom, gray, black );
	color : white;
} */

.k-grid-toolbar{
	background : #222327;
	color : white;
}

#grid .k-header k-grid-toolbar{
	background: rgb(38, 40, 44);
}

#reCount1{
	background : red;
	margin-left: 2.98%;
	position: absolute;
}

#grid{
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

.k-grid-header-wrap {
	border-color : #222327;
}


</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("kpi", "reOpCycle")	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		//date.setDate(date.getDate()-2)
		date.setDate(date.getDate())
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
//	var handle = 0;
	
	$(function(){
		$("#chkSpdLoad").val(2);
		setDate();
		getDvcId();
//		createNav("kpi_nav", 3);
		
//		setEl();
		setEl2();
		time();
		
		daily(getToday().substring(0,10));
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
	/* function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(400) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(160),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		$("#reCount1").css({
			"background" : "red",
			"margin-left" : "82.3%",
			"color" : "white",
//			"margin-top" : "-3%",
			"font-size" : getElSize(40)
		});
	}; */
	
	
	function setEl2(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		
		 $("#container").css({
				"width" : contentWidth - getElSize(30),
				"background-color" : "#26282c"
			})
			
			$("#content_table").css({
				"margin-top" : getElSize(300),
				"background-color" : "#26282c"
			})
			
			
/* 		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
			
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		}) */
		
		$("#content").css({
			"height" : getElSize(1650),
			"padding-left" : getElSize(50),
			"padding-right" : getElSize(50),
			"padding-top" : getElSize(20),
			"background-color" : "#26282c"
		})
		
		$("#grid").css({
			"margin-right" : getElSize(50),
			"background-color" : "#26282c",
			"margin-bottom" : getElSize(1)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
	
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
/* 		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		}); */
		
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(380) + marginHeight,
			"margin-bottom" : getElSize(30),
			"background-color" : "#26282c",
			"text-align" : "center"
		});
		
		$("#name").css({
			"padding-left" : getElSize(50)
		})
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#svg_td").css({
			"margin-left" : getElSize(30)
		})
	
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("select").css({
			"width" : getElSize(200),
			"margin-right" : getElSize(20)
		});		
		
		$("#dvcId").css({
			"width" : getElSize(600)
		})
			
		$("#td_first").css({
			"padding-left" : getElSize(50),
			"padding-top" : getElSize(20),
			"padding-bottom" : getElSize(10),
			"margin-top" : getElSize(50)
		});
		
		$(".k-header").css({
		    "font-family" : "NotoSansCJKkrBold",
		    "font-size" : getElSize(32) + "px",
		    "background-image" : "none",
		    "background-color" : "#353542"
		}); 
		
		$("button").css({
			"padding" : getElSize(25),
			"margin-left" : getElSize(7),
			"margin-right" : getElSize(10),
			"margin-top" : getElSize(7),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		})
		
		$("#sDate").css({
			"font-size" : getElSize(40),
			"width" : getElSize(300),
			"text-align" : "center",
			"margin-right" : getElSize(20),
			"border-color" : "#222327"
		});
		
		$("#sMonth").css({
			"font-size" : getElSize(40),
			"width" : getElSize(300),
			"text-align" : "center",
			"margin-right" : getElSize(20),
			"border-color" : "#222327"
		});
		
		$("#sDate2").css({
			"font-size" : getElSize(40),
			"width" : getElSize(300),
			"text-align" : "center",
			"margin-right" : getElSize(20),
			"border-color" : "#222327"
		});
		
		$("#eDate").css({
			"font-size" : getElSize(40),
			"width" : getElSize(300),
			"text-align" : "center",
			"margin-left" : getElSize(5),
			"border-color" : "#222327"
		});

		$("#grid .k-header k-grid-toolbar").css({
			"magrin-left" : getElSize(50)
		})
		
		
		$("#search").css({
			"cursor" : "pointer",
			"margin-right" : getElSize(30),
// 			"text-align" : "center",
			"vertical-align" : "middle",
 			"height" : getElSize(90),
 			"font-size" : getElSize(45),
 			"border-radius" : getElSize(8),
 			"padding-bottom" : getElSize(10),
 			"padding-top" : getElSize(10)
		});
		
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		$("#reCount1").css({
			"background" : "red",
//			"margin-left" : "82.3%",
			"color" : "black",
//			"margin-top" : "-3%",
			"text-align" : "center",
			"vertical-align" : "middle",
			"border-radius" : getElSize(8),
			"height" : getElSize(90),
 			"font-size" : getElSize(45),
 			"padding-bottom" : getElSize(10),
 			"padding-top" : getElSize(10)
		});
	};
	
	
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
	function daily(today){
		 
	 	if(today!=undefined || today!=null){
		 	$("input#sDate").val(today);
		 	$("input#eDate").val(today);
		 	$("#rangeRadio #sDate2").val(today)
	 	}
	 	$("input#eDate").val($("input#sDate").val())
	 	$("#dailyRadio #sDate").css({
	 		"background" : "black",
	 		"color" : "white",
	 		"pointer-events": "auto"
	 	})
		$("#monthlyRadio #sMonth ,#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"background" : "gray",
			"color" : "black",
			"pointer-events": "none"
		})
		
		$("#dailyRadio input#sDate").datepicker({
//			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(e){
				$("#rangeRadio #sDate2").val(e)
				$("input#sDate").val(e)
				$("input#eDate").val(e)
			}
		})
		
		$('.k-header').css("background-color","black");
		$('.k-button').css("background-color","#5D5D5D");
	}
	
	function monthly(){
		$('input#sMonth').datepicker( {
//			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            $('input#sMonth').val(year+'년'+month.toString()+'월');
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#sDate2').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    })
	    
		$("#monthlyRadio #sMonth ").css({
			"pointer-events": "auto",
			"background" : "black",
	 		"color" : "white"
	 	});
		$("#dailyRadio #sDate ,#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"pointer-events": "none",
			"color" : "black",
			"background" : "gray"
		});
	}
	function range(){
		$("#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"pointer-events": "auto",
			"background" : "black",
	 		"color" : "white"
	 	})
		$("#dailyRadio #sDate ,#monthlyRadio #sMonth ").css({
			"pointer-events": "none",
			"color" : "black",
			"background" : "gray"
		})
		
		$("#rangeRadio #sDate2").datepicker({
	        "maxDate": 0,
			onSelect:function(e){
				$("input#sDate").val(e);
			}
		})
		
		$("#rangeRadio #eDate").datepicker({
	        "maxDate": 0,
			onSelect:function(e){
				$("#rangeRadio input#eDate").val(e)
			}
		})

	}
	
	var dvcList=[];
	function getDvcId(){
		var url = "${ctxPath}/chart/getDvcId.do";
		var param = "shopId=" + shopId + 
					"&line=ALL";
		
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					var arr={};
					arr.dvcId=data.dvcId;
					arr.name=decode(data.name);
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
					dvcList.push(arr);
				});
				
				$("#dvcId").html(option);
				
				getReOpList();
			}
		});
	};
	

	function dvcDecode(name){
		return decode(name);
	}
	function spderrg(name){
		if(name=="2"){
			return "이상"
		}else{
			return "정상"
		}
	}
	var kendotable;
	$(document).ready(function(){
		
	})
	
	var className = "";
	var classFlag = true;

	function getReOpList(){
		$("#searchBtn").attr("disabled","true").css("color","darkgray")
		classFlag = true;
		
		var url = "${ctxPath}/chart/getReOpList.do";
		
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		var param = "date=" + sDate +
					"&dvcId=" + $("#dvcId").val() + 
					"&chkSpdLoad=" + $("#chkSpdLoad").val() +
					"&eDate=" + eDate;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			error : function(e1){
				alert("error")
			},
			success : function(data){
				$("#searchBtn").removeAttr('disabled').css("color","black")
				var json = data.dataList;
				
				$("#grid").empty();
				kendotable=$("#grid").kendoGrid({
					dataSource:{
						dataType : "json",
						data : json,
						serverPaging: true,
		                serverSorting: true,
		                pageSize: 50,
					height : getElSize(1000)
					},
					toolbar:[{
						name:"excel",
						style: "text-align: center; background-color:#222327; font-size:" + getElSize(35) +"; color : black;"
					}],
					dataBound:function(e){

						$('.k-grid tbody tr').each(function(){
					    	 if($(this).text().indexOf("정상")==-1){
					 	    	
					    	 }else{
					    		 $(this).addClass('normal')
					    	 }
				    	});
//						$(".k-grid-content.k-auto-scrollable").css("height",getElSize(584.5));
						$(".k-grid th").css("font-size",getElSize(39.5));
						$(".k-grid td").css("font-size",getElSize(39.5));
						$(".k-grid tbody tr td").css("background","#ED0000");
						$(".k-grid tbody tr.k-alt td").css("background","red");
						$(".k-grid tbody tr.normal td").css("background","#c1c0c2");
						$(".k-grid tbody tr.k-alt.normal td").css("background","#d5d5d6");
						
						$('.k-header').css("background-color","#353542");
						$('.k-header').css("border-color","#222327");
						
						$('.k-button').css("background-color","#858585");
						$('.k-button').css("color","black");
						$('.k-button').css("border-color","#858585");
					},
		 			scrollable:{
						virtual:true
					},  
					columns:[{
						field:"dvcName" , title:"${device}" , template: "#=dvcDecode(dvcName)#" , width : getElSize(310)
					},{
						field:"date" , title:"${date_}" , width : getElSize(260)
					},{
						field:"prgName" , title:"${op_program}" , width : getElSize(210)
					},{
						field:"chkSpdLoad" , title:"${spd_err_g}" , template: "#=spderrg(chkSpdLoad)#" , width : getElSize(250)
					}/* ,{
						field:"total" , title:"part<br>count" , width : getElSize(210)
					} */,{
						field:"cylTm" , title:"${cycle_time}" , width : getElSize(250)
					},{
						field:"cylAvgTm" , title:"${cycle_time_avrg}" , width : getElSize(250)
					},{
						field:"cylSdTm" , title:"${cycle_time_sd}" , width : getElSize(280)
					},{
						field:"totalSpdLoad" , title:"${spd_load_sum}" , width : getElSize(250)
					},{
						field:"totalAvgSpdLoad" , title:"${spd_load_sum_avrg}" , width : getElSize(250)
					},{
						field:"totalSdSpdLoad" , title:"${spd_load_sum_avrg_sd}" , width : getElSize(250)
					},{
						field:"smplCnt" , title:"${sample_cnt}" , width : getElSize(230)
					},{
						field:"sDate" , title:"${cycle_start}" , width : getElSize(450)
					}]
				}).data("kendoGrid");
				
				console.log("----Start----")
				console.log(json)
				var dupChk=[];
				var count=0;
				var list=[];
				var name;

				//pie 그림 그리기
				
				//장비별
				for(i=0,length=json.length; i<length; i++){
					json[i].dvcName=decode(json[i].dvcName)
					if(dupChk.indexOf(json[i].dvcName)==-1 ){
						for(j=0,length=json.length; j<length; j++){
							json[j].dvcName=decode(json[j].dvcName)
							
							if(json[i].dvcName==json[j].dvcName && json[i].chkSpdLoad=="2" && json[j].chkSpdLoad=="2"){
								count++;
								name=json[i].dvcName;
							}							
							if(j==length-1 && count!=0 ){
								var arr={};
								arr.value=count;
								arr.category=name;
								list.push(arr);
								dupChk.push(name);
								count=0;
								
							}
						}					
					
					}

				}
				byDvcPie(list);
				
				var dupChk=[];
				var count=0;
				var list=[];
				var name;

				//pie 그림 그리기
				
				//프로그램별
				for(i=0,length=json.length; i<length; i++){
					if(dupChk.indexOf(json[i].prgName)==-1){
						for(j=0,length=json.length; j<length; j++){	
	
							if(json[i].prgName==json[j].prgName && json[i].chkSpdLoad=="2" && json[j].chkSpdLoad=="2"){
								count++;
								name=json[i].prgName;
							}							
							if(j==length-1 && count!=0 && json[i].chkSpdLoad=="2"){
								var arr={};
								arr.value=count;
								arr.category=name;
								list.push(arr);
								dupChk.push(name);
								count=0;
								
							}
						}					
					
					}

				}
				byAlarmPie(list);
				
				//시간분포도
				for(i=0,length=json.length; i<length; i++){
					if(json[i].chkSpdLoad=="2"){
						var arr={}
						arr.hours=Number(json[i].sDate.substr(11,2))
						arr.min=Number(json[i].sDate.substr(14,2))
						arr.category=decode(json[i].dvcName)
						arr.size=1;
						list.push(arr);
					}
				}
				
				var arr={};
				arr.hours=-5
				arr.min=30
				arr.category="무쓸모"
				arr.size=15;
				list.push(arr);
				
				byWorkerPie(list);
				
				$("button, input[type='time']").css("font-size", getElSize(40));
				$.hideLoading();
			}
		});
	};
	
	function byDvcPie(list) {
		var totalCnt=0;
		/* for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		} */
		list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		var copy=[]
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			copy.push(list[i]);
		}
		list=copy;
        $("#byDvcPie").kendoChart({
        	chartArea: {
				background:"#121212",
			},
            title: {
            	font:getElSize(80) + "px sans-serif",	
            	position: "bottom",
                text: "TOP 5 장비별",
                color : "white"
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category # \n #= value# EA"
                }
            },
            dataSource: {
                data: list	
            },
            seriesColors:["red","#0080FF","#1D8B15","#990085","#4D00ED","#DB005B","#FFBB00"],
            series: [{
                type: "pie",
                startAngle: 100,
                field: "value",
                padding:0,
                labels: {
                    visible: true,
                    position: "center"
                }
            }],
            tooltip: {
                visible: true,
                template: "#= category #: #= value # EA"
//                format: "{0}EA"
            }
        });
    }
	
	function byAlarmPie(list) {
		list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		
		var xData=[];
		var yData=[];
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			xData.push(list[i].value)
			yData.push(list[i].category);
			
			
		}
        $("#byAlarmPie").kendoChart({
            title: {
                text: "Top 5 프로그램별",
              	position : "bottom",
              	color : "white",
                font:getElSize(80) + "px sans-serif",
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                type: "bar"
            },
            dataSource: {
                data: list
            },
            series: [{
                name: "Total Visits",
                data: xData,
                color : "red"
            }],
          chartArea: {
            background:"#121212"
          },
          valueAxis: {
//        	  	majorUnit: 1,
                line: {
                    visible: true,
                    color : "white"
                },
                majorGridLines: {
                    color : "#353535"
                },
                labels: {
                    rotation: "auto",
                    step : 2,
                    color:"white",
                    font:getElSize(50) + "px sans-serif",
                }
            },
            
            categoryAxis: {
                categories: yData,
                line:{
                	viible:true,
                	color:"white"
                },
                majorGridLines: {
                    color:"#121212"
                },
                color:"white",
                labels:{
                    font:getElSize(48) + "px sans-serif",
                }
            },
           
            tooltip: {
                visible: true,
                template: "#= category #: #= value # EA"
            }
        });
    }
	
	function byWorkerPie(list) {
      
      	console.log("chart")
      	console.log(list)
        $("#byWorkerPie").kendoChart({
            title: {
                text: "재가공의심 분포도(시간/분)",
                position: "bottom",
                color:"white",
                font:getElSize(80) + "px sans-serif"
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                type: "bubble"
            },
            chartArea: {
				background:"#121212",
			},
          	dataSource: list,
          	 series: [{
               	type:"bubble",
                xField: "hours",
                yField: "min",
                sizeField: "size",
                categoryField: "category",
                color:"#FF0000"
            }],
          
            xAxis: {
            	majorGridLines: {	//차트안 x축 실선
                    visible: true,
                	color:"#353535"
				},
                labels: {
                    format: "{0:N0}h",
                    skip: 1,
                    rotation: "auto",
                    visible:true,
                    visual: function(e){
                      
                      if(e.text>24){
                      	e.text=e.text-24
                      }
                     
                      return new kendo.drawing.Text(e.text, e.rect.origin, {
                        
                        fill: {
                          color: "white"
                        }
                      });
                    }

                },
              
                axisCrossingValue: 0,
                majorUnit: 1,
              	max:24,//33,
              	min:00,//08,

                plotBands: [{
                    from: 8.5,
                    to: 20.5,
                    color: "#00f",
                    opacity: 0.2
                }]
            },
            yAxis: {
            	majorGridLines: {	//차트안 x축 실선
                    visible: true,
                	color:"#353535"
				},
                labels: {
                    format: "{0:N0}m",
                    color:"white"
                },
                /* line: {
                    width: 0
                } */
            },
            tooltip: {
                visible: true,
                format: "{3}:  {2} EA  {0}시{1}분",
                opacity: 1
            },
            valueAxis: {
//        	  	majorUnit: 1,
                line: {
                    visible: true,
                    color : "white"
                },
               
                labels: {
                    rotation: "auto",
                    step : 2,
                    color:"white",
                    font:getElSize(50) + "px sans-serif",
                }
            }
        });
    }
	function reCount(){
		var url = "${ctxPath}/chart/getReCount.do";
		var param = "dvcId=" + $("#dvcId").val();
		var str="";
//		console.log($("#dvcId").val())
		$.showLoading();
		if($("#dvcId").val()=="ALL"){
			str="모든 "
		}else{
			for(i=0; i<dvcList.length; i++){
				if($("#dvcId").val()==dvcList[i].dvcId){
					str=dvcList[i].name;
				}
			}
		}
		
		var con=confirm(str + " 장비 사이클을 재계산 하시겠습니까? \n시간이 소요 될수 있습니다.")
		if(con==true){
			$.ajax({
				url : url,
				data : param,
				type : "post",
				success : function(data){
					console.log("suc");
					getReOpList();
					$.hideLoading();
				}
			});
		}else{
			$.hideLoading();
		}
	}

	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<%-- <Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center> --%>
	</div>
	
	<div id="container" style="background:#26282c;">
		<table id="table" style="border-collapse: collapse; background:#26282c;">
			
			<Tr>
				
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<table style="width: 100%; float: right;">
						<Tr>
							<Td id="name" style="color: white; text-align: left;">
								<spring:message code="device"></spring:message>
								<select id="dvcId"> </select>
<%-- 								<spring:message code="date_"></spring:message>
								<input type="date" class="date" id="sDate">
 --%>								
								
								
								<div style="display: inline;" id=dailyRadio>
									<label for="dailyRadio"><spring:message code="By_date"></spring:message></label>
									<input type="radio" id="dailyRadio" name="datePicker" value="0" onclick="daily()" checked="checked">
									<input type="text" id="sDate" readonly size=7px>
								</div>
								
								<div style="display: inline;" id=monthlyRadio>
									<label for="monthlyRadio"><spring:message code="monthly"></spring:message></label>
									<input type="radio" id="monthlyRadio" name="datePicker" value="1" onclick="monthly()">
									<input type="text" id="sMonth" readonly size="7px">
								</div>
								
								<div style="display: inline;" id="rangeRadio">
									<label for="rangeRadio"><spring:message code="By_period"></spring:message></label>
									<input type="radio" id="rangeRadio" name="datePicker" value="2" onclick="range()">
									<input type="text" id="sDate2" readonly size="10px">~
									<input type="text" id="eDate" readonly size="10px">
								</div>
								
								
								<spring:message code="status"></spring:message>								
								<select id="chkSpdLoad">
									<option value='ALL'><spring:message code="total"></spring:message></option>
									<option value='2'><spring:message code="unnormal"></spring:message></option>
								</select>
								<button id="search" onclick="getReOpList()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"> 검색</i></button>
							
								<button id="reCount1" onclick="reCount()" style="position: absolute;"> 재계산 </button>
							</Td>
						</Tr>
						
						
						<tr>
							<td>
								<div id="content" style="position: absolute; background: #26282c; ">
									<div style="height: 50%;width:100%; float: left; position: relative;">
									<div id="byDvcPie" style="height: 100%; width: 28.33%; background: #26282c; float: left;" >
										<!-- 장비별 -->
									</div>
									
									<div id="byAlarmPie" style="height: 100%; width: 28.33%; background: #26282c; float: left;" >
										<!-- 프로그램별 -->
									</div>
									
									<div id="byWorkerPie" style="height: 100%; width: 43.33%; background: #26282c; float: left;" >
										<!-- 작업자별 -->
									</div>
									
									
									</div>
									<div id="grid" style="height: 48%;width:100%; background-color:#26282c; float: left; position:  relative;"></div>
								</div>
 							</td>
						</tr>
					</table>
				</td>
				
			</Tr>
			
			
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	