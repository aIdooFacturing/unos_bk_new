<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
<title>Insert title here</title>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.load("visualization", "1", {packages:["gauge"]});
	//google.load("visualization", "1", {packages:["timeline"]});
	var spindle;
	var actualFeed;
	var targetFeed;
	var alarmCnt;
	var alarmNum;
	var line_chart;
	var guage_chart;
	var bar_chart;
	var time_chart;
	var line_options;
	var guage_options;
	var bar_options;
	var time_options;
	var line_data;
	var guage_data;
	var bar_data;
	var time_data;
	var myInterval;
	var intervalValue = 1000;
	var ip;
	var port;
	var status
	var pStatus;
	var pHour;
	var pMinute;
	var pSecond;
	var maxRPM = 0;
	<%
		String ip = (String) session.getAttribute("ip");
		String port = (String) session.getAttribute("port");
	%>
	ip = "<%=ip%>";
	port = "<%=port%>";
	$(function(){
		var date = new Date();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var second = date.getSeconds();
		
		$("#startTime").html(hour + " : " + minute + " : " + second);
		$("#interval").val(intervalValue/1000);
		guage_chart = new google.visualization.Gauge(document.getElementById('guage_chart'));
		//line_chart = new google.visualization.LineChart(document.getElementById('line_chart'));
		//bar_chart = new google.visualization.BarChart(document.getElementById('bar_chart'));
		time_chart = new google.visualization.Timeline(document.getElementById('time_chart'));
		
		line_data = new google.visualization.DataTable();
		line_data.addColumn('string', 'year');
	   line_data.addColumn('number', 'rpm');
	   line_data.addColumn({type:'string', role:'annotation'}); // annotation role col.
	   
	   time_data = new google.visualization.DataTable();
	   time_data.addColumn({ type: 'string', id: 'status' });
	   time_data.addColumn({ type: 'date', id: 'Start' });
	   time_data.addColumn({ type: 'date', id: 'End' });
	   
	 /*   time_data.addRows([
		   [ "RESTART", new Date(0, 0, 0, hour, minute,second), new Date(0, 0, 0, hour, minute,second) ],
		   [ "STOP", new Date(0, 0, 0, hour, minute,second), new Date(0, 0, 0, hour, minute,second) ]
	   ]);
	    */
	   guage_data = google.visualization.arrayToDataTable([
		                                                   		['Label', 'Value'],
		                                                   		['rpm', 0],
		                                                 			  ]);
	   
	  
	   
	   bar_options = {
	        	animation:{
	        	  	duration:500
		        },
	        	hAxis:{
	        		maxValue : 400,
	        		minValue : 300
	        	},
        		chartArea:{
	   				width : "95%",
	   				height : "80%"
	   			},
	   			legend: { 
	   				position: 'none' 
	   			},
	   			annotations:{
	   				textStyle:{
	   					fontSize: 15,
	   				},
	   				highContrast:false
	   			}
	        };
	   
	   time_options = {
	        	animation:{
	        	  	duration:500
		        },
		      timeline:{
		    	  rowLabelStyle:{
		    		  fontSize: "10"
		    	  }
		      }
	        };
	   
	   getDataLoop();
	});
	
	function getDataLoop(){
		/* myInterval = setInterval(function(){
			   loop();
				guage_loop();
				bar_loop();
		   },intervalValue); */
		
		myInterval = setInterval(getData,intervalValue);
	};
	
	var index=0;
	function loop(){
		
		line_options = {
				hAxis: {
					viewWindow: {
						min:0,
						max:7
					},
					textPosition:"none"
	   			},
		   		animation:{
		   			duration : 500,
		   			easing : "in"
		   			},
		   		chartArea:{
		   			width : "95%",
		   			height : "80%"
		   		},
		   		legend: { 
		   			position: 'none' 
		   		},
		   		title : "rpm",
		 		vAxis:{
		 			minValue : 0,
		 			maxValue : maxRPM
		 		},
		 		
		   	};
		
		line_data.insertRows(index,[[spindle,Number(spindle),spindle]]);
		index++;
		
		if(line_data.getNumberOfRows()>=7){
			line_options.hAxis.viewWindow.min += 1;
		  	line_options.hAxis.viewWindow.max += 1;
		};
		
		line_chart.draw(line_data, line_options);
	};
	
	var flag = true;
	function getData(){
		var url = "${ctxPath}/nfc/conn.do";
		var param = "ip=" + ip + 
						"&port=" + port;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				var index = data.split("/");
				spindle = index[0];
				actualFeed = index[1];
				alarmCnt = index[2];
				alarmNum = index[3];
				status = index[4];
				
				console.log(spindle + "/" + status)
				if(spindle>maxRPM){
					maxRPM = spindle;	
				};
				
				$("#alarmCnt").html("알람 : " + alarmCnt);
				$("#alarmNum").html(alarmNum);
				
				if(status=="STRT"){
					$("#blueLight").css("background-color","#2EFE64");
					$("#blueLight").html("정상");
				}else if(status=="HALT"){
					$("#blueLight").css("background-color","yellow");
					$("#blueLight").html("정지");
				}else if(status=="STOP"){
					$("#blueLight").css("background-color","red");
					$("#blueLight").html("알람");
				}else if(status=="RESET"){
					$("#blueLight").css("background-color","green");
					$("#blueLight").html("리셋");
				}
				
				//loop();
				guage_loop();
				bar_loop();
				
				if(pStatus!=status){
					time_loop();
				};
				$(".circle").css("display","inline");
				$("#alarm").css("display","inline");
				
				/* if(flag){
					$("#blueLight").css("opacity","1");
					flag = false;
				}else{
					$("#blueLight").css("opacity","0");
					flag = true;
				} */
			},
			error : function(){
				var date = new Date();
				var hour = date.getHours();
				var minute = date.getMinutes();
				var second = date.getSeconds();
				time_data.addRows([
				                   [ "Power OFF", new Date(0, 0, 0, hour, minute,second), new Date(0, 0, 0, hour, minute,second) ],
				                   ]);
				
				time_chart.draw(time_data, time_options);
				
				guage_options = {
				         width: 200, 
				         height: 200,
				         min : 0,
				         max : 1,
				      /*    redFrom: 1950, 
				         redTo: 2000,
				         yellowFrom:1900, 
				         yellowTo: 1950, */
				         minorTicks: 5
				       	};
				 
				guage_data.setValue(0, 1, Number(0));
				guage_chart.draw(guage_data, guage_options);
			}
		});
	};
	
	function guage_loop(){
		 guage_options = {
		         width: 200, 
		         height: 200,
		         min : 0,
		         max : maxRPM,
		      /*    redFrom: 1950, 
		         redTo: 2000,
		         yellowFrom:1900, 
		         yellowTo: 1950, */
		         minorTicks: 5
		       	};
		 
		guage_data.setValue(0, 1, Number(spindle));
		guage_chart.draw(guage_data, guage_options);
	};
	
	function time_loop(){
		var date = new Date();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var second = date.getSeconds();
		time_data.addRows([
			                   [ status, new Date(0, 0, 0, Number(hour), Number(minute),Number(second)), new Date(0, 0, 0, Number(hour), Number(minute), Number(second)) ]
			                   ]);
		 
		time_chart.draw(time_data, time_options);
		$("#now").html(hour + " : " + minute + " : " + second);
	};
	
	function bar_loop(){
		var feed = 300 + parseInt(Math.random()*100);
		bar_data = new google.visualization.DataTable();
		bar_data.addColumn("string","feed");
		bar_data.addColumn("number","value");
		bar_data.addColumn({type:'string', role:'annotation'});
		bar_data.addRows([
		                  	["feed",feed,String(actualFeed)]
		                  ]);
		//bar_chart.draw(bar_data, bar_options);
	};
	
	function setChartInterval(){
		var interval = $("#interval").val()*1000;
		intervalValue = interval;
		
		clearInterval(myInterval);
		getDataLoop();	
	};
	
	function resetIntervalVal(){
		$("#interval").val("");
	};
	
	function sendURI(){
		var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
		location.href="ditalk://talkurl?confirm=" + url;
	};
</script>
<style type="text/css">
	.circle{
		height: 10px;
		padding: 10px 5px 10px 5px;
		display: none;
		border-radius: 100%;
	   -o-border-radius: 100%;
	   -webkit-border-radius: 100%;
	   -moz-border-radius: 100%;
	}
	#alarm{
		display: none;
	}
</style>
</head>
<body>
	<Table width="100%" style="margin: 0px; text-align: center;">
		<tr>
			<td colspan="2">갱신주기 : 
				<input id="interval" type="tel" size="2" onfocus="resetIntervalVal();">(초)
				<button onclick="setChartInterval();" >설정</button>
				<button onclick="sendURI()">공유</button>
			</td>
		</tr>
		<tr>
			<td width="50%" align="left"><span id="startTime" style="margin-left: 15px"></span></td>
			<td width="50%" align="right"><span id="now"  style="margin-right: 15px"></span></td>
		</tr>
		<tr>
			<td colspan="2">
				<center>
					<div id="time_chart" style="width: 90%; height: 30%"></div>
				</center>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<center>
					<div id="line_chart" style="width: 95%; height: 30%"></div>
				</center>			
			</td>
		</tr>
		<tr>
			<td>
				<center>
					<div id="guage_chart" style="width: 95%; height: 30%"></div>
				</center>
			</td>
			<td align="center">
				<!-- <font style="font-weight: bold;" id="alarm"> Alarm</font><br> -->
				<br>
				<span class="circle" style="background-color: #2EFE64;" id="blueLight">정상</span>
			</td>		
		</tr>
		<%-- <tr>
			<td colspan="2">
				<center>
					<div id="bar_chart" style="width: 95%; height: 30%"></div>
				</center>
			</td>
		</tr> --%>
		<tr>
			<!-- <td colspan="2">
				<div id="alarmCnt"></div>
				<div id="alarmNum"></div>
			</td> -->
			
		</tr>
	</Table>
</body>
</html>