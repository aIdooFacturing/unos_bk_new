<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}


#grid{
	border-bottom : 1px solid #1E1E23  !important;
	border-right : 1px solid #1E1E23 !important;
	border-left : 1px solid #1E1E23  !important;
	border-top : 1px solid #1E1E23 !important;
}
.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}
.k-grid-header-wrap{
	border-right : 1px solid #1E1E23 !important;
}

.k-grid thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid tbody > tr
{
 background : #DCDCDC ;
 text-align: center;
}
.k-grid tbody > .k-alt
{
 background : #F0F0F0 ;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
  }
</style> 
<script type="text/javascript">


const loadPage = () =>{
	createMenuTree("qm", "addFaultyHistory")
	
}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	$(function(){
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		$("#oprNm_form select").change(function(){
			if($("#oprNm_form select").val()=="0" || $("#prdNo_form select").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form select").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form select").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form select").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form select").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form select").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form select").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
		$("#prdNo_form select").change(function(){
			if($("#oprNm_form select").val()=="0" || $("#prdNo_form select").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form select").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form select").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form select").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form select").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form select").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form select").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	function setDate(el, val){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		el.val(year + "-" + month + "-" + day);
		if(typeof(val)!="undefined"){
			el.val(val);	
		}
	};
	
	function addRow(){
		if(classFlag){
			className = "row2"
		}else{
			className = "row1"
		};
		classFlag = !classFlag;
		
		var checkTy = document.createElement("select");
		var prdNo = document.createElement("select");
		prdNo.setAttribute("id", "prdNo_selector_form");
		var process = document.createElement("select");
		var device = document.createElement("select");
		var worker = document.createElement("select");
		var sDate = document.createElement("input");
		sDate.setAttribute("type", "text");
		sDate.setAttribute("readonly", "readonly");
		var text = document.createElement("input");
		
		text.style.cssText = "width : " + getElSize(400);
		var cnt = document.createElement("input");
		cnt.setAttribute("class", "cnt");
		cnt.setAttribute("size", 5);
		var part = document.createElement("select");
		var situationTy = document.createElement("select");
		var situation = document.createElement("select");
		var cause = document.createElement("select");
		var gchTY = document.createElement("select");
		var gch = document.createElement("select");
		var action = document.createElement("select");
		
		var tr = document.createElement("tr");
		tr.setAttribute("class", className);
		
		var checkTy_td = document.createElement("td");
		checkTy_td.append(checkTy);
		tr.append(checkTy_td);
		$("#chkTy_form").html(checkTy_td)
		
		var prdNo_td = document.createElement("td");
		prdNo_td.append(prdNo);
		tr.append(prdNo_td);
		$("#prdNo_form").html(prdNo_td)
		
		var process_td = document.createElement("td");
		process_td.append(process);
		tr.append(process_td);
		$("#oprNm_form").html(process_td)
		
		var device_td = document.createElement("td");
		device_td.append(device);
		tr.append(device_td);
		$("#dvcId_form").html(device_td)
		
		var sDate_td = document.createElement("td");
		sDate_td.append(sDate);
		tr.append(sDate_td);
		$("#date_form").html(sDate_td)
		
		var text_td = document.createElement("td");
		text_td.append(worker);
		tr.append(text_td);
		$("#checker_form").html(text_td)
		
		var part_td = document.createElement("td");
		part_td.append(part);
		tr.append(part_td);
		$("#part_form").html(part_td)
		
		var situ_td = document.createElement("td");
		situ_td.append(situationTy);
		tr.append(situ_td);
		$("#situation_form").html(situ_td)
		
		var situTy_td = document.createElement("td");
		situTy_td.append(situation);
		tr.append(situTy_td);
		$("#situationTy_form").html(situTy_td)
		
		var cause_td = document.createElement("td");
		cause_td.append(cause);
		tr.append(cause_td);
		$("#cause_form").html(cause_td)
		
		var gchkTy_td = document.createElement("td");
		gchkTy_td.append(gchTY);
		tr.append(gchkTy_td);
		$("#gchTy_form").html(gchkTy_td)
		
		var gch_td = document.createElement("td");
		gch_td.append(gch);
		tr.append(gch_td);
		$("#gch_form").html(gch_td)
		
		var cnt_td = document.createElement("td");
		cnt_td.append(cnt);
		tr.append(cnt_td);
		$("#cnt_form").html(cnt_td)
		
		var action_td = document.createElement("td");
		action_td.append(action);
		tr.append(action_td); 
		$("#action_form").html(action_td)
		
		var button_td = document.createElement("td");
		var button = document.createElement("button");
		//button.setAttribute("id", "b" + data.id);
		var button_text = document.createTextNode("삭제");
		button.append(button_text);
		button_td.append(button)
		tr.append(button_td);
		
		//$("#tbody").append(tr);
		
		getCheckTyList($(checkTy));
		getProcessList($(process));
		getDevieList($(device));
		getWorkerList($(worker));
		setDate($(sDate));
		getPartList($(part));
		getSituationTyList($(situationTy));
		console.log("mmㅡㅡ")
		console.log($(situation))
		getSituationList(111,$(situation));
		getCauseList($(cause));
		getGChTyList($(gchTY));
		getGChList(7, $(gch));
		getActionList($(action)); 
		getPrdNoList($(prdNo));
		$(cnt).val(0);
		
		
		$(".row1").not(".tr_table_fix_header").css({
			"background-color" : "#222222"
		});

		$(".row2").not(".tr_table_fix_header").css({
			"background-color": "#323232"
		});
		
		
		$("#insertForm select, #insertForm input").css("font-size",getElSize(80))
		
		
		showCorver();
		$("#insertForm").css("z-index", 999)
		
		if(addFaulty!=""){
			$("#prdNo_selector_form option[value='" + $prdNo + "']").attr("selected", "selected");
			$("#cnt_form").children("td").children("input").val($cnt);
		}
		
		setEl();
	};
	function getGch(){
		console.log($(this))
		var val = $(this).val();
		console.log(val)
		var ty;
		if(val=="47"){			//업체
			ty = "com";
		}else if(val=="48"){	//작업자  
			ty = "worker"	
		}else if(val=="64"){
			ty = "dvc";
		};
		console.log(ty,val)
		getGChList(ty, $("#gch_form").children("td").children("select"));
//		getGChList(7, $(gch));
	};
	
	var className = "";
	var classFlag = true;

	var menu = false;

	function getProcessList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 3; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPartList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 4;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};
	function getSituationPart(){
		console.log("dd")
		
		var val = $(this).val();
		var ty;
		
		getSituationList(val,$("#situationTy_form").children("td").children("select"))
	}
	function getSituationTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 5;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log("---현상구분---")
				console.log(json);
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getSituationPart);
//				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCnt(el, id){
		var url = ctxPath + "/chart/getCnt.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(data);
			}
		});	
	};

	function getChecker(el, id){
		var url = ctxPath + "/chart/getChecker.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(decode(data));
			}
		});	
	};

	function getSituationList(val, el){
		//val==>  30 ->소재불량 31 ->가공불량 32 ->기타
		
		// index =6 이면   or 14 이면
		var index;
		if(val=="30"){
			index=6;
		}else if(val=="31"){
			index=14
		}else if(val=="32"){
			index=15
		}else{
			el.html("<option value='0'>${selection}</option>");
			return; 
		}
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + index +
					"&chkTy=" +val;
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var json = data.dataList;
				
				console.log("---현상---")
				console.log(json)
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				$("#situationTy_form select").val(0)
				el.html(options).val();
				$("#situationTy_form select").val(0)
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				$("#situationTy_form select").val(0)
				
			}
		});	
	};

	function getCauseList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getGch);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChList(ty, el){
		//var url = ctxPath + "/chart/getCheckTyList.do"
		var url;
		if(ty=="com"){
			url = ctxPath + "/chart/getComList.do"		
		}else if(ty=="worker"){
			url = ctxPath + "/common/getWorkerList.do"
		}else if(ty=="dvc"){
			url = ctxPath + "/common/dvcList.do"
		}else{
			el.html("<option value='0'>${selection}</option>");
			return; 
		}
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				console.log(json)
				$(json).each(function(idx, data){
					if(ty=="dvc"){
						options += "<option value='" + data.dvcId + "'>" + decode(data.dvcName) + "</option>";						
					}else{
						options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					}
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getActionList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 10;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCheckTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 2;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	var comPrdList;
	function getPrdNoList(el, val){
		var url = ctxPath + "/common/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				comPrdList=json;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				el.html(options);
				//품번변경시 장비라우팅 부르기
//				el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});
	};

	var addFaulty = "${addFaulty}";
	var dvclist =[]
	function getDevieList(el, obj){
		var url = ctxPath + "/common/dvcList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $(obj).val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='0'>${selection}</option>";	
				var list=[];
				$(json).each(function(idx, data){
					options += "<option value='" + data.dvcId + "'>" + decode(data.dvcName) + "</option>";
					
					var arr={};
					arr.id=data.dvcId
					arr.dvcName = decode(data.dvcName);
					list.push(arr)
				});
				dvclist=list
				el.html(options);
			}
		});	
	};
	
	var workerlist=[]
	function getWorkerList(el, obj){
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList1;
				var options = "<option value='0'>${selection}</option>";	
				var list=[]
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
					var arr={};
					arr.id=data.id;
					arr.name=decodeURIComponent(data.name).replace(/\+/gi, " ")
					list.push(arr)
				});
				workerlist=list;
				el.html(options);
			}
		});	
	};
	companylist=[]
	//업체 리스트(선택박스)
	function getComList(){
		var url = "${ctxPath}/chart/getComList.do";
		
		$.ajax({
			url :url,
			dataType :"json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var option = "<select >";
				$(json).each(function(idx, data){
					var arr=new Object();
					option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					arr.id=data.id;
					arr.name=decode(data.name);
					companylist.push(arr);
				});
			
			}
		});
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(40)
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});

		$("#selected").css({
			"color" : "black",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#wrapper").css({
// 			"height" :getElSize(1600),
			"width" : $("#container").width() - getElSize(90),
			"overflow" : "auto"
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
		});
		
		
		$("select").css({
			"padding-right" : getElSize(80)
		})
		
		$("#wrapper select").css({
			"width" : getElSize(600),
			"height" : getElSize(80),
			"border": "1px black #999",
           "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
		})
		
		$("input").css({
            "border": "1px black #999",
            
            "height" : getElSize(80),
            "font-family": "inherit",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none",
			"border-color" : "#222327"
        })
		
		$("#wrapper button").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"font-size" : getElSize(48),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8)
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});

		
		$("#popupTable").css({
			"height":"100%"
			,"width":"100%"
			,"padding" : 0
			,"margin" : 0
		})
		
		$("#editPopup table").css("background","#EAEAEA")
		
		$("#wrapper .table_title").css({
			"background-color" : "#2B2D32",
			"color" : "white",
			"padding" : getElSize(20),
			"height" : getElSize(128),
			"font-size" : getElSize(48)
		});
		
		$("#search").css("margin-left","25%");
	};
	
	function onDataBound() {
/*         var grid = this;

		$("#grid").on("dblclick", "tbody>tr", function (e) {
			console.log("--dbclick--")
			var dataItem = $("#grid").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			$("#editPopup").data("kendoDialog").open();
			console.log("몇번보낼까?")
			//			
//			$("#grid").data('kendoGrid').editRow(dataItem); 
		}); */
	}
	
	var allList=[]
	function getSelectList() {
		var url = ctxPath + "/chart/getSelectList.do"
		var param;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				$(json).each(function(idx,data){
					data.name=decode(data.name);
					allList.push(data)
				})
				
				console.log("---select---")
				console.log(data)
				getTable();
			}
		})
	}
	function viewDvc(id){
//		alert(id)
		if(id==0){
			return "미입력"
		}
		for(i=0,len=dvclist.length; i<len; i++){
			if(Number(dvclist[i].id)==Number(id)){
				return dvclist[i].dvcName;
			}
		}
	}
	function viewWorker(id){
		if(id==0){
			return "미입력"
		}
		for(i=0,len=workerlist.length; i<len; i++){
			if(Number(workerlist[i].id)==Number(id)){
				return workerlist[i].name;
			}
		}
	}
	function viewCompany(id){
		if(id==0){
			return "미입력"
		}
		for(i=0,len=companylist.length; i<len; i++){
			if(Number(companylist[i].id)==Number(id)){
				return companylist[i].name;
			}
		}
	}
	
	function viewName(id){
		if(id==0){
			return "미입력"
		}
		for(i=0,len=allList.length; i<len; i++){
			if(Number(allList[i].id)==Number(id)){
				return allList[i].name;
			}
		}
	}
	
	function chkTyList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "name",
			 dataValueField  : "id",
			 dataSource: allList,
		 }).data("kendoDropDownList");
	}
	
	function pop_print(){
		win = window.open();
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
	}
	
	function exportGridWithTemplatesContent(e){
		
		console.log(e);
		console.log(e.data);
		console.log(e.sender.columns);
		console.log(e.workbook.sheets[0]);
		
		var data = e.data;
		var gridColumns = e.sender.columns;
		var sheet = e.workbook.sheets[0];
		var visibleGridColumns = [];
		var columnTemplates = [];
		var dataItem;
		// Create element to generate templates in.
		var elem = document.createElement('div');
		
		// Get a list of visible columns
		for (var i = 0; i < gridColumns.length; i++) {
			if (!gridColumns[i].hidden) {
				visibleGridColumns.push(gridColumns[i]);
			}
		}
		// Create a collection of the column templates, together with the current column index
		for (var i = 0; i < visibleGridColumns.length; i++) {
			if (visibleGridColumns[i].template) {
				columnTemplates.push({ cellIndex: i, template: kendo.template(visibleGridColumns[i].template) });
			}
		}
		
		// Traverse all exported rows.
		for (var i = 1; i < sheet.rows.length; i++) {
// 			console.log("3333333")
			var row = sheet.rows[i];
			// Traverse the column templates and apply them for each row at the stored column position.
			// Get the data item corresponding to the current row.
			var dataItem = data[i - 1];
			if(dataItem!=undefined){
				for (var j = 0; j < columnTemplates.length; j++) {
					var columnTemplate = columnTemplates[j];
					// Generate the template content for the current cell.
					elem.innerHTML = columnTemplate.template(dataItem);
					if (row.cells[columnTemplate.cellIndex] != undefined)
					  // Output the text content of the templated cell into the exported cell.
						row.cells[columnTemplate.cellIndex].value = elem.textContent || elem.innerText || "";
				}
			}
		}
		
	}
	
	var kendotable;
	$(document).ready(function(){
		getComList();
		setDate($("#sDate"));
		setDate($("#eDate"));
		getPrdNoList($("#prdNo"));
//		getDevieList();	//장비이름
		addRow()
		getSelectList()
		kendotable = $("#grid").kendoGrid({
			height:getElSize(1600),
			scrollable:true,
			editable: "popup",
			selectable: true,
			toolbar :[{name:"excel"}]
			,excel:{
				fileName:"불량조회" + moment().format("YYYY-MM-DD") + ".xlsx"
			},
			excelExport: exportGridWithTemplatesContent,
			filterable: true,
			//dataBound: onDataBound,
			columns:[
	        	{
	        		field:"chkTy", 
	        		editor : chkTyList,
	        		title: "${check_ty}",
	        		width : getElSize(200),
	        		template: "#=viewName(chkTy)#",
	        		filterable: false,
	        		attributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }
	        	},
	        	{
	        		field:"prdNo", 
	        		title: "${prd_no}", 
	        		width : getElSize(350),
	        		filterable: {
	        	        multi:true
	        	    },
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"prdPrc", 
	        		title: "${operation}", 
	        		template: "#=viewName(prdPrc)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.prdPrc#'/><span>#= viewName(data.prdPrc)|| data.all #</span></label></li>"
	                    }
	        	    },
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"dvcName", 
	        		title: "${device}", 
	        		template: "#=viewDvc(dvcName)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.dvcName#'/><span>#= viewName(data.dvcName)|| data.all #</span></label></li>"
	                    }
	        	    },
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"sDate", 
	        		title: "${date_}", 
	        		filterable: false,
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"checker",	//등록자
	        		title: "${reporter}", 
	        		template: "#=viewWorker(checker)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.checker#'/><span>#= viewWorker(data.checker)|| data.all #</span></label></li>"
	                    }
	        	    },
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"part", //부위
	        		title: "${part}", 
	        		template: "#=viewName(part)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.part#'/><span>#= viewName(data.part)|| data.all #</span></label></li>"
	                    }
	        	    },     
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"situ", //현상구분
	        		title: "${divide_situ}", 
	        		width : getElSize(200),
	        		template: "#=viewName(situ)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.situ#'/><span>#= viewName(data.situ)|| data.all #</span></label></li>"
	                    }
	        	    },   
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"situTy", //현상
	        		title: "${situ}", 
	        		template: "#=viewName(situTy)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.situTy#'/><span>#= viewName(data.situTy)|| data.all #</span></label></li>"
	                    }
	        	    },   
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	  style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"cause",//원인
	        		title: "${cause}", 
	        		template: "#=viewName(cause)#",
	        		filterable: {
	        	        multi:true,
	        	        itemTemplate: function(e) {
	        	        	console.log(e)
	                        return "<li class='k-item'><label class='k-label'><input type='checkbox' name='" + e.field + "' value='#= data.cause#'/><span>#= viewName(data.cause)|| data.all #</span></label></li>"
	                    }
	        	    },   
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"gchTy",	//귀책구분
	        		title: "${gch_ty}", 
	        		width : getElSize(200),
	        		template: "#=viewName(gchTy)#",
	        		filterable : false,
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"gch",	//귀책
	        		title: "${gch}", 
//	        		template: "#=viewName(gch)#",
	        		template:kendo.template("#if (gchTy == 64) {# #=viewDvc(gch)##} else if(gchTy == 48){# #=viewWorker(gch)##}  else if(gchTy == 47){# #=viewCompany(gch)##} else{# #='미입력'# # } #"),
	        		filterable : false,
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"cnt", 
	        		title: "${count}",
	        		width : getElSize(100),
	        		filterable : false,
	        		
	        		footerTemplate : "333",
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    },
	        	{
	        		field:"action", 
	        		title: "${action}", 
	        		template: "#=viewName(action)#",
	        		filterable : false,
	        		attributes: {
	        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	},
        	    	headerAttributes: {
        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
        	    	}
        	    }
	        ]
		}).data("kendoGrid")

		
		
		$("#editPopup").kendoDialog({
			height: getElSize(1100)
			,width: getElSize(2200)
			,title: "불량등록 수정"
		 	/* ,content: "<table border=1 style='color:black;'> <tr> <th> Tool Cycle Count </th> <th> RunTime (h) </th> </tr>" +
		 			 "<tr> <td> <input id='preCnt' type='number' value=0> </td> <td>  <input id='preSec' type='number' value=0> </td> </tr> </table>"
			 */,actions: [{
				text: "OK"
				,action: function(e){
					// e.sender is a reference to the dialog widget object
					// OK action was clicked
					// Returning false will prevent the closing of the dialog
					valArray = [];
					var obj = new Object();
					obj.id = saveId;
					obj.chkTy = $("#chkTy_form select").val();	//검사구분
					obj.prdNo = $("#prdNo_form select").val();	//품번
					obj.prdPrc = $("#oprNm_form select").val();	//공정
					obj.dvcId = $("#dvcId_form select").val();	//장비
					obj.date = $("#date_form input").val();	//날짜
					obj.checker = $("#checker_form select").val();	//신고자
					obj.part = $("#part_form select").val();	//부위
					obj.situationTy = $("#situationTy_form select").val();	//현상
					obj.situation = $("#situation_form select").val();	//현상구분
					obj.cause = $("#cause_form select").val();	//원인
					obj.gchTY = $("#gchTy_form select").val();	//귀책구분
					obj.gch = $("#gch_form select").val();	//귀책
					obj.cnt = $("#cnt_form input").val();	//수량
					obj.action = $("#action_form select").val();	//조치
					if($("#oprNm_form select").val()=="20"){
						obj.proj="0000"
					}else if($("#oprNm_form select").val()=="20"){	//소재 자재
						obj.proj="0000"
					}else if($("#oprNm_form select").val()=="21"){	//라인 공정
						obj.proj="0005"
					}else if($("#oprNm_form select").val()=="22"){	//R	공정
						obj.proj="0010"
					}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
						obj.proj="0020"
					}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
						obj.proj="0030"
					}else if($("#oprNm_form select").val()=="25"){	//완성창고
						obj.proj="0090"
					}else if($("#oprNm_form select").val()=="26"){	//고객사
						obj.proj="0"
					}else if($("#oprNm_form select").val()=="27"){	//필드
						obj.proj="0"
					}
					valArray.push(obj);
					
					var obj = new Object();
					obj.val = valArray;
					
					var url = "${ctxPath}/chart/faultUpdate.do";
					var param = "val=" + JSON.stringify(obj);
					console.log(param)
					$.ajax({
						url : url,
						data : param,
						type : "post",
						dataType : "text",
						success : function(data){
							if(data=="success"){
								getTable();
								alert("${save_ok}");
							}
							$.hideLoading()
						}
					});	
					
//					alert("성공")

				},
				primary: true
			},{
				text: "Cancel"
			}]
		});
		$("#editPopup").data("kendoDialog").close();
		
		$("#grid").on("dblclick", "tbody>tr", function (e) {
			console.log("--dbclick--")
			console.log("몇번보낼까?")
			var dataItem = $("#grid").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			saveId = dataItem.id
			$("#editPopup").data("kendoDialog").open();
			
			$("#chkTy_form select").val(dataItem.chkTy);	//검사구분
			$("#prdNo_form select").val(dataItem.prdNo);	//품번
			$("#oprNm_form select").val(dataItem.prdPrc);	//공정
			$("#dvcId_form select").val(dataItem.dvcName);	//장비
			$("#date_form input").val(dataItem.sDate);	//날짜
			$("#checker_form select").val(dataItem.checker);	//신고자
			$("#part_form select").val(dataItem.part);	//부위
			$("#situation_form select").val(dataItem.situ);	//현상구분
			getSituationList(dataItem.situ,$("#situationTy_form select"))

			$("#situationTy_form select").val(dataItem.situTy);	//현상
			$("#cause_form select").val(dataItem.cause);	//원인
			$("#gchTy_form select").val(dataItem.gchTy);	//귀책구분
 			var ty;
			if(dataItem.gchTy=="47"){			//업체
				ty = "com";
			}else if(dataItem.gchTy=="48"){	//작업자  
				ty = "worker"	
			}else if(dataItem.gchTy=="64"){
				ty = "dvc";
			};
			console.log("--ty--")
			console.log(ty)
			getGChList(ty, $("#gch_form").children("td").children("select"));
//			getGChTyList($("#gch_form select"),ty)
 			setTimeout(function() {
				$("#gch_form select").val(dataItem.gch);	//귀책
 			}, 1000);
			$("#cnt_form input").val(dataItem.cnt);	//수량
			$("#action_form select").val(dataItem.action);	//조치
			
			$("#prdNo_form select").attr("disabled","disabled")	//품번
			$("#oprNm_form select").attr("disabled","disabled")	//공정
//			$("#date_form input").attr("disabled","disabled")	//날짜
			$("#checker_form select").attr("disabled","disabled")	//신고자
			$("#cnt_form input").attr("disabled","disabled")		//수량
			
			//			
//			$("#grid").data('kendoGrid').editRow(dataItem); 
		});
		
		$( "#date_form input" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				//getDeliveryHistory();
				$("#date_form input").val(e);
			}
		})
	});
	
	
	var saveId;
	function getTable(){
		
		var url = ctxPath + "/chart/getFaultList.do";
		console.log($("#sDate").val())
		console.log($("#eDate").val())
		var param = "prdNo=" + $("#prdNo").val() + 
		"&sDate=" + $(" #sDate").val() + //$(" #sDate").val()
		"&eDate=" + $(" #eDate").val() ; //$(" #eDate").val()
		
		console.log(param)
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			data : param,
			success : function(data){
				var json = data.dataList;
				console.log(json)
				$(json).each(function(idx,data){
					data.dvcName=decode(data.dvcName);
					data.checker=decode(data.checker);
				})
				
				var dataSource = new kendo.data.DataSource({
					data: json
					,schema: {
						model: {
							id: "id",
							fields: {
								sDate: { editable: false },
							}
						}
					}
				});
				kendotable.setDataSource(dataSource);
				
				$("#grid").data("kendoGrid").autoFitColumn(12);
				//print
				
				$("#print_table").empty();
				var table ="<table border='1' class='top_table' width='95%' align='center'><tr>"+
				"<th rowspan='3' colspan='2'>불량 등록 조회 <br>"+ 
				"<br>"+ $("#sDate").val() + " ~ " + $("#eDate").val() + "</th><th style='background : lightgray' colspan='3'>결제</th></tr>" +
				"<tr style='background : lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
				"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
				"</table>"+
				"<table class='p_table' border='1' width='95%' align='center'>"+
				"<tr style='background : lightgray'><th>검사구분</th> <th>품번</th> <th>공정</th> <th>장비</th> <th>날짜</th> <th>신고자</th> <th>부위</th> <th>현상구분</th> <th>현상</th> <th>원인</th> <th>귀책구분</th> <th>귀책</th> <th>수량</th> <th>조치</th></tr>";

				
				$(json).each(function(idx,data){
					var gch;
					if(data.gchTy==64){
						gch=viewDvc(data.gch)
					} else if(data.gchTy==48){
						gch=viewWorker(data.gch) 
					} else if(data.gchTy==47){
						gch=viewCompany(data.gch) 
					}else{
						gch="미입력 "
					}
					console.log(gch)
					//보여주는 테이블
					tr = "<tr class='" + className + "'>" + 
								"<td>" + viewName(data.chkTy) + "</td>" +
								"<td>" + data.prdNo + "</td>" + 
								"<td>" + viewName(data.prdPrc) + "</td>" + 
								"<td>" + viewDvc(data.dvcName) + "</td>" + 
								"<td>" + data.sDate + "</td>" + 
								"<td>" + data.checker + "</td>" + 
								"<td>" + viewName(data.part) + "</td>" + 
								"<td>" + viewName(data.situTy) + "</td>" + 
								"<td>" + viewName(data.situ) + "</td>" +
								"<td>" + viewName(data.cause) + "</td>" + 
								"<td>" + viewName(data.gchTy) + "</td>" + 
								"<td>" + gch + "</td>" + 
								"<td>" + data.cnt + "</td>" + 
								"<td>" + viewName(data.action) + "</td>" + 
							"</tr>";
					//$("#tbody").append(tr);
					
					
					//프린트 테이블
					table+=tr;
				})

				//프린트
				table+="</table>"
				$("#print_table").append(table)
				
				//print css
				$(".sign").css({
					"height" : "50px"
				})
				$(".sign th").css({
					"width" : "70px"
				})
				$(".p_table tr th").css({
					"font-size" : "0.1px"	
				})
				$(".p_table tr td").css({
					"font-size" : "0.1px"	
				})
				
				$(".top_table").css({
					"font-size" : "10px"
				})

				
			}
		})
	}
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="wrapper">
						<table style="width: 100%">
							<tr>
								<td>
								
								
									<table style="width: 100%">
										<Tr>
											<Td class='table_title'>
												<font class='label'><spring:message code="prd_no"></spring:message></font>		
												<select id="prdNo"></select>
												<input type="date" id="sDate" class="date"> ~ <input type="date" id="eDate" class="date">
												<button id="search" onclick="getTable()" style="cursor: pointer; x"><i class="fa fa-search" aria-hidden="true"></i>검색</button>
												<button id="print" class="disable" onclick="pop_print()"><i class="fa fa-print" aria-hidden="true"></i>프린트</button>
											</Td>
										</tr>	
									</table>
															
								
								
								</td>
							</tr>
							<Tr>
								<Td style="text-align: center; vertical-align: middle;">
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
		</table>
	 </div>
	<div id="editPopup">
	<table style="color: black;" id="popupTable">
		<Tr> 
			<Td class='table_title'> <spring:message code="prd_no"></spring:message> * </Td> <Td id="prdNo_form"> </Td> <Td class='table_title' width="20%"> <spring:message code="check_ty"></spring:message>  </Td> <Td id="chkTy_form"  width="30%"></Td> 
		</Tr>
		<Tr> 
			<Td class='table_title'  width="20%"><spring:message code="operation"></spring:message> * </Td> <Td id="oprNm_form"></Td> <Td class='table_title'> <spring:message code="device"></spring:message> </Td> <Td id="dvcId_form"></Td>
		</Tr>
		<Tr> 
			<Td class='table_title'> <spring:message code="reporter"></spring:message> * </Td> <Td id="checker_form"></Td> <Td class='table_title'> <spring:message code="event_date"></spring:message> </Td> <Td id="date_form"></Td> 
		</Tr>
		<Tr> 
			<Td class='table_title'> <spring:message code="divide_situ"></spring:message> * </Td>  <Td id="situation_form"></Td><Td class='table_title'> <spring:message code="part"></spring:message> </Td> <Td id="part_form"></Td> 
		</Tr>
		<Tr> 
			<Td class='table_title'> <spring:message code="situ"></spring:message> * </Td> <Td id="situationTy_form"></Td> <Td class='table_title' > <spring:message code="cause"></spring:message> </Td> <Td id="cause_form"></Td>
		</Tr>
		<Tr> 
			<Td class='table_title'>  <spring:message code="gch_ty"></spring:message> </Td> <Td id="gchTy_form"></Td> <Td class='table_title'> <spring:message code="gch"></spring:message> </Td> <Td id="gch_form"></Td>
		</Tr>
		<Tr> 
			<Td class='table_title'> <spring:message code="count"></spring:message> * (<label id="exCnt">0</label>)</Td> <Td id="cnt_form"></Td> <Td class='table_title'> <spring:message code="action"></spring:message>  </Td> <Td id="action_form"></Td>
		</Tr>
<%-- 		<Tr>
			<Td colspan="4" style="text-align: center;"><button onclick="saveRow2()"><spring:message code="save"></spring:message> </button></Td>
		</Tr>
 --%>
	</table>
	</div>
	<div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	