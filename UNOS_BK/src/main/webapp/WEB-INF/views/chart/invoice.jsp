<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<title></title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<style>
*{
	color:black !important;
}
</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("im", "StockHistory")	
	}

	$(function(){
		$("#flagDiv").remove()
		setEl();
		parsingData();
		$("#cat_div").remove()
	});
	
	function init() {
		
	}
	
	function setEl(){
		$("#corver").remove();
		$("h1").css({
			"font-size" :  "25px"
		});
		
		$("#main_table").css({
			"margin-top" : "10px;",
			"font-size" : "10px"
		})
		$(".table td").css({
			"text-align" : "center",
			"border" : "1px solid black",
			"font-size" : "11px"
		});
	}
	
	var jsonData = '${jsonData}';
	
	function createBarcode(barCode){
		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			async : false,
			data : "deliveryCd=" + barCode,
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					
				}
			}
		});
	}
	
	var lotsplit;
	function parsingData(){
		var json = JSON.parse(jsonData);
		$("#deliverer").html(decodeURIComponent(json[0].deliverer))
		$("#delivery_date").html(json[0].deliverDate)
		$("#deliveryNo").html(json[0].deliveryNo)
		$("#barcode").attr("src", "http://bkjm.iptime.org/barcode/" + json[0].deliveryNo + ".png");
		//$("#barcode").attr("src", ctxPath + "/images/1707240003.png");

		console.log(json)
		
		var tr;
		var barcdTable = "";
		$(json).each(function(idx, data){
			tr += "<tr>" + 
						"<td>" + (idx+1) + "</td>" +
						"<td>" + data.barcode + "</td>" +
						"<td>" + data.prdNo + "</td>" +
						"<td>" + data.ITEMNO + "</td>" +
						"<td>" + data.spec + "</td>" +
						"<td>" + data.unit + "</td>" +
						"<td>" + data.cnt + "</td>" +
						"<td>" + numberWithCommas(data.prc) + "</td>" +
						"<td>" + numberWithCommas(data.totalPrc) + "</td>" +
				 "</tr>;"
				 
			
			createBarcode(data.barcode);
			if(idx%3==0){
				barcdTable = "<div style='page-break-before: always;'>" + 
									"<center>" + 
										"<div  style='font-size: 23px; border: 1px solid black; height: 33px;'>" + 
											"<span style='display: table-cell'>" + data.lotNo + "</span>" + 
										"</div>" + 
									"</center>";
			}else{
//	 				barcdTable = "<div>" + 
				barcdTable = "<table style='width: 100%;'><tr><td>" + 
								"<center>" + 
									"<div  style='font-size: 23px; border: 1px solid black; height: 33px;'>" + 
										"<span style='display: table-cell'>" + data.lotNo + "</span>" + 
									"</div>" + 
								"</center>"
				+"</td></tr></table><div>";
				;
			}	
								
			for(var i = 0; i < 4; i++){
				barcdTable += "<table class='table' style='border:1px solid black; width:48%; border-collapse: collapse; float : left; margin:3px; height:138px'><tr>" + 
									"<td>납품번호</td>" + 
									"<td>" + json[0].deliveryNo + "</td>" +
									"<td rowspan='7' width='50%'><img width='70%' src='http://bkjm.iptime.org/barcode/" + data.barcode +".png'></td>" +
									//"<td rowspan='5' width='50%'><img width='90%' src='" + ctxPath + "/images/1707240003.png'></td>" +
								"</tr>" +
								"<Tr>" +
									"<Td>공급업체</tD>" +
									"<Td>" + decodeURIComponent(json[0].deliverer) + "</tD>" +
								"</tr>" +
								"<Tr>" +
									"<Td>납품일자</tD>" +
									"<Td>" + json[0].deliverDate + "</tD>" +
									"</tr>" + 
								"<Tr>" +
									"<Td>차종</tD>" +
									"<Td>" + data.prdNo + "</tD>" +
								"</tr>" +
								"<Tr>" +
									"<Td>품번</tD>" +
									"<Td>" + data.ITEMNO + "</tD>" +
								"</tr>" +
								"</tr>" + 
									"<Tr>" +
									"<Td>로트번호</tD>" +
									"<Td>" + data.barcode + "</tD>" +
								"</tr>" + 
									"<Tr>" +
									"<Td>로트수량</tD>" +
									"<Td>" + data.cnt + "</tD>" +
								"</tr></table>";
			} 
			
			barcdTable += "</div>";
			$("body").append(barcdTable)

		})
		
		$("#main_table").append(tr);
		setEl();
		window.print();
		
	};
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}


</script>
</head>
<body>
	<center><h1>납 품 서 (입고 처리용)</h1>
	
		<table id="top_table" style="width: 95%">
			<Tr>
				<Td style="width: 20%" >납품번호</Td>
				<Td style="width: 20%" id="deliveryNo"></Td>
				<td  rowspan="3" style="width: 60%"><img alt="" src="" width="100%" id="barcode"> </td> 
			</Tr>
			<tr>
				<Td>공급업체</Td>
				<td id="deliverer"></td>
			</tr>
			<tr>
				<Td>납품일자</Td>
				<td id="delivery_date"></td>
			</tr>
		</table>
	</center>
			
	<table id="main_table" style="width: 100%; border-collapse: collapse;" class="table">
		<tr>
			<Td style="width: 10%">합계금액</Td>
			<Td colspan="7" >금  원 (<span id="total_price"></span>원)</Td>
		</tr>
		<tr>
			<td>순번</td>
			<td>로트번호</td>
			<td>차종</td>
			<td>품번</td>
			<td>규격</td>
			<td>단위</td>
			<td>수량</td>
			<td>단가</td>
			<td>금액</td>
		</tr>
	</table>
	
	
	<!-- <div id="barcodeDiv" >
		<center>
			<div id="lotTrcerNo" style="font-size: 30px; border: 1px solid black; height: 70px;">
				<span style="display: table-cell">로트 추적 번호</span>
			</div>
		</center>	
	</div> -->
	
	<div id="cat_div"></div>
</body>
</html>	