package com.unomic.cnc;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.ByteArrayBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unomic.cnc.communication.params.RepHistoryParam;
import com.unomic.cnc.communication.params.RepLoginParam;
import com.unomic.cnc.communication.params.ReqLoginParam;
import com.unomic.cnc.communication.params.ReqLogoutParam;
import com.unomic.cnc.communication.params.ReqStatusParam;
import com.unomic.cnc.communication.params.ReqUpDownParam;
import com.unomic.cnc.data.CncFile;
import com.unomic.cnc.data.MemoryMap;



public class ConnectionController implements CNC
{
	private static final Logger logger = LoggerFactory.getLogger(ConnectionController.class);
	private final String CLASSNAME = this.getClass().getSimpleName();
	
	private static final int STATUS_INFO_BUFFER_SIZE = 8192;
	private static final int DEFAULT_BUFFER_SIZE = 1024;
	
	private Socket mTcpSocket;
	private OutputStream mOutputStream;
	private InputStream mInputStream;
	private String mIp;
	private String mPort;
	private boolean isConn;
	
	/**
	 * ���� ���� ���� Ƚ��
	 */
	private static final int RETRY_MAX	= 5;
	private int mRetryCount;
	
	/**
	 * �������� ����
	 */
	private String mErrMessage;
	private boolean mIsServerConnected;
	
	/**
	 * ���ŵ� ������
	 */
	private int mHeaderCommand;
	private int mHeaderContentSize;
	private byte[] mInputBuffer = new byte[8192];
	
	/**
	 * Single Instance
	 */
	private static ConnectionController instance = null;
	public static ConnectionController getInstance()
	{
		if(instance == null)
		{
			instance = new ConnectionController();
		}
		return instance;
	}
	
	
	public ConnectionController()
	{
		this.mRetryCount = 1;
//		this.mIsServerConnected = false;
		this.isConn = false;
	}
	
	public ConnectionController(String mIp, String mPort)
	{
		
		this.mRetryCount = 1;
//		this.mIsServerConnected = false;
		this.isConn = false;
		this.mIp = mIp;
		this.mPort = mPort;
		
	};
	 
	/**
	 * IP, PORT�� �����Ѵ�.
	 * @param ip
	 * @param port
	 */
	public void setServer(String ip, String port)
	{
		this.mIp = ip;
		this.mPort = port;
	}
	
	/**
	 * Socket ������ �����Ѵ�.
	 */
	public void disconnect()
	{
		//CNCUtils.Logi(CLASSNAME, "disconnect()");
		try
		{
			setConn(false);
			if(mTcpSocket != null) 
			{
				mTcpSocket.close();
			}
		} 
		catch (IOException e)
		{
			logger.error("disconnect fail.:"+e.toString());
		}
	}
	
	/**
	 * ������ �����Ѵ�. 
	 */
	public void connect()
	{
		try
		{
			// ���� ��
			mTcpSocket = getSocket();
			if(mTcpSocket != null)
			{
				setConn(true);
				mTcpSocket.setSoTimeout(CNC_CONNECTION_TIMEOUT);
				mOutputStream = mTcpSocket.getOutputStream();
				mInputStream = mTcpSocket.getInputStream();
				
				statusChange("CNC_CONNECTION_EXCEPTION_SUCCESS", true);
				//logger.info("Connection success.");
			}else{
				//CNCUtils.Logi(CLASSNAME, "Connection fail.");
				setConn(false);
				logger.error("mTcpSocket null. Connection fail.");
				statusChange("CNC_CONNECTION_EXCEPTION_SOCKFAIL", false);
			}
		}
		catch (SocketTimeoutException e)
		{
			setConn(false);
			if(mRetryCount < RETRY_MAX)
			{
				++mRetryCount;
				connect();
			}else{
				mRetryCount = 1;
				statusChange("CNC_CONNECTION_EXCEPTION_TIMEOUT", false);
				//CNCUtils.Logw(CLASSNAME, "Timeout : " + e.getMessage());
				logger.error(e.toString());
				//logger.info("socket timeout:"+e.getMessage());
			}
		}
		catch (Exception e)
		{
			setConn(false);
			statusChange("CNC_CONNECTION_EXCEPTION_FAIL", false);
			//CNCUtils.Logw(CLASSNAME, "Exception : " + e.getMessage());
			//logger.info("Try catch Exception:"+e.getMessage());
			logger.error(e.toString());
		}
	}
	
	/**
	 * ������ ������ ���Ѵ�.
	 * @return
	 */
	public Socket getSocket()
	{
		//CNCUtils.Logi(CLASSNAME, "getSocket()");
		Socket tcpSocket = null;
		
		try
		{
			tcpSocket = new Socket();
			//CNCUtils.Logi(CLASSNAME, "ip : [" + this.mIp + "], port : [" + this.mPort + "]");
			SocketAddress sa = new InetSocketAddress(mIp, Integer.parseInt(mPort));
			tcpSocket.connect(sa, CNC_CONNECTION_TIMEOUT);
		}catch(UnknownHostException e){
			statusChange("CNC_CONNECTION_EXCEPTION_UNKOWNHOST",false);
			//CNCUtils.Logw(CLASSNAME, "getSocket, UnknownHostException : " + e.getMessage());
			//e.printStackTrace();
			logger.error(e.toString());
		}
		catch(IOException ioe)
		{
			statusChange("CNC_CONNECTION_EXCEPTION_FAIL", false);
			//CNCUtils.Logw(CLASSNAME, "getSocket, IOException : " + ioe.getMessage());
			ioe.printStackTrace();
			logger.error(ioe.toString());
		}
		return tcpSocket;
	}
	
	/**
	 * �����޽����� �����Ѵ�.
	 * @return
	 */
	public String getmErrMessage()
	{
		return mErrMessage;
	}

	/**
	 * �������� ���θ� �����Ѵ�.
	 * @return
	 */
//	public boolean ismIsServerConnected()
//	{
//		return mIsServerConnected;
//	}

	/**
	 * �������ῡ ���� �޽����� ���� ���θ� �����Ѵ�.
	 * @param msg
	 * @param isConnection
	 */
	public void statusChange(String msg, boolean isConnection)
	{
		this.mErrMessage = msg;
		this.mIsServerConnected = isConnection;
	}
	
	/**
	 * COMMAND|CONTENT�� �̿��Ͽ� ������ �����͸� ����ϸ�, �̿� ���� response�� �����Ѵ�.
	 * @param cmd
	 * @param content
	 * @return
	 */
	public String request(int cmd, String content)
	{
		String inputContent = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		
		try
		{
			baos.write(StringUtils.Int2Byte(content.getBytes().length, cmd));
			baos.write(content.getBytes());
			buff = baos.toByteArray();
			
			// request
			mOutputStream.write(buff);
			mOutputStream.flush();
			
			// response
			mInputStream.read(mInputBuffer);
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			mHeaderContentSize = StringUtils.headerSizeToInt(mInputBuffer);
			mHeaderCommand = StringUtils.headerCommandToInt(mInputBuffer);
			
			//CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(mHeaderCommand));
			logger.info("Received header : " + CNCUtils.commandToString(mHeaderCommand));
			//CNCUtils.Logi(CLASSNAME, "Received content size : " + mHeaderContentSize);
			logger.info("Received content size : " + mHeaderContentSize);
			if(mHeaderContentSize != 0)
			{
				// Content Size��ŭ �о�´�.
				inputContent = StringUtils.Byte2String(mInputBuffer, mHeaderContentSize);
				//CNCUtils.Logi(CLASSNAME, "Received content : " + inputContent);
				logger.info("Received content : " + inputContent);
			}
		}
		catch (IOException e)
		{
			mErrMessage = "failed";
			//CNCUtils.Logi(CLASSNAME, "request, IOException.");
			//CNCUtils.Logi(CLASSNAME, "" + e.getMessage());
			logger.error(e.toString());
			return null;
		}
		return inputContent;
	}
	
	/**
	 * ��� ���� 1byte�� �о� �����Ѵ�.
	 * @param b byte buffer
	 * @param i content size
	 * @return
	 */
	public static final int getResultCode(byte[] b,int i)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		
		baos.write(b, 8, i);
		buff = baos.toByteArray();
		
		int j = (int)buff[0];
		return j;
	}
	
	public OutputStream getmOutputStream()
	{
		return mOutputStream;
	}

	public void setmOutputStream(OutputStream mOutputStream)
	{
		this.mOutputStream = mOutputStream;
	}

	public InputStream getmInputStream()
	{
		return mInputStream;
	}

	public void setmInputStream(InputStream mInputStream)
	{
		this.mInputStream = mInputStream;
	}
	
	/* #####################################################
	 * 
	 * 				JEFF Function's
	 * 
	 * #####################################################
	 */
	
	private static final int BUFFER_SIZE_HEADER = 8;
	private static final int BUFFER_SIZE_FILE_TRANS_RESPONSE_PARAM = BUFFER_SIZE_HEADER + 1;
	private static final int BUFFER_SIZE_FILE_TRANS_REPORT_PARAM = BUFFER_SIZE_HEADER + 5;
	
	/**
	 * ���α׷� ��/�ٿ�� �� ó�� <br/>
	 * �ƹ��� �ൿ�� ���� ���� �� Ÿ�Ӿƿ��� ���� �����̴�.
	 * @return
	 */
	public boolean ping()
	{
		// Header(8)
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		
		buffer.put(StringUtils.sendHeaderToByte(0, CMD_PING));
		try
		{
			mOutputStream.write(buffer.array());
			mOutputStream.flush();
			
			return true;
		}
		catch (IOException e)
		{
			CNCUtils.Loge(CLASSNAME, "ping(), IOException, " + e.getMessage());
		}
		return false;
	}
	
	/**
	 * ���α׷� ��/�ٿ�� �� ó�� <br/>
	 * �ƹ��� �ൿ�� ���� ���� �� Ÿ�Ӿƿ��� ���� �����̴�.
	 */
	public boolean pong()
	{
		// ����� �޴´�.
		byte[] received = new byte[8];
		
		int read = -1;
		try
		{
			read = mInputStream.read(received);
			int command = StringUtils.headerCommandToInt(received);
			if(read == 8 && command == CMD_PONG)
			{
				return true;
			}
		} 
		catch (IOException e)
		{
			CNCUtils.Loge(CLASSNAME, "pong(), IOException, " + e.getMessage());
		}
		return false;
	}
	
	/**
	 * @return
	 */
	public void downloadOk(int retCode, int size)
	{
		CNCUtils.Logi(CLASSNAME, "******************** SEND DOWNLOAD COMPLETE MESSAGE ********************");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;

		try
		{
			// request
			baos.write(StringUtils.sendHeaderToByte(5, CMD_FILETRANS_REPORT));
			baos.write(0x01);
			baos.write(size);
			buff = baos.toByteArray();
			
			mOutputStream.write(buff);
			mOutputStream.flush();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		CNCUtils.Logi(CLASSNAME, "************************************************************************");
	}
	
	/**
	 * UPLOAD/DOWNLOAD ��� �����Ѵ�. ������� ����
	 */
	public boolean updownReport(byte [] received)
	{
		CNCUtils.Logi(CLASSNAME, "******************** UPLOAD/DOWNLOAD REPORT ********************");
		boolean success = false;
		
		// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
		int size = StringUtils.headerSizeToInt(received);
		int command = StringUtils.headerCommandToInt(received);
		CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
		CNCUtils.Logi(CLASSNAME, "Received content size : " + size);
		
		if(size != 0)
		{
			byte[] content = readContent(received, size);
			if((int) content[0] == CMD_FILETRANS_RES_SUCCESS)
			{
				success = true;
			}
		}
		CNCUtils.Logi(CLASSNAME, "result: " + success); 
		CNCUtils.Logi(CLASSNAME, "****************************************************************");
		return success;
	}
	
	/**
	 * �ٿ�ε�� �����ϴ� �������̴�.
	 */
	public boolean sendTransPart(int result, int size)
	{
		// Header(8) + Result(1) + Size(4)
		ByteBuffer buffer = ByteBuffer.allocate(13);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		
		// request
		try
		{
			// 5 : Result(1) + Size(4)
			buffer.put(StringUtils.sendHeaderToByte(5, CMD_FILETRANS_REPORT));
			buffer.put((byte)result);
			buffer.putInt(size);
			
//			System.out.println("send: " + buffer.array().length);
			
			mOutputStream.write(buffer.array());
			mOutputStream.flush();
		} 
		catch (IOException e)
		{
			CNCUtils.Logw(CLASSNAME, "sendTransPart, IOException, " + e.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * ���ε�� ���Ϲ��� �������̴�.
	 * @return
	 */
	@SuppressWarnings("unused")
	public int getTransPart()
	{
		byte[] received = new byte[BUFFER_SIZE_FILE_TRANS_REPORT_PARAM];
		
		int read;
		try
		{
			read = mInputStream.read(received);
//			CNCUtils.Logi(CLASSNAME, "read : " + read);
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			int size = StringUtils.headerSizeToInt(received);
			int command = StringUtils.headerCommandToInt(received);
			
			if(size != 0)
			{
				byte[] content = readContent(received, size);
				
				return (int) content[0];
			}
		} 
		catch (IOException e)
		{
			CNCUtils.Loge(CLASSNAME, "getTransPart, " + e.getMessage());
		}
		return -1;
	}
	
	/**
	 * UPLOAD/DOWNLOAD ��� �����Ѵ�.
	 * @return
	 */
	public boolean updownResult()
	{
		CNCUtils.Logi(CLASSNAME, "******************** UPLOAD/DOWNLOAD REPORT ********************");
		boolean success = false;
		byte[] received = new byte[BUFFER_SIZE_FILE_TRANS_REPORT_PARAM];
		try
		{
			int read = mInputStream.read(received);
			CNCUtils.Logi(CLASSNAME, "read : " + read);
			
//			testPrint(received);
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			int size = StringUtils.headerSizeToInt(received);
			int command = StringUtils.headerCommandToInt(received);
			CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
			CNCUtils.Logi(CLASSNAME, "Received content size : " + size);
			
			if(size != 0)
			{
				byte[] content = readContent(received, size);
				if((int) content[0] == CMD_FILETRANS_RES_SUCCESS)
				{
					success = true;
				}
				else if((int) content[0] == CMD_FILETRANS_RES_FAIL_CRC)
				{
					CNCUtils.Logi(CLASSNAME, "CRC32 validation fail");
				}
			}
		}
		catch (IOException e)
		{
			CNCUtils.Logw(CLASSNAME, "updownResult, IOException");
			e.printStackTrace();
		}
		CNCUtils.Logi(CLASSNAME, "result: " + success); 
		CNCUtils.Logi(CLASSNAME, "****************************************************************");
		return success;
	}
	
	/**
	 * �����丮 �ٿ�ε� ���ɿ��� üũ
	 * @return
	 */
	public RepHistoryParam historyCheck()
	{
		CNCUtils.Logi(CLASSNAME, "******************** HISTORY DOWNLOAD CHECK ********************");
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.put(StringUtils.sendHeaderToByte(0, CMD_ALARM_HISTORY_REQ));
		buffer.rewind();
		
		// name(96) + size(4) + result(1)
		byte[] received = new byte[8 + 101];
		
		try
		{
			mOutputStream.write(buffer.array());
			mOutputStream.flush();
			
			int read = mInputStream.read(received);
			CNCUtils.Logi(CLASSNAME, "read : " + read);
			
			buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(received).rewind();
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			int size = buffer.getInt();
			int command = buffer.getInt();
			
			CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
			CNCUtils.Logi(CLASSNAME, "Received content size : " + size);
			
			if(command == CMD_ALARM_HISTORY_REP && size != 0)
			{
				RepHistoryParam rhp = new RepHistoryParam();
				
				byte [] name = new byte[96];
				buffer.get(name);
				
				rhp.setName(new String(name, "EUC-KR").trim());
				rhp.setSize(buffer.getInt());
				rhp.setResult((int) buffer.get());
				
				CNCUtils.Logi(CLASSNAME, "result : " + rhp.getResult());
				CNCUtils.Logi(CLASSNAME, "name : " + rhp.getName().trim());
				CNCUtils.Logi(CLASSNAME, "size : " + rhp.getSize());
				CNCUtils.Logi(CLASSNAME, "****************************************************************");
				return rhp;
			}
		}
		catch (IOException e)
		{
			CNCUtils.Loge(CLASSNAME, "IOException: " + e.getMessage());
//			e.printStackTrace();
		}
		CNCUtils.Logi(CLASSNAME, "****************************************************************");
		return null;
	}

	/**
	 * Upload/Download�� �������� Ȯ���Ѵ�.
	 */
	public int updown(ReqUpDownParam param)
	{
		CNCUtils.Logi(CLASSNAME, "******************** UPLOAD/DOWNLOAD CHECK ********************");
		ByteBuffer buffer = ByteBuffer.allocate(param.getMessageLength() + 8);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		
		int resCode = 0;
		byte[] received = new byte[BUFFER_SIZE_FILE_TRANS_RESPONSE_PARAM];
		
		try
		{
			CNCUtils.Logi(CLASSNAME, "Request header : " + CNCUtils.commandToString(CMD_FILETRANS_REQ));
			CNCUtils.Logi(CLASSNAME, "Request type : " + param.getType());
			CNCUtils.Logi(CLASSNAME, "Request name : " + param.getName());
			CNCUtils.Logi(CLASSNAME, "Request size : " + param.getSize());
			
			// request
			buffer.put(StringUtils.sendHeaderToByte(param.getMessageLength(), CMD_FILETRANS_REQ));
			buffer.put((byte)param.getType());
			
			// MTES�� ����� �� �ѱ��� ����ϱ� ���ؼ��� MS949�� ��ȯ�ؼ� ������ �Ѵ�.
			// FILE_TRNAS_REQUEST_PARAMETER�� NAME(96byte)�� ������ ������,
			// �ѱ۷� ��ȯ�� ��� byteũ�Ⱑ �޶����� ������ 96����Ʈ�� �ٽ� ������� �Ѵ�. 
			byte [] b = param.getName().getBytes("MS949");
			if(b.length > 96)
			{
				buffer.put(b, 0, 96);
			}
			else
			{
				buffer.put(b);
			}
			buffer.putInt(param.getSize());
			buffer.rewind();
			
//			testPrint(buffer.array());
			
			mOutputStream.write(buffer.array());
			mOutputStream.flush();
			
			int read = mInputStream.read(received);
			CNCUtils.Logi(CLASSNAME, "read : " + read);
			
			buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(received).rewind();
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			int size = buffer.getInt();
			int command = buffer.getInt();
			
			CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
			CNCUtils.Logi(CLASSNAME, "Received content size : " + size);
			
			if(size != 0)
			{
				// result code�� �д´�.
				resCode = (int) buffer.get();
			}
		}
		catch(Exception e)
		{
			CNCUtils.Loge(CLASSNAME, "Exception: " + e.getMessage());
			resCode = -1;
//			e.printStackTrace();
		}
		finally
		{
			buffer.clear();
		}
		CNCUtils.Logi(CLASSNAME, "content: " + CNCUtils.fileTransCommandToString(resCode));
		CNCUtils.Logi(CLASSNAME, "***************************************************************");
		return resCode;
	}
	
	/**
	 * CNC ���� ����Ʈ�� �����Ѵ�.
	 * @return ���� ����Ʈ
	 */
	public List<CncFile> cnc()
	{
		CNCUtils.Logi(CLASSNAME, "******************** CNC File's ********************");

		List<CncFile> files = new ArrayList<CncFile>();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		byte[] received = new byte[DEFAULT_BUFFER_SIZE];
		
		try
		{
			// request
			baos.write(StringUtils.sendHeaderToByte(0, CMD_FILELIST_REQ));
			buff = baos.toByteArray();
			
			mOutputStream.write(buff);
			mOutputStream.flush();
			
			int read = mInputStream.read(received);
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			int size = StringUtils.headerSizeToInt(received);
			int command = StringUtils.headerCommandToInt(received);
			
			CNCUtils.Logi(CLASSNAME, "read : " + read);
			CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
			CNCUtils.Logi(CLASSNAME, "Received content size : " + size);
			
			if(size != 0)
			{
				// Content size��ŭ �о�´�.
				byte [] content = readContent(received, size);
				String strContent = CNCUtils.encoding(content);
                //String strContent = CNCUtils.unicodeEncoding(content);				
				CNCUtils.Logi(CLASSNAME, "content: " + strContent);
				String [] split = strContent.split(":");
				for(int i=0; i<split.length; i=i+2)
				{
					files.add(new CncFile(split[i], Long.parseLong(split[i+1])));
					CNCUtils.Logi(CLASSNAME, "" + split[i] + " (" + split[i+1] + ")");
				}
			}
		}
		catch(Exception e)
		{
			CNCUtils.Logi(CLASSNAME, "return null");
			CNCUtils.Logi(CLASSNAME, "****************************************************");
			return null;
		}
		CNCUtils.Logi(CLASSNAME, "****************************************************");
		return files;
	}
	
	/**
	 * �ǽð����� MTES MemoryMap ������ �޾ƿ´�.
	 */
	public MemoryMap getDataMap(ReqStatusParam param)
	{
		MemoryMap map = null;
		ByteArrayBuffer br = new ByteArrayBuffer(STATUS_INFO_BUFFER_SIZE);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		
		try
		{
			// request
			// 8 (path(int) + req_type(4))
			baos.write(StringUtils.sendHeaderToByte(param.getMessageLength(), CMD_STATUSINFO_REQ));
			baos.write(param.getPath());
			baos.write(param.getReq_type());
			buff = baos.toByteArray();

			mOutputStream.write(buff);
			mOutputStream.flush();
			
			int size = 0;
			int command;
			byte [] temp = new byte[STATUS_INFO_BUFFER_SIZE];
			boolean first = true;
			
			while(isConn)
			{
				int read = mInputStream.read(temp);
				//logger.info("read :"+read);
				if(read == -1){
					break;
				}
				br.append(temp, 0, read);
				//logger.info("size :"+size);
				if(first)
				{
					size = StringUtils.headerSizeToInt(temp);
					command = StringUtils.headerCommandToInt(temp);

					first = false;
//					CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
//					CNCUtils.Logi(CLASSNAME, "Received content size : " + size);
				}
				
				// buffer�� ��� ������ �����Ѵ�.
				// size + 8(header)
				if(br.length() >= size + 8)
				{
					break;
				}
			}
			logger.info("Received buff size : " + br.length());
			if(size != 0){
				byte [] received = readContent(br.toByteArray(), size);
				map = new MemoryMap();
				map.reload(received, size);
			}
		}catch (IOException e){
			logger.error(e.toString());
			return null;
		}catch (NullPointerException e2){
			logger.error(e2.toString());
			return null;
		}finally{
			br.clear();
		}
		
		return map;
	}
	
	/**
	 * �α׾ƿ� ��û
	 * @param request
	 */
	public void logout(ReqLogoutParam param)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		try
		{
			baos.write(StringUtils.sendHeaderToByte(param.getMessageLength(), CMD_LOGOUT_REQ));
			baos.write(param.getAccount().getBytes());
			baos.write(param.getPasswd().getBytes());
			buff = baos.toByteArray();
			
			// request
			mOutputStream.write(buff);
			mOutputStream.flush();
		}
		catch(Exception e)
		{
		}
	}
	
	/**
	 * MTES ���� �α��� ��û�� ���� ��� �����Ѵ�.
	 */
	public RepLoginParam login(ReqLoginParam param)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		byte[] received = new byte[DEFAULT_BUFFER_SIZE];
		try
		{
			baos.write(StringUtils.sendHeaderToByte(param.getMessageLength(), CMD_LOGIN_REQ));
			baos.write(param.getAccount().getBytes());
			baos.write(param.getPasswd().getBytes());
			baos.write((byte)param.getDevice_type());
			baos.write(param.getReg_token().getBytes());
			buff = baos.toByteArray();
			
			// request
			mOutputStream.write(buff);
			mOutputStream.flush();
			
			// response
			mInputStream.read(received);
			
			// ���ŵ� �����Ϳ��� ���� 8����Ʈ(���)�� �д´�.
			int size = StringUtils.headerSizeToInt(received);
			int command = StringUtils.headerCommandToInt(received);
			
			CNCUtils.Logi(CLASSNAME, "Received header : " + CNCUtils.commandToString(command));
			CNCUtils.Logi(CLASSNAME, "Received content size : " + size);

			if(size != 0)
			{
				// Content size��ŭ �о�´�.
				byte [] content = readContent(received, size);
				int polling = StringUtils.byteToInt(content);
				int path_num = (int)content[4];
				int result = (int)content[5];
				
				ByteBuffer buffer = ByteBuffer.wrap(content);
                
                String hmiVer = "";
                String mtesVer = "";
                System.out.println("1polling: " + polling + ", result: " + result + ", path_num: " + path_num);
                System.out.println("1hmiVer: [" + hmiVer + "], mtesVer: [" + mtesVer + "]");
				
				System.out.println("polling: " + polling + ", result: " + result + ", path_num: " + path_num);
				System.out.println("hmiVer: [" + hmiVer + "], mtesVer: [" + mtesVer + "]");
				
				RepLoginParam rep = new RepLoginParam();
				rep.setPolling(polling);
				rep.setPath_num(path_num);
				rep.setResult(result);

				return rep;
			}
		}
		catch (IOException e)
		{
			CNCUtils.Logi(CLASSNAME, "login(), " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param b
	 * @param i
	 * @return
	 */
	public byte[] readContent(byte[] b,int size)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buff = null;
		
		baos.write(b, MTES_HEADER_SIZE, size);
		buff = baos.toByteArray();
		
//		testPrint2(buff);
		
		return buff;
	}
	
	/**
	 * �׽�Ʈ�ڵ�
	 * ������ �޽����� �����͸� ����Ѵ�.
	 */
	public void testPrint(byte[] buff)
	{
		int ic = 0;
		for (byte b: buff)
		{
			System.out.println("byte[" + ic + "] "+b);
			ic++;
		}
	}
	
	/**
	 * �׽�Ʈ�ڵ�
	 * ������ �޽����� �����͸� ���̸�ŭ ����Ѵ�.
	 */
	public void testPrint2(byte[] buff)
	{
		int start = 0;
		int stop = 9999;
		
		int ic = 0;
		for (byte b: buff)
		{
			if(ic >= start && ic <= stop)
			{
				System.out.println("byte[" + ic + "] "+ b);
			}
			ic++;
		}
	}

	public void setConn(boolean isConn)
	{
		this.isConn = isConn;
	}

	public boolean getConn()
	{
		return isConn;
	}

	public Socket getmTcpSocket()
	{
		return mTcpSocket;
	}

	public void setmTcpSocket(Socket mTcpSocket)
	{
		this.mTcpSocket = mTcpSocket;
	}
}
	