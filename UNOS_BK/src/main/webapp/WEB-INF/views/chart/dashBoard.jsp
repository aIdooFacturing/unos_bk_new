<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<%
	response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
	response.setHeader("pragma", "no-cache"); // HTTP 1.0
	response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">

<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>iDOO Control</title>
<style type="text/css">
body {
	background-color: black;
}

#detailPopup{
	background: rgb(38,39,43);
}
.k-window-titlebar.k-dialog-titlebar.k-header{
	background: rgb(38,39,43);
	color: white;
}
#grid thead tr th {
	text-align: center;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">


<script src="${ctxPath }/js/dashboard.js"></script>

<script>

var kendotable;

$(function(){
	// idoocontrolImg click event => layout update page
	$("#idooControlImg").dblclick(function(){
		location.href = ctxPath + "/chart/layout-Setting.do"
	})
	
	//clicking on tv picture open a popup
	$("#detailPopup").kendoDialog({
// 		scrollable: true
// 		,
		height: getElSize(1650)
		,width: getElSize(1600)
		,title: "TV LIST"
	 	/* ,content: "<table border=1 style='color:black;'> <tr> <th> Tool Cycle Count </th> <th> RunTime (h) </th> </tr>" +
	 			 "<tr> <td> <input id='preCnt' type='number' value=0> </td> <td>  <input id='preSec' type='number' value=0> </td> </tr> </table>"
		 */,actions: [{
			text: "OK"
			,action: function(e){
			//	alert("popup")
			},
			primary: true
		}]
	});
	
	$(".k-window-title.k-dialog-title").append("<button id='ALL' class='btn' onclick='getTable(this)'> 전체 보기 </button>")
	$(".k-window-title.k-dialog-title").append("<button class='btn' onclick='saveRow(this)'> 저장 </button>")
	
	$(".btn").css({
		"margin-left" : getElSize(50),
		"font-size" : getElSize(55),
	    "border-radius": getElSize(8),
	    "border-color": "black",
	    "color": "#515967",
	    "background-color": "gray",
	    "background-position": "50% 50%",
	    "cursor" : "pointer",
	    "color" : "black",
	    "font-weight" : "bold"
	})
	
	$(".btn").hover(
		// hover in
		function () {
			$(this).css({
			    "background-color": "#6699F0",
			})
		},
		// hover out
		function () {
			$(this).css({
			    "background-color": "gray",
			})
		}
	);

	
	//kendo grid table
	gridTable();
	
	$("#detailPopup").data("kendoDialog").close();
	
	// tvImg Create
	imgCreate()
})

function saveRow(){
	console.log("==save==")
	var savelist = kendotable.dataSource.data();

	var obj = new Object();
	obj.val = savelist;

	var url = "${ctxPath}/chart/getTvListSave.do";
	
	var param = "val=" + JSON.stringify(obj);

	console.log(savelist);
	console.log(param);

	$.showLoading()
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			$.hideLoading()
			if(data=="success"){
				
			}else{
				alert("저장 실패하였습니다.")
			}
		},error : function(data){
			$.hideLoading();
			alert("관리자에게 문의하세요_0")
		}
	})

}

function gridTable(){
	kendotable = $("#grid").kendoGrid({
		height : $("#detailPopup").height()
		,editable : false
		,dataBound:function(){
	        grid = this;
	        grid.tbody.find('tr').each(function(){
		        var item = grid.dataItem(this);
	            kendo.bind(this,item);
        	})
	    }
		,filterable: {
			mode: "row"
		}
		,columns : [{
			field : "dvcId"
			,title : "장비 번호"
			,width : getElSize(280)
		},{
			field : "name"
			,title : "장비명"
			,width : getElSize(520)
		},{
			field : "line"
			,title : "TV Num"
			,template : "<select data-role='number' style='width:50%; text-align:center'  data-bind='value:line'>" + optionMenu + "</select>"
// 			,template:'<input type="number" min="0" data-role="number" style="width:100%;"  data-bind="value:line"/>'
			,width : getElSize(150)
			,filterable: {
				cell: {
					enabled: false
				}
			}
		}]
	}).data("kendoGrid")
}
var optionMenu = "<option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>"


function imgCreate(){
	

	var url = "${ctxPath}/chart/getTvList.do";
			
	var param = "shopId=" + shopId;

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;
			console.log(json)
			
			$(json).each(function(idx,data){
				tv = '<span ondblclick="getTable(this)" id=' + data.idx +' class="k-icon k-i-toggle-full-screen-mode" style="color: dodgerblue; z-index:2; cursor:pointer; position:absolute; top:' + (getElSize(data.y) + marginHeight) + ';left:' + (getElSize(data.x) + marginWidth)+ '; font-size: ' + getElSize(55) +';"> abcd </span>'
				tv += "<br><label style='color: cornflowerblue; z-index:2; position:absolute; top:" + (getElSize(data.y) + marginHeight) + ";left:" + (getElSize(data.x) - getElSize(30) + marginWidth) + "; font-size: " + getElSize(40) +";'>" + data.idx + ".</label>"
				$("body").append(tv);
			})
		}
	})

	

}


function getTable(e){
	var line = e.id;
	

	var url = "${ctxPath}/chart/getTvShowList.do";
			
	var param = "line=" + line;
	
	$.showLoading()
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$.hideLoading()
			var json = data.dataList;
			console.log(json);
			
			$(json).each(function(idx,data){
				data.name = decode(data.name)
			})
			
			kendodata = new kendo.data.DataSource({
				data: json,
				batch: true,
				group: [{
					field: "line"
				}],
				/* sort: [{
                	field: "prdNo" , dir:"asc" 
                },{
                	field: "item" , dir:"asc"
                }], */
				schema: {
					model: {
						id: "idx",
						fields: {
						}
					}
				}
			});
			
			kendotable.setDataSource(kendodata);
			
			$("#detailPopup").data("kendoDialog").open();
		}
	})
	
// 	aa = $(e)
// 	console.log(e)
}
</script>

</head>
<body>
	<div id="container">
		<div id="svg"></div>
	</div>
	<div id="detailPopup">
		<div id="grid" style=" width: 100%;">
		</div>
	</div>
</body>
</html>
