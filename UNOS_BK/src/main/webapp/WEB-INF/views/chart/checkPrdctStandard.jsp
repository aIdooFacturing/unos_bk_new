<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}

#grid{
	border-bottom : 1px solid #1E1E23  !important;
	border-right : 1px solid #1E1E23 !important;
	border-left : 1px solid #1E1E23  !important;
	border-top : 1px solid #1E1E23 !important;
}

.k-grid-header-wrap, .k-grid-header-locked{
	border-left : 1px solid #1E1E23 !important;
	border-right : 1px solid #1E1E23 !important;
}

.k-grid-content, .k-grid-content-locked{
	border-left : 1px solid #1E1E23 !important;
	border-right : 1px solid #1E1E23 !important;
}
.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}

.k-grid thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid-content table tbody tr
{
 background : #DCDCDC  !important;
 color : black !important;
 text-align: center;
}
.k-grid-content-locked table tbody tr
{
 background : #F0F0F0 !important;
 color : black !important;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
  }
  
button:hover{
	background: #6699F0 !important;
	border-color : #6699F0 !important;
}

#wrapper tbody tr{
	background: #2B2D32;
	border-color : #2B2D32;
    color: white;
}

.k-grid td{
    white-space: nowrap;
    text-overflow: ellipsis;
}

</style> 
<script type="text/javascript">

const loadPage = () =>{
	createMenuTree("qm","checkPrdctStandard")
}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};
	
	$(function(){
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};

	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(40)	
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		
		$("button").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8),
			"cursor" : "pointer"
		})
		/* 
		$("#search").css({
			"margin-top" : getElSize(24)
		}) */
		
		$("#banner").css({
			"font-size" : getElSize(48),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#grid").css({
			"width" : $("#container").width() - getElSize(130)
		})
		$(".k-grid td").css({
		    "white-space": "nowrap",
		    "text-overflow": "ellipsis"
		})
		
		$("#wrapper").css({
			"width" : $("#container").width() - getElSize(90),
			"color" : "black",
			"border" : getElSize(1)+"px solid #1E1E23",
			"display" : "block",
			"table-layout": "fixed",
		})
		
		
		$("#wrapper tbody tr td").css({
		    "height" : getElSize(128),
		    "padding-top" : getElSize(20),
			"padding-left" : getElSize(20),
			"padding-right" : getElSize(20),
			"padding-bottom" : getElSize(20)
			
		})
		
		$("select").css({
			"font-size" : getElSize(48),
			"width" : getElSize(640),
			"height" : getElSize(80),
			"border": "1px black #999",
           "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
		})
			
	};
	
	//tab 기능
	function onGridKeydown(e) {
	    if (e.keyCode === kendo.keys.TAB) {
	        var grid = $(this).closest("[data-role=grid]").data("kendoGrid");
	        var current = grid.current();
	        
	        if (!current.hasClass("table-cell")) {
	          console.log(1)
	            var nextCell;
	            if (e.shiftKey) {
	                nextCell = current.prevAll(".table-cell");
	                if (!nextCell[0]) {
	                    //search the next row
	                    var prevRow = current.parent().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev();
	                    var nextCell = prevRow.children(".table-cell:last");
	                }
	            } else {
	                nextCell = current.nextAll(".table-cell");
	                if (!nextCell[0]) {
	                    //search the next row
	                    var nextRow = current.parent().next();
	                    var nextCell = nextRow.children(".table-cell:first");
	                }
	            }
	            
/* 	            console.log(nextCell)
	            console.log(nextCell[0]) */
	            grid.current(nextCell);
	            grid.editCell(nextCell[0]);
	        }
	
	    }
	};  
	//품번
	function getGroup(){
		var url = "${ctxPath}/common/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var option;
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#prdNo").html(option).val(json[0].prdNo).change(getDvcListByPrdNo);
				
				//getLeadTime();
				getDvcListByPrdNo();
			}
		});
	};
	//장비
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var option;
				console.log("여기로옴?")
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
				});
				
				if(json.length==0){
					option += "<option value='undefined'>장비 없음</option>";
					$("#dvcId").html(option).val("undefined")
				}   
				
				if(json.length!=0)
				$("#dvcId").html(option).val(json[0].dvcId);
			}
		});
	};
	
	var groupcode=[]
	function getGroupCode(){
		var url = "${ctxPath}/chart/getJSGroupCode.do";
		
		var options = "";
		$.ajax({
			url : url,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				
				$(data.dataList).each(function(idx,data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					var arr={};
					arr.value=data.id;
					arr.text=decode(data.name);
					groupcode.push(arr)
				});
				
				JSGroupCode = "<select>" + options + "</select>";
			}
		});
		
		return options;
	};
	
	function viewChkTy(chkTy){
		for(i=0 ,len=chkTypeList.length; i<len; i++){
			if(chkTypeList[i].value==chkTy){
				return chkTypeList[i].text
			}
		}
	}
	function viewGroupCd(name,attrTy){
		if(attrTy==1){
			for(i=0 ,len=groupcode.length; i<len; i++){
				if(groupcode[i].value==name){
					return groupcode[i].text
				}
			}
		}else{
			return ""
		}
	}
	var chkTypeList=[]
	//정량검사
	
/* 	var attrTyList = "<select onchange='changeAttrTy(this)' style='font-size : " + getElSize(40) + "'>" + 
	"<option value='1'>정성검사</option>" +
	"<option value='2'>정량검사</option>" + 
	"</select>"; */

	var attrTypeList=[{value : "1", text : "정성검사"},{value : "2", text : "정량검사"}]
	//검사유형
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.id + "' >" + decode(data.codeName) + "</option>"; 
					var arr={}
					arr.value=data.id;
					arr.text=decode(data.codeName);
					chkTypeList.push(arr);
				});
				

				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};
	
	function chkTyList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "text",
			 dataValueField  : "value",
			 dataSource: chkTypeList,
		 }).data("kendoDropDownList");
	}
	function attrTyList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "text",
			 dataValueField  : "value",
			 dataSource: attrTypeList,
			 change: function(e){
				 console.log("chagne")
				 console.log(options.model)
				 console.log(options.model.attrTy)
				 console.log(options.model.firstCd)
				 if(options.model.attrTy==1){
					 options.model.firstCd="L"
					/*  options.model.dp=""
					 options.model.unit=""
					 options.model.target=""
					 options.model.low=""
					 options.model.up="" */
				 }else if(options.model.attrTy==2){
					 options.model.firstCd="N"
				 }
				 $("#grid").data("kendoGrid").dataSource.fetch()

			 }
		 }).data("kendoDropDownList");
	}
	function jsTyList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "text",
			 dataValueField  : "value",
			 dataSource: groupcode,
		 }).data("kendoDropDownList");
	}
	function inputChk(container, options){
//		container.removeClass("k-edit-cell");
//		container.text(options.model.get(options.field));
		if(options.attrTy==1){
			container.removeClass("k-edit-cell");
			container.text(options.model.get(options.field));
		}
//		container.addClass("editable-cell");

//		container.attr('class','editable-cell');

	}
	function insertRow(){
		var grid = $("#grid").data("kendoGrid")
		grid.dataSource.insert(0, {
			prdNo: $("#prdNo").val()
			,id:0
			,name: $("#dvcId option:selected").html()
			,chkTy: 2
			,attrTy: 1
			,firstCd: "L"
			,attrCd: "11"
			,unit: ""
			,target: ""
			,spec:""
			,up: ""
			,low: ""
			,dp: ""
			,measurer: ""
			,jsGroupCd: "11000000"
			,attrNameOthr: "null"
		})
		 
		$('.k-grid table tbody tr:eq(0) td:eq(5)').trigger('click');
		$("div.k-grid-content").scrollTop(0);
	}
	
	
	function saveRow(){
		if($("#dvcId option:selected").html()==undefined){
			alert("장비를 선택해주세요")
			return
		}
		
		console.log("---save---")
		var gridlist = $("#grid").data("kendoGrid").dataSource.data();
		//조건문
		for(i=0, len=gridlist.length; i<len; i++){
			console.log("== 변경전 ==")
			console.log(gridlist[i].target)
			console.log(gridlist[i].up)
			console.log(gridlist[i].low)
			console.log("===============")
			
// 			console.log(gridlist[i].attrNameKo)
			if(gridlist[i].secondCd==undefined || gridlist[i].secondCd=="undefined" || gridlist[i].secondCd==""){
				alert("특성코드를 입력해주세요")
				$(".k-grid-content-locked table tbody tr:eq("+i+") td:eq(5)").trigger("click")
				return;
			}else if(gridlist[i].attrNameKo==undefined || gridlist[i].attrNameKo=="undefined"){
				alert("특성명을 입력해주세요")
				$(".k-grid-content table tbody tr:eq("+i+") td:eq(0)").trigger("click")
				return;
			}
			
			if(gridlist[i].attrTy==2){
				if(gridlist[i].target==undefined || (gridlist[i].target=="" && gridlist[i].target==="")){
					gridlist[i].target="";
// 11					alert("목표값을 입력해주세요")
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(4)").trigger("click")

// 11					return;
				}
				if(gridlist[i].low==undefined || (gridlist[i].low=="" && gridlist[i].low==="")){
					gridlist[i].low="";
// 11					alert("하한값을 입력해주세요")
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(5)").trigger("click")

// 11					return;
				}
				if(gridlist[i].up==undefined || (gridlist[i].up=="" && gridlist[i].up==="")){
					gridlist[i].up="";
// 11					alert("상한값을 입력해주세요")
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(6)").trigger("click")

// 11					return;
				}
				if(gridlist[i].dp==undefined || (gridlist[i].dp=="" && gridlist[i].dp==="")){
					alert("소수점을 입력해주세요")
					$(".k-grid-content table tbody tr:eq("+i+") td:eq(1)").trigger("click")

					return;
				} 
				console.log("== 변경후 저장할 값들 ==")
				console.log(gridlist[i].target)
				console.log(gridlist[i].up)
				console.log(gridlist[i].low)
				console.log("===============")
				var target = gridlist[i].target.toString();
				
				var min = gridlist[i].low.toString();
				var max = gridlist[i].up.toString();
				var dp = Number(gridlist[i].dp);
// 11				if(min.lastIndexOf(".")==-1){
// 11					alert("소수점 자리가 맞지 않습니다.");
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(5)").trigger("click")
// 11					valid = false;
// 11					return;
// 11				};
				var min_dp_length = min.substr(min.lastIndexOf(".")+1).length;
// 				console.log(dp)
// 				console.log(min_dp_length)
// 				console.log(dp==min_dp_length)
				/* if(dp!=min_dp_length){
					alert("소수점 자리가 맞지 않습니다.");
					$(".k-grid-content table tbody tr:eq("+i+") td:eq(5)").trigger("click")
					valid = false;
					return;
				}
				
				if(max.lastIndexOf(".")==-1){
					alert("소수점 자리가 맞지 않습니다.");
					$(".k-grid-content table tbody tr:eq("+i+") td:eq(6)").trigger("click")
					valid = false;
					return;
				};
				
				var max_dp_length = max.substr(max.lastIndexOf(".")+1).length;
				if(dp!=max_dp_length){
					alert("소수점 자리가 맞지 않습니다.");
					$(".k-grid-content table tbody tr:eq("+i+") td:eq(6)").trigger("click")
					valid = false;
					return;
				};
				
				if(target.lastIndexOf(".")==-1){
					alert("소수점 자리가 맞지 않습니다.");
					$(".k-grid-content table tbody tr:eq("+i+") td:eq(4)").trigger("click")
					valid = false;
					return;
				};
				
				var target_dp_length = target.substr(target.lastIndexOf(".")+1).length;
				if(dp!=target_dp_length){
					alert("소수점 자리가 맞지 않습니다.");
					$(".k-grid-content table tbody tr:eq("+i+") td:eq(4)").trigger("click")
					valid = false;
					return;
				}; */
				
// 11				if(Number(target) > Number(max)){
// 11					alert("목표 값이 상한 값보다 큽니다.");
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(4)").trigger("click")
// 11					valid = false;
// 11					return;
// 11				}else if(Number(target) < Number(min)){
// 11					alert("목표 값이 하한 값보다 작습니다.");
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(4)").trigger("click")
// 11					valid = false;
// 11					return;
// 11				}else if(Number(max) < Number(min)){
// 11					alert("상한 값이 하한 값보다 작습니다.");
// 11					$(".k-grid-content table tbody tr:eq("+i+") td:eq(6)").trigger("click")
// 11					valid = false;
// 11					return;
// 11				}	
			}
			
			if(gridlist[i].target==undefined || gridlist[i].target==null){
				gridlist[i].target=""
			}
			if(gridlist[i].low==undefined || gridlist[i].low==null){
				console.log("전 ::"+gridlist[i].low)
				gridlist[i].low=""
				console.log("후 ::"+gridlist[i].low)
			}
			if(gridlist[i].up==undefined || gridlist[i].up==null){
				gridlist[i].up=""
			}
			if(gridlist[i].dp==undefined || gridlist[i].dp==null){
				gridlist[i].dp=""
			}

		/* 	console.log(gridlist[i].target)
			console.log(gridlist[i].low)
			console.log(gridlist[i].up)
			console.log(gridlist[i].dp) */
		}
		//변경필요한값
		for(i=0, len=gridlist.length; i<len; i++){
			gridlist[i].attrCd=gridlist[i].firstCd+gridlist[i].secondCd
			gridlist[i].attrNameKo=encodeURIComponent(gridlist[i].attrNameKo);
			
			//품번 장비 변경했을시 변경한 값으로 복사하기 위해서
			//service 에서 id 값이 있으면 update
			//없으면 insert 이므로 id값 없애기
			if($("#dvcId option:selected").text()!=gridlist[i].name || $("#prdNo").val()!=gridlist[i].prdNo){
// 				console.log("wwwww")
				gridlist[i].id = 0
			}
// 			gridlist = p
			//name 값 저장하기
			if($("#dvcId").val()=="undefined"){
				gridlist[i].dvcId=0;
				gridlist[i].name="undefined";
				gridlist[i].prdNo=$("#prdNo").val();
			}else{
				gridlist[i].dvcId = $("#dvcId").val();
				gridlist[i].name = $("#dvcId option:selected").text();
				gridlist[i].prdNo=$("#prdNo").val();
			}
		}

		var obj = new Object();
		obj.val = gridlist;
		
		var param = JSON.stringify(obj);
// 		console.log(param)
// 		return;
		var url = "${ctxPath}/chart/checkPrdctStandardSave.do"
		$.showLoading()
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				for(i=0, len=gridlist.length; i<len; i++){
					gridlist[i].attrNameKo=decodeURI(gridlist[i].attrNameKo)
				}
				console.log(data)
				if(data=="success"){
					getTable()
//					decodeURI("%EC%8B%A4%EB%A6%B0%EB%8E%8C%20%EB%82%B4%EA%B2%BD(%EC%83%81)")
				}else{
					alert("error 발생");
				}
				$.hideLoading()
//				getTable();
			}
		})
		
	}

	function deleteRow(id,row){
		if(id!=0){
			if(confirm("삭제하시겠습니까?")){
				var url = ctxPath + "/chart/delChkStandard.do";
				var param = "id=" + id;
				$.showLoading()				
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "text",
					success : function(data){
						if(data=="success"){
							$.hideLoading()
						}
					}
				}); 
			}else{
				return
			}

		}
		var dataItem = grid.dataSource.getByUid(row.closest("tr").dataset.uid);
		grid.dataSource.remove(dataItem)

	}
	function demicalNumber(container, options){
		console.log(options.model.dp)
		$('<input name="' + options.field + '"/>')
	     .appendTo(container)
	     .kendoNumericTextBox({
	         decimals: options.model.dp
	         ,restrictDecimals:true
	         ,format:"n"+options.model.dp
	         ,spinners: false
	     })
	}
	
	
	var kendotable;
	var asd 
	$(document).ready(function(){
		
		getGroup();
		getGroupCode();
		
		$("#chkTy").html("<option value='ALL'>${total}</option>" + getCheckType());
		
		kendotable = $("#grid").kendoGrid({
			dataBound : function(e){
				var grid=$("#grid").data("kendoGrid")
				$(".k-grid-content table tbody tr td input").css({
					"height":getElSize(50)
					,"width":getElSize(100)
					,"margin":0
					,"padding":0
					,"font-size" : getElSize(36)
					,"border-color" : "#9B9B9B"
					,"background" : "#9B9B9B"
				})
				
// 				$(".k-grid-content-locked").css({
// 					"height" : getElSize(1470),
// 					"background" : "#282828"
// 				})
				
// 				$(".k-grid-content").css({
// 					"height" : getElSize(1470)
// 				})
				
				$(".k-grid-header-locked table thead").css({
					"height" : getElSize(132)
				})
				
				$(".k-grid-header-wrap table thead tr").css({
					"height" : getElSize(132)
				})
				
				$(".k-grid-header-locked table thead tr").css({
					"height" : getElSize(66)
				})
				$(".k-grid-header-locked table thead tr th").css({
					"font-size" : getElSize(36),
					
				})
				$(".k-grid-header-wrap table thead tr th").css({
					"font-size" : getElSize(36),
					"padding" : getElSize(40),
				})
				$(".k-grid-content-locked table tbody tr").css({
					
					"height" : getElSize(96)
				})
				$(".k-grid-content table tbody tr").css({
					
					"height" : getElSize(96)
				})
				$(".k-grid-content-locked table tbody tr td").css({
					"font-size" : getElSize(36),
					
				})
				$(".k-grid-content table tbody tr td").css({
					"font-size" : getElSize(36),
				
				})
				
				this.tbody.find('tr').each(function() {
			    	var item = grid.dataItem(this);
		    		asd = $(this).context.children
			    	if(item.attrTy==2){
			    		$(this).context.children
			    		console.log($(this).context.children)
			    		$(this).context.children[1].className="table-cell"
			    		$(this).context.children[2].className="table-cell"
			    		$(this).context.children[4].className="table-cell"
			    		$(this).context.children[5].className="table-cell"
			    		$(this).context.children[6].className="table-cell"
			    		$(this).context.children[8].className=""
			    	}
			    	if(item.attrTy==1){
			    		$(this).context.children[8].className="table-cell"
			    	}
			    })
/* 	    	    $('td').each(function(){
//					console.log($(this).text())
	    	    	if($(this).text()=='램프부 외경'){
	    	    		console.log($(this))
		    			$(this).addClass('table-cell')

//		    			var grid = $("#wrapper").data("kendoGrid");
						var col = $(this).closest("td");
		//    			console.log($(this))
		    		}
			    	if($(this).text()=='미배치'){
		    			$(this).addClass('routing')
		//    			console.log($(this))
		    		}
	    	    }) */
			    
			}
			,navigatable: true
			,editable : true
			,height : getElSize(1600)
			,columns : [{
				title : "${prd_no}"
				,field : "prdNo"
				,width : getElSize(300)
				,locked: true
				,lockable: false
			},{
				title : "${device}"
				,field : "name"
				,width : getElSize(300)
				,locked: true
				,lockable: false
			},{
				title : "${check_type}"
				,field : "chkTy"
				,width : getElSize(200)
				,editor : chkTyList
				,locked: true
				,lockable: false
				,template: "#=viewChkTy(chkTy)#"
				,attributes: {
				      "class": "table-cell"
				 }
//				,template : '<input data-role="dropdownlist" data-source= "chkTypeList" data-text-field="text" data-value-field="value" data-bind="value: chkTy">'
			},{
				title : "${character_type}"
				,field : "attrTy"
				,editor : attrTyList
				,width : getElSize(200)
				,locked: true
				,lockable: false
				,template:kendo.template("#if (attrTy == 1) {# #='정성검사'##} else if(attrTy == 2){# #='정량검사'##} #")
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "${character_cd}"
				,locked: true
				,lockable: false
				,columns:[{
					title :" "
					,field : "firstCd"
					,width : getElSize(100)
					,editable : false
					,editor : function(container, options){
						console.log("fetch")
						container.removeClass("k-edit-cell");
						container.text(options.model.get(options.field));
					}
				},{
					title :" "
					,field : "secondCd"
					,width : getElSize(200)
					,attributes: {
					      "class": "table-cell"
					 }
				}]	
			},{
				width : getElSize(50)
				,locked: true
				,lockable: false
				,attributes: {
			    	  style: "background-color:gray; "
				 }
			}/* ,{
				title : "${character_cd}"
				,field : "attrCd"
				,width : getElSize(300)
				,locked: true
				,lockable: false
				,attributes: {
				      "class": "table-cell"
				 }
			} */,{
				title : "${character_name}"
				,field : "attrNameKo"
				,width : getElSize(600)
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "${dp}"
				,field : "dp"
				,width : getElSize(200)
				,editor: demicalNumber
				,editable: function (dataItem) {
					return dataItem.attrTy === "2";
		        }
				,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=dp##} #")

//				,editor : inputChk
			},{
				title : "${unit}"
				,field : "unit"
				,width : getElSize(200)
				,editable: function (dataItem) {
					return dataItem.attrTy === "2";
		        }
				,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=unit##} #")
				//,editor : inputChk
			},{
				title : "${drawing}"
				,field : "spec"
				,width : getElSize(400)
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "${target_val}"
				,field : "target"
				,editor: demicalNumber
				,width : getElSize(200)
				,editable: function (dataItem) {
					return dataItem.attrTy === "2";
		        }
				,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=kendo.toString(target,'n'+dp)# #} #")
			},{
				title : "${min_val}"
				,field : "low"
				,editor: demicalNumber
				,width : getElSize(200)
				,editable: function (dataItem) {
					return dataItem.attrTy === "2";
		        }
				,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=kendo.toString(low,'n'+dp)# #} #")
		},{
				title : "${max_val}"
				,field : "up"
				,editor: demicalNumber
				,width : getElSize(200)
				,editable: function (dataItem) {
					return dataItem.attrTy === "2";
		        }
				,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=kendo.toString(up,'n'+dp)# #} #")
		},{
				title : "${measurer}"
				,field : "measurer"
				,width : getElSize(350)
				,attributes: {
				      "class": "table-cell"
				 }
			},{
				title : "${js_group_cd}"
				,field : "jsGroupCd"
				,editor : jsTyList
				,editable: function (dataItem) {
					return dataItem.attrTy === "1";
		        }
				,template: "#=viewGroupCd(jsGroupCd,attrTy)#"
				,width : getElSize(420)
//				,field : "?"
			}/* ,{
				title : "${character_name}"
				,field : "attrNameOthr"
				,width : getElSize(600)
				,attributes: {
				      "class": "table-cell"
				 }
			} */,{
				title : "${del}"
				,field : "del"
				,width : getElSize(200)
				,template : "<input type='button' value='${del}' onclick=deleteRow(#=id#,this)>"
			}]
		}).data("kendoGrid")
		
		getTable()
	})
	
	function getTable(){
		$("#loader").css("display", "block");
		var url = "${ctxPath}/chart/getChkStandardList.do";
		var dvcId = $("#dvcId option:selected").html()
		if(dvcId=="장비 없음"){
			dvcId = undefined
		}
 		var param = "prdNo=" + $("#prdNo").val() + 
					"&dvcId=" + dvcId + 
					"&chkTy=" + $("#chkTy").val();   
// 		var param = "prdNo=FS_RR_LH &dvcId=FS/R M#3 &chkTy=ALL" 

		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading();
				
				var json = data.dataList;
				console.log("테이블?")
				console.log(json)
				$(json).each(function(idx, data){
					data.name=decodeURIComponent(data.name).replace(/\+/gi, " ");
					data.attrNameKo=decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ")
					data.spec=decodeURIComponent(data.spec).replace(/\+/gi, " ")
					data.measurer=decodeURIComponent(data.measurer).replace(/\+/gi, " ")
					data.firstCd = data.attrCd.substr(0,1)
					data.secondCd = data.attrCd.substr(1,data.attrCd.length)
					
				})
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
		/* 			group: { field: "prdNo" },
					sort: [{
		            	field: "prdNo" , dir:"asc" 
		            },{
		            	field: "item" , dir:"asc"
		            }], */
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								name: { editable: false },
								dp: { editable: true ,type: "number" },
								target: { editable: true ,type: "number" },
								low: { editable: true ,type: "number" },
								up: { editable: true ,type: "number" },
								del: { editable: false },
								//firstCd : {editable: false}
							}
						}
					}
				}); 
				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");
				$("#grid").find("table").on("keydown", onGridKeydown);
//				$("#grid thead tr th").css("font-size",getElSize(40))
//				$("#grid tbody tr td").css("font-size",getElSize(45))
			/* 	var tr = "<tbody>";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					tr = "<tr class='" + className + " contentTr' id='tr" + data.id + "'>" + 
								"<td>" + data.prdNo + "</td>" +
								"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" + 
								"<td id='chkTy" + data.id + "'>" + chkTy + "</td>" + 
								"<td id='attrTy" + data.id + "'>" + attrTy + "</td>" + 
								"<td><font>" + data.attrCd.substr(0,1) + "</font><input type='text' value='" + data.attrCd.substr(1) + "' size='6'></td>" + 
								"<td><input type='text' value='" + decodeURIComponent(data.attrNameKo).replace(/\+/gi, " ") + "'></td>" + 
								"<td><input id='dp" + data.id + "' type='text' value='" + data.dp + "' size='2'></td>" + 
								"<td><input id='unit" + data.id + "' type='text' value='" + data.unit + "' size='2'></td>" + 
								"<td><input type='text' value='" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "'></td>" + 
								"<td><input id='target" + data.id + "' type='text' value='" + data.target + "' size='6'></td>" + 
								"<td><input id='low" + data.id + "' type='text' value='" + data.low + "' size='6'></td>" + 
								"<td><input id='up" + data.id + "' type='text' value='" + data.up + "' size='6'></td>" + 
								"<td><input type='text' value='" + decodeURIComponent(data.measurer).replace(/\+/gi, " ") + "' size='8'></td>" + 
								"<td>" + JSGroupCode + "</td>" + 
								"<td><input type='text' value='" + data.attrNameOthr + "'></td>" + 
								"<td><button onclick='chkDel(this)'>${del}</button></td>" + 
						"</tr>";
						
					$("#table2").append(tr);	
					$("#chkTy" + data.id + " select option[value=" + data.chkTy + "]").attr('selected','selected');
					$("#attrTy" + data.id + " select option[value=" + data.attrTy + "]").attr('selected','selected');
					
					if(data.attrTy==1){
						$("#dp" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#unit" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						
						$("#target" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#up" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#low" + data.id).val("").attr("disabled", true).css("background-color", "#929292");
						$("#jsGroupCd" + data.id).attr("disabled", false).css("background-color", "#ffffff");
					}else{
						$("#jsGroupCd" + data.id).attr("disabled", true).css("background-color", "#929292");
					}
				});
				
				$("#table2").append("</tbody>");
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				setEl();
				
				$("#loader").css("display", "none"); */
			}
		});
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="wrapper">
						<table style="width: 100%">
							<tr>
								<td>
									<spring:message code="prd_no"></spring:message>
									<select id="prdNo"></select>
									<spring:message code="device"></spring:message> 
									<select id="dvcId"></select>
									<spring:message code="check_type"></spring:message>
									<select  id="chkTy"></select>
									<button id="search" onclick="getTable()" style="cursor: pointer; x"><i class="fa fa-search" aria-hidden="true"></i>검색</button>								
								</td>
								<td style="text-align: right;">
									<button  onclick="insertRow()"><spring:message code="add"></spring:message></button>
									<button  onclick="saveRow()"><spring:message code="save"></spring:message></button>
								</td>
							</tr>
							<Tr>
								<Td colspan="2" style="text-align: center; vertical-align: middle;">
									
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	