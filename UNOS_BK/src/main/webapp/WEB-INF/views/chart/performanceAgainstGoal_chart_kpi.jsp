<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
.k-dialog-titlebar{
	display: none;
}
#kendoChart text{
	fill : white
}

.datebtn:hover{
	cursor: pointer;
	background: #6699F0 !important;
	border-color : #6699F0 !important;
}
.standardbtn:hover{
	cursor: pointer;
	background: #6699F0 !important;
	border-color : #6699F0 !important;
}

#search:hover{
	cursor: pointer;
	background: #6699F0 !important;
	border-color : #6699F0 !important;
}

</style> 
<script type="text/javascript">

const loadPage = () =>{
	createMenuTree("analysis", "performanceAgainstGoal_chart_kpi")
	
}
	function getGroup(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#group").html(option);
				
				getTableData("jig");
			}
		});
	};
	
	var chkChartData="target";
	var standard="dvc";
	var search="target";
	
	
	function chkGoalTarget(){
		$.showLoading();
		cBarPoint=0;
		var checkedStandard = standard;
		var searchValue = search;
		
		if(searchValue=='target'){
			chkChartData="target"
			getTableData();
		}else if(searchValue=='goalRatio'){
			chkChartData="goalRatio";
			var sDate = $("#sDate").val();
			var eDate = $("#eDate").val();
			var param = "sDate=" + sDate +
			"&eDate=" + eDate	;

			if(checkedStandard=='dvc'){
				
				var url = "${ctxPath}/kpi/getDeviceCnt.do";
				
				$.ajax({
					url : url,
					data : param,
					dataType : "json",
					type : "post",
					success : function(data){
						var json = data.dataList;
						
						console.log("----dvc----")
						console.log(json)
						
						maxPage = json.length - maxBar;
						console.log("maxPage is : " + maxPage)
						cPage = 1;
						wcName = [];
						capa = [];
						
						console.log(json)
						$(json).each(function(idx, data){
							wcName.push(decode(data.name));
							tmpWcName = wcName
							
							capa.push(Number(data.goalRatio));
							
							tmpCapa = capa;
						});
						//setEl();
						
						if(json.length > maxBar){
							reArrangeArray();
							$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
							
						};

						chart("chart");
						$.hideLoading()
						//addSeries();
						
					}
				});
				
			}else if(checkedStandard=='worker'){
				console.log("worker")
				var url = "${ctxPath}/kpi/getWorkerCnt.do";
							
				$.ajax({
					url : url,
					data : param,
					dataType : "json",
					type : "post",
					success : function(data){
						var json = data.dataList;
						console.log("----worker----")
						console.log(json)

						maxPage = json.length - maxBar;
						console.log("maxPage is : " + maxPage)
						cPage = 1;
						wcName = [];
						capa = [];
						
						console.log(json)
						$(json).each(function(idx, data){
							wcName.push(decode(data.worker));
							tmpWcName = wcName
							
							capa.push(Number(data.goalRatio));
							
							tmpCapa = capa;
						});
						
						
						//setEl();
						
						if(json.length > maxBar){
							reArrangeArray();
							$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
							
						};

						chart("chart");
						$.hideLoading()
						//addSeries();
						
					}
				});
				
			}else if(checkedStandard=='prdNo'){
				
				var url = "${ctxPath}/kpi/getPrdNoCnt.do";
				
				$.ajax({
					url : url,
					data : param,
					dataType : "json",
					type : "post",
					success : function(data){
						var json = data.dataList;
						console.log("----prdNo----")
						console.log(json)

						maxPage = json.length - maxBar;
						console.log("maxPage is : " + maxPage)
						cPage = 1;
						wcName = [];
						capa = [];
						
						console.log(json)
						$(json).each(function(idx, data){
							wcName.push(decode(data.prdNo));
							tmpWcName = wcName
							
							capa.push(Number(data.goalRatio));
							
							tmpCapa = capa;
						});
						//setEl();
						
						if(json.length > maxBar){
							reArrangeArray();
							$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
							
						};

						chart("chart");
						$.hideLoading()
						//addSeries();
					}
				});
				
			}
		}
	}
	
	
	
	function getTableData(el){
		$.showLoading()
		
		if(standard=='dvc'){
			$("button#dvc").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
		}
		
		if(search=='target'){
			$("button#target").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
		}
		
		if(checked==1){
			if($('input#sMonth').val()==''){
				kendo.alert("월별 검색시 검색 할 월을 선택해주세요.")
				$.hideLoading();
				return;
			}
		}
		
		
		var today = new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();
	    var s = today.getSeconds();
		
		//장비 , 작업자, 제품 체크 확인
		var checkedStandard = standard;
		
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		var url = "${ctxPath}/chart/getPerformanceAgainstGoal.do";

		window.localStorage.setItem("jig_sDate", sDate);
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + " 23:59:59" +
					"&shopId=" + shopId + 
					"&checkedStandard=" + checkedStandard +
					"&prdNo=" + 'all';
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				maxPage = json.length - maxBar;
				console.log("maxPage is : " + maxPage)
				cPage = 1;
				wcName = [];
				
				capa = [];
				plan = [];
				perform = [];
				machinePerform = [];
				fault =	[];
				
				console.log(json)
				$(json).each(function(idx, data){
					wcName.push(decode(data.name));
					tmpWcName = wcName
					
					capa.push(Number(data.capa*2));
					plan.push(Number(data.tgCyl))
					perform.push(Number(data.cntCyl));
					machinePerform.push(Number(data.dvcCntCyl));
					fault.push(Number(data.faultCnt))
					
					tmpCapa = capa;
					tmpPlan = plan;
					tmpPerform = perform;
					tmpMachinePerform = machinePerform;
					tmpFault = fault;
				});
				//setEl();
				
				if(json.length > maxBar){
					reArrangeArray();
					$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
					
				}; 

				chart("chart");
				$.hideLoading()
				//addSeries();
			}
		});
	};
	
	var jigCsv;
	var wcCsv;
	var selected_dvc;
	var className = "";
	var classFlag = true;
	var maxBar = 10;
	
	var cBarPoint = 0;

	var tmpWcName = new Array();

	var tmpCapa = [];
	var tmpPlan = [];
	var tmpPerform = [];
	var tmpMachinePerform = [];
	var tmpFault = [];

	function reArrangeArray(){
		
		capa = [];
		plan = [];
		perform = [];
		machinePerform = [];
		fault =	[];
		wcName = [];
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
			wcName[i-cBarPoint] = tmpWcName[i];

			capa[i-cBarPoint] = tmpCapa[i];
			plan[i-cBarPoint] = tmpPlan[i];
			perform[i-cBarPoint] = tmpPerform[i];
			machinePerform[i-cBarPoint] = tmpMachinePerform[i];
			fault[i-cBarPoint] = tmpFault[i];
		};
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
			//wcList[i-cBarPoint] = tmpWcList[i];	
		};
		
		$("#controller font").html(addZero(String(cPage)) + " / " + Math.ceil((maxPage + maxBar) / 10));
		
	};
	var maxPage;

	var cPage = 1;
	function nextBarArray(){
		if(cBarPoint>=maxPage || maxPage<=0){
			alert("${end_of_chart}");
			return;
		};
		
		cPage++
		cBarPoint += maxBar;
		reArrangeArray();
		
// 		setEl();
		chart("chart");
		//addSeries();
		
		$("#arrow_left").attr("src", ctxPath + "/images/left_en.png");
		if(Math.ceil((maxPage + maxBar) / 10)==cPage) $("#arrow_right").attr("src", ctxPath + "/images/right_dis.png");
	};

	function prevBarArray(){
		if(cBarPoint<=0){
			alert("${first_of_chart}");
			return;
		};
		
		cBarPoint -= maxBar;
		cPage--;
		reArrangeArray();
		
// 		setEl();
		chart("chart");
		//addSeries();
		
		$("#arrow_right").attr("src", ctxPath + "/images/right_en.png");
		if(cBarPoint==0) $("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
	};


	var capa = [];
	var plan = [];
	var perform = [];
	var machinePerform = [];
	var fault =	[];
	
	var dvc;
	var wcName = []
	
	function chart(id){
		//목표 실적 차트
		if(chkChartData=="target"){
			$("#kendoChart").kendoChart({
				chartArea: {
					height: getElSize(1450),
					background:"#2B2D32",
				},
				title: false,
	            legend: {	//범례표?
	             	labels:{
	            		font:getElSize(40) + "px sans-serif",
	            	},
	            	stroke: {
	            		width:100
	            	},
	            	position: "bottom",
	            	orientation: "horizontal",
	                offsetX: getElSize(1010),
	//                offsetY: getElSize(800)
	           
	            },
	            render: function(e) {	//범례 두께 조절
	                var el = e.sender.element;
	                el.find("text")
	                    .parent()
	                    .prev("path")
	                    .attr("stroke-width", getElSize(20));
	            },
				seriesDefaults: {	//데이터 기본값 ()
					gap: getElSize(1),
					type: "column" ,
					spacing: getElSize(0.5),
	 				labels:{
	 					font:getElSize(36) + "px sans-serif",	//no working
	 					margin:0,
	 					padding:0
					}, 
					visual: function (e) {
		                return createColumn(e.rect, e.options.color);
		            }
				},	
				categoryAxis: {	//x축값
	                categories: wcName,
	                line: {
	                    visible: true
	                },
	                labels:{
	                	font:getElSize(36) + "px sans-serif",	//no working
					} 
	            },
	            tooltip: {	//커서 올리면 값나옴
	                visible: true,
	                format: "{0}%",
	                template: "#= series.name #: #= value #"
	            },
	            valueAxis: {	//간격 y축
	                labels: {
	                    format: "{0}"
	                }
	//                majorUnit: 100	//간격
	            },
	            series: [{	//데이터
	                labels: {
	                  visible: true,
	                  background:"#7F3F97",
	                  rotation: 270,
	                },
	                color : "#7F3F97",
	                name: "Capa",
	                data: capa		
	              },{
	                labels: {
						visible: true,
						background:"#0080FF",
	//					position: "bottom",
						rotation: 270
	           	    },
						color : "#0080FF",
						name: "${plan}",
						data: plan
	              },{
	                labels: {
						visible: true,
						background:"#A3D800",
	//					position: "top",
						rotation: 270
					},
					color : "#A3D800",
	                name: "${machinePerformance}",
	                data: machinePerform
	              },{
	                  labels: {
		                  visible: true,
		                  background:"#FAAF40",
	//	                  position: "bottom",
		                  rotation: 270
					  },
					  color : "#FAAF40",
	                  name: "${worker_performance}",
	                  data: perform
	              },{
	                  labels: {
	                      visible: true,
	                      background:"#EC1C24",
	//                    position: "bottom",
	                      rotation: 270
					  },
			  		  color : "#EC1C24",
	                  name: "${faulty}",
	                  data: fault
				  }]
			})
		}
		
		//달성률 그래프
		if(chkChartData=="goalRatio"){
			$("#kendoChart").kendoChart({
				chartArea: {
					height: getElSize(1450),
					background:"#2B2D32",
				},
				title: false,
	            legend: {	//범례표?
	             	labels:{
	            		font:getElSize(48) + "px sans-serif",
	            	},
	            	stroke: {
	            		width:100
	            	},
	            	position: "bottom",
	            	orientation: "horizontal",
	                offsetX: getElSize(1410),
//	                offsetY: getElSize(800)
	           
	            },
	            render: function(e) {	//범례 두께 조절
	                var el = e.sender.element;
	                el.find("text")
	                    .parent()
	                    .prev("path")
	                    .attr("stroke-width", getElSize(20));
	            },
				seriesDefaults: {	//데이터 기본값 ()
					gap: getElSize(6),
					type: "column" ,
					spacing: getElSize(0.5),
	 				labels:{
	 					font:getElSize(45) + "px sans-serif",	//no working
	 					margin:0,
	 					padding:0
					} , 
					visual: function (e) {
		                return createColumn(e.rect, e.options.color);
		            }
				},	
				categoryAxis: {	//x축값
	                categories: wcName,
	                line: {
	                    visible: true
	                },
	                labels:{
	                	font:getElSize(48) + "px sans-serif",	//no working
					} 
	            },
	            tooltip: {	//커서 올리면 값나옴
	                visible: true,
	                format: "{0}%",
	                template: "#= series.name #: #= value #"
	            },
	            valueAxis: {	//간격 y축
	                labels: {
	                    format: "{0}"
	                }
//	                majorUnit: 100	//간격
	            },
	            series: [{
	                  labels: {
	                      visible: true,
	                      background:"#F2CB61",
	                      font:getElSize(100) + "px sans-serif",	//no working
//	                      rotation: 270
					  },
			  		  color : "#F2CB61",
	                  name: "달성율",
	                  data: capa
				  }]
			})
		}
		
	}
	
	var drawing = kendo.drawing;
	var geometry = kendo.geometry;
	function createColumn(rect, color) {
        var origin = rect.origin;
        var center = rect.center();
        var bottomRight = rect.bottomRight();
        var radiusX = rect.width() / 2;
        var radiusY = radiusX / 3;
        var gradient = new drawing.LinearGradient({
            stops: [{
                offset: 0,
                color: color
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 0.5,
                color: color,
                opacity: 0.9
            }, {
                offset: 1,
                color: color
            }]
        });

        var path = new drawing.Path({
                fill: gradient,
                stroke: {
                    color: "none"
                }
            }).moveTo(origin.x, origin.y)
            .lineTo(origin.x, bottomRight.y)
            .arc(180, 0, radiusX, radiusY, true)
            .lineTo(bottomRight.x, origin.y)
            .arc(0, 180, radiusX, radiusY);

        var topArcGeometry = new geometry.Arc([center.x, origin.y], {
            startAngle: 0,
            endAngle: 360,
            radiusX: radiusX,
            radiusY: radiusY                
        });

        var topArc = new drawing.Arc(topArcGeometry, {
            fill: {
                color: color
            },
            stroke: {
                color: "#BDBDBD"
            }
        });
        var group = new drawing.Group();
        group.append(path, topArc);
        return group;
    }
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};

	$(function(){
		
		getGroup();
		setDate();	
		setEl();
		
		daily(moment().format("YYYY-MM-DD"));
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
	
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(40)
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
	
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content_table").css({
			"height" :getElSize(1500),
			"width" : $("#container").width() - getElSize(90),
			"overflow" : "auto"
		});
		
		$("#content_table td").css({
			"color" : "white",
			"background" : "#2B2D32",
			"font-size" : getElSize(48),
		});
		

		$("#content_table tbody tr td div").css({
			"height" : getElSize(210),
			"margin-top" : getElSize(20)
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding-left" : getElSize(15),
			"padding-right" : getElSize(15),
			"padding-bottom" : getElSize(20),
		})
		
		$("#search").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"font-size" : getElSize(48),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8),
			"margin-left" : getElSize(1400)
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#controller").css({
			"display" : "table",
			"background-color" : "black",
			"border-radius" : getElSize(70),
			"position" : "absolute",
			"bottom" : getElSize(20),
			"padding" : getElSize(15),
			//"width" : getElSize(600),
			"vertical-align" : "top",
			"height" : getElSize(100)
		});
		
		$("#controller").css({
			"left" : $("#content_table").offset().left - marginWidth + ($("#content_table").width()/2) - ($("#controller").width()/2)
		});
	
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		
		$(".dateBtn").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"font-size" : getElSize(48),
			"border-radius" : getElSize(8)
		})
		
		$(".hasdatepicker, #sDate, #sMonth, #sDate2, #eDate").css({
			"border": "1px black #999",
            "width" : getElSize(500),
            "height" : getElSize(80),
            "font-family": "inherit",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none",
			"border-color" : "#222327"
		})
			
		$("#radioGroup").css({
			"height" : getElSize(128)
		})
		
		$("#searchStandardLabel").css({
	    	"color": "white"
		})
		
		$("#searchValueLabel").css({
	    	"color": "white",
		})
		
		$(".standardBtn").css({
			"height" : getElSize(80),
			"width" : getElSize(260),
			"font-size" : getElSize(48),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(8)
		})
		
	};
	
	
	function goGraph(){
		location.href = "${ctxPath}/chart/performanceAgainstGoal_chart.do"
	};
	
	
	var checked = 0;
	
	function daily(today){
	 
		checked = 0;
		console.log("첫 프로시져")

		
	 	if(today!=undefined || today!=null){
		 	$("input#sDate").val(today);
		 	$("input#eDate").val(today);
		 	$("#rangeRadio #sDate2").val(today)
	 	}
	 	$("input#eDate").val($("input#sDate").val())
	 	
	 	$("input#dailyRadio").css({
			"background" : "#6699F0"
		})
		$("input#monthlyRadio").css({
			"background" : "#9B9B9B"
		})
		$("input#rangeRadio").css({
			"background" : "#9B9B9B"
		})
		
	 	$("#dailyRadio #sDate").css({
	 		"background" : "black",
	 		"color" : "white",
	 		"pointer-events": "auto"
	 	})
		$("#monthlyRadio #sMonth ,#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"background" : "#666666",
			"color" : "#565656",
			"pointer-events": "none"
		})
		
		$("#dailyRadio input#sDate").datepicker({
	        "maxDate": 0,
			onSelect:function(e){
				$("#rangeRadio #sDate2").val(e)
				$("input#sDate").val(e)
				$("input#eDate").val(e)
			}
		})
	}
	
	function monthly(){
		
		checked = 1;
		
		
		$("input#dailyRadio").css({
			"background" : "#9B9B9B"
		})
		$("input#monthlyRadio").css({
			"background" : "#6699F0"
		})
		$("input#rangeRadio").css({
			"background" : "#9B9B9B"
		})
		
		
		$('input#sMonth').datepicker( {
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            $('input#sMonth').val(year+'년'+month.toString()+'월');
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#sDate2').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    })
	    
		$("#monthlyRadio #sMonth ").css({
			"pointer-events": "auto",
	 		"background" : "black",
	 		"color" : "white"
	 	});
		$("#dailyRadio #sDate ,#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"pointer-events": "none",
			"background" : "#666666",
			"color" : "#565656"
		});
	}
	function range(){
		
		checked = 2;
		
		$("input#dailyRadio").css({
			"background" : "#9B9B9B"
		})
		$("input#monthlyRadio").css({
			"background" : "#9B9B9B"
		})
		$("input#rangeRadio").css({
			"background" : "#6699F0"
		})
		
		
		$("#rangeRadio #sDate2 ,#rangeRadio #eDate").css({
			"pointer-events": "auto",
	 		"background" : "black",
	 		"color" : "white"
	 	})
		$("#dailyRadio #sDate ,#monthlyRadio #sMonth ").css({
			"pointer-events": "none",
			"background" : "#666666",
			"color" : "565656"
		})
		
		$("#rangeRadio #sDate2").datepicker({
	        "maxDate": 0,
			onSelect:function(e){
				$("input#sDate").val(e);
			}
		})
		
		$("#rangeRadio #eDate").datepicker({
	        "maxDate": 0,
			onSelect:function(e){
				$("#rangeRadio input#eDate").val(e)
			}
		})

	}
	
	function changeStandardValue(e){
		
		standard = e;
		
		if(e=='dvc'){
			$("button#dvc").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
			$("button#worker, button#prdNo").css({
				"background" : "linear-gradient(lightgray, gray)",
				"color" : "black"
			})
		}else if(e=='worker'){
			$("button#worker").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
			$("button#prdNo, button#dvc").css({
				"background" : "linear-gradient(lightgray, gray)",
				"color" : "black"
			})
		}else if(e=='prdNo'){
			$("button#prdNo").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
			$("button#worker, button#dvc").css({
				"background" : "linear-gradient(lightgray, gray)",
				"color" : "black"
			})
		}
		
	}
	
	function changeSearchValue(e){
		console.log(e)
		search = e;
		
		if(e=='target'){
			$("button#target").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
			$("button#goalRatio").css({
				"background" : "linear-gradient(lightgray, gray)",
				"color" : "black"
			})
		}else if(e=='goalRatio'){
			$("button#goalRatio").css({
				"background" : "#6699F0",
				"border-color" : "#6699F0",
				"color" : "black"
			})
			$("button#target").css({
				"background" : "linear-gradient(lightgray, gray)",
				"color" : "black"
			})
		}
		
	}
	
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table id="content_table" style="width: 100%"> 
						<tr>
							<td>
								<div>
									<div style="display: inline;" id=dailyRadio>
										<input class="dateBtn" type="button" id="dailyRadio" value="<spring:message code="By_date"></spring:message>" onclick="daily()" checked="checked">
										<input type="text" id="sDate" readonly>
									</div>
									
									<div style="display: inline;" id=monthlyRadio>
										<input class="dateBtn" type="button" id="monthlyRadio" value="<spring:message code="monthly"></spring:message>" onclick="monthly()">
										<input type="text" id="sMonth" readonly>
									</div>
									
									<div style="display: inline;" id="rangeRadio">
										<input class="dateBtn" type="button" id="rangeRadio" value="<spring:message code="By_period"></spring:message>" onclick="range()">
										<input type="text" id="sDate2" readonly>~
										<input type="text" id="eDate" readonly>
									</div>
									
									<div id="radioGroup">
										<div id="searchStandard" style="display : inline">
											<label id="searchStandardLabel"><spring:message code="Search_criteria"></spring:message></label>
											<button id="dvc" class="standardBtn" onclick="changeStandardValue('dvc')"><spring:message code="by_Equipment"></spring:message></button>
											<button id="worker" class="standardBtn" onclick="changeStandardValue('worker')"><spring:message code="By_worker"></spring:message></button>
											<button id="prdNo" class="standardBtn" onclick="changeStandardValue('prdNo')"><spring:message code="By_car"></spring:message></button>
										</div>
										<div id="searchValue" style="display : inline">
											<label id="searchValueLabel"><spring:message code="Search_value"></spring:message></label>
											<button id="target" class="standardBtn" onclick="changeSearchValue('target')"><spring:message code="target"></spring:message>,<spring:message code="Performance"></spring:message></button>
											<button id="goalRatio" class="standardBtn" onclick="changeSearchValue('goalRatio')"><spring:message code="Achievement_rate"></spring:message></button>
										</div>
										<button id="search" onclick="chkGoalTarget()" style="cursor: pointer; "><i class="fa fa-search" aria-hidden="true"></i>검색</button>							</Td>
		
									</div>
								</div>
							</td>
						</tr>
						<Tr>
							<td>
								<!-- <select id="group"></select> -->
							</td>
									
<%-- 							 <td>
								<spring:message code="op_period"></spring:message> 
								<input type="date" class="date" id="jig_sdate">
								<button id="search" onclick="getTableData()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<button onclick="goGraph()"><spring:message code="table"></spring:message></button>
							</td> 
--%>						
						</Tr>		
						<tr>
							<td>
								<div id="kendoChart"></div>
								<!-- <div id="chart"></div> -->
								<div id="controller">
									<img alt="" src="${ctxPath }/images/left_en.png" id="arrow_left"  onclick="prevBarArray();">
									<font style="display: table-cell; vertical-align:top">01 / 00</font>
									<img alt="" src="${ctxPath }/images/right_en.png" id="arrow_right" onclick="nextBarArray();">
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	