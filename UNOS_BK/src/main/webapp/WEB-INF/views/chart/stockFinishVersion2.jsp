<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<script src="${ctxPath}/js/lib/loading.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/lib/loading.css">
        
<link rel="shortcut icon" href="${ctxPath }/images/FL/icon/ico_mc.svg">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

.k-icon.k-i-collapse{
	display:none;
} 

.k-grid-content-locked table tr td{
	font-weight : bold;
}
#grid tbody tr.k-grouping-row td{
	font-size:0px;
	padding :5px;
}

.k-grid tbody > .k-alt
{
 background :  #DCDCDC ;
 text-align: center;
}

</style>

<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">

</head>

<script>

	const loadPage = () =>{
		createMenuTree("im", "stockStatusDay2")
	}
	
	$(function(){
		//datepicker event && date data input 
		dateEvt();
		// gridTable() => getTable()
		gridTable();
		setEl();
		
	})
	// grid css 작업
	function gridCss(){
		//table 해드 부분
		$("#grid thead tr th").css({
			"font-size" : getElSize(39)
			,"vertical-align" : "middle"
			,"text-align" : "center"
		})
		
		//table body 부분
		$("#grid tbody tr td").css({
			"font-size" : getElSize(35),
			"overflow" : "hidden",
			"white-space" : "nowrap",
		})
		
		//table body 부분
		$("#grid tbody tr").css({
			"height" : getElSize(80),
		})
		
		//template 수정가능한 부분 div
		$(".editCss").css({
			"white-space": "nowrap",
// 		    "height": "25px",
		    "overflow": "hidden",
		    "float": "left",
		    "width": "65%",
		    "background": "ivory"
		})

		//template btn css
		$(".editBtn").css({
			"float" : "right",
			"font-size" : getElSize(45),
		    "cursor" : "pointer",
// 		    "background": "red",
		    "background": "yellowgreen",
		    "color": "sienna",
		    "padding" : getElSize(5),
		    "border-radius" : getElSize(10)
		})
		

		//오른쪽 구분하기
		$(".k-grid tbody tr td").css({
			"border-bottom": getElSize(2)+"px solid black"
		});

		//라인구분하기 td
		$(".lineGp td").css({
			"border-bottom": getElSize(7.5)+"px solid black"
		});
		
		//라인구분하기 td
		$(".cellMerge").css({
			"border-bottom": getElSize(7.5)+"px solid black"
		});
		//오른쪽 구분하기
		$(".k-grid tbody tr td").css({
			"border-right": getElSize(2)+"px solid black"
		});

	}
	
	//라인 구분 위해 클래스 추가
	function griddivisionClass(e){
		var items = e.sender.items();

		//tr 만큼 반복문 돌리기
		items.each(function(idx, item){
			//다음행이랑 현재행이랑 비교해서 다를떄 현재행에 class 추가
			if(items.length-1!=idx){
				if(e.sender.dataItem(item).prdNo!=e.sender.dataItem(items[idx+1]).prdNo){
					$(item).addClass("lineGp")
				}
			}
		});
	}
	//셀 합치기
	function gridCellMerge(e){
		mergeChk=[]; mergeList=[];
		var grid = $("#grid").data("kendoGrid").dataSource

		$(".cellMerge").each(function(idx,data){
			// tr값 가져오기
			var uid = $(".cellMerge")[idx].closest("tr").dataset.uid
			var RowItem = 	grid.getByUid(uid)
			
			/* var dataItem = $(container).closest("tr")[0].dataset.uid;
			var grid = $("#wrapper").data("kendoGrid");
			var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + dataItem + "']");
			var idx = grid.dataSource.indexOf(grid.dataItem(row));
			 */
			
			
	//		var row = $("#wrapper").data("kendoGrid").tbody.find("tr[data-uid='" + uid + "']");
			 
			var row = $(this).closest("tr");
		    var rowIdx = $("tr", $("#grid").data("kendoGrid").tbody).index($(".cellMerge")[idx].closest("tr"));

			if(mergeChk.indexOf(RowItem.prdNo)==-1){
				mergeChk.push(rowIdx)
			}
			
			if(mergeChk.indexOf(RowItem.prdNo)==-1 && mergeChk.indexOf(rowIdx)!=-1){
				var count=0;
				// rowspan 몇으로 할지 정하기 위해서
				
				for(i=0, len=grid.data().length; i<len; i++){
//					console.log(data.prdNo + " ,, " + grid.data()[i].prdNo)
					if(RowItem.prdNo==grid.data()[i].prdNo){
						count ++;
					}
				}
				
				var arr={};
					arr.prdNo = RowItem.prdNo;
					arr.count = count;
				
				mergeChk.push(RowItem.prdNo);
				mergeList.push(arr);
				
			}else{
				if(mergeChk.indexOf(rowIdx)==-1)
				$(".cellMerge:eq(" + idx + ")").addClass("hidden");
//				$(".cellMerge:eq(" + idx + ")").attr("class","hidden")
			}
			
		})
		
		$(".cellMerge.hidden").attr("class","hidden");

 		$(".hidden").css("display","none");
		
		$(".cellMerge").each(function(idx,data){
			var uid = $(".cellMerge")[idx].closest("tr").dataset.uid
			var RowItem = 	grid.getByUid(uid)
			
			for(i=0, len=mergeList.length; i<len; i++){
//				console.log(data.prdNo + " ,, " + grid.data()[i].prdNo)
				if(RowItem.prdNo==mergeList[i].prdNo){
					$(".cellMerge:eq(" + idx + ")").attr("rowspan",mergeList[i].count);
				}
			}
		})
		
	}
	
	
	function gridsum(){
		//주간 야간 합산수량
		totalCnt = 0 ;
		//주간 합산수량
		var cntD = 0;
		//야간 합산수량
		var cntN = 0;
		
		//주간
		var jsonD = $("#detailGridDay").data("kendoGrid").dataSource.data();
		var dupleD = [];
		
		//야간
		var jsonN = $("#detailGridNight").data("kendoGrid").dataSource.data();
		var dupleN = [];

		console.log(jsonD)
		console.log(jsonN)
		$(jsonD).each(function(idx,data){
			var chk = true;
			data.name = decode(data.name)
			data.nm = decode(data.nm)
			data.prdNo = data.wccd
			
			//DB에서 값가져올때
			//wccd 기준 cnt 오름차순 이므로
			//데이터값 첫번쨰꺼 가지고 오면 될듯
			//값이 한개도 없을떄는 insert 시키기
		
			//돌면서 같은값 확인하기
			$(dupleD).each(function(idx,dataDuple){
				//품번이 같은게 있을경우
				if(data.prdNo==dataDuple.prdNo && data.dvcId!=dataDuple.dvcId){
					if(data.cnt < dataDuple.cnt || dataDuple.cnt==0){
						chk = true;
					}else{
						chk = false;
					}
				}
			})
			//초기시작할때
			if(dupleD.length==0){
				dupleD.push(data)
			}else{
				//chk 값이 트루면 값변경 및 추가
				if(chk==true){
					//같은것이 없을경우에 추가하기위함
					var addChk = true;
					$(dupleD).each(function(no,item){
						if(item.prdNo==data.prdNo && item.dvcId!=data.dvcId){
							addChk = false;
							//클경우 숫자만 업데이트 시키기
							if(item.cnt > data.cnt && data.cnt!=0){
								item.cnt = data.cnt;
							}
						}
					})
					
					if(addChk){
						dupleD.push(JSON.parse(JSON.stringify(data)))
// 						dupleD.push(data);
					}
				}
			}
			
			
		})
		
		//야간 데이터 가공
		$(jsonN).each(function(idx,data){
			var chk = true;
			
			data.name = decode(data.name)
			data.nm = decode(data.nm)
			data.prdNo = data.wccd
			
			//DB에서 값가져올때
			//wccd 기준 cnt 오름차순 이므로
			//데이터값 첫번쨰꺼 가지고 오면 될듯
			//값이 한개도 없을떄는 insert 시키기
		
			//돌면서 같은값 확인하기
			$(dupleN).each(function(idx,dataDuple){
				//품번이 같은게 있을경우
				if(data.prdNo==dataDuple.prdNo && data.dvcId!=dataDuple.dvcId){
					if(data.cnt < dataDuple.cnt || dataDuple.cnt==0){
						chk = true;
					}else{
						chk = false;
					}
				}
			})
// 			//초기시작할때
// 			if(dupleN.length==0){
// 				dupleN.push(data)
// 			}else{
// 				//chk 값이 트루면 값변경 및 추가
// 				if(chk==true){
// 					dupleN.push(data);
// 				}
// 			}
			
			//초기시작할때
			if(dupleN.length==0){
				dupleN.push(data)
			}else{
				//chk 값이 트루면 값변경 및 추가
				if(chk==true){
					//같은것이 없을경우에 추가하기위함
					var addChk = true;
					$(dupleN).each(function(no,item){
						if(item.prdNo==data.prdNo){
							addChk = false;
							//클경우 숫자만 업데이트 시키기
							if(item.cnt > data.cnt && data.cnt!=0){
								item.cnt = data.cnt;
							}
						}
					})
					
					if(addChk){
						dupleN.push(JSON.parse(JSON.stringify(data)))
// 						dupleD.push(data);
					}
				}
			}
			
		})
		
		//주간 합산
		$(dupleD).each(function(idx,data){
			cntD = cntD + data.cnt;
		})
		
		//야간 합산
		$(dupleN).each(function(idx,data){
			cntN = cntN + data.cnt;
		})
		totalCnt = Number(cntD)+Number(cntN)
		
		console.log(cntD)
		console.log(cntN)
		$("#cntD").html(cntD);
		$("#cntN").html(cntN);
		
		//span 값 넣기
		$("#cntT").html(totalCnt)
	}
	
	//전역변수 
	var totalCnt = 0 ;
	var prdNo;
	var oprNo;
	var oprNm;
	
	// grid databounding 될때
	function onDataBound(e){
		//class 명 추가하기
		griddivisionClass(e);
		//cellMerge 셀합치기
		gridCellMerge(e);
		
		// 클릭 이벤트 초기화
		$('.editBtn').unbind('click');

		// 클릭 이벤트 
		$(".editBtn").click(function(e){
			
			var heightD = getElSize(1650)
			
			var grid = $("#grid").data("kendoGrid");
			var row = this.closest("tr");
			var dataItem = grid.dataItem(row);

			//주간 야간 합산수량
			totalCnt = 0 ;
			//주간 합산수량
			var cntD = 0;
			//야간 합산수량
			var cntN = 0;
			//div id 랑 컬럼id랑 일치시켜 id값 구해오기
			var id = this.closest("td").children[0].id
			
			prdNo = dataItem.item
			oprNo = "";
			oprNm = "";
			
			if(id=="rcvCntR"){
				oprNo = "0010";
				oprNm = "R삭"
			}else if(id=="rcvCntM"){
				oprNo = "0020";
				oprNm = "MCT삭"
			}else if(id=="rcvCntC"){
				oprNo = "0030";
				oprNm = "CNC삭"
			}
			
			var param = "eDate=" + $("#eDate").val() +
					"&sDate=" + $("#sDate").val() +
					"&prdNo=" + dataItem.item +
					"&oprNo=" + oprNo;
			var url = "${ctxPath}/chart/getSelectStock.do";

			$.showLoading()
			$.ajax({
				url: url,
				data: param,
				dataType: "json",
				type: "post",
				success: function (data) {
					
					//주간
					var jsonD = data.dataListDay;
					var dupleD = [];
					//야간
					var jsonN = data.dataListNight;
					var dupleN = [];
					//주간 데이터 가공
					$(jsonD).each(function(idx,data){
						var chk = true;
						data.name = decode(data.name)
						data.nm = decode(data.nm)
						data.prdNo = data.wccd
						
						//DB에서 값가져올때
						//wccd 기준 cnt 오름차순 이므로
						//데이터값 첫번쨰꺼 가지고 오면 될듯
						//값이 한개도 없을떄는 insert 시키기
					
						//돌면서 같은값 확인하기
						$(dupleD).each(function(idx,dataDuple){
							//품번이 같은게 있을경우
							if(data.prdNo==dataDuple.prdNo && data.dvcId!=dataDuple.dvcId){
								if(data.cnt < dataDuple.cnt || dataDuple.cnt==0){
									chk = true;
								}else{
									chk = false;
								}
							}
						})
						//초기시작할때
						if(dupleD.length==0){
							dupleD.push(data)
						}else{
							//chk 값이 트루면 값변경 및 추가
							if(chk==true){
								dupleD.push(data);
							}
						}
						
						
					})
					
					//야간 데이터 가공
					$(jsonN).each(function(idx,data){
						var chk = true;
						
						data.name = decode(data.name)
						data.nm = decode(data.nm)
						data.prdNo = data.wccd
						
						//DB에서 값가져올때
						//wccd 기준 cnt 오름차순 이므로
						//데이터값 첫번쨰꺼 가지고 오면 될듯
						//값이 한개도 없을떄는 insert 시키기
					
						//돌면서 같은값 확인하기
						$(dupleN).each(function(idx,dataDuple){
							//품번이 같은게 있을경우
							if(data.prdNo==dataDuple.prdNo && data.dvcId!=dataDuple.dvcId){
								if(data.cnt < dataDuple.cnt || dataDuple.cnt==0){
									chk = true;
								}else{
									chk = false;
								}
							}
						})
						//초기시작할때
						if(dupleN.length==0){
							dupleN.push(data)
						}else{
							//chk 값이 트루면 값변경 및 추가
							if(chk==true){
								dupleN.push(data);
							}
						}
					})
					
					//주간 합산
					$(dupleD).each(function(idx,data){
						cntD = cntD + data.cnt;
					})
					
					//야간 합산
					$(dupleN).each(function(idx,data){
						cntN = cntN + data.cnt;
					})
					
					totalCnt = Number(cntD)+Number(cntN)
					//주간 그리드 그리기
					kendoDetailDay = $("#detailGridDay").kendoGrid({
						dataSource: {
							data : jsonD
						}
						,dataBound : function(e){
							grid = this;
					        grid.tbody.find('tr').each(function(){
						        var item = grid.dataItem(this);
					            kendo.bind(this,item);
				        	})
				        	
				        	$(".cntText").off("blur").blur(function(){
				        		gridsum()
				        	})
							griddivisionClass(e);
							gridCss();
						}
// 						,cellClose : function(e){
// 							gridsum()
// 						}
// 						,editable : true
						,height : heightD - getElSize(520)
						,columns : [{
							title : "주간"
							,columns : [{
								field : "dvcId"
								,title : "번호"
								,width : getElSize(100)
							},{
								field : "name"
								,title : "장비명"
								,width : getElSize(420)
							},{
								field : "nm"
								,title : "작업자"
								,width : getElSize(250)
							},{
								field : "cnt"
								,title : "수량"
								,width : getElSize(200)
								,template:"<input class='cntText' type='number' data-role='number' style='width:100%;'  data-bind='value:cnt'/>"
								,footerTemplate: "<span id='cntD'>0</span>"
							}]
						}]
					}).data("kendoGrid");
				
					kendoDetailNight = $("#detailGridNight").kendoGrid({
						dataSource: {
							data : jsonN
						}
						,dataBound : function(e){
							
							grid = this;
					        grid.tbody.find('tr').each(function(){
						        var item = grid.dataItem(this);
					            kendo.bind(this,item);
				        	})
				        	
				        	$(".cntText").off("blur").blur(function(){
				        		gridsum()
				        	})
				        	
							griddivisionClass(e);
							gridCss();
						}
// 						,cellClose : function(e){
// 							gridsum()
// 						}
// 						,editable : true
						,height : heightD - getElSize(520)
						,columns : [{

							title : "야간"
							,columns : [{
								field : "dvcId"
								,title : "번호"
								,editable : false
								,width : getElSize(100)
							},{
								field : "name"
								,title : "장비명"
								,width : getElSize(420)
							},{
								field : "nm"
								,title : "작업자"
								,width : getElSize(250)
							},{
								field : "cnt"
								,title : "수량"
								,width : getElSize(200)
								,template:"<input class='cntText' type='number' data-role='number' style='width:100%;'  data-bind='value:cnt'/>"
								,footerTemplate: "<span id='cntN'>0</span>"
							}]
						}]
					}).data("kendoGrid");
					
					//popup 그리기
					$("#editPopup").kendoDialog({
						actions: [{
							text: "수정하기"
							,action: function(e){
								$.showLoading();
								
								console.log(e)
								saveRow();
							}
						},{
							text: "취소"
							,action: function(e){
								$("#editPopup").data("kendoDialog").close()
								return false
							}
						}]
						,title: dataItem.item + " (" + dataItem.ITEMNO + ") " + oprNm
						,width: getElSize(2800)
						,height: heightD
// 						,content: "<div id='detailGrid'></div>"
				    });
					
					$("#cntD").html(cntD);
					$("#cntN").html(cntN);
					
					//span 값 넣기
					$("#cntT").html(totalCnt)
					 
					//합계 표시 CSS
				    $("#totalCnt").css({
						"float": "left",
					    "width": "99.4%",
					    "background": "#222327",
					    "height": getElSize(85),
					    "margin-top": getElSize(25),
				    })
				    
				    $("#totalCnt table tr th").css({
				    	"padding-right": getElSize(85),
				    	"font-size": getElSize(50)
				    })
					
				    $("#editPopup").data("kendoDialog").open()
				    
			    	$.hideLoading();
					

				}
			})
			
		})

		gridCss();
	}
	
	function saveRow(){
		
		var grid = $("#grid").data("kendoGrid");
		var dataItem = grid.dataSource.data();
		
		grid.unlockColumn("prdNo");
		grid.unlockColumn("item");
		grid.unlockColumn("ITEMNO");
		
		$(dataItem).each(function(idx,data){
			if(prdNo==data.item){
				if(oprNo=="0010"){
					data.set("rcvCntR",totalCnt);
				}else if(oprNo=="0020"){
					data.set("rcvCntM",totalCnt);
				}else if(oprNo=="0030"){
					data.set("rcvCntC",totalCnt);
				}
			}
		})
		
// 		$("#grid").data("kendoGrid").dataSource.data()[0].set("rcvCntM",150);
		
		grid.lockColumn("prdNo");
		grid.lockColumn("item");
		grid.lockColumn("ITEMNO");

		setTimeout(function() {
			$.hideLoading();
		}, 200);
		
	}
	
	
	var kendotable;
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height: getElSize(1650),
			editable: "popup",	
			groupable: false,
			navigatable: true,
			scrollable: true,
			dataBound: onDataBound,
			dataSource: {
			    aggregate: [
			        { field: "rcvCnt", aggregate: "sum" }
				]
			},
			columns: [{
				title:""
				,width:getElSize(10)
				,locked: true,
			},{
				field: "prdNo",
				title: "차종",
				attributes: {
					class: "cellMerge" ,
     			},
				encoded: true,
// 				locked: true,
// 				lockable: false,
				width: getElSize(420)
			},{
				field: "item",
				title: "부품명",
// 				locked: true,
// 				lockable: false,
				width: getElSize(405)
			},{
				field: "ITEMNO"
				,title: "품번"
				,width: getElSize(280)
			},{
				title : "소재창고"
				,columns:[{
						field: "iniohdCnt"
						, title: "전일<br>재고"
						,hideMe: true
						, width: getElSize(170)
						,attributes: {
							class: "cellMerge" ,
		     			}
					},{
						field: "rcvCnt"
						,title: "입고"
						,width: getElSize(170)
						,footerTemplate: "#:data.rcvCnt.sum#"
//	 					,footerTemplate: "#=sumAcRcv#"
						,attributes: {
							class: "cellMerge" ,
		     			}
					},{
						field: "notiCnt"
						, title: "불량"
						, width: getElSize(170)
//	 					,footerTemplate: "#=sumAcNoti#"
						,attributes: {
							class: "cellMerge" ,
		     			}
					},{
						field:"sumIn"
						,title: "누적<br>입고"
						,width: getElSize(170)
//	 					,footerTemplate: "#=sumAcIn#"
						,attributes: {
							class: "cellMerge" ,
		     			}
					},{
						field: "issCnt"
						, title: "출고"
//	 					,footerTemplate: "#=sumAcIss#"
						,attributes: {
							class: "cellMerge" ,
		     			}, width: getElSize(170)
					},{
						field: "ohdCnt"
//	 					, template: "<div class='#=ohdCnt#'>#=iniohdCnt+rcvCnt-notiCnt-issCnt#"
						, title: "창고<br>재고"
						, width: getElSize(170)
						,attributes: {
							class: "cellMerge" ,
		     			}
				}]
				
			},{
				title : "공정창고"
				,columns:[{
				title: "R",
				columns: [{
					field: "rcvCntR"
					,template : "<div id='rcvCntR' class='editCss'>#=rcvCntR#</div><span class='k-icon k-i-edit editBtn'></span>"
					, title: "완성품"
					, width: getElSize(220)
					,attributes: {
						class: "0010" ,
	     			}
					
				}, {
					field:"sumInR"
					,title: "누적완성"
					,width: getElSize(210)
					
				},{
					field: "notiCntR"
					, title: "불량"
					, width: getElSize(170)
					
				}]
			},{
				title: "MCT",
				columns: [{
					field: "rcvCntM"
					,template : "<div id='rcvCntM' class='editCss'>#=rcvCntM#</div><span class='k-icon k-i-edit editBtn'></span>"
					, title: "완성품"
					, width: getElSize(220)
					,attributes: {
						class: "0020" ,
	     			}
				}, {
					field:"sumInM"
					,title: "누적완성"
					,width: getElSize(210)
					
				}, {
					field: "notiCntM"
					, title: "불량"
					, width: getElSize(170)
					
				}]
			},{
				title: "CNC",
				columns: [{
					field: "rcvCntC"
					,template : "<div id='rcvCntC' class='editCss'>#=rcvCntC#</div><span class='k-icon k-i-edit editBtn'></span>"
					, title: "완성품"
					, width: getElSize(220)
					,attributes: {
						class: "0030" ,
	     			}
				}, {
					field:"sumInC"
					,title: "누적완성"
					,width: getElSize(210)
				}, {
					field: "notiCntC"
					, title: "불량"
					, width: getElSize(170)
				}]
			},{
				title : "합계"
				,columns: [{
					field : "iniohdCntPro"
					,title : "전일재고"
					,width: getElSize(210)
					,attributes: {
						class: "cellMerge" ,
	     			}
				},{
					field : "ohdCntPro"
					,title : "현재재고"
					,width: getElSize(210)
					,attributes: {
						class: "cellMerge" ,
	     			}
				}]
			}]
				
			},{
				title: "도금",
				columns: [{
						field: "iniohdCntF"
						, title: "전일<br>재고"
						, width: getElSize(170)
					}, {
						field: "rcvCntF"
						, title: "입고"
						, width: getElSize(170)
	 				}, {
						field:"sumInF"
						,title: "누적<br>입고"
						, width: getElSize(170)
					}, {
						field: "issCntF"
						, title: "출고"
						, width: getElSize(170)
					},{
						field: "notiCntF"
						, title: "불량"
						, width: getElSize(170)
	 				},{
						field: "ohdCntF"
						, title: "재고"
						, width: getElSize(170)
					}]
			},{
				title : "완성창고"
				,columns: [{
					field: "iniohdCntP"
					, title: "전일<br>재고"
					, width: getElSize(170)
				},{
					field: "rcvCntP"
					, title: "입고"
					, width: getElSize(170)
				}, {
					field:"sumInP"
					,title: "누적<br>입고"
					, width: getElSize(170)
				}, {
					field: "issCntP"
					, title: "출고"
					, width: getElSize(170)
				}, {
					field: "sumOutP"
					, title: "누적<br>출고"
					, width: getElSize(170)
				}, {
					field: "notiCntP"
					, title: "불량"
					, width: getElSize(170)
				}, {
					field: "ohdCntP",
					template: "#=Number(iniohdCntP)+Number(rcvCntP)-Number(issCntP)-Number(notiCntP)#"
					, title: "현재고"
					, width: getElSize(170)
// 					, editor: readOnly
				}]
			}]
		
		}).data("kendoGrid")
		
		getTable();
		
	}
	
	//datepicker event
	function dateEvt(){
		
		$(".date").val(moment().subtract("days",1).format("YYYY-MM-DD"))
// 		$(".date").val(moment().subtract("days",3).format("YYYY-MM-DD"))
		
	    $( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getTable();
		    	$("#eDate").val(e);
		    }
	    })
	}
	
	function setEl(){
		$("#div1").css({
			"height" : getElSize(150)
			,"background" : "red"
			,"width" : contentWidth
		})
		
		$("#div2").css({
			"height" : getElSize(1650)
			,"background" : "blue"
		})
		
		$("button").css({
			"background" : "#9B9B9B"
			,"border-color" : "#9B9B9B"
			,"font-size" : getElSize(47)
			,"font-weight" : "bold"
		})
	}
	
	function getTable(){
		var param = "eDate=" + $("#eDate").val() +
					"&sDate=" + $("#sDate").val();
		var url = "${ctxPath}/chart/getRealTimeStockFinish.do";

		$.showLoading()
		$.ajax({
			url: url,
			data: param,
			dataType: "json",
			type: "post",
			success: function (data) {
				
				var grid = $("#grid").data("kendoGrid");
				grid.unlockColumn("prdNo");
				grid.unlockColumn("item");
				grid.unlockColumn("ITEMNO");
				
				var json = data.dataList;
				console.log(json)
				$.hideLoading();

				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: "popup",
					aggregate: [
				        { field: "rcvCnt", aggregate: "sum" }
				    ],
// 				    sort: [{
//                     	field: "prdNo" , dir:"asc" 
//                     },{
//                     	field: "item" , dir:"asc"
//                     },{
//                     	field: "ex" , dir:"asc" 
//                     }],
// 					group: [{field:"ex",dir:"asc"},{ field: "prdNo" ,dir: "asc"}],
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								item: { editable: false },
							}
						}
					}
				})
				
				kendotable.setDataSource(kendodata);

// 				setTimeout(function(){
					//잠금
// 				}, 500);

				//column 잠금
				var grid = $("#grid").data("kendoGrid");
				grid.lockColumn("prdNo");
				grid.lockColumn("item");
				grid.lockColumn("ITEMNO");
			}
		})
		
	}
</script>
<body>	
	<div id="container">
		<div id="div1" style="display: table;">
			<div style="display: table-cell; vertical-align: middle;">
				작업자 : <select id="worker"></select>
				날짜 <input type="text" id="sDate" class="date" readonly="readonly"> ~ <input type="text" id="eDate" class="date" readonly="readonly">
				<button onclick="getTable()">
					<i class="fa fa-search" aria-hidden="true"></i> 조회
				</button>
			</div>
		</div>
		
		<div id="div2">
			<div id="grid">
				
			</div>
		</div>
	</div>
	<div id="editPopup">
		<div style="color: red;">※ 투보아의 경우 수량이 적은 장비가 합산 됩니다.</div>
		<div id="detailGridDay" style="width: 48%; float: left;"></div>
		<div id="spaceDiv" style="width: 3%; float: left;">　</div>
		<div id="detailGridNight" style="width: 48%; float: left;"></div>
		<div id="totalCnt" style="float: left; width: 100%;"><table style="width: 100%; text-align: right;"><tr><th>합계 : <span id ="cntT"></span></span></th></tr></table></div>
	</div>
</body>
</html>	