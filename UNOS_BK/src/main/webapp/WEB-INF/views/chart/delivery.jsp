<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	var chk_del = "${chk_del}"
</script>


    
<style type="text/css">
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
#grid{
	background: black;
	border-color : #222327;
}

#grid .k-grid-header {
	border-color : #222327;
}

#grid .k-grid-header-wrap {
	border-color : #222327;
}

</style> 
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.material.mobile.min.css" />
<!-- <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
 --><script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
<script type="text/x-kendo-template" id="windowTemplate">
	<center>
    	<button class="k-button" id="yesButton">${check}</button>
    	<button class="k-button" id="noButton">${cancel}</button>
	</center>
</script>
<script src="${ctxPath }/js/kendo_lib.js"></script>
	<script type="text/javascript">
	
	const loadPage = () =>{
		createMenuTree("im", "delivery")	 	
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};
	
	
	
	var grid;
	var delivererId;
	
	//bert jquery 달력
	$( function() {
	    $( ".date" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getDeliveryHistory();
		    	$(".date").val(e);
		    }
	    })
	    
	    console.log("TTTTTT")
	    setEl2();
   });
	var kendotable="";
	$(document).ready(function(){
		gridTable();
		setEl2();
	});
	
	function gridTable(){
		kendotable=$("#grid").kendoGrid({
			
    	change: function(e) {  
    		
    		//금액 계산
			        		 
    		  var cell = this.select();
    		  var cellIndex = cell[0].cellIndex;
    		  var column = this.columns[cellIndex];
    		  var dataItem = this.dataItem(cell.closest("tr"));
		      dataItem.set("Discontinued",true);
    		  /* newTotal =  dataItem.cnt * dataItem.prc;
    		  dataItem.set("totalPrc", newTotal) */
    		  
    		  selectedRow = dataItem    		  		        		  
    		  var tr = cell.closest("tr");
//	    		  $(tr).children("td:nth(0)").children("input").attr("checked", true)
    	},
    	dataBound:function(e){
    		$("#checkall").css("margin-left","0px");
    	},
    	selectable: "row",
//	    	style: "border-color : black;",
        height: getElSize(1600),
        scrollable:true,
	     //  toolbar: ["create"],
        groupable: false,
        sortable: true,
        columns: [
	        		{
						title:"<input type='checkbox' id='checkall' style='vertical-align: middle ; text-align : center; width: "+getElSize(80)+"; height: "+getElSize(96)+";'>",width:getElSize(150),template: '<input type="checkbox" #= Discontinued ? \'checked="checked"\' : "" # #= checkdisable ? \'disabled="checked"\' : "" # class="checkbox"    style="width: '+getElSize(80)+'; height: '+getElSize(80)+';"/>' 
						,attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}						
					},
					{
            			field: "delivererId",
        				title: "${deliverer}",
        				editor: categoryDropDownDelivererEditor,
        				template: "#= delivererName(delivererId) #",
        				attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
                  	{
            			field: "lotNo",
            			title: "${lot_no}",
            			attributes: {
            				style: "text-align: center; color:black; background-color:rgb(245, 244, 245); font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			}, 
        			{
            			field: "prdNo",
        				title: "${prd_no}",
        				width : getElSize(400) + "px",
        				editor: categoryDropDownEditor1,
        				template: "#= prdName(prdNo) #",
        				attributes: {
            				style: "text-align: center; color:black; background-color:rgb(245, 244, 245); font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "spec",
            			title: "${spec}",
            			width : getElSize(220) + "px",
            			editor: readOnly,
            			attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "unit",
            			title: "${unit}",
            			editor: readOnly,
            			width : getElSize(170) + "px",
            			attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "cnt",
        				title: "${count}",
        				width : getElSize(230) + "px",
        				template : "#= numberWithCommas(cnt) #",
        				attributes: {
            				style: "text-align: center; color:black; background-color:rgb(245, 244, 245); font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "prc",
        				title: "${unit_price}",
        				template : "#= numberWithCommas(prc) #",
        				editor: readOnly,
        				attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "totalPrc",
        				title: "${price_}",
        				template : "#= numberWithCommas(totalPrc) #",
        				editor: readOnly,
        				attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "deliveryDate",
            			title: "${Delivery_date }",
            			editor: readOnly,
            			attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},
        			{
            			field: "deliveryNo",
            			title: "${Delivery_number }",
            			editor: readOnly,
            			width : getElSize(350) + "px",
            			attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			},{
        				
        				command :[{
        					
        					name :"${del}",
        					click : function(e){
        						del_btn_evt(e,this,"delDeliverHistory(id)")
        					}
        				}],
        				attributes: {
            				style: "text-align: center; color:black; font-size:" + getElSize(35)
            			},headerAttributes: {
            				style: "text-align: center; vertical-align:middle; background-color:black; color:white; font-size:" + getElSize(42)
            			}
        			}
        			
        			
        		],
        		editable: true,
        		edit:function(e){
        			var input = e.container.find(".k-input");
                    var value = input.val();
                    //기존 저장된 값 들고오기
                    //edit들어갈때
                    input.keyup(function () {
                        value = input.val();
                    });
                    //bert edit 수정후 나올때 금액계산
                    input.blur(function () {
                    	var row = $(this).closest("tr");
    			        grid = $("#grid").data("kendoGrid"),
    			        dataItem = grid.dataItem(row);
    			        //,빼고 숫자만 저장
    			        var prc=dataItem.prc.replace(/[^0-9]/g,'');
    	    			dataItem.set("totalPrc", numberWithCommas(dataItem.cnt * prc));
    	    			
                    });
                  //bert 엔터키 금액 데이터 넣기 (input.blur 써서 없어도 될듯)
    				$("#grid td").on("keyup", function (e) {
    			        //if current key is Enter
    			        var row = $(this).closest("tr");
    			        grid = $("#grid").data("kendoGrid"),
    			        dataItem = grid.dataItem(row);
    					        
    			        console.log(dataItem.cnt,dataItem.prc)
    	    			dataItem.set("totalPrc", numberWithCommas(dataItem.cnt * dataItem.prc))    		  
    				});
                  
    			    $(".k-grid th.k-header .k-link").css("color","white")				
    			    $(".k-grid-content.k-auto-scrollable").css("background","black")
    			    
        		}
	    }).data("kendoGrid");
	}
	
	//테이블 table
	function getDeliveryHistory(){
		$.showLoading(); 
		$("#checkall")[0].checked="";
		var url = "${ctxPath}/chart/getDeliveryHistory.do";
		var sDate = $("#sDate").val();
		var param = "date=" + sDate +
					"&delivererId=" + $("#deliverer").val() + 
					"&deliveryNo=" + $("#delivery_no").val(); 
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
							
				console.log(json)
				
	        var kendodata = new kendo.data.DataSource({
	        	data:json,
	            batch: true,
	            schema: {
	                   model: {
	                     id: "id",
	                     fields: {
	                        id: { editable: false},
	                        delivererId : { editable : false},
	                        lotNo: { editable : true },
	                        prdNo: { editable : true },
	                        spec : { editable : true},
	                        unit : { editable : true },
	                        totalPrc : { editable : true },
	                        cnt : { editable : true, type : "number" },
	                        deliveryDate : { editable : false },  
	                        deliveryNo: { editable : false },
	                        check: { editable: false, nullable: true },
                            Discontinued:{ type: "boolean" }, //체크박스 선택 기억하기
                            checkdisable:{ type: "boolean"}, //checkbox disable
	                     	barcode:{ editable : false, nullable: true}
	                     }
	                   }
	               }
	    	})
				
				//수정시 체크박스 초기화되서 초기화방지
				$("#wrapper .k-grid-content").off("change").on("change", "input.checkbox", function(e) {
					var grid = $("#grid").data("kendoGrid");
					dataItem = grid.dataItem($(e.target).closest("tr"));
			        dataItem.set("Discontinued", this.checked);
			    });
				
				//전체선택
				$("#checkall").click(function(e){
					var grid = $("#grid").data("kendoGrid");
					//클릭되었으면
			        if($("#checkall").prop("checked")){
			        	gridlist=grid.dataSource.data();
			        	$(gridlist).each(function(idx,data){
			        		if(data.checkdisable!=true)
			        		data.set("Discontinued", true);
			        	})	        	
			        }else{
			        	gridlist=grid.dataSource.data();
			        	$(gridlist).each(function(idx,data){
			        		if(data.checkdisable!=true)
			        		data.set("Discontinued", false);
			        	})	        		
			        }
			    })

			    $(".k-grid th.k-header .k-link").css("color","white")	
			    $(".k-grid th.k-header .k-link").css("font-size",getElSize(40))
			    $(".k-grid th.k-header .k-link").css("font-family","NotoSansCJKkrBold")
			    $(".k-grid th.k-header .k-link").css("vertical-align","middle")
			    $(".k-grid th.k-header .k-link").css("text-align","center")
			    
			
			    $(".k-grid-content.k-auto-scrollable").css("background","black")
			    
			    $("#grid th").css({
					"font-size" : getElSize(35) + "px",
					"text-align" : "center"
				})
				
				$(".k-button").css({
					"padding" : getElSize(10)
				})
				
				$(".checkbox").css({
					"width" : getElSize(70) + "px",
					"height" : getElSize(70) + "px"
				})
				kendotable.setDataSource(kendodata);
 				grid = $("#grid").data("kendoGrid");
				grid.table.on("click", ".checkbox" , selectRow);
 
				$.hideLoading();	
			
				
				
				
			}
	
		}); 
		
	}; 
	
	
	
	var selectedTr;
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}


	function readOnly(container, options) {
        container.removeClass("k-edit-cell");
        container.text(options.model.get(options.field));
      }
	

    function del(){
      var grid = $("#grid").data("kendoGrid");
      $("#grid").find("input:checked").each(function(){
        grid.removeRow($(this).closest('tr'));
      })
    }
	
	function delDeliverHistory(id){
		var url = "${ctxPath}/chart/delDeliverHistory.do";
		var param = "id=" + id;	
		console.log(id);
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				console.log(data)
			}
		});
	}
	
	function prdName(prdId) {
		for (var i = 0; i < prdNos.length; i++) {
			if (prdNos[i].prdNo == prdId) {
				return prdNos[i].ITEMNOVIEW;
			}
	    }
	}
	 
	 
	 var prdNos =[]
	 
	//bert 품번 셀렉트
	function categoryDropDownEditor(container, options) {
			
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "prdNo",
			 dataValueField: "prdNo",
			 dataSource: prdNos,
			 select: onSelect,
			 change: onChange
		});
    }
	
	function categoryDropDownEditor1(container, options) {
	$('<input required name="' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
			autoWidth: true,
			filter: "contains",
			autoBind: false,
			dataTextField: "ITEMNOVIEW",
			dataValueField: "prdNo",
			dataSource: prdNos,
			select: onSelect,
			change: onChange
	});
	} 
	 
	//bert select change 됬을때
	function onChange(e){
		var dataItem = this.dataItem(e.item);
		
		if(dataItem==undefined){
			return;
		}
		
		//bert RW 확인작업
		var prdName=dataItem.prdNo;
		var lastidx=prdName.lastIndexOf("_");
		var prdChk=prdName.substring(lastidx+1,prdName.length);
		

		
		if(prdChk!='RW'){
			alert('소재 품번이 RW가 아닙니다')
		}
		
	}
	
	var selectedRow;
	//bert 확인중
	function onSelect(e) {
		var dataItem = this.dataItem(e.item);
		
		if(dataItem==undefined){
			alert("품번을 제대로 입력해주세요")
			return;
		}
		
		var prdNo = dataItem.prdNo;

		var url = "${ctxPath}/chart/getPrcNSpec.do";
		var param = "prdNo=" + prdNo;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data.prc, data.spec)
				//var row = $('#grid').data().kendoGrid.dataSource.data()[0];
				selectedRow.set("prc", numberWithCommas(data.prc));
				selectedRow.set("spec", numberWithCommas(data.spec));
				selectedRow.set("unit", numberWithCommas(data.unit));
				selectedRow.set("totalPrc", numberWithCommas(Number(data.prc)*Number(selectedRow.cnt)));	           	
	         	
			}
		});
	 };
	
	 function getAllDeliverer(){
		var url = "${ctxPath}/chart/getAllDeliverer.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var object = {
							delivererId : data.delivererId,
							delivererName : decode(data.delivererName)
					}
					delivererArray.push(object);
				})
			},
			error : function(e1,e2,e3){
			}
		});
	 };
	 
	 function delivererName(delivererId) {
			for (var i = 0; i < delivererArray.length; i++) {
				if (delivererArray[i].delivererId == delivererId) {
					return delivererArray[i].delivererName;
				}
		    }
		}
	 
	 var delivererArray = [];

	 function categoryDropDownDelivererEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({			 
			 autoBind: false,
			 dataTextField: "delivererId",
			 dataValueField: "delivererName",
			 dataSource: delivererArray,
			 autoWidth: true
		});
    }
	 
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	 function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	}; 
	
	function getDeliverer(){
		var url = "${ctxPath}/chart/getDeliverer.do";
		var param = "delivererId=" + delivererId
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data :param,
			success : function (data){
				var json = data.chartStatus;
				
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" +decode(data.name) + "</option>"
				})				
				
				delivererId = json[0].id
				
				$("#deliverer").html(options)

				$("#deliverer").change(function(){
// 					gridTable();
					getPrdNo();
				})

				init();
			}
		});
	}
	
	var shopId = 1;
//	var handle = 0;
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId +
					"&delivererId=" + $("#deliverer").val();
		console.log("===")
		console.log(param)
		$.ajax({
			url : url,
			dataType : "json",
			data : param,
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				//prdNos = json;
				prdNos = [];
				
				var choice = {"prdNo" : "선택", "ITEMNOVIEW" : "선택"}
				prdNos.push(choice)
					
				$(json).each(function(idx, data){
					var noView;
					if(data.ITEMNO==""){
						noView = data.prdNo
					}else{
						noView = data.prdNo+" , "+data.ITEMNO
					}
					var prdNo = {"prdNo" : data.prdNo, "ITEMNOVIEW" : noView, "ITEMNO" : data.ITEMNO}
					prdNos.push(prdNo)
				});
				
				getDeliveryHistory();
			}
		});
	}
	
	$(function(){
		getDeliverer();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		getAllDeliverer();
//		createNav("inven_nav", 0);
		
//		setEl();
		setDate();	
//		time();

			
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		setEl2();
		//chkBanner();
	});
	

 	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	}; 
	
	function addRow(){
		checkedIds = {}
		grid.dataSource.add({
			"Discontinued" : "<input type='checkbox' class='checkbox' checked=checked/>"
			,"checkdisable" : true
			,"prdNo" : "선택"
			,"prc" : "0"
			,"cnt" : "0"
			,"totalPrc" : ""
			,"delivererId" : $("#deliverer").val()
			,"deliveryDate" : $("#sDate").val()
		});
		
/* 		$(".k-button").css({
			"padding" : getElSize(10)
		})
 */		
		$("#grid td").on("keyup", function (e) {
			        //if current key is Enter
			        var row = $(this).closest("tr");
			        grid = $("#grid").data("kendoGrid"),
			        dataItem = grid.dataItem(row);					        
	    			dataItem.set("totalPrc", numberWithCommas(dataItem.cnt * removeComma(dataItem.prc)))    		  
		})
	};
	
	function removeComma(str){
		return str.replace(/,/gi, "");	
	};
	
	var update_deliver_obj = [];
	
	function updateRow(){
		var obj = new Object();
		obj.val = update_deliver_obj;
	
		var url = "${ctxPath}/chart/updateHistory.do";
		
		var param = "val=" + JSON.stringify(obj);
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data
			}
		});
		
		return str;
	};
	
	function saveRow(){
		var flag=true;
		deliver_obj = [];
		update_deliver_obj = [];
		
		
		var $checked = $('input:checkbox[class=checkbox]:checked');
		//납품번호, 공급업체, 납품일자, 품번, 규격, 단위, 수량, 단가, 금액
		
		if($checked.length==0){
			alert("납품항목을 선택하지 않으셨습니다.")
			return
		}
		
		$($checked).each(function(idx, data){
			var checked = data,
	        row = $(data).closest("tr"),
	        grid = $("#grid").data("kendoGrid"),
	        dataItem = grid.dataItem(row);
			
			var prdChk=false;
			console.log(dataItem.prdNo)
			for (var i = 0; i < prdNos.length; i++) {
				if (prdNos[i].prdNo == dataItem.prdNo) {
					prdChk=true;
					break;
				}
		    }
			
			if(prdChk==false){
				alert("품번을 확인해주세요");
				flag= false;
				return flag;
			}
			
			var id = dataItem.id;
			var lotNo = dataItem.lotNo;
			var prdNo = dataItem.prdNo;
			var spec = dataItem.spec;
			var unit = dataItem.unit;
			var cnt = removeComma(String(dataItem.cnt))
			var deliveryNo = dataItem.deliveryNo;
			var totalPrc = removeComma(String(dataItem.totalPrc));
			
			if(typeof(lotNo)=="undefined"){
				alert("로트 번호가 입력되지 않았습니다.");
				flag=false;
				return flag;
			}else if(prdNo=="선택"){
				alert("품번이 입력되지 않았습니다.");
				flag=false;
				return flag;
			}
			
			var object = {};
			
			object.id = id;
			object.delivererId = $("#deliverer").val()
			object.lotNo = lotNo;
			object.prdNo = prdNo;
			object.spec = spec;
			object.unit = unit;
			object.cnt = cnt;
			object.totalPrc = totalPrc;
			object.deliveryDate = $("#sDate").val();
			object.deliveryNo = deliveryNo;
			
			if(typeof(deliveryNo)=="undefined"  || deliveryNo ==""){
				deliver_obj.push(object)
				if(id!=""){
					update_deliver_obj.push(object)
				}				
			}else{
				alert("납품서를 발행한 항목은 수정 할 수 없습니다.");
				flag=false;
				return flag;
			}
			if(flag==false) return flag;
		})
		
		var obj = new Object();
		obj.val = deliver_obj;
	
		var url = "${ctxPath}/chart/addDeliveryHst.do";
		
		var param = "val=" + JSON.stringify(obj);
		
		if(update_deliver_obj.length!=0) {
			updateRow()
		}
		
		console.log(param);
		if(flag==false) return;
		$.showLoading(); 
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					getDeliveryHistory()
				}
				$.hideLoading(); 
			},error : function(e1,e2,e3){
				console.log(e1,e2,e3)
				$.hideLoading(); 	
			}
		,
		});
	};

/* 	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	}; */
	
	function init (){
		getPrdNo();
	}
	
	
/* 	
 	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#iframe").css({
			"width" : getElSize(3500),
			"height" : getElSize(1800),
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$("#iframe").css({
			"top" : (originHeight/2) - ($("#iframe").height()/2),
			"left" : (originWidth/2) - ($("#iframe").width()/2),
		});
		
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$("#content_table td").css({
			"color" : "white"
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	}; 
*/

	

function setEl2(){
	
	var width = window.innerWidth;
	var height = window.innerHeight;
	
/* 	$("#content_table td").css({
		"color" : "white"
	});
	
	$("#container").css({
		"width" : contentWidth,
		"height" : contentHeight,
	});
	
	$("#container").css({
		"margin-left" : (originWidth/2) - ($("#container").width()/2),
		"margin-top" : (originHeight/2) - ($("#container").height()/2)
	}) */
	
	
	$("#container").css({
		"width" : contentWidth - getElSize(30)
	})
	
	$("#content_table").css({
		"margin-top" : getElSize(300)
	})
	
	
	$("#table").css({
		"position" : "absolute",
		"width" : $("#container").width(),
		"top" : getElSize(100) + marginHeight
	});
	
	$("#wrapper").css({
		"margin-left" : getElSize(50),
		"margin-right" : getElSize(50)
	});
	
	
	$("#home").css({
		"cursor" : "pointer"
	})
	
	
	$("#table span").css({
		"color" : "#8D8D8D",
		"position" : "absolute",
		"font-size" : getElSize(45),
		"margin-top" : getElSize(20),
		"margin-left" : getElSize(20)
	});
	
	$("#selected").css({
		"color" : "white",
	});
	
	$("span").parent("td").css({
		"cursor" : "pointer"
	});
	
	$(".title_span").css({
		"color" : "white",
		"font-size" : getElSize(40),
		"background-color" : "#353535",
		"padding" : getElSize(15)
	});
	
	
	$("select, button, input").css({
		"font-size" : getElSize(40),
		"margin-left" : getElSize(22),
		"margin-right" : getElSize(22)
	});
	
	$("select").css({
		"width" : getElSize(380),
		"margin-right" : getElSize(50)
	});
	
	$("#sDate").css({
		"width" : getElSize(350),
		"text-align" : "center",
		"margin-right" : getElSize(50)
	});
	
	$("input").css({
		"background-color" : "black",
		"border-color" : "black",
		"height" : getElSize(80)		
	})

	$("#td_first").css({
		"padding-left" : getElSize(50),
		"padding-bottom" : getElSize(30)
	});
	

	$(".k-header").css({
	    "font-family" : "NotoSansCJKkrBold",
	    "font-size" : getElSize(40),
	    "background-image" : "none",
	    "background-color" : "#353542"
	}); 
	
	$("button").css({
		"padding" : getElSize(10),
		"margin-left" : getElSize(7),
		"margin-right" : getElSize(30),
		"font-size" : getElSize(44),
		"background" : "#909090",
		"border-color" : "#222327"
	})
	
	$("#search").css({
		"cursor" : "pointer",
		"width" : getElSize(80),
	});
	
	$("#content_table td").css({
		"color" : "#BFBFBF",
		"font-size" : getElSize(50)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	

	$("#intro").css({
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
		
	$("#intro_back").css({
		"width" : contentWidth,
		"display" : "none",
		"height" : getElSize(180),
			"opacity" : 0.5,
		"position" : "absolute",
		"background-color" : "black",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	})
	
	
	$("#intro").css({
		"font-size" : getElSize(100)
	});

	$("#iframe").css({
		"width" : getElSize(3500),
		"height" : getElSize(1800),
		"position" : "absolute",
		"z-index" : 999,
		"display" : "none"
	});

};


	var deliver_obj = [];
	
	//납품서 발행
	function print_invoice(){
		isPreSend = false;
		var flag = true;
		deliver_obj = [];

		var $checked = $('input:checkbox[class=checkbox]:checked');
	
		if($checked.length==0){
			alert("납품항목을 선택하지 않으셨습니다.")
			return
		}
		//납품번호, 공급업체, 납품일자, 품번, 규격, 단위, 수량, 단가, 금액
		
		$($checked).each(function(idx, data){
			var checked = data,
	        row = $(data).closest("tr"),
	        grid = $("#grid").data("kendoGrid"),
	        dataItem = grid.dataItem(row);
			
			var id = dataItem.id;
			var lotNo = dataItem.lotNo;
			var prdNo = dataItem.prdNo;
			var spec = dataItem.spec;
			var unit = dataItem.unit;
			var cnt = dataItem.cnt;
			var deliveryNo = dataItem.deliveryNo;
			var prc = dataItem.prc;
			var totalPrc = dataItem.totalPrc;
			var ITEMNO="";
			if(deliveryNo!=""){
				isPreSend = true;
			}
			
			if(id==""){
				alert("데이터 저장 후 납품서를 발행해 주세요.");
				flag = false;
				return flag;
			}
			
			var object = {};
			
			object.lotNo = lotNo;
			object.prdNo = prdNo;
			object.spec = spec;
			object.unit = unit;
			object.cnt = cnt;
			object.prc = prc;
			object.totalPrc = totalPrc;
			object.id=id;
			for (var i = 0; i < prdNos.length; i++) {
				if (prdNos[i].prdNo == prdNo) {
					ITEMNO=prdNos[i].ITEMNO;
				}
		    }
			object.ITEMNO = ITEMNO;
			
			deliver_obj.push(object)
			
		})
		
		if(flag){
			if(!isPreSend){
				var deliverNo = getDeliveryNo();
				var delieverylist=[]

				$($checked).each(function(idx, data){
					var checked = data,
			        row = $(data).closest("tr"),
			        grid = $("#grid").data("kendoGrid"),
			        dataItem = grid.dataItem(row);
					
					var arr=new Object();
					var deliveryNo = dataItem.deliveryNo;
					arr.id=dataItem.id;
					arr.deliverNo=deliverNo;
					arr.lotNo=dataItem.lotNo;
					arr.deliveryDate=$("#sDate").val();
					delieverylist.push(arr);
				});
				addDeliveryDate(delieverylist,deliverNo);

//				addDeliveryDate(dataItem.id, deliverNo,dataItem.lotNo);
				
				//add delivery_date
				/* for(key in checkedIds){
					if(checkedIds[key]){
						addDeliveryDate(key, deliverNo);
					}
				} */
				console.log(deliverNo);
				console.log("===========================")
				console.log(deliver_obj)
				
			}else{
				alert("발행된 명세서가 포함되어 있습니다.");
			}
		}
	}
	
	function cancel_invoice(){
		var cancellist=[];
		var flag=true;
		var $checked = $('input:checkbox[class=checkbox]:checked');
	
		if($checked.length==0){
			alert("취소할항목을 선택하지 않으셨습니다.")
			return
		}
		//납품번호, 공급업체, 납품일자, 품번, 규격, 단위, 수량, 단가, 금액
		
		$($checked).each(function(idx, data){
			var checked = data,
	        row = $(data).closest("tr"),
	        grid = $("#grid").data("kendoGrid"),
	        dataItem = grid.dataItem(row);
			
			var id = dataItem.id;			

			if(dataItem.deliveryNo==""){
				alert("납품되지 않은 항목이 선택되었습니다.");
				flag = false;
				return flag;
			}
			
			if(id==""){
				alert("데이터 저장 후 납품서 발행취소해 주세요.");
				flag = false;
				return flag;
			}
			
			var arr=new Object();
			arr.id=id;
			cancellist.push(arr)
			
		});
		
		if(flag==false)return;

		var obj2= new Object();		
		obj2.val = cancellist;		
		
		var url="${ctxPath}/chart/cancelInvoice.do";
		var param = JSON.stringify(obj2);
		console.log(param);
		$.ajax({
			url : url,
			data : "val="+param,
			type : "post",
			dataType : "text",
			success : function(data){
				getDeliveryHistory()
				alert("발행취소 됐습니다.")
			}
		})
		
	}
	var isPreSend = false;
	function addDeliveryDate(delieverylist,deliverNo){
		var obj = new Object();
		obj.val = delieverylist;
		
		var url = "${ctxPath}/chart/addDeliveryDate.do";
		var param = "val="+ JSON.stringify(obj) ;
		
		console.log(param)		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json=data.dataList;
				for(i=0; i<deliver_obj.length; i++){
					for(j=0; j<json.length; j++){
						if(deliver_obj[i].id==json[j].id){
							deliver_obj[i].barcode=json[j].barcode;
						}
					}
				}
				createBarcode(deliverNo);
			},error:function(request,status,error){
		        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	       }

		});
	}
	
	function createBarcode(barCode){
		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			data : "deliveryCd=" + barCode,
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					checkedIds = {};
					
					$("#iframe").attr("src", "${ctxPath}/chart/printInvoice.do?json=" + encodeURIComponent(JSON.stringify(deliver_obj))).css({
						"display" : "inline",
						"background-color" : "white"
					});
					
					var close_btn = document.createElement("img");
					close_btn.setAttribute("src", "${ctxPath}/images/close_btn.png");
					close_btn.setAttribute("id", "close_btn");
					
					$("body").append(close_btn);						
											
					$("#close_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(100),
						"z-index" : 9999
					}).click(function(){
						$(this).remove();
						$("#print_btn").remove();
						$("#iframe").css("display" , "none")
					})
							
					
					var print_btn = document.createElement("img");
					print_btn.setAttribute("src", "${ctxPath}/images/printer.png");
					print_btn.setAttribute("id", "print_btn");
					
					$("body").append(print_btn);						
											
					$("#print_btn").css({
						"width" : getElSize(100) + "px",
						"height" : getElSize(100) + "px",
						"position" : "absolute",
						"cursor" : "pointer",
						"top" : $("#iframe").offset().top,
						"left" : $("#iframe").offset().left + $("#iframe").width() - getElSize(250),
						"z-index" : 9999
					}).click(function(){
						var frm = document.getElementById("iframe").contentWindow;
						frm.focus();// focus on contentWindow is needed on some ie versions
						frm.print();	
					})
					
					getDeliveryHistory(); 
				}
				
			}
		});
	}
	
	function getDeliveryNo(){
		var url = "${ctxPath}/chart/getDeliveryNo.do";
		var param = "sDate=" + $("#sDate").val()
		
		var str;
		$.ajax({
			url : url,
			data : param,
			async :false,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data
				
				$(deliver_obj).each(function(idx, data){
					data.deliveryNo = str
					data.deliverer =  encodeURIComponent($("#deliverer option:selected").text());
					data.deliverDate = $("#sDate").val();
				})
			}
		});
		
		return str;
	}
	
/* 	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	}; */
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<iframe id="iframe"></iframe>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td id="td_first"> 
								<spring:message code="deliverer" ></spring:message>
								<select id="deliverer"></select>
								<spring:message code="Delivery_date" ></spring:message>
								<input type='text' id='sDate' class="date" >
								<spring:message code="Delivery_number" ></spring:message>
								<input type="text" id="delivery_no" style="color:#ffffff;"> 
								<button id="addBtn" onclick="addRow()"><spring:message code="add" ></spring:message></button> 
								<button onclick="saveRow()"><spring:message code="save" ></spring:message></button>
								<button onclick="print_invoice()"><spring:message code="delivery_note" ></spring:message></button>
								<button onclick="cancel_invoice()"><spring:message code="delivery_note" ></spring:message><spring:message code="cancel_" ></spring:message></button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="wrapper">
									<div id="grid">
										
									</div>																		
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			
		</table>
	 </div>
	 
	<script>
	
	</script>

	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	