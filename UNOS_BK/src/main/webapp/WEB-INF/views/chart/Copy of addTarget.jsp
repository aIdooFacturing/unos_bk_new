<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<spring:message code="start_time" var="start_time"></spring:message>
<spring:message code="end_time" var="end_time"></spring:message>
<spring:message code="fixed_time" var="fixed_time"></spring:message>
<spring:message code="alarm" var="alarm"></spring:message>
<spring:message code="content" var="content"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var valueArray = [];
function saveRow(){
	valueArray = [];
	$(".contentTr").each(function(idx, data){
		var obj = new Object();
		obj.dvcId = $(data).children("td:nth(0)").children("input").val();
		obj.tgCyl = $(data).children("td:nth(1)").children("input").val();
		obj.tgRunTime = $(data).children("td:nth(2)").children("input").val()*3600;
		obj.tgDate = getToday();
		obj.type = 2;
		obj.cntPerCyl = $(data).children("td:nth(3)").children("input").val()
		
		valueArray.push(obj);
		
		var obj = new Object();
		obj.dvcId = $(data).children("td:nth(0)").children("input").val();
		obj.tgCyl = $(data).children("td:nth(4)").children("input").val();
		obj.tgRunTime = $(data).children("td:nth(5)").children("input").val()*3600;
		obj.tgDate = getToday();
		obj.type = 1;
		obj.cntPerCyl = $(data).children("td:nth(6)").children("input").val()
		
		valueArray.push(obj)
		
	});
	
	var obj = new Object();
	obj.val = valueArray;
	
	var url = "${ctxPath}/order/addTargetCnt.do";
	var param = "val=" + JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function (data){
			if(data=="success"){
				alert("저장 되었습니다.");
			}		
		}
	});
};

function getTargetData(){
	var url = "${ctxPath}/order/getTargetData.do";
	
	var param = "shopId=" + shopId + 
				"&date=" + $("#date").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var  json = data.dataList;
			
			var tr = "<thead>" +
						"<tr>" + 
							"<td rowspan='2'>${device}</td><td colspan='3'>주간</td><td colspan='3'>야간</td>" +
						"</tr>" + 
						"<tr>" + 
							"<td style='text-align: center;'>생산량</td><td style='text-align: center;'>${ophour} (h)</td><td style='text-align: center;'>사이클당 생산량</td>" +	
							"<td style='text-align: center;'>생산량</td><td style='text-align: center;'>${ophour} (h)</td><td style='text-align: center;'>사이클당 생산량</td>" +
						"</tr>" + 
					"</thead><tbody>";
			var class_name = "";
			$(json).each(function(idx, data){
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				class_name = " ";

				var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
				
				tr += "<tr class='" + className + " contentTr'>" + 
						"<td><input type='hidden' value='" + data.dvcId + "'>" + name + "</td>" +
						"<td><input type='text' value='" + data.tgCntD + "'></td>" +
						"<td><input type='text' value='" + data.tgRunTimeD/60/60 + "'></td>" +
						"<td><input type='text' value='" + data.cntPerCylD + "'></td>" +
						"<td><input type='text' value='" + data.tgCntN + "'></td>" +
						"<td><input type='text' value='" + data.tgRunTimeN/60/60 + "'></td>" +
						"<td><input type='text' value='" + data.cntPerCylN + "'></td>" +
				  "</tr>";
			});
			
	
			tr += "</tbody>";
			
			$(".wrapper").css("z-index" , -99999)
			
			$("#wrapper .targetTable").html(tr);
			$("#wrapper .targetTable td").css("font-size",getElSize(60));
			$("#wrapper .targetTable td input").css({
				"font-size" : getElSize(60),
				"width" : getElSize(200)
			});
			
			$("#wrapper").css("z-index", 9999);
			
			$("#wrapper div:last").remove();
			scrolify($("#wrapper .targetTable"), getElSize(1550));
			$("#wrapper div:last").css("overflow", "auto")
			
			

			$(".save_btn").click(function(){
				var img = document.createElement("img");
				img.setAttribute("id", "loading_img");
				img.setAttribute("src", "${ctxPath}/images/load.gif");
				img.style.cssText = "width : " + getElSize(500) + "; " + 
									"position : absolute;" + 
									"z-index : 99999999;" + 
									"border-radius : 50%;"
									
				$("body").prepend(img);
				$("#loading_img").css({
					"top" : (window.innerHeight/2) - ($("#loading_img").height()/2),
					"left" : (window.innerWidth/2) - ($("#loading_img").width()/2)
				});
				
			});
			
			$(".targetCnt").css({
				"font-size" : getElSize(40),
				"width" : getElSize(250),
				"outline" : "none",
				"border" : "none"
			});
			
			$(".span").css({
				"font-size" : getElSize(40),
				"color" : "red",
				"margin-left" : getElSize(10),
				"font-weight" : "bolder"
			});
			
			$(".save_btn").css({
				"background-color" : "white",
				"color" : "black",
				"border-radius" : getElSize(10),
				"font-weight" : "bolder",
				"padding" : getElSize(10)
			});
			
			
			$(".tdisable").each(function(idx, data){
				this.disabled = true;
				this.value = "";
			});
			
			$(".tdisable").css({
				"background-color" : "rgba(	4,	238,	91,0.5)"
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222",
				"font-size" : getElSize(40)
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232",
				"font-size" : getElSize(40)
			});
			
		}
	});
};

function copyRow(){
	$(".contentTr").each(function(idx, data){
		$(data).children("td:nth(4)").children("input").val($(data).children("td:nth(1)").children("input").val());
		$(data).children("td:nth(5)").children("input").val($(data).children("td:nth(2)").children("input").val())
		$(data).children("td:nth(6)").children("input").val($(data).children("td:nth(3)").children("input").val())
	});
};

function getLatestDate(){
	var url = "${ctxPath}/chart/getLatestDate.do";
	
	$.ajax({
		url : url,
		dataType : "text",
		type : "post",
		success : function(data){
			$("#date").val(data);
			getTargetData();	
		}
	});
};

$(function(){
	//$("#date").val(getToday());
	getLatestDate();
	//getTargetData("day");
	$(".excel").click(csvSend);
	setEl();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu10").removeClass("unSelected_menu");
	$("#menu10").addClass("selected_menu");
	
});

var className = "";
var classFlag = true;

var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$("#date, button").css({
		"font-size" :getElSize(50)
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		//"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".targetTable td").css({
		"border": getElSize(5) + "px solid black"
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$(".wrapper").css({
		"width" : "95%",
		"margin-top" : getElSize(120),
		"position" : "absolute"	
	});
	
	$(".wrapper").css({
		"left" : (window.innerWidth/2) - ($(".wrapper").width()/2) 
	});
	
	chkBanner();
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

</script>
</head>

<body >
	<!-- <div id="machineListForTarget_day" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>
	<div id="machineListForTarget_night" style="opacity:0" class="machineListForTarget">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>		 -->
	
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title"> </div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu"><spring:message code="layout"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu7" class="menu"><spring:message code="devicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu4" class="menu"><spring:message code="dailydevicestatus"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu1" class="menu"><spring:message code="analsysperformance"></spring:message> </td> <!-- 장비별 가동실적분석 -->
				</tr>
				<tr>
					<td id="menu8" class="menu"><spring:message code="performancegraph1"></spring:message></td>
				</tr>
				<tr>
					<td id="menu9" class="menu"><spring:message code="performancegraph2"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu12" class="menu">일생산 실적 분석 (표)</td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu6" class="menu"><spring:message code="toolmanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr> 
				<tr>
					<td id="menu11" class="menu"><spring:message code="prdctboard"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu10" class="menu"><spring:message code="addprdctgll"></spring:message> </td>
				</tr>
				<tr>
					<td id="menu5" class="menu"><spring:message code="tracemanagement"></spring:message> </td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu3" class="menu"><spring:message code="24barchart"></spring:message></td><!-- 무인장비 가동현황 -->
				</tr>
				<tr>
					<td id="menu2" class="menu"><spring:message code="alarmhistory"></spring:message> </td> <!--알람 내역 -->
				</tr>
				<tr>
					<td id="menu100" class="menu">재고 현황</td> 
				</tr>
				<tr>
					<td id="menu99" class="menu">Catch Phrase 관리</td> 
				</tr>
				<tr>
					<td id="menu101" class="menu">프로그램별 가공이상 분석</td> 
				</tr>
				<tr>
					<td id="menu102" class="menu">제조 리드타임</td> 
				</tr>
				<tr>
					<td id="menu103" class="menu">고객불량율 / 공정뷸량율</td> 
				</tr>
				<tr>
					<td id="menu104" class="menu">불량등록</td> 
				</tr>
			</table>
	</div>
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder; " class="title" >
							일 생산 계획 등록
						</Td>
					</tr>
				</table>
				
				<div style="float: right;">
					<input type="date" id="date"> <button onclick="getTargetData()">조회</button> <button onclick="saveRow()">저장</button> <button onclick="copyRow()">데이터 복사</button>
				</div>
				<div>
					<center>
					<span class='daynight_span day' id='day'>주간</span>&nbsp;&nbsp;&nbsp; 
					<span class='daynight_span night' id='night'>야간</span>
					</center>
				</div>
				<img  id='save_btn' class='save_btn' src="${ctxPath }/images/save.png" style="display: none" />
				<div id="wrapper" class="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" id="target" class="targetTable" border="1">
					</table>
				</div>
		</div>
	</div>
</body>
</html>