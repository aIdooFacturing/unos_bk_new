<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>
<style>
*{
	margin: 0px;
}

tr, td {
	padding-top: 3px;
	padding-bottom:3px;
}

  

td.table_title {
	padding-left: 25px;
}
body{
/* 	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
  	font-family:'Helvetica';
  	 */
  		background-color: black;
}
html{
	overflow : hidden;

</style> 
<script type="text/javascript">
	
const loadPage = () =>{
	createMenuTree("qm", "addFaulty")
	
}
	var workerCd = "${workerCd}"
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(el, val){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		el.val(year + "-" + month + "-" + day);
		if(typeof(val)!="undefined"){
			el.val(val);	
		}
	};
	
	
	$(function(){
//		createNav("quality_nav", 2);
		
		setEl();
//		time();


		    
	//	$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		addRow();
		
		$("#oprNm_form select").change(function(){
			if($("#oprNm_form select").val()=="0" || $("#prdNo_form select").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form select").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form select").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form select").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form select").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form select").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form select").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
		$("#prdNo_form select").change(function(){
			if($("#oprNm_form select").val()=="0" || $("#prdNo_form select").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form select").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form select").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form select").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form select").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form select").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form select").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
		
	function setEl(){
		
	var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
	
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : marginWidth + getElSize(500)
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			//"border": getElSize(5) + "px solid black"
		});
	
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(48),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});

		$("select").css({
            "border": "1px black #999",
            "width" : getElSize(600),
            "height" : getElSize(80),
            "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
        })
        
        $("select option").css({
            "background-color" : "rgba(0,0,0,5)",
            "color" : "white",
            "border" : "1",
            "transition-delay" : 0.05,
            "border-bottom" : getElSize(10) + "px solid red",
            "text-align-last": "center",
/*             "border-top-color": "rgb(215, 0, 0)",
            "border-right-color": "rgb(215, 0, 0)",
            "border-left-color": "rgb(215, 0, 0)", */
            "padding" : getElSize(15),
            "font-size": getElSize(48),
            "transition" : "1s"
        })
	
        $("input").css({
            "border": "1px black #999",
            "width" : getElSize(600),
            "height" : getElSize(80),
            "font-family": "inherit",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none",
            "font-size" : getElSize(48),
			"border-color" : "#222327"
        })
        
        $("button").css({
    			"font-size" : getElSize(48),
    			"width" : getElSize(500),
    			"height" : getElSize(96),
    			"border-color" : "#6699F0",
    			"background" : "#6699F0",
    			"padding-top" : getElSize(20),
    			"padding-bottom" : getElSize(20),
    			"margin-top" : getElSize(80),
    			"margin-bottom" : getElSize(80),
    			"border-radius" : getElSize(8)
    		
        })
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			//"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"position" : "absolute",
			"width" : getElSize(2800),
			"height" : getElSize(1750),
			"z-index" : 999,
		});
		
		$(".table_title").css({
			"width" : getElSize(500)
		});

		$("#insertForm table td").css({
			"font-size" : getElSize(48),
			"padding" : getElSize(15),
			"background-color" : "#1E1E23",
			"color" : "white",
		});

		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	
		
		
        
	}
	
	var valArray = [];

	function saveRow2(){
		$.showLoading()
		valArray = [];
		
		var obj = new Object();
		
		obj.chkTy = $("#chkTy_form select").val();	//검사구분
		obj.prdNo = $("#prdNo_form select").val();	//품번
		obj.prdPrc = $("#oprNm_form select").val();	//공정
		obj.dvcId = $("#dvcId_form select").val();	//장비
		obj.date = $("#date_form input").val();	//날짜
		obj.checker = $("#checker_form select").val();	//신고자
		obj.part = $("#part_form select").val();	//부위
		obj.situationTy = $("#situationTy_form select").val();	//현상
		obj.situation = $("#situation_form select").val();	//현상구분
		obj.cause = $("#cause_form select").val();	//원인
		obj.gchTY = $("#gchTy_form select").val();	//귀책구분
		obj.gch = $("#gch_form select").val();	//귀책
		obj.cnt = $("#cnt_form input").val();	//수량
		obj.action = $("#action_form select").val();	//조치
		obj.sendCnt = $("#sendCnt_form input").val();
		
		if($("#oprNm_form select").val()=="20"){
			obj.proj="0000"
		}else if($("#oprNm_form select").val()=="20"){	//소재 자재
			obj.proj="0000"
		}else if($("#oprNm_form select").val()=="21"){	//라인 공정
			obj.proj="0005"
		}else if($("#oprNm_form select").val()=="22"){	//R	공정
			obj.proj="0010"
		}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
			obj.proj="0020"
		}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
			obj.proj="0030"
		}else if($("#oprNm_form select").val()=="25"){	//완성창고
			obj.proj="0090"
		}else if($("#oprNm_form select").val()=="26"){	//고객사
			obj.proj="0"
		}else if($("#oprNm_form select").val()=="27"){	//필드
			obj.proj="0"
		}
		valArray.push(obj);
		
		var obj = new Object();
		obj.val = valArray;
		
		var url = "${ctxPath}/chart/saveRow.do";
		var param = "val=" + JSON.stringify(obj);
		
		console.log(param)
		
		/* if($("#chkTy_form select").val()=="0"){
			alert("검사구분 선택하십시오.");
			$("#chkTy_form").focus();
			$.hideLoading();
			return;
		}else  */
		
		if(Number($("#exCnt").html()) < Number($("#cnt_form input").val()) || Number($("#exCnt").html()) < Number($("#sendCnt_form input").val())){
			alert("재고보다 수량이 많을수 없습니다.");
			$.hideLoading();
			return;
		}
		
		if($("#prdNo_selector_form").val()=="0"){
			alert("품번을 선택하십시오.");
			$("#prdNo_selector_form").focus();
			$.hideLoading()
			return;
		}else if($("#prdNo_selector_form").val().indexOf("RW")==-1 && ($("#oprNm_form select").val()==20 || $("#oprNm_form select").val()==21)){
			alert("완성품번일때 소재,라인창고를 선택하실수 없습니다.")
			$.hideLoading()
			return;
		}else if($("#prdNo_selector_form").val().indexOf("RW")!=-1 && ($("#oprNm_form select").val()!=20 && $("#oprNm_form select").val()!=21)){
			alert("소재일때는 소재,라인창고만 선택 가능합니다.")
			$.hideLoading()
			return;
		}
		else if($("#oprNm_form select").val()=="0"){
			alert("공정을 선택하십시오.");
			$("#oprNm_form select").focus();
			$.hideLoading()
			return;
		}/* else if($("#dvcId_form select").val()=="0"){
			alert("장비를 선택하십시오.");
			$("#dvcId_form select").focus();
			$.hideLoading()
			return;
		}else if($("#date_form select").val()=="0"){
			alert("날짜를 선택하십시오.");
			$("#date_form select").focus();
			$.hideLoading()
			return;
		} */else if($("#checker_form select").val()=="0"){
			alert("신고자를 선택하십시오.");
			$("#checker_form select").focus();
			$.hideLoading()
			return;
		}/* else if($("#part_form select").val()=="0"){
			alert("부위를 선택하십시오.");
			$("#part_form select").focus();
			$.hideLoading()
			return;
		} */else if($("#situation_form select").val()=="0"){
			alert("현상구분 선택하십시오.");
			$("#situation_form").focus();
			$.hideLoading();
			return;
		}else if($("#situationTy_form select").val()=="0"){
			alert("현상을 선택하십시오.");
			$("#situationTy_form select").focus();
			$.hideLoading()
			return;
		}/*else if($("#cause_form select").val()=="0"){
			alert("원인을 선택하십시오.");
			$("#cause_form select").focus();
			$.hideLoading()
			return;
		}else if($("#gchTy_form select").val()=="0"){
			alert("귀책구분을 선택하십시오.");
			$("#gchTy_form select").focus();
			$.hideLoading()
			return;
		}else if($("#gch_form select").val()=="0"){
			alert("귀책을 선택하십시오.");
			$("#gch_form select").focus();
			$.hideLoading()
			return;
		} */else if($("#cnt_form input").val()=="0" || $("#cnt_form input").val()==0){
			alert("수량을 입력하세요.");
			$("#cnt_form input").focus();
			$("#cnt_form input").select();
			$.hideLoading()
			return;
		}/* else if($("#action_form select").val()=="0"){
			alert("조치을 선택하십시오.");
			$("#cnt_form select").focus();
			$.hideLoading()
			return;
		} *//* else if($("#sendCnt_form input").val()=="0" || $("#sendCnt_form input").val()==0){
			alert("수량을 입력하세요.");
			$("#sendCnt_form input").focus();
			$("#sendCnt_form input").select();
			$.hideLoading()
			return;
		} */else if(isNaN($("#cnt_form input").val())){
			alert("수량은 숫자만 입력가능합니다.");
			$("#cnt_form input").focus();
			$("#cnt_form input").select();
			$.hideLoading()
			return;
		}else if(isNaN($("#sendCnt_form input").val())){
			alert("수량은 숫자만 입력가능합니다.");
			$("#sendCnt_form input").focus();
			$("#sendCnt_form input").select();
			$.hideLoading()
			return;
		}else if($("#cnt_form input").val() < $("#sendCnt_form input").val()){
			alert("불량등록 수량보다 이동수량이 많을수 없습니다.");
			$("#sendCnt_form input").focus();
			$("#sendCnt_form input").select();
			$.hideLoading()
			return;
		}
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					addRow();
					alert("${save_ok}");
				}
				$.hideLoading()
			}
		});	
	};
	
	function addRow(){
		if(classFlag){
			className = "row2"
		}else{
			className = "row1"
		};
		classFlag = !classFlag;
		
		var checkTy = document.createElement("select");
		var prdNo = document.createElement("select");
		prdNo.setAttribute("id", "prdNo_selector_form");
		var process = document.createElement("select");
		var device = document.createElement("select");
		var worker = document.createElement("select");
		var sDate = document.createElement("input");
		sDate.setAttribute("class", "date");
		sDate.setAttribute("type", "date");
		sDate.setAttribute("readonly", "readonly");
		var text = document.createElement("input");
		
		text.style.cssText = "width : " + getElSize(400);
		var cnt = document.createElement("input");
		cnt.setAttribute("class", "cnt");
		cnt.setAttribute("size", 5);
		var part = document.createElement("select");
		var situationTy = document.createElement("select");
		var situation = document.createElement("select");
		var cause = document.createElement("select");
		var gchTY = document.createElement("select");
		var gch = document.createElement("select");
		var action = document.createElement("select");
		var sendCnt = document.createElement("input");
//		sendCnt.setAttribute("class", "cnt");
		sendCnt.setAttribute("size", 5);
		
		var tr = document.createElement("tr");
		tr.setAttribute("class", className);
		
		var checkTy_td = document.createElement("td");
		checkTy_td.append(checkTy);
		tr.append(checkTy_td);
		$("#chkTy_form").html(checkTy_td)
		
		var prdNo_td = document.createElement("td");
		prdNo_td.append(prdNo);
		tr.append(prdNo_td);
		$("#prdNo_form").html(prdNo_td)
		
		var process_td = document.createElement("td");
		process_td.append(process);
		tr.append(process_td);
		$("#oprNm_form").html(process_td)
		
		var device_td = document.createElement("td");
		device_td.append(device);
		tr.append(device_td);
		$("#dvcId_form").html(device_td)
		
		var sDate_td = document.createElement("td");
		sDate_td.append(sDate);
		tr.append(sDate_td);
		$("#date_form").html(sDate_td)
		
		var text_td = document.createElement("td");
		text_td.append(worker);
		tr.append(text_td);
		$("#checker_form").html(text_td)
		
		var part_td = document.createElement("td");
		part_td.append(part);
		tr.append(part_td);
		$("#part_form").html(part_td)
		
		var situ_td = document.createElement("td");
		situ_td.append(situationTy);
		tr.append(situ_td);
		$("#situation_form").html(situ_td)
		
		var situTy_td = document.createElement("td");
		situTy_td.append(situation);
		tr.append(situTy_td);
		$("#situationTy_form").html(situTy_td)
		
		var cause_td = document.createElement("td");
		cause_td.append(cause);
		tr.append(cause_td);
		$("#cause_form").html(cause_td)
		
		var gchkTy_td = document.createElement("td");
		gchkTy_td.append(gchTY);
		tr.append(gchkTy_td);
		$("#gchTy_form").html(gchkTy_td)
		
		var gch_td = document.createElement("td");
		gch_td.append(gch);
		tr.append(gch_td);
		$("#gch_form").html(gch_td)
		
		var cnt_td = document.createElement("td");
		cnt_td.append(cnt);
		tr.append(cnt_td);
		$("#cnt_form").html(cnt_td)
		
		var action_td = document.createElement("td");
		action_td.append(action);
		tr.append(action_td); 
		$("#action_form").html(action_td)

		var sendCnt_td = document.createElement("td");
		sendCnt_td.append(sendCnt);
		tr.append(sendCnt_td); 
		$("#sendCnt_form").html(sendCnt_td)
		
		var button_td = document.createElement("td");
		var button = document.createElement("button");
		//button.setAttribute("id", "b" + data.id);
		var button_text = document.createTextNode("삭제");
		button.append(button_text);
		button_td.append(button)
		tr.append(button_td);
		
		//$("#tbody").append(tr);
// 		여기여기
		getCheckTyList($(checkTy));
		getProcessList($(process));
		getDevieList($(device));
		getWorkerList($(worker));
		setDate($(sDate));
		getPartList($(part));
		getSituationTyList($(situationTy));
		console.log("mmㅡㅡ")
		console.log($(situation))
		getSituationList(111,$(situation));
		getCauseList($(cause));
		getGChTyList($(gchTY));
		getGChList(7, $(gch));
		getActionList($(action)); 
		getPrdNoList($(prdNo));
		$(cnt).val(0);
		$(sendCnt).val(0)
		
		
		$(".row1").not(".tr_table_fix_header").css({
			"background-color" : "#222222"
		});

		$(".row2").not(".tr_table_fix_header").css({
			"background-color": "#323232"
		});
		
		setEl();
		
		showCorver();
		$("#insertForm").css("z-index", 999)
		
		if(addFaulty!=""){
			$("#prdNo_selector_form option[value='" + $prdNo + "']").attr("selected", "selected");
			$("#cnt_form").children("td").children("input").val($cnt);
		}
		
		//setEl();
		
		$( ".date" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				//getDeliveryHistory();
				$(".date").val(e);
			}
		})
	};
	
	function getGch(){
		var val = $(this).val();
		var ty;
		if(val=="47"){			//업체
			ty = "com";
		}else if(val=="48"){	//작업자  
			ty = "worker"	
		}else if(val=="64"){
			ty = "dvc";
		};
		console.log(ty,val)
		getGChList(ty, $("#gch_form").children("td").children("select"));
	};
	
	var className = "";
	var classFlag = true;

	var menu = false;

	function getProcessList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 3; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPartList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 4;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};
	function getSituationPart(){
		console.log("dd")
		
		var val = $(this).val();
		var ty;
		
		getSituationList(val,$("#situationTy_form").children("td").children("select"))
	}
	function getSituationTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 5;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log("---현상구분---")
				console.log(json);
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getSituationPart);
//				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCnt(el, id){
		var url = ctxPath + "/chart/getCnt.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(data);
			}
		});	
	};

	function getChecker(el, id){
		var url = ctxPath + "/chart/getChecker.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(decode(data));
			}
		});	
	};

	function getSituationList(val, el){
		//val==>  30 ->소재불량 31 ->가공불량 32 ->기타
		
		// index =6 이면   or 14 이면
		var index;
		if(val=="30"){
			index=6;
		}else if(val=="31"){
			index=14
		}else if(val=="32"){
			index=15
		}else if(val=="201"){
			index=17
		}
		else{
			el.html("<option value='0'>${selection}</option>");
			return; 
		}
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + index +
					"&chkTy=" +val;
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var json = data.dataList;
				
				console.log("---현상---")
				console.log(json)
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				$("#situationTy_form select").val(0)
				el.html(options).val();
				$("#situationTy_form select").val(0)
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				$("#situationTy_form select").val(0)
				
			}
		});	
	};

	function getCauseList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getGch);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChList(ty, el){
		//var url = ctxPath + "/chart/getCheckTyList.do"
		var url;
		if(ty=="com"){
			url = ctxPath + "/chart/getComList.do"		
		}else if(ty=="worker"){
			url = ctxPath + "/common/getWorkerList.do"
		}else if(ty=="dvc"){
			url = ctxPath + "/common/dvcList.do"
		}else{
			el.html("<option value='0'>${selection}</option>");
			return; 
		}
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				console.log(json)
				$(json).each(function(idx, data){
					if(ty=="dvc"){
						options += "<option value='" + data.dvcId + "'>" + decode(data.dvcName) + "</option>";						
					}else{
						options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					}
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getActionList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 10;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCheckTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 2;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	var comPrdList;
	function getPrdNoList(el, val){
		var url = ctxPath + "/common/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				comPrdList=json;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				el.html(options);
				//품번변경시 장비라우팅 부르기
//				el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});
	};

	var addFaulty = "${addFaulty}";
	function getDevieList(el, obj){
		var url = ctxPath + "/common/dvcList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $(obj).val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.dvcId + "'>" + decode(data.dvcName) + "</option>";
				});
				
				el.html(options);
			}
		});	
	};
	
	function getWorkerList(el, obj){
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var options = "<option value='0'>${selection}</option>";	

				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				
				el.html(options);
			}
		});	
	};
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>			
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="insertForm">
						<table style="width: 100%; ">
							<Tr> 
								<Td class='table_title'> <spring:message code="prd_no"></spring:message> * </Td> <Td id="prdNo_form"> </Td> <Td class='table_title' width="20%"> <spring:message code="check_ty"></spring:message>  </Td> <Td id="chkTy_form"  width="30%"></Td>
							</Tr>
							<Tr> 
								 <Td class='table_title'  width="22%"><spring:message code="operation"></spring:message> * </Td> <Td id="oprNm_form"></Td>  <Td class='table_title'> <spring:message code="device"></spring:message> </Td> <Td id="dvcId_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="reporter"></spring:message> * </Td> <Td id="checker_form"></Td> <Td class='table_title'> <spring:message code="event_date"></spring:message> </Td> <Td id="date_form"></Td> 
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="divide_situ"></spring:message> * </Td>  <Td id="situation_form"></Td> <Td class='table_title'> <spring:message code="part"></spring:message> </Td> <Td id="part_form"></Td> 
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="situ"></spring:message> * </Td> <Td id="situationTy_form"></Td> <Td class='table_title' > <spring:message code="cause"></spring:message> </Td> <Td id="cause_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'>  <spring:message code="gch_ty"></spring:message> </Td> <Td id="gchTy_form"></Td> <Td class='table_title'> <spring:message code="gch"></spring:message> </Td> <Td id="gch_form"></Td>
							</Tr>
							<Tr> 
								<Td class='table_title'> <spring:message code="count"></spring:message> * (<label id="exCnt">0</label>)</Td> <Td id="cnt_form"></Td> <Td class='table_title'> <spring:message code="action"></spring:message>  </Td> <Td id="action_form"></Td>
							</Tr>
							<Tr>
								<Td class='table_title'> 불량창고이동수량 * </Td> <Td id="sendCnt_form">  </Td> <Td> </Td> <Td> </Td>
							</Tr>
							<Tr>
								<Td colspan="4" style="text-align: center; padding-right:90px"><button onclick="saveRow2()"><spring:message code="save"></spring:message> </button></Td>
							</Tr>
						</table> 
					</div>
				</td>
			</Tr>
			
		</table>
	 </div>
	 
	
</body>
</html>	