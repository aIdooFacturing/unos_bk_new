<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script src="https://code.jquery.com/jquery-3.1.0.min.js" ></script>
<script type="text/javascript" src="${ctxPath }/template/unos_template.js"></script>
<script type="text/javascript" src="${ctxPath }/js/adapter_manager.js"></script>
</head>
<body>
	<div id="container">
		<time></time>
		<center><h1>Adapter 관리</h1></center>

		<table class="unos_table" id="search_table">
			<tr>
				<td class="table_title center-align" width="40%">Serial No.</td>
				<td class="center-align"><input type="text" id="serialNo" class="unos_input"> </td>
			</tr>
		</table>
		<div id="adapter_list">
			<table class="unos_table">
				
			</table>
		</div>
		<br>
		<br>
		
		<table class="unos_table">
			<tr>	
				<td  class="table_title table_label">
					Serial No.
				</td >
				<td id="serial_no"  colspan="2" >
				
				</td>
				<Td  class="table_title table_label" >
					Machine No.
				</Td>
				<td colspan="2" >
					<select class="unos_select" id="dvcId"><option>Menu1</option> <option>Menu2</option><option>Menu3</option></select>
				</td>
			</tr>
			<tr>
				<Td class="table_title">
					Firmware Version
				</Td>
				<td id="firm_ver"  >
					
				</td>
				<td class="table_title table_label" >
					IP
				</td>
				<td id="ip">
					
				</td>
				<td  class="table_title table_label">
					Mac Addr
				</td>
				<td id="macAddr"  >
				
				</td>
			</tr>
			<tr>
				<td class="table_title">
					Agent App Version
				</td>
				<td id="agent_app_ver">
				
				</td>
				<td class="table_title">
					Agent App
				</td>
				<td colspan="3">
					<button class="unos_btn">관리</button> 
				</td>
			</tr>
			<tr>
				<td class="table_title">
					Adapter App Version
				</td>
				<td id="adapter_app_ver">
				
				</td>
				<td class="table_title">
					Adapter App
				</td>
				<td colspan="3">
					<button class="unos_btn">관리</button> 
				</td>
			</tr>
			<tr>
				<td class="table_title">
					Center App Version
				</td>
				<td id="center_app_ver">
				
				</td>
				<td class="table_title">
					Server Setting App
				</td>
				<td colspan="3">
					<button class="unos_btn">관리</button> 
				</td>
			</tr>
			<tr>
				<Td class="table_title">
					Remarks
				</Td>
				<Td colspan="5">
					<textarea rows="" cols="" id="remark"></textarea>
				</Td>
			</tr>
			<tr>
				<Td class="table_title">
					Writer
				</Td>
				<td id="writer">
				
				</td>
				<Td class="table_title">
					Reg Date
				</Td>
				<td id="regDate" colspan="3">
				
				</td>
			</tr>
			<tr>
				<Td class="table_title">
					Modifier
				</Td>
				<td id="modifier">
				
				</td>
				<Td class="table_title">
					Modification Date
				</Td>
				<td id="updateDate" colspan="3">
				
				</td>
			</tr>
		</table>
	</div>
</body>
</html>