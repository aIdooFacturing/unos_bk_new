<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}


#wrapper{
	border-color : #1E1E23 !important;
}

#grid{
	border-bottom : 1px solid #1E1E23  !important;
	border-right : 1px solid #1E1E23 !important;
	border-left : 1px solid #1E1E23  !important;
	border-top : 1px solid #1E1E23 !important;
}
.k-grid-header{
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
}
.k-grid-header-wrap{
	border-right : 1px solid #1E1E23 !important;
}

.k-grid thead tr th{
    background : #353542;
    border-color : #1E1E23;
    color: white;
    text-align: center  !important;
}
.k-grid tbody > tr
{
 background : #DCDCDC ;
 text-align: center;
}
.k-grid tbody > .k-alt
{
 background : #F0F0F0 ;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background : #6699F0;
}

td {
	border-bottom : 1px solid #1E1E23  !important;
	border-left : 1px solid #1E1E23 !important;
  }
  

</style> 
<script type="text/javascript">



const loadPage = () =>{
	createMenuTree("qm","lotTracer")
}
	var grid;
	
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(el, val){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		el.val(year + "-" + month + "-" + day);
		if(typeof(val)!="undefined"){
			el.val(val);	
		}
	};

	$(function(){
	
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#table").css({
			"position" : "absolute",
			"bottom" : marginHeight,
			"height" : getElSize(1750),
			"left" : getElSize(40) + marginWidth,
		
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
	
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		
		$("button").css({
			"height" : getElSize(80),
			"width" : getElSize(240),
			"border-color" : "#9B9B9B",
			"background" : "#9B9B9B",
			"border-radius" : getElSize(0),
			"font-size" : getElSize(48)
		})
		
		$("input").css({
            "border": "1px black #999",
            "width" : getElSize(500),
            "height" : getElSize(80),
            "font-family": "inherit",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none",
            "font-size" : getElSize(48),
			"border-color" : "#222327"
        })
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#table tbody tr td table tbody tr td").css({
			 "height" : getElSize(128),
		    "padding-top" : getElSize(20),
			"padding-left" : getElSize(20),
			"padding-right" : getElSize(20),
			"padding-bottom" : getElSize(20),
			"background" : "#2B2D32"
		})
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white",
			"padding" : getElSize(10)
		});
		
		$(".alarmTable, .alarmTable tr, .alarmTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".alarmTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100),
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#grid").css({
			"width" : $("#container").width() - getElSize(130),
			"height" : getElSize(1650)
		})
	};
	
	function getData(){
		var url;
		var	lotNo = $("#lotNo").val();
		var	url = ctxPath + "/chart/getLotTracer.do";
		
		var param = "lotNo=" + lotNo;
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				if(json.length==0){
					alert("등록된 소재로트가 없습니다")
					return;
				}
				var dataSource = new kendo.data.DataSource({
					  data:json
					});

				console.log(json)
				grid.setDataSource(dataSource)
				var length = json.length
				
				for(var i=0; i < length ; i++){
					
				}
			}
		});
	};
	
	var className = "";
	var classFlag = true;

	
	function clearInput(ty,evt){
		console.log(ty)
		if(evt.keyCode==13){
			getData();
			console.log("enter")
		}
		
		if(ty=="in"){
			$("#shipLotNo").val("");
		}else{
			$("#lotNo").val("");
		}
	};
	
	
	$(function(){
		
		
		var target_input = $('#keyword'); // 포커스 인풋
	    var chk_short = true;
		 
	    $(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#lotNo").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#lotNo").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		grid = $("#grid").kendoGrid({
			
			dataBound : function(e){
				var grid=$("#grid").data("kendoGrid")
				$(".k-grid-content table tbody tr td input").css({
					"height":getElSize(80)
					,"width":getElSize(100)
					,"margin":0
					,"padding":0
					,"font-size" : getElSize(48)
					,"border-color" : "#9B9B9B"
					,"background" : "#9B9B9B"
				})
				

				$(".k-grid-header-wrap table thead tr").css({
					"height" : getElSize(96)
				})
				
				$(".k-grid-header-wrap table thead tr th").css({
					"font-size" : getElSize(36),
					"padding" : getElSize(40),
				})
			
				$(".k-grid-content table tbody tr").css({
					
					"height" : getElSize(96)
				})
				
				$(".k-grid-content").css({
					
					"height" : getElSize(1410)
				})
				$(".k-grid-content table tbody tr td").css({
					"font-size" : getElSize(36),
				
				})
			},
			columns: [
		        { 	
		        	title:"${Stock_number}",
		        	field: "prdNo",
		        	width: getElSize(300)
	        	},
		        { 	title:"${cmpl} ${prd_no}",
		        	field: "item",
		        	width: getElSize(300)
		        },
		        { title: "${Material_warehouse}" ,
	        	  columns: [{
                        field: "wLotNo",
                        title: "${lot_no}",
                        width: getElSize(250)
                    },{
                        field: "wLotCnt",
                        title: "${income_cnt}",
                        width: getElSize(250)
                    },{
                    	field: "wLotStock",
                        title: "${Current_Inventory}",
                        width: getElSize(250)
                    },{
                        field: "wRegDate",
                        title: "${reg_time}",
                        width: getElSize(280)
                    }]
		        },{ title: "R삭" ,
		        	  columns: [{
	                        field: "rLotNo",
	                        title: "${lot_no}",
	                        width: getElSize(250)
	                    },{
	                        field: "rLotCnt",
	                        title: "${income_cnt}",
	                        width: getElSize(250)
	                    },{
	                    	field: "rLotStock",
	                        title: "${Current_Inventory}",
	                        width: getElSize(250)
	                    },{
	                        field: "rRegDate",
	                        title: "${reg_time}",
	                        width: getElSize(280)
	                    }]
			    },{ title: "MCT삭",
	        	  columns: [{
                       field: "mLotNo",
                       title: "${lot_no}",
                       width: getElSize(250)
                   },{
                       field: "mLotCnt",
                       title: "${income_cnt}",
                       width: getElSize(250)
                   },{
                   	   field: "mLotStock",
                       title: "${Current_Inventory}",
                       width: getElSize(250)
                   },{
                       field: "mRegDate",
                       title: "${reg_time}",
                       width: getElSize(280)
                   }]
		        },{ title: "CNC삭" ,
	        	  columns: [{
                       field: "cLotNo",
                       title: "${lot_no}",
                       width: getElSize(250)
                   },{
                       field: "cLotCnt",
                       title: "${income_cnt}",
                       width: getElSize(250)
                   },{
                   	   field: "cLotStock",
                       title: "${Current_Inventory}",
                       width: getElSize(250)
                   },{
                       field: "cRegDate",
                       title: "${reg_time}",
                       width: getElSize(280)
                   }]
		        },{ title: "${cmpl}" ,
	        	  columns: [{
                        field: "pLotNo",
                        title: "${lot_no}",
                        width: getElSize(250)
                    },{
                        field: "pLotCnt",
                        title: "${income_cnt}",
                        width: getElSize(250)
                    },{
                    	field: "pLotStock",
                        title: "${Current_Inventory}",
                        width: getElSize(250)
                    },{
                        field: "pRegDate",
                        title: "${reg_time}",
                        width: getElSize(280)
                    }]
		        }
		    ]
		}).data("kendoGrid");
	})
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>			
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table style="width: 100%">
						<tr>
							<Td style="color: white; text-align: right;">
								<spring:message code="mat_lot"></spring:message>
								<input type="text" id="lotNo" size="10" onkeydown="clearInput('in',event)">
								<button id="search" onclick="getData()" style="cursor: pointer; "><i class="fa fa-search" aria-hidden="true"></i>검색</button>							</Td>
							</Td>
						</tr>
					</table>
					<div id="grid">
					</div>

				</td>
			</Tr>
			
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	