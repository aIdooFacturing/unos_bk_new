<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>


<style type="text/css">
body{
	margin : 0;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var focusText = "main";
	var causeList = [];
	var resultList = [];
	
	$(function(){
		setEl();
		
		<% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%>
		//focus TEXT 맞추기
		var chk_short = true;
	
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	if(focusText=="main"){
		            	 $("#empCd").focus().select();
	            	}else{
		            	 $("#fpId").focus().select();
	            	}
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	if(focusText=="main"){
		            	 $("#empCd").focus().select();
	            	}else{
		            	 $("#fpId").focus().select();
	            	}
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
	//시작 이벤트 집어넣기
	function ready(){
		/* (function poll(){ 
			$.ajax({
				url: "server"
				,success: function(data){
					//Update your dashboard gauge
			//		salesGauge.setValue(data.value);
					cnsole.log("==t==")
				}
				,dataType: "json"
				,complete: poll,
				timeout: 30000
			});
		})(); */

		
 		if(popId!=null){
			getWorkerList();
			setEl();
		}
		
	}

	// 실시간 데이터 받아오기 위해
	function poll(){
		setTimeout(function() {
			// 작업,장비 명단 가지고 오기
			getWorkerList();
		}, 10000);
	}
	//enter key 처리
	function enterEvt(event) {
		if(event.keyCode == 13){
			getTable()
		}
	}

	function fpEnterEvt(event) {
		if(event.keyCode == 13){
			fpLogin();
		}
	}
	
	//사원 바코드를 입력해주세요
	function alarmMsg(){		
		clearTimeout(evtMsg)
		
		return evtMsg = setTimeout(function() {$("#aside").html("<span></span>")}, 10000)
//		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span>")}, 10000)
	}
	
	function getWorkerList(){
		var url = "${ctxPath}/pop/getWorkerList.do";
		
		var param = "popId=" + popId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			complete : poll,
			error : function(request,status,error){
				console.log("workerlist error")
// 				getWorkerList();
// 				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);

			},
			timeout : 3500,
			success : function(data){
				var json = data.dataList;
				var fpjson = data.fplist;
				
				console.log("------===-------")
				console.log(fpjson)
				var fpCount = 0;
// 				var fpCnt = fpjson.length;
// 				$("#fpCnt").html("　" + fpCnt + " 대");
				// 12개 이상으로 보여줄경우 화면에 다표시가 안되어
				// 자동으로 5초마다 바뀌게 하였음
				// 1번 테이블
				var table;
				// 2번 테이블
				var tableTwo;
				table = "<table id='workTable' style='width:100%; height:70%; text-align:center;'><tr><th></th><th>작업자</th><th>장비</th><th>FP</th></tr>";
				tableTwo = "<table id='workTable' style='width:100%; height:70%; text-align:center;'><tr><th></th><th>작업자</th><th>장비</th><th>FP</th></tr>";
// 				console.log(json.length)
				$(json).each(function(idx,data){
					var fpColor = "";
						fpCount = data.cnt;
					$(fpjson).each(function(id,item){
						if(id==0){
// 							console.log(data.cnt)
						}
						if(data.dvcId==item.dvcId){
							console.log(item.dvcId)
							console.log(item.ty)
							data.fp = item.ty;
							data.fpColor = "red";
							

						}
						if(data.fp==undefined){
							data.fp = "";
						}
					})
					if(idx<=12){
						table +="<tr><td width='5%;' style='background:" + data.color +";'> </td>"
						table +="<td width='25%;'>" + decode(data.nm) + "</td>"
						table +="<td width='60%;' style=''>" + decode(data.name) + "</td>"
						table +="<td width='10%;' style='background:" + data.fpColor +";color:white'>" + data.fp + "</td></tr>"
						//					}else if(idx==11){
//						table +="<tr><td></td><td> 왼쪾 </td> <td> 오른쪽 </td></tr>"
					}else{
						tableTwo +="<tr><td width='5%;' style='background:" + data.color +";'> </td>"
						tableTwo +="<td width='25%;'>" + decode(data.nm) + "</td>"
						tableTwo +="<td width='60%;' style=''>" + decode(data.name) + "</td>"
						tableTwo +="<td width='10%;' style='background:" + data.fpColor +";color:white'>" + data.fp + "</td></tr>"
					}
				})
				table +="</table>";
				tableTwo +="</table>";
				
				$("#fpCnt").html("　" + fpCount + " 대")
				//javascript 테이블 그린 후 css 수정하기
				tableCss(table);
				
				//뒤에 페이지가 있을경우 5초후에 페이지 변경하기 위해
				if(json.length>13){
					setTimeout(function() {
						// 작업,장비 명단 가지고 오기
						tableCss(tableTwo);
					}, 5000);
				}
				
			}
		})
		
		
	}
	
	function tableCss(table){
		$("#workerList").empty();
		$("#workerList").append(table);

		//작업자 리스트 CSS
		$("#workerList").css({
			"top" : $("#noticeMsg").height() + $("#header").height(),
			"display" : "",
			"opacity" : 1,
			"width" : originWidth * 0.32,
			"height" : originHeight * 0.75,
			"transition" : "1s"
		})
		
		$("#workTable").css({
//		    "border-top": "1px solid #444444",
		    "border-collapse": "collapse",
		})
		
		$("#workTable tr").css({
			"height": $("#logo2").height() *  1.38,
			"font-size" : $("#logo2").height() *  0.45,
		});
		
		$("#workTable th").css({
		    "border-bottom": "1px solid #444444",
		    "padding": "10px",
		});
		
		$("#workTable td").css({
		    "border-bottom": "1px solid #444444",
//		    "padding": "10px",
			"margin" : 0,
		    "overflow":"hidden",
			"white-space" : "nowrap",
			"text-overflow": "ellipsis",
		})
		
	}
	
	//fpList Login
	function fpLogin(){
		if($("#fpId").val()==0){
			return;
		}
		
		//popId 체크하기
		if(popId == null || popId == undefined){
			$("#fpaside").html("<span style='color:red;'>POP 장비 식별 불가능 입니다.</span>")
			alarmMsg();
			return;
		}
		
		$("#fpId").focus();
		$("#fpId").select();		
		fpId=$("#fpId").val();
	
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(fpId.substring(0,4)!="1111"){
			$("#fpaside").html("<span style='color:red;'>바코드를 다시 확인해 주세요</span>")
			alarmMsg();
			return;
		}
		
		fpId = fpId.substring(4,fpId.length);
		
		var url = "${ctxPath}/pop/popLoginChk.do";
		var param = "fpId=" + fpId +
					"&barcode=" + $("#fpId").val();
// 		console.log(fpId)

		$.showLoading();

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				empCd = json[0].empCd;
				getFpList();
				
				$.hideLoading();
			},error : function(request,status,error){
				console.log(request.responseText);
				if(request.responseText=="no"){
					
					$("#fpaside").html("<span style='color:red;'>등록된 사원번호가 아닙니다.</span>")
					alarmMsg();
					
				}else{
	
					$("#fpaside").html("<span style='color:red;'>관리자에게 문의해주세요.</span>")
					alarmMsg();
				}
				$.hideLoading(); 

			}
		})
	}
	//로그인하기 
	function getTable(){
		if($("#empCd").val()==0){
			return;
		}
		
		//popId 체크하기
		if(popId == null || popId == undefined){
			$("#aside").html("<span style='color:red;'>POP 장비 식별 불가능 입니다.</span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		$("#empCd").focus();
		$("#empCd").select();
		empCd=$("#empCd").val();
	
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(empCd.substring(0,4)!="1111"){
			$("#aside").html("<span style='color:red;'>바코드를 다시 확인해 주세요</span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		empCd = empCd.substring(4,empCd.length);
		
		var url = "${ctxPath}/pop/popLoginChk.do";
		var param = "empCd=" + empCd +
					"&barcode=" + $("#empCd").val();
		console.log(empCd)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
/* 				alert("su")
				console.log(data); */
				

				var json = data.dataList;
				location.href="${ctxPath}/pop/popMainMenu.do?empCd=" + json[0].empCd
				
			},error : function(request,status,error){
				console.log(request.responseText);
				if(request.responseText=="no"){
					
					$("#aside").html("<span style='color:red;'>등록된 사원번호가 아닙니다.</span>")
					alarmMsg();
					
				}else{
	
					$("#aside").html("<span style='color:red;'>관리자에게 문의해주세요.</span>")
					alarmMsg();
				}
				$.hideLoading(); 

			}
		})
	}
	
	function showFpList(){
		
		focusText = "fp";

		$("#FPWrite").css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": "lightcoral",
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.08,
//		        "top": $("#content").height() * 0.35,
//		        "left": $("#content").width() * 0.29,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.83,
//		        "height": $("#content").height() * 0.40,
//		        "width": $("#content").width() * 0.43,
	        "z-index" : 10,
		    "border": $("#logo").width() * 0.03 + "px solid white",
		    "transition": "all 1s ease 0s",
		    "border-radius": $("#logo").width() * 0.1 + "px",

	    })
	}
	
	function getFpList(){
		var url = "${ctxPath}/pop/getFpList.do";
		var param = "popId=" + popId ;

		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data.fpActionList);
				
				
				var json = data.dataList;
				
				console.log(json);
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.checker = false;
					data.cause = 1;
					data.result = 4;
				})
				
				$(data.fpActionList).each(function(idx,data){
					data.content = decode(data.content);
					//원인일때
					if(data.ty==1){
						var arr = {};
						arr.id = data.id;
						arr.content = decode(data.content);
						
						causeList.push(arr);
					}else if (data.ty==2){//결과일때
						var arr = {};
						arr.id = data.id;
						arr.content = decode(data.content);
						
						resultList.push(arr);
					}
				})
				
				$("#InfpList").empty();
				
				kendotable = $("#InfpList").kendoGrid({
					dataSource : json
					,editable : true
					,dataBound : function(e){
						
// 						grid = this;
						
// 						grid.tbody.find('tr').each(function(){
// 							var item = grid.dataItem(this);
// 							kendo.bind(this,item);
// 						})
						
						$("#InfpList tr").css({
// 							"height": $("#logo2").height() *  1.38,
							"font-size" : $("#logo2").height() *  0.45,
						});
						
						// check box 사이즈
						$("#checkallOp").css({
							"width" : originWidth * 0.030,
							"height" : originWidth * 0.030
						})

						// check box 사이즈
						$("#checkall").css({
							"width" : originWidth * 0.035,
							"height" : originWidth * 0.035
						})
						
						// check box 사이즈
						$(".checkbox").css({
							"width" : originWidth * 0.030,
							"height" : originWidth * 0.030
						})
						
					}
					,columns : [{
						field:"checker",
						title:"<input type='checkbox' id='checkall' onclick='checkAll(this)'>",
						template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" ,
// 						editable : false		
						editable : function (dataItem){
							return;
						}
					},{
						field : "name"
						,title : "발생 장비"
						,editable : function (dataItem){
							return;
						}
					},{
						field : "ty"
						,title : "FP 종류"
						,editable : function (dataItem){
							return;
						}
					},{
						field : "cause"
						,title : "발생사유"
						,editor : dropdowncauseList
						,template : "#=causeName(cause)#"
					},{
						field : "result"
						,title :"조치내용"
						,editor : dropdownresultList
						,template : "#=resultName(result)#"
					},{
						field : "endDateTime"
						,title : "조치시간"
						,editable : function (dataItem){
							return;
						}
					}]
				}).data("kendoGrid");
				
				var grid = $("#InfpList").data("kendoGrid");
				grid.autoFitColumn(0);
				grid.autoFitColumn(2);
				grid.autoFitColumn(5);
				
				$.hideLoading();
				
			}
		})
	}
	
	
	function causeName(content){
		content = Number(content);
		for(i=0; i<causeList.length; i++){
			if(causeList[i].id==content){
				return causeList[i].content
			}
		}
		return "";
	}
	
	function resultName(content){
		content = Number(content);
		for(i=0; i<resultList.length; i++){
			if(resultList[i].id==content){
				return resultList[i].content
			}
		}
		return "";
	}
	
	//원인 선택박스
	function dropdowncauseList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "content",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: causeList
		});
	}
	
	//결과 선택박스
	function dropdownresultList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "content",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: resultList
		});
	}
	
	//전체 선택기능
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=kendotable.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=kendotable.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		kendotable.dataSource.fetch();
	}
	
	function checkRow(e){
		console.log("event click")
		var gridList=kendotable.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#InfpList").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 
		 var initData = kendotable.dataItem(row)
		 
		 if(initData.checkSelect){
			 initData.checkSelect = false;
		 }else{
			 initData.checkSelect = true;
		 }
		 kendotable.dataSource.fetch();
	}
	
	function FPclose(){
		location.reload(true);

		focusText = "main";
		 $("#FPWrite").css({
	        "opacity" : 0,
	        "display": "none",
	    })
	}
	
	function saveRow(){
		var list = kendotable.dataSource.data()
		var savelist = [];
		
		$(list).each(function(idx,data){
			if(data.checkSelect){
				data.empCd = empCd;
				savelist.push(data);
			}
		})
		
		if(savelist.length==0){
			alert("저장할 데이터를 선택해주세요.")
			return;
		}
		
		//조건 체크하기
		for(i=0,len=savelist.length; i < len; i++){
			if(savelist[i].cause=="" || savelist[i].result==""){
				alert("발생 사유,조치를 입력해주세요");
				return;
			}
		}
		
		var obj = new Object();
		obj.val = savelist;
		var url = "${ctxPath}/pop/fpSaveList.do";
		var param = "val=" + JSON.stringify(obj);
		$.showLoading();
		console.log(param)
		$.ajax({
			url : url
			,data : param
			,type : "post"
			,success : function(data){
				if(data=="success"){
					alert("저장이 완료되었습니다.")
					location.reload(true);
				}else{
					alert("ERROR 발생 관리자에게 문의해주세요")
					return;
				}
				$.hideLoading();
				console.log(data)
			}
		})
		console.log(savelist);
	}
	
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})
		
		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		$("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		 $(".unos_input").css({
	        "background-color" : "rgba(0,0,0,0)",
	        "border" : "0",
	        "color" : "black",
	        "inline" : "0",
	        "outline" : "0",
	        "border-bottom" : getElSize(10) + "px solid lightgray",
	        "padding" : getElSize(15) + "px",
	        "font-size": $("#logo2").width() * 0.31 + "px",
	        "transition" : "1s"
        	
	    }).focus(()=>{
	        $(".unos_input").css({
	            "border-bottom" : getElSize(10) + "px solid red"
	        })
	    }).blur(()=>{
	        $(".unos_input").css({
	            "border-bottom" : getElSize(10) + "px solid lightgray"
	        })
	    })
	    
		$(".unos_input").css({
			"margin-left" : ((originWidth + originHeight * 0.5)/ 2) - ($(".unos_input").width() / 2),
			"margin-top" : (originHeight / 2) - ($(".unos_input").height() / 2) - $("#header").height(),
			"display" : "inline"
		}).focus();
		
		 $("#fpId").css({
	        "background-color" : "rgba(0,0,0,0)",
	        "border" : "0",
	        "color" : "black",
	        "inline" : "0",
	        "outline" : "0",
	        "border-bottom" : getElSize(10) + "px solid lightgray",
	        "padding" : getElSize(15) + "px",
	        "font-size": $("#logo2").width() * 0.31 + "px",
	        "transition" : "1s"
        	
	    }).focus(()=>{
	        $("#fpId").css({
	            "border-bottom" : getElSize(10) + "px solid red"
	        })
	    }).blur(()=>{
	        $("#fpId").css({
	            "border-bottom" : getElSize(10) + "px solid lightgray"
	        })
	    })
	    
// 		$("#fpId").css({
// 			"margin-left" : ((originWidth + originHeight * 0.5)/ 2) - ($("#fpId").width() / 2),
// 			"margin-top" : (originHeight / 2) - ($("#fpId").height() / 2) - $("#header").height(),
// 			"display" : "inline"
// 		}).focus()
		
		$("#aside").css({
//			"margin-left" : (originWidth / 2) - ($(".unos_input").width() / 2),
//			"margin-top" : (originHeight / 2) - ($(".unos_input").height() / 2) - $("#header").height(),
			"margin-left" : originHeight * 0.5,	
			"margin-top" : getElSize(50),
			"font-size" : $("#logo2").width() * 0.31 + "px",
			"text-align" : "center"
//			"display" : "inline"
		})

		$("#fpaside").css({
// 			"margin-left" : originHeight * 0.1,	
			"margin-top" : getElSize(50),
			"font-size" : $("#logo2").width() * 0.31 + "px",
			"text-align" : "center"
		})
		
		$("#kioskStatus").css({
			"top" : getElSize(170)
			,"width" : originWidth
			,"height" : getElSize(120)
		})
		
		$(".k-icon.k-i-preview").css({
			"font-size" : $("#logo2").width() * 0.31 + "px"
			,"background" : "aliceblue"
		})
		
		$("#fpInput").css({
			"cursor" : "pointer"
		})
		
		$(".result").css({
			"height" : originHeight * 0.045 + "px",
			"margin-top" : originHeight * 0.03 + "px",
			"font-size" : $("#logo").width() * 0.27 + "px",
			"text-align" : "center",
			"vertical-align" : "middle"
		});
		
		//팝업창에서 내용 리스트
		$(".popContent").css({
			"height" : originHeight * 0.55 + "px",
			"margin-top": originHeight * 0.04 + "px",
			"overflow" : "auto",
			"text-align" : "center",
			"vertical-align" : "middle"
		});
		
		$(".btn").css({
			/* "top" : $("#content").height() * 0.96,
			"left" : getElSize(30),
			"position" : "absolute" , */
			"margin-top" : originHeight * 0.03 + "px",
		    "background": "lightgray",
		    "border-radius": originWidth * 0.004 + "px",
		    "padding-left": originWidth * 0.012 + "px", 
		    "padding-right": originWidth * 0.012 + "px", 
		    "padding-top": originWidth * 0.004 + "px", 
		    "padding-bottom": originWidth * 0.004 + "px", 
		    "font-size": $("#logo").width() * 0.12 + "px",
		    "color": "white",
		    "cursor": "pointer",
		    "text-align": "center",
		})

		$(".btn").hover(function(){
		    $(this).css({
		        "background" : "red"
		    })
		}, function(){
		    $(this).css({
		        "background" : "lightgray"
		    })
		});
		
		$("#kioskStatus table").css({
			"font-size" : $("#logo2").height() *  0.45,
		});
	}
</script>

</head>
<body>
    <div id="header">
<%-- 			<img id="logo" src="https://www.digitaltwincloud.com/assets/imgs/default/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/> --%>
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<img id="logo2" src="${ctxPath}/images/FL/logo_bk.png"/>
<!-- 			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/> -->
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
		<div id="workerList" style="position: absolute; float: left; display: none; opacity: 0;">
<!-- 			<table> -->
<!-- 				<tr> -->
<!-- 					<td width=""> -->
<!-- 					</td> -->
<!-- 				</tr> -->
<!-- 			</table> -->
		</div>
		
        <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>        
	</div>
	
	<div id="kioskStatus" style="position: absolute;">
		<table style="height: 100%;">
			<tr>
				<td style="background: lightcyan;">
					FP 미입력
				</td>
				<td id="fpCnt" style="background: lightblue;">
					　${fpCnt } 대
				</td>
				<td id="fpInput" onclick="showFpList()">
					<c:if test="${fpCnt ne 0}">
						<span class="k-icon k-i-preview"></span>
					</c:if>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="FPWrite" style="opacity: 0; display: none;">
		<div class="result"> <span> FP 입력보고 </span></div>
		<div style="display: table; width: 100%;">
			<div id="InfpList" class="popContent" style="display: table-cell;">
				<input id="fpId" onkeyup="fpEnterEvt(event)" placeholder="바코드를 입력해 주세요.">
				<div id="fpaside"><span></span></div>        
				
			</div>
		</div>
		<div style="text-align: center;">
<!-- 			<button id="saveOp" class="btn" onclick="saveNonOp()">저장</button> -->
			<button onclick="saveRow()" class="btn"> 저장 </button>
			<button onclick="FPclose()" class="btn"> 닫기 </button>
		</div>
	</div>

</body>
</html>