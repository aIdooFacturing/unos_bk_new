<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>

<style type="text/css">
body{
	margin : 0;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	$(function(){
		$(".date").datepicker({
		})
		$("#date").val(moment().format("YYYY-MM-DD"))
		setEl();
		sessionChk();
		getGroupMenu();
		
		gridTable();
		
		// 품번가져오기
		getPrdNoList();
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})

	function getGroupMenu(){
		$.showLoading()
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + 1;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("getGroup is Complete")
				json = data.dataList;
				
// 				var option = "";
				
// 				$(json).each(function(idx, data){
// 					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
// 				});
				
				$("#prdNo").kendoComboBox({
					dataSource : json,
					autoWidth : true,
					dataTextField: "prdNo",
					dataValueField: "prdNo",
// 					clearButton: false,
					value: json[0].prdNo,
					//품번 변경되었을시
					change: function(e){
						var value = this.value();
						
						getDvcListByPrdNo(value)
					}
				});
				
// 				$("#prdNo").html(option).change(getDvcListByPrdNo)
				 
				$("#chkTy").html(getCheckType());
				
				getDvcListByPrdNo();
				
			}
		});
	}
	
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(data.dataList)
				$(json).each(function(idx, data){
					var codeName;
					if(decode(data.codeName)=="입고검사"){
						codeName = decode(data.codeName);
					}else{
						codeName = decode(data.codeName);
					}
					
					option += "<option value='" + data.id + "'>" + codeName + "</option>"; 
				});
				
				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};
	
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		console.log(url+'?'+param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$.hideLoading()
				console.log("getDvcListByPrdNo is Complete")
				var json = data.dataList;
				console.log(json)
				var option = "";
				
				$(json).each(function(idx, data){
					data.name = decode(data.name)
// 					option += "<option value='" + data.dvcId + "'>" + data.name + "</option>"; 
				});
				
				if(json.length==0){
					var arr = {}
					arr.dvcId = 0;
					arr.name = "장비 없음"
					json.push(arr);
				}
				
				$("#Check_dvcId").kendoComboBox({
					dataSource : json,
					autoWidth : true,
					dataTextField: "name",
					dataValueField: "dvcId",
// 					clearButton: false,
					value: json[0].name,
					//품번 변경되었을시
					change: function(e){
						var value = this.value();
						
// 						getDvcListByPrdNo(value)
					}
				});
				
// 				if(json.length==0){
// 					option += "<option value='0'>장비 없음</option>";
// 				}
// 				console.log($("#Check_dvcId").html(option).val())
// 				$("#Check_dvcId").html(option).val(json[0].dvcId);
				//getCheckList();
				
			}
		});
	};
	//시작메뉴 집어넣기
	function ready(){
		
	}
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//타이틀 클릭시 경로이동
	function pageMove(){
		location.href='${ctxPath}/pop/popMainMenu.do?empCd='+empCd
	}
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
	        "background-color" : "rgb(170, 120, 220)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    $(".menu .mainSpan").css({
	    	"display" : "inline-block",
	    	"margin-top" : originHeight * 0.075
	    })
	    
	    
	    $(".result").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"text-align" : "center",
// 	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px",
	    	"font-weight" : "bold"
	    });
		
	    //팝업창에서 내용 리스트
	    $(".popContent").css({
	    	"height" : originHeight * 0.60 + "px",
	    	"margin-top": originHeight * 0.04 + "px",
	    	"overflow" : "auto"
	    });
	    
	    $(".btnDiv").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"text-align" : "center",
// 	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px"
	    })
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
	    $("button").css({
			"cursor" : "pointer",
			"margin-right" : getElSize(30),
		    "background": "rgb(144, 144, 144)",
		    "border-color": "rgb(34, 35, 39)",
			"vertical-align" : "middle",
 			"height" : getElSize(90),
 			"font-size" : getElSize(45),
 			"border-radius" : getElSize(8),
 			"padding-bottom" : getElSize(10),
 			"padding-top" : getElSize(10)
		});
	    
	    $("select").css({
	    	"font-size" : getElSize(45),
	    	"padding-top" : getElSize(5),
	    	"padding-bottom" : getElSize(5)
	    })
	}
	
	// 팝업창 생성 팝업 CSS #popup_submenu #showGoWork #showOffWork #showEarlyWork #showOutWork
	function popup_submenu(selected_menu_id,div_id){
		
		console.log(div_id);
		
		//출근 보고 클릭시
		if(div_id=="showGoWork"){
// 			goWork();
			getCheckList()
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showOffWork"){// 퇴근 보고 클릭시
			offWork();
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showChangeWork"){// 조퇴 보고 클릭시
			ChangeWork(selected_menu_id,div_id);
			openPopup(selected_menu_id,div_id);
		}
	}
	
	//팝업창 띄우기
	function openPopup(selected_menu_id,div_id){
		showCorver()
//		alert("1. :" + selected_menu_id + ", 2. :" + div_id)
		var selected_item = $("#" + selected_menu_id);
		/* var backColor;
		//1번 박스 선택했을경우 배경색
		if(selected_menu_id=="box1"){
			backColor = "rgb(214,240,255)"
		}else{
			backColor = $(selected_item).css("background")
		} */
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })

	    $("#" + div_id).css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.08,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.83,
	        "z-index" : 10,
	    })
	    
	    console.log(div_id)
	}
	
// 	dataSource : json,
// 	autoWidth : true,
// 	dataTextField: "prdNo",
// 	dataValueField: "prdNo",
// //		clearButton: false,
// 	value: json[0].prdNo,
// 	//품번 변경되었을시
// 	change: function(e){
// 		var value = this.value();
		
// 		getDvcListByPrdNo(value)
// 	}
		
	function categoryDropDownEditor(container, options) {
		$('<input name="' + options.field + '"/>')
		.appendTo(container)
		.kendoDropDownList({
			dataSource: [
				{ text: "양호", value: 2 },
				{ text: "불량", value: 1 }
			],
			valuePrimitive: true,
			dataTextField: "text",
			dataValueField: "value",
			optionLabel: "==선택==",
		});
	}

	function selectOption(num){
		if(num==1){
			return "불량";
		}else if(num==2){
			return "양호";
		}else{
			return "선택하세요"
		}
	}
	
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			scrollable:true,
			editable: true,
			selectable: "row",
			height:$("#grid").height(),
			noRecords: {
				template: "현재 품번, 장비에 데이터가 없습니다."
			},
			dataBound:function(){
				for (var i = 0; i < this.columns.length; i++) {
	            	this.autoFitColumn(i);
	            }
			
				$(".k-grid-header").css({
					"padding-right" : getElSize(62 * 2)
				})
				
				$("#grid thead tr th").css({
					"vertical-align": "middle",
				    "text-align": "center"
				})
			},
			columns: [
			  {
				  field:"checker",
				  editable: true,
				  title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";' disabled='disabled>",
// 				  width:getElSize(133),
				  template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";' disabled='disabled/>", 
				  attributes: {
          				style: "text-align: center; font-size:" + getElSize(35)
       			  },headerAttributes: {
          				style: "text-align: center; font-size:" + getElSize(37)
       			  }						
			  },
			  {
			    field: "prdNo",
			    title: "<spring:message  code='prd_no'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "name",
			    title: "<spring:message  code='device'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "chkTy",
			    title: "<spring:message  code='check_type'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "attrTy",
			    title: "<spring:message  code='character_type'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "attrNameKo",
			    title: "<spring:message  code='character_name'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "spec",
			    title: "<spring:message  code='drawing'></spring:message>Spec" ,
// 			    width: getElSize(350),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "target",
			    title: "<spring:message  code='target_val'></spring:message>" ,
// 			    width: getElSize(200),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "low",
			    title: "<spring:message  code='min_val'></spring:message>" ,
// 			    width: getElSize(200),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "up",
			    title: "<spring:message  code='max_val'></spring:message>" ,
// 			    width: getElSize(200),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "measurer",
			    title: "<spring:message  code='measurer'></spring:message>" ,
// 			    width: getElSize(350),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "resultVal",
			    template:"#=selectOption(resultVal)#",
			    title: "<spring:message  code='check_result'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: false,
			    editor: categoryDropDownEditor,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
// 			    	class: "editable-cell" ,
			    	"class": "# if(data.rowClass === '0') { # editable-cell # } else { # editable-cell-alt # } #" ,
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "resultVal2",
			    template:"#=selectOption(resultVal2)#",
			    title: "<spring:message  code='check_result'></spring:message><br>계측기 후" ,
// 			    width: getElSize(300),
			    editable: false,
			    editor: categoryDropDownEditor,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	"class": "# if(data.rowClass === '0') { # editable-cell # } else { # editable-cell-alt # } #" ,
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "result",
			    template:"#=selectOption(result)#",
			    title: "<spring:message  code='result'></spring:message>" ,
// 			    width: getElSize(300),
			    editable: false,
			    editor: categoryDropDownEditor,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	"class": "# if(data.rowClass === '0') { # editable-cell # } else { # editable-cell-alt # } #" ,
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "date",
			    template: "#=moment().format('YYYY-MM-DD')#",
			    editable: true,
			    title: "<spring:message  code='check_date'></spring:message>" ,
// 			    width: getElSize(300),
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "checkCycle",
			    title: "<spring:message  code='check_cycle'></spring:message>" ,
// 			    width: getElSize(200),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  },
			  {
			    field: "workTy",
			    title: "<spring:message  code='division'></spring:message>" ,
// 			    width: getElSize(200),
			    editable: true,
			    headerAttributes: {
			        style: "font-size :" + getElSize(30)
			    },
			    attributes: {
			    	style: "font-size :" + getElSize(30)
				}
			  }
			]
			
		}).data("kendoGrid")
	}
	
	var result = "<select style='font-size:" + getElSize(40) + "' onchange='chkFaulty(this, \"select\")'><option value='0' >${selection}</option><option value='1'>${ok}</option><option value='2'>${faulty}</option></select>";

	function getCheckList(){
		classFlag = true;
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
		var dvcIdName ="";
		if($('#Check_dvcId').data('kendoComboBox').dataItem()!=undefined){
			dvcIdName = $('#Check_dvcId').data('kendoComboBox').dataItem().name
		}
		
		
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + $("#chkTy").val() +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val() + 
					"&dvcId=" + dvcIdName;
// 					"&dvcId=" + $("#Check_dvcId option:selected").html();
// 		var param = "prdNo=FS_RR_LH &chkTy=2&checkCycle=1&date=2019-06-27&ty=2&dvcId=FS/R M#3";
		
		console.log(param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				
				var dataSource = new kendo.data.DataSource({});
				console.log(json)
				$(json).each(function(idx, data){
					
					if(idx%2==0){
						data.rowClass="0"
					}else{
						data.rowClass="1"
					}
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var attrTy;
						if(data.attrTy==1){
							//attrTy = "정성검사";
							attrTy = "${js_check}";
						}else{
							//attrTy = "정량검사";
							attrTy = "${jr_check}";
						};
					
						var date;
						if(data.date==""){
							date = $(".date").val();
						}else{
							date = data.date;
						};
						
						var resultVal, resultVal2, resultVal3, resultVal4;
						
						if(data.attrTy==1){
							resultVal = resultVal2 = resultVal3 = resultVal4 = result;
						}else{
							resultVal = "<input type='text' value='" + data.result + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal2 = "<input type='text' value='" + data.result2 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal3 = "<input type='text' value='" + data.result3 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal4 = "<input type='text' value='" + data.result4 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						};
						
						var chkTy;
						if(decodeURIComponent(data.chkTy).replace(/\+/gi, " ")=="입고검사"){
							chkTy = "${in_check}"
						}else{
							chkTy = decodeURIComponent(data.chkTy).replace(/\+/gi, " ");
						}
						
						data.name = decode(data.name);
						data.attrNameKo = decode(data.attrNameKo);
						data.spec = decode(data.spec);
						data.measurer = decode(data.measurer);
						data.chkTy = chkTy;
						data.attrTy = attrTy;
						data.resultVal = resultVal;
						data.resultVal2 = resultVal2;
						
						data.resultVal = 2;
						data.resultVal2 = 2;
						data.result = 2;
						
						data.checkSelect = true;
						if($("#checkCycle").val()==1){
							data.checkCycle = "초물"
						}else if($("#checkCycle").val()==2){
							data.checkCycle = "중물"
						}else{
							data.checkCycle = "종물"
						}
						
						if($("#workTime").val()==1){
							data.workTy="야간";
						}else if($("#workTime").val()==2){
							data.workTy="주간";
						}
						
					}
					dataSource.add(data);
				});
				
				
				kendotable.setDataSource(dataSource);
			}
		});
	};
	
	function saveRow(){
		valueArray = [];
		
		
		if($("#checker").val()==null){
			kendo.alert("검사자를 선택하여 주세요.");
			return;
		}
		
		gridlist=kendotable.dataSource.data();
		for(var i=0;i<gridlist.length;i++){
			if(gridlist[i].checkSelect){
				console.log("gridlist[i].result : "+gridlist[i].result)
				console.log("gridlist[i].resultVal : "+gridlist[i].resultVal)
				console.log("gridlist[i].resultVal2 : " + gridlist[i].resultVal2)
				if(gridlist[i].result!=1 && gridlist[i].result!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				if(gridlist[i].resultVal!=1 && gridlist[i].resultVal!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				if(gridlist[i].resultVal2!=1 && gridlist[i].resultVal2!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				gridlist[i].checker=$("#checker").val()
				gridlist[i].chkCycle=$("#checkCycle").val()
				gridlist[i].date=$(".date").val()
				gridlist[i].workTy=$("#workTime").val()
				gridlist[i].dvcId=$("#Check_dvcId").val()
				gridlist[i].chkTy=$("#chkTy").val()
				gridlist[i].fResult=gridlist[i].result
				gridlist[i].result=gridlist[i].resultVal
				gridlist[i].result2=gridlist[i].resultVal2
				gridlist[i].attrNameKo=encodeURIComponent(gridlist[i].attrNameKo)
				
				valueArray.push(gridlist[i]);
			}
		}
		if(valueArray.length==0){
			kendo.alert("저장할 항목을 선택하여 주세요.")
			return;
		}
		
		var obj = new Object();
		obj.val = valueArray;
		
		console.log(obj)
// 		return			addCheckStandardList
		var url = "${ctxPath}/chart/addCheckStandardList.do";
		var param = "val=" + JSON.stringify(obj);
		console.log(url+"?"+param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				$.hideLoading()
				if(data=="success") {
					alert ("저장이 완료되었습니다.");
					pageMove();
				}else{
					alert("관리자에게 문의하세요_31")
				}
			}
		});
	}
	
	//품번 가져오기
	function getPrdNoList(){
		var url = ctxPath + "/common/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				comPrdList=json;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
// 				el.html(options);

// 				if(typeof(val)!="undefined"){
// 					el.val(val);	
// 				}

				//품번변경시 장비라우팅 부르기
//				el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
				
			}
		});
	};
	
	function offWork(){
		
	}
	
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="pageMove()">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				4. 품질 관리
			</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
      	<table style="width: 100%; height: 100%;">
			<Tr>
			    <td style="text-align: center">
					<div class="menu" id="box1" style="opacity: 0" onclick="popup_submenu('box1','showGoWork')">
					    <span class="mainSpan">4-1 자주검사</span><br>
					    <span class="subSpan">(Self Inspection)</span>
					</div>
					
<!-- 					<div class="menu" id="box2" style="opacity: 0" onclick="popup_submenu('box2','showOffWork')"> -->
<!-- 					<br> -->
<!-- 					    <span>BOX2</span> -->
<!-- 					</div> -->

<!-- 					<div class="menu" id="box3" style="opacity: 0" onclick="popup_submenu('box3','showEarlyWork')"> -->
<!-- 					<br> -->
<!-- 					    <span>BOX3</span> -->
<!-- 					</div> -->
<!-- 					<div class="menu" id="box4" style="opacity: 0" onclick="popup_submenu('box4','showOutWork')"> -->
<!-- 			        <br> -->
<!-- 			            <span>BOX4</span> -->
<!-- 			        </div> -->
			    </td>
			</Tr>
		</table>
<!--         <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>         -->
	</div>
	
<!-- 	자주검사 화면 -->
	<div id="showGoWork">
		<div class="result"> 자주 검사
		
		</div>
		<div class="popContent">
			<table id="content_table" style="width: 100%; height: 13%;"> 
				<tr>
					<Td><spring:message  code="prd_no"></spring:message></Td>
					<Td><!-- <select id="prdNo"></select > --><input id="prdNo"></Td>
					<Td><spring:message  code="device"></spring:message></td>
					<td ><input id="Check_dvcId"></td>
					<Td ><spring:message  code="check_cycle"></spring:message></Td>
					
					<Td>
						<select id="checkCycle"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
						<select id="workTime"><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
					</Td>

					<Td rowspan="2">
						<div id="buttonGroup" style="float: right;">
							<%-- <button id="moveFaulty" onclick="goAddFaulty()"><i class="fa fa-share" aria-hidden="true"></i><spring:message  code="add_faulty"></spring:message></button> --%>
							<button id="checkSearchBtn" onclick="getCheckList()"> <i class="fa fa-search" aria-hidden="true"></i>조회</button>
<%-- 							<button id="saveCheckList" onclick="saveRow();"><i class="fa fa-floppy-o" aria-hidden="true"></i><spring:message  code="save_check_result"></spring:message></button> --%>
						</div>
					</Td>
				</tr>
				<tr>
					<Td><spring:message  code="check_date"></spring:message></Td>
					<Td><input type="text" id="date" class="date" readonly="readonly"/></Td>
					<Td><spring:message  code="check_type"></spring:message></Td>
					<Td><select id="chkTy"></select></Td>
					<Td><spring:message  code="checker"></spring:message></Td><Td><select id="checker" class="worker" disabled><option value="${empCd }">${nm }</option></select></Td>
				</tr>
			</table> 
			<div id="grid" style="width: 99.8%;height: 86%;">
			
			</div>
		</div>
		<div class="btnDiv">
			<button id="startJob" class="btn" onclick="saveRow()">저장</button>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popQuality.do'">취소</button>
		</div>
	</div>
	
	<div id="showOffWork">
		<table style="width: 100%; ">
			<Tr> 
				<Td class='table_title'> <spring:message code="prd_no"></spring:message> * </Td>
				<Td>
					<div id="prdNo_form"></div>
				</Td>
				<Td class='table_title' width="20%"><spring:message code="check_ty"></spring:message>  </Td>
				<Td id="chkTy_form"  width="30%"></Td>
			</Tr>
			<Tr>
				<Td class='table_title' width="22%"><spring:message code="operation"></spring:message> * </Td>
				<Td id="oprNm_form"></Td>
				<Td class='table_title'><spring:message code="device"></spring:message></Td>
				<Td id="dvcId_form"></Td>
			</Tr>
			<Tr>
				<Td class='table_title'><spring:message code="reporter"></spring:message> * </Td>
				<Td id="checker_form"></Td>
				<Td class='table_title'><spring:message code="event_date"></spring:message></Td>
				<Td id="date_form"></Td>
			</Tr>
			<Tr>
				<Td class='table_title'><spring:message code="divide_situ"></spring:message> * </Td>
				<Td id="situation_form"></Td>
				<Td class='table_title'><spring:message code="part"></spring:message></Td>
				<Td id="part_form"></Td>
			</Tr>
			<Tr>
				<Td class='table_title'><spring:message code="situ"></spring:message> * </Td>
				<Td id="situationTy_form"></Td>
				<Td class='table_title'><spring:message code="cause"></spring:message></Td>
				<Td id="cause_form"></Td>
			</Tr>
			<Tr>
				<Td class='table_title'><spring:message code="gch_ty"></spring:message></Td>
				<Td id="gchTy_form"></Td>
				<Td class='table_title'><spring:message code="gch"></spring:message></Td>
				<Td id="gch_form"></Td>
			</Tr>
			<Tr>
				<Td class='table_title'><spring:message code="count"></spring:message> * (<label id="exCnt">0</label>)</Td>
				<Td id="cnt_form"></Td>
				<Td class='table_title'><spring:message code="action"></spring:message></Td>
				<Td id="action_form"></Td>
			</Tr>
			<Tr>
				<Td class='table_title'>불량창고이동수량 *</Td>
				<Td id="sendCnt_form"></Td>
				<Td></Td>
				<Td></Td>
			</Tr>
			<Tr>
				<Td colspan="4" style="text-align: center; padding-right: 90px"><button onclick="saveRow2()"><spring:message code="save"></spring:message></button></Td>
			</Tr>
		</table> 
	</div>
</body>
</html>