<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>

<style type="text/css">
body{
	margin : 0;
}
.selectRow {
	background: red !important;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	var tCons;
	
	$(function(){
		$(".date").datepicker({
		})
		
		$("#date_form").val(moment().format("YYYY-MM-DD"));
		
		$("#date").val(moment().format("YYYY-MM-DD"));
		setEl();
		sessionChk();
		getGroupMenu();
		//선택리스트 가지고 오기
		getAllCheckTyList();
		
		// 품번가져오기
		getPrdNoList();
		
		//select change event
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})

	function selectEvt(json){
		//현상구분 값 변경시 현상 SELECT 변경하기
		
		$("#situation_form").change(function(){
			var val = $("#situation_form").val();
			
			var situationTy_form = "<option value='0'>선택</option>"; //현상

			var index;
			if(val=="30"){			//소재불량
				index=6;
			}else if(val=="31"){	//가공불량
				index=14
			}else if(val=="32"){	//기타
				index=15
			}else if(val=="201"){	//도금불량
				index=17
			}
			
			$(json).each(function(idx,data){
				//소재불량
				if(data.group==index){
					situationTy_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
				}
			})
			
			$("#situationTy_form").empty()
			$("#situationTy_form").append(situationTy_form);
		})
		
		//공정 값 변경시 수량 가지고 오기
		$("#oprNm_form").change(function(){
			if($("#oprNm_form").val()=="0" || $("#prdNo_form").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
		//품번 값 변경시 수량 가지고 오기
		$("#prdNo_form").change(function(){
			if($("#oprNm_form").val()=="0" || $("#prdNo_form").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
	}
	function getGroupMenu(){
		$.showLoading()
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + 1;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("getGroup is Complete")
				json = data.dataList;
				
// 				var option = "";
				
// 				$(json).each(function(idx, data){
// 					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
// 				});
				
				$("#prdNo").kendoComboBox({
					dataSource : json,
					autoWidth : true,
					dataTextField: "prdNo",
					dataValueField: "prdNo",
// 					clearButton: false,
					value: json[0].prdNo,
					//품번 변경되었을시
					change: function(e){
						
						var value = this.value();
						getDvcListByPrdNo(value)
					}
				});
				
// 				$("#prdNo").html(option).change(getDvcListByPrdNo)
				 
				$("#chkTy").html(getCheckType());
				
				getDvcListByPrdNo();
				
			}
		});
	}
	
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(data.dataList)
				$(json).each(function(idx, data){
					var codeName;
					if(decode(data.codeName)=="입고검사"){
						codeName = decode(data.codeName);
					}else{
						codeName = decode(data.codeName);
					}
					
					option += "<option value='" + data.id + "'>" + codeName + "</option>"; 
				});
				
				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};
	
	tab = false;

	//Tab X클릭시  Tab remove
	function tabRemove(e){
		
		tabstrip.remove(e);
		
		var size = $(".grid").length
		
		//전체 먼저 초록색으로 변경하기
		$(".flex-container > label").css({
			"background-color" : "rgb(174, 213, 67)"
			,"opacity" : 1
		});
		
		//grid Id값 찾아서 현재 남아있는애들만 빨강색으로 표기
		for( i=0; i < size; i++){
			var gridId = $(".grid")[i].id;
			gridId = $("#"+gridId).attr("name");
			$("#"+gridId).css({
				"background" : "red"
				,"opacity" : 0.6
			})
		}
	}
	
	var chkTypeList = [];
	var arr = {};
	arr.value = "0";
	arr.text = "O";
	chkTypeList.push(arr);
	var arr = {};
	arr.value = "1";
	arr.text = "X";
	chkTypeList.push(arr);
	
	function chkTrue(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "text",
			 dataValueField  : "value",
			 dataSource: chkTypeList,
		 }).data("kendoDropDownList");
	}

// 	var input = $("<input type='date' class='date' style='width : " + getElSize(390) + "px; font-size:"+getElSize(32)+"'/>");
//     input.attr("name", options.field);
//     input.appendTo(container);
    
	function demicalNumber(container, options){
		console.log(options.model.dp)
		$('<input class="cntInput" name="' + options.field + '"/>')
	     .appendTo(container)
	     .kendoNumericTextBox({
	         decimals: options.model.dp
	         ,restrictDecimals:true
	         ,format:"n" + options.model.dp
	         ,spinners: false
	     })
	}
		
	//INPUT 넣기 데이터 값넣기
	// 클릭시 수량 집어넣기
	function inputCnt(){
		console.log("넣자")
		var cnt = Number($("#five").val() + $("#four").val() + "." +$("#three").val() + $("#two").val() + $("#one").val() );
		
		var kendotable = $("#"+showId).data("kendoGrid");
		var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
		var dataItem = kendotable.dataItem(row)
// 		console.log("====넣기====")
// 		console.log("showId :" + showId)
// 		console.log("uid :" + uid)
// 		console.log(kendotable)
// 		console.log(dataItem)
// 		console.log("넣을값 :" + cnt)
// 		console.log("row :" + dataItem.cvt1)
		eval("dataItem."+columnId+"="+cnt);
		
		kendotable.dataSource.fetch();
		
		$("#keyPad").css({
			"display":"none",
		})
		
		tCons.className = "";
	}
	
	//장비 클릭시 Tab 추가하기
	function gridTabAdd(dvcId,name){

		// 클릭한 애들 표기하기
		$("#"+dvcId).css({
			"background" : "red"
			,"opacity" : 0.6
		})
		
		console.log(tab)
		if(!tab){
			tabstrip = $("#grid").kendoTabStrip().data("kendoTabStrip");
			tab = true;
		}else{
			tabstrip = $("#grid").data("kendoTabStrip");
		}

// 		closeButton = "<span onclick='tabstrip.remove($(this).closest(\"li\"))'>X</span>";
		closeButton = "<span onclick='tabRemove($(this).closest(\"li\"))'>X</span>";
		
		//id 부여하기
		var gridId = "grid" + dvcId;
		// 이미 같은것이 있을경우 추가할 필요없음
		if($("#"+gridId).length!=0){
			return;
		}
		tabstrip.append([
			{
				text: name + " " + closeButton,
				encoded: false,
				content: "<div id = '" + gridId + "' class='grid' name=" + dvcId +"></div>"
			}
		]);
		
		$("#"+gridId).kendoGrid({
			editable : false,
			height : getElSize(900),
			dataBound : function(e){
				grid = this;
				
				grid.tbody.find('tr').each(function(){
					var item = grid.dataItem(this);
					kendo.bind(this,item);
				})
				
				for (var i = 0; i < this.columns.length; i++) {
					if(i<4)
	            	this.autoFitColumn(i);
	            }
				
				$("#" + gridId + " tr th").css({
					"font-size" : getElSize(48)
				})
				
				$("#" + gridId + " tr td").css({
					"font-size" : getElSize(45)
				})
				
				//숫자 입력을 위한 이벤트 생성
				$(".cntInput").click(function(e){
					var tr = $(this).closest("tr");
					var td = $(this).closest("td");
					//현재 보고있는 Tab gridId 구하기
					var gridId = $(".k-state-active div")[0].id;
					
					//전역변수로 지정후 input,입력 버튼 클릭시 활용하기
					uid = tr[0].dataset.uid;
					showId = gridId;
					columnId = td.context.dataset.bind;
					columnId = columnId.substring(columnId.indexOf(":")+1,columnId.length);
					
					var kendotable = $("#"+gridId).data("kendoGrid");
					var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
					
					console.log(tr)
					console.log(td)
					console.log(td.context)
					console.log(td.context.name)
					console.log(kendotable)
					console.log(row)
					
					//기존 초기화
					if(tCons!=undefined){
						tCons.className = "";
					}
					
					tCons = tr[0];
					tCons.className = "selectRow";
		    		$("#one").val("")
		    		$("#two").val("")
		    		$("#three").val("")
		    		$("#four").val("")
		    		$("#five").val("")
		    		$("#six").val("")
					inputCntData = grid.dataItem(row)
					
					// 클릭한 데이터 숫자 값
					eval("var nStr = inputCntData."+columnId+".toString()");
// 				    var nStr = inputCntData.cvt1.toString();
		    		
		    		//소수 . 위치 확인
		    		var idx = 0;
		    		//자연수,소수 변수
		    		var natural,decimal;

		    		//소수점 자리확인하기
		    		idx = nStr.indexOf(".");
		    		//소수점이 없을경우 (자연수임)
		    		if(idx==-1)
		    			idx = nStr.length;
		    		
		    		//자연수 변수
		    		natural = nStr.substring(0,idx);
		    		decimal = nStr.substring(idx+1,nStr.length);
		    		
					// 1,10,100,1000 의 자리 분리를 위해서 반목문
				    for(var i = 0; i < natural.length; i++){
				    	// 1의 자리일 경우
				    	if(i==natural.length-1){
				    		$("#four").val(Number(natural[i]))
				    	}else{
				    		$("#five").val(Number($("#five").val()+natural[i]))
				    	}
// 				        answer.push(Number(nStr[nStr.length -1 -i]));
				    }
					// 0.1,0.01 소수점 자리 분리를 위해서 반목문
				    for(var i = 0; i < decimal.length; i++){
				    	if(i==0){
				    		$("#three").val(Number(decimal[i]))
				    	}else if(i==1){
				    		$("#two").val(Number(decimal[i]))
				    	}else{
				    		$("#one").val(Number($("#one").val()+decimal[i]))
				    	}
				    	
// 				        answer.push(Number(nStr[nStr.length -1 -i]));
				    }
				    
				    
// 					console.log(answer)
					
					
					$("#keyPad").css({
						"background":"gray",
						"width": originWidth * 0.33,
						"height": originHeight * 0.25,
						"margin-top": $("#header").height() * 1.12,
						"z-index": 11,
						"display":""
					})
					$("#keyPad").css({
						"left" : (originWidth / 2) - ($("#keyPad").width() / 2),
					})
					
					/* $("#headTitle").css({
						"position":"absolute",
						"top":  $("#header").height()/2 - $("#header").height() * 0.36,
			//			"left": getElSize(2000),
						
						"display" : "inline-block",
				        "background-color" : "rgb(80, 140, 245)",
				        "border" : getElSize(7) + "px solid white",
				        "border-radius" : getElSize(30) + "px",
				        "padding-left" : getElSize(15),
				        "padding-right" : getElSize(15),
				        "cursor" : "pointer",
				        
						"font-size": $("#logo2").height() *  0.7
					}) */
			
					$("#headTitle").css({
						"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
					})
				})
				
				//입력창 나올때 이벤트 발쌩
				$('html').click(function(e) {
//					console.log(e.target)
					if(!($(e.target).hasClass("keyPad") || $(e.target).hasClass("cntInput")) ){
						if(tCons!=undefined)
						tCons.className = "";
						
						$("#keyPad").css({
							"display":"none",
						})
					}
				});
				
				$(".displayTy").css({
					"background" : "darkslateblue"
				})
				
				$(".grid select").css({
					"font-size" : getElSize(75)
				})
				
				$(".cntInput").css({
					"font-size" : getElSize(50)
				})

	
			},
			columns : [{
				title : "no"
				,field : "clmNo"
				,width : getElSize(150)
			},{
				title : "prdNo"
				,field : "prdNo"
			},{
				field : "attrNameKo"
			},{
				field : "spec"
			},{
				field : "measurer",hidden: true
			},{
				title : "cavity"
				,columns : [{
					field : "cvt1"
// 					,width : getElSize(230)
					,editor : function (container,options){
						attrTy = options.model.attrTy
						if(attrTy==1){
							chkTrue(container,options);
						}/* else{
							demicalNumber(container,options);
						} */
					}
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' data-bind='value:cvt1' style='width:90%;'> #}else{# <select data-bind='value:cvt1' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
// 					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' data-bind='value:cvt1' style='width:90%;'> # } #"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
// 				    	"class": "# if(data.rowClass === '0') { # editable-cell # } else { # editable-cell-alt # } #" ,
// 				    	style: "font-size :" + getElSize(30)
					}
					
// 					,template:kendo.template("#if (attrTy == 1) {# #=chkTrue##} else if(attrTy == 2){# #=kendo.toString(2,'3'+4)# #} #")
// 					,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=kendo.toString(low,'n'+dp)# #} #")


				},{
					field : "cvt2"
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' data-bind='value:cvt2' style='width:90%;'> #}else{# <select data-bind='value:cvt2' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
					}
// 					,width : getElSize(230)
				},{
					field : "cvt3"
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' data-bind='value:cvt3' style='width:90%;'> #}else{# <select data-bind='value:cvt3' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
					}
// 					,width : getElSize(230)
				},{
					field : "cvt4"
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' data-bind='value:cvt4' style='width:90%;'> #}else{# <select data-bind='value:cvt4' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
					}
// 					,width : getElSize(230)
				}]
			}]
		}).data("kendoGrid")
		
		//grid 값넣어주기
		gridTable(gridId,name,dvcId);
		
		var tabSize = $(".grid").length
		tabstrip.select(length-1);

	}
	
	//테이블 가져오기,데이터 가져오기,자주검사,자주 검사,자주검사 데이터
	function gridTable(gridId,name,dvcId){
		
		console.log(gridId)
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
// 		if($('#Check_dvcId').data('kendoComboBox').dataItem()!=undefined){
// 			dvcIdName = $('#Check_dvcId').data('kendoComboBox').dataItem().name
// 		}
		
//		$("#checkCycle")	검사주기
//		$("#chkTy")			검사유형(자주검사 : 2)
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + 2 +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val() + 
					"&dvcId=" + name;
// 					"&dvcId=" + $("#Check_dvcId option:selected").html();
// 		var param = "prdNo=FS_RR_LH &chkTy=2&checkCycle=1&date=2019-06-27&ty=2&dvcId=FS/R M#3";
		
		console.log(param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				
				$(json).each(function(idx,data){
					data.chkTy = decode(data.chkTy);
					data.attrNameKo = decode(data.attrNameKo);
					data.spec = decode(data.spec);
					data.measurer = decode(data.measurer);
					data.name = decode(data.name);
					data.dvcId = dvcId;
					if(data.cvt1==""){
						data.cvt1=0
					}
					if(data.cvt2==""){
						data.cvt2=0
					}
					if(data.cvt3==""){
						data.cvt3=0
					}
					if(data.cvt4==""){
						data.cvt4=0
					}
// 					data.dp = 1;

				})
				console.log(json)
				
				var dataSource = new kendo.data.DataSource({
					data : json,
					autoSync: true,
		            schema: {
		                model: {
		                	id: "id",
		                	fields: {
		                	measurer: { editable: false, nullable: true },
		                    cvt1: { editable: true, type : "number"},
		                    cvt2: { editable: true, type : "number"},
		                  }
		                }
		            }
				})
				console.log("#"+gridId)
				$("#"+gridId).data("kendoGrid").setDataSource(dataSource)
				
// 				var grid = $(".grid").data("kendoGrid");
// 				grid.autoFitColumn("spec");
				
			}
		})
		
	}
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				$.hideLoading()
				
				var json = data.dataList;
				console.log(json)
				
				var machineList = "";
				$(json).each(function(idx, data){
					data.name = decode(data.name)
					data.originName = data.name
					
					data.name = data.name.replace(/\s/g,"<br>")
					
					machineList += "<label class='dvc' id=" + data.dvcId + " onclick='gridTabAdd(\"" + data.dvcId + "\" ,\"" + data.originName + "\")'> " + data.name + "</label>";
				});
// 				onClick="gotoNode(\'' + result.name + '\')" />'
// 				" + data.dvcId + "," + data.originName + "
				if(json.length==0){
					machineList += "장비가 없습니다. 품번을 선택해주세요.";
				}
				
				$("#machineList").empty();
				$("#machineList").append(machineList);
				
				$(".dvc").css({
					"text-aling" : "center"
					,"margin-left" : getElSize(30)
					,"border-radius" : getElSize(15)
					,"cursor" : "pointer"
						
				})
// 				 style='text-align:center; margin-left:" + getElSize(30) +"'
				
				$(".flex-container").css({
					"display" : "flex"
				})
				
				$(".flex-container > label").css({
					"background-color" : "rgb(174, 213, 67)"
					,"width" : getElSize(210)
					,"height" : getElSize(210)
					,"line-height" : getElSize(3.2)
				})
				
			}
		});
	};
	//시작메뉴 집어넣기
	function ready(){
		
	}
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//타이틀 클릭시 경로이동
	function pageMove(){
		location.href='${ctxPath}/pop/popMainMenu.do?empCd='+empCd
	}
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
	        "background-color" : "rgb(170, 120, 220)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    $(".menu .mainSpan").css({
	    	"display" : "inline-block",
	    	"margin-top" : originHeight * 0.075
	    })
	    
	    
	    $(".result").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"text-align" : "center",
// 	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px",
	    	"font-weight" : "bold"
	    });
		
	    //popSubMenu 내용 리스트
	    $(".popSubMenu").css({
	    	"height" : originHeight * 0.18 + "px",
	    	"margin-top": originHeight * 0.015 + "px",
// 	    	"overflow" : "auto"
	    });

// 	    //popSubMenu 내용 리스트
// 	    $("#sub_table").css({
// 	    	"height" : $(".popSubMenu").height + "px",
// 	    });
	    
	    //팝업창에서 내용 리스트
	    $(".popContent").css({
	    	"height" : originHeight * 0.48 + "px",
	    	"margin-top": originHeight * 0.015 + "px",
	    	"overflow" : "auto",
	    	"background" : "white"
	    });
	    
	    $(".btnDiv").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"text-align" : "center",
// 	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px"
	    })
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
	    $("button").css({
			"cursor" : "pointer",
			"margin-right" : getElSize(30),
		    "background": "rgb(144, 144, 144)",
		    "border-color": "rgb(34, 35, 39)",
			"vertical-align" : "middle",
 			"height" : getElSize(90),
 			"font-size" : getElSize(45),
 			"border-radius" : getElSize(8),
 			"padding-bottom" : getElSize(10),
 			"padding-top" : getElSize(10)
		});
	    
	    $("select").css({
	    	"font-size" : getElSize(45),
	    	"padding-top" : getElSize(5),
	    	"padding-bottom" : getElSize(5)
	    })
	    
	    $("#showOffWork select").css({
	    	"height": getElSize(112)
	    	,"font-size":"100%"
	    })

	    $("#showOffWork input").css({
	    	"height": getElSize(112)
	    	,"font-size":"100%"
	    })
	    
	    $(".textSize").css({
	    	"font-size" : getElSize(60)
	    })
	    
	}
	
	// 팝업창 생성 팝업 CSS #popup_submenu #showGoWork #showOffWork #showEarlyWork #showOutWork
	function popup_submenu(selected_menu_id,div_id){
		
		console.log(div_id);
		
		//출근 보고 클릭시
		if(div_id=="showGoWork"){
// 			goWork();
// 			location.href = "SelfInspection.do"
			location.href='${ctxPath}/pop/SelfInspection.do'
// 			getCheckList()
// 			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showOffWork"){// 퇴근 보고 클릭시
			offWork();
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showChangeWork"){// 조퇴 보고 클릭시
			ChangeWork(selected_menu_id,div_id);
			openPopup(selected_menu_id,div_id);
		}
	}
	
	//팝업창 띄우기
	function openPopup(selected_menu_id,div_id){
		showCorver()
//		alert("1. :" + selected_menu_id + ", 2. :" + div_id)
		var selected_item = $("#" + selected_menu_id);
		/* var backColor;
		//1번 박스 선택했을경우 배경색
		if(selected_menu_id=="box1"){
			backColor = "rgb(214,240,255)"
		}else{
			backColor = $(selected_item).css("background")
		} */
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })

	    $("#" + div_id).css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.08,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.83,
	        "z-index" : 10,
	    })
	    
	    console.log(div_id)
	}
	
// 	dataSource : json,
// 	autoWidth : true,
// 	dataTextField: "prdNo",
// 	dataValueField: "prdNo",
// //		clearButton: false,
// 	value: json[0].prdNo,
// 	//품번 변경되었을시
// 	change: function(e){
// 		var value = this.value();
		
// 		getDvcListByPrdNo(value)
// 	}
		
	function categoryDropDownEditor(container, options) {
		$('<input name="' + options.field + '"/>')
		.appendTo(container)
		.kendoDropDownList({
			dataSource: [
				{ text: "양호", value: 2 },
				{ text: "불량", value: 1 }
			],
			valuePrimitive: true,
			dataTextField: "text",
			dataValueField: "value",
			optionLabel: "==선택==",
		});
	}

	function selectOption(num){
		if(num==1){
			return "불량";
		}else if(num==2){
			return "양호";
		}else{
			return "선택하세요"
		}
	}
	
	var result = "<select style='font-size:" + getElSize(40) + "' onchange='chkFaulty(this, \"select\")'><option value='0' >${selection}</option><option value='1'>${ok}</option><option value='2'>${faulty}</option></select>";

	function getCheckList(){
		classFlag = true;
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
		var dvcIdName ="";
// 		if($('#Check_dvcId').data('kendoComboBox').dataItem()!=undefined){
// 			dvcIdName = $('#Check_dvcId').data('kendoComboBox').dataItem().name
// 		}
		
		
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + $("#chkTy").val() +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val() + 
					"&dvcId=" + dvcIdName;
// 					"&dvcId=" + $("#Check_dvcId option:selected").html();
// 		var param = "prdNo=FS_RR_LH &chkTy=2&checkCycle=1&date=2019-06-27&ty=2&dvcId=FS/R M#3";
		
		console.log(param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				
				var dataSource = new kendo.data.DataSource({});
				console.log(json)
				$(json).each(function(idx, data){
					
					if(idx%2==0){
						data.rowClass="0"
					}else{
						data.rowClass="1"
					}
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var attrTy;
						if(data.attrTy==1){
							//attrTy = "정성검사";
							attrTy = "${js_check}";
						}else{
							//attrTy = "정량검사";
							attrTy = "${jr_check}";
						};
					
						var date;
						if(data.date==""){
							date = $(".date").val();
						}else{
							date = data.date;
						};
						
						var resultVal, resultVal2, resultVal3, resultVal4;
						
						if(data.attrTy==1){
							resultVal = resultVal2 = resultVal3 = resultVal4 = result;
						}else{
							resultVal = "<input type='text' value='" + data.result + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal2 = "<input type='text' value='" + data.result2 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal3 = "<input type='text' value='" + data.result3 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal4 = "<input type='text' value='" + data.result4 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						};
						
						var chkTy;
						if(decodeURIComponent(data.chkTy).replace(/\+/gi, " ")=="입고검사"){
							chkTy = "${in_check}"
						}else{
							chkTy = decodeURIComponent(data.chkTy).replace(/\+/gi, " ");
						}
						
						data.name = decode(data.name);
						data.attrNameKo = decode(data.attrNameKo);
						data.spec = decode(data.spec);
						data.measurer = decode(data.measurer);
						data.chkTy = chkTy;
						data.attrTy = attrTy;
						data.resultVal = resultVal;
						data.resultVal2 = resultVal2;
						data.id = data.id;
						data.resultVal = 2;
						data.resultVal2 = 2;
						data.result = 2;
						
						data.checkSelect = true;
						if($("#checkCycle").val()==1){
							data.checkCycle = "초물"
						}else if($("#checkCycle").val()==2){
							data.checkCycle = "중물"
						}else{
							data.checkCycle = "종물"
						}
						
						if($("#workTime").val()==1){
							data.workTy="야간";
						}else if($("#workTime").val()==2){
							data.workTy="주간";
						}
						
					}
					dataSource.add(data);
				});
				
				
// 				kendotable.setDataSource(dataSource);
			}
		});
	};
	
	// 화살표 버튼 클릭시 플러스
	function plusCnt(num){
		console.log(num)
		if(num==0.001){
			// 9 보다 클 경우에는 증가 X
			if($("#one").val()>=9){
				return;
			}
			$("#one").val(Number($("#one").val())+1)
		}else if(num==0.01){
			// 9 보다 클 경우에는 증가 X
			if($("#two").val()>=9){
				return;
			}
			$("#two").val(Number($("#two").val())+1)
		}else if(num==0.1){
			// 9 보다 클 경우에는 증가 X
			if($("#three").val()>=9){
				return;
			}
			$("#three").val(Number($("#three").val())+1)
		}else if(num==1){
			// 9 보다 클 경우에는 증가 X
			if($("#four").val()>=9){
				return;
			}
			$("#four").val(Number($("#four").val())+1)
		}else if(num==10){
			// 9 보다 클 경우에는 증가 X
			$("#five").val(Number($("#five").val())+1)
		}
	}
	
	// 화살표 버튼 클릭시 마이너스
	function minusCnt(num){
		console.log(num)
		if(num==0.001){
			// 9 보다 클 경우에는 증가 X
			if($("#one").val()<=0){
				return;
			}
			$("#one").val(Number($("#one").val())-1)
		}else if(num==0.01){
			// 9 보다 클 경우에는 증가 X
			if($("#two").val()<=0){
				return;
			}
			$("#two").val(Number($("#two").val())-1)
		}else if(num==0.1){
			// 9 보다 클 경우에는 증가 X
			if($("#three").val()<=0){
				return;
			}
			$("#three").val(Number($("#three").val())-1)
		}else if(num==1){
			// 9 보다 클 경우에는 증가 X
			if($("#four").val()<=0){
				return;
			}
			$("#four").val(Number($("#four").val())-1)
		}else if(num==10){
			// 9 보다 클 경우에는 증가 X
			if($("#five").val()<=0){
				return;
			}
			$("#five").val(Number($("#five").val())-1)
		}
	}
	
	function saveRow(){

		//tab 갯수 구하기
		var size = $(".grid").length;
		//저장할 리스트
		var savelist = [];
		
		for(i = 0; i<size; i++){
			var gridId = $(".grid")[i].id;
			
			var dataList = $("#"+gridId).data("kendoGrid").dataSource.data();
			
			$(dataList).each(function(idx,data){
				
				console.log(idx + " , " + savelist.length + " : " + data.id)
				data.id=data.id;
				data.attrNameKo=encodeURIComponent(data.attrNameKo);
				data.chkCycle = $("#checkCycle").val()
// 				data.work = empCd;
				data.checker = empCd;
				data.date = $("#date").val();
				data.workTy = Number($("#workTime").val())
// 				savelist.push(data)

// 				if($("#workTime").val()==1){
// 					data.workTy=1;
// 				}else if($("#workTime").val()==2){
// 					data.workTy=2;
// 				}
				

			})
		}
		
		
		
		var obj = new Object();
		obj.val = dataList;

// 		return
		var url = "${ctxPath}/chart/addCheckStandardList.do";
		var param = "val=" + JSON.stringify(obj);
		
		console.log(savelist)
		console.log(param)
		
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				$.hideLoading()
				if(data=="success") {
					alert ("저장이 완료되었습니다.");
					pageMove();
				}else{
					alert("관리자에게 문의하세요_31")
				}
			}
		});
	}
	
	//품번 가져오기
	function getPrdNoList(){
		var url = ctxPath + "/common/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				comPrdList=json;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
// 				el.html(options);

// 				if(typeof(val)!="undefined"){
// 					el.val(val);	
// 				}

				//품번변경시 장비라우팅 부르기
//				el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
				
			}
		});
	};
	
	function offWork(){
		
	}
	
	function getAllCheckTyList(){
		getCommonTyList();
		
		var url = "${ctxPath}/chart/getAllCheckTyList.do"
		var param = "";
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(1111);
				console.log(json);
				
				var chkTy_form = "<option value='0'>선택</option>"; // 검사구분
				var oprNm_form = "<option value='0'>선택</option>"; // 공정
				var situation_form = "<option value='0'>선택</option>"; // 현상구분
				var situationTy_form = "<option value='0'>선택</option>"; // 현상
				var part_form = "<option value='0'>선택</option>"; // 부위
				var cause_form = "<option value='0'>선택</option>"; //원인
				var gchTy_form = "<option value='0'>선택</option>"; //귀책구분
				var gch_form = "<option value='0'>선택</option>"; // 귀책
				var action_form = "<option value='0'>선택</option>"; // 조치
				
				var end = "</select>";
				$(json).each(function(idx,data){
					//검사구분
					if(data.group==2){
						chkTy_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}
					//공정
					if(data.group==3){
						oprNm_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}
					//현상구분
					if(data.group==5){
						situation_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}
					//부위
					if(data.group==4){
						part_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}
					
					//원인
					if(data.group==8){
						gchTy_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}

					//귀책구분
					if(data.group==7){
						cause_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}

					//조치
					if(data.group==10){
						action_form += "<option value=" + data.id +">" + data.cd + ". " +decode(data.name) + "</option>";
					}
					
				})
				
				$("#chkTy_form").append(chkTy_form);
				$("#oprNm_form").append(oprNm_form);
				$("#part_form").append(part_form);
				$("#situation_form").append(situation_form);
				$("#situationTy_form").append(situationTy_form);
				$("#cause_form").append(cause_form);
				$("#gchTy_form").append(gchTy_form);
				$("#gch_form").append(gch_form);
				$("#action_form").append(action_form);
				
				//group 2 => 검사구분	,	group 3 => 공정 ,  group 4 => 부위 ,	group 5 => 현상구분
				//group 6 => 현상구분 소재불량에 대한 현상  ,	 group 8 => 원인
				//group 7 => 귀책구분 , group 10 => 조치 , group 14 => 현상구분 가공불량에 대한 현상, group 17 => 도금불량에 대한 현상
				
				
				selectEvt(json);

			}
		})
		
	}
	
	function getCommonTyList(){
		
		var url = "${ctxPath}/common/getCommonTyList.do";
		var param = "";
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			success : function(data){
				var prdNolist = data.prdNoList.dataList;
				var workerlist = data.workerlist.dataList;
				var dvclist = data.dvclist.dataList;

				var arr = {};
				arr.name = "선택";		arr.id=0;
				arr.dvcName = "선택";		arr.dvcId=0;
				arr.prdNo = "선택";		arr.id=0;
				
				$(prdNolist).each(function(idx,data){
					data.id = data.prdNo;
				})

				$(workerlist).each(function(idx,data){
					data.name = decode(data.name);
				})

				$(dvclist).each(function(idx,data){
					data.dvcName = decode(data.dvcName);
				})
				
				prdNolist.unshift(arr);
				workerlist.unshift(arr);
				dvclist.unshift(arr);

				console.log("=============")
				console.log(prdNolist);
				console.log(workerlist);
				console.log(dvclist);
				console.log("=============")
				
				//품번
				$("#prdNo_form").kendoComboBox({
					dataSource : prdNolist,
					autoWidth : true,
					dataTextField: "prdNo",
					dataValueField: "id",
// 					clearButton: false,
					value: prdNolist[0].id,
					//품번 변경되었을시
					change: function(e){
// 						var value = this.value();
					}
				});

				//장비
				$("#dvcId_form").kendoComboBox({
					dataSource : dvclist,
					autoWidth : true,
					dataTextField: "dvcName",
					dataValueField: "dvcId",
// 					clearButton: false,
					value: dvclist[0].dvcId,
					//품번 변경되었을시
					change: function(e){
// 						var value = this.value();
					}
				});
				
				//등록자
				$("#checker_form").kendoComboBox({
					dataSource : workerlist,
					autoWidth : true,
					dataTextField: "name",
					dataValueField: "id",
// 					clearButton: false,
					value: workerlist[0].id,
					//품번 변경되었을시
					change: function(e){
// 						var value = this.value();
					}
				});
			}
			
		})
	}
	
	function saveRow2(){
		$.showLoading()
		valArray = [];
		
		var obj = new Object();
		
		obj.chkTy = $("#chkTy_form").val();	//검사구분
		obj.prdNo = $("#prdNo_form").val();	//품번
		obj.prdPrc = $("#oprNm_form").val();	//공정
		obj.dvcId = $("#dvcId_form").val();	//장비
		obj.date = $("#date_form").val();	//날짜
		obj.checker = $("#checker_form").val();	//신고자
		obj.part = $("#part_form").val();	//부위
		obj.situationTy = $("#situationTy_form").val();	//현상
		obj.situation = $("#situation_form").val();	//현상구분
		obj.cause = $("#cause_form").val();	//원인
		obj.gchTY = $("#gchTy_form").val();	//귀책구분
		obj.gch = $("#gch_form").val();	//귀책
		obj.cnt = $("#cnt_form").val();	//수량
		obj.action = $("#action_form").val();	//조치
		obj.sendCnt = $("#sendCnt_form").val();
		
		if($("#oprNm_form").val()=="20"){
			obj.proj="0000"
		}else if($("#oprNm_form").val()=="20"){	//소재 자재
			obj.proj="0000"
		}else if($("#oprNm_form").val()=="21"){	//라인 공정
			obj.proj="0005"
		}else if($("#oprNm_form").val()=="22"){	//R	공정
			obj.proj="0010"
		}else if($("#oprNm_form").val()=="23"){	//MCT 공정
			obj.proj="0020"
		}else if($("#oprNm_form").val()=="24"){	//CNC 공정
			obj.proj="0030"
		}else if($("#oprNm_form").val()=="25"){	//완성창고
			obj.proj="0090"
		}else if($("#oprNm_form").val()=="26"){	//고객사
			obj.proj="0"
		}else if($("#oprNm_form").val()=="27"){	//필드
			obj.proj="0"
		}
		valArray.push(obj);
		
		var obj = new Object();
		obj.val = valArray;
		
		var url = "${ctxPath}/chart/saveRow.do";
		var param = "val=" + JSON.stringify(obj);
		
		console.log(param)
		
		/* if($("#chkTy_form select").val()=="0"){
			alert("검사구분 선택하십시오.");
			$("#chkTy_form").focus();
			$.hideLoading();
			return;
		}else  */
		
		if(Number($("#exCnt").html()) < Number($("#cnt_form").val()) || Number($("#exCnt").html()) < Number($("#sendCnt_form").val())){
			alert("재고보다 수량이 많을수 없습니다.");
			$.hideLoading();
			return;
		}
		
		if($("#prdNo_form").val()=="0" || $("#prdNo_form").data("kendoComboBox").select()==-1){
			alert("품번을 선택하십시오.");
			$("#prdNo_form").focus();
			$.hideLoading()
			return;
		}else if($("#prdNo_form").val().indexOf("RW")==-1 && ($("#oprNm_form").val()==20 || $("#oprNm_form").val()==21)){
			alert("완성품번일때 소재,라인창고를 선택하실수 없습니다.")
			$.hideLoading()
			return;
		}else if($("#prdNo_form").val().indexOf("RW")!=-1 && ($("#oprNm_form").val()!=20 && $("#oprNm_form").val()!=21)){
			alert("소재일때는 소재,라인창고만 선택 가능합니다.")
			$.hideLoading()
			return;
		}else if($("#oprNm_form").val()=="0"){
			alert("공정을 선택하십시오.");
			$("#oprNm_form").focus();
			$.hideLoading()
			return;
		}/* else if($("#dvcId_form").val()=="0"){
			alert("장비를 선택하십시오.");
			$("#dvcId_form").focus();
			$.hideLoading()
			return;
		}else if($("#date_form").val()=="0"){
			alert("날짜를 선택하십시오.");
			$("#date_form").focus();
			$.hideLoading()
			return;
		} */else if($("#checker_form").val()=="0" || $("#checker_form").data("kendoComboBox").select()==-1){
			alert("신고자를 선택하십시오.");
			$("#checker_form").focus();
			$.hideLoading()
			return;
		}/* else if($("#part_form").val()=="0"){
			alert("부위를 선택하십시오.");
			$("#part_form").focus();
			$.hideLoading()
			return;
		} */else if($("#situation_form").val()=="0"){
			alert("현상구분 선택하십시오.");
			$("#situation_form").focus();
			$.hideLoading();
			return;
		}else if($("#situationTy_form").val()=="0"){
			alert("현상을 선택하십시오.");
			$("#situationTy_form").focus();
			$.hideLoading()
			return;
		}/*else if($("#cause_form").val()=="0"){
			alert("원인을 선택하십시오.");
			$("#cause_form").focus();
			$.hideLoading()
			return;
		}else if($("#gchTy_form").val()=="0"){
			alert("귀책구분을 선택하십시오.");
			$("#gchTy_form").focus();
			$.hideLoading()
			return;
		}else if($("#gch_form").val()=="0"){
			alert("귀책을 선택하십시오.");
			$("#gch_form").focus();
			$.hideLoading()
			return;
		} */else if($("#cnt_form").val()=="0" || $("#cnt_form").val()==0){
			alert("수량을 입력하세요.");
			$("#cnt_form").focus();
			$("#cnt_form").select();
			$.hideLoading()
			return;
		}/* else if($("#action_form").val()=="0"){
			alert("조치을 선택하십시오.");
			$("#cnt_form").focus();
			$.hideLoading()
			return;
		} *//* else if($("#sendCnt_form").val()=="0" || $("#sendCnt_form").val()==0){
			alert("수량을 입력하세요.");
			$("#sendCnt_form").focus();
			$("#sendCnt_form").select();
			$.hideLoading()
			return;
		} */else if(isNaN($("#cnt_form").val())){
			alert("수량은 숫자만 입력가능합니다.");
			$("#cnt_form").focus();
			$("#cnt_form").select();
			$.hideLoading()
			return;
		}else if(isNaN($("#sendCnt_form").val())){
			alert("수량은 숫자만 입력가능합니다.");
			$("#sendCnt_form").focus();
			$("#sendCnt_form").select();
			$.hideLoading()
			return;
		}else if($("#cnt_form").val() < $("#sendCnt_form").val()){
			alert("불량등록 수량보다 이동수량이 많을수 없습니다.");
			$("#sendCnt_form").focus();
			$("#sendCnt_form").select();
			$.hideLoading()
			return;
		}
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					addRow();
					alert("${save_ok}");
				}
				$.hideLoading()
			}
		});	
	};
	
	
	
	
	
	
	
	
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="pageMove()">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				4. 품질 관리
			</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	
	<div id="content">
      	<table style="width: 100%; height: 100%;">
			<Tr>
			    <td style="text-align: center">
					<div class="menu" id="box1" style="opacity: 0" onclick="popup_submenu('box1','showGoWork')">
					    <span class="mainSpan">4-1 자주검사</span><br>
					    <span class="subSpan">(Self Inspection)</span>
					</div>
					
					<div class="menu" id="box2" style="opacity: 0" onclick="popup_submenu('box2','showOffWork')">
						<span class="mainSpan">4-2 불량등록</span><br>
						<span class="subSpan">(Defect Register)</span>
					</div>

<!-- 					<div class="menu" id="box3" style="opacity: 0" onclick="popup_submenu('box3','showEarlyWork')"> -->
<!-- 					<br> -->
<!-- 					    <span>BOX3</span> -->
<!-- 					</div> -->
<!-- 					<div class="menu" id="box4" style="opacity: 0" onclick="popup_submenu('box4','showOutWork')"> -->
<!-- 			        <br> -->
<!-- 			            <span>BOX4</span> -->
<!-- 			        </div> -->
			    </td>
			</Tr>
		</table>
<!--         <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>         -->
	</div>
	
<!-- 	자주검사 화면 -->
	<div id="showGoWork">
<!-- 		<div class="result"> 자주 검사 -->
		
<!-- 		</div> -->
		<div class="popSubMenu">
			<div id="sub_table" style="width: 100%; height: 100%; overflow: auto; overflow-y:hidden; white-space: nowrap; display: table; table-layout: fixed;">
				<div style="display: table-cell; vertical-align: middle; height: 100%; width: 30%; table-layout: fixed;">
					<div style="text-align: center; font-weight: bold; height: 40%; font-size: 150%;">
						자주 검사
					</div>
					<div style="height: 30%;">
						<spring:message  code="prd_no"></spring:message> : <input id="prdNo">
					</div>
					<div style="height: 30%;">
						<spring:message  code="check_cycle"></spring:message> 
						 <select id="checkCycle" class="textSize"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
						 <select id="workTime" class="textSize" ><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
						 <input id="date" class="date textSize" type="text" readonly="readonly">
					</div>
				</div>
				<div style="display: table-cell; vertical-align: middle; height: 100%; width: 70%; table-layout: fixed; overflow: auto; overflow-y: hidden; ">
					<span id="machineList" class="flex-container">장비가 없습니다. 품번을 선택해주세요.</span>
				
				</div>
			</div>
		</div>
		<div class="popContent">
			<div id="content_table" style="width: 100%; height: 100%;">
				<div id="grid"></div>
			</div>
		</div>
		<div class="btnDiv">
			<button id="startJob" class="btn" onclick="saveRow()">저장</button>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popQuality.do'">취소</button>
		</div>
	</div>
	
	<div id="showOffWork">
		<table style="width: 100%; height: 100%;">
			<Tr> 
				<Td class='table_title'>
<!-- 					품번 -->
					<spring:message code="prd_no"></spring:message> * 
				</Td>
				<Td>
					<input type="text" id="prdNo_form">
				</Td>
				<Td class='table_title' width="20%">
<!-- 					검사구분 -->
					<spring:message code="check_ty"></spring:message>  
				</Td>
				<Td width="30%">
					<select id="chkTy_form"></select>
				</Td>
			</Tr>
			<Tr>
<!-- 				공정 -->
				<Td class='table_title' width="22%"><spring:message code="operation"></spring:message> * </Td>
				<Td><select id="oprNm_form"></select></Td>
<!-- 				장비 -->
				<Td class='table_title'><spring:message code="device"></spring:message></Td>
				<Td><input type="text" id="dvcId_form"></Td>
			</Tr>
			<Tr>
<!-- 				등록자 -->
				<Td class='table_title'><spring:message code="reporter"></spring:message> * </Td>
				<Td><input type="text" id="checker_form"></Td>
<!-- 				발생일자 -->
				<Td class='table_title'><spring:message code="event_date"></spring:message></Td>
				<Td><input type="text" id="date_form" class="date"></Td>
			</Tr>
			<Tr>
<!-- 			현상구분 -->
				<Td class='table_title'><spring:message code="divide_situ"></spring:message> * </Td>
				<Td><select id="situation_form"></select></Td>
<!-- 				부위 -->
				<Td class='table_title'><spring:message code="part"></spring:message></Td>
				<Td><select id="part_form"></select></Td>
			</Tr>
			<Tr>
<!-- 			현상 -->
				<Td class='table_title'><spring:message code="situ"></spring:message> * </Td>
				<Td><select id="situationTy_form"></select></Td>
<!-- 				원인 -->
				<Td class='table_title'><spring:message code="cause"></spring:message></Td>
				<Td><select id="cause_form"></select></Td>
			</Tr>
			<Tr>
<!-- 			귀책구분 -->
				<Td class='table_title'><spring:message code="gch_ty"></spring:message></Td>
				<Td><select id="gchTy_form"></select></Td>
<!-- 				귀책 -->
				<Td class='table_title'><spring:message code="gch"></spring:message></Td>
				<Td><select id="gch_form"></select></Td>
			</Tr>
			<Tr>
<!-- 				수량 -->
				<Td class='table_title'><spring:message code="count"></spring:message> * (<label id="exCnt">0</label>)</Td>
				<Td><input type="text" id="cnt_form" value=0></Td>
<!-- 				조치 -->
				<Td class='table_title'><spring:message code="action"></spring:message></Td>
				<Td><select id="action_form"></select></Td>
			</Tr>
			<Tr>
<!-- 				불량창고이동수량 -->
				<Td class='table_title'>불량창고이동수량 *</Td>
				<Td><input type="text" id="sendCnt_form" value=0></Td>
				<Td></Td>
				<Td></Td>
			</Tr>
			<Tr>
				<Td colspan="4" style="text-align: center; padding-right: 90px">
					<button onclick="saveRow2()"><spring:message code="save"></spring:message></button>
					<button onclick="location.href='${ctxPath}/pop/popQuality.do'"><spring:message code="cancel"></spring:message></button>
				</Td>
			</Tr>
		</table> 
	</div>
	
	<!-- 	    생산실적 수량을 입력하기 위한 키패드 화면 -->
	<div id="keyPad" class="keyPad" style="opacity: 1; position: absolute; top: 0; display: none;">
		<table border="2" class="keyPad" style="width: 100%; height: 100%;text-align: center;">
			<tr class="keyPad">
				<td class="keyPad" onclick="plusCnt(10)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(1)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(0)" style="font-size: 300%; color: red;">
					.
				</td>
				<td class="keyPad" onclick="plusCnt(0.1)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(0.01)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(0.001)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" rowspan="3" style="font-size: 300%;" onclick="inputCnt()">
					INPUT	
				</td>
			</tr>
			<tr class="keyPad">
				<td class="keyPad">
					<input type="number" id="five" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="four" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					.
				</td>
				<td class="keyPad">
					<input type="number" id="three" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="two" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="one" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%; " readonly="readonly">
				</td>
			</tr>
			<tr class="keyPad">
				<td class="keyPad" onclick="minusCnt(10)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(1)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(0)" style="font-size: 300%; color: blue;">
					.
				</td>
				<td class="keyPad" onclick="minusCnt(0.1)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(0.01)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(0.001)" style="font-size: 300%; color: blue;">
					▼
				</td>
			</tr>
		</table>
	</div>
</body>
</html>