<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>

<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<script src="${ctxPath}/js/jquery.js"></script>
<script src="${ctxPath}/js/jquery-ui.min.js"></script>
<script src="${ctxPath}/js/jquery.loading.min.js"></script>
<script src="${ctxPath}/js/moment.js"></script>
<%@ page session = "true" %>
<html lang = "en">

<head>
	<meta charst = "UTF-8">
	<title>POP</title>
 <script src="${ctxPath}/js/jqMap.js"></script>
 <script src="${ctxPath}/js/lib/selection/classie.js"></script>
 <script src="${ctxPath}/js/lib/selection/selectFx.js"></script>
 
 <script src="${ctxPath}/js/lib/velocity.js"></script>
 
 <script src="${ctxPath}/js/jquery-1.9.0.min.js"></script>
        
 <script src="${ctxPath}/js/default1.js"></script>
 <script src="${ctxPath}/js/menu.js"></script>
    <link rel="stylesheet" href="${ctxPath}/css/default1.css">
</head>
       <script>
       var evtMsg;
   	var nm = '<%=(String)session.getAttribute("nm")%>';
   		nm = decode(nm);
   	var empCd = '<%=(String)session.getAttribute("empCd")%>';
   	
   	$(function(){
   		
   		//session 있는지 체크
   		sessionChk();

   		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
//   		msg();
   		
   		//focus TEXT 맞추기
   		var chk_short = true;
   		
   		$(document).bind("keydown keyup", function(e) {
   	        var key = e.keyCode;
   	        var tg = e.target;
   	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
   	        
   	        var specific = key >= 8 && key <= 46;
   	        if(e.type == "keydown") {
   	            if(specific) {
   	                chk_short = false;
   	                return true;
   	            }
   	            if(!specific && chk_short) {
   	            	 $("#empCd").focus().select();
   	                //target_input.focus().select(); return false;
   	            }
   	            if(e.ctrlKey && e.keyCode == 86){
   	            	 $("#empCd").focus().select();
   	            }
   	        } else {
   	            if(specific) {
   	                chk_short = true;
   	            }
   	        }
   	    });
   		
   		//시간
   		setInterval(function() {
   			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
   		}, 1000);
   	})
   	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		
	}
		//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){

		}
	}
	//사원 바코드를 입력해주세요
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>"+ nm +"</marquee></span>")}, 10000)
	}
	function msg(){
		$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>"+ nm +" 님 반갑습니다.</marquee></span>")
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	function getTable(){
		
	}
	
	//출근보고 하기
	function goWork(){
		sessionChk();
		if(empCd==null || empCd=="null"){
			return;
		}
		$.showLoading(); 
		var url = "${ctxPath}/pop/GoWorkSave.do";
		
		
		
		var param = "empCd=" + empCd+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
//			dataType : "json",
			success : function(data){
				if(data=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					alarmMsg();
				}else if(data=="duple"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>이미 출근처리 되었습니다.</marquee></span>")
					alarmMsg();
				}else if(data=="success"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>정상적으로 출근처리 되었습니다.</marquee></span>")
					alarmMsg();

					$("#iText").html("출근처리 되었습니다.")
					$("#iframe").css("display","");
					setTimeout(function() {
						location.href='${ctxPath}/pop/popIndex.do';
					},5000);
//					alert("출근처리 되었습니다.");
//					location.href='${ctxPath}/pop/popIndex.do';
				}else if(data=="fail"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					alarmMsg();
				}
				
				$.hideLoading(); 
			},error : function(e){
				$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
				alarmMsg();
				$.hideLoading(); 

			}
		});
	}
	
	function offWork(){
		sessionChk();
		if(empCd==null || empCd=="null"){
			return;
		}
		$.showLoading(); 
		
		var url = "${ctxPath}/pop/OffWorkSave.do";
		
		
		
		var param = "empCd=" + empCd+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
//			dataType : "json",
			success : function(data){
				if(data=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					alarmMsg();
				}else if(data=="duple"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>출근 기록이 없습니다.</marquee></span>")
					alarmMsg();
				}else if(data=="success"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>정상적으로 퇴근처리 되었습니다.</marquee></span>")
					alarmMsg();
					
					$("#iText").html("퇴근처리 되었습니다.")
					$("#iframe").css("display","");
					setTimeout(function() {
						location.href='${ctxPath}/pop/popIndex.do';
					},5000);
					
//					alert("퇴근처리 되었습니다.");
					
//					location.href='${ctxPath}/pop/popIndex.do';
				}
				console.log(data)
				var json=data.dataList;
				
				$.hideLoading(); 
			},error : function(e){
				console.log("error")	
			}
		});
	}
	
	function startJob(){
		sessionChk();
		if(empCd==null || empCd=="null"){
			return;
		}
		location.href="${ctxPath}/pop/moveStartJob.do"
	}
	
	function endJob(){
		sessionChk();
		if(empCd==null || empCd=="null"){
			return;
		}
		location.href="${ctxPath}/pop/moveEndJob.do"
	}

	
       </script>
 <body>
        <table style="width: 100%">
            <Tr>
                <td style="text-align: center">
                    <div class="menu" id="gt" style="opacity: 0">
                        1. 근태 관리<br>

                        <img src="${ctxPath}/images/gt.png" class="menu_icon" ><br>
                        <span>(time attendance management)</span><br>
                    </div>
                    <div class="menu" id="operation" style="opacity: 0">
                        2. 작업 관리<br>
                            <img src="${ctxPath}/images/clock.png" class="menu_icon" ><br>
                            <span>(operation management)</span><br>
                    </div>
                    <div class="menu" id="history" style="opacity: 0">
                        3. 이력 조회<Br>
                            <img src="${ctxPath}/images/history.png" class="menu_icon" ><br>
                            <span>(history)</span><br>
                    </div>
                </td>
            </Tr>
        </table>
    </body>
</html>