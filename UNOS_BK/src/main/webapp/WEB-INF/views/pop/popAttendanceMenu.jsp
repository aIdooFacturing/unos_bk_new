<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/unos_selection.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>

<style type="text/css">
body{
	margin : 0;
	overflow: hidden;
}
.k-grid thead tr th{
	background : linear-gradient( to bottom, gray, black );
	color: white;
	text-align: center;
}
.k-grid thead tr.k-filter-row th{
	background : linear-gradient( to bottom, #E8D9FF, #A566FF );
	color: white;
	text-align: center;
}
.selectRow {
	background: red !important;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

#ApiGroupList thead{
	background: gray;
}

#ApiGroupList tbody{
	background: white;
}

</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	var ds = new kendo.data.DataSource();

	//퇴근 보고 kendoTable 전역변수
	var kendotable ;
	
	//작업 보고에서 화살표 클릭시 수량 변동하기 위한 전역변수
	var inputCntData ;
	
	//작업시작 리스트
	var startList;
	
	//외출 인지 아닌지 확인하는 변수
	var outingAll;
	
	// box chk
	var box;
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	// 팝업창 생성 팝업 CSS #popup_submenu #showGoWork #showOffWork #showEarlyWork #showOutWork
	function popup_submenu(selected_menu_id,div_id){
		
		console.log(selected_menu_id);
		console.log(div_id);
		
		//출근 보고 클릭시
		if(div_id=="showGoWork"){
			// 출근 저장하기
			postCommute("I",0,"");
			// 작업시작 리스트 가져오기
			goWork();
			//팝업창 생성
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showOffWork"){// 퇴근 보고 클릭시
			// 퇴근 저장하기
			postCommute("O",0,"");
			// 작업 종료 리스트 가져오기
			box = "box2"
			offWork();
			//팝업창 생성
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showEarlyWork"){// 조퇴 보고 클릭시
			box = "box3";
			//조퇴 사유 선택하기
			earlyWork(selected_menu_id,div_id);
		}else if(div_id=="showOutWork"){// 외출 보고 클릭시
			
			outWork(selected_menu_id,div_id);
		}else if(div_id=="showInWork"){
			// 외출 복귀 저장하기
			postCommute("I",1,"");

			
			outWork(selected_menu_id,div_id);
		}else if(div_id=="showComplete"){
			//팝업창 생성
			//openPopup(selected_menu_id,div_id);
		}else if(div_id=="showGroupWare"){
			//휴가 신청시 처리하는 함수
// 			alert("기능 구현중입니다.")
// 			return;
			groupWareOpen();
			
// 			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showGroupWareList"){
// 			alert("기능 구현중입니다.")
// 			return;
			//결재 리스트 가지고 오기
			groupWareListOpen();
			//화면 띄우기
			openPopup(selected_menu_id,div_id);
		}

	}
	
	$(function(){
		
		$.showLoading();
		
		//외출 했는지 확인하기
		chkOuting();
		
		//grid 그리기
		gridTable();
		
		//주변 어둡게 하기위해
		createCorver();
		
		//css 설정
		setEl();
		
		//세션 체크
		sessionChk();
		
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setTimeout(()=>{
			$.hideLoading();
		}, 500)
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})

	//시작메뉴 집어넣기
	function ready(){
		
	}
	
	//외출 했는지 확인하기
	function chkOuting(){
//		var url = "192.168.0.55:8080/aIdoo/popApi/chkOuting.do";
		var url = "${ctxPath}/popApi/chkOuting.do"; 
		
		var param = "worker=" + empCd;
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				outingAll = data
				if(data=="YES"){
					$("#box4 .mainSpan").html("1-4 외출 복귀")
					$("#box4 .subSpan").html("(Came Back)")
					$("#box4").attr("onclick","popup_submenu('box4','showInWork')")
					
				}else if(data=="FAILED"){
					alert("관리자한테 문의하세요.1111");
				}else if(data=="NO"){
					$("#box4 .mainSpan").html("1-4 외출 보고")
					$("#box4 .subSpan").html("(Go Out)")
				}

// 				<span class="mainSpan">1-4 외출 보고</span><br>
// 			    <span class="subSpan">(Go to work)</span>
			},error : function(request,status,error){
				 alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				$.hideLoading()
				 
			}
		})
	}
	
	function openPopup(selected_menu_id,div_id){
		showCorver()
		var selected_item = $("#" + selected_menu_id);
		
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })

	    $("#" + div_id).css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.13,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.73,
	        "z-index" : 10,
	    })
	}
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50),
            "text-align" : "center",
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});
		
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
	        "background-color" : "rgb(80, 140, 245)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
  	    //이력조회 배경색
	    $("#box4").css({
	        "background-color" : "rgb(118, 182, 188)"
	    })

	    //groupware 휴가 신청 배경색
	    $("#box5").css({
	        "background-color" : "rgb(209, 120, 255)"
	    })

	    //groupware 휴가 신청 배경색
	    $("#box6").css({
	        "background-color" : "rgb(166, 166, 166)"
	    })
	    
	    // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    //출근보고 클릭시 나오는 화면
	    $(".popCss").css({
	    	"text-align" : "center",
			"font-size" : $("#logo").width() * 0.18 + "px",
		    "border": $("#logo").width() * 0.03 + "px solid white",
		    "transition": "all 1s ease 0s",
		    "border-radius": $("#logo").width() * 0.1 + "px",
	    })
	    
	    
	    $(".result").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px"
	    });
		
	    //팝업창에서 내용 리스트
	    $(".popContent").css({
	    	"height" : originHeight * 0.55 + "px",
	    	"margin-top": originHeight * 0.04 + "px",
	    	"overflow" : "auto"
	    });
	    
	    //위쪽 화살표
	    $(".allowImg").css({
	    	"float": "right",
	    	"height" : originHeight * 0.045 + "px",
	    	"margin-right" : originWidth * 0.015,
	    	"padding" : originHeight * 0.012,
	    	"background" : "gray",
	    	"cursor" : "pointer"
	    })

	     $("#gt_3_reason").css({
	        "background-color" : "rgba(0,0,0,0)",
	        "border" : "0",
	        "color" : "black",
	        "inline" : "0",
	        "outline" : "0",
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(200) + "px",
	        "transition" : "1s"
	    })
	    
		 $("#gt_3_reason option").css({
	        "background-color" : "rgba(0,0,0,5)",
	        "color" : "white",
		    "border" : "1",
	        "transition-delay" : 0.05,
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(200) + "px",
	        "transition" : "1s"
	    })
	    
	    $("#gt_4_reason").css({
	    	"background-color" : "rgba(0,0,0,0)",
	        "border" : "1",
	        "color" : "black",
	        "inline" : "0",
	        "outline" : "0",
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(200) + "px",
	        "transition" : "1s"
	    })
	    
	    $("#gt_4_reason option").css({
	        "background-color" : "rgba(0,0,0,5)",
	        "color" : "white",
	        "transition-delay" : 0.05,
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(200) + "px",
	        "transition" : "1s"
	    })
			    
	    $(".btn").css({
	    	/* "top" : $("#content").height() * 0.96,
	    	"left" : getElSize(30),
	    	"position" : "absolute" , */
	    	"margin-top" : originHeight * 0.03 + "px",
	        "background-color": "lightgray",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
  	    $(".btn2").css({
	    	/* "top" : $("#content").height() * 0.96,
	    	"left" : getElSize(30),
	    	"position" : "absolute" , */
	    	"margin-top" : originHeight * 0.03 + "px",
	        "background-color": "lightgray",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
	    $(".btn").hover(function(){
	        $(this).css({
	            "background-color" : "red"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : "lightgray"
	        })
	    });

	    $(".btn2").hover(function(){
	        $(this).css({
	            "background-color" : "red"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : "lightgray"
	        })
	    });
	    
	    $(".menu .mainSpan").css({
	    	"display" : "inline-block",
	    	"margin-top" : originHeight * 0.075
	    })
	    //box1 span
	}
//	$("#content").height() * 0.80

	//출근 보고 클릭시 뜨는 화면?
	function goWork(){
		$.showLoading(); 
		var url = "${ctxPath}/popApi/getListPrdTg.do";
		
		var param = "empCd=" + empCd;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data)
				var json = data;
				startList = json;
				console.log()
				var table = "<table border='3' id='tableList' style='width:80%; height:100%;  text-align:center; margin-left:10%; background:white;'>"
					table += "<tr><th rowspan=" + (json.length + 1) +"> 작업 리스트 </th></tr>"
//					table += "<tr><th>장비</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'								location.href='${ctxPath}/pop/popIndex.do'
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.empCd = empCd;
					table += "<tr><td>" + data.name + "</td></tr>"
						
				});
				
				table += "</table>";
				
				$("#joblist").empty()
				$("#joblist").append(table)
				
				$.hideLoading();
			}
		})
				
	}
	//퇴근 보고시 뜨는 화면
	function offWork(){
		//작업보고
		$.showLoading(); 
		
		var date ;
		var workIdx ;
		if(moment().format("HH:mm")<"09:00"){
			date = moment().subtract(1, 'day').format("YYYY-MM-DD")
			workIdx = 1
//			$("#nd").val(1)
		}else{
			date = moment().format("YYYY-MM-DD")
			workIdx = 2
//			$("#nd").val(2)
		}
		
		console.log(date);
		console.log()
		var list=[];
		var url = "${ctxPath}/chart/getTodayWorkByName.do";
		var param = "&date=" + date +
					"&workIdx=" + workIdx +
//		"&empCd=" + $("#popup2 .worker").val() +
		"&empCd=" + empCd ;
		
		console.log(param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				try{
					if(typeof json[0].workTy != 'undefined'){
						$("#popup2 #nd").val(json[0].workTy)
					}
				}catch(e){
					modal_Window.content("등록된 작업이 없거나 작업한 장비가 라우팅 되지 않았습니다.<br> 장비가 라우팅 되었는지, 일 생산계획에 작업자가 등록 되었는지 확인 하세요.").open();
				}
				
				
				var tr = "";
				$(".content").remove();
				$(json).each(function(idx, data){
					var targetRatio = Math.round(Number(data.workerCnt) / Number(data.tgCnt) * 100);
					
					if(Number(data.tgCnt)==0){
						targetRatio = 0
					}
					
					var arr=new Object();
					arr.prdNo=data.prdNo
					arr.a=data.dvcId;
					arr.b=decode(data.name);
					arr.c=data.tgCnt;
					arr.d=data.partCyl;
					arr.e=data.cntCyl
					arr.f=data.workerCnt;
					arr.g=targetRatio;
					arr.h=data.lot1;
					arr.i=data.lot2;
					arr.j=data.lot3;
					arr.k=data.lot4;
					arr.l=data.lot5;
					arr.action=''
					arr.empCd=data.empCd
					list.push(arr);
				});
				
				var dataSource = new kendo.data.DataSource({
	                data: list,
	                autoSync: true,
	                schema: {
	                    model: {
	                      id: "a",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                    	 prdNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         b: { editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         c: { editable: false,validation: { required: true } },
	                         d: { editable: false,validation: { required: true } },
	                         e: { editable: false,validation: { required: false },nullable:true },
	                         f: { editable: true,validation: { required: false },nullable:true },
	                         g: { editable: false,validation: { required: true } },
	                         h: { editable: true,validation: { required: false },nullable:true },
	                         i: { editable: true,validation: { required: false },nullable:true },
	                         j: { editable: true,validation: { required: false },nullable:true },
	                         k: { editable: true,validation: { required: false },nullable:true },
	                         l: { editable: true,validation: { required: false },nullable:true },
	                         action: {
	     		          		editable: false,
		     		        },
		     		        newRow: {
		     		          		editable: false,
		     		          		 type: "Boolean"
		     		        }
	                      }
	                    }
	                }
	             });
				
				kendotable.setDataSource(dataSource)
				
				$.hideLoading(); 
			}
		});
	}
	
	function earlyWork(selected_menu_id,div_id){
		showCorver()
		var selected_item = $("#" + selected_menu_id);
		
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })

	    $("#reasonEarly").css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.35,
	        "left": $("#content").width() * 0.29,
	        "height": $("#content").height() * 0.40,
	        "width": $("#content").width() * 0.43,
	        "z-index" : 10,
		    "border": $("#logo").width() * 0.03 + "px solid white",
		    "transition": "all 1s ease 0s",
		    "border-radius": $("#logo").width() * 0.1 + "px",

	    })
	    
	    //조퇴 클릭시
	    $("#ealryJob").click(function(){
	    	var bigo = $("#gt_3_reason").val();
			// 조퇴 저장하기
			postCommute("O",0,bigo);

	    	offWork();

	    	$("#reasonEarly").css({
				"display" : "none"
			})
	    	$("#reName").html("조퇴 처리가 되었습니다.");
			openPopup(selected_menu_id,"showOffWork");
	    })
	}
	function outWork(selected_menu_id,div_id){
		showCorver()
		var selected_item = $("#" + selected_menu_id);
		
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })
	    
	    $("#" + div_id).css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.35,
	        "left": $("#content").width() * 0.29,
	        "height": $("#content").height() * 0.40,
	        "width": $("#content").width() * 0.43,
	        "z-index" : 10,
		    "border": $("#logo").width() * 0.03 + "px solid white",
		    "transition": "all 1s ease 0s",
		    "border-radius": $("#logo").width() * 0.1 + "px",

	    })
	    
	}
	
	function pageMove(){
		location.href='${ctxPath}/pop/popMainMenu.do?empCd='+empCd
	}

	// 화살표 버튼 클릭시 플러스
	function plusCnt(num){
		if(num==1){
			// 9 보다 클 경우에는 증가 X
			if($("#one").val()>=9){
				return;
			}
			$("#one").val(Number($("#one").val())+1)
		}else if(num==10){
			// 9 보다 클 경우에는 증가 X
			if($("#two").val()>=9){
				return;
			}
			$("#two").val(Number($("#two").val())+1)
		}else if(num==100){
			$("#three").val(Number($("#three").val())+1)
		}
	}
	
	// 화살표 버튼 클릭시 마이너스
	function minusCnt(num){
		if(num==1){
			// 0보다 작을경우에는 감소 X
			if($("#one").val() <= 0){
				return;
			}
			$("#one").val(Number($("#one").val())-1)
		}else if(num==10){
			// 0보다 작을경우에는 감소 X
			if($("#two").val() <= 0){
				return;
			}
			$("#two").val(Number($("#two").val())-1)
		}else if(num==100){
			// 0보다 작을경우에는 감소 X
			if($("#three").val() <= 0){
				return;
			}
			$("#three").val(Number($("#three").val())-1)
		}
	}
	
	// 클릭시 수량 집어넣기
	function inputCnt(){
		console.log("넣자")
		var cnt = Number($("#three").val() + $("#two").val() + $("#one").val() );
		inputCntData.f = cnt ;
		kendotable.dataSource.fetch();
		
		$("#keyPad").css({
			"display":"none",
		})
		
		tCons.className = "";
	}
	
	var tCons
	//퇴근 (작업보고) 그리드 그리기
	function gridTable(){
		kendotable = $(".kendo").kendoGrid({
			editable:false,
			dataBound:function(){
				grid = this;
				grid.tbody.find('tr').each(function(){
					var item = grid.dataItem(this);
					kendo.bind(this,item);
				})
				
				$(".kendo thead tr th").css({
					"font-size" : $("#logo").width() * 0.16 + "px",
					"text-align" : "center",
				    "vertical-align" : "middle"
				});
				
				$(".kendo tbody tr td").css({
					"font-size" : $("#logo").width() * 0.13 + "px"
				})
				
				
				//숫자 입력을 위한 이벤트 생성
				$("input.cntInput").click(function(e){
					
					var tr = $(this).closest("tr");
					var uid = tr[0].dataset.uid;
					
					var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
					
					//기존 초기화
					if(tCons!=undefined){
						tCons.className = "";
					}
					
					tCons = tr[0];
					tCons.className = "selectRow";
		    		$("#one").val(0)
		    		$("#two").val(0)
		    		$("#three").val(0)
					inputCntData = grid.dataItem(row)
					// 클릭한 데이터 숫자 값
				    var nStr = inputCntData.f.toString();
					// 1,10,100,1000 의 자리 분리를 위해서 반목문
				    for(var i = 0; i < nStr.length; i++){
				    	// 1의 자리일 경우
				    	if(i==nStr.length-1){
				    		$("#one").val(Number(nStr[i]))
				    	}else if(i==nStr.length-2){
				    		$("#two").val(Number(nStr[i]))
				    	}else{
				    		$("#three").val(Number($("#three").val()+nStr[i]))
				    	}
// 				        answer.push(Number(nStr[nStr.length -1 -i]));
				    }
				    
				    
					console.log(inputCntData)
// 					console.log(answer)
					
					
					$("#keyPad").css({
						"background":"gray",
						"width": originWidth * 0.33,
						"height": originHeight * 0.25,
						"margin-top": $("#header").height() * 1.12,
						"z-index": 11,
						"display":""
					})
					$("#keyPad").css({
						"left" : (originWidth / 2) - ($("#keyPad").width() / 2),
					})
					
					/* $("#headTitle").css({
						"position":"absolute",
						"top":  $("#header").height()/2 - $("#header").height() * 0.36,
			//			"left": getElSize(2000),
						
						"display" : "inline-block",
				        "background-color" : "rgb(80, 140, 245)",
				        "border" : getElSize(7) + "px solid white",
				        "border-radius" : getElSize(30) + "px",
				        "padding-left" : getElSize(15),
				        "padding-right" : getElSize(15),
				        "cursor" : "pointer",
				        
						"font-size": $("#logo2").height() *  0.7
					}) */
			
					$("#headTitle").css({
						"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
					})
				})
				
				//입력창 나올때 이벤트 발쌩
				$('html').click(function(e) {
//					console.log(e.target)
					if(!($(e.target).hasClass("keyPad") || $(e.target).hasClass("cntInput")) ){
						if(tCons!=undefined)
						tCons.className = "";
						
						$("#keyPad").css({
							"display":"none",
						})
					}
				});
				
				//scroll 이동 시키기
				$("#scrollUp").click(function(){
					var scroll = $("div.k-grid-content").scrollTop();
					$("div.k-grid-content").scrollTop(scroll-100);
				})
				//scroll 이동 시키기
				$("#scrollDown").click(function(){
					var scroll = $("div.k-grid-content").scrollTop();
					$("div.k-grid-content").scrollTop(scroll+100);
				})
		    },
		    change: function(e) {
		        var selectedRows = this.select();
		        console.log(selectedRows)
	      	},
	      	edit: function(e) {
	      		console.log(e)
	      	    if (!e.model.isNew()) {
	      	      // Disable the editor of the "id" column when editing data items
	      	      var numeric = e.container.find("input[name=id]").data("kendoNumericTextBox");
	      	      numeric.enable(false);
	      	    }
	      	},
	      	columns:[
				{
					field:"prdNo",title:"차종",width: originWidth * 0.08
				},
				{
					field:"b",title:"장비",width:	originWidth * 0.09
				},
				{
					field:"c",title:"생산 계획<br>수량",width: originWidth * 0.055
				},{
					title:"생산실적",
					columns:[{
						field:"d",title:"HMI",width: originWidth * 0.035
					},{
						field:"e",title:"aIdoo",width: originWidth * 0.038
					},{
						field:"f",
						title:"생산실적수량",
						width: originWidth * 0.07,
						template:'<input type="number" class="cntInput" min="0" data-role="number" style="width:100%; font-size:200%;"  data-bind="value:f" readOnly="readOnly"/>',
					}]
				},{
					field:"g",
					title:"달성률",
					width: originWidth * 0.042,
					template:"#=kendo.toString((f/c)*100, 'n0')#%"
				}/* ,{
					title:"LOT Number",attributes: {
        				style: "text-align: center; color:white; font-size:" + getElSize(45)
        			},headerAttributes: {
        				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
        			}							,
					columns:[{
						field:"h",
						title:"#1",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text'style='width:100%;' data-bind='value:h'/>"
					},{
						field:"i",
						title:"#2",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;'  data-bind='value:i'/>"
					},{
						field:"j",
						title:"#3",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:j'/>"
					},{
						field:"k",
						title:"#4",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:k'/>"
					},{
						field:"l",
						title:"#5",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:l'/>"
					}
					]
				} */]
		}).data("kendoGrid")
	}
	
	//출근 저장하기
	function postCommute(ty,outing,bigo){
		$.showLoading(); 
		console.log(ty + " , " + outing + " , " + bigo + " ,")
		var url = "${ctxPath}/popApi/postCommute.do";
		var param = "worker=" + empCd +
					"&ty_start=" + ty +
					"&is_outing=" + outing +
					"&pop_id=" + popId +
					"&bigo=" + bigo;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="SUCCESS"){
					console.log("출근")
					console.log(data)
					$.hideLoading(); 
				}else if(data=="FAILED"){
					alert("관리자에게 문의해주세요._1");
					$.hideLoading(); 
				}else{
					alert("관리자에게 문의해주세요._11");
					$.hideLoading(); 
				}
			},error : function(data){
				alert("관리자에게 문의해주세요._2");
				$.hideLoading(); 
			}
		})
	}
	
	function outingSave(){
		
		var bigo = $("#gt_4_reason").val()
		// 외출 저장하기
		postCommute("O",1,bigo);

		$("#showOutWork").html(
			'<table style="width: 100%; height: 100%; font-size: 200%;">' +
				'<tr><td> 외출 처리가 되었습니다. </td></tr>' +
				'<tr><td> <button id="cancle" class="btn2" onclick="location.href=\'${ctxPath}/pop/popIndex.do\'">확인</button>' +
			'</table>'
		)
//		location.href='${ctxPath}/pop/popIndex.do'
	}
	//작업 시작 보고하기
	function postJobHist(){
		console.log("시작");
		console.log(startList);
		
		$(startList).each(function(idx,data){
			data.pop_id = popId;
			data.worker = empCd;
			data.dvc_id = data.dvcId;
			data.ty_se = 1;
			
			data.dvc_cnt = 0;
			data.rst_cnt = 0;
			data.prd_no = "";
		})
		
		var obj = new Object();
		obj.val = startList;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		$.showLoading()
		
		var url = "${ctxPath}/popApi/postJobHist.do"
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="SUCCESS"){
					
					
					$("#showGoWork").css({
						"display" : "none",
						"z-index" : 0
					})
				    
				    $("#showComplete").css({
				        "transition" : "1s",
				        "opacity" : 1,
				        "display" : "",
				        "position": "absolute",
				        "background": $("#box1").css("background"),
				        "top": $("#content").height() * 0.35,
				        "left": $("#content").width() * 0.29,
				        "height": $("#content").height() * 0.40,
				        "width": $("#content").width() * 0.43,
				        "z-index" : 10,
					    "border": $("#logo").width() * 0.03 + "px solid white",
					    "transition": "all 1s ease 0s",
					    "border-radius": $("#logo").width() * 0.1 + "px",
	
				    })
				    
				    $.hideLoading();
				}else if(data=="FAILED"){
					alert("관리자에게 문의하세요_100");
					$.hideLoading();
					return;
				}else{
					alert("관리자에게 문의하세요_101");
					$.hideLoading();
					return;
				}
			    
			} 
		})
		
	}
	
	//퇴근 작업보고
	function jobEndHist(){

		console.log("작업종료");
		
		var startList = kendotable.dataSource.data();
		var savelist =[];
		$(startList).each(function(idx,data){
			var arr = {};
			arr.worker = empCd;
			arr.dvc_id = data.id;
			arr.ty_se = 2;
			arr.pop_id = popId;
			if(kendotable.dataSource.data()[0].idx==null || kendotable.dataSource.data()[0].idx=="null"){
                arr.dvc_cnt = 0;
            }else{
                arr.dvc_cnt = data.e;
            }
			arr.rst_cnt = data.f;
			arr.prd_no = data.prdNo;
			savelist.push(arr);
			
/* 			data.pop_id = 1;
			data.worker = empCd;
			data.dvc_id = data.dvcId;
			data.ty_se = 1; */
		})
		
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		$.showLoading()
		
		var url = "${ctxPath}/popApi/postJobHist.do"
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="SUCCESS"){ 
					$("#showOffWork").css({
						"display" : "none",
						"z-index" : 0
					})
				    
				    $("#showComplete").css({
				        "transition" : "1s",
				        "opacity" : 1,
				        "display" : "",
				        "position": "absolute",
				        "background": $("#" + box).css("background"),
				        "top": $("#content").height() * 0.35,
				        "left": $("#content").width() * 0.29,
				        "height": $("#content").height() * 0.40,
				        "width": $("#content").width() * 0.43,
				        "z-index" : 10,
					    "border": $("#logo").width() * 0.03 + "px solid white",
					    "transition": "all 1s ease 0s",
					    "border-radius": $("#logo").width() * 0.1 + "px",

				    })
					popup_submenu("box2","showComplete");
					$.hideLoading()
				}else if(data=="FAILED"){
					alert("관리자 에게 문의하세요._200");
					$.hideLoading()
					return
				}
			}
		})
		
		

	}
	
	//휴가신청시 처리하는 화면 보여주기
	function groupWareOpen(){

// 		var param="shopId=" + shopId.toString()
// 				+"&id=" + empCd
// 				+"&ty=" + "writer"
// 				+"&status=" + "all";
			
// 		$.ajax({
// 			type : "get",
// 			url : "https://www.bkpi.co.kr/EA/Document?shopId=" + shopId + "&writerId=" + empCd +"&title=휴가%20신청서&formId=4",
// 			 data : param,
// 			//  data : JSON.stringify(param),
// 			dataType : "json",
// 			success : function(data) {
// 				console.log("asd");
// 			},error:function(request,status,error){
// 				console.log("dsa")
// 				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
// 			}
// 		});
		
		window.open("https://www.bkpi.co.kr/EA/Document?shopId=" + shopId + "&writerId=" + empCd +"&title=휴가%20신청서&formId=4")
	}
	
	//결재함 리스트보기
	function groupWareListOpen(){
		
		var param="shopId=" + shopId.toString()
				+"&id=" + empCd
				+"&ty=" + "writer"
				+"&status=" + "all";
			
		$.ajax({
			type : "get",
			url : "https://bkpi.co.kr/EA/getMyESList",
			 data : param,
			//  data : JSON.stringify(param),
			dataType : "json",
			success : function(data) {
				
				totalgroupList = data;
				
				var table = "<table width='92%' style='margin-left:4%;'>";
					table += "<thead><tr><td>idx</td>";
					table += "<td>title</td>"
					table += "<td>상태</td>"
					table += "<td>등록 날짜</td></tr></thead><tbody>"
				
				$(data).each(function(idx,data){
					table += "<tr><td>" + data.id + "</td>"
					table += "<td id=" + data.id + " onclick='detailList(this)' style='color:blue;'>" + data.form + "</td>"
					table += "<td>" + data.status + "</td>"
					table += "<td>" + data.date + "</td></tr>"
				});
				
				table += "</tbody></table>"
				
				$("#ApiGroupList").append(table);
			    console.log(data)
			},error:function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			}
		});
		
// 		$("#ApiGroupList");
		console.log("결재함");
	}
	
	function detailList(id){
		var item;
		//id 게시글 정보 찾기
		$(totalgroupList).each(function(idx,data){
			if(id.id==data.id){
				item = data
			}
		})
		
		window.open("https://www.bkpi.co.kr/EA/ESViewer?shopId=" + item.shop_id + "&formId=" + item.form_id +"&docuId=" + item.id +"&uId=" + item.writer_id)

		console.log(item);
	}
//	location.href='${ctxPath}/pop/popAttendanceMenu.do'
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="pageMove()">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				1. 근태관리
			</div>
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
		<table style="width: 100%; height: 100%;">
			<Tr>
			    <td style="text-align: center">
					<div class="menu" id="box1" style="opacity: 0" onclick="popup_submenu('box1','showGoWork')">
					    <span class="mainSpan">1-1 출근 보고</span><br>
					    <span class="subSpan">(Go to work)</span>
					</div>
					<div class="menu" id="box2" style="opacity: 0" onclick="popup_submenu('box2','showOffWork')">
					    <span class="mainSpan">1-2 퇴근 보고</span><br>
					    <span class="subSpan">(Leave work)</span>
					</div>
					
					<div class="menu" id="box5" style="opacity: 0" onclick="popup_submenu('box5','showGroupWare')">
					    <span class="mainSpan">1-3 휴가 신청</span><br>
					    <span class="subSpan">(Paid Leave)</span>
					</div>

					<div class="menu" id="box6" style="opacity: 0" onclick="popup_submenu('box6','showGroupWareList')">
					    <span class="mainSpan">1-4 결재함</span><br>
					    <span class="subSpan">(Approval List)</span>
					</div>
					
<!-- 					<div class="menu" id="box3" style="opacity: 0" onclick="popup_submenu('box3','showEarlyWork')"> -->
<!-- 					    <span class="mainSpan">1-3 조퇴 보고</span><br> -->
<!-- 					    <span class="subSpan">(Leave Early)</span> -->
<!-- 					</div> -->
<!-- 					<div class="menu" id="box4" style="opacity: 0" onclick="popup_submenu('box4','showOutWork')"> -->
<!-- 			            <span class="mainSpan">1-4 외출 보고</span><br> -->
<!-- 					    <span class="subSpan">(Go Out)</span> -->
<!-- 			        </div> -->
			    </td>
			</Tr>
		</table>
	</div>
        
<!-- 		출근 클릭시 보여주는 화면 -->
        <div id="showGoWork" class="popCss" style="opacity: 0; display: none;">
			<div class="result"> 정상 출근 처리가 되었습니다.
			
			</div>
			<div id="joblist" class="popContent"></div>
			<div>
				<button id="startJob" class="btn" onclick="postJobHist()">승인</button>
				<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">거부</button>
			</div>
        </div>
        
<!--         퇴근 클릭시 보여주는 화면 -->
       	<div id="showOffWork" class="popCss" style="opacity: 0; display: none;">
			<div class="result"> <span id="reName">정상 퇴근 처리가 되었습니다.</span>
				<img id="scrollUp" class="allowImg" src="${ctxPath}/css/images/icons-png/arrow-u-black.png"/>
			</div>
			<div id="woringList" class="popContent kendo"></div>
			<div>
				<button id="startJob" class="btn" onclick="jobEndHist()">승인</button>
				<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">거부</button>
				<img id="scrollDown" class="allowImg" src="${ctxPath}/css/images/icons-png/arrow-d-black.png"/>
			</div>
		</div>
		
<!-- 		조퇴 사유 입력  -->
		<div id="reasonEarly" style="display: none; opacity: 0; text-align: center;">
			<table style="width: 100%; height: 100%; text-align: center;">
				<tr>
					<td style="font-size: 200%;">조퇴 사유를 선택해주세요.</td>
				</tr>
				<tr>
					<td>
						<select id='gt_3_reason' class='unos_select'>
							<option>반차</option>
							<option>병가</option>
							<option>개인 사정</option>
							<option>기타</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<button id="ealryJob" class="btn2">조퇴</button>
						<button id="cancle" class="btn2" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">취소</button>
					</td>
				</tr>
			</table>
		</div>
		
<!-- 		조퇴 클릭시 보여주는 화면 -->
<%--        	<div id="showEarlyWork" class="popCss" style="opacity: 0; display: none;">
			<div class="result"> 조퇴 처리가 되었습니다.
				<img id="scrollUp" class="allowImg" src="${ctxPath}/css/images/icons-png/arrow-u-black.png"/>
			</div>
			<div id="woringList2" class="popContent kendo"></div>
			<div>
				<button id="startJob" class="btn" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">승인</button>
				<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">거부</button>
				<img id="scrollDown" class="allowImg" src="${ctxPath}/css/images/icons-png/arrow-d-black.png"/>
			</div>
		</div>
		 --%>

<!-- 		외출 클릭시 보여주는 화면 -->
       	<div id="showOutWork" style="opacity: 0; display: none; text-align: center;">
			<table style="width: 100%; height: 100%; text-align: center;">
				<tr>
					<td style="font-size: 200%;">외출 사유를 선택해주세요.</td>
				</tr>
				<tr>
					<td>
						<select id='gt_4_reason' class='unos_select'>
							<option>병가</option>
							<option>개인 사정</option>
							<option>기타</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>
						<button id="ealryJob" class="btn2" onclick="outingSave()">외출</button>
						<button id="cancle" class="btn2" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">취소</button>
					</td>
				</tr>
			</table>
		</div>
		
       	<div id="showInWork" style="opacity: 0; display: none; text-align: center;">
			<table style="width: 100%; height: 100%;">
				<tr>
					<td style="font-size: 200%;">외출 복귀 되었습니다.</td>
				</tr>
				<tr>
					<td>
						<button id="cancle" class="btn2" onclick="location.href='${ctxPath}/pop/popIndex.do'">확인</button>
					</td>
				</tr>
			</table>
		</div>

       	<div id="showComplete" style="opacity: 0; display: none; text-align: center;">
			<table style="width: 100%; height: 100%;">
				<tr>
					<td style="font-size: 200%;">처리 완료되었습니다.</td>
				</tr>
				<tr>
					<td>
						<button id="cancle" class="btn2" onclick="location.href='${ctxPath}/pop/popIndex.do'">확인</button>
					</td>
				</tr>
			</table>
		</div>
	    
	    <div id="showGroupWareList" style="opacity: 0; display: none; text-align: center;">
	    
	    	<div class="result"> 결재 보관함
			
			</div>
			<div id="ApiGroupList" class="popContent"></div>
			<div>
				<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">확인</button>
			</div>

	    </div>
	    
	    
<!-- 	    생산실적 수량을 입력하기 위한 키패드 화면 -->
		<div id="keyPad" class="keyPad" style="opacity: 1; position: absolute; top: 0; display: none;">
			<table border="2" class="keyPad" style="width: 100%; height: 100%;text-align: center;">
				<tr class="keyPad">
					<td class="keyPad" onclick="plusCnt(100)" style="font-size: 300%; color: red;">
						▲
					</td>
					<td class="keyPad" onclick="plusCnt(10)" style="font-size: 300%; color: red;">
						▲
					</td>
					<td class="keyPad" onclick="plusCnt(1)" style="font-size: 300%; color: red;">
						▲
					</td>
					<td class="keyPad" rowspan="3" style="font-size: 300%;" onclick="inputCnt()">
						INPUT	
					</td>
				</tr>
				<tr class="keyPad">
					<td class="keyPad">
						<input type="number" id="three" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
					</td>
					<td class="keyPad">
						<input type="number" id="two" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
					</td>
					<td class="keyPad">
						<input type="number" id="one" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%; " readonly="readonly">
					</td>
				</tr>
				<tr class="keyPad">
					<td class="keyPad" onclick="minusCnt(100)" style="font-size: 300%; color: blue;">
						▼
					</td>
					<td class="keyPad" onclick="minusCnt(10)" style="font-size: 300%; color: blue;">
						▼
					</td>
					<td class="keyPad" onclick="minusCnt(1)" style="font-size: 300%; color: blue;">
						▼
					</td>
				</tr>
			</table>
		</div>
	
</body>
</html>