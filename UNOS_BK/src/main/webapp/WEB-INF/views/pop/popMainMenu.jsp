<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<style type="text/css">
body{
	margin : 0;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';

	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//세션 체크
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		console.log("?? :: " + empCd)
		$("#userId").html(nm);
	}
	
	$(function(){
		sessionChk();
		setEl();
		
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})

	//시작메뉴 집어넣기
	function ready(){
		
	}
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})
		
		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
                
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50) +"px",
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		// 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		//밑에 ( 영어 ) CSS
		$(".menu span").css({
	        "font-size" : $("#logo").height() * 1.1 + "px",
	        "margin-top" : getElSize(40) + "px"
	    })

	   /*  // 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.28 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    }) */
	    
	    // 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : getElSize(650) + "px",
	        "width" : getElSize(1100) + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : getElSize(50) + "px",
//	        "margin-top" : getElSize(550) + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })

	    /* .off().on("click", selectMenu) */
	    

	    // 근태관리,작업관리,이력조회 아이콘 CSS
	    $(".menu_icon").css({
	        "width" : getElSize(200) + "px",
	    });

		//근태관리 배경색
	    $("#gt").css({
	        "background-color" : "rgb(80, 140, 245)"
	    })

	    //작업관리 배경색
	    $("#operation").css({
	        "background-color" : "rgb(230, 111, 45)"
	    })

	    //이력조회 배경색
	    $("#history").css({
	        "background-color" : "rgb(150, 200, 95)"
	    })

	    //품질관리 배경색
	    $("#quality").css({
	        "background-color" : "rgb(170, 120, 220)"
	    })
	    
	    //관리자 배경색
	    $("#admin").css({
	        "background-color" : "rgb(255, 0, 0)"
	    })

	    

	}
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId">asd</div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	
	<div id="content">
		<table style="width: 100%; height: 100%;">
            <Tr>
                <td style="text-align: center">
                    <div class="menu" id="gt" style="opacity: 0" onclick="location.href='${ctxPath}/pop/popAttendanceMenu.do'">
                        1. 근태 관리<br>

                        <img src="${ctxPath}/images/gt.png" class="menu_icon"><br>
                        <span>(Time Attendance)</span><br>
                    </div>
                    <div class="menu" id="operation" style="opacity: 0" onclick="location.href='${ctxPath}/pop/popWorking.do'">
                        2. 작업 관리<br>
                            <img src="${ctxPath}/images/clock.png" class="menu_icon"><br>
                            <span>(Operation)</span><br>
                    </div>
                    <div class="menu" id="history" style="opacity: 0" onclick="location.href='${ctxPath}/pop/popHistory.do'">
                        3. 이력 조회<Br>
                            <img src="${ctxPath}/images/history.png" class="menu_icon"><br>
                            <span>(History)</span><br>
                    </div>
                    
                    <div class="menu" id="quality" style="opacity: 0" onclick="location.href='${ctxPath}/pop/popQuality.do'">
                    	4. 품질 관리<br>
                            <img src="${ctxPath}/images/quality.png" class="menu_icon"><br>
                            <span>(Quality)</span><br>
                    </div>
<!--                     관리자일때 생성 -->
                    <c:if test="${lv eq '2' }">
                    	
					<div class="menu" id="admin" style="opacity: 0" onclick="location.href='${ctxPath}/pop/adminSetting.do'">
                        4. 관리자 메뉴<Br>
	                    <img src="${ctxPath}/images/tool_black.png" class="menu_icon"><br>
	                    <span>(Admin Settings)</span><br>
                    </div>

                    </c:if>
                    <div>
                    
                    </div>
                </td>
            </Tr>
        </table>
	</div>
        <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>
</body>
</html>