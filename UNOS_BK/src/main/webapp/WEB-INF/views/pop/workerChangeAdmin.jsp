<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>


<style type="text/css">
body{
	margin : 0;
 	overflow: hidden;
}

.k-grouping-row td{
	height: 4 !important;
	font-size: 0.3 !important;
	line-height: 0;
	background: lavender;
}

td.k-group-cell{
	background: lavender;
}

.copyData{
	background: lightskyblue;
}

/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';

	var kendotable;
	var kendoRow;
	
	var currentStart;
	var currentEnd;
	
	var currentStartN;
	var currentEndN;
	
	var compareStartTimeN;
	var compareEndTimeN;
	
	var selectlist=[];
	
	$(function(){
		setEl();
		sessionChk();
		
		//작업자 리스트 가져오기
		getWorkerList();
		
		//grid 그리기
		gridTable();
		
		//주변 어둡게 하기위해
		createCorver();
		
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
		
	})

	//처음 시작 $(function(){})		$(document).ready(function(){})
	function ready(){
		getTable();
	}
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	
	
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//타이틀 클릭시 경로이동
	function pageMove(){
		location.href='${ctxPath}/pop/popMainMenu.do?empCd='+empCd
	}
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
	        "background-color" : "rgb(255, 0, 0)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})
		
		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
	    //저장 취소 CSS
	    $(".btn").css({
	    	"margin-right" : originWidth * 0.006 + "px",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.014 + "px", 
	        "padding-right": originWidth * 0.014 + "px", 
	        "padding-top": originWidth * 0.012 + "px", 
	        "padding-bottom": originWidth * 0.012 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
	    $("#save").css({
	    	"background-color" : "green",
	    })

	    $("#cancle").css({
	    	"background-color" : "red",
	    	"margin-right" : originWidth * 0.02 + "px",
	    })
	    
	    $(".allowImg").css({
	    	"height" : originHeight * 0.055 + "px",
	    	"margin-right" : originWidth * 0.03,
	    	"padding" : originHeight * 0.015,
	    	"cursor" : "pointer"
	    })
	    
	    $("#scrollDown").css({
	    	"background" : "blue"
	    })

	    $("#scrollUp").css({
	    	"background" : "red"
	    })
	    
	    $(".btn").hover(function(){
	    	color = $(this).css("background");
	    	
	        $(this).css({
	            "background-color" : "black"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : color
	        })
	    });
		 
	    var scroll_css = document.createElement('style')
		scroll_css.innerHTML = 
			"::-webkit-scrollbar {" +
				"background: white;" +
				"width: " + getElSize(62 * 2) + "px;"+
			"}" +
			
			"::-webkit-scrollbar-track-piece {" +
		  		"background-color:rgba(0,0,0,0);" +
			"}" +

			"::-webkit-scrollbar-thumb {" +
		  		"height :" + getElSize(100 * 2) + ";" +
				"background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;" +
			"}" +
			
			"::-webkit-scrollbar-thumb:ACTIVE {" +
	  		"height :" + getElSize(100 * 2) + ";" +
				"background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;" +
			"}" +
			
			"::-webkit-scrollbar-button:start:decrement {" +
				"display: block;" +
				"height: " +getElSize(62 * 2) +"px;" +
			    "background: url(${ctxPath}/images/FL/btn_scroll_up_pushed.svg) ;" +
				"background-size: 100% 100%" +
			"}" +
			
			"::-webkit-scrollbar-button:start:decrement:ACTIVE{" +
				"display: block;" +
				"height: " + getElSize(62 * 2) + "px;" +
				"background: url(${ctxPath}/images/FL/btn_scroll_up_default.svg) ;" +
			    "background-size: 100% 100%}" +
		    
		    "::-webkit-scrollbar-button:end:increment {" +
				"display: block;" +
				"height: " + getElSize(62 * 2) + "px;" +
		    	"background: url(${ctxPath}/images/FL/btn_scroll_down_pushed.svg) ;" +
		    	"background-size: 100% 100%" +
		 	"}" +
		 	
		 	"::-webkit-scrollbar-button:end:increment:ACTIVE {" +
				"display: block;" +
				"height: " +  getElSize(62 * 2) + "px;" +
		    	"background: url(${ctxPath}/images/FL/btn_scroll_down_default.svg) ;" +
		    	"background-size: 100% 100%" +
		 	"}" 
			
		console.log(scroll_css)
		document.body.appendChild(scroll_css)
			
	    
	}
	// 팝업창 그리드 수정
	function upGridCss(){
		$("#upGrid thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$("#upGrid tbody tr td").css({
			"font-size" : $("#logo").width() * 0.2 + "px",
			"text-align" : "center",
			"height" : $("#logo").height() * 2 + "px",
			"padding" : 0
		});
		
		$(".actionBtn").css({
			"border" : "1px solid black",
			"font-size" : $("#logo").width() * 0.15 + "px",
			"border-radius" : getElSize(15)+"px",
			"padding" : $("#logo").width() * 0.05,
			"padding-left" : $("#logo").width() * 0.12,
			"padding-right" : $("#logo").width() * 0.12,
			"cursor" : "pointer",
			"background" : "aliceblue"
		});

		$(".delBtn").css({
			"margin-left" : $("#logo").width() * 0.16,
		});
	}
	
	// 팝업창 오픈
	function openPopup(e){
		showCorver()
/* 		var selected_item = $("#" + selected_menu_id);
		
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    }) */
		var tr = $(e).closest("tr");
		var uid = tr[0].dataset.uid;
		
		var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
		rowItem = $("#grid").data("kendoGrid").dataItem(row)
		
		$("#cTitle span").html(rowItem.name)
		
		console.log("확인----")
		console.log(rowItem)
		
		//같은 장비 담을 리스트 변수
		var rowlist = []
		//같은 장비 데이터 찾아서 팝업창에 모두 띄어주기
		var list = kendotable.dataSource.data();
		$(list).each(function(idx,data){
			if(rowItem.dvcId==data.dvcId){
				var arr = {};
				arr.dvcId = data.dvcId
				arr.name = data.name
				arr.date = data.date
				arr.tgCyl = data.tgCyl
				arr.cntPerCyl = data.cntPerCyl
				arr.empCdD = data.empCdD
				arr.startTimeD = data.startTimeD
				arr.endTimeD = data.endTimeD
				arr.empCdN = data.empCdN
				arr.startTimeN = data.startTimeN
				arr.endTimeN = data.endTimeN
				arr.compareStartTimeN = data.compareStartTimeN
				arr.compareEndTimeN = data.compareEndTimeN
				arr.prdNo = data.name
				arr.copy = data.copy
				arr.group = data.group
				
				rowlist.push(arr)
			}
		})
		console.log(rowlist)
		var dataRowSource = new kendo.data.DataSource({
			/* data: [{
					"dvcId" : rowItem.dvcId
					,"name" : rowItem.name
					,"date" : rowItem.date
					,"empCdD" : rowItem.empCdD
					,"startTimeD" : rowItem.startTimeD
					,"endTimeD" : rowItem.endTimeD
					,"empCdN" : rowItem.empCdN
					,"startTimeN" : rowItem.startTimeN
					,"endTimeN" : rowItem.endTimeN
					,"compareStartTimeN" : rowItem.compareStartTimeN
					,"compareEndTimeN" : rowItem.compareEndTimeN
					,"prdNo" : rowItem.name
					,"copy" : rowItem.copy
					,"group" : rowItem.group
				}],  */
			data: rowlist,
			autoSync: true,
			schema: {
			    model: {
					id: "a",	 
					fields: {
					attributes:{style:"text-align:right;"},
						l: { editable: true,validation: { required: false },nullable:true },
					}
			    }
			},
			sort: [{
            	field: "startTimeD" , dir:"asc"
            },{
            	field:"compareStartTimeN",dir:"asc"
            }]
		});
		
		kendoRow.setDataSource(dataRowSource)
		
	    $("#popup").css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": "aliceblue",
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.08,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.83,
	        "z-index" : 10,
	    })
	}
	
	//스크롤 업다운 기능
	function scrollF(allow){
		//플러스 선택했을경우
		if(allow=="plus"){
			var scroll = $("#grid div.k-grid-content").scrollTop();
			$("#grid div.k-grid-content").scrollTop(scroll - 150);
		}else if(allow=="minus"){
			var scroll = $("#grid div.k-grid-content").scrollTop();
			$("#grid div.k-grid-content").scrollTop(scroll + 150);
		}
	}
	
	//시간 포맷팅 함수..
	function fommatTime(time){
		var hour = time.substring(0,2);
		var min = time.substring(3,5);
		return moment().set({'hour' : hour, 'minute' : min}).format("HH:mm");
	}
	//분할 버튼 클릭시
	function insertRow(e){
		$('.actionBtn').attr('disabled', true);
		$.showLoading()

		gridSort();
		
		setTimeout(function() {
			
			kendoRow;
			
			var row = $(e).closest("tr");
			var rowIdx = $("tr", grid.tbody).index(row);
			
			console.log("---insert---")
			var dataItem = $(e).closest("tr")[0].dataset.uid;
			var initData = kendoRow.dataItem(row);
			var length = kendoRow._data.length;
			var loopChk=0; // 1이면 빠져나감
			var maxGroup;	// 분할 수량 구하기
			
			var list=[];
			for(i=0; i<length; i++){
				if(kendoRow._data[i].name==initData.name ){
					if((kendoRow._data[i].startTimeD==kendoRow._data[i].endTimeD ) && kendoRow._data[i].startTimeN==kendoRow._data[i].endTimeN){
						loopChk=1;
						alert("작업시간을 선택해 주세요");
						$.hideLoading()
						$('.actionBtn').attr('disabled', false);
						return;
					}
					if(kendoRow._data[i].prdNo!=initData.prdNo){
						list.push(kendoRow._data[i].prdNo)
					}
				}
			}
			
			//작업시간 변경없이 한번더 추가할경우
			if(loopChk==1){
				console.log("return;");
				$.hideLoading()
				$('.actionBtn').attr('disabled', false);
				return false;
			}
			
			kendoRow.dataSource.insert({
				 routing: initData.routing,
				 dvcId: initData.dvcId,
				 name: initData.name,
				 date: initData.date,
				 empCdD: "",
				 empCdN: "",
				 tgCyl: initData.tgCyl,
				 cntPerCyl: initData.cntPerCyl,
	            /*  capa: initData.capa,
	             empD: initData.empD,
	             empN: initData.empN,
	             prdNo: initData.prdNo,
	             uph: initData.uph, */
	             startTimeD: moment().set({'hour' : 20, 'minute' : 00}).format("HH:mm"),
	             endTimeD: moment().set({'hour' : 20, 'minute' : 00}).format("HH:mm"),
	             startTimeN:  moment().set({'hour' : 08, 'minute' : 00}).format("HH:mm"),
	             endTimeN: moment().set({'hour' : 08, 'minute' : 00}).format("HH:mm"),
	             compareStartTimeN: "38:00",
	             compareEndTimeN: "38:00",
	             copy:"true",
	             group: kendoRow.dataSource.data().length-1
			});

			gridSort();
			
			$('.actionBtn').attr('disabled', false);
	
			$.hideLoading()
		}, 10);
		
	}
	
	//시간순 정렬하기
	function gridSort(){
		
		
		kendoRow.dataSource.data().sort(function(a, b) { // 오름차순
		    //return a.startTimeD < b.startTimeD ? -1 : a.startTimeD > b.startTimeD ? 1 : 0;
		    if(a.startTimeD < b.startTimeD)
		    	return -1;
		    if(a.startTimeD > b.startTimeD)
		    	return 1;
		    if(a.compareStartTimeN < b.compareStartTimeN)
		    	return -1;
		    if(a.compareStartTimeN < a.compareStartTimeN)
		    	return 1;
		});
		
		var list = kendoRow.dataSource.data()
		
		/* $(list).each(function(idx,data){
			if(data.startTimeN >="30:00"){
				hour = data.startTimeN.substring(0,1);
				hour -= 3;
				
				hour = hour + data.startTimeN.substring(1,5);
				console.log(hour);
				data.startTimeN = hour;
				
				list[idx-1].endTimeN = hour;
			}
		}) */
		
	}
	
	var as;
	function gridTable(){
		//메인 그리드
		kendotable = $("#grid").kendoGrid({
			editable:false,
			dataBound :function(e){
				// 데이터가 있을때 바인딩 ()
				if(kendotable!=undefined){
					var list = kendotable.dataSource.data();
					
					as = list[0]
					$(list).each(function(idx,data){
						if(data.copy==true || data.copy=="true"){
							var row = kendotable.tbody.find("tr[data-uid='" + data.uid + "']");
							row[0].setAttribute("class","copyData")
						}
					})
				}
				
				//group css 버튼 삭제
				$(".k-icon.k-i-collapse").remove();
				
				$("#grid thead tr th").css({
					"font-size" : $("#logo").width() * 0.16 + "px",
					"text-align" : "center",
				    "vertical-align" : "middle"
				});
				
				$("#grid tbody tr td").css({
					"font-size" : $("#logo").width() * 0.2 + "px",
					"text-align" : "center",
					"height" : $("#logo").height() * 2 + "px"
					
				});
				
				$(".actionBtn").css({
					"border" : "1px solid black",
					"font-size" : $("#logo").width() * 0.2 + "px",
					"border-radius" : getElSize(15)+"px",
					"padding" : $("#logo").width() * 0.05,
					"padding-left" : $("#logo").width() * 0.15,
					"padding-right" : $("#logo").width() * 0.15,
					"cursor" : "pointer",
					"background" : "aliceblue"
				});
			},
	      	columns:[
				{
					field:"name",title:"장비",width: originWidth * 0.1
					,groupHeaderTemplate: '　'
				},{
					title : "주간"
					,columns:[{
						
							field:"empCdD",title:"작업자",width:	originWidth * 0.058
							,editor : comboBox
							,template:"#=worKerName(empCdD)#"
						},{
							field:"startTimeD",title:"시작",width: originWidth * 0.040
							,format:"{0:HH:mm}"
						},{
							field:"endTimeD",title:"종료",width: originWidth * 0.045
							,format:"{0:HH:mm}"
						}
					]
				},{
					title : "야간"
					,columns:[{
						field:"empCdN",title:"작업자",width:	originWidth * 0.058
						,editor : comboBox
						,template:"#=worKerName(empCdN)#"
					},{
						field:"startTimeN",title:"시작",width: originWidth * 0.040
					},{
						field:"endTimeN",title:"종료",width: originWidth * 0.045
					}]
				},{
					template : "<button class='actionBtn' onclick='openPopup(this)'> 변경 </button>"
//					template : "<button class='actionBtn' onclick='insertRow(this)'> 분할 </button> <button class='actionBtn' onclick='deleteRow(this)'> 삭제 </button>"
					,width: originWidth * 0.060
				}
			]
		}).data("kendoGrid")

		//팝업창 그리드
		kendoRow = $("#upGrid").kendoGrid({
			editable:true,
			dataBound :function(e){
				// 데이터가 있을때 바인딩 ()
				if(kendoRow!=undefined){
					var list = kendoRow.dataSource.data();
					
					as = list[0]
					$(list).each(function(idx,data){
						if(data.copy==true || data.copy=="true"){
							var row = kendoRow.tbody.find("tr[data-uid='" + data.uid + "']");
							row[0].setAttribute("class","copyData")
						}
					})
				}
				
				// 팝업창 그리드 CSS
				upGridCss();
			},
			cellClose:function(e){
				
				console.log(e)
			},
	      	columns:[
				{
					title : "주간"
					,columns:[{
						
							field:"empCdD",title:"작업자",width:	originWidth * 0.055
							,editor : comboBox
							,template:"#=worKerName(empCdD)#"
						},{
							template: "<span class='k-icon k-i-close-outline k-i-x-outline k-i-error' style='color:red;'></span>"
							,width:	originWidth * 0.010
						},{
							field:"startTimeD",title:"시작",width: originWidth * 0.04
							,format:"{0:HH:mm}"
							,editor : TimeDSelect
						},{
							field:"endTimeD",title:"종료",width: originWidth * 0.04
							,format:"{0:HH:mm}"
							,editor : TimeDSelect
						}
					]
				},{
					title : "야간"
					,columns:[{
						field:"empCdN",title:"작업자",width:	originWidth * 0.055
						,editor : comboBox
						,template:"#=worKerName(empCdN)#"
					},{
						template: "<span class='k-icon k-i-close-outline k-i-x-outline k-i-error' style='color:red;'></span>"
						,width:	originWidth * 0.010
					},{
						field:"startTimeN",title:"시작",width: originWidth * 0.04
						,format:"{0:HH:mm}"
						,editor : TimeNSelect
					},{
						field:"endTimeN",title:"종료",width: originWidth * 0.04
						,format:"{0:HH:mm}"
						,editor : TimeNSelect
					}]
				},{
//					template : "<button class='actionBtn' onclick='openPopup(this)'> 변경 </button>"
					template : "<button class='actionBtn' onclick='insertRow(this)'> 분할 </button> <button class='actionBtn delBtn' onclick='deleteRow(this)'> 삭제 </button>"
					,width: originWidth * 0.085
				}
			]
		}).data("kendoGrid")
		
	    $(kendoRow.tbody).on("click", "td", function (e) {
	    	
	    	console.log(e)
	    	console.log($(this))
    		/* if(typeof($(this).children()[0])!="undefined"){
    			return
    		}; */
    		
    		var row = $(this).closest("tr");
            var curRowIdx = $("tr", kendoRow.tbody).index(row);
            var colIdx = $("td", row).index(this);
            var item = kendoRow.dataItem(row);
            
            //  $("#workerList option[id=245]").attr("selected", true);
            
    		//첫번째 행 1번선택했을경우 주간작업자 리셋
    		if($(this)[0].cellIndex==1){
    			item.empCdD="";
    			kendoRow.dataSource.fetch()
				return;
    		}
    		//첫번째 행 5번선택했을경우 야간작업자 리셋
    		if($(this)[0].cellIndex==5){
    			item.empCdN="";
    			kendoRow.dataSource.fetch()
				return;
    		}
    		
/*             var row = $(this).closest("tr");
            var curRowIdx = $("tr", kendoRow.tbody).index(row);
            var colIdx = $("td", row).index(this);
            var item = kendoRow.dataItem(row);
            
            console.log(row)
            console.log(curRowIdx)
            console.log(colIdx)
            console.log(item) */
            //showEditForm(item.a);
            
            
	    });
	    
	}

	//작업자 리스트 불러오기
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + 1;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var table = "<tbody>";
							
				//workerList 에도 추가
				var select ;
				$(json).each(function(idx, data){
					select += "<option id=" + data.id + ">" + decode(data.name) + "</option>";
					
					var arr=new Object();
					arr.text=decode(data.name);
					arr.value=decode(data.id);
					arr.textname=decode(data.name);
					selectlist.push(arr)
				});
				
				$("#workerList").append(select);
					console.log(selectlist)
			}
		});
	}
	
	// comboBox
	function comboBox(container, options, a){
		autoComplete = $('<input data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
        	dataSource: selectlist,
			autoWidth: true,
			dataTextField: "text",
			dataValueField: "value",
            placeholder: "작업자 선택",
            height : getElSize(1000),
            change:function(e){
            	upGridCss();
            }
        }).data("kendoComboBox");
	}

	//template name
	function worKerName(id){
		var chk=0;
		for(var i = 0; i < selectlist.length; i++){
			if(selectlist[i].value==id){
				chk++;
				return selectlist[i].textname;
			}
		}
		if(chk==0){
			return "미 배 치"
		}
	}
	//삭제 버튼 클릭시
	function deleteRow(e){
		
		if(confirm("삭제하시겠습니까?")==false){
			return false;
		}
		
		$('.actionBtn').attr('disabled', true);
		
		setTimeout(function() {
			var row = $(e).closest("tr");
			// 선택한 행의 인덱스값
			var rowIdx = $("tr", kendoRow.tbody).index(row);
			// 선택한 행의 배열 값
			var dataItem = $(e).closest("tr")[0].dataset.uid;
				dataItem = kendoRow.dataItem(row);
			// 체크??
			var check = 0;
			// 팝업 데이터 길이
			var length = kendoRow.dataSource.data().length	
			// 팝업 데이터 리스트
			var table= kendoRow.dataSource.data();

			//한개 일때 삭제버튼을 클릭시 막기 // 기존 데이터는 삭제 불가능
			console.log(length)
			console.log(dataItem.copy)
			if(length==1 || dataItem.copy==false || dataItem.copy=="false"){
				alert("처음 데이터는 지울수 없습니다. ")
				$('.actionBtn').attr('disabled', false);
				$.hideLoading()
				return;
			}
			
			// 삭제 시키기
			// 먼저 그리드 먼저 삭제해보기
			// 이전 데이터의 종료시간에 삭제할 데이터 종료시간 넣기
			if(table[rowIdx-1]!=undefined){
				table[rowIdx-1].endTimeD = dataItem.startTimeD;
				table[rowIdx-1].compareEndTimeD = dataItem.compareStartTimeD;
				table[rowIdx-1].endTimeN = dataItem.startTimeN;
				table[rowIdx-1].compareEndTimeN = dataItem.compareStartTimeN;
			}
			// 다음 데이터의 시작시간에 삭제할 데이터 종료시간 넣기
			if(table[rowIdx+1]!=undefined){
				table[rowIdx+1].startTimeD = dataItem.startTimeD;
				table[rowIdx+1].compareStartTimeD = dataItem.compareStartTimeD;
				table[rowIdx+1].startTimeN = dataItem.startTimeN;
				table[rowIdx+1].compareStartTimeN = dataItem.compareStartTimeN;
			}
			// 뒤에 데이터가 없을경우에는 종료시간에 마지막 시간 집어넣어주기 위함
			if(table[rowIdx+1]==undefined){
				table[rowIdx-1].endTimeD = dataItem.endTimeD;
				table[rowIdx-1].compareEndTimeD = dataItem.compareEndTimeD;
				table[rowIdx-1].endTimeN = dataItem.endTimeN;
				table[rowIdx-1].compareEndTimeN = dataItem.compareEndTimeN;
			}
			
			kendoRow.dataSource.remove(dataItem);
			
			/* var removelist=[];
			var a="";
			console.log(dataItem.endTimeD);
			console.log()
			
			
			for(var i=0;i<length;i++){			 
				if(kendoRow.dataSource.data()[i].dvcId==dataItem.dvcId && kendoRow.dataSource.data()[i].prdNo == dataItem.prdNo){
					 check++;
				}
				
				if(table[i].endTimeD==dataItem.startTimeD && table[i].name==dataItem.name && table[i].endTimeN==dataItem.startTimeN ){
//					console.log(table[i])
					table[i].set("endTimeD",dataItem.endTimeD)
					table[i].set("endTimeN",dataItem.endTimeN)
				}
				
				if(table[i].name==dataItem.name && table[i].prdNo!=dataItem.prdNo && table[i].startTimeD==dataItem.startTimeD && table[i].startTimeN==dataItem.startTimeN){
					removelist.push(i)
				}
			}
			
			if(check<=1 || dataItem.startTimeD=="08:00"){
				alert(" 1개 일때는 지울 수 없습니다. ")
				$('.actionBtn').attr('disabled', false);
				$.hideLoading()
				return;
			}else{
				console.log("--delete--")
				console.log(removelist)
				console.log(dataItem.dvcId)
				console.log(dataItem.group)
				
				//var url = "${ctxPath}/order/deleteJobSplit.do";
			
				var param = "dvcId=" + dataItem.dvcId + 
							"&tgDate=" + $("#date").val() +
							"&group=" + dataItem.group;
				
				$.ajax({
					url : url,
					data : param,
//					dataType : "json",
					type : "post",
					success : function(data){
						console.log("suc")
					}
				});
						
				
				
				
				
				if(removelist.length!=0){
					grid.dataSource.remove(table[removelist[0]])
				}
				grid.dataSource.remove(dataItem)
			} */
			$.hideLoading()
			$('.actionBtn').attr('disabled', false);
		}, 10);
		$.showLoading()
	}
	
	
	//주간 작업시작 변경시
	function TimeDSelect(container, options){
		
 		currentStart=options.model.startTimeD;
		currentEnd=options.model.endTimeD; 
	//	startTime="08:00"
	//	endTime="20:00"
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"08:00",
	    	    max:"20:00",
	    	    change:function(e){
	    	    	//시간 타입을 format 변경하여 저장하기.
	    	    	if(options.field=="startTimeD"){
		    	    	var hour = options.model.startTimeD.getHours();
	           			var minute = options.model.startTimeD.getMinutes();
	    	    	}else if(options.field=="endTimeD"){
	    	    		var hour = options.model.endTimeD.getHours();
	           			var minute = options.model.endTimeD.getMinutes();
	    	    	}
           			
	    	    	options.model.startTimeD = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
	    	    	
	    	    	var list = kendoRow.dataSource.data();
	    	    	
	    			var row = kendoRow.tbody.find("tr[data-uid='" + options.model.uid + "']");
	    			var idx = kendoRow.dataSource.indexOf(kendoRow.dataItem(row));
	    			var maxLength = kendoRow.dataSource.data().length;
	    			
	    			//작업 종료시간은 변경 못하게 막기
					if(options.field=="endTimeD" && maxLength!=1){
						alert("종료시간을 변경할수 없습니다. ( 작업시작 시간을 변경해주세요 )")
						
	    				options.model.startTimeD = currentStart
	    				options.model.endTimeD = currentEnd
	    				
	    				//grid 재정렬
		    	    	gridSort();
						
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
					}

	    			// 데이터가 한개일 경우에는 수정 불가능하게 하기
	    			if(maxLength==1){
	    				alert("시작,종료 시간을 변경할수 없습니다. 작업자를 먼저 추가해주세요.");
	    				
	    				options.model.startTimeD = currentStart
	    				options.model.endTimeD = currentEnd
	    				
	    				//grid 재정렬
		    	    	gridSort();
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
	    			}else{	// 종료시간을 시작시간 일치시키기
	    				//젤 위쪽의 데이터먼저 수정시키기 위해서
	    				if(list[idx-1].endTimeD == currentStart && list[idx-1].startTimeD == currentStart){
	    					alert("이전 작업을 먼저 변경해주세요");
	    					options.model.startTimeD = currentStart
		    				options.model.endTimeD = currentEnd
		    				
		    				//grid 재정렬
			    	    	gridSort();
			    	    	// grid CSS
			    	    	upGridCss();
			    	    	kendoRow.dataSource.fetch()

		    				return;
	    				}else{	// 최종 변경시키기
	    					
	    					console.log("===최종 선택===")
	    					console.log(options.model)
	    					
	    					// 시작시간 변경시 그전작업자의 시작시간보다 빠르지 못하게 막기
	    					if(options.model.startTimeD <= list[idx-1].startTimeD){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						options.model.startTimeD = currentStart
			    				options.model.endTimeD = currentEnd
			    				
			    				//grid 재정렬
				    	    	gridSort();
				    	    	// grid CSS
				    	    	upGridCss();
				    	    	kendoRow.dataSource.fetch()

			    				return;
	    					}
		    				list[idx-1].endTimeD = options.model.startTimeD
	    				}
	    			}
	    	    	
	    	    	kendoRow.dataSource.fetch()
	    	    	
	    	    	console.log(options)
	    	    	console.log(container)
	    	    	
	    	    	//grid 재정렬
	    	    	gridSort();
	    	    	
	    	    	// grid CSS
	    	    	upGridCss(); 
	    	    }
	   	});
	}
	
	//야간 작업시작 변경시
	function TimeNSelect(container, options){
		currentStartN=options.model.startTimeN;
		currentEndN=options.model.endTimeN;
		
		compareStartTimeN = options.model.compareStartTimeN;
		compareEndTimeN = options.model.compareEndTimeN;
		
		$('<input data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
	    .appendTo(container)
	    .kendoTimePicker({
		    	dateInput: true,
	    	    format: "HH:mm",
	    	    min:"20:00",
	    	    max:"08:00",
	    	    change:function(e){
	    	    	//시간 타입을 format 변경하여 저장하기.
	    	    	if(options.field=="startTimeN"){
		    	    	var hour = options.model.startTimeN.getHours();
	           			var minute = options.model.startTimeN.getMinutes();
	    	    	}else if(options.field=="endTimeN"){
	    	    		var hour = options.model.endTimeN.getHours();
	           			var minute = options.model.endTimeN.getMinutes();
	    	    	}
	    	    	
					options.model.startTimeN = moment().set({'hour' : hour, 'minute' : minute}).format("HH:mm");
	    	    	
	    	    	var list = kendoRow.dataSource.data();
	    	    	
	    			var row = kendoRow.tbody.find("tr[data-uid='" + options.model.uid + "']");
	    			var idx = kendoRow.dataSource.indexOf(kendoRow.dataItem(row));
	    			var maxLength = kendoRow.dataSource.data().length;
	    			
	    			//작업 종료시간은 변경 못하게 막기
					if(options.field=="endTimeN" && maxLength!=1){
						alert("종료시간을 변경할수 없습니다. ( 작업시작 시간을 변경해주세요 )")

	    				options.model.startTimeN = currentStartN
	    				options.model.endTimeN = currentEndN
	    				
	    				//grid 재정렬
		    	    	//gridSort();
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
					}
					
					// 데이터가 한개일 경우에는 수정 불가능하게 하기
	    			if(maxLength==1){
	    				alert("시작,종료 시간을 변경할수 없습니다. 작업자를 먼저 추가해주세요." + currentStartN);
	    				
//	    				options.model.compareStartTimeN = currentStartN
//	    				options.model.compareEndTimeN = currentEndN
	    				options.model.startTimeN = currentStartN
	    				options.model.endTimeN = currentEndN
	    				
	    				//grid 재정렬
		    	    	//gridSort();
		    	    	// grid CSS
		    	    	upGridCss();
		    	    	kendoRow.dataSource.fetch()

	    				return;
	    			}else{	// 종료시간을 시작시간 일치시키기
	    				//젤 위쪽의 데이터먼저 수정시키기 위해서
	    				if(list[idx-1].endTimeN == currentStartN && list[idx-1].startTimeN == currentStartN){
	    					alert("이전 작업을 먼저 변경해주세요");
	    					options.model.startTimeN = currentStartN
		    				options.model.endTimeN = currentEndN
		    				
		    				//grid 재정렬
			    	    	//gridSort();
			    	    	// grid CSS
			    	    	upGridCss();
			    	    	kendoRow.dataSource.fetch()

		    				return;
	    				}else{	// 최종 변경시키기
	    					//시간 계산을 위해 시간 +30 변환	 00:00 ~ 08:00 일 경우
	    					
	    					/* if(options.model.startTimeN >= "00:00" && options.model.startTimeN <="08:00"){
	    						hour = options.model.startTimeN.substring(0,2);
								time = options.model.startTimeN.substring(3,5);
								
								hour = Number(hour) + 30;
//								time = Number(time);
	    						options.model.startTimeN = hour + ":" + time
	    					} */
	    					
							if(options.model.startTimeN >= "00:00" && options.model.startTimeN <="08:00"){
	    						//최종 결졍 ( 비교 시작시간 변경)
	    						options.model.compareStartTimeN  = plusTh(options.model.startTimeN);
	    					}else{
	    						options.model.compareStartTimeN = options.model.startTimeN;
	    					}
	    					
	    					// 그전작업자의 시작시간보다 빠르거다 같을경우 제외처리
	    					console.log("==== 바교 ====")
	    					console.log("현재값 : " + options.model.compareStartTimeN + "<= , 이전값 : " + list[idx-1].compareStartTimeN);
	    					if(options.model.compareStartTimeN <= list[idx-1].compareStartTimeN){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						
	    						options.model.startTimeN = currentStartN
			    				options.model.endTimeN = currentEndN
			    				options.model.compareEndTimeN = compareEndTimeN;
			    				options.model.compareStartTimeN = compareStartTimeN;
			    				//grid 재정렬
				    	    	//gridSort();
				    	    	// grid CSS
				    	    	upGridCss();
				    	    	kendoRow.dataSource.fetch()

			    				return;
	    					}
	    					
	    					
	    					
//	    					options.model.compareStartTimeN = options.model.startTimeN
	    					
	    					//최종 결정 ( 실제 종료시간 변경 )
	    					list[idx-1].endTimeN = options.model.startTimeN	
	    					//최종 결정 ( 비교 종료시간 변경)
	    					list[idx-1].compareEndTimeN = options.model.compareStartTimeN;
	    					
	    					/* // 시작시간 변경시 그전작업자의 시작시간보다 빠르지 못하게 막기
	    					if(options.model.startTimeD <= list[idx-1].startTimeD){
	    						alert("그전 작업자와 시작시간이 같거나 작을수 없습니다.");
	    						options.model.startTimeD = currentStart
			    				options.model.endTimeD = currentEnd
			    				
			    				//grid 재정렬
				    	    	gridSort();
				    	    	// grid CSS
				    	    	upGridCss();
				    	    	kendoRow.dataSource.fetch()

			    				return;
	    					}
		    				list[idx-1].endTimeD = options.model.startTimeD */
	    				}
	    			}
					
	    			kendoRow.dataSource.fetch()
	    	    	
	    	    	console.log(options)
	    	    	console.log(container)
	    	    	
	    	    	//grid 재정렬
	    	    	gridSort();
	    	    	
	    	    	// grid CSS
	    	    	upGridCss();
	    	    	
	    	    }
	   	});
	
	}
	
	//저장한값 저장하기 위한 함수
	function saveRow(){
		
		var kendolist = kendoRow.dataSource.data();
		var savelist = [];
		$(kendolist).each(function(idx,data){
			// 주간 데이터 넣기
			var arr={};
			arr.dvcId = data.dvcId;
			arr.name = data.name;
			arr.date = data.date;
			arr.empCd = data.empCdD;
			arr.tgCyl = data.tgCyl;
			arr.cntPerCyl = data.cntPerCyl;
			arr.startTime = data.startTimeD;
			arr.endTime = data.endTimeD;
			arr.workIdx = 2;
			arr.group = data.group;
			arr.copy = data.copy;
			
			savelist.push(arr);
			
			var arr={};
			arr.dvcId = data.dvcId;
			arr.name = data.name;
			arr.date = data.date;
			arr.empCd = data.empCdN;
			arr.tgCyl = data.tgCyl;
			arr.cntPerCyl = data.cntPerCyl;
			arr.startTime = data.startTimeN;
			arr.endTime = data.endTimeN;
			arr.workIdx = 1;
			arr.group = data.group;
			arr.copy = data.copy;
			
			savelist.push(arr);
			
			
			/* {
// 				"dvcId":"32"
// 				,"name":"OS/R M#1"
// 				,"date":"2019-04-26"
// 				,"empCdD":"157"
// 				,"startTimeD":"08:00"
// 				,"endTimeD":"20:00"
				,"empCdN":"239"
				,"startTimeN":"20:00"
				,"endTimeN":"08:00"
				,"compareStartTimeN":"20:00"
				,"compareEndTimeN":"08:00"
				,"prdNo":"OS/R M#1"
				,"copy":"false"
			} */
			
		})
		
		var obj = new Object();
		obj.val = savelist;
		
		var url = "${ctxPath}/pop/getPopChangeWorker.do";
		var param = "val=" + JSON.stringify(obj);

		$.showLoading()
		console.log(param)
 		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function (data){
				if(data=="success"){
					$("#popup").css({
				        "transition" : "0.5s",
				        "opacity" : 0,
				        "display" : "none",
				        "z-index" : 1,
				    })
				    hideCorver();
					getTable();
					alert("저장 완료되었습니다");
				}else{
					alert("저장 실패하였습니다. ");
				}
				$.hideLoading()
			}
 		})
		
		console.log("saveRow")
	}
	
	//취소 버튼 클릭시 작업자변경화면으로 이동하기 위한 함수
	function cancelRow(){
		$("#popup").css({
//	        "transition" : "1s",
	        "opacity" : 0,
	        "display" : "none",
//	        "position": "absolute",
// 	        "background": "aliceblue",
// 	        "top": $("#content").height() * 0.16,
// 	        "left": $("#content").width() * 0.08,
// 	        "height": $("#content").height() * 0.80,
// 	        "width": $("#content").width() * 0.83,
	        "z-index" : 1,
	    })
	    
	    hideCorver();
		getTable();
		//location.href='${ctxPath}/pop/workerChangeAdmin.do';
	}
	
	// 시간 + 30 하여 비교하기 위해서 함수 생성
	function plusTh(time){
		//hour = "3" + options.model.startTimeN.substring(1,5);
		hour = time.substring(0,1);
		hour = Number(hour) + 3;
		time = hour + time.substring(1,5);
		
		return time;
	}
	
	//테이블 그리기
	function getTable(){
		var url = "${ctxPath}/pop/getPopDevice.do";
		var param = "popId=" + popId ;

		$.showLoading(); 
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList
				
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					if(data.startTimeN >="00:00" && data.startTimeN <="08:00"){
						data.compareStartTimeN = plusTh(data.startTimeN);
						data.compareEndTimeN = plusTh(data.endTimeN);
					}else{
						data.compareStartTimeN = data.startTimeN;
						data.compareEndTimeN = data.endTimeN;
					}
					
					
				})
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
	                autoSync: true,
	                schema: {
	                    model: {
	                      id: "a",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                         l: { editable: true,validation: { required: false },nullable:true },
	                      }
	                    }
	                },
	                group: {
                        field: "name",
                        dir: "asc",
                    },
					sort: [{
						field: "name" , dir:"asc" 
					},{
						field: "startTimeD" , dir:"asc"
					},{
						field:"compareStartTimeN",dir:"asc"
					}]
	             });
				
				kendotable.setDataSource(dataSource)
				
				$.hideLoading(); 
			}
		})
	}
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="location.href='${ctxPath}/pop/adminSetting.do'">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				4-1. 작업자 변경
			</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
		<div style="width: 100%; height: 10%; float: left"></div>
      	<div id="grid" style="width: 100%; height: 75%; float: left;"></div>
      	<div id="allowBtn" style="width: 100%; height: 15%;">
      		<table style="width: 100%; height: 100%; text-align: right;">
      			<tr>
      				<td>
     			      	<img id="scrollUp" class="allowImg" onclick="scrollF('plus')" src="${ctxPath}/css/images/icons-png/arrow-u-black.png"/>
    					<img id="scrollDown" class="allowImg" onclick="scrollF('minus')" src="${ctxPath}/css/images/icons-png/arrow-d-black.png"/>
      				</td>
      			</tr>
      		</table>
      	</div>
	</div>

	<div id="popup" style="display: none; opacity: 0;">
		<div id="cTitle" style="height: 15%; width: 100%; font-size: 350%; font-weight: bold; text-align: center;">
			<span>
			</span>
			<div style="float: right;">
				<button id="save" class="btn" onclick="saveRow()">저장</button>
				<button id="cancle" class="btn" onclick="cancelRow()">취소</button>
			</div>
		</div>
		<table style="height: 85%; width: 99%;">
			<div id="upGrid" style="width: 99.7%; height: 89.7%%;"></div>
		</table>
	</div>
	
</body>
</html>