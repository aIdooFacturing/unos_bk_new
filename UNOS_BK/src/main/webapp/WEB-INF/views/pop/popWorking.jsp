<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>

<style type="text/css">
body{
	margin : 0;
}
body{
	margin : 0;
}
.k-grid thead tr th{
	background : linear-gradient( to bottom, gray, black );
	color: white;
	text-align: center;
}
.k-grid thead tr.k-filter-row th{
	background : linear-gradient( to bottom, #E8D9FF, #A566FF );
	color: white;
	text-align: center;
}
.selectRow {
	background: red !important;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	var startList;
	var tCons;
	
	//비가동 사유 리스트 담기 위한 변수
	var nonOpTy = [];
	
	$(function(){
		$.showLoading();
		
		setEl();
		
		
		//비가동 항목 선택이유 가지고 오기
		selectMenu();
		//grid 그리기
		gridTable();
		
		//주변 어둡게 하기위해
		createCorver();
		
		sessionChk();
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setTimeout(()=>{
			$.hideLoading();
		}, 500)
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
	//시작메뉴 집어넣기
	function ready(){
		
	}
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//타이틀 클릭시 경로이동
	function pageMove(){
		location.href='${ctxPath}/pop/popMainMenu.do?empCd='+empCd
	}
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
	        "background-color" : "rgb(230, 111, 45)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    //출근보고 클릭시 나오는 화면
	    $(".popCss").css({
	    	"text-align" : "center",
			"font-size" : $("#logo").width() * 0.18 + "px",
		    "border": $("#logo").width() * 0.03 + "px solid white",
		    "transition": "all 1s ease 0s",
		    "border-radius": $("#logo").width() * 0.1 + "px",
	    })
	    
	    $(".result").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px"
	    });
		
	    //팝업창에서 내용 리스트
	    $(".popContent").css({
	    	"height" : originHeight * 0.55 + "px",
	    	"margin-top": originHeight * 0.04 + "px",
	    	"overflow" : "auto"
	    });
	    
	    //위쪽 화살표
	    $(".allowImg").css({
	    	"float": "right",
	    	"height" : originHeight * 0.045 + "px",
	    	"margin-right" : originWidth * 0.015,
	    	"padding" : originHeight * 0.012,
	    	"background" : "gray",
	    	"cursor" : "pointer"
	    })
	    
	    //확인 취소 버튼
	    $(".btn").css({
	    	/* "top" : $("#content").height() * 0.96,
	    	"left" : getElSize(30),
	    	"position" : "absolute" , */
	    	"margin-top" : originHeight * 0.03 + "px",
	        "background-color": "lightgray",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
	    $(".btn").css({
	    	/* "top" : $("#content").height() * 0.96,
	    	"left" : getElSize(30),
	    	"position" : "absolute" , */
	    	"margin-top" : originHeight * 0.03 + "px",
	        "background-color": "lightgray",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
  	    $(".btn2").css({
	    	/* "top" : $("#content").height() * 0.96,
	    	"left" : getElSize(30),
	    	"position" : "absolute" , */
	    	"margin-top" : originHeight * 0.03 + "px",
	        "background-color": "lightgray",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
	    $(".btn").hover(function(){
	        $(this).css({
	            "background-color" : "red"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : "lightgray"
	        })
	    });

	    $(".btn2").hover(function(){
	        $(this).css({
	            "background-color" : "red"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : "lightgray"
	        })
	    });
	    
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
	    $(".menu .mainSpan").css({
	    	"display" : "inline-block",
	    	"margin-top" : originHeight * 0.075
	    })
	    
	    
	    var scroll_css = document.createElement('style')
		scroll_css.innerHTML = 
			"::-webkit-scrollbar {" +
				"background: white;" +
				"width: " + getElSize(62 * 2) + "px;"+
			"}" +
			
			"::-webkit-scrollbar-track-piece {" +
		  		"background-color:rgba(0,0,0,0);" +
			"}" +

			"::-webkit-scrollbar-thumb {" +
		  		"height :" + getElSize(100 * 2) + ";" +
				"background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;" +
			"}" +
			
			"::-webkit-scrollbar-thumb:ACTIVE {" +
	  		"height :" + getElSize(100 * 2) + ";" +
				"background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;" +
			"}" +
			
			"::-webkit-scrollbar-button:start:decrement {" +
				"display: block;" +
				"height: " +getElSize(62 * 2) +"px;" +
			    "background: url(${ctxPath}/images/FL/btn_scroll_up_pushed.svg) ;" +
				"background-size: 100% 100%" +
			"}" +
			
			"::-webkit-scrollbar-button:start:decrement:ACTIVE{" +
				"display: block;" +
				"height: " + getElSize(62 * 2) + "px;" +
				"background: url(${ctxPath}/images/FL/btn_scroll_up_default.svg) ;" +
			    "background-size: 100% 100%}" +
		    
		    "::-webkit-scrollbar-button:end:increment {" +
				"display: block;" +
				"height: " + getElSize(62 * 2) + "px;" +
		    	"background: url(${ctxPath}/images/FL/btn_scroll_down_pushed.svg) ;" +
		    	"background-size: 100% 100%" +
		 	"}" +
		 	
		 	"::-webkit-scrollbar-button:end:increment:ACTIVE {" +
				"display: block;" +
				"height: " +  getElSize(62 * 2) + "px;" +
		    	"background: url(${ctxPath}/images/FL/btn_scroll_down_default.svg) ;" +
		    	"background-size: 100% 100%" +
		 	"}" 
	 	
	 	document.body.appendChild(scroll_css)
	}
	
	// 팝업창 생성 팝업 CSS #popup_submenu #showGoWork #showOffWork #showEarlyWork #showOutWork
	function popup_submenu(selected_menu_id,div_id){
		
		console.log(div_id);
		
		//작업 시작 클릭시
		if(div_id=="showGoWork"){
			goWork();
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showOffWork"){// 작업 종료 클릭시
			offWork();
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showNonOperation"){// 비가동 보고 클릭시
			getNonOp();
			openPopup(selected_menu_id,div_id);
		}
	}
	
	//팝업창 띄우기
	function openPopup(selected_menu_id,div_id){
		showCorver()
//		alert("1. :" + selected_menu_id + ", 2. :" + div_id)
		var selected_item = $("#" + selected_menu_id);
		/* var backColor;
		//1번 박스 선택했을경우 배경색
		if(selected_menu_id=="box1"){
			backColor = "rgb(214,240,255)"
		}else{
			backColor = $(selected_item).css("background")
		} */
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })

	    $("#" + div_id).css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.08,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.83,
	        "z-index" : 10,
	    })
	}
	
	//비가동 사유 선택 select 메뉴 가지고 오기
	function selectMenu(){
		
		nonOpTyDataSource = new kendo.data.DataSource({});
		
		//이유 선택박스 가지고오기
		var url = ctxPath + "/chart/getNonOpTy.do"
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var options = ""
				$(json).each(function(idx, data){
					
					var obj = {
							"nonOpTy" : data.id,
							"nonOpName" : decode(data.nonOpTy)
					}
					
					nonOpTyDataSource.add(obj)
					nonOpTy.push(obj)
				});
			}	
		})
	}
	//모든 그리드 그리기
	function gridTable(){
		//퇴근 (작업보고) 그리드 그리기
		kendotable = $(".kendo").kendoGrid({
			editable:false,
			dataBound:function(){
				grid = this;

				grid.tbody.find('tr').each(function(){
					var item = grid.dataItem(this);
					kendo.bind(this,item);
				})
				
				gridCss();
				
				//숫자 입력을 위한 이벤트 생성
				$("input.cntInput").click(function(e){
					
					var tr = $(this).closest("tr");
					var uid = tr[0].dataset.uid;
					
					var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
					
					//기존 초기화
					if(tCons!=undefined){
						tCons.className = "";
					}
					
					tCons = tr[0];
					tCons.className = "selectRow";
		    		$("#one").val(0)
		    		$("#two").val(0)
		    		$("#three").val(0)
					inputCntData = grid.dataItem(row)
					// 클릭한 데이터 숫자 값
				    var nStr = inputCntData.f.toString();
					// 1,10,100,1000 의 자리 분리를 위해서 반목문
				    for(var i = 0; i < nStr.length; i++){
				    	// 1의 자리일 경우
				    	if(i==nStr.length-1){
				    		$("#one").val(Number(nStr[i]))
				    	}else if(i==nStr.length-2){
				    		$("#two").val(Number(nStr[i]))
				    	}else{
				    		$("#three").val(Number($("#three").val()+nStr[i]))
				    	}
// 				        answer.push(Number(nStr[nStr.length -1 -i]));
				    }
				    
				    
					console.log(inputCntData)
// 					console.log(answer)
					
					
					$("#keyPad").css({
						"background":"gray",
						"width": originWidth * 0.33,
						"height": originHeight * 0.25,
						"margin-top": $("#header").height() * 1.12,
						"z-index": 11,
						"display":""
					})
					$("#keyPad").css({
						"left" : (originWidth / 2) - ($("#keyPad").width() / 2),
					})
					
					/* $("#headTitle").css({
						"position":"absolute",
						"top":  $("#header").height()/2 - $("#header").height() * 0.36,
			//			"left": getElSize(2000),
						
						"display" : "inline-block",
				        "background-color" : "rgb(80, 140, 245)",
				        "border" : getElSize(7) + "px solid white",
				        "border-radius" : getElSize(30) + "px",
				        "padding-left" : getElSize(15),
				        "padding-right" : getElSize(15),
				        "cursor" : "pointer",
				        
						"font-size": $("#logo2").height() *  0.7
					}) */
			
					$("#headTitle").css({
						"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
					})
				})
				
				//입력창 나올때 이벤트 발쌩
				$('html').click(function(e) {
//					console.log(e.target)
					if(!($(e.target).hasClass("keyPad") || $(e.target).hasClass("cntInput")) ){
						if(tCons!=undefined)
						tCons.className = "";
						
						$("#keyPad").css({
							"display":"none",
						})
					}
				});
				
				//scroll 이동 시키기
				$("#scrollUp").click(function(){
					var scroll = $("div.k-grid-content").scrollTop();
					$("div.k-grid-content").scrollTop(scroll-100);
				})
				//scroll 이동 시키기
				$("#scrollDown").click(function(){
					var scroll = $("div.k-grid-content").scrollTop();
					$("div.k-grid-content").scrollTop(scroll+100);
				})
		    },
		    change: function(e) {
		        var selectedRows = this.select();
		        console.log(selectedRows)
	      	},
	      	edit: function(e) {
	      		console.log(e)
	      	    if (!e.model.isNew()) {
	      	      // Disable the editor of the "id" column when editing data items
	      	      var numeric = e.container.find("input[name=id]").data("kendoNumericTextBox");
	      	      numeric.enable(false);
	      	    }
	      	},
	      	columns:[
	      		{
					field:"checker",
					title:"<input type='checkbox' id='checkall' onclick='checkAll(this)'>"
					,width:originWidth * 0.035,
					template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" 
				},
				{
					field:"prdNo",title:"차종",width: originWidth * 0.09
				},
				{
					field:"b",title:"장비",width:	originWidth * 0.09
				},
				{
					field:"c",title:"계획",width: originWidth * 0.035
				},{
					title:"생산실적",
					columns:[{
						field:"d",title:"HMI",width: originWidth * 0.038
					},{
						field:"e",title:"aIdoo",width: originWidth * 0.038
					},{
						field:"f",
						title:"수량",
						width: originWidth * 0.045,
						template:'<input type="number" class="cntInput" min="0" data-role="number" style="width:100%; font-size:200%;"  data-bind="value:f" readOnly="readOnly"/>',
					}/* ,{
						field:"fault",
						title : "불량수량",
						width: originWidth * 0.045,
						template:'<input type="number" class="cntInput" min="0" data-role="number" style="width:100%; font-size:200%;"  data-bind="value:fault" readOnly="readOnly"/>',
					} */]
				},{
					field:"g",
					title:"달성률",
					width: originWidth * 0.042,
					template:"#=kendo.toString((f/c)*100, 'n0')#%"
				}/* ,{
					title:"LOT Number",attributes: {
        				style: "text-align: center; color:white; font-size:" + getElSize(45)
        			},headerAttributes: {
        				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
        			}							,
					columns:[{
						field:"h",
						title:"#1",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text'style='width:100%;' data-bind='value:h'/>"
					},{
						field:"i",
						title:"#2",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;'  data-bind='value:i'/>"
					},{
						field:"j",
						title:"#3",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:j'/>"
					},{
						field:"k",
						title:"#4",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:k'/>"
					},{
						field:"l",
						title:"#5",width:getElSize(130),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:l'/>"
					}
					]
				} */]
		}).data("kendoGrid")
		
		kendotableOp = $("#nonOpList").kendoGrid({
			editable : true
			,dataBound : function(e) {
				gridOp = this;
				gridCss()
			}
			,columns : [{
				field:"checkerOp",
				title:"<input type='checkbox' id='checkallOp' onclick='checkOpAll(this)'>"
				,width:originWidth * 0.055,
				template: "<input type='checkbox' onclick='checkRowOp(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox'/>" 
			},{
				title : "장비"
				,field : "oprNo"
			},{
				title : "비가동 유형"
				,field : "chartStatus"
			},{
				title : "시작"
				,field : "sDate"
			},{
				title : "종료"
				,field : "eDate"
			},{
				title : "비가동<br>시간(분)"
				,field : "waitTime"
			},{
    			field: "reason",
				title: "비가동사유",
				template : "#=changeNonOpty(reason)# ",
				editor: categoryDropDownEditorByNonOpty,
				editable : false,
    			attributes: {
        			style: "text-align: center; font-size: " + getElSize(35) + "px"
      			}
			}]
		}).data("kendoGrid")
	}
	
	function gridCss(){
		
		$(".kendo thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$(".kendo tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px"
		})
	
		$("#nonOpList thead tr th").css({
			"font-size" : $("#logo").width() * 0.16 + "px",
			"text-align" : "center",
		    "vertical-align" : "middle"
		});
		
		$("#nonOpList tbody tr td").css({
			"font-size" : $("#logo").width() * 0.13 + "px",
			"text-align" : "center"
		})
		// check box 사이즈
		$("#checkallOp").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})

		// check box 사이즈
		$("#checkall").css({
			"width" : originWidth * 0.035,
			"height" : originWidth * 0.035
		})
		
		// check box 사이즈
		$(".checkbox").css({
			"width" : originWidth * 0.030,
			"height" : originWidth * 0.030
		})
	}
	
	function categoryDropDownEditorByNonOpty(container, options) {
		$('<input name="' + options.field + '"/>')
           .appendTo(container)
           .kendoDropDownList({
           	valuePrimitive: true,
               dataTextField: "nonOpName",
               dataValueField: "nonOpTy",
               optionLabel: "==선택==",
               dataSource: nonOpTyDataSource,
               change:function(e){
            	   gridCss();
               }
         });
    }
	
	function changeNonOpty(num){
		var length = nonOpTy.length;
		for(var i=0;i<length;i++){
			if(nonOpTy[i].nonOpTy==num){
				return nonOpTy[i].nonOpName
			}
		}
		return "사유를 선택해주세요"
	}
	
	//전체 선택기능
	function checkAll(e){
		console.log($("#checkall").is(":checked"))
		if($("#checkall").is(":checked")){
			gridlist=kendotable.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=kendotable.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		kendotable.dataSource.fetch();
	}
	
	//비가동 전체 선택기능
	function checkOpAll(e){
		console.log($("#checkallOp").is(":checked"))
		if($("#checkallOp").is(":checked")){
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = true;
        	})
		}else{
			gridlist=kendotableOp.dataSource.data();
        	$(gridlist).each(function(idx,data){
        		data.checkSelect = false;
        	})
		}
		kendotableOp.dataSource.fetch();
	}
	
	function checkRow(e){
		console.log("event click")
		var gridList=kendotable.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#woringList").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = grid.dataItem(row)
		 
		 if(initData.checkSelect){
			 initData.checkSelect = false;
		 }else{
			 initData.checkSelect = true;
		 }
		 kendotable.dataSource.fetch();
	}
	
	//체크박스 tbody 선택시 (비가동)
	function checkRowOp(e){
		console.log("event click")
		var gridList=kendotableOp.dataSource.data();
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		
		console.log(dataItem)
		
		 var row = $("#nonOpList").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = gridOp.dataItem(row)
		 console.log(initData)
		 
		 if(initData.checkSelect){
			 initData.checkSelect = false;
		 }else{
			 initData.checkSelect = true;
		 }
		 kendotableOp.dataSource.fetch();
	}
	
	//출근 보고 클릭시 뜨는 화면?
	function goWork(){
		$.showLoading();
		var url = "${ctxPath}/popApi/getListPrdTg.do";
		
		var param = "empCd=" + empCd;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data;
				startList = json
				var table = "<table border='3' id='tableList' style='width:80%; height:100%;  text-align:center; margin-left:10%; background:white;'>"
					table += "<tr><th rowspan=" + (json.length + 1) +"> 작업 리스트 </th></tr>"
//					table += "<tr><th>장비</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'								location.href='${ctxPath}/pop/popIndex.do'
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					
					table += "<tr><td>" + data.name + "</td></tr>"
						
				});
				
				table += "</tr>";
				
				$("#joblist").empty()
				$("#joblist").append(table)
				
				$.hideLoading();
			}
		})
				
	}
	//퇴근 보고시 뜨는 화면
	function offWork(){
		//작업보고
		$.showLoading(); 
		
		var date ;
		var workIdx ;
		if(moment().format("HH:mm")<"09:00"){
			date = moment().subtract(1, 'day').format("YYYY-MM-DD")
			workIdx = 1
//			$("#nd").val(1)
		}else{
			date = moment().format("YYYY-MM-DD")
			workIdx = 2
//			$("#nd").val(2)
		}
		
		var list=[];
		var url = "${ctxPath}/chart/getTodayWorkByName.do";
		var param = "&date=" + date + 
//		"&empCd=" + $("#popup2 .worker").val() +
		"&empCd=" + empCd +
		"&workIdx=" + workIdx;
		console.log(param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				try{
					if(typeof json[0].workTy != 'undefined'){
						$("#popup2 #nd").val(json[0].workTy)
					}
				}catch(e){
					modal_Window.content("등록된 작업이 없거나 작업한 장비가 라우팅 되지 않았습니다.<br> 장비가 라우팅 되었는지, 일 생산계획에 작업자가 등록 되었는지 확인 하세요.").open();
				}
				
				
				var tr = "";
				$(".content").remove();
				$(json).each(function(idx, data){
					var targetRatio = Math.round(Number(data.workerCnt) / Number(data.tgCnt) * 100);
					
					if(Number(data.tgCnt)==0){
						targetRatio = 0
					}
					
					var arr=new Object();
					arr.prdNo=data.prdNo
					arr.a=data.dvcId;
					arr.b=decode(data.name);
					arr.c=data.tgCnt;
					arr.d=data.partCyl;
					arr.e=data.cntCyl
					arr.f=data.workerCnt;
					arr.g=targetRatio;
					arr.h=data.lot1;
					arr.i=data.lot2;
					arr.j=data.lot3;
					arr.k=data.lot4;
					arr.l=data.lot5;
					arr.action=''
					arr.empCd=data.empCd
					list.push(arr);
				});
				
				var dataSource = new kendo.data.DataSource({
	                data: list,
	                autoSync: true,
	                schema: {
	                    model: {
	                      id: "a",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                    	 prdNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         b: { editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         c: { editable: false,validation: { required: true } },
	                         d: { editable: false,validation: { required: true } },
	                         e: { editable: false,validation: { required: false },nullable:true },
	                         f: { editable: true,validation: { required: false },nullable:true },
	                         g: { editable: false,validation: { required: true } },
	                         h: { editable: true,validation: { required: false },nullable:true },
	                         i: { editable: true,validation: { required: false },nullable:true },
	                         j: { editable: true,validation: { required: false },nullable:true },
	                         k: { editable: true,validation: { required: false },nullable:true },
	                         l: { editable: true,validation: { required: false },nullable:true },
	                         action: {
	     		          		editable: false,
		     		        },
		     		        newRow: {
		     		          		editable: false,
		     		          		 type: "Boolean"
		     		        }
	                      }
	                    }
	                }
	             });
				
				kendotable.setDataSource(dataSource)
				
				$.hideLoading(); 
			}
		});
	}
	
	//비가동 리스트 불러오기
	function getNonOp (){
		console.log(empCd + " , " + moment().format("YYYY-MM-DD"))

		var url = "${ctxPath}/chart/getWorkerWatingTime.do";
				
		var param = "empCd=" + empCd
					+"&sDate=" + moment().format("YYYY-MM-DD");

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				
				$(json).each(function(idx,data){
					data.prdNo = decode(data.name)
					data.oprNo = decode(data.oprNo);
					data.reason = 0;
					data.time = data.waitTime;
					data.checkSelect = false;
				})
				
				var dataSource = new kendo.data.DataSource({
	                data: json,
	                autoSync: true,
	                schema: {
	                    model: {
							id: "a",	 
							fields: {
								checkerOp :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	oprNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
		                    	chartStatus: { editable: false,validation: { required: false },nullable:true },
		                    	sDate: { editable: false,validation: { required: false },nullable:true },
		                    	eDate: { editable: false,validation: { required: false },nullable:true },
		                    	waitTime: { editable: false,validation: { required: false },nullable:true },
		                    	reason: { editable: true,validation: { required: false },nullable:true },
		                    }
	                    }
	                }
	            });
				
				kendotableOp.setDataSource(dataSource)
			}
		})

	}
	
	// 화살표 버튼 클릭시 플러스
	function plusCnt(num){
		if(num==1){
			// 9 보다 클 경우에는 증가 X
			if($("#one").val()>=9){
				return;
			}
			$("#one").val(Number($("#one").val())+1)
		}else if(num==10){
			// 9 보다 클 경우에는 증가 X
			if($("#two").val()>=9){
				return;
			}
			$("#two").val(Number($("#two").val())+1)
		}else if(num==100){
			$("#three").val(Number($("#three").val())+1)
		}
	}
	
	// 화살표 버튼 클릭시 마이너스
	function minusCnt(num){
		if(num==1){
			// 0보다 작을경우에는 감소 X
			if($("#one").val() <= 0){
				return;
			}
			$("#one").val(Number($("#one").val())-1)
		}else if(num==10){
			// 0보다 작을경우에는 감소 X
			if($("#two").val() <= 0){
				return;
			}
			$("#two").val(Number($("#two").val())-1)
		}else if(num==100){
			// 0보다 작을경우에는 감소 X
			if($("#three").val() <= 0){
				return;
			}
			$("#three").val(Number($("#three").val())-1)
		}
	}
	
	// 클릭시 수량 집어넣기
	function inputCnt(){
		console.log("넣자")
		var cnt = Number($("#three").val() + $("#two").val() + $("#one").val() );
		inputCntData.f = cnt ;
		kendotable.dataSource.fetch();
		
		$("#keyPad").css({
			"display":"none",
		})
		
		tCons.className = "";
	}
	
	//작업 시작 보고하기
	function postJobHist(){
		console.log("시작");
		console.log(startList);
		
		$(startList).each(function(idx,data){
			data.pop_id = popId;
			data.worker = empCd;
			data.dvc_id = data.dvcId;
			data.ty_se = 1;
			
			data.dvc_cnt = 0;
			data.rst_cnt = 0;
			data.prd_no = "";
		})
		
		var obj = new Object();
		obj.val = startList;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		$.showLoading()
		
		var url = "${ctxPath}/popApi/postJobHist.do"
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="SUCCESS"){
					$("#showGoWork").css({
						"display" : "none",
						"z-index" : 0
					})
				    
				    $("#showComplete").css({
				        "transition" : "1s",
				        "opacity" : 1,
				        "display" : "",
				        "position": "absolute",
				        "background": $("#box1").css("background"),
				        "top": $("#content").height() * 0.35,
				        "left": $("#content").width() * 0.29,
				        "height": $("#content").height() * 0.40,
				        "width": $("#content").width() * 0.43,
				        "z-index" : 10,
					    "border": $("#logo").width() * 0.03 + "px solid white",
					    "transition": "all 1s ease 0s",
					    "border-radius": $("#logo").width() * 0.1 + "px",

				    })
				    
				    $.hideLoading();
				}else if(data=="FAILED"){
					alert("관리자에게 문의하세요._001")
				    $.hideLoading();
					return;
				}
				
			    
			} 
		})
		
	}
	
	//작업 종료
	function jobEndHist(){

		console.log("시작");
		
		var startList = kendotable.dataSource.data();
		var savelist =[];
		$(startList).each(function(idx,data){
			if(data.checkSelect==true){
				var arr = {};
				arr.worker = empCd;
				arr.dvc_id = data.id;
				arr.ty_se = 2;
				arr.pop_id = popId;
				if(kendotable.dataSource.data()[0].idx==null || kendotable.dataSource.data()[0].idx=="null"){
	                arr.dvc_cnt = 0;
	            }else{
	                arr.dvc_cnt = data.e;
	            }
				arr.rst_cnt = data.f;
				arr.prd_no = data.prdNo;
				savelist.push(arr);
			}

/* 			data.pop_id = 1;
			data.worker = empCd;
			data.dvc_id = data.dvcId;
			data.ty_se = 1; */
		})
		
		if(savelist.length==0){
			alert("항목을 선택하세요");
			return;
		}
		var obj = new Object();
		obj.val = savelist;
		
		var param = JSON.stringify(obj);
		
		console.log(param)
		$.showLoading()
		
		var url = "${ctxPath}/popApi/postJobHist.do"
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="SUCCESS"){
					$("#showOffWork").css({
						"display" : "none",
						"z-index" : 0
					})
				    
				    $("#showComplete").css({
				        "transition" : "1s",
				        "opacity" : 1,
				        "display" : "",
				        "position": "absolute",
				        "background": $("#box2" ).css("background"),
				        "top": $("#content").height() * 0.35,
				        "left": $("#content").width() * 0.29,
				        "height": $("#content").height() * 0.40,
				        "width": $("#content").width() * 0.43,
				        "z-index" : 10,
					    "border": $("#logo").width() * 0.03 + "px solid white",
					    "transition": "all 1s ease 0s",
					    "border-radius": $("#logo").width() * 0.1 + "px",

				    })
					popup_submenu("box2","showComplete");
					$.hideLoading()
				}else if(data=="FAILED"){
					alert("관리자에게 문의하세요._002");
					$.hideLoading()
					return;
				}
				
			}
		})
		
	}
	
	//비가동 보고
	function saveNonOp(){
		var savelist = [];
		var json = kendotableOp.dataSource.data();
		// 선택되어 있는 리스트 가져오기
		$(json).each(function(idx,data){
			if(data.checkSelect==true){
				savelist.push(data)
			}
		})
		
		if(savelist.length==0){
			alert("비가동 보고하실 항목을 선택해 주세요.");
			return ;
		}
		
		for(i=0 , len = savelist.length; i < len; i++) {
			if(savelist[i].reason==0){
				alert("비가동 사유를 선택해 주세요.");
				return;
			}
		}

		
		var obj = new Object();
		obj.val = savelist;

		var url = "${ctxPath}/chart/addNonOpHst_One.do";
		var param = "val=" + JSON.stringify(obj);

		//비가동 사유 입력 
		console.log("--저장리스트--")
		console.log(param)
		
		$.ajax({
				url : url,
				data : param,
				type : "post",
				success : function(data){
					if(data=="success"){
						alert("저장되었습니다;");
						return;
					}else if(data=="fail"){
						alert("저장에 실패하셨습니다. 관리자에게 문의해 주세요_0930");
						return;
					}
					console.log(data);
				}
		})
		
	}
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="pageMove()">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				2. 작업관리
			</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
      	<table style="width: 100%; height: 100%;">
			<Tr>
			    <td style="text-align: center">
					<div class="menu" id="box1" style="opacity: 0" onclick="popup_submenu('box1','showGoWork')">
						<span class="mainSpan">2-1 작업 시작</span><br>
					    <span class="subSpan">(Start Job)</span>
					</div>

					<div class="menu" id="box2" style="opacity: 0" onclick="popup_submenu('box2','showOffWork')">
						<span class="mainSpan">2-2 작업 종료</span><br>
					    <span class="subSpan">(End Job)</span>
					</div>

					<div class="menu" id="box3" style="opacity: 0" onclick="popup_submenu('box3','showNonOperation')">
						<span class="mainSpan">2-3 비가동 보고</span><br>
					    <span class="subSpan">(Non Operational)</span>
					</div>
<!-- 					<div class="menu" id="box3" style="opacity: 0" onclick="popup_submenu('box3','showChangeWork')"> -->
<!-- 						<span class="mainSpan">2-3 변경 요청</span><br> -->
<!-- 					    <span class="subSpan">(Worker Change)</span> -->
<!-- 					</div> -->
					<!-- <div class="menu" id="box4" style="opacity: 0" onclick="popup_submenu('box4','showOutWork')">
			        <br>
			            <span>BOX4</span>
			        </div> -->
			    </td>
			</Tr>
		</table>
<!--         <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>         -->
	</div>
	
	<div id="showGoWork" class="popCss" style="opacity: 0; display: none;">
		<div class="result"> 미작업 장비 작업시작하시겠습니까.
		
		</div>
		<div id="joblist" class="popContent"></div>
		<div>
			<button id="startJob" class="btn" onclick="postJobHist()">승인</button>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popWorking.do'">거부</button>
		</div>
	</div>
	
	<div id="showOffWork" class="popCss" style="opacity: 0; display: none;">
		<div class="result"> <span id="reName">작업 종료하실항목 선택해주세요.</span>
			<img id="scrollUp" class="allowImg" src="${ctxPath}/css/images/icons-png/arrow-u-black.png"/>
		</div>
		
		<div id="woringList" class="popContent kendo"></div>
		<div>
			<button id="startJob" class="btn" onclick="jobEndHist()">승인</button>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popWorking.do'">거부</button>
			<img id="scrollDown" class="allowImg" src="${ctxPath}/css/images/icons-png/arrow-d-black.png"/>
		</div>
	</div>
	
	<div id="showNonOperation" class="popCss" style="opacity: 0; display: none; text-align: center;">
		<div class="result"> <span> 비가동 항목을 선택해주세요</span></div>
		<div id="nonOpList" class="popContent"></div>
		<div>
			<button id="saveOp" class="btn" onclick="saveNonOp()">저장</button>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popWorking.do'">취소</button>
		</div>
	
	</div>
	
	
	<!-- 	    생산실적 수량을 입력하기 위한 키패드 화면 -->
	<div id="keyPad" class="keyPad" style="opacity: 1; position: absolute; top: 0; display: none;">
		<table border="2" class="keyPad" style="width: 100%; height: 100%;text-align: center;">
			<tr class="keyPad">
				<td class="keyPad" onclick="plusCnt(100)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(10)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(1)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" rowspan="3" style="font-size: 300%;" onclick="inputCnt()">
					INPUT	
				</td>
			</tr>
			<tr class="keyPad">
				<td class="keyPad">
					<input type="number" id="three" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="two" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="one" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%; " readonly="readonly">
				</td>
			</tr>
			<tr class="keyPad">
				<td class="keyPad" onclick="minusCnt(100)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(10)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(1)" style="font-size: 300%; color: blue;">
					▼
				</td>
			</tr>
		</table>
	</div>
	
	<div id="showComplete" style="opacity: 0; display: none; text-align: center;">
		<table style="width: 100%; height: 100%;">
			<tr>
				<td style="font-size: 200%;">처리 완료되었습니다.</td>
			</tr>
			<tr>
				<td>
					<button id="cancle" class="btn2" onclick="location.href='${ctxPath}/pop/popIndex.do'">확인</button>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>