<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<style type="text/css">
body{
	margin : 0;
}
/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	};
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	};

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	};
	
	$(function(){
		setEl();
		sessionChk();

		//주변 어둡게 하기위해
		createCorver();
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})

	//시작메뉴 집어넣기
	function ready(){
		
	}
	
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//타이틀 클릭시 경로이동
	function pageMove(){
		location.href='${ctxPath}/pop/popMainMenu.do?empCd='+empCd
	}
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"	
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
	        "background-color" : "rgb(150, 200, 95)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
		$(".menu").css({
		    "font-size" : getElSize(150) + "px",
		    "border" : getElSize(7) + "px solid white",
		    "height" : originHeight * 0.3 + "px",
		    "width" : originWidth * 0.35 + "px",
		    "vertical-align" : "middle",
		    "cursor" : "pointer",
		    "transition" : "1s",
		    "margin" : originWidth * 0.04 + "px",
		    "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
		    "border-radius" : getElSize(30) + "px",
		    "display" : "inline-block",
		    "color" : "white",
		})

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    //근태관리 배경색
	    $("#box1").css({
	        "background-color" : "rgb(102,117,206)"
	    })

	    //작업관리 배경색
	    $("#box2").css({
	        "background-color" : "rgb(185, 56, 79)"
	    })

	    //이력조회 배경색
	    $("#box3").css({
	        "background-color" : "rgb(223, 208, 2)"
	    })
	    
	    //작업 이력 근태 이력
	    $(".result").css({
	    	"height" : originHeight * 0.045 + "px",
	    	"margin-top" : originHeight * 0.03 + "px",
	    	"font-size" : $("#logo").width() * 0.27 + "px"
	    });
		 
	    //팝업창에서 내용 리스트
	    $(".popContent").css({
	    	"height" : originHeight * 0.55 + "px",
	    	"margin-top": originHeight * 0.04 + "px",
	    	"overflow" : "auto"
	    });
	    
	    
	    $(".btn").css({
	    	"margin-top" : originHeight * 0.03 + "px",
	        "background-color": "lightgray",
	        "border-radius": originWidth * 0.004 + "px",
	        "padding-left": originWidth * 0.012 + "px", 
	        "padding-right": originWidth * 0.012 + "px", 
	        "padding-top": originWidth * 0.004 + "px", 
	        "padding-bottom": originWidth * 0.004 + "px", 
	        "font-size": $("#logo").width() * 0.12 + "px",
	        "color": "white",
	        "cursor": "pointer",
	        "text-align": "center",
	    })
	    
	    $(".btn").hover(function(){
	        $(this).css({
	            "background-color" : "red"
	        })
	    }, function(){
	        $(this).css({
	            "background-color" : "lightgray"
	        })
	    });
	    
	    // 태두리
		$(".popCss").css({
			"text-align" : "center",
			"font-size" : $("#logo").width() * 0.18 + "px",
			"border": $("#logo").width() * 0.03 + "px solid white",
			"transition": "all 1s ease 0s",
			"border-radius": $("#logo").width() * 0.1 + "px",
		})
  
	    $(".menu .mainSpan").css({
	    	"display" : "inline-block",
	    	"margin-top" : originHeight * 0.075
	    })
	}
	
	function setElTable(){
		$(".mainTable").css({
	        "border-spacing" : getElSize(5) + "px",
	        "border-radius" : getElSize(50) + "px"
	    })

	    $(".mainTable th").css({
	        "color" : "black",
	        "text-align" : "center",
	        "padding" : getElSize(30) + "px",
	        "font-size" : getElSize(40) + "px",
	        "background" : "white"
	    })

	    $(".mainTable thead th").css({
	        "background-color" : "rgba(255,0,0,0.6)",
	        "border-radius" : getElSize(20) + "px",
	        "padding" : getElSize(20) + "px",
	        "color" : "white",
	    })

	    $(".mainTable tbody th").css({
//	        "background-color" : "rgba(255,0,0,0.6)",
	        "border-radius" : getElSize(20) + "px",
	        "padding" : getElSize(20) + "px",
	        "color" : "black",
	    })

	    $(".mainTable td, .searchTable td").css({
	        "border-bottom" : getElSize(3) + "px solid #E1DBD9"
	    })
	}
	
	// 팝업창 생성 팝업 CSS #popup_submenu #showGoList #showJobList #showEarlyWork #showOutWork
	function popup_submenu(selected_menu_id,div_id){
		
		console.log(selected_menu_id);
		console.log(div_id);
		
		//출퇴근 조회 클릭시
		if(div_id=="showGoList"){
			// 작업시작 리스트 가져오기
			goList();
			//팝업창 생성
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showJobList"){// 작업 조회 클릭시

			// 작업 종료 리스트 가져오기
			jobList();
			//팝업창 생성
			openPopup(selected_menu_id,div_id);
		}else if(div_id=="showEarlyWork"){// BOX3
			//조퇴 사유 선택하기
			openPopup(selected_menu_id,div_id);
		}

	}
	
	function openPopup(selected_menu_id,div_id){
		showCorver()
		var selected_item = $("#" + selected_menu_id);
		
		$(selected_item).css({
	        "transition" : "0s",
	        "opacity" : 0
	    })

	    $("#" + div_id).css({
	        "transition" : "1s",
	        "opacity" : 1,
	        "display" : "",
	        "position": "absolute",
	        "background": $(selected_item).css("background"),
	        "top": $("#content").height() * 0.16,
	        "left": $("#content").width() * 0.13,
	        "height": $("#content").height() * 0.80,
	        "width": $("#content").width() * 0.73,
	        "z-index" : 10,
	    })
	}
	
	function goList() {
		$.showLoading(); 
		var url = "${ctxPath}/pop/popGoEndHistory.do";
		
		var param = "empCd=" + empCd;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				startList = json;
				var table = "<table border='4' id='tableList' class='mainTable' style='width:80%; height:100%;  text-align:center; margin-left:10%;'>"
					table += "<thead><tr><th> 날짜 </th> <th> 출근 </th> <th> 퇴근 </th> <th> 조퇴 </th> <th> 외출 </th> </tr></thead>"
//					table += "<tr><th>장비</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'								location.href='${ctxPath}/pop/popIndex.do'
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.empCd = empCd;
					table += "<tr><th>" + data.date + "</th>" 
					table += "<th>" + data.startTime + "</th>"
					table += "<th>" + data.endTime + "</th>"
					table += "<th>" + data.earlyTime + "</th>"
					table += "<th>" + data.outTime + "</th>"

						
				});
				
				table += "</tr></table>";
				
				$("#golistTable").empty()
				$("#golistTable").append(table)
				
				setElTable();
				
				$.hideLoading();
			},error : function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
//					$.hideLoading()
					 
			}
		})
			
	};
	
	function jobList() {
		$.showLoading(); 
		var url = "${ctxPath}/chart/getTodayWorkByName.do";
		var date ;
		var workIdx ;
		if(moment().format("HH:mm")<"09:00"){
			date = moment().subtract(1, 'day').format("YYYY-MM-DD")
			workIdx = 1
//			$("#nd").val(1)
		}else{
			date = moment().format("YYYY-MM-DD")
			workIdx = 2
//			$("#nd").val(2)
		}
		
		var param = "empCd=" + empCd +
					"&date=" + date +
					"&workIdx=" + workIdx ;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				startList = json;
				var table = "<table border='4' id='tableList' class='mainTable' style='width:80%; height:100%;  text-align:center; margin-left:10%;'>"
					table += "<thead><tr><th> 장비 </th> <th> 품번 </th> <th> 계획 </th> <th> 수량 </th> <th> 날짜 </th> </tr></thead>"
//					table += "<tr><th>장비</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'								location.href='${ctxPath}/pop/popIndex.do'
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.empCd = empCd;
					table += "<tr><th>" + data.name + "</th>" 
					table += "<th>" + data.prdNo + "</th>"
					table += "<th>" + data.tgCnt + "</th>"
					table += "<th>" + data.workerCnt + "</th>"
					table += "<th>" + date + "</th>"
						
				});
				
				table += "</tr></table>";
				
				$("#joblistTable").empty()
				$("#joblistTable").append(table)
				
				setElTable();
				
				$.hideLoading();
			}
		})
	}
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="pageMove()">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				3. 이력 조회
			</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content">
      	<table style="width: 100%; height: 100%;">
			<Tr>
			    <td style="text-align: center">
					<div class="menu" id="box1" style="opacity: 0" onclick="popup_submenu('box1','showGoList')">
						<span class="mainSpan">3-1 출퇴근 조회</span><br>
					    <span class="subSpan">(Go to work)</span>
					</div>
					<div class="menu" id="box2" style="opacity: 0" onclick="popup_submenu('box2','showJobList')">
						<span class="mainSpan">3-2 작업 조회</span><br>
					    <span class="subSpan">(Go to work)</span>
					</div>
<!-- 					<div class="menu" id="box3" style="opacity: 0" onclick="popup_submenu('box3','showEarlyWork')"> -->
<!-- 						<span class="mainSpan">3-3 BOX3</span><br> -->
<!-- 					    <span class="subSpan">(box3..)</span> -->
<!-- 					</div> -->
					
			    </td>
			</Tr>
		</table>
<!--         <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>         -->
	</div>
	
	<div id="showGoList" class="popCss" style="opacity: 0; display: none; text-align: center;">
		<div class="result"> 근태 이력
			
		</div>
		<div id="golistTable" class="popContent"></div>
		<div>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popHistory.do'">확인</button>
		</div>
	</div>
	
	<div id="showJobList" class="popCss" style="opacity: 0; display: none; text-align: center;">
		<div class="result"> 작업 이력
			
		</div>
		<div id="joblistTable" class="popContent"></div>
		<div>
			<button id="cancle" class="btn" onclick="location.href='${ctxPath}/pop/popHistory.do'">확인</button>
		</div>
	</div>
	
	<div id="showEarlyWork" style="opacity: 0; display: none; text-align: center;">
		<table style="width: 100%; height: 100%;">
			<tr>
				<td style="font-size: 200%;">BOX 3 기능 추가중입니다</td>
			</tr>
			<tr>
				<td>
					<button id="cancle" class="btn2" onclick="location.href='${ctxPath}/pop/popIndex.do'">확인</button>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>