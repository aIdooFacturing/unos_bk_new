<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>

<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>

<style type="text/css">
body{
	margin : 0;
}

.flex-container{
	display: flex;
	flex-wrap: wrap;
    overflow: auto;
    align-items: center; /* 중앙정렬 */
    justify-content: center;
}

.flex-container:nth-child(even) {
 	background-color : #F6F6F6;
}

.flex-container:nth-child(odd) {
  	background-color : white;
}

#viewMain {
   	background : silver;
}
#viewMain .flex-container {
   	background : darkgray;
}

#viewMain .flex-container {
	height : 100%;	
}
.keyPad tbody{
	-webkit-box-shadow: 0 4px 15px 0 rgba(0, 0, 0, 0.2), 0 6px 15px 0 black;
	-moz-box-shadow: 0 4px 15px 0 rgba(0, 0, 0, 0.2), 0 6px 15px 0 black;
	box-shadow: 0 4px 15px 0 rgba(0, 0, 0, 0.2), 0 6px 15px 0 black;
}
#okCss{
	-webkit-box-shadow: -5px -5px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 black !important;
	-moz-box-shadow: -5px -5px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 black !important;
	box-shadow: -5px -5px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 10px 0 black !important;
}

/* #logo{
    height: 33.732px;
    margin-right: 7.44792px;
    border-radius: 3.72396px;
    float: right;
    background-color: white;
}  */
</style>


<script type="text/javascript">

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	var tCons;
	
	$(function(){
		
	})

	//시작메뉴 집어넣기
	function ready(){
		setEl();
		sessionChk();
		<%-- <% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%> --%>
		//focus TEXT 맞추기
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
		
		$(".date").datepicker({
		})
		
		$("#date").val(moment().format("YYYY-MM-DD"));
		//품번,차종 가지고 오기
		getGroupMenu();
	}
	
	const createCorver = () => {
	    const corver = document.createElement("div")
	    corver.setAttribute("id", "corver")
	    corver.style.cssText =
	        "position : absolute;" +
	        "width : " + originWidth + "px;" +
	        "height : " + originHeight + "px;" +
	        "background-color : rgba(0, 0, 0, 0);" +
	        "transition : 1s;" +
	        "z-index : -1;";

	    $("body").prepend(corver)
	}
	
	const showCorver = () =>{
		$.showLoading();
		
	    $("#corver").css({
	        "background-color" : "rgba(0, 0, 0, 0.7)",
	        "z-index": 2
	    })
	    
		setTimeout(()=>{
			$.hideLoading();
		}, 1000)
	   
	}

	const hideCorver = () => {
	    $("#corver").css({
	        backgroundColor : "rgba(0, 0, 0, 0)"
	    })

	    setTimeout(()=>{
	        $("#corver").css("z-index", -1)
	    }, 1000)
	}
	
	//사용자 session Chk 
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//타이틀 클릭시 경로이동
	function pageMove(){
		location.href='${ctxPath}/pop/popQuality.do?empCd='+empCd
	}
	
	function setEl(){
		
		$("#header").css({
/*             "position" : "absolute",
			"width" : getElSize(targetWidth), */
			"height" : originHeight * 0.06,
			"background" : "black",
			"color" : "white",
			"display": "block",
            "font-size" : getElSize(50)
		})
		
		$("#logo").css({
			"height": $("#header").height() * 0.5,
			"float": "left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.25,
			"margin-left": getElSize(30),
			"border-radius": getElSize(30),
			"cursor": "pointer"
		})

		$("#userId").css({
			"float":"left",
			"margin-left": getElSize(50) +"px",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.30,
			"font-size": $("#logo").height() *  0.7
		})

		$("#logout").css({
			"cursor" : "pointer",
			"float":"left",
			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.20,
			"margin-left": getElSize(30) +"px",
			"height": $("#logo").height() *  0.7
		})
		
		$("#logo2").css({
			"height":  $("#header").height() * 0.65,
			"float": "right",
			"margin-top": $("#header").height()/2 - $("#header").height() * 0.32,
			"margin-right": getElSize(30),
			"border-radius": getElSize(30),
			"background-color": "white"
		})
		
		$("#time").css({
//		    "position": "absolute",
		    "float":"right",
 			"margin-top":  $("#header").height()/2 - $("#header").height() * 0.32,
 			"margin-right": getElSize(50),
// 			"margin-right": getElSize($("#logo2").width()*15),
 			/*			"left": getElSize($("#logo2").width()*15), */
			"font-size": $("#logo2").height() *  0.7
		});

		 // 
	    $("#content").css({
	    	"height" : originHeight - $("#header").height() + "px"
	    	,"width" : "100%"
	    })
	    
		$("#headTitle").css({
			"position":"absolute",
			"top":  $("#header").height()/2 - $("#header").height() * 0.36,
//			"left": getElSize(2000),
			
			"display" : "inline-block",
			"background-color" : "rgb(170, 120, 220)",
	        "border" : getElSize(7) + "px solid white",
	        "border-radius" : getElSize(30) + "px",
	        "padding-left" : getElSize(15),
	        "padding-right" : getElSize(15),
	        "cursor" : "pointer",
	        
			"font-size": $("#logo2").height() *  0.7
		})

		$("#headTitle").css({
			"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
		})
		
		// 네모 박스 위치 글자
	    $(".menu").css({
	        "font-size" : getElSize(150) + "px",
	        "border" : getElSize(7) + "px solid white",
	        "height" : originHeight * 0.3 + "px",
	        "width" : originWidth * 0.35 + "px",
	        "vertical-align" : "middle",
	        "cursor" : "pointer",
//	        "padding-top" : getElSize(50) + "px",
	        "transition" : "1s",
	        "margin" : originWidth * 0.04 + "px",
	        "margin-top" : (originHeight - $("#header").height()) * 0.06 + "px",
	        "border-radius" : getElSize(30) + "px",
	        "display" : "inline-block",
	        "color" : "white",
	        //"position" : "relative"
	    })

	    $(".menu").css({
	        "opacity" : 1,
	        "text-align" : "center"
	    })
	    
	    $(".menu .mainSpan").css({
	    	"display" : "inline-block",
	    	"margin-top" : originHeight * 0.075
	    })

	}
	
	function boxCss(size){
		
	    $("#viewMain").css({
	    	"height" : "25%",
	    	"width" : "100%"
// 	    	"background" : "darkgray"
	    })
	    
	    $("#viewSub").css({
	    	"height" : "75%",
	    	"width" : "100%",
	    	"overflow" : "auto"
// 	    	,"position" : "absolute"
	    })
	    
	    $("#subShow").css({
	    	"height" : "90%"
	    })

	    //기본 prdNoBox 
		$(".prdNoBox").css({
// 			"width" : getElSize(320),
// 			"height" : getElSize(320),
			"width" : size,
			"height" : size,
			"background" : "rgb(174, 213, 67)",
			"background" : "linear-gradient( rgb(174, 213, 67),#1DDB16)",
			"margin" : getElSize(50),
			"color" : "black",
			"text-align" : "center",
			"font-size" : getElSize(70),
			"display" : "table",
			"table-layout" : "fixed",
			"border-radius": getElSize(18),
			"overflow-wrap": "break-word",
			"cursor" : "pointer",
// 		    "font-weight": "bold"
		})
		
		$(".prdNoBox span").css({
			"display":"table-cell",
			"vertical-align": "middle"
		})
		//전체 품번은 따로 표기
		$(".prdNoBox1").css({
// 			"width" : getElSize(320),
// 			"height" : getElSize(320),
			"width" : size,
			"height" : size,
			"background" : "rgb(174, 213, 67)",
			"background" : "linear-gradient(#F15F5F, #F361DC)",
			"margin" : getElSize(50),
			"color" : "black",
			"text-align" : "center",
			"font-size" : getElSize(70),
			"display" : "table",
			"table-layout" : "fixed",
			"border-radius": getElSize(18),
			"overflow-wrap": "break-word",
			"cursor" : "pointer",
// 		    "font-weight": "bold"
		})
		
		$(".prdNoBox1 span").css({
			"display":"table-cell",
			"vertical-align": "middle"
		})

		////////////////////////////////////////////////////////
		/* $(".prdNoBox1").css({
			"width" : getElSize(320),
			"height" : getElSize(320),
			"background" : "red",
			"margin" : getElSize(30),
			"color" : "black",
			"text-align" : "center",
			"font-size" : getElSize(70),
			"display" : "table",
			"table-layout" : "fixed",
			"border-radius": getElSize(18),
			"overflow-wrap": "break-word",
			"cursor" : "pointer"
		})
		
		$(".prdNoBox1 span").css({
			"display":"table-cell",
			"vertical-align": "middle"
		}) */
		////////////////////////////////////////////////////////

		$("#viewMain >.flex-container").css({
			"width" : "100%",
			"justify-content": "center",
			"flex-direction" : "column"
		})

		$("#viewSub >.flex-container").css({
			"width" : "100%",
			"justify-content": "center"
		})
		
		$(".title_Form").css({
			"font-size" : "170%"
		})
		
		$(".value_Form div >").css({
			"font-size" : "135%"
			,"width" : "100%"
		})
		
		$(".article").css({
//				"font-size" : "150%"
			"width" : "50%"
			,"float" : "left"
		})
		
		$(".title_Form button").css({
			"cursor" : "pointer"
		})
		
		$("#resetBtn").css({
			"background" : "red"
			,"width" : "100%"
			,"height" : "85%"
			,"font-size" : "150%"
		})
		
		$("#saveBtn").css({
			"background" : "green"
			,"width" : "100%"
			,"height" : "90%"
			,"font-size" : "150%"
		})
		
	}
	
	
	var Allgroup = [];
	//전체 품번 그리기
	function AllgroupMenu(){
		// 전체 리스트 전역 변수에 담음
		// Allgroup
		var json = Allgroup;
		
		var boxs = "<div class='flex-container' >";
		var boxsMain = "<div class='flex-container' >";
		var compare = "";
		$(json).each(function(idx,data){
			var prdNo = data.prdNo.charAt(0)
			
					
			boxsMain += "<div class='prdNoBox' onclick = changePrdNo(\"" + data.prdNo + "\") ><span>" + data.prdNo + "</span></div>"
					
			if(compare!=prdNo){
				boxs += "</div><div class='flex-container' ><div class='prdNoBox' onclick = changePrdNo(\"" + data.prdNo + "\") ><span>" + data.prdNo + "</span></div>"
			}else{
				boxs += "<div class='prdNoBox' onclick = changePrdNo(\"" + data.prdNo + "\") ><span>" + data.prdNo + "</span></div>"
			}
			compare = prdNo;
		})
		
// 				boxsMain +="<div class='prdNoBox1' onclick = 'AllgroupMenu(\"" + dataList + "\")' ><span>전체 품번</span></div></div>"
		boxs +="<div class='prdNoBox1' onclick = 'getGroupMenu()' ><span>메인 품번</span></div></div>"
		boxs +="</div>"

		$("#viewTotal").empty();
		$("#viewTotal").append(boxs);
		
		boxCss(getElSize(310));
		
		console.log(Allgroup)
		
	}
	//품번 가지고 오기
	function getGroupMenu(){
		$.showLoading()
		var url = "${ctxPath}/pop/PoPprdNoList.do";
		var param = "shopId=" + 1 +
					"&popId=" + popId;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data)
// 				json = data.dataList;
				json = data.popList;
				var dataList = data.dataList;
				var boxs = "<div class='flex-container' >";
				var boxsMain = "<div class='flex-container' style='height:100%;'>";
				var compare = "";
				$(json).each(function(idx,data){
					var prdNo = data.prdNo.charAt(0)
					
					
					boxsMain += "<div class='prdNoBox' onclick = changePrdNo(\"" + data.prdNo + "\") ><span>" + data.prdNo + "</span></div>"
					
// 					if(compare!=prdNo){
// 						boxs += "</div><div class='flex-container' ><div class='prdNoBox' onclick = changePrdNo(\"" + data.prdNo + "\") ><span>" + data.prdNo + "</span></div>"
// 					}else{
// 						boxs += "<div class='prdNoBox' onclick = changePrdNo(\"" + data.prdNo + "\") ><span>" + data.prdNo + "</span></div>"
// 					}
// 					compare = prdNo;
				})
				
				//전체 리스트
				Allgroup = dataList;
				
// 				boxsMain +="<div class='prdNoBox1' onclick = 'AllgroupMenu(\"" + dataList + "\")' ><span>전체 품번</span></div></div>"
				boxsMain +="<div class='prdNoBox1' onclick = 'AllgroupMenu()' ><span>전체 품번</span></div></div>"
				boxs +="</div>"
				
				$("#viewTotal").empty();
				$("#viewTotal").append(boxsMain);
				
				
// 				$("#viewSub").empty();
// 				$("#viewSub").append(boxs);
				
				//prdNo에 값넣기
				$("#prdNo").kendoComboBox({
					dataSource : json,
					autoWidth : true,
					dataTextField: "prdNo",
					dataValueField: "prdNo",
					placeholder: "please select..",
					clearButton: false,
// 					value: json[0].prdNo,
					//품번 변경되었을시
					change: function(e){
						var value = this.value();
						// 초기화 시키는데 무한루프 돌므로 공백일 경우에는 변경 할필요 없음
						if(value=="")
							return
						selectProcess(value);
// 						getDvcListByPrdNo(value);
					}
				});
				
				//초기화 시키기
				var combobox = $("#prdNo").data("kendoComboBox");
				combobox.value("");
				combobox.trigger("change");

				boxCss("27%");
				
// 					"position": "relative",
// 				    "left": "50%",
// 				    "transform": "translateX(-50%)"
// 				})
				
// 				$(".prdNoBox").css({
// 					"background" : "black"
// 				})
				
				$.hideLoading()
			}
		});
	}
	
	//품번 선택시 prdNo 값 변경
	function changePrdNo(prdNo){
		var combobox = $("#prdNo").data("kendoComboBox");
		combobox.value(prdNo);
		combobox.trigger("change");
		
	}
	
	//R,MCT,CNC 선택하기
	function selectProcess(prdNo){
		var boxsMain = "<div class='flex-container' style='height:100%; '>";
		boxsMain += "<div class='prdNoBox' onclick ='getDvcListByPrdNo(\"" + prdNo + "\" ,\"" + "0010" + "\")' ><span>" + "R" + "</span></div>"
		boxsMain += "<div class='prdNoBox' onclick ='getDvcListByPrdNo(\"" + prdNo + "\" ,\"" + "0020" + "\")' ><span>" + "MCT" + "</span></div>"
		boxsMain += "<div class='prdNoBox' onclick ='getDvcListByPrdNo(\"" + prdNo + "\" ,\"" + "0030" + "\")' ><span>" + "CNC" + "</span></div>"
		boxsMain +="</div>"
		
//		machineList += "<label class='dvc' id=" + data.dvcId + " onclick='gridTabAdd(\"" + data.dvcId + "\" ,\"" + data.originName + "\")'> " + data.name + "</label>";

		$("#viewTotal").empty();
		$("#viewTotal").append(boxsMain);
		
		boxCss("27%");
	}
	//값 변경시 장비 호출
	function getDvcListByPrdNo(prdNo,prc){
		console.log(prdNo,prc)
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				// 자주검사 서 품번 클릭했을시 등록된 기준시트지가 없을 경우 리턴하기
				if(json.length==0){
					alert("시트가 등록되지 않았습니다.")
					var combobox = $("#prdNo").data("kendoComboBox");
					combobox.value("");
					combobox.trigger("change");
					$.hideLoading()
					return;
				}
				
				gridDeviceBox(json,prc)
				console.log(json)
				$.hideLoading()
			}
		})
	}
	
	//장비 박스 그리기
	function gridDeviceBox(json,prc){
		//tabstrip 선언하기 위한 변수
		tab = false;
		
		//확대 부분 그리기
		var boxs = "<div id='viewSub'><div id='gridView' style='height:17%; width:100%;'>"
			boxs += "<table style='width:100%; height:100%; font-size:150%;'><tr height='20%'><th width='5%'>No</th><th width='30%'>attrNameKo</th><th width='25%'>spec</th><th width='10%'>cvt1</th><th width='10%'>cvt2</th><th width='10%'>cvt3</th><th width='10%'>cvt4</th></tr>"
			boxs += "<tr height='78%'><td> <input type='text' id='clmNo' class='textZoom' readOnly='readOnly'> </td>"; 
			boxs += "<td> <input type='text' id='attrNameKo' class='textZoom' readOnly='readOnly'> </td>"; 
			boxs += "<td> <input type='text' id='spec' class='textZoom' readOnly='readOnly'> </td>"; 
			boxs += "<td> <input type='text' id='cvt1' class='textZoom' readOnly='readOnly'> </td>"; 
			boxs += "<td> <input type='text' id='cvt2' class='textZoom' readOnly='readOnly'> </td>"; 
			boxs += "<td> <input type='text' id='cvt3' class='textZoom' readOnly='readOnly'> </td>"; 
			boxs += "<td> <input type='text' id='cvt4' class='textZoom' readOnly='readOnly'> </td></tr></table>"; 
		　	boxs +=	"</div><div id='grid' style='height:81%; width:100%;'></div></div>";
		　	
		boxs = "<div id='grid' style='height:74%; width:100%;'></div>";
		　
		//Main 그리기
		var boxsMain = "<div id='viewMain'><div class='flex-container' >";
		var compare = "";
		console.log("prd :" + prc)
		$(json).each(function(idx,data){
// 			console.log(data.oprNo)
// 			console.log(data.oprNo.indexOf("001"))
			if(prc=="0010"){
				console.log("1")
				if(data.oprNo.indexOf("001")!=-1){
					data.name = decode(data.name)
					data.originName = data.name
					boxsMain += "<div class='prdNoBox'id=" + data.dvcId + " onclick='gridTabAdd(\"" + data.dvcId + "\" ,\"" + data.originName + "\")' ><span>" + data.name + "</span></div>"
				}
			}else if(prc=="0020"){
				console.log("2")

				if(data.oprNo.indexOf("002")!=-1){
					data.name = decode(data.name)
					data.originName = data.name
					boxsMain += "<div class='prdNoBox'id=" + data.dvcId + " onclick='gridTabAdd(\"" + data.dvcId + "\" ,\"" + data.originName + "\")' ><span>" + data.name + "</span></div>"
				}
			}else if(prc=="0030"){
				console.log("3")

				if(data.oprNo.indexOf("003")!=-1){
					data.name = decode(data.name)
					data.originName = data.name
					boxsMain += "<div class='prdNoBox'id=" + data.dvcId + " onclick='gridTabAdd(\"" + data.dvcId + "\" ,\"" + data.originName + "\")' ><span>" + data.name + "</span></div>"
				}
			}
// 				machineList += "<label class='dvc' id=" + data.dvcId + " onclick='gridTabAdd(\"" + data.dvcId + "\" ,\"" + data.originName + "\")'> " + data.name + "</label>";
		})
		
		boxsMain +="</div></div>"
		boxs +="</div>"
		$("#viewTotal").empty();
		$("#viewTotal").append(boxsMain);
		$("#viewTotal").append(boxs);
		
		boxCss(getElSize(370));
		
// 		$("#viewSub").empty();
// 		$("#viewSub").append(boxs);
		// tabstrip 그리기
		// gridTabAdd 에 연관있음
		// tab = true => kendoTabStrip 선언되어있음
		// 젤 처음 초기에만 tabstrip 생성 false => true 변경
		if(!tab){
			tabstrip = $("#grid").kendoTabStrip().data("kendoTabStrip");
			tab = true;
		}else{
			tabstrip = $("#grid").data("kendoTabStrip");
		}

		$(".k-tabstrip-wrapper").css({
			"width" : "99.7%"
		})
		
		$(".textZoom").css({
			"width" : "90%"
			,"height" : "80%"
			,"font-size": "120%"
			,"text-align" : "center"
		})
	}
	
	//행 선택할때마다
	function onChanget(){
		
		//uid 값 확인
		var selected = $.map(this.select(), function(item) {
			uidSelect = $(item).data().uid
        	return uidSelect;
        });

		//gridId, inputCntData(선택한 값 model) 초기화
		var gridId = "";
		var inputCntData = "";
		//tab 갯수 구하기
		var size = $(".grid").length;
		//tab 갯수만큼 for문 돌린 후 선택 값을 찾는다 
		for(i = 0; i<size; i++){
			//현재 도는 gridIdFind 구하기
			var gridIdFind = $(".grid")[i].id;
			
			var dataList = $("#"+gridIdFind).data("kendoGrid");
			//근접한 tr 찾기
			var row = dataList.tbody.find("tr[data-uid='" + uidSelect + "']");
			//선택한 값 도출
			var inputCntDataFind = grid.dataItem(row)
			
			//만약 일치하는값을 찾았으면  저장하기
			if(inputCntDataFind!=null){
				gridId = gridIdFind ;
				inputCntData = inputCntDataFind ;

				setTimeout(function() {
					row.addClass("k-state-selected")
				}, 200);
				
			}
			
		}
		
		//확대 하는곳 보여주기
		zoomView(gridId,inputCntData);
		//결과 일치하는것들
// 		console.log(gridId,inputCntData)
//         console.log("Selected: " + selected.length + " item(s), [" + selected.join(", ") + "]");
	}
	
	//확대하는곳 그리고 보여주는 함수
	function zoomView(id,dataItem){
		
		var columns = $("#"+id).data("kendoGrid").options.columns;
		var form = $("#gridView");
		
		for (var i = 0; i < columns.length; i++) {
			var field = columns[i].field;
// 			console.log(dataItem[field])
			form.find("#" + field).val(dataItem[field]);
			
			//columns 행 부제목이 있을경우
			if(columns[i].columns!=undefined){
				//부제목 안 컬럼 수량만큼 반복문 돌리기
				for ( var j = 0; j < columns[i].columns.length; j++){
					var field = columns[i].columns[j].field ;
					form.find("#" + field).val(dataItem[field])
				}
			}
			
// 			console.log(columns[i].columns)
			//부제목 안에 컬럼이 있을경우
			
		}
	}
	
	var tCons;
	var tab = false;
	//장비 클릭시 Tab 추가하기
	function gridTabAdd(dvcId,name){

		// 클릭한 애들 표기하기
		$("#"+dvcId).css({
			"background" : "red"
			,"opacity" : 0.6
		})
		
		console.log(tab)
		
// 		closeButton = "<span onclick='tabstrip.remove($(this).closest(\"li\"))'>X</span>";
		closeButton = "<span onclick='tabRemove($(this).closest(\"li\"))'>X</span>";
		
		//id 부여하기
		var gridId = "grid" + dvcId;
		// 이미 같은것이 있을경우 추가할 필요없음
		if($("#"+gridId).length!=0){
			return;
		}
		tabstrip.append([
			{
				text: name + " " + closeButton,
				encoded: false,
				content: "<div id = '" + gridId + "' class='grid' name=" + dvcId +"></div>"
			}
		]);
		
		$("#"+gridId).kendoGrid({
			editable : false,
			height : $("#grid").height() * 0.85,
// 			height : getElSize(1010),
// 			height : $("#grid").height(),
			selectable: true,
			change: onChanget,
			dataBound : function(e){
				grid = this;
				
				grid.tbody.find('tr').each(function(){
					var item = grid.dataItem(this);
					kendo.bind(this,item);
				})
				
				for (var i = 0; i < this.columns.length; i++) {
					if(i<4)
	            	this.autoFitColumn(i);
	            }
				
				$("#" + gridId + " tr th").css({
					"font-size" : getElSize(48)
				})
				
				$("#" + gridId + " tr td").css({
					"font-size" : getElSize(45)
				})

				//event 초기화 
				$('.cntInput').unbind('click');
				//숫자 입력을 위한 이벤트 생성
				$(".cntInput").click(function(e){
					var tr = $(this).closest("tr");
					var td = $(this).closest("td");
					//현재 보고있는 Tab selectGridId 구하기
					var selectGridId = $(".k-state-active div")[0].id;
					
					//전역변수로 지정후 input,입력 버튼 클릭시 활용하기
					uid = tr[0].dataset.uid;
					showId = selectGridId;
					columnId = td.context.dataset.bind;
					columnId = columnId.substring(columnId.indexOf(":")+1,columnId.length);
					
					var kendotable = $("#"+selectGridId).data("kendoGrid");
					var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
					
					setTimeout(function() {
						row.addClass("k-state-selected")
					}, 200);
					
// 					console.log(tr)
// 					console.log(td)
// 					console.log(td.context)
// 					console.log(td.context.name)
// 					console.log(kendotable)
// 					console.log(row)

					
					//기존 초기화
					if(tCons!=undefined){
						tCons.className = "";
					}
					
					tCons = tr[0];
					tCons.className = "selectRow";
		    		$("#one").val("")
		    		$("#two").val("")
		    		$("#three").val("")
		    		$("#four").val("")
		    		$("#five").val("")
		    		$("#six").val("")
					rowData = kendotable.dataItem(row)
					
// 					console.log("현재 보고있는 tab :" + selectGridId)
// 					console.log("현재 id :" + columnId)
// 					console.log(row)
// 					console.log(kendotable)
// 					console.log(grid)
// 					console.log("== 선택 Row ==")
					// 클릭한 데이터 숫자 값
					eval("var nStr = rowData."+columnId+".toString()");
// 				    var nStr = inputCntData.cvt1.toString();
		    		
		    		//소수 . 위치 확인
		    		var idx = 0;
		    		//자연수,소수 변수
		    		var natural,decimal;

		    		console.log(nStr)
		    		//값넣기
		    		$("#inputCntKIOSK").val(nStr);
		    		
		    		//name 에다가 소수점 넣기 나중에 PlUS MINUS 하기 위함
		    		$("#inputCntKIOSK").attr("name",rowData.dp);
		    		
		    		//소수점 자리확인하기
		    		idx = nStr.indexOf(".");
		    		//소수점이 없을경우 (자연수임)
		    		if(idx==-1)
		    			idx = nStr.length;
		    		
		    		//자연수 변수
		    		natural = nStr.substring(0,idx);
		    		decimal = nStr.substring(idx+1,nStr.length);
		    		
					// 1,10,100,1000 의 자리 분리를 위해서 반목문
				    for(var i = 0; i < natural.length; i++){
				    	// 1의 자리일 경우
				    	if(i==natural.length-1){
				    		$("#four").val(Number(natural[i]))
				    	}else{
				    		$("#five").val(Number($("#five").val()+natural[i]))
				    	}
// 				        answer.push(Number(nStr[nStr.length -1 -i]));
				    }
					// 0.1,0.01 소수점 자리 분리를 위해서 반목문
				    for(var i = 0; i < decimal.length; i++){
				    	if(i==0){
				    		$("#three").val(Number(decimal[i]))
				    	}else if(i==1){
				    		$("#two").val(Number(decimal[i]))
				    	}else{
				    		$("#one").val(Number($("#one").val()+decimal[i]))
				    	}
				    	
// 				        answer.push(Number(nStr[nStr.length -1 -i]));
				    }
				    
				    
// 					console.log(answer)
					
					
					$("#keyPadDiv").css({
// 						"border-radius": getElSize(40),
						"background":"silver",
						"width": originWidth * 0.33,
						"height": originHeight * 0.25,
						"margin-top": $("#header").height() * 1.12,
						"z-index": 11,
						"display":"",
						"box-shadow": "rgba(0,0,0,0.5) 0 0 0 9999px, rgba(0,0,0,0.5) 2px 2px 3px 3px"

					})
					
// 					$(".keyPad").css({
// 						"border" : "2px solid black"
// 					})
					
					$("#keyPadDiv").css({
						"left" : (originWidth / 2) - ($("#keyPadDiv").width() / 2),
					})
					
					/* $("#headTitle").css({
						"position":"absolute",
						"top":  $("#header").height()/2 - $("#header").height() * 0.36,
			//			"left": getElSize(2000),
						
						"display" : "inline-block",
				        "background-color" : "rgb(80, 140, 245)",
				        "border" : getElSize(7) + "px solid white",
				        "border-radius" : getElSize(30) + "px",
				        "padding-left" : getElSize(15),
				        "padding-right" : getElSize(15),
				        "cursor" : "pointer",
				        
						"font-size": $("#logo2").height() *  0.7
					}) */
			
					$("#headTitle").css({
						"left" : (originWidth / 2) - ($("#headTitle").width() / 2),
					})
				})
				
				//입력창 나올때 이벤트 발쌩
				$('html').click(function(e) {
//					console.log(e.target)
					if(!($(e.target).hasClass("keyPad") || $(e.target).hasClass("cntInput")) ){
						if(tCons!=undefined)
						tCons.className = "";
						
						$("#keyPadDiv").css({
							"display":"none",
						})
					}
				});
				
				$(".displayTy").css({
					"background" : "darkslateblue"
				})
				
				$(".grid select").css({
					"font-size" : getElSize(75)
				})
				
				$(".cntInput").css({
					"font-size" : getElSize(50)
				})

	
			},
			columns : [{
				title : "no"
				,field : "clmNo"
				,width : getElSize(150)
			},{
				title : "prdNo"
				,field : "prdNo"
			},{
				field : "attrNameKo"
			},{
				field : "spec"
			},{
				field : "measurer",hidden: true
			},{
				title : "cavity"
				,columns : [{
					field : "cvt1"
// 					,width : getElSize(230)
					,editor : function (container,options){
						attrTy = options.model.attrTy
						if(attrTy==1){
							chkTrue(container,options);
						}/* else{
							demicalNumber(container,options);
						} */
					}
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' name='cvt1' data-bind='value:cvt1' style='width:90%;'> #}else{# <select data-bind='value:cvt1' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
// 					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' data-bind='value:cvt1' style='width:90%;'> # } #"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
// 				    	"class": "# if(data.rowClass === '0') { # editable-cell # } else { # editable-cell-alt # } #" ,
// 				    	style: "font-size :" + getElSize(30)
					}
					
// 					,template:kendo.template("#if (attrTy == 1) {# #=chkTrue##} else if(attrTy == 2){# #=kendo.toString(2,'3'+4)# #} #")
// 					,template:kendo.template("#if (attrTy == 1) {# #=' '##} else if(attrTy == 2){# #=kendo.toString(low,'n'+dp)# #} #")


				},{
					field : "cvt2"
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' name='cvt2' data-bind='value:cvt2' style='width:90%;'> #}else{# <select data-bind='value:cvt2' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
					}
// 					,width : getElSize(230)
				},{
					field : "cvt3"
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' name='cvt3' data-bind='value:cvt3' style='width:90%;'> #}else{# <select data-bind='value:cvt3' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
					}
// 					,width : getElSize(230)
				},{
					field : "cvt4"
					,template : "#if(attrTy=='2'){# <input type='text' class='cntInput' name='cvt4' data-bind='value:cvt4' style='width:90%;'> #}else{# <select data-bind='value:cvt4' style='width : 90%;'><option value='0'>O</option><option value='1'>X</option>#}#"
					,attributes: {
						"class": "# if(data.attrTy === '2') {# displayTy #}#",
					}
// 					,width : getElSize(230)
				}]
			}]
		}).data("kendoGrid")
		
		//grid 값넣어주기
		gridTable(gridId,name,dvcId);
		
		var tabSize = $(".grid").length
		tabstrip.select(length-1);

	}
	
	var chkTypeList = [];
	var arr = {};
	arr.value = "0";
	arr.text = "O";
	chkTypeList.push(arr);
	var arr = {};
	arr.value = "1";
	arr.text = "X";
	chkTypeList.push(arr);
	
	function chkTrue(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 valuePrimitive: true,
			 autoWidth: true,
			 height: 3300,
			 dataTextField : "text",
			 dataValueField  : "value",
			 dataSource: chkTypeList,
		 }).data("kendoDropDownList");
	}
	
	//Tab X클릭시  Tab remove
	function tabRemove(e){
		
		tabstrip.remove(e);
		
		var size = $(".grid").length
		
		//전체 먼저 초록색으로 변경하기
		$(".prdNoBox").css({
			"background" : "linear-gradient( rgb(174, 213, 67),#1DDB16)"
			,"opacity" : 1
		});
		
		//grid Id값 찾아서 현재 남아있는애들만 빨강색으로 표기
		for( i=0; i < size; i++){
			var gridId = $(".grid")[i].id;
			gridId = $("#"+gridId).attr("name");
			$("#"+gridId).css({
				"background" : "red"
				,"opacity" : 0.6
			})
		}
	}

	//테이블 가져오기,데이터 가져오기,자주검사,자주 검사,자주검사 데이터
	function gridTable(gridId,name,dvcId){
		
		console.log(gridId)
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
// 		if($('#Check_dvcId').data('kendoComboBox').dataItem()!=undefined){
// 			dvcIdName = $('#Check_dvcId').data('kendoComboBox').dataItem().name
// 		}
		
//		$("#checkCycle")	검사주기
//		$("#chkTy")			검사유형(자주검사 : 2)
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + 2 +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val() + 
					"&dvcId=" + name;
					
// 					"&dvcId=" + $("#Check_dvcId option:selected").html();
// 		var param = "prdNo=FS_RR_LH &chkTy=2&checkCycle=1&date=2019-06-27&ty=2&dvcId=FS/R M#3";
		
		console.log(param)
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$.hideLoading()
				var json = data.dataList;
				
				$(json).each(function(idx,data){
					data.chkTy = decode(data.chkTy);
					data.attrNameKo = decode(data.attrNameKo);
					data.spec = decode(data.spec);
					data.measurer = decode(data.measurer);
					data.name = decode(data.name);
					data.dvcId = dvcId;
					
					var target = 0;
					if(data.target==""){
						target = 0;
					}else{
						target = data.target;
					}
					//최근값 가지게 오고 하고 싶으면 아래 주석 해제
					if(data.cvt1==""){
						data.cvt1=0
					}
					if(data.cvt2==""){
						data.cvt2=0
					}
					if(data.cvt3==""){
						data.cvt3=0
					}
					if(data.cvt4==""){
						data.cvt4=0
					}
					
					//이것은 target 목표값을 가지고와서 뿌리기
					if(data.attrTy==2){
						console.log(target)
						data.cvt1 = Number(target).toFixed(data.dp);
						data.cvt2 = Number(target).toFixed(data.dp);
						data.cvt3 = Number(target).toFixed(data.dp);
						data.cvt4 = Number(target).toFixed(data.dp);
						
					}
					
// 					data.dp = 1;

				})
				console.log(json)
				
				var dataSource = new kendo.data.DataSource({
					data : json,
					autoSync: true,
		            schema: {
		                model: {
		                	id: "id",
		                	fields: {
		                	measurer: { editable: false, nullable: true },
// 		                    cvt1: { editable: true, type : "number"},
// 		                    cvt2: { editable: true, type : "number"},
		                  }
		                }
		            }
				})
				
				
// 				zoomView(gridId,json[0]);
				console.log("#"+gridId);
				$("#"+gridId).data("kendoGrid").setDataSource(dataSource)
				
				var grid = $("#" + gridId).data("kendoGrid");
				grid.select("tr:eq(0)");
				
// 				var grid = $(".grid").data("kendoGrid");
// 				grid.autoFitColumn("spec");
				
			}
		})
		
	}
	
	//초기화 버튼
	function resetFnc(){
		getGroupMenu();
	}
	
	//INPUT 넣기 데이터 값넣기
	// 클릭시 수량 집어넣기
	function inputCnt(){
		console.log("넣자")
		var cnt = Number($("#five").val() + $("#four").val() + "." +$("#three").val() + $("#two").val() + $("#one").val() );
		
		var kendotable = $("#"+showId).data("kendoGrid");
		var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
		var dataItem = kendotable.dataItem(row)
// 		console.log("====넣기====")
// 		console.log("showId :" + showId)
// 		console.log("uid :" + uid)
// 		console.log(kendotable)
// 		console.log(dataItem)
// 		console.log("넣을값 :" + cnt)
// 		console.log("row :" + dataItem.cvt1)
		eval("dataItem."+columnId+"="+cnt);
		
		kendotable.dataSource.fetch();
		
		$("#keyPadDiv").css({
			"display":"none",
		})
		
		tCons.className = "";
	}
	
	//화살표 버튼 클릭스 플러스하기
	//젤 마지막 자리수 올리기
	function plusCvt(){
		//현재 입력값
		var cntVal = $("#inputCntKIOSK").val();
		//현재 소수점
		var cntDecimal = $("#inputCntKIOSK").attr("name");
		//결과값 저장하기
		var resultCnt = 0;

		//plus 하기
		resultCnt = Number(cntVal) +  (1 / Math.pow(10,cntDecimal));
		resultCnt = resultCnt.toFixed(cntDecimal);
		
		//결과값 넣기
		$("#inputCntKIOSK").val(resultCnt)

	}
	
	//화살표 버튼 클릭시 마이너스
	//젤 마지막 자리수 올리기
	function minusCvt(){
		//현재 입력값
		var cntVal = $("#inputCntKIOSK").val();
		//현재 소수점
		var cntDecimal = $("#inputCntKIOSK").attr("name");
		//결과값 저장하기
		var resultCnt = 0;

		//plus 하기
		resultCnt = Number(cntVal) -  (1 / Math.pow(10,cntDecimal));
		resultCnt = resultCnt.toFixed(cntDecimal);
		
		//결과값 넣기
		$("#inputCntKIOSK").val(resultCnt)
	}
	
	//값 입력하기
	function inputCvt(){
		var cnt = $("#inputCntKIOSK").val();
		
		var kendotable = $("#"+showId).data("kendoGrid");
		var row = kendotable.tbody.find("tr[data-uid='" + uid + "']");
		var dataItem = kendotable.dataItem(row)
// 		console.log("====넣기====")
// 		console.log("showId :" + showId)
// 		console.log("uid :" + uid)
// 		console.log(kendotable)
// 		console.log(dataItem)
// 		console.log("넣을값 :" + cnt)
// 		console.log("row :" + dataItem.cvt1)
		eval("dataItem."+columnId+"="+cnt);
		
		kendotable.dataSource.fetch();
		
		$("#keyPadDiv").css({
			"display":"none",
		})
		
		tCons.className = "";
		
	}
	// 화살표 버튼 클릭시 플러스
	function plusCnt(num){
		console.log(num)
		if(num==0.001){
			// 9 보다 클 경우에는 증가 X
			if($("#one").val()>=9){
				return;
			}
			$("#one").val(Number($("#one").val())+1)
		}else if(num==0.01){
			// 9 보다 클 경우에는 증가 X
			if($("#two").val()>=9){
				return;
			}
			$("#two").val(Number($("#two").val())+1)
		}else if(num==0.1){
			// 9 보다 클 경우에는 증가 X
			if($("#three").val()>=9){
				return;
			}
			$("#three").val(Number($("#three").val())+1)
		}else if(num==1){
			// 9 보다 클 경우에는 증가 X
			if($("#four").val()>=9){
				return;
			}
			$("#four").val(Number($("#four").val())+1)
		}else if(num==10){
			// 9 보다 클 경우에는 증가 X
			$("#five").val(Number($("#five").val())+1)
		}
	}
	
	// 화살표 버튼 클릭시 마이너스
	function minusCnt(num){
		console.log(num)
		if(num==0.001){
			// 9 보다 클 경우에는 증가 X
			if($("#one").val()<=0){
				return;
			}
			$("#one").val(Number($("#one").val())-1)
		}else if(num==0.01){
			// 9 보다 클 경우에는 증가 X
			if($("#two").val()<=0){
				return;
			}
			$("#two").val(Number($("#two").val())-1)
		}else if(num==0.1){
			// 9 보다 클 경우에는 증가 X
			if($("#three").val()<=0){
				return;
			}
			$("#three").val(Number($("#three").val())-1)
		}else if(num==1){
			// 9 보다 클 경우에는 증가 X
			if($("#four").val()<=0){
				return;
			}
			$("#four").val(Number($("#four").val())-1)
		}else if(num==10){
			// 9 보다 클 경우에는 증가 X
			if($("#five").val()<=0){
				return;
			}
			$("#five").val(Number($("#five").val())-1)
		}
	}
	
	

	function saveRow(){

		//tab 갯수 구하기
		var size = $(".grid").length;
		//저장할 리스트
		var savelist = [];
		
		for(i = 0; i<size; i++){
			var gridId = $(".grid")[i].id;
			
			var dataList = $("#"+gridId).data("kendoGrid").dataSource.data();
			
			$(dataList).each(function(idx,data){
				
				console.log(idx + " , " + savelist.length + " : " + data.id)
				data.id=data.id;
				data.dvcId=data.dvcId;
				data.attrNameKo=encodeURIComponent(data.attrNameKo);
				data.chkCycle = $("#checkCycle").val()
// 				data.work = empCd;
				data.checker = empCd;
				data.date = $("#date").val();
				data.workTy = Number($("#workTime").val())
				savelist.push(data)

// 				if($("#workTime").val()==1){
// 					data.workTy=1;
// 				}else if($("#workTime").val()==2){
// 					data.workTy=2;
// 				}
				

			})
		}
		
		if(savelist.length==0){
			alert("자주검사할 항목을 선택해 주세요");
			return;
		}
		
		var obj = new Object();
		obj.val = savelist;

// 		return
		var url = "${ctxPath}/chart/addCheckStandardList.do";
		var param = "val=" + JSON.stringify(obj);
		
		console.log(savelist)
		console.log(param)
		
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				$.hideLoading()
				if(data=="success") {
					alert ("저장이 완료z되었습니다.");
					resetFnc();
// 					pageMove();
				}else{
					alert("관리자에게 문의하세요_31")
				}
			}
		});
	}
	
	// 벽돌 행
	var brickRowCount = 3;
	// 벽돌 열
	var brickColumnCount = 5;
	// 벽돌 길이
	var brickWidth = 20;
	// 벽돌 높이
	var brickHeight = 20;
	// 벽돌 사이 간격
	var brickPadding = 10;
	// 벽돌 스타트 위치?
	var brickOffsetTop = 30;
	var brickOffsetLeft = 30;
	
	function drawCanvas(){
		var canvas = document.getElementById("canvas");
		var ctx = canvas.getContext("2d");
		
		var bricks = [];
		// c = 열
		// r = 행
		// 벽돌위치  x,y
		for(var c=0; c<brickColumnCount; c++) {
		    bricks[c] = [];
		    for(var r=0; r<brickRowCount; r++) {
		        bricks[c][r] = { x: 0, y: 0 };
		    }
		}
		
		for(var c=0; c<brickColumnCount; c++) {
	        for(var r=0; r<brickRowCount; r++) {
	            var brickX = (c*(brickWidth+brickPadding))+brickOffsetLeft;
	            var brickY = (r*(brickHeight+brickPadding))+brickOffsetTop;
	            bricks[c][r].x = brickX;
	            bricks[c][r].y = brickY;
	            ctx.beginPath();
	            ctx.rect(brickX, brickY, brickWidth, brickHeight);
	            ctx.fillStyle = "red";
	            ctx.fill();
	            ctx.closePath();
	        }
	    }
	}
	
</script>

</head>
<body>
    <div id="header">
			<img id="logo" src="${ctxPath}/images/FL/logo.png" onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="userId"></div>
			<img id="logout" src="${ctxPath}/images/FL/logout.png"  onclick="location.href='${ctxPath}/pop/popIndex.do'"/>
			<div id="headTitle" onclick="pageMove()">
				<span id='backIcon' class="k-icon k-i-undo"></span>
				4-1 자주검사
			</div>
			
			<img id="logo2" src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
			<div id="time"></div>
<%-- 		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
		</div> --%>
		<!-- <div id="rightH">
			<span id="time"></span>
		</div>
		<div id="aside">
		</div> -->
	</div>
	<div id="content" style="position: absolute;">
		<div style="width: 19%; height: 100%; float: left;">
						
	
			<div id="subShow">
				<div class="title_Form" style="height: 8%">
					<div>
						<button id = "resetBtn" onclick = "resetFnc()"> Reset </button>
					</div>
				</div>
				
				<div class="title_Form" style="height: 8%">
					<div>
						품번
					</div>
				</div>

				<div class="value_Form" style="height: 8%">
					<div>
						<input type="text" id = "prdNo">
					</div>
				</div>

				<div class="title_Form" style="height: 8%">
					<div>
						날짜
					</div>
				</div>
				
				<div class="value_Form" style="height: 8%">
					<div>
						<input type="text" id = "date" class="date">
					</div>
				</div>
				
				<div class="title_Form" style="height: 8%">
					<div>
						검사주기
					</div>
				</div>
				
				<div id="prd_Form" class="value_Form" style="height: 8%">
					<div>
						<div class="article" style="width: 50%;">
<!-- 							<input type="text" value="PRDJOP"> -->
							<select id="checkCycle" class="textSize"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
						</div>
						<div class="article" style="width: 50%;">
							<select id="workTime" class="textSize" ><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
<!-- 							<input type="text" value="123d"> -->
						</div>
					</div>
				</div>
				
<!-- 				공백 -->
				<div class="title_Form" style="height: 35%">
					<div>
						　
					</div>
				</div>
				
				<div class="title_Form" style="height: 8%">
					<div>
						<button id = "saveBtn" onclick = "saveRow()"> Save </button>
					</div>
				</div>
				
			</div>
		</div>
		<div id = "viewTotal" style="width: 81%; height: 90%; float: left; overflow: auto;">
			<div id = "viewMain" >
				
	    	</div>
	    	<div id = "viewSub" ><!-- class="flex-container" -->
	    	
	    	</div>
		</div>
		
<!--         <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
		<div id="aside"><span></span></div>         -->
	</div>
	
	
	<!-- 	    생산실적 수량을 입력하기 위한 키패드 화면 -->
	<div id="keyPadDiv" class="keyPad" style="opacity: 1; position: absolute; top: 0; display: none;">
		<table border="1" class="keyPad" style="width: 100%;height: 100%;text-align: center;">
			<tr class="keyPad">
				<td class="keyPad" style="font-size: 300%; color: red; width: 70%;" onclick="plusCvt()">
					▲
				</td>
				<td id="okCss" class="keyPad" rowspan="3" onclick="inputCvt()" style="width: 30%; font-size: 300%; border: 0; border-radius: 4px; cursor: pointer;">
					OK	
				</td>
			</tr>
			<div id="test">
			<tr class="keyPad">
				<td class="keyPad">
					<input type="text" id = "inputCntKIOSK" class="keyPad" style="width: 90%; height: 90%; font-size: 250%; text-align: center;">
				</td>
			</tr>
			
			<tr class="keyPad">
				<td class="keyPad" style="font-size: 300%; color: blue;" onclick="minusCvt()">
					▼
				</td>
			</tr>
			</div>
		</table>
		<!-- <table border="1" class="keyPad" style="width: 100%; height: 100%;text-align: center;">
			<tr class="keyPad">
				<td class="keyPad" onclick="plusCnt(10)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(1)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(0)" style="font-size: 300%; color: red;">
					.
				</td>
				<td class="keyPad" onclick="plusCnt(0.1)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(0.01)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" onclick="plusCnt(0.001)" style="font-size: 300%; color: red;">
					▲
				</td>
				<td class="keyPad" rowspan="3" style="font-size: 300%;" onclick="inputCnt()">
					INPUT	
				</td>
			</tr>
			<tr class="keyPad">
				<td class="keyPad">
					<input type="number" id="five" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="four" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					.
				</td>
				<td class="keyPad">
					<input type="number" id="three" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="two" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%;" readonly="readonly">
				</td>
				<td class="keyPad">
					<input type="number" id="one" class="keyPad" style="height: 80%; width: 80%; text-align: center; font-size: 200%; " readonly="readonly">
				</td>
			</tr>
			<tr class="keyPad">
				<td class="keyPad" onclick="minusCnt(10)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(1)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(0)" style="font-size: 300%; color: blue;">
					.
				</td>
				<td class="keyPad" onclick="minusCnt(0.1)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(0.01)" style="font-size: 300%; color: blue;">
					▼
				</td>
				<td class="keyPad" onclick="minusCnt(0.001)" style="font-size: 300%; color: blue;">
					▼
				</td>
			</tr>
		</table> -->
	</div>


</body>
</html>