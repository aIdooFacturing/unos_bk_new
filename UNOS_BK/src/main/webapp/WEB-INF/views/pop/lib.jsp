<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<link rel="stylesheet" href="${ctxPath }/css/jquery-ui.css" />
<link rel="stylesheet" href="${ctxPath }/css/loading.min.css" />

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
</head>


<style>
body{
	overflow: hidden;
}
.editable-cell{
	background: gainsboro;
}

.editable-cell-alt {
	background: darkgray;
}
</style>
<script type="text/javascript">

/* 	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight; */

	var nm = '<%=(String)session.getAttribute("nm")%>';
	nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	var shopId = 1;
	
	var ip;
	var popId;
	var line;
	var test;
	$(function(){
		
		$.datepicker.setDefaults({
		    dateFormat: 'yy-mm-dd',
		    prevText: '이전 달',
		    nextText: '다음 달',
		    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		    showMonthAfterYear: true,
		    yearSuffix: '년'
		});
		
		//전체적인 기본 CSS
		defulatCss();
		
		//POP 명 가져오기
		popLineChk();

		//공지사항 가지고오기
		noticeChk();
		// 5분 마다 SESSION 로그인 체크
		setInterval(function() {
			sessionChkCommon() 
		}, 300000);
	})
	
	function defulatCss(){
//		console.log($("#header").height())
		// 제일 마지막으로 CSS적용 시키기 위해
		setTimeout(function() {
			
			$("#noticeMsg").css({
				"float" : "left",
				"top" : $("#header").height(),
				"height" : originHeight * 0.11,
				"width" : originWidth,
//				"background" : "red"
			})
			
			$("#backIcon").css({
				"font-size": $("#logo2").height() *  0.7
			})
			
			 var scroll_css = document.createElement('style')
		scroll_css.innerHTML = 
			"::-webkit-scrollbar {" +
				"background: white;" +
				"width: " + getElSize(62 * 2) + "px;"+
				"height: " + getElSize(50 * 2) + "px;"+
			"}" +
			
			"::-webkit-scrollbar-track-piece {" +
		  		"background-color:rgba(0,0,0,0);" +
			"}" +

			"::-webkit-scrollbar-thumb {" +
		  		"height :" + getElSize(100 * 2) + ";" +
				"background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;" +
			"}" +
			
			"::-webkit-scrollbar-thumb:ACTIVE {" +
	  		"height :" + getElSize(100 * 2) + ";" +
				"background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;" +
			"}" +
			
			"::-webkit-scrollbar-button:start:decrement {" +
				"display: block;" +
				"height: " +getElSize(62 * 2) +"px;" +
			    "background: url(${ctxPath}/images/FL/btn_scroll_up_pushed.svg) ;" +
				"background-size: 100% 100%" +
			"}" +
			
			"::-webkit-scrollbar-button:start:decrement:ACTIVE{" +
				"display: block;" +
				"height: " + getElSize(62 * 2) + "px;" +
				"background: url(${ctxPath}/images/FL/btn_scroll_up_default.svg) ;" +
			    "background-size: 100% 100%}" +
		    
		    "::-webkit-scrollbar-button:end:increment {" +
				"display: block;" +
				"height: " + getElSize(62 * 2) + "px;" +
		    	"background: url(${ctxPath}/images/FL/btn_scroll_down_pushed.svg) ;" +
		    	"background-size: 100% 100%" +
		 	"}" +
		 	
		 	"::-webkit-scrollbar-button:end:increment:ACTIVE {" +
				"display: block;" +
				"height: " +  getElSize(62 * 2) + "px;" +
		    	"background: url(${ctxPath}/images/FL/btn_scroll_down_default.svg) ;" +
		    	"background-size: 100% 100%" +
		 	"}" +
		 	
		 	
            " ::-webkit-scrollbar-button:start:horizontal:decrement {" +
                "display: block;" +
                "height: " + getElSize(62 * 2) + "px;" +
                "width: " + getElSize(62 * 2) + "px;" +
				"background : url(${ctxPath}/images/FL/btn_scroll_left_default.svg);" +
                "background-size: 100% 100%" +
           " }" +
			    
            "::-webkit-scrollbar-button:start:horizontal:decrement:ACTIVE{" +
                "display: block;" +
                "height: " + getElSize(62 * 2) + "px;" +
                "width: " + getElSize(62 * 2) + "px;" +
				"background : url(${ctxPath}/images/FL/btn_scroll_left_pushed.svg);" +
                "background-size: 100% 100%" +
            "}" +
             
             
             "::-webkit-scrollbar-button:end:horizontal:increment {" +
                "display: block;" +
                "height: " + getElSize(62 * 2) + "px;" +
                "width: " + getElSize(62 * 2) + "px;" +
				"background : url(${ctxPath}/images/FL/btn_scroll_right_default.svg);" +
                "background-size: 100% 100%" +
             "}" +
                
             "::-webkit-scrollbar-button:end:horizontal:increment:ACTIVE {" +
                "display: block;" +
                "height: " + getElSize(62 * 2) + "px;" +
                "width: " + getElSize(62 * 2) + "px;" +
				"background : url(${ctxPath}/images/FL/btn_scroll_right_pushed.svg);" +
                "background-size: 100% 100%" +
             "}"
             
//              console.log(scroll_css)
			document.body.appendChild(scroll_css)
			
		}, 1);
	}


	//사용자 session Chk 
	function sessionChkCommon(){
		nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
		empCd = '<%=(String)session.getAttribute("empCd")%>';
		console.log(empCd)
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//라인 글자 CSS 적용시키기
	function lineCss(){
		$("#popName").css({
//			"top" : originHeight,
//			"left" : originWidth,
			"color" : "black",
			"font-size" : originHeight * 0.05,
//		    "font-weight": "bold",
	        "font-family": "fantasy"
		})
		
		setTimeout(function() {
			$("#popName").css({
				"top" : originHeight - $("#popName").height() * 1.20,
				"left" : originWidth * 0.015
//				"left" : originWidth - $("#popName").width() * 1.15
			})
		}, 1);
		
	}
	
	//공지사항 확인하기
	function noticeChk(){
		
		chk = false;
		text = "나는 공지사항이다. 공지사항 공지사항";
		color = "rgb(245, 190, 220) none repeat scroll 0% 0% / auto padding-box border-box";
		if(chk){
			$(".noticetext").html(text);
			
			$(".noticetext").css({
				"font-size" : $("#logo2").height() *  0.8
				,"background" : color
			})
		}
	}
	//pop 장비 확인하기
	function popLineChk(){
		

		var url = "${ctxPath}/pop/popIpChk.do";
				
		var param ;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				private_ip=data;

                var url = "${ctxPath}/pop/popLineChk.do";
				var param = "ip=" + private_ip;
                

                $.ajax({
                    url : url,
                    data : param,
                    type : "post",
                    dataType : "json",
                    success : function(data){
                    	popIp = data.ip;
        				popId = data.popId;
        				line = data.line;
//         				console.log("===장비 정보 확인하기===");
//         				console.log("컴퓨터 IP : " + private_ip);
//         				console.log("DB 매칭 IP : " + ip);
//         				console.log("LINE 명 : " + decode(line));
//         				console.log("popID : " + popId);
//         				console.log(data);
//         				console.log("======== E N D =========");
        				
        				test = data.pregDate;
        				
        				if(line==null || line==undefined){
	        				$("#popName").html("KIOSK 등록 필요");
        				}else{
	        				$("#popName").html(popId + ". " + decode(line));
        				}
        				//popName Css
        				lineCss();
        				
        				ready();
                    },error:function(request,status,error){
                        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
                    }

                })
			}
		})
		
	   /*  var head = document.getElementsByTagName('head')[0];
	    var script= document.createElement('script');

	    console.log(head)
	    console.log(script)
	    
	    window.getIP = function(json) {
	    	//외부 IP
	        public_ip = json.ip;
			console.log('11. 외부 ip : ' + public_ip);

	        //내부(사설 IP) 가져오기
	        getPrivateIP(function(ip){
	        	console.log("33. 내부 ip :" + ip);

	            private_ip = ip;

	           
                var url = "${ctxPath}/pop/popLineChk.do";
				var param = "ip=" + private_ip;
                

                console.log(param);
                $.ajax({
                    url : url,
                    data : param,
                    type : "post",
                    dataType : "json",
                    success : function(data){
                    	console.log("44. " + data.ip);
                    	popIp = data.ip;
        				popId = data.popId;
        				line = data.line;
        				console.log("===장비 정보 확인하기===");
        				console.log("컴퓨터 IP : " + private_ip);
        				console.log("DB 매칭 IP : " + ip);
        				console.log("LINE 명 : " + decode(line));
        				console.log("popID : " + popId);
        				console.log(data);
        				console.log("======== E N D =========");
        				
        				test = data.pregDate;
        				
        				if(line==null || line==undefined){
	        				$("#popName").html("KIOSK 등록 필요");
        				}else{
	        				$("#popName").html(popId + ". " + decode(line));
        				}
        				//popName Css
        				lineCss();
        				
        				ready();
                    },error:function(request,status,error){
                        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
                    }

                })

	        });
	    };

	    script.type= 'text/javascript';
	    script.src= 'https://api.ipify.org?format=jsonp&callback=getIP';
	    head.appendChild(script);
 */
		
		
		
		
		/* console.log("chk")
// popLineChk		

		
 		var url = "${ctxPath}/pop/popLineChk.do";
		
		$.ajax({
			url : url,
//			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				ip = data.ip;
				popId = data.popId;
				line = data.line;
				
				test = data.pregDate;
				
				$("#popName").html(decode(line))
				//popName Css
				lineCss();
				console.log(data.ip)
				console.log(data.popId)
				console.log(data.line)
			},error : function(data){
				alert("err")
			}
		})  */
	}
	
	function getPrivateIP(onNewIP) { //  onNewIp - your listener function for new IPs
	    //compatibility for firefox and chrome
	    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
// 		alert("2-1" + myPeerConnection)
	    var pc = new myPeerConnection({
	            iceServers: []
	        }),
	        noop = function() {},
	        localIPs = {},
	        ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
	        key;


	    function iterateIP(ip) {
	    	console.log("000")
	    	console.log(localIPs[ip])
	        if (!localIPs[ip]) onNewIP(ip);
	        localIPs[ip] = true;
	    }


// 	    alert("2-2" + iterateIP)
	    //create a bogus data channel
	    pc.createDataChannel("");

	    // create offer and set local description
	    pc.createOffer().then(function(sdp) {
	        sdp.sdp.split('\n').forEach(function(line) {
// 	        	alert("2-3" + line.indexOf('candidate'))
	            if (line.indexOf('candidate') < 0) return;
	            line.match(ipRegex).forEach(iterateIP);
	        });

	        pc.setLocalDescription(sdp, noop, noop);
	    }).catch(function(reason) {
	        // An error occurred, so handle the failure to connect
	    });


	    //listen for candidate events
	    console.log(pc.onicecandidate)
	    pc.onicecandidate = function(ice) {
 	    	console.log("2-4.")
 	    	console.log(ice)
 	    	console.log(ice.candidate)
 	    	console.log(ice.candidate.candidate)
 	    	console.log(ice.candidate.candidate.match(ipRegex))
 	    	
 	    	
 	    	

	        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
	        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
	    };

	}
	
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
</script>
<body>

	<div id="noticeMsg" style="position: absolute;">
		<table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
			<tr>
				<td class="noticetext">
				</td>
			</tr>
		</table>
	</div>

<!-- marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span> -->
<!-- 	아래 왼쪽에 키오스크 라인명 영역 -->
	<div id="popName" style="position: absolute;">
		
	</div>
</body>
</html>