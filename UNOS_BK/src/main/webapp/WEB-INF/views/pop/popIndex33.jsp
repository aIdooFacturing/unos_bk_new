<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>

<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>
<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/lib/selection/classie.js"></script>
<script src="${ctxPath }/js/lib/selection/selectFx.js"></script>
<script src="${ctxPath }/js/lib/velocity.js"></script>        
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/default1.js"></script>
<script src="${ctxPath }/js/index.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/lib/selection/unos_selection.css">
<link rel="stylesheet" href="${ctxPath }/css/default1.css">

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>POP</title>
    </head>
    <style>
    #backBtn{
	
	margin-left : 80%;
	margin-top : -7.5%;
}
#aside{
	background: green;
	width: 100%;
	height: 15%;
	display: table;
	background: darkorchid;
	color:indigo;
}
    </style>
    <script>
	var empCd;
	var evtMsg;
	
	$(function(){
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		<% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			System.out.println("cmpCd:"+session.getAttribute("empCd"));
		%>
		//focus TEXT 맞추기
		var chk_short = true;
	
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
				//시간
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
		//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){
			getTable()
		}
	}
	//사원 바코드를 입력해주세요
	function alarmMsg(){		
		clearTimeout(evtMsg)
		
		return evtMsg = setTimeout(function() {$("#aside").html("<span>바코드를 입력해주세요</span>")}, 10000)
//		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span>")}, 10000)
	}
	function getTable(){
		if($("#empCd").val()==0){
			return;
		}
		$("#empCd").focus();
		$("#empCd").select();		
		empCd=$("#empCd").val();
	
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(empCd.substring(0,4)!="1111"){
			$("#aside").html("<span style='color:red;'>바코드를 다시 확인해 주세요</span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		empCd = empCd.substring(4,empCd.length);
		
		var url = "${ctxPath}/pop/popLoginChk.do";
		var param = "empCd=" + empCd ;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				alert("su")
				console.log("aswwdasd")
				console.log(data);
				

				var json = data.dataList;
//				location.href="${ctxPath}/pop/popMainMenu.do?empCd=" + empCd
				
			},error : function(request,status,error){
				alert("er")
				console.log("err");
				if(request.responseText=="no"){

					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
				}else{
	
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
					
				}
				$.hideLoading(); 
				 alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);


			}
		})
	}
    </script>
    <body>
    <div id="header">
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<img src="http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png"/>
		</div>
	<div id="rightH">
			<span id="time"></span>
		</div>
	<div id="aside">
	</div>
        <input id="empCd" class="unos_input"onkeyup="enterEvt(event)" style="display: none;" placeholder="바코드를 입력해 주세요.">
    </body>
    <div id="content">
    <div id="jobList">
    </div>
</html>