

$(function(){
	main();
});
	function main() {
	  // Get A WebGL context
	  var canvas = document.getElementById("rGaugeCanvas1");
	  var gl = getWebGLContext(canvas);
	  if (!gl) {
	    return;
	  }
	  // setup GLSL program
	  vertexShader = createShaderFromScriptElement(gl, "2d-vertex-shader");
	  fragmentShader = createShaderFromScriptElement(gl, "2d-fragment-shader");
	  program = createProgram(gl, [vertexShader, fragmentShader]);
	  gl.useProgram(program);

	  // look up where the vertex data needs to go.
	  var positionLocation = gl.getAttribLocation(program, "a_position");

	  // lookup uniforms
	  var resolutionLocation = gl.getUniformLocation(program, "u_resolution");
	  var colorLocation = gl.getUniformLocation(program, "u_color");
	  var matrixLocation = gl.getUniformLocation(program, "u_matrix");

	  // set the resolution
	  gl.uniform2f(resolutionLocation, canvas.width, canvas.height);

	  // Create a buffer.
	  var gaugeBuffer = gl.createBuffer();
	  gl.bindBuffer(gl.ARRAY_BUFFER, gaugeBuffer);
	  gl.enableVertexAttribArray(positionLocation);
	  gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);
	  
	  var gaugeBuffer = gl.createBuffer();
	  gl.bindBuffer(gl.ARRAY_BUFFER, gaugeBuffer);
	  gl.enableVertexAttribArray(positionLocation);
	  gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);

	  // Set Geometry.
	  setGeometry(gl);

	  // Set a random color.
	  //gl.uniform4f(colorLocation, Math.random(), Math.random(), Math.random(), 1);
	  gl.uniform4f(colorLocation, 0, 255, 255, 1);

	  var translation = [170, 170];
	  var angleInRadians = 150;
	  var scale = [1, 1];

	  drawScene();

	  // Setup a ui.
	  $("#x").gmanSlider({value: translation[0], slide: updatePosition(0), max: canvas.width });
	  $("#y").gmanSlider({value: translation[1], slide: updatePosition(1), max: canvas.height});
	  $("#angle").gmanSlider({value: angleInRadians, slide: updateAngle, max: 360});
	  $("#scaleX").gmanSlider({value: scale[0], slide: updateScale(0), min: -5, max: 5, step: 0.01, precision: 2});
	  $("#scaleY").gmanSlider({value: scale[1], slide: updateScale(1), min: -5, max: 5, step: 0.01, precision: 2});

	  function updatePosition(index) {
	    return function(event, ui) {
	      translation[index] = ui.value;
	      drawScene();
	    }
	  }

	  function updateAngle(event, ui) {
	    var angleInDegrees = 360 - ui.value;
	    angleInRadians = angleInDegrees * Math.PI / 180;
	    drawScene();
	  }

	  function updateScale(index) {
	    return function(event, ui) {
	      scale[index] = ui.value;
	      drawScene();
	    }
	  }

	  // Draw the scene.
	  function drawScene() {
	    // Clear the canvas.
	    gl.clear(gl.COLOR_BUFFER_BIT);

	    // Compute the matrices
	    var moveOriginMatrix = makeTranslation(-7, 0);
	    var translationMatrix = makeTranslation(translation[0], translation[1]);
	    var rotationMatrix = makeRotation(angleInRadians);
	    var scaleMatrix = makeScale(scale[0], scale[1]);

	    // Multiply the matrices.
	    var matrix = matrixMultiply(moveOriginMatrix, scaleMatrix);
	    matrix = matrixMultiply(matrix, rotationMatrix);
	    matrix = matrixMultiply(matrix, translationMatrix);

	    // Set the matrix.
	    gl.uniformMatrix3fv(matrixLocation, false, matrix);

	    // Draw the geometry.
	    gl.drawArrays(gl.TRIANGLES, 0, 3);
	  }
	}

	function makeTranslation(tx, ty) {
	  return [
	    1, 0, 0,
	    0, 1, 0,
	    tx, ty, 1
	  ];
	}

	function makeRotation(angleInRadians) {
	  var c = Math.cos(angleInRadians);
	  var s = Math.sin(angleInRadians);
	  return [
	    c,-s, 0,
	    s, c, 0,
	    0, 0, 1
	  ];
	}

	function makeScale(sx, sy) {
	  return [
	    sx, 0, 0,
	    0, sy, 0,
	    0, 0, 1
	  ];
	}

	function matrixMultiply(a, b) {
	  var a00 = a[0*3+0];
	  var a01 = a[0*3+1];
	  var a02 = a[0*3+2];
	  var a10 = a[1*3+0];
	  var a11 = a[1*3+1];
	  var a12 = a[1*3+2];
	  var a20 = a[2*3+0];
	  var a21 = a[2*3+1];
	  var a22 = a[2*3+2];
	  var b00 = b[0*3+0];
	  var b01 = b[0*3+1];
	  var b02 = b[0*3+2];
	  var b10 = b[1*3+0];
	  var b11 = b[1*3+1];
	  var b12 = b[1*3+2];
	  var b20 = b[2*3+0];
	  var b21 = b[2*3+1];
	  var b22 = b[2*3+2];
	  return [a00 * b00 + a01 * b10 + a02 * b20,
	          a00 * b01 + a01 * b11 + a02 * b21,
	          a00 * b02 + a01 * b12 + a02 * b22,
	          a10 * b00 + a11 * b10 + a12 * b20,
	          a10 * b01 + a11 * b11 + a12 * b21,
	          a10 * b02 + a11 * b12 + a12 * b22,
	          a20 * b00 + a21 * b10 + a22 * b20,
	          a20 * b01 + a21 * b11 + a22 * b21,
	          a20 * b02 + a21 * b12 + a22 * b22];
	}

	// Fill the buffer with the values that define a letter 'F'.
	function setGeometry(gl) {
	  gl.bufferData(
	      gl.ARRAY_BUFFER,
	      new Float32Array([
	          // left column
	          0, 0,
	          14, 0,
	          7, 170,

	          ]),
	      gl.STATIC_DRAW);
	}

	function setImage(imgURL){
		var canvas = document.getElementById('rGaugeCanvas2');
	    var ctx = canvas.getContext('2d');
	    var x = 30;
	    var y = 30;
	    var width = 200;
	    var height = 200;
	    var imageObj = new Image();

	    imageObj.onload = function() {
			ctx.drawImage(imageObj, x, y, width, height);
	    };
	    //imageObj.src = 'http://www.html5canvastutorials.com/demos/assets/darth-vader.jpg';
	    imageObj.src = imgURL;
	}