let dvcId = window.localStorage.getItem("dvcId");
let dvcName = "";

if(dvcId==null) dvcId = 114;
if(dvcName==null) {
	dvcName = decodeURIComponent("EPB%2FR%2015%20%2F16%20R%231")
}else{
	dvcName = decodeURIComponent(window.localStorage.getItem("name"))
}




const loadPage = () =>{
	createMenuTree("monitoring", "Single_Chart_Status")
	createMachineInfo()
	createBox()
	createCircleChartDiv()
	createAlarmDiv()
	createBarChartDiv()
	
	getDetailData()
	getCurrentDvcStatus(dvcId);
}
	
const getCurrentDvcStatus = (dvcId) =>{
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	var today = year + "-" + month + "-" + day;
	
	var url = ctxPath + "/chart/getCurrentDvcData.do";
	var param = "dvcId=" + dvcId + 
				"&workDate=" +  $("#date").html().trim().replace(/\./gi, "-")
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var chartStatus = data.chartStatus;
			var type = data.type;
			var progName = data.lastProgramName;
			var progHeader = data.lastProgramHeader;
			var name = data.name;
			var inCycleTime = data.inCycleTime;
			var cuttingTime = data.cuttingTime;
			var waitTime = data.waitTime;
			var alarmTime = data.alarmTime;
			var noConTime = data.noConnectionTime;
			var spdLoad = data.spdLoad;
			var feedOverride = data.feedOverride;
			var opRatio = data.opRatio;
			var cuttingRatio = data.cuttingRatio;
			var alarm = data.alarm;
			
			if(data.chartStatus=="null"){
				name = window.sessionStorage.getItem("name");
			}
			
			
			//spd, feed
			$("#opratio_gauge").empty();
			$("#opratio_gauge").circleDiagram({
				textSize: getElSize(25 * 2), // text color
				percent : Math.round(opRatio) + "%",
				size: getElSize(152 * 2), // graph size
				borderWidth: getElSize(5 * 2), // border width
				bgFill: "black", // background color
				frFill: "00C6FF", // foreground color
				//font: "serif", // font
				textColor: '#00C6FF' // text color
			});
			
			$("#cutting_gauge").empty();
			$("#cutting_gauge").circleDiagram({
				textSize: getElSize(25 * 2), // text color
				percent : Math.round(cuttingRatio) + "%",
				size: getElSize(152 * 2), // graph size
				borderWidth: getElSize(5 * 2), // border width
				bgFill: "black", // background color
				frFill: "00C6FF", // foreground color
				//font: "serif", // font
				textColor: '#00C6FF' // text color
			});
					
			$("#cutting_gauge, #opratio_gauge").css("position" ,"absolute")
			
			$("#pgmInfo").html(`${dvcName}</span> / <span style="color:white">[${progHeader}] ${progName}</span`)
			
			if(String(type).indexOf("IO")!=-1){
				//$("#progHeader" + idx).append("I/O Logik");
			};
			
			//var pieChart = $("#pie" + idx).highcharts();
			
			//console.log(idx)
			//var a = (24*60*60) - (inCycleTime + waitTime+ alarmTime + noConTime);
			
			//drawPieData(chartStatus, inCycleTime - cuttingTime, cuttingTime, waitTime, alarmTime, noConTime ,a, pieChart);

			
			drawPieChart()
			
			/* var opTimeChart = $("#opTime" + idx).highcharts().series[0].points[0];
			opTimeChart.update(Number(Number(opRatio).toFixed(1)));
		
			var cuttingTime = $("#cuttingTime" + idx).highcharts().series[0].points[0];
			cuttingTime.update(Number(Number(cuttingRatio).toFixed(1))); */
			
			$("#incycleVal").html((inCycleTime/60/60).toFixed(1));
			$("#waitVal").html((waitTime/60/60).toFixed(1));
			$("#alarmVal").html((alarmTime/60/60).toFixed(1));
			$("#noConnVal").html((noConTime/60/60).toFixed(1));
			$("#cutVal").html((cuttingTime/60/60).toFixed(1));
			
			
			$("#workTimeVal, #hr").remove()
			const workTimeVal = 
				`
					<span id="workTimeVal"
						style=
							"
								position : absolute
								; z-index : 2
								; color : #00C6FF
								; font-size : ${getElSize(55 * 2)}px
								; top : ${getElSize(679 * 2) + marginHeight}px
								
							"
					>
						${Number((Number(inCycleTime) + Number(waitTime) + Number(alarmTime)) / 3600).toFixed(1)}
					</span>
				`
			
			$("#container").append(workTimeVal)
			
			$("#workTimeVal").css({
				"left" : $("#pieChart").offset().left + ($("#pieChart").width() / 2) - ($("#workTimeVal").width() / 2) 
			})
			
			const hr = 
				`
					<span id="hr"
						style=
							"
								position : absolute
								; z-index : 2
								; color : white
								; font-size : ${getElSize(18 * 2)}px
								; top : ${getElSize(713 * 2) + marginHeight}px
								
							"
					>
						hr
					</span>
				`
			
			$("#container").append(hr)
			
			$("#hr").css({
				"left" : $("#workTimeVal").offset().left +  $("#workTimeVal").width() + getElSize(4 + 2)  
			})
			
			
			pieChart.series[0].data[0].update(Number((inCycleTime/60/60).toFixed(1)))
			pieChart.series[0].data[1].update(Number((cuttingTime/60/60).toFixed(1)))
			pieChart.series[0].data[2].update(Number((waitTime/60/60).toFixed(1)))
			pieChart.series[0].data[3].update(Number(alarmTime/60/60))
			pieChart.series[0].data[4].update(Number((noConTime/60/60).toFixed(1)))
//			
//			if(inCycleTime==0 && waitTime==0 && alarmTime==0 && noConTime==0 ){
//				pieChart.series[0].data[4].update(1)
//			}
			
			setInterval(function(){
				getCurrentDvcStatus(dvcId)					
			}, 1000*60*10)
		}
	});
};

let pieChart;
const drawPieChart = () =>{
	/* incycleColor=10
	cuttingColor=7
	waitColor=5
	alarmColor=2
	noconnColor=1 */

	// Build the chart
    $('#pieChart').highcharts({
        chart: {
            plotBackgroundColor: null,
            width : getElSize(212 * 2),
            height : getElSize(212 * 2),
            plotBorderWidth: null,
            backgroundColor : "rgba(255,255,255,0)",
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: null
        },
        tooltip: {
			enabled :false,
        	pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        legend : {
        	enabled : false
        },
        exporting : false,
        credits : false,
        plotOptions: {
            pie: {
            	size: getElSize(200 * 2),
                innerSize: '90%',
                borderColor : "rgba(0,0,0,0)",
                	 
            	//borderWidth: 0,
                //allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'In-Cycle',
                y: 0,
                color : incycleColor
            },{
                name: 'Cut',
                y: 0,
                color : cutColor
            }, {
                name: 'Wait',
                y: 0,
                color : waitColor
            }, {
                name: 'Alarm',
                y: 0,
                color : alarmColor
            }, {
                name: 'Off',
                y: 0,
                color : noConnColor
            }]
        }]
    });
	  
    pieChart = $('#pieChart').highcharts()
    
//    $('#pieChart').css({
//    	"margin-top" : "18.82%",
//    	"margin-left" : "0.28%",
//			"z-index" : 1,
//	//	"opacity" : 0.8
//    })

};


const getDetailData = () =>{
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var date_ = addZero(String(date.getDate()));
	var day = date.getDay();
	
	var today = year + "-" + month + "-" + date_;
	
	var url = ctxPath + "/chart/getDetailBlockData.do";
	var param = "dvcId=" + dvcId + 
				"&sDate=" +  $("#date").html().trim().replace(/\./gi, "-")
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			console.log(param)
			console.log(json)
			var alarm = "";
			$(json).each(function(idx, data){
				var hour = new Date().getHours()-8;
				if(hour==0) hour=1;
				 
				var sign = "";
				
				prgmHeader = data.programName
				prgm = data.programName
				
				if(prgmHeader == null){
					prgmHeader = ""
				}
				if(prgm == null){
					prgm = ""
				}
				
				
				$("#targetCnt span:nth(1)").html(data.tgCnt);
				$("#prdcCnt span:nth(1)").html(data.lastFnPrdctNum);
				$("#cntPerCycle span:nth(1)").html(data.prdctPerCyl);
				$("#cycleCnt span:nth(1)").html(Math.round(data.lastFnPrdctNum/data.prdctPerCyl));
				$("#avrgCycleTime span:nth(1)").html(Number(data.LastAvrCycleTime/60).toFixed(1));
				$("#prdcCntPerHour span:nth(1)").html(Number(data.prdctPerHour).toFixed(1));
				$("#feedOvrrd span:nth(1)").html(Number(data.feedOverride));
				$("#spdLd span:nth(1)").html(Number(data.spdLoad));
				
			
				
				//일 생산 현황
				if(isNaN(data.lastFnPrdctNum / data.tgCnt * 100)){
					drawPrdctChart(0);
				}else if((data.lastFnPrdctNum / data.tgCnt * 100)==Infinity){
					drawPrdctChart(0);
				}else{

					drawPrdctChart(data.lastFnPrdctNum / data.tgCnt * 100);
				}
				//data.lastAlarmCode + " - " + data.lastAlarmMsg;
				var alarm1, alarm2, alarm3;
				if(data.ncAlarmNum1==""){
					alarm1 = "";
				}else{
					alarm1 = data.ncAlarmNum1 + " - " +  decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum2==""){
					alarm2 = "";
				}else{
					alarm2 = data.ncAlarmNum2 + " - " +  decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " ") + "<br>";
				};
				if(data.ncAlarmNum3==""){
					alarm3 = "";
				}else{
					alarm3 = data.ncAlarmNum3 + " - " +  decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " ") + "<br>";
				};
				
				
				alarm += alarm1 +
						alarm2 +
						alarm3; 
						
				if(data.status=="ALARM" && alarm == ""){
					//console.log("get")
					alarm = "[장비 확인 필요]";
				}
				
				if(alarm == ""){
					alarm = "알람 없음"
				}
				
				$("#alarm_div").html(alarm);	
				
				
			});
			
			
		
		}
	});
};

var startTimeLabel = new Array();
var startHour, startMinute;

const getStartTime = () =>{
	var url = ctxPath + "/chart/getStartTime.do";
	var param = "shopId=" + shopId;;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			startHour = data.split("-")[0]
			startMinute = data.split("-")[1]
			startHour = "08"
			
			//getTimeData();
				
			drawBarChart("timeChart");	
		}, error : function(e1,e2,e3){
			console.log(e1,e2,e3)
		}
	});	
};

var spdLoadPoint = [];
var spdOverridePoint = [];

const getSpdFeed = () => {
	$('#spdFeed').highcharts({
        chart: {
            type: 'line',
            backgroundColor : "rgba(0,0,0,0)",
            //height : getElSize(400),
        },
        exporting : false,
        credits : false,
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        legend : {
        	enabled : false,
        	itemStyle : {
        			color : "white",
        			fontWeight: 'bold'
        		}
        },
        xAxis: {
            //categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        	labels : {
        		enabled : false
        	}
        },
        yAxis: {
        	min : 0,
        	gridLineColor : "#404957",
//        	gridLineWidth: 0,
//			minorGridLineWidth: 0,
            title: {
                text: null
            },
            labels : {
            	style : {
            		color : "white",
            		fontSize : getElSize(12 * 2)
            	}
            }
        },
        plotOptions: {
            line: {
                dataLabels: {	
                    enabled: false,
                    color: 'white',
                    style: {
                        textShadow: false 
                    }
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Spindle Load',
            color : '#666BF0',
            //data: spdLoadPoint
            data : spdLoadPoint,
            lineWidth : getElSize(5),
            marker : {
                enabled : false,
            },
        }, {
            name: 'Spindle Override',
            color : '#00C6FF',
            data: spdOverridePoint,
            lineWidth : getElSize(5),
            marker : {
                enabled : false,
            },
        }]
    });	
	
	spdChart = $('#spdFeed').highcharts();
	//$("#legend").css("display", "inline");
};

let spdChart;
function drawBarChart(id) {
	var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + startMinute + "=" + j);
		
//		startTimeLabel.push(m0);
		startTimeLabel.push((j - 1) + ":30");
		startTimeLabel.push(m02);
		startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08);
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		startTimeLabel.push(m16);
		startTimeLabel.push(m18);
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28);
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		startTimeLabel.push(m36);
		startTimeLabel.push(m38);
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48);
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		startTimeLabel.push(m56);
		startTimeLabel.push(m58);
		
		if(j==24){ j = 0}
	};

	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};

	var height = window.innerHeight;

	
	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgba(0,0,0,0)',
			height : getElSize(150),
			marginTop : -getElSize(300),
//	    	marginRight : getElSize(50),
//	    	marginLeft : getElSize(50)
		},
		credits : false,
		title : false,
		xAxis : {
			categories : startTimeLabel,
			labels : {
				step: 1,
				formatter : function() {
					var val = this.value

					if( val == "17:30"
						|| val == "21:30"
						|| val == "1:30"
						|| val == "5:30"
						|| val == "9:30"
						|| val == "13:30"
											
					){
						
					}else{
						val = ""
					}
					
					
					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(18 * 2)
					//fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};

	$("#" + id).highcharts(options);

	var status = $("#" + id).highcharts();
	var options = status.options;

	options.series = [];
	options.title = null;
	options.exporting = false;
	getTimeData(options);
	
	///////////////////////// demo data
	
	
//	options.series.push({
//		data : [ {
//			y : Number(20),
//			segmentColor : "red"
//		} ],
//	});
//	
//	
//	for(var i = 0; i < 719; i++){
//		var color = "";
//		var n = Math.random() * 10;
//		if(n<=5){
//			color = "green";
//		}else if(n<=8){
//			color = "yellow";
//		}else {
//			color = "red";
//		}
//		options.series[0].data.push({
//			y : Number(20),
//			segmentColor : color
//		});
//	};
//	
//	status = new Highcharts.Chart(options);
	
	
	
	////////////////////////////////
	
	setInterval(function(){
		var cMinute = String(addZero(new Date().getMinutes())).substr(0,1);
		if(targetMinute!=cMinute){
			targetMinute = cMinute;
			drawBarChart(id, idx);
		};
	},5000)
};


const getTimeData = (options) =>{
	var url = ctxPath + "/chart/getTimeData.do";
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes())).substr(0,1);
	
	
	if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
		day = addZero(String(new Date().getDate()+1));
	};
	
	var today = year + "-" + month + "-" + day;
	var param = "workDate=" +  $("#date").html().trim().replace(/\./gi, "-") + 
				"&dvcId=" + dvcId;
	
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			var json = data.statusList;
			console.log(json)
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = incycleColor
			}else if(status=="WAIT"){
				color = waitColor
			}else if(status=="ALARM"){
				color = alarmColor
			}else if(status=="NO-CONNECTION"){
				color = noConnColor
			};
			
			spdLoadPoint = [];
			spdOverridePoint = [];
			
			var blank;
			var f_Hour = json[0].startDateTime.substr(11,2);
			var f_Minute = json[0].startDateTime.substr(14,2);
			
			var startN = 0;
			if(f_Hour==startHour && f_Minute==(startMinute*10)){
				options.series.push({
					data : [ {
						y : Number(20),
						segmentColor : color
					} ],
				});
			}else{
				if(f_Hour>=20){
					startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
				}else{
					console.log(f_Hour, f_Minute)
					startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
					startN += (f_Hour*60/2) + (f_Minute/2);
				};
				
				console.log(startN)
				
				options.series.push({
					data : [ {
						y : Number(20),
						segmentColor : noConnColor
					} ],
				});
					
				for(var i = 0; i < startN-1; i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : noConnColor
					});
					spdLoadPoint.push(Number(0));
					spdOverridePoint.push(Number(0));
				};
			};
			
			
			$(json).each(function(idx, data){
				spdLoadPoint.push(Number(data.spdLoad));
				spdOverridePoint.push(Number(data.spdOverride));
				
				if(data.status=="IN-CYCLE"){
					if(Number(data.spdLoad)==0){
						color = incycleColor;
					}else{		//순절삭
						color = cutColor;
					}
				}else if(data.status=="WAIT"){
					color = waitColor;
				}else if(data.status=="ALARM"){
					color = alarmColor;
				}else if(data.status=="NO-CONNECTION"){
					color = noConnColor
				};
				
				options.series[0].data.push({
					y : Number(20),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 719-(json.length+startN); i++){
				options.series[0].data.push({
					y : Number(20),
					segmentColor : "black"
				});
				spdLoadPoint.push(Number(-10));
				spdOverridePoint.push(Number(-10));
			};
			
			$("#timeChart").highcharts(options);
			getSpdFeed()
			//status = new Highcharts.Chart(options);
			//getSpdFeed();
			
			//drawLabelPoint();
		},error : function(e1,e2,e3){
		}
	});

}

const boxItemList = [
	[dailyprdcttarget, "targetCnt"]
	, [dailyprdctcnt, "prdcCnt"]
	, [prdct_per_cycle, "cntPerCycle"]
	, [daily_cycle_cnt, "cycleCnt"]
	, [daily_prdct_avrg_cycle_time, "avrgCycleTime"]
	, [daily_prdct_cnt_per_hour, "prdcCntPerHour"]
	, ["feed Override", "feedOvrrd"]
	, ["Spindle Load", "spdLd"]
];

const createBox = () =>{
	let boxes = ""
	$(boxItemList).each((idx, data) => {
		const boxWidth = getElSize(218 * 2)
		//const leftMargin = getElSize(16 * 2) + marginWidth
		
		
		const leftMargin = (boxWidth * idx) + (getElSize(16 * 2) * (idx + 1))
		
		boxes += 
			`
				<div id="${data[1]}" class="box"
					style="
						width : ${boxWidth}px
						; height : ${getElSize(192 * 2)}px
						; background : url(${ctxPath}/images/FL/bg_info_board_daily_default.svg)
						; background-size : 100%
						; float : left
						; top : ${getElSize(274 * 2) + marginHeight}px
						; left : ${leftMargin + marginWidth}px
						; color : white
						; text-align : center
						; font-weight : bolder
						; font-size : ${getElSize(18 * 2)}px
						; position : absolute
						; user-select: none;
					"
				>
					<span style="user-select: none; margin-top : ${getElSize(24 * 2)}px; display : inline-block">${data[0]}</span><Br>
					<span style="user-select: none; margin-top : ${getElSize(35 * 2)}px; display : inline-block; font-size : ${getElSize(60 * 2)}px; color : #00C6FF;">0</span>
				</div>
			`
	})
	
	
	//last box
	const last_box = 
			`
				<div class="box"
					style="
						width : ${getElSize(32 * 2)}px
						; height : ${getElSize(192 * 2)}px
						; background : url(${ctxPath}/images/FL/bg_info_board_daily_default.svg)
						; top : ${getElSize(274 * 2) + marginHeight}px
						; left : ${getElSize(1888 * 2) + marginWidth}px
						; color : white
						; text-align : center
						; font-weight : bolder
						; font-size : ${getElSize(18 * 2)}px
						; position : absolute
						; user-select: none;
					"
				>
					
				</div>
			`
		
	$("#container").append(boxes, last_box)
	
	$(".box").hover((el)=>{
		$(el.currentTarget).css({
			"background" : `url(${ctxPath}/images/FL/bg_info_board_daily_pushed.svg)`
		})
	}, (el)=>{
		$(el.currentTarget).css({
			"background" : `url(${ctxPath}/images/FL/bg_info_board_daily_default.svg)`
		})
	})
} 


let prgmHeader = "";
let prgm = "";

const createMachineInfo = () =>{
//	<img src="${ctxPath}/images/FL/default/bg_3rd_titlebar.svg"
//		style="
//			width : ${getElSize(1888 * 2)}px
//			; top : ${getElSize(194 * 2) + marginHeight}px
//			; left : ${getElSize(16 * 2) + marginWidth}px
//			; position : absolute
//		"
//	>
	const div = 
		`	<div 
		style="
				width : ${getElSize(1888 * 2)}px
				; height : ${getElSize(64 * 2)}px
				; top : ${getElSize(194 * 2) + marginHeight}px
				; left : ${getElSize(16 * 2) + marginWidth}px
				; position : absolute
				; background: #2B2D32
				; box-shadow: 0 ${getElSize(2 * 2)}px ${getElSize(4 * 2)}px 0 rgba(0,0,0,0.50)
				; border-radius: ${getElSize(4 * 2)}px
			"
			></div>
			
			
			<img src="${ctxPath}/images/FL/icon/ico_mc.svg"
				style="
					width : ${getElSize(40 * 2)}px
					; top : ${getElSize(205 * 2) + marginHeight}px
					; left : ${getElSize(40 * 2) + marginWidth}px
					; position : absolute
				"
			>
			
			<div id="dvc_name_n_pgm_info"
				style="
					font-size : ${getElSize(24 * 2)}px
					; color : white
					; position : absolute
					; top : ${getElSize(214 * 2) + marginHeight}px
					; left : ${getElSize(99 * 2) + marginWidth}px
				"
			>
				<span style="color : #6699F0" id="pgmInfo">
			</div>
			
		`
	$("#container").append(div)
	
	const arrow_left = 
		`
			<img src="${ctxPath}/images/FL/btn_arrow_left_default.svg" class="arrow"
				id="prevDay"
				style = 
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(13 * 2)}px
						; top : ${getElSize(216.5 * 2) + marginHeight}px
						; left : ${getElSize(1664.5 * 2) + marginWidth}px
					"
			>
		`
	
	
	const arrow_right = 
		`
			<img src="${ctxPath}/images/FL/btn_arrow_right_default.svg" class="arrow"
				id="nextDay"
				style = 
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(13 * 2)}px
						; top : ${getElSize(216.5 * 2) + marginHeight}px
						; left : ${getElSize(1864.5 * 2) + marginWidth}px
					"
			>
		`
	$("#container").append(arrow_left, arrow_right)	
	
	$(".arrow").hover((el)=>{
		const src = $(el.currentTarget).attr("src").replace("default", "pushed")
		$(el.currentTarget).attr("src", src)
	}, (el)=>{
		const src = $(el.currentTarget).attr("src").replace("pushed", "default")
		$(el.currentTarget).attr("src", src)
	})
	
	$("#prevDay").click(()=>{
		$("#date").html(getYesterDay)
		
		getStartTime()
		getDetailData()
		getCurrentDvcStatus(dvcId);
	})
	
	$("#nextDay").click(()=>{
		$("#date").html(getTomorrow)
		
		getStartTime()
		getDetailData()
		getCurrentDvcStatus(dvcId);
	})
	
	const date = 
		`
			<span
				id="date"
				style=
					"
						color : white
						; position : absolute
						; z-index : 2
						; font-size : ${getElSize(24 * 2)}px
						; top : ${getElSize(213 * 2) + marginHeight}px
						; left : ${getElSize(1709 * 2) + marginWidth}px
					"
			>
				${getToday().substr(0,10)}
			</span>
		`
	$("#container").append(date)			
}


const getYesterDay = () =>{
    var date = new Date($("#date").html().trim())
    date.setDate(date.getDate() - 1)

    var year = date.getFullYear();
    var month = addZero(String(date.getMonth() + 1))
    var day = addZero(String(date.getDate()))

    return year + "." + month + "." + day;
};

const getTomorrow = () =>{
	if($("#date").html().trim() == getToday().substr(0,10)){
		alert("오늘 이후 날짜는 선택 할 수 없습니다.")
		return
	}
	
    var date = new Date($("#date").html().trim())
    date.setDate(date.getDate() + 1)

    var year = date.getFullYear();
    var month = addZero(String(date.getMonth() + 1))
    var day = addZero(String(date.getDate()))

    return year + "." + month + "." + day;
};

const createCircleChartDiv = () =>{
	const div = 
		`
			<div
				style="
					width : ${getElSize(686 * 2)}px
					; height : ${getElSize(582 * 2)}px
					; background : url(${ctxPath}/images/FL/bg_monitoring_3.svg)
					; background-size : 100%
					; position : absolute
					; top : ${getElSize(482 * 2) + marginHeight}px
					; left : ${getElSize(16 * 2) + marginWidth}px
				"
			>
				
			</div>
		`
	$("#container").append(div)
	
	const title = 
		`
			<span style = 
				"
					position : absolute
					; font-weight : bolder
					; z-index : 2
					; font-size  : ${getElSize(24 * 2)}px
					; top : ${getElSize(506 * 2) + marginHeight}px
					; left : ${getElSize(48 * 2) + marginWidth}px
					; color : white;
					
				"
			>
				${dailystackedstatus}
			</span>
		`
	$("#container").append(title)
	
	creataStatusLabel()
	
	const opratio_gauge = 
		`
			<div id="opratio_gauge"
				style=
					"
						position : absolute
						; z-index : 2
						; background : url(${ctxPath}/images/FL/work_s_circle.svg)
						; background-size : 100%
						; width : ${getElSize(152 * 2)}px
						; height : ${getElSize(152 * 2)}px
						; top : ${getElSize(848 * 2) + marginHeight}px
						; left : ${getElSize(69 * 2) + marginWidth}px
					"
			></div>
		`
	const cutting_gauge = 
		`
			<div id="cutting_gauge"
				style=
					"
						position : absolute
						; z-index : 2
						; background : url(${ctxPath}/images/FL/work_s_circle.svg)
						; background-size : 100%
						; width : ${getElSize(152 * 2)}px
						; height : ${getElSize(152 * 2)}px
						; top : ${getElSize(848 * 2) + marginHeight}px
						; left : ${getElSize(263 * 2) + marginWidth}px
					"
			></div>
		`		
			
	
	const opratio_label = 
		`
			<span 
				id="opRatioSpan"
				style=
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(1027 * 2) + marginHeight}px
						; left : ${getElSize(120 * 2) + marginWidth}px 
					"
			>
				${op_ratio}
			</span>
		`
		
	const cutting_label = 
		`
			<span 
				id="cuttingRatioSpan"
				style=
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(1027 * 2) + marginHeight}px
						; left : ${getElSize(314 * 2) + marginWidth}px 
					"
			>
				${cuttingratio}
			</span>
		`
		
	const pieChart = 
		`
			<div id="pieChart"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(212 * 2)}px
						; height : ${getElSize(212 * 2)}px
						; top : ${getElSize(600 * 2) + marginHeight}px
						; left : ${getElSize(136 * 2) + marginWidth}px
					"
			></div>
		`
		
	const circle_label = 
		`
			<span 
				style=
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(645 * 2) + marginHeight}px
						; left : ${getElSize(218 * 2) + marginWidth}px 
					"
			>
				Work
			</span>
		`
		
	$("#container").append(opratio_gauge, opratio_label, cutting_gauge, cutting_label, pieChart, circle_label)
	
	$("#opRatioSpan").css({
			"left" : $("#opratio_gauge").offset().left + ($("#opratio_gauge").width() / 2) - ($("#opRatioSpan").width() / 2)
		})
	$("#cuttingRatioSpan").css({
		"left" : $("#cutting_gauge").offset().left + ($("#cutting_gauge").width() / 2) - ($("#cuttingRatioSpan").width() / 2)
	})
}

const creataStatusLabel = () =>{
	const incycleLabel = 
		`
			<img src="${ctxPath}/images/FL/incycle_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(562 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	const waitLabel = 
		`
			<img src="${ctxPath}/images/FL/wait_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(658 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	const alarmLabel = 
		`
			<img src="${ctxPath}/images/FL/alarm_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(706 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	const noConnLabel = 
		`
			<img src="${ctxPath}/images/FL/noConn_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(754 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`	
		
	const modeLabel = 
		`
		<img src="${ctxPath}/images/FL/mode_auto_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(206 * 2)}px
					; height : ${getElSize(48 * 2)}px
					; top : ${getElSize(498 * 2) + marginHeight}px
					; left : ${getElSize(480 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`

	const mode_title = 
		`
			<span
				style=
				"
					position : absolute
					; font-size : ${getElSize(16 * 2)}px
					; top : ${getElSize(512 * 2) + marginHeight}px
					; left : ${getElSize(492 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				"
			>MODE</span>
		`
	const auto_title = 
		`
			<span
				style=
				"
					position : absolute
					; font-size : ${getElSize(16 * 2)}px
					; top : ${getElSize(512 * 2) + marginHeight}px
					; left : ${getElSize(568 * 2) + marginWidth}px
					; z-index : 2
					; color : black
				"
			>AUTOMATIC</span>
		`
		
	$("#container").append(incycleLabel, waitLabel, alarmLabel, noConnLabel, modeLabel, mode_title, auto_title)
	
	
	
	const incycle_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(569 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>In-cycle</span>
		`
	const cut_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(614 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>Cut</span>
		`
	const wait_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(665 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>STOP</span
		`
	const alarm_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(713 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>Alarm</span
		`	
	const noConn_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(761 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>None</span
		`	
	$("#container").append(incycle_title, cut_title, wait_title, alarm_title, noConn_title)
	
	
	const incycle_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(573 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const cut_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(618 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const wait_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(669 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const alarm_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(717 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const noConn_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(765 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`			
	$("#container").append(incycle_hr, cut_hr, wait_hr, alarm_hr, noConn_hr)
	
	
	const incycleVal = 
		`
			<span id="incycleVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(565 * 2) + marginHeight}px
						; left : ${getElSize(592 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`
	const cutVal = 
		`
			<span id="cutVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(610 * 2) + marginHeight}px
						; left : ${getElSize(592 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`		
		
	const waitVal = 
		`
			<span id="waitVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(661 * 2) + marginHeight}px
						; left : ${getElSize(592 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`	
	const alarmVal = 
		`
			<span id="alarmVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(709 * 2) + marginHeight}px
						; left : ${getElSize(592 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`	
		
	const noConnVal = 
		`
			<span id="noConnVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(757 * 2) + marginHeight}px
						; left : ${getElSize(592 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`			
	$("#container").append(incycleVal, cutVal, waitVal, alarmVal, noConnVal)
}

const createAlarmDiv = () =>{
	const div = 
		`
			<div
				style="
					width : ${getElSize(1188 * 2)}px
					; height : ${getElSize(144 * 2)}px
					; background : url(${ctxPath}/images/FL/bg_monitoring_1.svg)
					; position : absolute
					; top : ${getElSize(482 * 2) + marginHeight}px
					; left : ${getElSize(718 * 2) + marginWidth}px
				"
			>
				
			</div>
		`
	$("#container").append(div)
	
	const alarm_tag = 
		`
			<img src="${ctxPath}/images/FL/ico_alarm_message.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(72 * 2)}px
					; height : ${getElSize(24 * 2)}px
					; top : ${getElSize(501 * 2) + marginHeight}px
					; left : ${getElSize(734 * 2) + marginWidth}px
				"
			
			>
			
		`
	
	
	const alarm_title = 
		`
			<span
				style = 
				"
					position : absolute
					; z-index : 2
					; font-size : ${getElSize(16 * 2)}px
					; color : black
					; top : ${getElSize(505 * 2) + marginHeight}px
					; left : ${getElSize(748 * 2) + marginWidth}px					
				"
			>
				Alarm
			</span>
		`
	
	const alarm_div = 
		`
			<div id="alarm_div"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(1059 * 2)}px
					; height : ${getElSize(100 * 2)}px
					; top : ${getElSize(501 * 2) + marginHeight}px
					; left : ${getElSize(814 * 2) + marginWidth}px
					; color : white
					; font-size : ${getElSize(18 * 2)}px
					; overflow : auto
					
				"
			>
			
			</div>	
		`
	$("#container").append(alarm_tag, alarm_title, alarm_div)		
	

	
	
	$("body::-webkit-scrollbar-track").css({
		"-webkit-box-shadow" : "inset 0 0 6px rgba(0,0,0,0.3)",
		"border-radius" : "10px",
		"background-color" : "#F5F5F5"
	})

	$("body::-webkit-scrollbar-thumb").css({
		"border-radius" : "10px",
		"-webkit-box-shadow" : "inset 0 0 6px rgba(0,0,0,.3)",
		"background-color" : "#555"
	})

	$("body::-webkit-scrollbar").css({
		"width" : "12px",
		"background-color" : "#F5F5F5"
	})
}


	const createBarChartDiv = () =>{
	const div = 
		`
			<div
				style="
					width : ${getElSize(1188 * 2)}px
					; height : ${getElSize(422 * 2)}px
					; background : url(${ctxPath}/images/FL/bg_monitoring_2.svg)
					; position : absolute
					; top : ${getElSize(642 * 2) + marginHeight}px
					; left : ${getElSize(718 * 2) + marginWidth}px
				"
			>
				
			</div>
		`
	const title = 
		`
			<span style = 
				"
					position : absolute
					; font-weight : bolder
					; z-index : 2
					; font-size  : ${getElSize(24 * 2)}px
					; top : ${getElSize(658 * 2) + marginHeight}px
					; left : ${getElSize(734 * 2) + marginWidth}px
					; color : white;
					
				"
			>
				${operation_status}
			</span>
		`
		
	const spdOvvrd_label = 
		`
			<img src="${ctxPath}/images/FL/info_spindle_over.svg"
				style =
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(32 * 2)}px
						; top : ${getElSize(1035 * 2) + marginHeight}px
						; left : ${getElSize(1507 * 2) + marginWidth}px 
					"
			>
		`
	const spdOvvrd_title = 
		`
			<span
				style =
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(1028 * 2) + marginHeight}px
						; left : ${getElSize(1560.5 * 2) + marginWidth}px 
					"
			>
				Spindle Override
			</span>
		`		
		
	const spdLd_label = 
		`
			<img src="${ctxPath}/images/FL/info_spindle_load.svg"
				style =
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(32 * 2)}px
						; top : ${getElSize(1035 * 2) + marginHeight}px
						; left : ${getElSize(1714.5 * 2) + marginWidth}px 
					"
			>
		`	
	const spdLd_title = 
		`
			<span
				style =
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(1028 * 2) + marginHeight}px
						; left : ${getElSize(1770.5 * 2) + marginWidth}px 
					"
			>
				Spindle Load
			</span>
		`
		
	const barChart = 
		`
			<div id="timeChart"
				style=
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(1109 * 2)}px
					; height : ${getElSize(76 * 2)}px
					; top : ${getElSize(733 * 2) + marginHeight}px
					; left : ${getElSize(757 * 2) + marginWidth}px
				"
			></div>
		`
		
	const spdFeedChart = 
		`
			<div id="spdFeed"
				style=
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(1109 * 2)}px
					; height : ${getElSize(150 * 2)}px
					; top : ${getElSize(846 * 2) + marginHeight}px
					; left : ${getElSize(757 * 2) + marginWidth}px
				"
			></div>
		`
					
	$("#container").append(div, title, spdOvvrd_label, spdOvvrd_title, spdLd_label, spdLd_title, barChart, spdFeedChart)
	
	getStartTime()
}