const loadPage = () =>{
	createMenuTree("monitoring", "24hrChart")

	drawBarChartDiv()
	
	
	getAllDvcList();
}

const getAllDvcList = () =>{
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));

	var sDate = year + "-" + month + "-" + day;
	var eDate = year + "-" + month + "-" + day + " 23:59:59"; 
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId + 
				"&maxRow=" + 200 + 
				"&offset=" + ((c_page-1)*max_row);
	

	var url = ctxPath + "/chart/getBarChartDvcId.do";
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dvcId;
			
			dvcListLength = json.length;
			
			getDvcIdList()
			
			//pager
			createPager(c_page)
		}
	})
}

const moveBarChartPage = (page) =>{
	c_page = page
	getDvcIdList()
	
	createPager(c_page)
}
const createPager = (page) =>{
	let pageCnt = Math.ceil(dvcListLength/max_row) 
	
	$(".pager").remove()
	for(i = pageCnt, j = 0; i > 0; i--, j++){
		//getElSize(1864 * 2) + marginWidth
		let pagerWidth = getElSize(32 * 2)
		let margin = getElSize(16 * 2)
		
		let left = (j * -margin) + (j * -pagerWidth) +  getElSize(1864 * 2) + marginWidth
		
		const pager = 
			`
				<div id="page${i}"
					class="pager"
					onclick="moveBarChartPage(${i});"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${pagerWidth}px
							; height : ${getElSize(32 * 2)}px
							; top :  ${getElSize(1024 * 2) + marginHeight}px
							; left : ${left}px
							; background : url(${ctxPath}/images/FL/btn_page_off.svg)
							; background-size : 100%
							; color : white
							; cursor : pointer
							; text-align : center
							; display : table
							
							; font-size : ${getElSize(18 * 2)}px
						"
				>
					<span style="display:table-cell; vertical-align:middle">${i}</span>
					
				</div>
			`
		$("#container").append(pager)	
			
	}
	
	$("#page" + page).css({
		"color" : "black",
		"background" : `url(${ctxPath}/images/FL/btn_page_on.svg)`,
		"background-size" : "100%"
	})
}
var c_page = 1;
var max_row = 12;

let dvcListLength = 0
const getDvcIdList = () =>{
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));

	var sDate = year + "-" + month + "-" + day;
	var eDate = year + "-" + month + "-" + day + " 23:59:59"; 
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId + 
				"&maxRow=" + max_row + 
				"&offset=" + ((c_page-1)*max_row);
	

	var url = ctxPath + "/chart/getBarChartDvcId.do";
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dvcId;
			
			
			try{
				$(".barChart").each((idx, data)=>{
					$(data).highcharts().destroy()
				})
				
				$(".name").html("")
			}catch (e){
				
			}
			
			
			for(var i = 0; i < json.length; i++){
				$("#name" + (i + 1)).html(decodeURIComponent(json[i].name).replace(/\+/gi, " "))
				drawBarChart("bar" + (i + 1));	
				getStatusChart(json[i].dvcId, i + 1);
			}
		}
	});
};
	
const getStatusChart = (dvcId, idx) =>{
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes())).substr(0,1);
	
	
	var today = year + "-" + month + "-" + day;
	
	var url = ctxPath + "/chart/getTimeData.do";
	var param = "dvcId=" + dvcId + 
				"&workDate=" + today;
	
//	setInterval(function (){
//		var minute = String(new Date().getMinutes());
//		if(minute.length!=1){
//			minute = minute.substr(1,2);
//		};
//		
//		if(minute==2 && eval("dvcMap" + idx).get("initFlag") || minute==2 && typeof(eval("dvcMap" + idx).get("initFlag")=="undefined")){
//			getStatusChart(dvcId, idx);
//			console.log("init")
//			eval("dvcMap" + idx).put("initFlag", false);
//			eval("dvcMap" + idx).put("currentFlag", true);
//		}else if(minute!=2){
//			eval("dvcMap" + idx).put("initFlag", true);
//		};
//	}, 1000 * 10);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			
			eval("dvcMap" + idx + " = new JqMap();");
			
			if(data==null || data==""){
				eval("dvcMap" + idx).put("noSeries", true);
				getCurrentDvcStatus(dvcId, idx);
				return;
			}else{
				eval("dvcMap" + idx).put("noSeries", false);
			};

			 
			 var status = $("#bar" + idx).highcharts();
			var options = status.options;
				
			var json = data.statusList;
			
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = incycleColor
			}else if(status=="WAIT"){
				color = waitColor
			}else if(status=="ALARM"){
				color = alarmColor
			}else if(status=="NO-CONNECTION"){
				color = noConnColor
			};
			
			var blank;
			var f_Hour = json[0].startDateTime.substr(11,2);
			var f_Minute = json[0].startDateTime.substr(14,2);
			
			var startHour = 8;
			var startMinute = 3;
			
			var startN = 0;
			
			spdLoadPoint = [];
			spdOverridePoint = [];
			
			
			if(f_Hour==startHour && f_Minute==(startMinute*10)){
				options.series.push({
					data : [ {
						y : Number(20),
						segmentColor : color
					} ],
				});
			}else{
				if(f_Hour>=20){
					startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
				}else{
					startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
					startN += (f_Hour*60/2) + (f_Minute/2);
				};
				
				options.series.push({
					data : [ {
						y : Number(20),
						segmentColor : noConnColor
					} ],
				});
					
				for(var i = 0; i < startN-1; i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : noConnColor
					});
					spdLoadPoint.push(Number(0));
					spdOverridePoint.push(Number(0));
				};
			};
			
			
			
			
			$(json).each(function(idx, data){
				if(data.status=="IN-CYCLE"){
					color = incycleColor
				}else if(data.status=="WAIT"){
					color = waitColor
				}else if(data.status=="ALARM"){
					color = alarmColor
				}else if(data.status=="NO-CONNECTION"){
					color = noConnColor
				};
				options.series[0].data.push({
					y : Number(20),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 719-(json.length+startN); i++){
				options.series[0].data.push({
					y : Number(20),
					segmentColor : "black"
				});
			};
			
			
			
			$("#bar" + idx).highcharts(options);
			//status = new Highcharts.Chart(options);
			//getCurrentDvcStatus(dvcId, idx);
		}
	});
};

var startTimeLabel = new Array();

var startHour = 8;
var startMinute = 3;

const drawBarChart = (id) =>{
	var fontColor = "white;"
	
	var m0 = "",
	m02 = "",
	m04 = "",
	m06 = "",
	m08 = "",
	
	m1 = "";
	m12 = "",
	m14 = "",
	m16 = "",
	m18 = "",
	
	m2 = "";
	m22 = "",
	m24 = "",
	m26 = "",
	m28 = "",
	
	m3 = "";
	m32 = "",
	m34 = "",
	m36 = "",
	m38 = "",
	
	m4 = "";
	m42 = "",
	m44 = "",
	m46 = "",
	m48 = "",
	
	m5 = "";
	m52 = "",
	m54 = "",
	m56 = "",
	m58 = "";

var n = Number(startHour);
if(startMinute!=0) n+=1;

startTimeLabel = []
for(var i = 0, j = n ; i < 24; i++, j++){
	eval("m" + startMinute + "=" + j);
	
	//startTimeLabel.push(m0);
	startTimeLabel.push((j - 1) + ":30");
	startTimeLabel.push(m02);
	startTimeLabel.push(m04);
	startTimeLabel.push(m06);
	startTimeLabel.push(m08);
	
	startTimeLabel.push(m1);
	startTimeLabel.push(m12);
	startTimeLabel.push(m14);
	startTimeLabel.push(m16);
	startTimeLabel.push(m18);
	
	startTimeLabel.push(m2);
	startTimeLabel.push(m22);
	startTimeLabel.push(m24);
	startTimeLabel.push(m26);
	startTimeLabel.push(m28);
	
	startTimeLabel.push(m3);
	startTimeLabel.push(m32);
	startTimeLabel.push(m34);
	startTimeLabel.push(m36);
	startTimeLabel.push(m38);
	
	startTimeLabel.push(m4);
	startTimeLabel.push(m42);
	startTimeLabel.push(m44);
	startTimeLabel.push(m46);
	startTimeLabel.push(m48);
	
	startTimeLabel.push(m5);
	startTimeLabel.push(m52);
	startTimeLabel.push(m54);
	startTimeLabel.push(m56);
	startTimeLabel.push(m58);
	
	if(j==24){ j = 0}
};

	
	
	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgba(255, 255, 255, 0)',
			height : getElSize(330.3), 
//			marginTop: -60,
//			marginBottom: 25
			borderColor : "red",
	        borderWidth: 0,
		},
		credits : false,
		exporting: false,
		title : {
			text :name,
			align :"left",
			y:10,
			style : {
				color : "black",
				fontSize: getElSize(40) + "px",
				fontWeight: 'bold'
			}
		},
		yAxis : {
	        gridLineColor: 'rgba(0,0,0,0)',
			labels : {
				enabled : false,
			},
			title : {
				text : false,
                rotation: 0,
               
			},
		},
		xAxis:{
	        lineColor: 'rgba(0,0,0,0)',
	           categories:startTimeLabel,
	            labels:{
	            	step: 1,
					formatter : function() {
						var val = this.value

						
						if( val == "17:30"
							|| val == "21:30"
							|| val == "1:30"
							|| val == "5:30"
							|| val == "9:30"
							|| val == "13:30"
												
						){
							
						}else{
							val = ""
						}
						
						return val;
					},
				    style :{
				    	color : fontColor,
				    	fontSize : getElSize(18 * 2)
				    },
	            }
	        },
	       
		tooltip : {
			headerFormat : "",
			style : {
				fontSize : '10px',
			},
			enabled : false
		}, 
		plotOptions: {
		    line: {
		        marker: {
		            enabled: false
		        }
		    },
		    series : {
		    	animation : false
		    }
		},
		legend : {
			enabled : false
		},
		series: []
	}

   	$('#' + id).highcharts(options);
};

const drawBarChartDiv = () =>{
	for(let i = 1, j = 1, k = 1; i <= 12; i++, j++){
		let barWidth = getElSize(936 * 2)
		let barHeight = getElSize(120 * 2)
		let margin = getElSize(16 * 2)
		
		if(j > 6){
			j = 1
			k = 2
		}
		
		let top = ( margin * j + ((j - 1) * barHeight) ) + $("#container").offset().top
		
	
		
		let left = ( margin * k + ((k - 1) * barWidth) ) + $("#container").offset().left
		
		
		const div = 
			`
				<div id="bar${i}"
					class="barChart"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${barWidth}px
							; height : ${barHeight}px
							; background-color : #2B2D32
							; top : ${top}
							; left : ${left}
							; border-radius : ${getElSize(4 * 2)}px
							
						"
				></div>
				
				<img src="${ctxPath}/images/FL/icon/ico_mc.svg"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(24 * 2)}px
							; height : ${getElSize(24 * 2)}px
							; top : ${top + getElSize(3 * 2)}
							; left : ${left + getElSize(15.9 * 2)}
						">
				<span
					class="name"
					id="name${i}"
					style=
						"
							position : absolute
							; z-index : 2
							; color : #6699F0
							; font-family : NanumSquareOTFEB
							; font-size : ${getElSize(18 * 2)}px
							; top : ${top + getElSize(4 * 2)}
							; left : ${left + getElSize(47.6 * 2)}
						"
				></span>
			`
			
		$("#container").append(div)	
	}
	
	
	const pager = 
		`
			<div
				style=
					"
						position : absolute
						; border-radius : ${getElSize(4 * 2)}px
						; z-index : 2
						; background-color : #1E1E23
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(48 * 2)}px
						; top : ${getElSize(1016 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
						
					"
			></div>
		`
	
	$("#container").append(pager)	
}
