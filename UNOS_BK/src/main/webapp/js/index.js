const loadPage = () =>{
	//setEl();
	bindMyEvt()
	drawLogoNIcon()
}
	
const drawLogoNIcon = () =>{
	let subTitle = "";
	if(appTy == "auto"){
		subTitle = "(Automotive)"
	}else{
		subTitle = "(Standard)"
	}
	
	const logo = 
		`
			<img src = "${ctxPath }/images/FL/logo/aidoo_control_v_w.svg"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(184 * 2)}px
						; top : ${getElSize(242 * 2) + marginHeight}px
						; left : ${getElSize(868.5 * 2) + marginWidth}px
					"
			>
		`
		
	const subtitle_text = 
		`
			<span id="subtitle_text"
				style=
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(65)}px
						; top : ${getElSize(410 * 2) + marginHeight}px
					"
			>${subTitle}</span>
		`
	const id_input = 
		`
			<input onkeyup="chkKeyEvt(event)" 
				id="id" 
				type="text" 
				placeholder="ID" 
				autocomplete="off" 
				style="
						background: transparent url(${ctxPath}/images/FL/form/text_input_default.svg)
						; position : absolute
						; z-index : 2
						; top : ${getElSize(492 * 2) + marginHeight}px
						; left : ${getElSize(800 * 2) + marginWidth}px
						; width: ${getElSize(320 * 2)}px
						; height : ${getElSize(48 * 2)}px
						; font-size : ${getElSize(24 * 2)}px
						; border : 0px
						; color : white
						; padding-left : ${getElSize(20)}px
						 
					"
				>
		`
	const pwd_input = 
		`
			<input onkeyup="chkKeyEvt(event)" 
				id="pwd" 
				type="password" 
				placeholder="Password" 
				autocomplete="new-password" 
				style="
						background: transparent url(${ctxPath}/images/FL/form/text_input_default.svg)
						; position : absolute
						; z-index : 2
						; top : ${getElSize(556 * 2) + marginHeight}px
						; left : ${getElSize(800 * 2) + marginWidth}px
						; width: ${getElSize(320 * 2)}px
						; height : ${getElSize(48 * 2)}px
						; font-size : ${getElSize(24 * 2)}px
						; border : 0px
						; color : white
						; padding-left : ${getElSize(20)}px
						 
					"
				>
		`

		const login = 
		`
			<div 
				id="login" 
				onclick="login();" 
				style="
						background-image : url(${ctxPath}/images/FL/form/btn_login_default.svg)
						; background-size : 100% 
						; display:table
						; position : absolute
						; z-index : 2
						; width : ${getElSize(320 * 2)}px
						; height : ${getElSize(48 * 2)}px
						; cursor : pointer
						; text-align : center
						; top : ${getElSize(620 * 2) + marginHeight}px
						; left : ${getElSize(800 * 2) + marginWidth}px
						; font-size : ${getElSize(24 * 2)}px
					"
			>
				<span style="display:table-cell; vertical-align:middle; ">Log in</span>
			</div>
		`
			
	const footer_logo = 
		`
			<img src="${ctxPath }/images/FL/default/dmt_logo.svg" id="dmt_logo"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(301.5 * 2)}px
						; height : ${getElSize(33.5 * 2)}px
						; top : ${getElSize(1013.5 * 2) + marginHeight}px
						; left : ${getElSize(809 * 2) + marginWidth}px 
					"
			>
		`
	$("#container").append(logo, id_input, pwd_input, login, footer_logo, subtitle_text)
	
	$("#subtitle_text").css({
		"left" : (originWidth / 2) - ($("#subtitle_text").width() / 2) 
	})
	
	$("#id, #pwd").focus((el)=>{
			$(el.currentTarget).css({
				"background" : `transparent url("${ctxPath}/images/FL/form/text_input_selected.svg")`,
				"outline" : "none",
				"background-size" : "100% 100%"
				
			})
		}).blur((el)=>{
			$(el.currentTarget).css({
				"background" : `transparent url("${ctxPath}/images/FL/form/text_input_default.svg")`,
				"outline" : "none",
				"background-size" : "none"
			})
		})
		
		
		$("#login").hover((el)=>{
		$(el.currentTarget).css({
			"background-image" : `url("${ctxPath}/images/FL/form/btn_login_pushed.svg")`
		})
	}, (el)=>{
		$(el.currentTarget).css({
			"background-image" : `url("${ctxPath}/images/FL/form/btn_login_default.svg")`
		})
	})
	
	$("#login span").css({
		"color" : "black",
		"font-size" : getElSize(24 * 2),
	});
	
	$("#id").focus()
}

const setEl = () =>{
	$("#icon_table td").css({
		"text-align" : "center"
	})

	$("#icon_table td img").css({
		"cursor" : "pointer",
		"width" : getElSize(300)
	})
	
	$("#icon_table td span").css({
		"color" : "white",
		"font-size" : getElSize(35) + "px"
	});
	
	$("#dmt_logo").css({
		"position" : 'absolute',
		"width" : getElSize(350 * 2),
	})
	
	$("#dmt_logo").css({
		"top" : getElSize(1013 * 2),
		"left" : ($("#container").width()/2 - ($("#dmt_logo").width() / 2) + marginWidth)
	});
	
	$("#main_logo").css({
		"width" : getElSize(500),
		"margin-top" : getElSize(242 * 2) - $("#header").height(),
		"margin-bottom" : getElSize(200),
		"opacity" : 1
	});
	
	
	
	
	
};
	
const bindMyEvt = () =>{
	$("#icon_table td img").not("#main_logo").hover((el)=>{
		const ic = $(el.target).attr("ic")
		
		$(el.target).attr("src", ctxPath + "/images/FL/icon/" + ic + "_pushed.svg")
	}, (el) =>{
		const ic = $(el.target).attr("ic")
		
		$(el.target).attr("src", ctxPath + "/images/FL/icon/" + ic + "_default.svg")
	}).click((el)=>{
		const ic = $(el.target).attr("ic")
		
		if(ic == "monitoring"){
			location.href = "/iDOO_Dashboard/index.do"
		}else if(ic == "analysis"){
			location.href = "/iDOO_Analysis/Performance_Report_Chart.do"
		}else if(ic == "im"){
			//location.href = ""
			alert("준비 중입니다.")
		}else if(ic == "kpi"){
			location.href = "/iDOO_KPI/kpi/productionStatusKpi_backUp.do"
		}else if(ic == "qm"){
			location.href = "/iDOO_QM/chart/addFaulty.do"
		}else if(ic == "tm"){
			location.href = "/iDOO_TM/chart/toolLifeManager.do"
		}else if(ic == "om"){
			location.href = "/iDOO_OM/order/addTarget.do"
		}else if(ic == "maintenance"){
			location.href = "/iDOO_maintenance/chart/alarmReport.do"
		}else if(ic == "config"){
			location.href = "/iDOO_Config/chart/traceManager.do"
		}
	})	
}

