const loadPage = () =>{
	createMenuTree("analysis", "Performance_Report_Chart")
	
	createSearchDiv()
	getGroup()
}

function reArrangeArray(){
	dateList = new Array();
	inCycleBar = new Array();
	waitBar = new Array();
	alarmBar = new Array();
	noConnBar = new Array();
	wcName = new Array();
	wcList = new Array();
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
		dateList[i-cBarPoint] = tmpArray[i];
		inCycleBar[i-cBarPoint] = tmpInCycleBar[i];
		waitBar[i-cBarPoint] = tmpWaitBar[i];
		alarmBar[i-cBarPoint] = tmpAlarmBar[i];
		noConnBar[i-cBarPoint] = tmpNoConnBar[i];
		wcName[i-cBarPoint] = tmpArray[i];
	};
	
	for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
		wcList[i-cBarPoint] = tmpWcList[i];	
	};
	
	var wc = new Array();
//	wc.push("${division}");
//	wc.push("${ophour}");
//	wc.push("${wait}");
//	wc.push("${stop}");
//	wc.push("${noconnection}");
	
	wc.push(division);
	wc.push(ophour);
	wc.push(wait);
	wc.push(stop);
	wc.push(noconnection);

	
	//wcList.push(wc);
	wcList[0] = wc;
	
	$("#controller font").html(String(cPage) + " / " + Math.ceil((maxPage + maxBar) / 10));
};

var cPage = 1;



const getGroup = () => {
	//var url = "${ctxPath}/chart/getGroup.do";
	var url = ctxPath + "/chart/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = `<option value='all'>${total}</option>`;
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
			});
			
			$("#group").html(option).css({
				"display" : "inline",
				"width" : getElSize(500)
				
			})
			
			//setSelectDesign()
			
			getAllDvcList()
			//getDvcList();
		}
	});
};

var barChart;
var maxBar = 10;

var wcDataList = new Array();
var dateList = new Array();
var wcList = new Array();
var wcName = new Array();
var maxPage;
var cBarPoint = 0;

let c_page = 1;
const max_row = 10;

let dvcListLength = 0
const getAllDvcList = () =>{
	var url = ctxPath + "/chart/getTableData.do";

	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + " 23:59:59" + 
				"&shopId=" + shopId +
				"&group=" + $("#group").val() + 
				"&maxRow=" + 200 + 
				"&offset=" + ((c_page-1)*max_row);
	
	$.showLoading();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.tableData;
			
			dvcListLength = json.length;
			getDvcList()
			
			createPager(c_page)
		}
	});
	
}

const moveBarChartPage = (page) =>{
	$.showLoading();
	
	c_page = page
	getDvcList()
	
	createPager(c_page)
}

const createPager = (page) =>{
	let pageCnt = Math.ceil(dvcListLength/max_row) 
	
	$(".pager").remove()
	for(i = pageCnt, j = 0; i > 0; i--, j++){
		//getElSize(1864 * 2) + marginWidth
		let pagerWidth = getElSize(32 * 2)
		let margin = getElSize(16 * 2)
		
		let left = (j * -margin) + (j * -pagerWidth) +  getElSize(1864 * 2) + marginWidth
		
		const pager = 
			`
				<div id="page${i}"
					class="pager"
					onclick="moveBarChartPage(${i});"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${pagerWidth}px
							; height : ${getElSize(32 * 2)}px
							; top :  ${getElSize(1024 * 2) + marginHeight}px
							; left : ${left}px
							; background-color : #2B2D32
							; color : white
							; cursor : pointer
							; text-align : center
							; display : table
							
							; font-size : ${getElSize(18 * 2)}px
						"
				>
					<span style="display:table-cell; vertical-align:middle">${i}</span>
					
				</div>
			`
		$("#container").append(pager)	
			
	}
	
	$("#page" + page).css({
		"color" : "black",
		"background-color" : "#6699F0"
	})
}

const getDvcList = () => {
	var url = ctxPath + "/chart/getTableData.do";

	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + " 23:59:59" + 
				"&shopId=" + shopId +
				"&group=" + $("#group").val() +
				"&maxRow=" + max_row + 
				"&offset=" + ((c_page-1)*max_row);
	
	window.localStorage.setItem("jig_sDate", sDate);
	window.localStorage.setItem("jig_eDate", eDate);
	
	
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			cBarPoint = 0;
			cPage = 1;
			var json = data.tableData;
			

			
			console.log(json)
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;
			
			var tr = "";

			wcList = new Array();
			var wc = new Array();
			wc.push(division);
			wc.push(ophour);
			wc.push(wait);
			wc.push(stop);
			wc.push(noconnection);
			
			wcList.push(wc);
			
			inCycleBar = new Array();
			waitBar = new Array();
			alarmBar = new Array();
			noConnBar = new Array();
			wcName = new Array();
			
			$(json).each(function(idx, data){
				dateList.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
				tmpArray = dateList;
				
				
				wcName.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					 
				var wc = new Array();
				wc.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
				wc.push(Number(data.inCycle_time));
				wc.push(Number(data.wait_time));
				wc.push(Number(data.alarm_time));
				wc.push(Number(Number(n * 24 * 60 * 60 - (Number(data.inCycle_time) + Number(data.wait_time) + Number(data.alarm_time) )).toFixed(1)));
				wc.push(data.WC);
				
				
				wcList.push(wc);
				
				tmpWcList = wcList;
				
				console.log("------wc------")
				console.log(wc)
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				inCycleBar.push(incycle);
				waitBar.push(wait);
				alarmBar.push(alarm);
				noConnBar.push(noconn);
				
				tmpInCycleBar = inCycleBar;
				tmpWaitBar = waitBar;
				tmpAlarmBar = alarmBar;
				tmpNoConnBar = noConnBar;
			});
		
			var blank = maxBar - json.length
			maxPage = json.length - maxBar;
			
		
			if(blank >= 0){
				for(var i = 0; i < blank; i++){
					wcName.push("");
					var wc = new Array();
					wc.push("______");
					wc.push("");
					wc.push("");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);
					
					inCycleBar.push(0);
					waitBar.push(0);
					alarmBar.push(0);
					noConnBar.push(0);
				};	
			}else{
				for(var i = 0; i < maxBar - json.length % 10; i++){
					wcName.push("");
					var wc = new Array();
					wc.push("______");
					wc.push("");
					wc.push("");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);
					
					inCycleBar.push(0);
					waitBar.push(0);
					alarmBar.push(0);
					noConnBar.push(0);
				}; 	
			}
			
			
			if(json.length > maxBar){
				reArrangeArray();
				$("#arrow_left").attr("src", ctxPath + "/chart/images/left_dis.png");
			}else{
				$("#controller font").html("01 / 01");
			} 
			
			$(".chartTable").remove();
			var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			
			table_total += "<tr class='contentTr' >" + 
								"<td align='center' colspan='16' style='font-weight: bolder; height : " + getElSize(40 * 2) + "px; background-color: #353542;' class='content'>일일 평균</td>" + 
							"</tr>";
			
		 	var stDate = new Date($("#sDate").val()) ;
		    var endDate = new Date($("#eDate").val()) ;
		 
		    var btMs = endDate.getTime() - stDate.getTime() ;
		    var btDay = btMs / (1000*60*60*24) ;
							    
			var day_cnt = btDay+1;

			for(var i = 0; i < wcList[0].length; i++){
				table += "<tr class='contentTr'>";
				table_total += "<tr class='contentTr'>";

				let color = ""
				var bgColor;
				if(i==1){
					bgColor = "#A3D800";
					color = "black";
				}else if(i==2){
					bgColor = "#FF9100";
					color = "black";
				}else if(i==3){
					bgColor = "#C41C00";
					color = "white";
				}else if(i==4){
					bgColor = "#6B6C7C";
					color = "white";
				}else{
					bgColor = "#323232";
				}
				
				color = "black";
				for(var j = 0; j < wcList.length; j++){
					if(j==0){
						table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + ";height:" + getElSize(40 * 2) +"; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
						
						if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color : " + color + "; height : " + getElSize(40 * 2) + "; width :" + getElSize(300) + "; ' class='title_'>" + wcList[j][i] + "</td>";
					}else if(j==0 || i==0){
						table += "<td style='font-weight: bolder;  background-color: " + bgColor + ";' class='content' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
					}else {
						var n;
						if(typeof(wcList[j][i])=="number"){
							n = Number(wcList[j][i]/60/60).toFixed(1)
						}else{
							n = "";
						};
						
						table += "<td class='content'>" + n + "</td>";
						table_total += "<td class='content'>" + Number(n/day_cnt).toFixed(1) + "</td>";
					};
				};
				table += "</tr>";
				table_total += "</tr>";
			};
			
			table += "</table>";
			table_total += "</table>";
			$("#tableContainer").append(table)
		
			$("#tableContainer tr:nth(0) td").css({
				"background-color" : "#353542",
				"color" : "white"
			})
	
			$("#tableContainer tr:nth(1) td").not("td:nth(0)").css({
				"background-color" : "#DCDCDC",
				"color" : "black"
			})
			
			$("#tableContainer tr:nth(3) td").not("td:nth(0)").css({
				"background-color" : "#DCDCDC",
				"color" : "black"
			})
			
			$("#tableContainer tr:nth(2) td").not("td:nth(0)").css({
				"background-color" : "#F0F0F0",
				"color" : "black"
			})
			
			$("#tableContainer tr:nth(4) td").not("td:nth(0)").css({
				"background-color" : "#F0F0F0",
				"color" : "black"
			})
			
			$("#tableContainer_avg").append(table_total)
					
			$("#tableContainer_avg tr:nth(2) td").not("td:nth(0)").css({
				"background-color" : "#DCDCDC",
				"color" : "black"
			})
			
			$("#tableContainer_avg tr:nth(4) td").not("td:nth(0)").css({
				"background-color" : "#DCDCDC",
				"color" : "black"
			})
			
			$("#tableContainer_avg tr:nth(3) td").not("td:nth(0)").css({
				"background-color" : "#F0F0F0",
				"color" : "black"
			})
			
			$("#tableContainer_avg tr:nth(5) td").not("td:nth(0)").css({
				"background-color" : "#F0F0F0",
				"color" : "black"
			})
			
			//setEl();
			
			$(".title_").css({
				"width" : getElSize(300),
				"padding" : getElSize(5),
				"font-size" : getElSize(40)
			});
			
			$(".content").css({
				"font-size" : getElSize(40),
				"width" : ($(".contentTr").width() - getElSize(300))/10
			})
			
			chart("chart",[wcList[1][0]
						,wcList[2][0]
						,wcList[3][0]
						,wcList[4][0]
						,wcList[5][0]
						,wcList[6][0]
						,wcList[7][0]
						,wcList[8][0]
						,wcList[9][0]
						,wcList[10][0]]);
			
			//addSeries();
			
			
			$.hideLoading();
		//	$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
		}
	});
};

const chart = (id, nameList) => {
	
	//0값 라벨 제거
/* 		for(var i=0;i<maxBar;i++ ){
		console.log(alarmBar[i])
		console.log(alarmBar[i]==0)
		if(alarmBar[i]==0){
			alarmBar[i]=null;
		}
		if(inCycleBar[i]==0){
			inCycleBar[i]=null;
		}
		if(waitBar[i]==0){
			waitBar[i]=null;
		}
	} */
	
	$("#kendoChart").kendoChart({
		chartArea: {
			height: getElSize(250 * 2),
			background:"rgba(0,0,0,0)",
			margin:{
				left : $("#tableContainer table tr:nth(0) td:nth(1)").offset().left - marginWidth - getElSize(75)
			}
		},
		title: false,
        legend: {	//범례표?
         	labels:{
        		font:getElSize(48) + "px sans-serif",
        		color:"white"
        	},
        	stroke: {
        		width:getElSize(100)
        	},
        	
        	position: "bottom",
        	orientation: "horizontal",
            offsetX: getElSize(1010),
            visible : false
//            offsetY: getElSize(800)
       
        },
        render: function(e) {	//범례 두께 조절
            var el = e.sender.element;
            el.find("text")
                .parent()
                .prev("path")
                .attr("stroke-width", getElSize(20));
        },
		seriesDefaults: {	//데이터 기본값 ()
			gap: getElSize(5),
			type: "column" ,
			stack:true,
			spacing: getElSize(1),
				labels:{
					font:getElSize(18 * 2) + "px sans-serif",	//no working
					margin:0,
					padding:0
			}/* , 
			visual: function (e) {
                return createColumn(e.rect, e.options.color);
            } */
		},	
		categoryAxis: {	//x축값
            categories: wcName,
            majorGridLines: {
                color: "rgba(0,0,0,0)"
              },
            line: {
                visible: false
            },
            labels:{
            	font:getElSize(14 * 2) + "px sans-serif",	
            	color:"white"
			} 
        },
        tooltip: {	//커서 올리면 값나옴
            visible: true,
            format: "{0}%",
            template: "#= series.name #: #= value #"
        },
        valueAxis: {	//간격 y축
        	majorGridLines: {
                color: "#353542"
              },
            labels: {
                format: "{0}",
                font:getElSize(14 * 2) + "px sans-serif",
                color:"white"
            },
             min : 0,
             max : 24
//            majorUnit: 100	//간격
        },

        
        series: [/* {	//데이터
            labels: {
                visible: true,
                color: "black",	
                background:"gray",
                font:getElSize(60) + "px sans-serif",	
          	    position: "center",
              },
              color : "gray",
              name: "noConnBar",
              data: noConnBar
              		
            }, */{	//데이터
            	
            	 overlay: { gradient: "none" },
            labels: {
                visible: true,
                color: "white",	
                background:"#A3D800",
                font:getElSize(18 * 2) + "px sans-serif",	
          	    position: "center",
                margin: {
              		right : 45
             	}
//          	    margin:10
              },
              color : incycleColor,

              name: ophour,
              data: inCycleBar		
            },{	//데이터
            	overlay: { gradient: "none" },
                labels: {
                    visible: true,
                    background:"#FF9100",
                    font:getElSize(18 * 2) + "px sans-serif",	
                    position: "center",
            	    color: "white",
            	    margin: {
//                    	bottom : 100,
                    	left : 45
                    }
                },
                    color : waitColor,
                    name: wait,
                    data: waitBar		
             },{	//데이터
            	 overlay: { gradient: "none" },
                labels: {
                    visible: true,
                    background:"#C41C00",
                    font:getElSize(18 * 2) + "px sans-serif",	
//                    position: "center",
                    color: "white",
                    margin: {
//                    	bottom : 100,
                    	right : 45
                    }
                },
					color : alarmColor,
					name: stop,
					data: alarmBar		
			}]
	})
	
}



const createSearchDiv = () =>{
	const div = 
		`
			<div
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(64 * 2)}px
						; top : ${getElSize(194 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
						; background-color : #2B2D32 
					"
			>
				<select id="group"
					style=
						"
							margin-top : ${getElSize(12 * 2)}px
							; margin-left : ${getElSize(12 * 2)}px
							; display : none
						"
				>
				</select>
				
				<span 
					style=
						"
							position : absolute
							; z-index : 2
							; color : white
							; font-size : ${getElSize(24 * 2)}px
							; top : ${getElSize(22 * 2)}px
							; left : ${getElSize(274 * 2)}px
							; line-height : ${getElSize(24 * 2)}px
						"
				>가동 기간</span>
				
				<input type="date"
					id="sDate"
					style=
						"
							position : absolute
							; z-index : 2
							; top : ${getElSize(14 * 2)}px
							; left : ${getElSize(379 * 2)}px
						"
				>
				
				<input type="date"
					id="eDate"
					style=
						"
							position : absolute
							; z-index : 2
							; top : ${getElSize(14 * 2)}px
							; left : ${getElSize(671 * 2)}px
						"
				>
				
				<div
					onclick="$.showLoading();getDvcList();"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(120 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; background-color : #9B9B9B
							; font-size : ${getElSize(24 * 2)}px
							; top : ${getElSize(12 * 2)}px
							; border-radius : ${getElSize(2 * 2)}px
							; text-align:center
							; left : ${getElSize(941 * 2)}px
							; display : table
							; cursor : pointer
						"
				>
					<img src="${ctxPath}/images/FL/default/ico_search.svg" style="width : ${getElSize(23 * 2)}px; margin:${getElSize(8 * 2)}px; margin-right:0px;  margin-left:0px">
					<span style="display:table-cell; vertical-align:middle">조회</span>
				</div>
				
				<img src="${ctxPath}/images/FL/btn_graph_pushed.svg"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(40 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; cursor : pointer
							; top : ${getElSize(190 * 2) + marginHeight - $("#container").offset().top}px
							; left : ${getElSize(1776 * 2) + marginWidth - $("#container").offset().left}px
						"
				>
				
				<img src="${ctxPath}/images/FL/btn_table_default.svg"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(40 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; cursor : pointer
							; top : ${getElSize(190 * 2) + marginHeight - $("#container").offset().top}px
							; left : ${getElSize(1832 * 2) + marginWidth - $("#container").offset().left}px
						"
						onclick="goGraph()"
				>
			</div>
		`
	
	const graphDiv = 
		`
			<div
				id="kendoChart"
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${contentWidth}px
						; height : ${getElSize(280 * 2)}px
						; top : ${getElSize(274 * 2) + marginHeight - $("#container").offset().top}px
					"
			></div>
		`
		
		
	const tableContainer = 
		`
			<div 
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(478 * 2)}px
						; background-color : #1E1E23
						; top : ${getElSize(528 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
					"
			>
				<div id="tableContainer" class="tableContainer" style="margin : ${getElSize(16 * 2)}px"></div>
				
				<div id="tableContainer_avg"  class="tableContainer" style="margin : ${getElSize(16 * 2)}px"></div>
			</div>
		`
	
	const pager = 
		`
			<div
				style=
					"
						position : absolute
						; z-index : 2
						; background-color : #1E1E23
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(48 * 2)}px
						; top : ${getElSize(1016 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
						
					"
			></div>
		`
	
	$("#container").append(div, graphDiv, tableContainer, pager)	
	
	
	
	$("#sDate").val(caldate(7))
	$("#eDate").val(caldate(0))
	setDateDesign()
}

const goGraph = () => {
	location.href = ctxPath + "/chart/performanceReport.do"
};

