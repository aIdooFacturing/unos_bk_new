const loadPage = () =>{
	createMenuTree("analysis", "Performance_Report_Chart")
	
	createSearchDiv()
	createTableDiv()
	
	getGroup()
}

const getGroup = () => {
	//var url = "${ctxPath}/chart/getGroup.do";
	var url = ctxPath + "/chart/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='all'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
			});
			
			$("#group").html(option);
			
			setSelectDesign()
			
			getTableData("jig")
		}
	});
};


const createTableDiv = () =>{
	const div = 
		`
			<div class="tableContainer" style=
				"
					width:${getElSize(1888 * 2)}px 
					
					; position : absolute
					; z-index : 2
					; top : ${getElSize(290 * 2) + marginHeight}px
					; left : ${getElSize(16 * 2) + marginWidth}px 
				"
			>
				<table style="color: white; width: 100%; text-align: center; " class="tmpTable" id="jigTable">
					<thead>
						<tr style="font-weight: bolder; background-color: #353542" class="thead">
							<td rowspan="2">${device}</td> 
							<td rowspan="2">${target_op_time}</td>
							<td rowspan="2">${total_op_time}</td>
							<td rowspan="2">${number_of_op_day}</td>
							<td colspan="4">${avrg_of_day}</td>
							<td rowspan="2">${opratio_against_target}</td>
						</tr>
						<tr style="font-weight: bolder; background-color: #353542" class="thead">
							<td style="width : ${getElSize(105 * 2)}px; background-color : ${incycleColor}; color : black">${incycle}</td>
							<td style="width : ${getElSize(105 * 2)}px; background-color : ${waitColor}; color : black">${wait}</td>
							<td style="width : ${getElSize(105 * 2)}px; background-color : ${alarmColor}; color : black">${stop}</td>
							<td style="width : ${getElSize(105 * 2)}px; background-color : ${noConnColor}; color : black">${noconnection}</td>
						</tr>
					</thead>
				</table>
			</div>
		`
	$("#container").append(div)
	
	$(".tableContainer thead td").css({
		"font-size" : getElSize(18 * 2)
		,"height" : getElSize(66)
	})
}

var jigCsv;
var wcCsv;
var selected_dvc;
var className = "";
var classFlag = true;
var preLine;
var sum_target_op_time = 0;
	sum_total_op_time = 0;
	sum_op_date = 0;
	sum_incycle = 0;
	sum_wait = 0;
	sum_alarm = 0;
	sum_noConn = 0;

const getTableData = (el) =>{
	var id = this.id;

	var sDate;
	var eDate;
	var ty;
	var url = ctxPath + "/chart/getTableData.do";
	if(id=="jig_sdate" || id=="jig_edate" || el=="jig"){
		sDate = $("#jig_sdate").val();
		eDate = $("#jig_edate").val();
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
		
	}else{
		sDate = $("#wc_sdate").val();
		eDate = $("#wc_edate").val();
		
		showWcData(selected_dvc,sDate, eDate);
		
		window.localStorage.setItem("wc_sDate", sDate);
		window.localStorage.setItem("wc_eDate", eDate);
		
		return;
	};
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&ty=" +ty +
				"&shopId=" + shopId + 
				"&group=" + $("#group").val() +
				"&maxRow=" + 200 + 
				"&offset=0";
	
	$.showLoading();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.tableData;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			$(".contentTr").remove();
			var tr = "<tbody>";
			jigData = "${device},${target_op_time},${total_op_time},${number_of_op_day},${incycle},${wait},${alarm},${noconnection},${opratio_against_target}LINE";
			
			var sum_target_op_time = 0;
			var sum_total_op_time = 0;
			var sum_op_date = 0;
			var sum_incycle = 0;
			var sum_wait = 0;
			var sum_alarm = 0;
			var sum_noConn = 0;
			var sum_opRatio = 0;
			var dvcCnt = 0;
			$(json).each(function(idx, data){
				if(preLine != data.line && idx!=0){
					tr += "<tr>" + 
							"<td>" + preLine + " (${sum})</td>" + 
							"<td>" + Math.round(sum_target_op_time) + "</td>" +
							"<td>" + Math.round(sum_total_op_time) + "</td>" + 
							"<td>" + Math.round(sum_op_date) + "</td>" + 
							"<td>" + Math.round(sum_incycle) + "</td>" +
							"<td>" + Math.round(sum_wait) + "</td>" + 
							"<td>" + Math.round(sum_alarm) + "</td>" +
							"<td>" + Math.round(sum_noConn) + "</td>" + 
							"<td>" + (sum_opRatio/dvcCnt).toFixed(1) + "%</td>" + 
						"</tr>";
						
					sum_target_op_time = 0;
					sum_total_op_time = 0;
					sum_op_date = 0;
					sum_incycle = 0;
					sum_wait = 0;
					sum_alarm = 0;
					sum_noConn = 0;
					sum_opRatio = 0;
					
					dvcCnt = 0;
				}
				preLine = data.line;
				dvcCnt++;
				
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				var incycle = Number(Number(data.inCycle_time/60/60/n).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60/n).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60/n).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr " + className + "' ondblclick='showWcData(\"" + data.name + "\","  + "\"" + sDate + "\"," +  "\"" + eDate + "\"" + ",\"jig\")'>" +
							"<td>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</td>" +
							"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + Number(data.inCycle_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + n + "</td>" + 
							"<td>" + incycle + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/(data.target_time)) * 100).toFixed(1) + "%</td>" + 
					 "</tr>";
					 
				jigData += decodeURIComponent(data.name).replace(/\+/gi, " ") + "," + 
						Number(data.target_time/60/60).toFixed(1) + "," +  
						Number(data.inCycle_time/60/60).toFixed(1) + "," + 
						n + "," + 
						incycle + "," + 
						wait + "," + 
						alarm + "," + 
						noconn + "," + 
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
				
				sum_target_op_time += Number(Number(data.target_time/60/60).toFixed(1));
				sum_total_op_time += Number(Number(data.inCycle_time/60/60).toFixed(1));
				sum_op_date += Number(n);
				sum_incycle += Number(incycle);
				sum_wait += Number(wait);
				sum_alarm += Number(alarm);
				sum_noConn += Number(noconn);	
				sum_opRatio += Number(Number((data.inCycle_time/(data.target_time)) * 100).toFixed(1));
			});
			
//			tr+= "<tr>" + 
//					"<td>" + preLine + " (${sum})</td>" + 
//					"<td>" + Math.round(sum_target_op_time) + "</td>" +
//					"<td>" + Math.round(sum_total_op_time) + "</td>" + 
//					"<td>" + Math.round(sum_op_date) + "</td>" + 
//					"<td>" + Math.round(sum_incycle) + "</td>" +
//					"<td>" + Math.round(sum_wait) + "</td>" + 
//					"<td>" + Math.round(sum_alarm) + "</td>" +
//					"<td>" + Math.round(sum_noConn) + "</td>" + 
//					"<td>" + (sum_opRatio/dvcCnt).toFixed(1) + "%</td>" + 
//				"</tr></tbody>";
			
			$("#jigTable").append(tr);
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#F0F0F0",
				"color" : "black",
				"height" : getElSize(40*2)
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#DCDCDC",
				"color" : "black",
				"height" : getElSize(40*2)
			});
			
			//setEl();
			
			$(".tableContainer div:last").remove()
			
			$(".tableContainer td").css({
				"font-size" : getElSize(18 * 2)
			})
			
		
			scrolify($('#jigTable'), getElSize(1400));
			
			$("*").not("#dvcDiv, font, span").css({
				"overflow-x" : "hidden",
				"overflow-y" : "auto"
			});
			
			$("html, body").css({
				"overflow-y" : "hidden"
			});
			
			$.hideLoading();
		}
	});
};


const createSearchDiv = () =>{
	const div = 
		`
			<div
				style=
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(1888 * 2)}px
						; height : ${getElSize(64 * 2)}px
						; top : ${getElSize(194 * 2) + marginHeight}px
						; left : ${getElSize(16 * 2) + marginWidth}px
						; background-color : #2B2D32 
					"
			>
				<select id="group"
					style=
						"
							margin-top : ${getElSize(12 * 2)}px
							; margin-left : ${getElSize(12 * 2)}px
							; display : none
						"
				>
				</select>
				
				<span 
					style=
						"
							position : absolute
							; z-index : 2
							; color : white
							; font-size : ${getElSize(24 * 2)}px
							; top : ${getElSize(22 * 2)}px
							; left : ${getElSize(274 * 2)}px
							; line-height : ${getElSize(24 * 2)}px
						"
				>${op_period}</span>
				
				<input type="date"
					id="jig_sdate"
					style=
						"
							position : absolute
							; z-index : 2
							; top : ${getElSize(14 * 2)}px
							; left : ${getElSize(379 * 2)}px
						"
				>
				
				<input type="date"
					id="jig_edate"
					style=
						"
							position : absolute
							; z-index : 2
							; top : ${getElSize(14 * 2)}px
							; left : ${getElSize(671 * 2)}px
						"
				>
				
				<div
					onclick="getTableData('jig');"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(120 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; background-color : #9B9B9B
							; font-size : ${getElSize(24 * 2)}px
							; top : ${getElSize(12 * 2)}px
							; border-radius : ${getElSize(2 * 2)}px
							; text-align:center
							; left : ${getElSize(941 * 2)}px
							; display : table
							; cursor : pointer
						"
				>
					<img src="${ctxPath}/images/FL/default/ico_search.svg" style="width : ${getElSize(23 * 2)}px; margin:${getElSize(8 * 2)}px; margin-right:0px;  margin-left:0px">
					<span style="display:table-cell; vertical-align:middle">${Search}</span>
				</div>
				
				<img src="${ctxPath}/images/FL/btn_graph_default.svg"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(40 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; cursor : pointer
							; top : ${getElSize(190 * 2) + marginHeight - $("#container").offset().top}px
							; left : ${getElSize(1776 * 2) + marginWidth - $("#container").offset().left}px
						"
						onclick="goGraph()"
				>
				
				<img src="${ctxPath}/images/FL/btn_table_pushed.svg"
					style=
						"
							position : absolute
							; z-index : 2
							; width : ${getElSize(40 * 2)}px
							; height : ${getElSize(40 * 2)}px
							; cursor : pointer
							; top : ${getElSize(190 * 2) + marginHeight - $("#container").offset().top}px
							; left : ${getElSize(1832 * 2) + marginWidth - $("#container").offset().left}px
						"
						
				>
			</div>
		`
	

	$("#container").append(div)	
	
	$("#jig_sdate").val(caldate(7))
	$("#jig_edate").val(caldate(0))
	setDateDesign()
}

const goGraph = () => {
	location.href = ctxPath + "/chart/jigGraph.do"
};

