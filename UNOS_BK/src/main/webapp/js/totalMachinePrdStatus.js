const loadPage = () =>{
	createMenuTree("monitoring", "totalMachinePrdStatus")
	
	$("#svg").css({
    	"position" : "relative",
    	
    	"width" : $("#container").width(),
    	"height" : $("#container").height()
    	
    });
	
	createDateInput()
	setEl();
	bindMyEvt()
	
	getStartTime()
	createStatusLabel()
	
	
	
	drawGroupDiv();

	
	
}
	
const createDateInput = () =>{
	const sDate = 
		`
			<input type="hidden" id="sDate" value="${getToday().substr(0,10)}">
		`
	$("#container").append(sDate)	
}

let dateInterval = null;

const getStartTime = () =>{
	var url = ctxPath + "/chart/getStartTime.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			startHour = Number(data.split("-")[0])
			startMinute = data.split("-")[1] * 10;
			
			createMachine()
		}, error : function(e1,e2,e3){ 
			console.log(e1,e2,e3)
		}
	});	
};


const drawGroupDiv = () =>{
	//LFA RR
	var table = "<table class='gr_table' id='lfa_rr'>" + 
					"<tr>" + 
						"<Td class='label'>MC ED / FFM<br>VF LS</td>" +
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" +
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#lfa_rr").css({
		"left" : getElSize(265) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#lfa_rr").css({
		"width" : getElSize(190)
	});
	
	//YP FRT (cnc)
	var table = "<table class='gr_table' id='yp_frt'>" + 
					"<tr>" + 
						//"<Td class='label'>YP FRT (CNC)<br>내수, 북미</td>" +
						"<Td class='label'>EN ACF<br>AC WLV 14 / 11</td>" +
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#yp_frt").css({
		"left" : getElSize(730 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#yp_frt td").css({
		"width" : getElSize(250)
	});

	//UM FRT (CNC)
	var table = "<table class='gr_table' id='um_frt'>" + 
					"<tr>" + 
						"<Td class='label'>CC RKF 16 / QV BG /<br>EF LL 16</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#um_frt").css({
		"left" : getElSize(1030 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#um_frt td").css({
		"width" : getElSize(400)
	});
	
	//TA RR
	var table = "<table class='gr_table' id='ta_Rr'>" + 
					"<tr>" + 
						"<Td class='label'>QMF VM /<br>AA WOL</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#ta_Rr").css({
		"left" : getElSize(1490 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#ta_Rr td").css({
		"width" : getElSize(250)
	});
	
	//TA FRT
	var table = "<table class='gr_table' id='ta_frt'>" + 
					"<tr>" + 
						"<Td class='label'>SM QKF /<br>VL NQO</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#ta_frt").css({
		"left" : getElSize(1800 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#ta_frt td").css({
		"width" : getElSize(250)
	});
	
	
	//JC RR
	var table = "<table class='gr_table' id='jc_rr'>" + 
					"<tr>" + 
						"<Td class='label'>LQ LLX /<br>MF RKG</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#jc_rr").css({
		"left" : getElSize(2100 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#jc_rr td").css({
		"width" : getElSize(250)
	});
	
	//
	var table = "<table class='gr_table' id='jc_rr2'>" + 
					"<tr>" + 
						"<Td class='label'>VL QQ 자동</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#jc_rr2").css({
		"left" : getElSize(2400 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#jc_rr2 td").css({
		"width" : getElSize(250)
	});
	
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu'>" + 
					"<tr>" + 
						"<Td class='label'>CM ED</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu").css({
		"left" : getElSize(2700 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu td").css({
		"width" : getElSize(300)
	});
	
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu2'>" + 
					"<tr>" + 
						"<Td class='label'>QE KRM 13</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu2").css({
		"left" : getElSize(3040 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu2 td").css({
		"width" : getElSize(250)
	});
	
	//HR PU/FRT
	var table = "<table class='gr_table' id='hr_pu3'>" + 
					"<tr>" + 
						"<Td class='label'>MS EKF</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#hr_pu3").css({
		"left" : getElSize(3340 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#hr_pu3 td").css({
		"width" : getElSize(250)
	});
	
	
	//TQ FRT
	var table = "<table class='gr_table' id='tq_frt'>" + 
					"<tr>" + 
						"<Td class='label'>EV VFV /<br>RF BM</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";

	$("#container").append(table);
			
	$("#tq_frt").css({
		"left" : getElSize(3640 - 235) + marginWidth,
		"top" : getElSize(380 - 20) + marginHeight,	
	});
	
	$("#tq_frt td").css({
		"width" : getElSize(190)
	});
	
	
	
	
	
	
	var table = "<table class='gr_table' id='yp_rr'>" + 
						"<tr>" + 
							"<Td class='label'>EPB 16 / EPB 15</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
				
				$("#container").append(table);
				
				$("#yp_rr").css({
					"left" : getElSize(2030  - 235) + marginWidth,
					"top" : getElSize(1180 - 10) + marginHeight,	
				});
				
				$("#yp_rr td").css({
					"width" : getElSize(470)
				});

				
	var table = "<table class='gr_table' id='yp_rr2'>" + 
				"<tr>" + 
					"<Td class='label'>RM SK LV/<br>RM CD RF</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr2").css({
			"left" : getElSize(2550 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr2 td").css({
			"width" : getElSize(450)
		});
		
		
		var table = "<table class='gr_table' id='yp_rr3'>" + 
				"<tr>" + 
					"<Td class='label'>EMF LV</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr3").css({
			"left" : getElSize(3040 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr3 td").css({
			"width" : getElSize(250)
		});
	
		
		var table = "<table class='gr_table' id='yp_rr4'>" + 
				"<tr>" + 
					"<Td class='label'>CM QD / LV LCQ</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" + 
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#yp_rr4").css({
			"left" : getElSize(3340 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr4 td").css({
			"width" : getElSize(250)
		});
		
		
		//UM FRT
		var table = "<table class='gr_table' id='yp_rr5'>" + 
		"<tr>" + 
			"<Td class='label'>FM QEK /<br>CV OKL</td>" + 
		"</tr>" + 
		"<tr>" + 
			"<Td class='icon'></td>" + 
		"</tr>" +
		"</table>";
	
		$("#container").append(table);
		
		$("#yp_rr5").css({
			"left" : getElSize(3640 - 235) + marginWidth,
			"top" : getElSize(1180 - 10) + marginHeight,	
		});
		
		$("#yp_rr5 td").css({
			"width" : getElSize(190)
		});
		
	
	var table = "<table class='gr_table' id='tmp'>" + 
			"<tr>" + 
				"<Td class='label'>SAMPLE</td>" + 
			"</tr>" + 
			"<tr>" + 
				"<Td class='icon'></td>" +
			"</tr>" +
		"</table>";
	
	$("#container").append(table);
	
	$("#tmp").css({
		"left" : getElSize(500 - 235) + marginWidth,
		"top" : getElSize(1180 - 10) + marginHeight,	
	});
	
	

	
	$(".gr_table").css({
		"position" : "absolute",
		"border-spacing" : "0px",
		//"border-collapse" : "collapse",
		"z-index" : 1
	});
	
	
	
	
	$(".gr_table .label").css({
		"background-color" : "#373737",
		"text-align" : "center",
		"height" : getElSize(70),
		//"border" : getElSize(3) + "px solid #535556",
		"border-top-left-radius" : getElSize(4 * 2) + "px",
		"border-top-right-radius" : getElSize(4 * 2) + "px",
	});
	
	$(".gr_table .icon").css({
		//"height" : getElSize(530),
		"height" : getElSize(730),
		"border-left" : getElSize(3) + "px solid #535556",
		"border-right" : getElSize(3) + "px solid #535556",
		"border-bottom" : getElSize(3) + "px solid #535556",
		"border-bottom-left-radius" : getElSize(4 * 2) + "px",
		"border-bottom-right-radius" : getElSize(4 * 2) + "px",

	});
	
	$("#tmp .icon").css({
		"width" : getElSize(190),
		"height" : getElSize(630)
	});
	
	$("#lfa_rr .icon,#yp_frt .icon,  #ta_frt .icon, #jc_rr .icon, #hr_pu .icon").css({
		"height" : getElSize(730),

	});
	
	$("#tq_frt .icon").css({
		"height" : getElSize(1700),

	});
	
	$("#yp_rr, #yp_rr2, #yp_rr3, #yp_rr4, #yp_rr5, #epb16").css({
		"height" : getElSize(980),
	})
	
	$("#fs_rr .icon").css({
		"height" : getElSize(790),

	});
	
	$(".gr_table td").css({
		"color" : "white",
		"font-size" : getElSize(30),
	});
	
	
};

let isTodayN = false;
let nd;
let startHour, startMinute;

const createMachine = (btnNd) =>{
	if(isTodayN==false){
		$( "#sDate" ).datepicker( "option", "disabled", true );
	}else{
		$( "#sDate" ).datepicker( "option", "disabled", false );
	}
	clearInterval(dateInterval);
	
	const url = ctxPath + "/chart/getMachineInfo.do";
	
	var date = new Date();
	var year = date.getFullYear();
	
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	
	var minute = date.getMinutes();

	
	var target_time_n = (startHour - 12) * 60 + startMinute;
	var target_time_d = startHour * 60 + startMinute;
	var current_time = hour * 60 + minute;
	
	var isToday = true;
	
	if(getToday().substr(0,10) != today){
		isToday = false;
	}
	var worker;
	var prdctRatio;
	
	
	var day = moment(new Date(moment().year(), moment().month(), moment().date(), 8, 30, 0));
	var night = moment(new Date(moment().year(), moment().month(), moment().date(), 20, 30, 0));
	
	if(btnNd==1){
		nd=btnNd;
		$("#nightWorkBtn").css({
			"background" : "#004080"
		})
		$("#dayWorkBtn").css({
			"background" : "linear-gradient(#007FFE, #004080)"
		})
	}else if(btnNd==2){
		nd=btnNd;
		$("#nightWorkBtn").css({
			"background" : "linear-gradient(#007FFE, #004080)"
		})
		$("#dayWorkBtn").css({
			"background" : "#004080"
		})
	}
	
	if(isTodayN==false){
		if(moment().isAfter(day)){
    		if(moment().isAfter(night)){
    			nd=1;
    			$('input:radio[name="nd"][value="1"]').prop('checked', true);
    			$("#nightWorkBtn").css({
    				"background" : "#004080"
    			})
    			$("#dayWorkBtn").css({
    				"background" : "linear-gradient(#007FFE, #004080)"
    			})
    			$("input#sDate").val(moment().format("YYYY-MM-DD"))
    		}else{
    			nd=2;
    			$("#nightWorkBtn").css({
    				"background" : "linear-gradient(#007FFE, #004080)"
    			})
    			$("#dayWorkBtn").css({
    				"background" : "#004080"
    			})
    			$('input:radio[name="nd"][value="2"]').prop('checked', true);
    			$("input#sDate").val(moment().format("YYYY-MM-DD"))
    		}
    	}else{
    		nd=1;
    		$('input:radio[name="nd"][value="1"]').prop('checked', true);
    		$("input#sDate").val(moment().subtract('day', 1).format("YYYY-MM-DD"))
    	}
	}else{
		
	}
	
	var today = $("#sDate").val();
	//var today = getToday().substr(0,10).replace(/\./gi, "-")
	var param = "shopId=" + shopId + 
				"&startDateTime=" + today + 
				//"&nd=" + nd;
				"&nd=" + 2;

	
	console.log(param)
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : (data) => {
			incycleCnt = 0;
			waitCnt= 0;
			alarmCnt= 0;
			noConnCnt = 0;
			
			var json = data.machineList;
			
			$(".box").remove();
			

			var machineStatus;
			var cntCyl;
			
			maxTime = new Date(json[0].workDate.substr(0,4), json[0].workDate.substr(5,2), json[0].workDate.substr(8,2), json[0].workDate.substr(11,2),json[0].workDate.substr(14,2), json[0].workDate.substr(17,2))
			
			
			$(json).each(function(idx, data){
				var time = data.workDate;
				var uDate = new Date(time.substr(0,4), time.substr(5,2), time.substr(8,2), time.substr(11,2),time.substr(14,2), time.substr(17,2))
				
				if(maxTime<uDate) maxTime = uDate
				
				
				if(moment().format("YYYY-MM-DD")==$("#sDate").val()){
					
					if(nd==2){
						if(hour>=8 && hour<=20){
							cntCyl =  Math.round(data.tgCyl * (hour - (startHour - 12)) /12)
						}else{
							cntCyl =  data.tgCyl
						}
					}else{
						if(hour<=8){
							cntCyl =  Math.round(data.tgCyl * ((hour+24) - startHour)/12)
						}else if(hour>=20){
							cntCyl =  Math.round(data.tgCyl * (hour - startHour)/12)
						}else{
							cntCyl =  data.tgCyl
						}
					} 
				}else{
					if(nd==2){
						cntCyl =  data.tgCyl
					}else{
						cntCyl =  data.tgCyl
					}
				}
			   //현시간 기준 목표 수량 (주간인 경우 : 주간목표 수량 X (현재시간 – 주간시작시간) / 12 )
			   /*if(nd==2){
					cntCyl =  data.tgCyl
				}else{
					cntCyl =  data.tgCyl
				} */
				
				worker = decode(data.worker);
				
				prdctRatio = Math.round(data.cntCyl / cntCyl * 100)

				if(cntCyl == 0 || data.cntCyl == 0){
					prdctRatio = 0;
				}
				
				var prdctRatiofontColor = "black";
				var prdctRatioColor;
				if(prdctRatio>=100){
					prdctRatioColor = "#004110";	
					prdctRatiofontColor = "white";
				}else if(prdctRatio>=80){
					prdctRatioColor = "green";
				}else if(prdctRatio>=60){
					prdctRatioColor = "yellow";
				}else if(prdctRatio==0 &&  data.cntCyl==0){
					prdctRatioColor = "rgba(255, 0, 0, 0);";
				}else{
					prdctRatioColor = "red";
				}
				
				if(data.lastChartStatus=="IN-CYCLE"){
					incycleCnt++;
					machineStatus = incycleColor;
				}else if(data.lastChartStatus=="WAIT"){
					waitCnt++;
					machineStatus = waitColor;
				}else if(data.lastChartStatus=="ALARM"){
					alarmCnt++;
					machineStatus = alarmColor;
				}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
					noConnCnt++;
					machineStatus = noConnColor;
				};
				
				var box = document.createElement("div");
				box.setAttribute("class", "box");
				box.setAttribute("id", "box" + data.dvcId);
				box.setAttribute("boxId", data.dvcId);	
				
				//수정 박스사이즈 수정
				
				if(data.dvcId==88 || data.dvcId==83 || data.dvcId==84 || data.dvcId==85 || data.dvcId==87 || data.dvcId==27 || data.dvcId==86 || data.dvcId==99){
					var boxWidth = getElSize(140);
					var boxHeight = getElSize(100);
				}else{
					var boxWidth = getElSize(100);
					var boxHeight = getElSize(140);
				}

				box.style.cssText = "position : absolute;" + 
									"width:" + boxWidth + ";" + 
									"z-index : 99;" + 
									"font-size : " + getElSize(20) + "px;" + 
									"height : " + boxHeight + ";" +
									"top : " + (getElSize(data.y)) + "px;" + 
									"left : " + (getElSize(data.x)) + "px;";
				
				

				var cntCylText = document.createTextNode(decode(worker));
				var cntCyl = document.createTextNode(" (" + cntCyl + ")");
				var br = document.createElement("br");
				//var cntCylText = document.createTextNode(data.dvcId + " (" + cntCyl + ")");
				var cntCylSpan = document.createElement("span");
				cntCylSpan.style.cssText = "display : table-cell;" +
										   "white-space : nowrap;" + 
										 //  "border : 1px solid black;" + 
										   "border-bottom : 0px;"+
										   "vertical-align:middle";
				
				cntCylSpan.append(cntCylText);
				cntCylSpan.appendChild(br);
				cntCylSpan.append(cntCyl);
				
				
				var statusBox = document.createElement("div");
				statusBox.setAttribute("id", "m" + data.dvcId);
				statusBox.style.cssText = "width : " + boxWidth + ";" + 
											"height : " + boxHeight/2 + ";" +
											"font-size: " + getElSize(30) +"px;" +  
											"font-weight : bolder;" + 
											"display : table;" +
											"color : black;" + 
											"text-align : center;" +
											"background-color : " + machineStatus + ";" + 
											"border-top-left-radius : " + getElSize(4 * 2) + "px;" + 
											"border-top-right-radius : " + getElSize(4 * 2) + "px;"
				
				statusBox.append(cntCylSpan)
				$(box).append(statusBox)
				
				
				var prdctText = document.createTextNode(data.cntCyl);
				var prdctSpan = document.createElement("span");
				prdctSpan.style.cssText = "display : table-cell;" +
											"vertical-align:middle";
				
				prdctSpan.append(prdctRatio + "% ")
				prdctSpan.append("(" + data.cntCyl + ")")
				//prdctSpan.append(document.createElement("br"))
				
				
				
				var prdctBox = document.createElement("div");
				prdctBox.setAttribute("id", "m" + data.dvcId);
				prdctBox.style.cssText = "width : " + boxWidth + ";" +
											"font-size:" + getElSize(30) +"px;" +
											"height : " + boxHeight/2 + ";" + 
											"display : table;" +
											"font-weight:bolder;" + 
											"color : " + prdctRatiofontColor + ";" +
											"text-align : center;" + 
											"overflow : visible;" + 
											"background-color : " + prdctRatioColor + ";" + 
											"border-bottom-left-radius : " + getElSize(4 * 2) + "px;" + 
											"border-bottom-right-radius : " + getElSize(4 * 2) + "px;"
								
				
				prdctBox.append(prdctSpan);
				$(box).append(prdctBox);
				
				
				$("#svg").append(box);
				
				$(box).dblclick(function(){
					window.localStorage.setItem("dvcId", data.dvcId);
					window.localStorage.setItem("name", decode(data.name));
					
					let url = "";
					if(appTy == "auto"){
						url = ""
					}else{
						url = "_STD"
					}
//					location.href = `/iDOO${url}_Single_Chart_Status/index.do?fromDashBoard=true&lang=${lang}`
					location.href = ctxPath + `/chart/singleChartStatus.do?fromDashBoard=true&lang=${lang}`

				});
				
				
			})
			
			$("#incycleCnt").html(incycleCnt)
			$("#waitCnt").html(waitCnt)
			$("#alarmCnt").html(alarmCnt)
			$("#noConnCnt").html(noConnCnt)
			
			$("#totalCnt").html((incycleCnt + waitCnt + alarmCnt + noConnCnt))
			
			//$("#container").append(boxes)
			
			$(".machine").hover((el)=>{
				$(el.currentTarget).css({
					"background-color" : "white"
				})
			}, (el)=>{
				let status = $(el.currentTarget).attr("status")
				let bgColor = ""
					
				if(status == "IN-CYCLE"){
					bgColor = incycleColor;
				}else if(status == "WAIT"){
					bgColor = waitColor;
				}else if(status == "ALARM"){
					bgColor = alarmColor;
				}else if(status == "NO-CONNECTION"){
					bgColor = noConnColor;
				}
				
				$(el.currentTarget).css({
					"background-color" : bgColor
				})
			}).click((el) =>{
				const id = $(el.currentTarget).attr("dvcId")
				const name = $(el.currentTarget).attr("dvcName")
				
				window.localStorage.setItem("dvcId", id);
				window.localStorage.setItem("name", name);
				
				//location.href=ctxPath + "/chart/chart/singleChartStatus.do?fromDashBoard=true";
			
				let url = "";
				if(appTy == "auto"){
					url = ""
				}else{
					url = "_STD"
				}
//				location.href = `/iDOO${url}_Single_Chart_Status/index.do?fromDashBoard=true&lang=${lang}`
				location.href = ctxPath + `/chart/singleChartStatus.do?fromDashBoard=true&lang=${lang}`
				
				console.log(id)
			})
		}, error : (e1,e2,e3) =>{
			console.log(e1,e2,e3)
		}
	});
};

const createStatusLabel = () =>{
	const img = 
		`
			<img src="${ctxPath}/images/FL/layout_view_info.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; height : ${getElSize(75 * 2)}px
					; top : ${getElSize(939 * 2) + marginHeight}px
					; left : ${getElSize(48 * 2) + marginWidth}px
				"
			>		
		`
		
	$("#container").append(img)
	
	const cutting_title = 
		`
			<span
				style=
				"
					font-size : ${getElSize(12 * 2)}px
					; color : white
					; position : absolute
					; z-index : 2
					; top : ${getElSize(1014 * 2) + marginHeight}px
					; left : ${getElSize(50 * 2) + marginWidth}px
				"
			>CUTTING</span>
		`
		
	const wait_title = 
		`
			<span
				style=
				"
					font-size : ${getElSize(12 * 2)}px
					; color : white
					; position : absolute
					; z-index : 2
					; top : ${getElSize(1014 * 2) + marginHeight}px
					; left : ${getElSize(116 * 2) + marginWidth}px
				"
			>WAITING</span>
		`
			
	const alarm_title = 
		`
			<span
				style=
				"
					font-size : ${getElSize(12 * 2)}px
					; color : white
					; position : absolute
					; z-index : 2
					; top : ${getElSize(1014 * 2) + marginHeight}px
					; left : ${getElSize(185 * 2) + marginWidth}px
				"
			>ALARM</span>
		`
				
	const noConn_title = 
		`
		<span
			style=
			"
				font-size : ${getElSize(12 * 2)}px
				; color : white
				; position : absolute
				; z-index : 2
				; top : ${getElSize(1014 * 2) + marginHeight}px
				; left : ${getElSize(257 * 2) + marginWidth}px
			"
		>OFF</span>
	`
	const total_title = 
		`
		<span
			style=
			"
				font-size : ${getElSize(12 * 2)}px
				; color : white
				; position : absolute
				; z-index : 2
				; top : ${getElSize(1014 * 2) + marginHeight}px
				; left : ${getElSize(313.5 * 2) + marginWidth}px
			"
		>TOTAL</span>
	`		
	$("#container").append(cutting_title, wait_title, alarm_title, noConn_title, total_title)
	
	const incycleVal = 
		`
			<span id="incycleCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #AED543
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(60 * 2) + marginWidth}px
				"
			></span>
		`
	const waitVal = 
		`
			<span id="waitCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #F19537
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(123 * 2) + marginWidth}px
				"
			></span>
		`	
		
	const alarmVal = 
		`
			<span id="alarmCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #B42F1A
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(194 * 2) + marginWidth}px
				"
			></span>
		`
		
	const noConnVal = 
		`
			<span id="noConnCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #AEAEAF
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(250 * 2) + marginWidth}px
				"
			></span>
		`
		
	const totalVal = 
		`
			<span id="totalCnt"
				style=
				"
					font-size : ${getElSize(32 * 2)}px
					; color : #ffffff
					; position : absolute
					; z-index : 2
					; top : ${getElSize(967 * 2) + marginHeight}px
					; left : ${getElSize(305 * 2) + marginWidth}px
				"
			></span>
		`
	$("#container").append(incycleVal, waitVal, alarmVal, noConnVal, totalVal)

}

const setEl = () =>{
	
};
	
const bindMyEvt = () =>{
	
}

