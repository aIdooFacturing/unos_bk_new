let appTy = "auto"
var appVer = "v.1.0 ";
var shopId = 1;

var targetWidth = 3840;
var targetHeight = 2160;

var originWidth = window.innerWidth;
var originHeight = window.innerHeight;

var contentWidth = originWidth;
var contentHeight = targetHeight/(targetWidth/originWidth);

var screen_ratio = getElSize(240);

if(originHeight/screen_ratio<9){
	contentWidth = targetWidth/(targetHeight/originHeight)
	contentHeight = originHeight; 
};

function getElSize(n){
	return contentWidth/(targetWidth/n);
};

function setElSize(n) {
	return Math.floor(targetWidth / (contentWidth / n));
};

var marginWidth = (originWidth-contentWidth)/2;
var marginHeight = (originHeight-contentHeight)/2;


const cutColor = "#3D8522"
const incycleColor = "#AED543"
const waitColor = "#F19537"
const alarmColor = "#B42F1A"
const noConnColor = "#AEAEAF"


const chkKeyEvt = (evt) =>{
	if(evt.keyCode == 13){
		login()
	}
}

function login(){
	var url = ctxPath + "/chart/login.do";
	var $id = $("#id").val();
	var $pwd = $("#pwd").val();
	var param = "id=" + $id + 
				"&pwd=" + $pwd + 
				"&shopId=" + shopId;
	
	$.showLoading()
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$.hideLoading();
			
			console.log(data.message)
			if(data.message=="success"){
				setCookie("login_time", new Date().getTime(), 1);
				setCookie("login", true, 1);
				setCookie("user_id", $id, 1);
				
				//loginSuccess();
				//drawLogout();
				location.href = ctxPath + "/chart/main.do"
			}else{
				alert("계정 정보가 올바르지 않습니다.")
				//$("#errMsg").html("계정 정보가 올바르지 않습니다.")
			}
		}
	});
};


function chkKeyCd(evt){
	if(evt.keyCode==13) login();
};


let appName = "";
	


const monitor_menu_map = new JqMap();
monitor_menu_map.put("Dashboard", [layout, ctxPath + "/chart/dashBoard.do"])
monitor_menu_map.put("totalMachinePrdStatus", [total_machine_prd_status, ctxPath + "/chart/totalMachinePrdStatus.do"])
//monitor_menu_map.put("beyerMonitoring", ["베이어 모니터링", ctxPath + "/chart/beyerMonitoring.do"])
monitor_menu_map.put("Single_Chart_Status", [devicestatus, ctxPath + "/chart/singleChartStatus.do"])
monitor_menu_map.put("24hrChart", [barchart, ctxPath + "/chart/main3.do"])
monitor_menu_map.put("prdStatus", [prdct_board, ctxPath + "/chart/prdStatus.do"])

const analysis_menu_map = new JqMap();
analysis_menu_map.put("performanceAgainstGoal_chart_kpi", [performance_chart, ctxPath + "/chart/performanceAgainstGoal_chart_kpi.do"])
analysis_menu_map.put("Performance_Report_Chart", [operation_chart, ctxPath + "/chart/jigGraph.do"])
analysis_menu_map.put("Performance_Report_Daily", [operation_graph_daily, ctxPath + "/chart/wcGraph.do"])
analysis_menu_map.put("FPAnalysis", ["FP 발생 분석", ctxPath + "/chart/FPAnalysis.do"])
/*analysis_menu_map.put("MTBF_MTTR", ["MTBF / MTTR", "/iDOO_MTBF_MTTR/chart/MTBF_MTTR.do"])
analysis_menu_map.put("productionStatusKpi", ["OEE 종합", "/iDOO_OM/kpi/productionStatusKpi.do"])*/

const tm_menu_map = new JqMap();
tm_menu_map.put("toolLifeManager", [tool_manage, ctxPath + "/chart/toolLifeManager.do"])
tm_menu_map.put("programManager_tool", [check_program_error, ctxPath + "/chart/programManager_tool.do"])

const maintenance_map = new JqMap();

maintenance_map.put("Device_Status", [machine_list, ctxPath + "/chart/traceManager.do"])
maintenance_map.put("banner", ["Catch Phrase", ctxPath + "/chart/banner.do"])
maintenance_map.put("workerMaanger", [worker_manage, ctxPath + "/chart/workerMaanger.do"])
maintenance_map.put("routingManager", [routing_Manager, ctxPath + "/chart/routingManager.do"])
maintenance_map.put("mstmat", [manage_Item_Master, ctxPath + "/chart/mstmat.do"])
//maintenance_map.put("request_Repair", ["장비 수리 요청", ctxPath + "/chart/request_Repair.do"])
maintenance_map.put("getMaintenanceReportListPage", [maintenance_Request_List, ctxPath + "/chart/getMaintenanceReportListPage.do"])
maintenance_map.put("SupplierManage", ["납품리스트 관리", ctxPath + "/chart/SupplierManage.do"])
//maintenance_map.put("TVDeviceGroup", ["TV Manager", ctxPath + "/chart/TVDeviceGroup.do"])
//maintenance_map.put("Alarm_Manager", [alarm_manage, ctxPath + "/chart/alarmReport.do"])
//maintenance_map.put("as", ["두산서비스요청", ctxPath + "/chart/getMachineList.do"])

const im_map = new JqMap();
im_map.put("delivery", [delivery_Plan, ctxPath + "/chart/delivery.do"])
im_map.put("incomeStock", [income_manage, ctxPath + "/chart/incomeStock.do"])
im_map.put("transferLine", [release_manage, ctxPath + "/chart/transferLine.do"])
im_map.put("transferOpr2", [Movestock, ctxPath + "/chart/transferOpr2.do"])
im_map.put("exportManagement", [ExportManagement, ctxPath + "/chart/exportManagement.do"])
im_map.put("importManagement", [Importmanagement, ctxPath + "/chart/importManagement.do"])
im_map.put("shipment2", [ship_manage, ctxPath + "/chart/shipment2.do"])
im_map.put("StockHistory", [Historysearch, ctxPath + "/chart/StockHistory.do"])
im_map.put("stockStatus2", [stock_status, ctxPath + "/chart/stockStatus2.do"])
im_map.put("stockStatusDay2", [Inventory_Master, ctxPath + "/chart/stockStatusDay2.do"])
//im_map.put("stockUptPg", ["재고 수정", "/iDOO_IM/chart/stockUptPg.do"])

//const im_menu_map = new JqMap();
const pm_menu_map = new JqMap();
pm_menu_map.put("fileUpDown", [fileUpDown, "/iDOO_PM/chart/fileUpDown.do"])

const kpi_menu_map = new JqMap();
kpi_menu_map.put("productionStatusKpi_backUp", [total_status, ctxPath + "/kpi/productionStatusKpi_backUp.do"])
kpi_menu_map.put("productionKpi", [product_rank, ctxPath + "/kpi/productionKpi.do"])
kpi_menu_map.put("alarmReport", [Manage_alarm_actions, ctxPath + "/chart/alarmReport.do"])
//kpi_menu_map.put("reOpCycle", [reoperation_cycle, ctxPath + "/chart/reOpCycle.do"])
kpi_menu_map.put("reOpCycle", ["이상 가공 현황", ctxPath + "/chart/reOpCycle.do"])
kpi_menu_map.put("faulty", ["가공 불량 현황", ctxPath + "/chart/faulty.do"])
kpi_menu_map.put("operationStatus", [operationStatusRank, ctxPath + "/kpi/operationStatus.do"])
//kpi_menu_map.put("analysisKpi", ["생산 실적 분석", ctxPath + "/kpi/analysisKpi.do"])
kpi_menu_map.put("inventoryKpi", ["생산 실적 분석", ctxPath + "/kpi/inventoryKpi.do"])
kpi_menu_map.put("FPAction", ["FP 조치 관리", ctxPath + "/kpi/FPAction.do"])
kpi_menu_map.put("productionStatusKpi", ["　", ctxPath + "/kpi/productionStatusKpi.do"])

const qm_menu_map = new JqMap();

qm_menu_map.put("checkPrdct", ["초중종물 검사 기준", ctxPath + "/chart/checkPrdct.do"])
qm_menu_map.put("checkPrdctStandard", ["초중종물 검사 결과", ctxPath + "/chart/checkPrdctStandard.do"])
qm_menu_map.put("addFaulty", [add_faulty, ctxPath + "/chart/addFaulty.do"])
qm_menu_map.put("addFaultyHistory", [add_faulty_hist, ctxPath + "/chart/addFaultyHistory.do"])
qm_menu_map.put("lotTracer", ["로트 추적 조회", ctxPath + "/chart/lotTracer.do"])
qm_menu_map.put("instrumentManager", ["계측기 관리", ctxPath + "/chart/instrumentManager.do"])

const om_map = new JqMap();
om_map.put("addTarget", [add_prdct_target, ctxPath + "/order/addTarget.do"])
om_map.put("addTargetDetail", ["목표 수량 상세 관리", ctxPath + "/order/addTargetDetail.do"])
om_map.put("workingReportTotalMenu", ["작업 보고 종합", ctxPath + "/order/workingReportTotalMenu.do"])
om_map.put("getAttendanceList", ["근태 요청 승인", ctxPath + "/order/getAttendanceList.do"])
om_map.put("getPartCyl", ["Part Cycle현황", ctxPath + "/order/getPartCyl.do"])
om_map.put("getGoWorkList", ["출퇴근 이력조회", ctxPath + "/order/getGoWorkList.do"])


const config_map = new JqMap();

config_map.put("mstmat", ["생산 품번 관리", "/iDOO_Conf/chart/mstmat.do"])
config_map.put("routingManager", ["생산 공정 관리", "/iDOO_Conf/chart/routingManager.do"])
config_map.put("account_Setting", ["계정 관리", "/iDOO_Conf/chart/account_Setting.do"])
config_map.put("status-Setting", ["상태 로직 관리", "/iDOO_Conf/chart/status-Setting.do"])
config_map.put("lampExpression", ["램프 신호 관리", "/iDOO_Conf/chart/lampExpression.do"])
config_map.put("layout_Setting", ["레이아웃 관리", "/iDOO_Conf/chart/layout_Setting.do"])
config_map.put("ETC_Setting", ["기타 정보 설정", "/iDOO_Conf/chart/ETC_Setting.do"])
config_map.put("catch phrase", ["Catch phrase", "/iDOO_Conf/chart/banner.do"])

const menu_tree = new JqMap()
menu_tree.put("monitoring",monitor_menu_map)
menu_tree.put("analysis", analysis_menu_map)
menu_tree.put("pm", pm_menu_map)
menu_tree.put("kpi",kpi_menu_map)
menu_tree.put("qm", qm_menu_map)
menu_tree.put("tm", tm_menu_map)
menu_tree.put("om", om_map)
menu_tree.put("maintenance",maintenance_map)
menu_tree.put("config", config_map)
menu_tree.put("im", im_map)
	
const createMenuTree = (menu, cat) =>{
	drawMainIcon(menu)
	
	const catList = menu_tree.get(menu).keys()
	
	let cat_div = 
		`
			<div id="cat_div" style="
				width : ${getElSize(1920 * 2)}px
				; height : ${getElSize(64 * 2)}px
				; top : ${getElSize(115 * 2) + marginHeight}px
				; left : ${getElSize(0) + marginWidth}px
				; position : absolute
				; font-family: NotoSansCJKkrDemiLight
			"
			></div
		`
	$("#container").append(cat_div)
		
	let cat_title = ""
	$(catList).each((idx, data) =>{
		let color = "white";
		
		let firstMargin = 0
		let fontFamily = "NotoSansCJKkrLight"
		let class_name = "unSelected"
		
			if(data.toLowerCase() == cat.toLowerCase()){
			color = "#00C6FF";
			fontFamily = "NotoSansCJKkrBold";
				
			class_name = "selected"
		}
		
		if(idx == 0){
			firstMargin = getElSize(286 * 2)
		}
		
		
		cat_title += 
			`
			<div 
				class="cat ${class_name}" 
				style="
					color : ${color}
					; font-size : ${getElSize(24 * 2)}px
					
					; margin-left : ${firstMargin}
					; font-family: ${fontFamily}
					
					; padding-left : ${getElSize(20 * 2)}px
					; padding-right : ${getElSize(20 * 2)}px
					
					
					; display : table
					; float : left
					; cursor : pointer
					
							
					
					; height : ${$("#cat_div").height()}px 
				"
				
				 onclick="movePage('${menu_tree.get(menu).get(data)[1]}')"
				>
					<span style="display : table-cell; ">${menu_tree.get(menu).get(data)[0]}</span>
			</div>
			
			`
	})
	
	$("#cat_div").append(cat_title)	
	
	$(".cat").hover((el) =>{
		$(el.currentTarget).css({
			"background-color" : "00C6FF",
			"color" : "black",
			//"font-weight" : "bolder"
		})
	}, (el) =>{
		let color = "#ffffff";
		let fontWeight = "normal";
			
		if($(el.currentTarget).hasClass("selected")){
			color = "#00C6FF";
			fontWeight = "bolder";
		}	
		$(el.currentTarget).css({
			"background-color" : "rgba(0,0,0,0)",
			"color" : color,
			//"font-weight" : fontWeight
		})
	})
	
	setElement()
}

const movePage = (url) =>{
	location.href = url + "?lang=" + lang
}

const indexPath = ctxPath ;


function drawFlag(){
	var ko = "<img src=" + ctxPath + "/images/ko.png id='ko' class='flag'/>";
	var cn = "<img src=" + ctxPath + "/images/cn.png id='cn' class='flag'/>";
	var en = "<img src=" + ctxPath + "/images/en.png id='en' class='flag'/>";
	var de = "<img src=" + ctxPath + "/images/de.png id='de' class='flag'/>";
	
	var div = "<div id='flagDiv'>" + cn 
									+ ko 
									+ en 
									+ de
									+ "</div>";
	$("body").prepend(div);
	
	$("#flagDiv").css({
		"position" : "absolute",
		"left" : marginWidth,
		"top" : marginHeight + getElSize(20)
	});
	
	$(".flag").css({
		"width" : getElSize(100),
		"cursor" : "pointer"
	}).click(changeLang);
	
	$(".flag").css("filter", "grayscale(100%)")
	var lang = window.localStorage.getItem("lang");
	$("#" +lang).css("filter", "grayscale(0%)");
};


function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	location.reload();
};

function changeLang(){
	var lang = this.id;
	var url = window.location.href;
	var param = "?lang=" + lang;
	
	window.localStorage.setItem("lang", lang)
	
	if(url.indexOf("fromDashBoard")!=-1){
		param = "&lang=" + lang;
		if(url.indexOf("&lang")!=-1){
			url = url.substr(0, url.lastIndexOf("&lang"));
		}
	}else{
		url = url.substr(0, url.lastIndexOf("?"))
	}
	
	location.href = url + param;
	
	$(".flag").css("filter", "grayscale(100%)")
	$("#" +lang).css("filter", "grayscale(0%)");
}

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};


function showCorver(){
	$("#corver").css({
		"z-index" : 9999,
		"background-color" : "black",
		//"opacity" : 0.6
	});
};

function closeCorver(){
	$("#corver").css({
		"z-index" : -999,
	});
}


function getBanner(){
	var url = ctxPath + "/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100); 
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			//twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = false;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 300)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},8000 * 2, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};


const setElement = () =>{
	$("html").css({
		"font-family" : "NotoSansCJKkrRegular",
		"overflow" : "hidden"
	});
	
	$("body").css({
		"margin" : 0,
		"padding" : 0
		//"height" : contentHeight - ($("#cat_div").offset().top + $("#cat_div").height()),
	});
	
	$("#container").css({
		"background-image" : "linear-gradient(rgb(27,27,31) 50%, #000000)",
		"width" : contentWidth,
		"height" : contentHeight - ($("#cat_div").height() + getElSize(80 * 2) + $("#header").height()),
//		"height" : contentHeight - ($("#cat_div").offset().top + $("#cat_div").height()),
		"margin-top" : ($("#cat_div").offset().top + $("#cat_div").height()) ,
		"margin-left" : marginWidth
	})
	
	$("#intro").css({
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder"
	});
	
	$("#intro_back").css({
		"width" : contentWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.5,
		"position" : "absolute",
		"background-color" : "black",
		"bottom" : 0 + marginHeight
	})
}


const setDateDesign = () =>{
	const input_css = document.createElement("style")
	
	input_css.innerHTML =
		`
			input[type="date"], .date {
				color : white;
				background-color : black;
				font-size : ${getElSize(24 * 2)};
				border : none;
				width : ${getElSize(256 * 2)}px;
				height : ${getElSize(40 * 2)}px;
			}
		`	
		
		
	document.body.appendChild(input_css);
}
const setSelectDesign = () =>{
	const select_css = document.createElement("style")
	
	select_css.innerHTML =
		`
			select {
				display : inline;
				color : white;
				font-size : ${getElSize(24 * 2)};
				height : ${getElSize(40 * 2)};
				background : url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%;
				background-color : black;
				border : none;
				padding-right : ${getElSize(90)};
				appearance : none;
				-webkit-appearance : none;
				-moz-appearance : none;
				-ms-appearance : none;
				-o-appearance : none;
			}
				
			select option {
				background-color : black;
				font-family : NotoSansCJKkrRegular
			}
		`
		
	
	
	document.body.appendChild(select_css);
}

let handle = 0;
const time = () =>{
	$("#time").html(getToday());
	 handle = requestAnimationFrame(time)
};

const getToday = () =>{

	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));

	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));

	var ty;

	if(hour>12){
		ty = "${pm}";
		hour -= 12;
	}else{
		ty = "${am}";
	};

	
//	return year + "-" + month + "-" + day + " " + ty + " " + hour + " : " + minute + " : " + second;
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
	/*const date = new Date();
	const year = date.getFullYear();
	const month = addZero(String(date.getMonth() + 1))
    const day = addZero(String(date.getDate()))

    const hour = addZero(String(date.getHours()))
    const minute = addZero(String(date.getMinutes()))
    const second = addZero(String(date.getSeconds()))

    return year + "." + month + "." + day + " " + hour + ":" + minute + ":" + second*/
}


let lang = window.sessionStorage.getItem("lang")
let lang_span = "KO"
if(lang == null){
	lang = "ko"
}else if(lang == "ko"){
	lang_span = "EN"
}

const createHeader = () =>{
	let subtitle = "";
		
	if(appTy == "auto"){
		subTitle = "Automotive"
	}else{
		subTitle = "Standard"
	}
	 const div = `
		 			<div id="header"
		 			style=
		 			"
		 				height : ${getElSize(34 * 2)}px
		 				; width : ${getElSize(1920 * 2)}px
		 				; background: #000000
		 				; position : absolute
		 				; z-index : 2
		 				; top : ${marginHeight}px
		 				; left : ${marginWidth}px
		 			"
		 			>
		 				
		 			<img id="clock" src="${ctxPath}/images/FL/default/ico_clock.svg" 
		 					style ="
		 						width : ${getElSize(15 * 2)}px
		 						; height : ${getElSize(15 * 2)}px
		 						; position : absolute
		 						; top : ${getElSize(7 * 2)}px
		 						; left : ${getElSize(1645 * 2)}px
		 					"
		 				>
		 				
		 				<div id="time"
		 					style = "
		 						font-size : ${getElSize(18 * 2)}px
		 						; position : absolute
		 						; width : ${getElSize(170 * 2)}px
		 						; top : ${getElSize(4 * 2)}px
		 						; left : ${getElSize(1668 * 2)}px
		 						; color : #ffffff
		 						
		 					"
		 				></div>
		 				<div id="lang"
		 					style = "
		 						font-size : ${getElSize(18 * 2)}px
		 						; position : absolute
		 						; top : ${getElSize(4 * 2)}px
		 						; left : ${getElSize(1848 * 2)}px
		 						; color : #ffffff
		 						; text-decoration : underline
		 						; color : #09E1FF
		 						; cursor : pointer
		 						
		 					"
		 				>${lang_span}</div>
		 			</div>
		 		`
	$("#container").prepend(div)	 
	
	
	$("#lang").click(()=>{
		if(lang == "en"){
			location.href = location.href.substr(0, location.href.indexOf("?")) + "?lang=ko"
			window.sessionStorage.setItem("lang", "ko")
		}else if (lang == "ko") {
			window.sessionStorage.setItem("lang", "en")
			location.href = location.href.substr(0, location.href.indexOf("?")) + "?lang=en"
		}
	})
	 
	if(is_login){
		let logout = 
			`
				<img id="logout" src="${ctxPath}/images/FL/default/btn_logout_default.svg" 
 					style ="
 						width : ${getElSize(86 * 2)}px
 						; height : ${getElSize(26 * 2)}px
 						; top : ${getElSize(4 * 2)}px
 						; left : ${getElSize(8 * 2)}px
 						; cursor : pointer;
 						; position : absolute
 					"
 				>
			`
			
		let comName = 
			`
			<div id="comName"
				style = "
					font-size : ${getElSize(18 * 2)}px
					; position : absolute
					; top : ${getElSize(4 * 2)}px
					; left : ${getElSize(1530 * 2)}px
					; color : #ffffff
					
				"
			>${subTitle}</div>	
			`
		$("#header").append(logout, comName)
		
	}
}

const createTitle = () =>{
	const div =
		`
			
		 		
		 				<img id="blue_bar" src="${ctxPath}/images/FL/default/title_blue_bar.svg" 
		 					style ="
		 						position : absolute
		 						; width : ${getElSize(290 * 2)}px
		 						; height : ${getElSize(80 * 2)}px
		 						; top : ${getElSize(34 * 2) + marginHeight}px
		 						; left : ${getElSize(0 * 2) + marginWidth}px
		 						; z-index : 2
		 				
		 					"
		 				>	
		 				<img id="title_bar" src="${ctxPath}/images/FL/default/bg_2nd_titlebar.svg" 
		 					style ="
		 						width : ${getElSize(1920 * 2)}px
		 						; top : ${getElSize(34 * 2) + marginHeight}px
		 						; left : ${getElSize(0 * 2) + marginWidth}px
		 						; position : absolute
		 					"
		 				>
		 				
		 	
		`
		
		
	$("#container").append(div)
	
	const middle_bar = 
		`<img id="middle_bar" 
			style = "
				width : ${getElSize(1920 * 2)}px
				
				; position : absolute
				; left : ${getElSize(0) + marginWidth}px
				; top : ${getElSize(114 * 2) + marginHeight}px
				
			
			"
			src="${ctxPath}/images/FL/default/bg_middle_bar.svg"
		 > 		
		`
		
	$("#container").append(middle_bar)
	
	
	
	const factory_logo = 
		`
			<img src="${ctxPath}/images/FL/default/ico_factory.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(42 * 2)}px
					; left : ${getElSize(323 * 2) + marginWidth}px
					; top : ${getElSize(60 * 2) + marginHeight}px
				"
			>
		`
		
	const idoo_control_logo = 
		`
			<img id="idooControlImg" src="${ctxPath}/images/FL/logo/aidoo_control_h_w.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(212.4 * 2)}px
					; left : ${getElSize(1679 * 2) + marginWidth}px
					; top : ${getElSize(52 * 2) + marginHeight}px
				"
			>
		`
		
	const factory_title = 
		`
			<font id="comName_big"
				style = 
				"
					position : absolute
					; z-index : 2
					; font-size : ${getElSize(30 * 2)}px
					; top : ${getElSize(62 * 2) + marginHeight}px
					; left : ${getElSize(378 * 2) + marginWidth}px
					; color : #00C6FF
					; font-family : NotoSansCJKkrBold
					; letter-spacing: -${getElSize(0.3 * 2)}px
					; line-height: ${getElSize(25 * 2)}px
				"
			>
			부광정밀공업(주)
			</font>
		`
		
	$("#container").append(idoo_control_logo, factory_logo, factory_title)
	//; left : ${($("#comName_big").offset().left + $("#comName_big").width() + getElSize(12 * 2))}px
	
	let sub_title_left = "부광정밀공업(주)";
	if(lang == "en"){
		sub_title_left = getElSize(1430) + marginWidth
	}else{
		sub_title_left = getElSize(660 * 2) + marginWidth
	}
	const factory_sub_title = 
		`
			<span
				style = 
				"
					position : absolute
					; z-index : 2
					; font-size : ${getElSize(24 * 2)}px
					; top : ${getElSize(65 * 2) + marginHeight}px
					; left : ${sub_title_left}px
					; color : #ffffff
					; opacity : 0.7
					; font-family : NotoSansCJKkrLight
					; letter-spacing: -${getElSize(0.3 * 2)}px
					; line-height: ${getElSize(25 * 2)}px
				"
			>
			(창원시)
			</span>
		`		
	
	$("#container").append(factory_sub_title)
	
	//widget icons
	
	const home_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_home_default.svg" class="widget" action="home"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(16 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`
	const undo_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_undo_default.svg" class="widget" action="undo"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(72 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`	
	const touchlock_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_touchlock_default.svg" class="widget" action="touchLock"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(128 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`		
	const capture_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_capture_default.svg" class="widget" action="capture"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(184 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`				
	$("#container").append(home_ic, undo_ic, touchlock_ic, capture_ic)
	
	$(".widget").hover((el) =>{
		const src = $(el.currentTarget).attr("src").replace("default.svg", "pushed.svg")
		$(el.currentTarget).attr("src", src)
	}, (el) =>{
		const src = $(el.currentTarget).attr("src").replace("pushed.svg", "default.svg")
		$(el.currentTarget).attr("src", src)
	}).click((el)=>{
		const action = $(el.currentTarget).attr("action")
	
		if(action == "home"){
			location.href = indexPath + "/chart/main.do?lang=" + lang
		}else if(action == "undo"){
			history.back()
		}
	});
	
}

const caldate = (day) => {
	var caledmonth, caledday, caledYear;
	var loadDt = new Date();
	var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
 
	caledYear = v.getFullYear();
 
	if( v.getMonth() < 10 ){
		caledmonth = '0'+(v.getMonth()+1);
	}else{
		caledmonth = v.getMonth()+1;
	}
	if( v.getDate() < 10 ){
		caledday = '0'+v.getDate();
	}else{
		caledday = v.getDate();
	}
	return caledYear + "-" + caledmonth+ '-' + caledday;
}
	

const drawMainIcon = (ic) =>{
	const img = 
		`
			<img src="https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/${ic}.svg"
				style = 
				"
					width : ${getElSize(40 * 2)}px
					; height : ${getElSize(40 * 2)}px
					; position : absolute
					; top : ${getElSize(126 * 2) + marginHeight}px
					; left : ${getElSize(12 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	
		
	let cat_name = "";
	
	if(ic == "monitoring"){
		cat_name = "MONITORING"
	}else if(ic == "analysis"){
		cat_name = "ANALYSIS"
	}else if(ic == "im"){
		cat_name = "INVENTORY"
	}else if(ic == "kpi"){
		cat_name = "KPI"
	}else if(ic == "qm"){
		cat_name = "QUALITY"
	}else if(ic == "tm"){
		cat_name = "TOOL"
	}else if(ic == "om"){
		cat_name = "ORDER"
	}else if(ic == "maintenance"){
		cat_name = "MAINTENANCE"
	}else if(ic == "config"){
		cat_name = "CONFITURATION"
	}else if(ic == "pm"){
		cat_name = "PROGRAM"
	}
	
	const title = 
		`
			<div id="title"
				style = "
					font-size : ${getElSize(20 * 2)}px
					; position : absolute
					; top : ${getElSize(134 * 2) + marginHeight}px
					; left : ${getElSize(60 * 2) + marginWidth}px
					; color : #ffffff
					; z-index : 2
					
				"
			>${cat_name}</div>
		`
		
	$("#container").append(img, title)
}

const bindEvt = () =>{
	try{
		$.datepicker.setDefaults({
    		dateFormat: 'yy-mm-dd',
	prevText: '이전 달',
    		nextText: '다음 달',
        monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    		monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
        dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    		dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
        dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    		showMonthAfterYear: true,
        yearSuffix: '년'
 });

	}catch (e){
		
	}
	
	$("#logout").off().on("click", ()=>{
		deleteCookie("login")
		deleteCookie("login_time")
		deleteCookie("user_id")
		
		window.sessionStorage.removeItem("login_time");
		location.href = indexPath + "/chart/index.do"
	})
	
	$("#logout").hover(()=>{
		$("#logout").attr("src", ctxPath + "/images/FL/default/btn_logout_pushed.svg")
	}, ()=>{
		$("#logout").attr("src", ctxPath + "/images/FL/default/btn_logout_default.svg")
	})
};

const chkLogin = () =>{
	
	var is_login = window.sessionStorage.getItem("login");
	var login_time = window.sessionStorage.getItem("login_time");
	var time = new Date().getTime();
	
	//118.35.20.208
	
	var head = document.getElementsByTagName('head')[0];
    var script= document.createElement('script');

    
    window.getIP = function(json) {
        var url = ctxPath + "/chart/chkIp.do";
        var param = "ip=" + json.ip;
        console.log(json.ip)
        $.ajax({
        		url : url,
        		data : param,
        		dataType : "text",
        		type : "get",
        		success : function(data){
        			console.log(data)
        			if(data != "true"){
        				loginSuccess();
        			}else{
        			}
        		}
        })
    };

    script.type= 'text/javascript';
    script.src= 'https://api.ipify.org?format=jsonp&callback=getIP';
    head.appendChild(script);
//	if(!is_login && location.href.indexOf(indexPath + "/chart/index.do") == -1){
//		
//		location.href = indexPath + "/chart/index.do"
//	}
}

function loginSuccess(){
	if((appName != "index" && appName !="mobile" && appName !="mobile2") && is_login==false){
		location.href = indexPath + "/chart/index.do"
	}
}
const getComName = () => {
	var url = ctxPath + "/chart/getComName.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			$("#title_right").html(decode(data));
		}
	});
};

const addZero = (str) => {
	if(str.length==1) str = "0" + str;
	return str;
}

const setCookie = (name, value, exp)  => {
	var date = new Date();
    date.setTime(date.getTime() + exp * 24 * 60 * 60 * 1000);
    //date.setTime(date.getTime() + 60 * 60 * 1000);

    document.cookie = name + '=' + value + ';expires=' + date.toUTCString() + '; path=/';
};

const getCookie = (name) => {
    var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return value? value[2] : null;
};

let is_login = Boolean(getCookie("login"))

const deleteCookie = (name) => {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
}

const addFontFamily = () =>{
	var font_css = document.createElement('style')
	font_css.innerHTML = 
		`
			@font-face {
				font-family: NotoSansCJKkrDemiLight; 
				src : url(/iDOO_Single_Chart_Status/fonts/NotoSansCJKkr-DemiLight.otf);
			}
			
			@font-face {
				font-family: NotoSansCJKkrBold; 
				src : url(/iDOO_Single_Chart_Status/fonts/NotoSansCJKkr-Bold.otf);
			}
			
			@font-face {
				font-family: NotoSansCJKkrLight; 
				src : url(/iDOO_Single_Chart_Status/fonts/NotoSansCJKkr-Light.otf);
			}
			
			}
			
			@font-face {
				font-family: NotoSansCJKkrRegular; 
				src : url(/iDOO_Single_Chart_Status/fonts/NotoSansCJKkr-Regular.otf);
			}
			
			@font-face {
				font-family: NanumSquareOTFEB; 
				src : url(/iDOO_Single_Chart_Status/fonts/NanumSquareOTFExtraBold.otf);
			}
			
		`
	document.body.appendChild(font_css);
}

const setScrollDesign = () =>{
	var scroll_css = document.createElement('style')
	scroll_css.innerHTML = 
		`
			::-webkit-scrollbar {
                  width: ${getElSize(32 * 2)}px;
                  height: ${getElSize(32 * 2)}px;
            }

            ::-webkit-scrollbar-track-piece {
                  background-color:rgba(0,0,0,0);
            }

            ::-webkit-scrollbar-thumb {
                background: url(${ctxPath}/images/FL/btn_scroll_bar_default.svg) center 50%;
            }
            
            ::-webkit-scrollbar-thumb:ACTIVE {
                background: url(${ctxPath}/images/FL/btn_scroll_bar_pushed.svg) center 50%;
            }
            
            
            
            ::-webkit-scrollbar-button:start:decrement {
                 display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
                background: url(${ctxPath}/images/FL/btn_scroll_up_default.svg) ;
                background-size: 100% 100%
            }
            ::-webkit-scrollbar-button:start:decrement:ACTIVE{
                display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
                background: url(${ctxPath}/images/FL/btn_scroll_up_pushed.svg) ;
                background-size: 100% 100%
            }
            
            
            ::-webkit-scrollbar-button:end:increment {
                display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
                background: url(${ctxPath}/images/FL/btn_scroll_down_default.svg) ;
                background-size: 100% 100%
             }
             ::-webkit-scrollbar-button:end:increment:ACTIVE {
                display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
                background: url(${ctxPath}/images/FL/btn_scroll_down_pushed.svg) ;
                background-size: 100% 100%
             }
             
             
             
             ::-webkit-scrollbar-button:start:horizontal:decrement {
                 display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
//                background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_left_default.svg) ;
				background : url(${ctxPath}/images/FL/btn_scroll_left_default.svg) no-repeat 98% 50%;
                background-size: 100% 100%
            }
            ::-webkit-scrollbar-button:start:horizontal:decrement:ACTIVE{
                display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
//                background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_left_pushed.svg) ;
				background : url(${ctxPath}/images/FL/btn_scroll_left_pushed.svg) no-repeat 98% 50%;
                background-size: 100% 100%
            }
             
             
             ::-webkit-scrollbar-button:end:horizontal:increment {
                display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
//                background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_right_default.svg) ;
				background : url(${ctxPath}/images/FL/btn_scroll_right_default.svg) no-repeat 98% 50%;
                background-size: 100% 100%
             }
             ::-webkit-scrollbar-button:end:horizontal:increment:ACTIVE {
                display: block;
                height: ${getElSize(32 * 2)}px;
                width: ${getElSize(32 * 2)}px;
//                background: url(https://www.digitaltwincloud.com:7443/lib/idoo_dark/imgs/default/btn_scroll_right_pushed.svg) ;
				background : url(${ctxPath}/images/FL/btn_scroll_right_pushed.svg) no-repeat 98% 50%;
                background-size: 100% 100%
             }
             
             

            .k-auto-scrollable, .k-grid-header {
                 background-color : rgb(38,39,43);
                 background : rgb(38,39,43);
             }
			
		`
	document.body.appendChild(scroll_css);
}

$(function(){
	setSelectDesign()
	setDateDesign()
	setScrollDesign()
	
	addFontFamily()
	chkLogin()
	time();
	
	
//	$("#home").click(function(){
//		var url = location.href.substr(0,location.href.indexOf(ctxPath));
//    	//location.href = "/aIdoo/chart/index.do"
//		location.href = "/aIdoo_exhibit/chart/index.do"
//	});
		
	
	
	appName = location.href.split("/")[5]
	appName = appName.substr(0,appName.indexOf("."))
	createHeader()
	
	if(appName != "index" && appName != "main" && appName != "prdStatus"){
		createTitle()
	}
	
	bindEvt()

	
	loadPage()
	
	
	
	
	
	    
	
	
	//getComName();
	
	//drawFlag();
	
	
//	var is_login = window.sessionStorage.getItem("login");
//	var login_time = window.sessionStorage.getItem("login_time");
//	var time = new Date().getTime();
//	if(login==null || (login !=null && (time - login_time) / 1000 > 60 * 20)) {
//		showCorver();
//		$("#loginForm").css("display", "block");
//		$("#email").focus();
//	}else{
//		loginSuccess();
//		drawLogout();
//	}
//	loginSuccess();
//	var url = location.href
//	   
//	if(location.href.lastIndexOf("aIdoo_exhibit") != -1 || location.href.lastIndexOf("aIdoo_namsan_exhibit") != -1){
//		chkBanner();
//	}
	
	
});
