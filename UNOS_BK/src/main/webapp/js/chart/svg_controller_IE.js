var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";

var pieChart1;
var pieChart2;
var borderColor = "";
var totalOperationRatio = 0;

$(function() {
	pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();

	$("#title_main").css({
		left : window.innerWidth/2 - ($("#title_main").width()/2)
	});
	
	//draw = SVG("svg");
	
	//getMarker();
	
//	background_img = draw.image(imgPath+"Road.png").size(window.innerWidth * 0.9, window.innerHeight);
//	background_img.x(450);
//	background_img.y(100);
	
//	var logoText =  draw.text(function(add) {
//		  add.tspan("　　　성역 없는 ").fill("#313131");
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("전원이 참여하는 ").fill("#0005D5").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  add.tspan("　　나로 부터의 ").fill("#c00000").newLine();
//		  add.tspan("혁신!").fill("#ff0000");
//		  
//	});
//		
//	logoText.font({
//			'font-family':'나눔고딕'
//			, size:     75
//			, anchor:   'right'
//			, leading:  '1.3em'
//		   ,'font-weight':'bolder',
//	});
//	
//	logoText.x(2600);
//	logoText.y(1730);

	getMachineInfo();
	//id, width, height, w, h, viewBox, d, transform
	svg("SVG_29", "127.86", "54.955", "380","220","18.24 18.143 127.86 54.955", "M127.86,65.21v25.13c0,0.55-0.45,1-1,1h-10.91c-0.55,0-1,0.45-1,1v7.8c0,0.551-0.45,1-1,1h-7.92" + 
					"c-0.55,0-1,0.45-1,1v3.061c0,0.55-0.45,1-1,1H59.44c-0.55,0-1-0.45-1-1v-3.061c0-0.55-0.45-1-1-1H22.96c-0.55,0-1-0.449-1-1" + 
					"v-7.8c0-0.55-0.45-1-1-1H1c-0.55,0-1-0.45-1-1v-23.02c0-0.55,0.45-1,1-1h23.52c0.55,0,1-0.45,1-1v-8.21c0-0.55,0.45-1,1-1h0.76" + 
					"c0.55,0,1,0.45,1,1v2.35c0,0.55,0.45,1,1,1h21.75c0.55,0,1.24-0.38,1.521-0.85L57.19,52c0.29-0.47,0.96-0.8,1.5-0.75" + 
					"c0.54,0.06,0.99,0.1,0.99,0.1s0.01,0.01,0.02,0.02l-0.01-0.02l0.45,0.05v0.17c0-0.13,0.45-0.23,1-0.23h45.27c0.55,0,1,0.45,1,1" + 
					"v10.87c0,0.55,0.45,1,1,1h18.45C127.41,64.21,127.86,64.66,127.86,65.21z","translate(18.24,-33.1023)");
	
	svg("SVG_20", "134.02", "83.62", "400","300","18.243 18.241 134.02 83.62", "M63.777,37.481v81.62c0,0.55-0.45,1-1,1H0.996c-0.55,0-1-0.45-1-1v-10.21h-46.57c-0.55,0-1-0.45-1-1"+
					"v-13.3h-21.67c-0.55,0-1-0.45-1-1v-30.6c0-0.551,0.45-1,1-1h40.9v-4.67c0-0.55,0.45-1,1-1h9.12v-7.5c0-0.55,0.45-1,1-1h17.22"+
					"v-10.34c0-0.55,0.45-1,1-1h61.78C63.326,36.481,63.777,36.93,63.777,37.481z","translate(88.4864,-18.24)");
	
	svg("SVG_16", "160.32", "66.38", "530","289","18.237 18.238 160.32 66.38", "M81.208,68.171c0.55,0,1,0.45,1,1v37.18c0,0.55-0.45,1-1,1h-50.44c-0.55,0-1,0.45-1,1v3.66"+
					"c0,0.55-0.45,1-1,1h-12.18c-0.55,0-1,0.45-1,1v2.261c0,0.55-0.45,1-1,1h-2.25c-0.55,0-1,0.45-1,1v2.25c0,0.55-0.45,1-1,1h-17.21"+
					"c-0.55,0-1-0.45-1-1v-12.171c0-0.55-0.45-1-1-1h-68.24c-0.55,0-1-0.45-1-1v-37.18c0-0.55,0.45-1,1-1h76.11c0.55,0,1-0.45,1-1"+
					"v-11.03c0-0.55,0.45-1,1-1h32.02c0.55,0,1,0.45,1,1v11.03c0,0.55,0.45,1,1,1H81.208z","translate(96.3495,-36.9029)");
	
	svg("SVG_6", "124.72", "59.8", "400","240","18.242 18.239 124.72 59.8", "M123.723,63.223c0.55,0,1,0.45,1,1v31.06c0,0.55-0.45,1-1,1h-9.341c-0.55,0-1,0.45-1,1v10.57" +
					"c0,0.55-0.45,1-1,1H1.002c-0.55,0-1-0.45-1-1v-57.8c0-0.55,0.45-1,1-1h36.27c0.55,0,1,0.45,1,1v0.95c0,0.55,0.45,1,1,1h30.59" +
					"c0.55,0,1,0.45,1,1v0.95c0,0.55,0.45,1,1,1h27.77c0.55,0,1,0.45,1,1v6.27c0,0.55,0.45,1,1,1H123.723z","translate(18.24,-30.8137)");

});

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;

			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				}
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "blue";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			bodyNeonEffect(borderColor);
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize){
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:     fontSize
		, anchor:   'middle'
		, leading:  '0em'
	});
	
	var wMargin = 0;
	var hMargin = 0;
	
	if(name=="SET UP"){
		hMargin = 30;
	}else if(name.indexOf("WASHING")!=-1){
		wMargin = 10;
	}else if(name.indexOf("NHM")!=-1){
		wMargin = -10;
	}
	
	machineName[arrayIdx].x(x + (w/2) - wMargin);
	machineName[arrayIdx].y(y + (h/2)-(40+hMargin));
	
	machineName[arrayIdx].leading(1);
};

function nl2br(value) {
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;
function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			newMachineStatus = new Array();
			
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			
			
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
			//	if(data.name=="MCM25")svg(data.name, data.width, data.height, data.w, data.h, data.viewBox, data.d, data.transform);
			
				$("#SVG_" + data.dvcId).css({
					"top" : data.ieY,
					"left" : data.ieX,
					"position" : "absolute" 
				})
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(data.x);
				array.push(data.y);
				array.push(data.w);
				array.push(data.h);
				array.push(data.pic);
				array.push(data.lastChartStatus);
				array.push(data.dvcId);
				array.push(data.fontSize);
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
			
//			if(!compare(machineStatus,newMachineStatus)){
//				reDrawMachine();
//				redrawPieChart();
//			};
			
//			if(first){
//				for(var i = 0; i < machineProp.length; i++){
//					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fintSize
//					drawMachine(machineProp[i][0],
//							machineProp[i][1],
//							machineProp[i][2],
//							machineProp[i][3],
//							machineProp[i][4],
//							machineProp[i][5],
//							machineProp[i][6],
//							machineProp[i][7],
//							i,
//							machineProp[i][8],
//							machineProp[i][9]);
//				};
//				first = false;
//			};
			
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
			var totalMachine = 0;
			totalMachine += (inCycleMachine + waitMachine + alarmMachine);
			var totalOperationRatio = Number(inCycleMachine / totalMachine * 100).toFixed(1);
//			pieChart1.series[0].data[0].update(Number(totalOperationRatio));
//			pieChart1.series[0].data[1].update(100-Number(totalOperationRatio));
		}
	});
	
	//setTimeout(getMachineInfo, 3000);
};

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize){
	var svgFile;
	if(status==null){
		svgFile = ".svg";
	}else{
		svgFile = "-" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status=="WAIT" || status=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	//idx, x, y, w, h, name
	printMachineName(i, x, y, w, h, name, text_color, fontSize);
	
	//idx, machineId, w, h
	//setDraggable(i, id, w, h);
	
	machineList[i].dblclick(function(){
		alert("dvc Id=" + dvcId);
	})
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};

function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	//machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
		machineName[arrayIdx].x(x + (w/2) - 30);
		machineName[arrayIdx].y(y + (h/2) - 10);
		
		//DB Access
		setMachinePos(id, x, y);
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};

function svg(id, width, height, w, h, viewBox, d, transform){
	var svg=document.createElementNS(NS,"svg");
	svg.setAttribute("id", id);
	svg.setAttribute("width", width);
	svg.setAttribute("height", height);
	svg.setAttribute("viewBox", viewBox);
	svg.setAttribute("z-index", "99999999");
	svg.setAttribute("position", "absolute");
	
	document.body.appendChild(svg);
	path(d, transform, w, h, id)
};

function path(d, transform, w, h, svg){
	var grad  = document.createElementNS(NS,'linearGradient');
	grad.setAttribute("id", "anim");
	grad.setAttribute("x1", "0%");
	grad.setAttribute("y1", "100%");
	
	var anim  = document.createElementNS(NS,'animate');
	anim.setAttribute("attributeName", "stop-color");
	anim.setAttribute("values", "#C7C402;#FEFE82;#C7C402");
	anim.setAttribute("dur", "500ms");
	anim.setAttribute("repeatCount", "indefinite");
	
	var stop  = document.createElementNS(NS,'stop');
	stop.setAttribute("offset", "0%");
	stop.setAttribute("stop-color", "#FEFE82");
	stop.setAttribute("stop-opacity", "1");
	
	stop.appendChild(anim);
	grad.appendChild(stop);
	document.querySelector('#' + svg).appendChild(grad);
	
	var obj=document.createElementNS(NS,"path");
	obj.setAttribute("d", d);
	obj.setAttribute("transform", transform);
	obj.setAttribute("fill", "url(#anim)");
	
	document.querySelector('#' + svg).appendChild(obj);
	
	scale(svg, w, h)
}

function scale(id, width, height){
	$("#" + id).width(width)
	$("#" + id).height(height)
	$("#" + id).draggable({
		stop : function(){
			var offset = $(this).offset();
            var x = offset.left;
            var y = offset.top;
            
            id = id.substr(id.indexOf("_")+1);
            console.log(x,y, id);
            
            setMachinePosIE(id, x, y);
		}
	});
	
//	$("#" + id).css({
//		"top" :y,
//		"left" : x
//	})
};

function setMachinePosIE(id, x, y){
	var url = "${ctxPath}/setMachinePosIE.do";
	
	
};


