var NS="http://www.w3.org/2000/svg";
var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";

var borderColor = "";
var shopId = 1;
var totalOperationRatio = 0;
var change = 0;
$(function() {
	draw = SVG("svg");
	//getMarker();
	
	var timeStamp = new Date().getMilliseconds();	
	getMachineInfo();
});


var getParameters = function (paramName) {
    // 리턴값을 위한 변수 선언
    var returnValue;

    // 현재 URL 가져오기
    var url = location.href;

    // get 파라미터 값을 가져올 수 있는 ? 를 기점으로 slice 한 후 split 으로 나눔
    var parameters = (url.slice(url.indexOf('?') + 1, url.length)).split('&');

    // 나누어진 값의 비교를 통해 paramName 으로 요청된 데이터의 값만 return
    for (var i = 0; i < parameters.length; i++) {
        var varName = parameters[i].split('=')[0];
        if (varName.toUpperCase() == paramName.toUpperCase()) {
            returnValue = parameters[i].split('=')[1];
            return decodeURIComponent(returnValue);
        }
    }
};


function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			
			noconn = [];
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				if(data.notUse==1){
				//	powerOffMachine--;
				};
				
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
					powerOffMachine++;
					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
				};
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "green";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			
			$(".status_span").remove();
			var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
			$("#container").append(inCycleSpan);
			
			var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
			$("#container").append(waitSpan);
			
			var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
			$("#container").append(alarmSpan);
			
			var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
			$("#container").append(noConnSpan);
			
			$(".total").remove();
			var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
			$("#container").append(totalSpan);
			
			$("#inCycleSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(100),
				"z-index" : 10,
				"color" : "#A3D800",
				"top" : getElSize(1460) + marginHeight,
				"left" : getElSize(1205) + marginWidth
			});
			
			$("#waitSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(100),
				"z-index" : 10,
				"color" : "#FF9100",
				"top" : getElSize(1700) + marginHeight,
				"left" : getElSize(1450) + marginWidth
			});
			
			$("#alarmSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(100),
				"z-index" : 10,
				"color" : "#EC1C24",
				"top" : getElSize(1920) + marginHeight,
				"left" : getElSize(1205) + marginWidth
			});
			
			$("#noConnSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(100),
				"z-index" : 10,
				"color" : "#CFD1D2",
				"top" : getElSize(1700) + marginHeight,
				"left" : getElSize(950) + marginWidth
			});
			
			$("#totalSpan").css({
				"position" : "absolute",
				"font-size" : getElSize(70),
				"z-index" : 10,
				"color" : "#ffffff",
				"top" : getElSize(1680) + marginHeight,
				"left" : getElSize(1200) + marginWidth,
				"text-align" : "center"
			});
			
			$("#total_title").css({
				"font-size" : getElSize(45),
			});
		}
	});
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize, dvcId, notUse){
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:     fontSize
		, anchor:   'middle'
		, leading:  '0em'
	});
	
	if(notUse==0){
		machineName[arrayIdx].dblclick(function(){
			if(dvcId!=0){
				window.localStorage.setItem("dvcId", dvcId);
				window.localStorage.setItem("name", name);
				
				//location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
				location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
			};
		})
	}
	
	var wMargin = 0;
	var hMargin = 0;
	
	machineName[arrayIdx].x(x + (w/2) - wMargin);
	machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/50)+hMargin));
	
	machineName[arrayIdx].leading(1);
};

function nl2br(value) { 
	  return value.replace(/<br>/g, "\n");
};

var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var machineArray2 = new Array();
var first = true;

var preTimestap = 0;

var maxTime = "";
function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo.do";
	var date = new Date();
	
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	var today = year + "-" + month + "-" + day;
	
	var param = "shopId=" + shopId + 
				"&startDateTime=" + today;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			
			var nowTime = new Date().getTime();
			
			//console.log((nowTime - preTimestap) / 1000);
			preTimestap = nowTime
			
			
			var json = data.machineList;
			
			newMachineStatus = new Array();
			machineProp = [];
			noconn = [];
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;

			operationTime = 0;
			maxTime = new Date(json[0].workDate.substr(0,4), json[0].workDate.substr(5,2), json[0].workDate.substr(8,2), json[0].workDate.substr(11,2),json[0].workDate.substr(14,2), json[0].workDate.substr(17,2))
			
			$(json).each(function(key, data){
				var time = data.workDate;
				var uDate = new Date(time.substr(0,4), time.substr(5,2), time.substr(8,2), time.substr(11,2),time.substr(14,2), time.substr(17,2))
				
				if(maxTime<uDate) maxTime = uDate
				
				var array = new Array();
				var machineColor = new Array();
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(getElSize(data.x)-$("#svg").offset().left + marginWidth);
				array.push(getElSize(data.y));
				array.push(getElSize(data.w * 0.8));
				array.push(getElSize(data.h * 0.8));
				array.push(data.pic);
				
				if(data.notUse==1){
					array.push("NOTUSE");
					//powerOffMachine--;
				}else{
					array.push(data.lastChartStatus);
				}
				array.push(data.dvcId);
				array.push(getElSize(data.fontSize));
				array.push(data.notUse);
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
					powerOffMachine++;
					noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
				};
				
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			maxTime = timeFormatter(maxTime)
			
			//$("#time").html("데이터수신시간 : " + maxTime);
			
			if(!compare(machineStatus,newMachineStatus)){
				reDrawMachine();
				redrawPieChart();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					//array = id, name, x, y, w, h, pic, status, idx, dvcId, fontSize, notUse
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8],
							machineProp[i][9],
							machineProp[i][10]);
				};
				first = false;
			};
			
			//cal totalOperationRatio
			totalOperationRatio = 0;
			for(var i = 0; i < machineArray2.length; i++){
				totalOperationRatio += machineArray2[i][20];
			};
			
		}
	});
	
	setTimeout(getMachineInfo, 3000);
};

//시간 포맷팅 해주는 넘
function timeFormatter(time){
	var year = time.getFullYear();
	var month = addZero(String(time.getMonth()));
	var day = addZero(String(time.getDate()));
	var hour = time.getHours();
	var minute = addZero(String(time.getMinutes()));
	var second = addZero(String(time.getSeconds()));
	
	var ty;
	//hour가 12 이상이면 오후로 바꾼다.
	//12를 빼줘야 그게 적용되지요
	if(hour>12){
		ty = "오후";
		hour -= 12;
	}else{
		ty = "오전"
	}
	
	return today = year + "-" + month + "-" + day + " " + ty + " " + hour + " : " + minute + " : " + second
	
}

function replaceAll(str, src, target){
	return str.split(src).join(target)
};

function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8],
					machineProp[i][9],
					machineProp[i][10]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			//marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

function drawMachine(id, name, x, y, w, h, pic, status, i, dvcId, fontSize, notUse){
	var svgFile;
	var timeStamp = new Date().getMilliseconds();
	if(status==null){
		svgFile = ".svg";
	}else{
		svgFile = "-" + status + ".svg";
	};
	
//	console.log("id :" + dvcId + " ,w :" + w + " ,h :" +h +" ,x :" + x + " ,y :" + y);
	//박스 생성 공간부족해서 예외 DVC_ID 사이즈 줄임
	if(/*dvcId==27 || dvcId==86 || dvcId==99 ||*/ dvcId==88 || dvcId==87 || dvcId==85 || dvcId==84 || dvcId==83 || dvcId==76 || dvcId==77 || dvcId==78){
		machineList[i] = draw.image(imgPath + pic + "-" + status + "-if.svg" + "?dummy=" + timeStamp).size(w, h);
		machineList[i].x(x);
		machineList[i].y(y);
	}else{
		machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
		machineList[i].x(x);
		machineList[i].y(y);
	}
	var text_color;
	if(status=="WAIT" || status=="NO-CONNECTION" || status=="IN-CYCLE"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	//idx, x, y, w, h, name
	console.log("asdasdasd")
	if(getParameters('dvcId')==1){
		name = "I" + dvcId;
	}else{
		name = name.substr(name.lastIndexOf("#")-1);
	}
	
	
//	text_color = "blue"
//	fontSize = 8
	
	printMachineName(i, x, y, w, h, name, text_color, fontSize, dvcId, notUse);
	
	//idx, machineId, w, h

	//setDraggable(i, id, w, h);
	
	if(notUse==0){
		machineList[i].dblclick(function(){
			if(dvcId!=0){
				window.localStorage.setItem("dvcId", dvcId);
				window.localStorage.setItem("name", name);
				
				//location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
				location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
			};
		})
	}
	
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};


function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
		machineName[arrayIdx].x(x + (w/2) - getElSize(30));
		machineName[arrayIdx].y(y + (h/2) - getElSize(10));
		
		//DB Access
		setMachinePos(id, setElSize(x - marginWidth + $("#svg").offset().left) , setElSize(y - marginHeight) );
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};